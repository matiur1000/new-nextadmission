  <?php
	require_once("function.php");
	
	$student_data= new OnlineForm;
  ?>
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Online Form</title>

    <!--bootstrap link -->
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">


<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <style>
	</style>
	
		
  </head>
  <body width="100%" height="100%" style="background:#e3e3e3" >
	<div class="container-fluid" width="100%" height="100%">
			<div class="row" align="center">
			<div class="col-lg-12">
<?php
	if(isset($_POST['submit'])){
			$name=$_POST['name'];
			$mobile=$_POST['mobile'];
			$country=$_POST['country'];
			$purpose=$_POST['purpose'];
			$course=$_POST['course'];
			$city=$_POST['city'];
			$education=$_POST['education'];
			
			if($name=='' or $mobile=='' or $country=='' or $purpose=='' or $course=='' or $city=='' or $education==''){
				
      echo "<h3 style='color:red;'>Required: please fill up the empty fields.</h3>";
			}
			
			else {
				$data=$student_data->Insert($name,$mobile,$country,$purpose,$course,$city,$education);
			}
	}
?>				
			
			
				<div class="col-lg-3 col-md-6 col-xs-12"></div>
				
				
				<div class="col-lg-6 col-md-6 col-xs-12" style="padding: 20px; background:#fff; border: 1px solid rgb(5, 86, 175);
            border-radius: 10px;">
				<div class="table" align="center" style="padding-left:110px">
					<div class="col-lg-12 col-md-12 col-xs-12">
<form class="form-horizontal" action="index.php" method="post">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="name" placeholder="আপনার নাম লিখুন">
    </div>
  </div>
  
  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Mobile No:<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="mobile" placeholder="মোবাইল নম্বর লিখুন">
    </div>
  </div>

  <div class="form-group">
    <label for="contact" class="col-sm-2 control-label">Interested Country<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="country" placeholder="কোন দেশে যেতে চান">
    </div>
  </div>
  <div class="form-group">
    <label for="note" class="col-sm-2 control-label">Purpose Study or Others?<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="purpose" placeholder="অধ্যয়ন বা অন্য">
    </div>
  </div>

 
  <div class="form-group">
    <label for="address" class="col-sm-2 control-label">Interested Course<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="course" placeholder="অধ্যয়ন হলে কোন কোর্স">
    </div>
  </div>
  

  <div class="form-group">
    <label for="note" class="col-sm-2 control-label">From Which City?<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="city" placeholder="এখন কোথায় থাকেন">
    </div>
  </div>

  <div class="form-group">
    <label for="note" class="col-sm-2 control-label">Education Qualification<font style="color:red;">*</font></label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="education" placeholder="শিক্ষাগত যোগ্যতা">
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <button type="submit" class="btn btn-success" name="submit">Submit</button>
    </div>
  </div>
</form>      
					

					</div>
				</div>					</div>
				
				<div class="col-lg-3 col-md-6 col-xs-12"></div>
			</div>
			

			</div>
		
	</div>
  </body>
</html>