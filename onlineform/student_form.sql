-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2016 at 04:15 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `online_form`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_form`
--

CREATE TABLE IF NOT EXISTS `student_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `mobile_no` int(20) NOT NULL,
  `interested_country` varchar(200) NOT NULL,
  `purposes` varchar(200) NOT NULL,
  `interested_course` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `edu_qualification` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `student_form`
--

INSERT INTO `student_form` (`id`, `name`, `mobile_no`, `interested_country`, `purposes`, `interested_course`, `city`, `edu_qualification`) VALUES
(1, 'shewa', 0, 'india', 'study', '0', 'dhaka', 'diploma'),
(4, 'shewa', 1921711839, 'india', 'study', '0', 'dhaka', 'diploma'),
(5, 'shewa', 1921711839, 'india', 'study', '0', 'dhaka', 'diploma'),
(6, 'shewa', 1921711839, 'india', 'study', '0', 'dhaka', 'diploma'),
(7, 'shewa', 1921711839, 'india', 'study', '0', 'dhaka', 'diploma'),
(8, 'shewa', 1921711839, 'india', 'study', 'computer', 'dhaka', 'diploma');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
