<?php

	class M_search_table extends CI_Model {
	
		const TABLE	= 'search_table';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);    
		}
		
		
		public function adValueChkByDate($where, $publish_date, $expire_date, $select_website, $region)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->where('booking_date >= "'.$publish_date.'" and booking_date <= "'.$expire_date.'"');
			if($select_website =='main'){
			 $this->db->where(array('region' => ""));
			}else{
			  $this->db->where(array('region' => $region));	
			}
			$query = $this->db->get();
			return $query->result();
		}
		
		
		
		
		
		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		


	    public function findByBlogId($blogId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('blog_id', $blogId);
			$query = $this->db->get();
			return $query->row();
		}


		 public function findByAdId($adId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('ad_id', $adId);
			$query = $this->db->get();
			return $query->row();
		}
		


		public function findByProgId($progId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('programe_id', $progId);
			$query = $this->db->get();
			return $query->row();
		}


		public function findByAllSearchData($search_all)
		{
			$this->db->select('search_table.*,all_user_registration.image,general_user_details.mobile,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.res_user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'search_table.res_user_id = general_user_details.user_id', 'left');
			$this->db->join('organization_user_details', 'search_table.res_user_id = organization_user_details.user_id', 'left');
			$this->db->like('search_table.user_type', $search_all);
			$this->db->or_like('search_table.name', $search_all); 
			$query = $this->db->get();
			return $query->result();
		}



       public function findByAllSearchTitle($search_all)
		{
			$this->db->select('search_table.*,all_user_registration.image,user_post_manage.description');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.blog_user_id = all_user_registration.id', 'left');
			$this->db->join('user_post_manage', 'search_table.blog_id = user_post_manage.id', 'left');
			$this->db->like('search_table.blog_title', $search_all);
			$query = $this->db->get();
			return $query->result();
		}


		public function findByAllSearchProgram($search_all)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->like('search_table.program_name', $search_all);
			$query = $this->db->get();
			return $query->result();
		}


		public function findByAllSearchAd($search_all)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.ad_user_id = all_user_registration.id', 'left');
			$this->db->like('search_table.ad_title', $search_all);
			$query = $this->db->get();
			return $query->result();
		}



		public function findByAllAdvanceSearchData($search_all, $country_id, $organize_id, $program_id)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->where('( MATCH (search_table.program_details) AGAINST (+"'.$search_all.'"))');
			$this->db->where('search_table.country_id', $country_id);
			$this->db->where('search_table.organize_programe_id', $organize_id);
			$this->db->where('search_table.programe_id', $program_id);
			$query = $this->db->get();
			return $query->result();
		}


       public function findByAllAdvanceSearchData2($search_all, $country_id, $organize_id)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->like('search_table.program_name', $search_all);
			$this->db->where('search_table.country_id', $country_id);
			$this->db->where('search_table.organize_programe_id', $organize_id);
			$query = $this->db->get();
			return $query->result();
		}



		 public function findByAllAdvanceSearchData3($search_all, $organize_id, $program_id)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->where('( MATCH (search_table.program_details) AGAINST (+"'.$search_all.'"))');
			$this->db->where('search_table.organize_programe_id', $organize_id);
			$this->db->where('search_table.programe_id', $program_id);
			$query = $this->db->get();
			return $query->result();
		}


		public function findByAllAdvanceSearchData4($search_all, $country_id, $program_id)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->where('( MATCH (search_table.program_details) AGAINST (+"'.$search_all.'"))');
			$this->db->where('search_table.country_id', $country_id);
			$this->db->where('search_table.programe_id', $program_id);
			$query = $this->db->get();
			return $query->result();
		}



		public function findByAllOrgGenAdvanceSearchData($search_all, $country_id)
		{
			$this->db->select('search_table.*,all_user_registration.image,general_user_details.mobile,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.res_user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'search_table.res_user_id = general_user_details.user_id', 'left');
			$like   = "( search_table.user_type like '%".$search_all."%' OR search_table.name like '%".$search_all."%' )";
			$this->db->join('organization_user_details', 'search_table.res_user_id = organization_user_details.user_id', 'left');
			$this->db->where($like);
			$this->db->where('search_table.country_id', $country_id);
			$query = $this->db->get();
			return $query->result();
		}




		public function findByAllProgramAdvanceSearchData($search_all, $country_id)
		{
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->like('search_table.program_name', $search_all);
			$this->db->where('search_table.country_id', $country_id);
			$query = $this->db->get();
			return $query->result();
		}




		 public function findByAllTitleAdvanceSearchData($search_all, $country_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,user_post_manage.description');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.blog_user_id = all_user_registration.id', 'left');
			$this->db->join('user_post_manage', 'search_table.blog_id = user_post_manage.id', 'left');
			$this->db->like('search_table.blog_title', $search_all);
			$this->db->where('search_table.country_id', $country_id);
			$query = $this->db->get();
			return $query->result();
		 }





		 public function findByAllAdAdvanceSearchData($search_all, $country_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.ad_user_id = all_user_registration.id', 'left');
			$this->db->like('search_table.ad_title', $search_all);
			$this->db->where('search_table.country_id', $country_id);
			$query = $this->db->get();
			return $query->result();
		  }


         public function findByAllProgramAdvanceSearchByOrg($search_all, $organize_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->like('search_table.program_name', $search_all);
			$this->db->where('search_table.organize_programe_id', $organize_id);
			$query = $this->db->get();
			return $query->result();
		 }




		 public function findByAllTitleAdvanceSearchByOrg($search_all, $organize_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,user_post_manage.description');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.blog_user_id = all_user_registration.id', 'left');
			$this->db->join('user_post_manage', 'search_table.blog_id = user_post_manage.id', 'left');
			$this->db->like('search_table.blog_title', $search_all);
			$this->db->where('search_table.blog_user_id', $organize_id);
			$query = $this->db->get();
			return $query->result();
		 }





		 public function findByAllAdAdvanceSearchByOrg($search_all, $organize_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.ad_user_id = all_user_registration.id', 'left');
			$this->db->like('search_table.ad_title', $search_all);
			$this->db->where('search_table.ad_user_id', $organize_id);
			$query = $this->db->get();
			return $query->result();
		 }




		 public function findByAllProgramAdvanceSearchByProId($search_all, $program_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->where('( MATCH (search_table.program_details) AGAINST (+"'.$search_all.'"))');
			$this->db->where('search_table.programe_id', $program_id);
			$query = $this->db->get();
			return $query->result();
		 }


		 public function findByAllAsiaSearchData($country_id, $organize_id, $program_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->where('search_table.country_id', $country_id);
			$this->db->where('search_table.organize_programe_id', $organize_id);
			$this->db->where('search_table.programe_id', $program_id);
			$query = $this->db->get();
			return $query->result();
		 }


		 public function findByAllCountrySearchData($organize_id, $program_id)
		 {
			$this->db->select('search_table.*,all_user_registration.image,all_user_registration.name as regName,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'search_table.organize_programe_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'search_table.organize_programe_id = organization_user_details.user_id', 'left');
			$this->db->where('search_table.organize_programe_id', $organize_id);
			$this->db->where('search_table.programe_id', $program_id);
			$query = $this->db->get();
			return $query->result();
		 }



		
		
		
		public function update2($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}

		public function updateBlog($data, $blogId)
		{
			$this->db->update(self::TABLE, $data, array('blog_id' => $blogId));        
		}
		

		public function updateProg($data, $progId)
		{
			$this->db->update(self::TABLE, $data, array('programe_id' => $progId));        
		}
		
		
		public function updateAd($data, $adId)
		{
			$this->db->update(self::TABLE, $data, array('ad_id' => $adId));        
		}
		
		
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}



		public function destroyByBlockId($id)
		{
			$this->db->delete(self::TABLE, array('blog_id' => $id));        
		}



		public function destroyByProId($proId)
		{
			$this->db->delete(self::TABLE, array('programe_id' => $proId));        
		}

		public function destroyOPromotion($promoId)
		{
			$this->db->delete(self::TABLE, array('ad_id' => $promoId));        
		}

		public function destroyUser($userId)
		{
			$this->db->delete(self::TABLE, array('res_user_id' => $userId));        
		}



			
	}
?>