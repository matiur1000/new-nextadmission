<?php

	class M_menu_manage extends CI_Model {
	
		const TABLE	= 'menu_manage';
	
		public function __construct()
		{
				parent::__construct();
		}		
				
	    public function save($data)
		{
		  $this->db->insert(self::TABLE, $data);  
		  return $this->db->insert_id();  
		}
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findMaxSl()
		{
			$this->db->select('max(sl_no) as sl_no');
			$this->db->from(self::TABLE);
			$query = $this->db->get();
			return $query->row()->sl_no+1;
		}
		
		
		public function findByCatId($categoryId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $categoryId);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findByName($category)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('category', $category);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findAllAdmin($where = array(), $onset = 0)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->order_by('id', 'desc');
			$this->db->limit(5, $onset); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function countAll($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);			
			return $this->db->count_all_results();
		}
		
		public function findAllTopMenu()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('position_top' => "Yes", 'sub_menu_status' => "Available"));
			$this->db->order_by('id', 'desc');
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findAllFooterMenu()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('position_footer' => "Yes", 'sub_menu_status' => "Available"));
			$this->db->order_by('id', 'desc');
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findAll($where)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->order_by('id', 'asc');
			//$this->db->limit(10, 0);
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllDetailes($where = array())
		{
			$List 		= $this->findAll($where);
			
			foreach($List as $model)
			{
				$sub 		= $this->M_submenu_manage->findAllDetailes(array("menu_id" => $model->id));
				$model->sub = $sub;
			
			}
			return $List;
		}
		
		
		public function findSlDesc()
		{
			$this->db->select('max(sl_no) as sl_no');
			$this->db->from(self::TABLE);
			$query = $this->db->get();
			return $query->row()->sl_no + 1;
		}
			
		
		
		public function update($data, $id)
			{
				$this->db->update(self::TABLE, $data, array('id' => $id));        
			}
	
		public function destroy($id)
		{
		  $this->db->delete(self::TABLE, array('id' => $id));    
		}	
		
		
		
		public function findAllFooterMenu1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('position_footer' => 'Yes','status' => 'Available'));
			$this->db->order_by('id', 'asc');
			$this->db->limit(4, 0);
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllFooterMenu2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('position_footer' => 'Yes','status' => 'Available'));
			$this->db->order_by('id', 'asc');
			$this->db->limit(4, 4);
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllFooterMenu3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('position_footer' => 'Yes','status' => 'Available'));
			$this->db->order_by('id', 'asc');
			$this->db->limit(4, 8);
			$query = $this->db->get();
			return $query->result();
		}
			
			
	   public function findAllFooterMenu4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('position_footer' => 'Yes','status' => 'Available'));
			$this->db->order_by('id', 'asc');
			$this->db->limit(2, 12);
			$query = $this->db->get();
			return $query->result();
		}
		
		
			
	}

