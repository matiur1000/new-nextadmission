<?php

	class M_submenu_manage extends CI_Model {
	
		const TABLE	= 'sub_menu_manage';
	
		public function __construct()
		{
				parent::__construct();
		}		
				
	    public function save($data)
		{
		  $this->db->insert(self::TABLE, $data);    
		}
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findMaxSl()
		{
			$this->db->select('max(sl_no) as sl_no');
			$this->db->from(self::TABLE);
			$query = $this->db->get();
			return $query->row()->sl_no+1;
		}
		
		
		public function findAll($menuId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('menu_id', $menuId);
			$this->db->order_by('id', 'desc');
			//$this->db->limit(10, 0);
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllName(Array $where = array('status' => 'Available'))
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findByDetails($where)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findAllDetailes($where = array())
		{
			$List 		= $this->findAllName($where);
			
			foreach($List as $model)
			{
				$deeperSub 		  = $this->M_deeper_sub->findAllDep(array("sub_menu_id" => $model->id));
				$model->deeperSub = $deeperSub;
			
			}
			return $List;
		}
		
		
		public function findAllCat($where = array(), $onset = 0)
		{
			$this->db->select('sub_menu_manage.*, menu_manage.menu_name');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('menu_manage', 'sub_menu_manage.menu_id = menu_manage.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(5, $onset); 
			$query = $this->db->get();
			return $query->result();
		}
		
		/*public function findAllCat($a, $b)
		{
			$this->db->select($a.*, $b.menu_name);
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join($a, $b.menu_id = $b.id, 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(5, $onset); 
			$query = $this->db->get();
			return $query->result();
		}*/
		
		public function countAll($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);			
			return $this->db->count_all_results();
		}	
		
		
		public function update($data, $id)
			{
				$this->db->update(self::TABLE, $data, array('id' => $id));        
			}
	
		public function destroy($id)
		{
		  $this->db->delete(self::TABLE, array('id' => $id));    
		}	
			
			
	}

