<?php

	class M_job_post_manage extends CI_Model {
	
		const TABLE	= 'job_post_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data); 
			 return $this->db->insert_id();       
		}
		
		
		
		
		
		public function findAllHome()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->group_by("job_post_manage.user_id", "asc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAll()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllByOrganize($organizeId)
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where('job_post_manage.user_id', $organizeId);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllByCountry($country_name)
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('country_name' => $country_name));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllAsia()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('region' => "Asia"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllSoutAm()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('region' => "South America"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllnorthAm()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('region' => "North America"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllAfrica()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('region' => "Africa"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllAustrallia()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('region' => "Australia"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllEurop()
		{
			$this->db->select('job_post_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('region' => "Europ"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findById($id)
		{
			$this->db->select('job_post_manage.*, all_user_registration.name,  organization_user_details.address, organization_user_details.designation');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'job_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('organization_user_details', 'job_post_manage.user_id = organization_user_details.user_id', 'left');
			$this->db->where('job_post_manage.id', $id);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findByOrganize($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('user_id', $userId);
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllDetailes($userId)
		{
			$List 		= $this->findByOrganize($userId);
			
			foreach($List as $model)
			{
				$model->totalApplicant		= $this->M_job_application_details->findAllDep(array("post_id" => $model->id));
				$model->viewed				= $this->M_job_application_details->findAllDep(array("post_id" => $model->id, "application_view_status" => "view"));
				$model->notviewed			= $this->M_job_application_details->findAllDep(array("post_id" => $model->id, "application_view_status" => "notview"));
				$model->sortlist			= $this->M_job_application_details->findAllDep(array("post_id" => $model->id, "shortlist_status" => "yes"));
				$model->star				= $this->M_job_application_details->findAllDep(array("post_id" => $model->id, "star_candidate_status" => "yes"));			
			}
			return $List;
		}
		
		public function findByDraft($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('user_id' => $userId, 'draft_job_status' => "yes"));
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllDraft($userId)
		{
			$List 		= $this->findByDraft($userId);
			
			foreach($List as $model)
			{
				$model->totalApplicant		= $this->M_job_application_details->findAllDraft(array("post_id" => $model->id));
				$model->viewed				= $this->M_job_application_details->findAllDraft(array("post_id" => $model->id, "application_view_status" => "view"));
				$model->notviewed			= $this->M_job_application_details->findAllDraft(array("post_id" => $model->id, "application_view_status" => "not view"));
				$model->sortlist			= $this->M_job_application_details->findAllDraft(array("post_id" => $model->id, "shortlist_status" => "yes"));
				$model->star				= $this->M_job_application_details->findAllDraft(array("post_id" => $model->id, "star_candidate_status" => "yes"));			
			}
			return $List;
		}
		
		public function findByAchived($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('user_id' => $userId, 'archive_status' => "yes"));
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllArchived($userId)
		{
			$List 		= $this->findByAchived($userId);
			
			foreach($List as $model)
			{
				$model->totalApplicant		= $this->M_job_application_details->findAllAchived(array("post_id" => $model->id));
				$model->viewed				= $this->M_job_application_details->findAllAchived(array("post_id" => $model->id, "application_view_status" => "view"));
				$model->notviewed			= $this->M_job_application_details->findAllAchived(array("post_id" => $model->id, "application_view_status" => "not view"));
				$model->sortlist			= $this->M_job_application_details->findAllAchived(array("post_id" => $model->id, "shortlist_status" => "yes"));
				$model->star				= $this->M_job_application_details->findAllAchived(array("post_id" => $model->id, "star_candidate_status" => "yes"));			
			}
			return $List;
		}
		
		public function countLatestJob($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('user_id' => $userAutoId));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function countLatestDraft($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('user_id' => $userAutoId, 'draft_job_status' => "yes"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function countLatestAchived($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('user_id' => $userAutoId, 'archive_status' => "yes"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}


		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>