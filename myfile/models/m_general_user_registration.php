<?php

	class M_general_user_registration extends CI_Model {
	
		const TABLE	= 'general_user_details';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data); 
		}
		
		public function findAll(Array $where = array())
		{
			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');
			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');
			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(10, 0);
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAllGeneral(Array $where = array())
		{
			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');
			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');
			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(10, 0);
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllValu($id)
		{
			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');
			$this->db->from(self::TABLE);
			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');
			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');
			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');
			$this->db->where('all_user_registration.id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findByEmail($user_id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function findAllGeneralUserMobile($user_id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findByUserName($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('user_id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findByAllInstitute()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "asc");
			$this->db->group_by('institute_name'); 
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findByAllSubject()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "asc");
			$this->db->group_by('group'); 
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('user_id' => $id));        
		}
		
		
		public function update2($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
