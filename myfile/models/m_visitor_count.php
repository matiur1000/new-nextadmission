<?php

	class M_visitor_count extends CI_Model {
	
		const TABLE	= 'total_post_visitor_count';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
	

		public function findByOrganize($user_id, $onset = 0)
		{
		
			$this->db->select('total_post_visitor_count.*,all_user_registration.name,all_user_registration.user_id,general_user_details.mobile,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'total_post_visitor_count.visitor_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'total_post_visitor_count.visitor_id = general_user_details.user_id', 'left');
			$this->db->join('organization_user_details', 'total_post_visitor_count.visitor_id = organization_user_details.user_id', 'left');
			$this->db->where(array('total_post_visitor_count.organize_id' => $user_id));
			$this->db->where('visitor_id !=', "unknown");
			$this->db->where('visitor_id !=', $user_id);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(6, $onset); 
			$query = $this->db->get();
			return $query->result();
		}	



		public function findByOrganizeId($where = array())
		{
		
			$this->db->select('total_post_visitor_count.*,all_user_registration.user_id,all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'total_post_visitor_count.visitor_id = all_user_registration.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}	


		public function countAll($user_id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('organize_id' => $user_id));		
			$this->db->where('visitor_id !=', "unknown");
			$this->db->where('visitor_id !=', $user_id);
			return $this->db->count_all_results();
		}


		public function findAllBycurrentDate($user_id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currantDate = date('Y-m-d');
			$this->db->where(array('organize_id' => $user_id, 'date' => $currantDate));		
			return $this->db->count_all_results();
		}

		public function findAllBycurrentWeek($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);	
			return $this->db->count_all_results();
		}

        public function findAllBycurrentMonth($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);		
			return $this->db->count_all_results();
		}



		public function visitorSort($where = array())
		{
		
			$this->db->select('total_post_visitor_count.*,all_user_registration.name,all_user_registration.user_id,general_user_details.mobile,organization_user_details.mobile_com');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'total_post_visitor_count.visitor_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'total_post_visitor_count.visitor_id = general_user_details.user_id', 'left');
			$this->db->join('organization_user_details', 'total_post_visitor_count.visitor_id = organization_user_details.user_id', 'left');
			$this->db->where($where);
			$this->db->where('total_post_visitor_count.visitor_id !=', "unknown");
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		
		
		
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>