<?php

	class M_job_application_details extends CI_Model {
	
		const TABLE	= 'job_application_details';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		
		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		public function findByUser($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('app_user_id', $userId);
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function viewApplication($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('app_user_id' => $userId, 'application_view_status' => "view"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function shortList($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('app_user_id' => $userId, 'shortlist_status' => "yes"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function findAllDep($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function findAllDraft($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function findAllAchived($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		public function countApplicationJob($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_user_id' => $userAutoId));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function countViewJob($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_user_id' => $userAutoId, 'application_view_status' => "view"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function countShortViewJob($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_user_id' => $userAutoId, "shortlist_status" => "yes"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		public function countNotViewJob($userAutoId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_user_id' => $userAutoId, "application_view_status" => "not view"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		public function countJobWiseApplication($jobId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_id' => $jobId));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		public function countJobWiseView($jobId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_id' => $jobId, 'application_view_status' => "view"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		public function countJobWiseNotView($jobId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_id' => $jobId, 'application_view_status' => "notview"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		public function countJobWiseShortList($jobId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_id' => $jobId, 'shortlist_status' => "yes"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		public function countJobWiseStarCandidates($jobId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_id' => $jobId, 'star_candidate_status' => "yes"));
			$query = $this->db->get();
			return  $rowcount = $query->num_rows();	
		}
		
		
		public function findAllByUser($appUserId)
		{
			$this->db->select('job_application_details.*, job_post_manage.title');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('job_post_manage', 'job_application_details.post_id = job_post_manage.id', 'left');
			$this->db->where('job_application_details.app_user_id', $appUserId);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findByView($appUserId)
		{
			$this->db->select('job_application_details.*, job_post_manage.title');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('job_post_manage', 'job_application_details.post_id = job_post_manage.id', 'left');
			$this->db->where(array('app_user_id' => $appUserId, 'application_view_status' => "view"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function shortListCom($appUserId)
		{
			$this->db->select('job_application_details.*, job_post_manage.title');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('job_post_manage', 'job_application_details.post_id = job_post_manage.id', 'left');
			$this->db->where(array('app_user_id' => $appUserId, 'shortlist_status' => "yes"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findByJobId($jobId)
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image,  
			general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->where('job_application_details.post_id', $jobId);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		public function findByCvView($jobId)
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image,  
			general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->where(array('post_id' => $jobId, 'application_view_status' => "view"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findByCvNotView($jobId)
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image,  
			general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->where(array('post_id' => $jobId, 'application_view_status' => "notview"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		public function findByshortList($jobId)
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image,  
			general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->where(array('post_id' => $jobId, 'shortlist_status' => "yes"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findBystarCan($jobId)
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image,  
			general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->where(array('post_id' => $jobId, 'star_candidate_status' => "yes"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		
		
		// QUICK SEARCH QUERY START

		public function applySearch($where = array())
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image, general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}


		public function applySearch2($where2 = array())
		{
			$this->db->select('job_application_details.*, all_user_registration.name, all_user_registration.image, general_user_details.mobile,  general_user_details.email, general_user_details.address, general_user_details.degree_title,
			 general_user_details.institute_name, general_user_details.date_of_birth');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'job_application_details.app_user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'job_application_details.app_user_id = general_user_details.user_id', 'left');
			$this->db->where($where2);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}


		// QUICK SEARCH QUERY END
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findByIdApplyId($postId, $applyId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('post_id' => $postId, 'app_user_id' => $applyId));
			$query = $this->db->get();
			return $query->row();
		}
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		
		public function update2($data, $applyUserId, $postId)
		{
			$this->db->update(self::TABLE, $data, array('post_id' => $postId, 'app_user_id' => $applyUserId));        
		}
		
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>