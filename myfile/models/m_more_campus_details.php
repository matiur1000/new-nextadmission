<?php

	class M_more_campus_details extends CI_Model {
	
		const TABLE	= 'more_campus_details';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		
		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('organization_id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('organization_id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>