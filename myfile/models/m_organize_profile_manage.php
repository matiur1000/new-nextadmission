<?php

	class M_organize_profile_manage extends CI_Model {
	
		const TABLE	= '';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($table, $data)
		{
			$this->db->insert($table, $data);        
		}
		
		
		
		public function findAboutByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "about"));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findContactByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "contact"));
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findById($table, $id)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('id' => $id));
			$query = $this->db->get();
			return $query->row();
		}

		

		public function findbyByGenUser($table, $sendToUser, $onset)
		{
			$this->db->select('send_message_to_user.*, all_user_registration.name');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'send_message_to_user.send_from_user_id = all_user_registration.id', 'left');
			$this->db->where(array('send_to_user_id' => $sendToUser));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(10, $onset); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findByMessageId($table, $msgViewId)
		{
			$this->db->select('send_message_to_user.*, all_user_registration.name,all_user_registration.image');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'send_message_to_user.send_from_user_id = all_user_registration.id', 'left');
			$this->db->where(array('send_message_to_user.id' => $msgViewId));
			$query = $this->db->get();
			return $query->row();
		}



		public function findByReply($table, $msgViewId)
		{
			$this->db->select('message_reply.*, all_user_registration.name,all_user_registration.image');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'message_reply.rep_user_id = all_user_registration.id', 'left');
			$this->db->where(array('message_reply.message_id' => $msgViewId));
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllReplyByMsg($where = array())
		{
			$this->db->select('message_reply.*, all_user_registration.name,all_user_registration.image');
			$this->db->from('message_reply');
			$this->db->join('all_user_registration', 'message_reply.rep_user_id = all_user_registration.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllByCountId($table, $countId)
		{
			$this->db->select('send_message_to_user.*, all_user_registration.name,all_user_registration.image');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'send_message_to_user.send_from_user_id = all_user_registration.id', 'left');
			$this->db->where(array('send_message_to_user.count_id' => $countId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findByVisitorCountId($table, $countId)
		{
			$List 		= $this->findAllByCountId($table, $countId);
			
			foreach($List as $model)
			{
				$model->reply 		= $this->findAllReplyByMsg(array("message_reply.message_id" => $model->id));
				
			}
			return $List;
		}



		
		public function countAll($table, $sendToUser)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('send_to_user_id' => $sendToUser));			
			return $this->db->count_all_results();
		}
		
		
		public function findAllNewsByOrganize($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(5,0);
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllPhotoByOrganize($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(4,0);
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllSlideDataByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllPhotoDataByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAboutContactByUser($table, $userId, $pageType)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => $pageType));
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function update($table, $id, $type, $data)
		{
			$this->db->update($table, $data, array('organize_id' => $id, 'page_type' => $type));        
		}
		
		public function update2($table, $id, $data)
		{
			$this->db->update($table, $data, array('id' => $id));        
		}
		
		
		
		public function destroy($table, $id)
		{
			$this->db->delete($table, array('id' => $id));        
		}
			
	}
