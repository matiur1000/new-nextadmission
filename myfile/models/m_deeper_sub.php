<?php

	class M_deeper_sub extends CI_Model {
	
		const TABLE	= 'deeper_menu_manage';
	
		public function __construct()
		{
				parent::__construct();
		}		
				
	    public function save($data)
		{
		  $this->db->insert(self::TABLE, $data);    
		}
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findAll($menuId,$subId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by('id', 'desc');
			$this->db->where('menu_id','sub_menu_id', $menuId ,$subId);
			$query = $this->db->get();
			return $query->result();
		}
	
		public function findAllDep($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findMaxSl()
		{
			$this->db->select('max(sl_no) as sl_no');
			$this->db->from(self::TABLE);
			$query = $this->db->get();
			return $query->row()->sl_no+1;
		}
		
		
		public function findAllCat($where = array(), $onset = 0)
		{
			$this->db->select('deeper_menu_manage.*, menu_manage.menu_name, sub_menu_manage.sub_menu_name');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('menu_manage', 'deeper_menu_manage.menu_id = menu_manage.id', 'left');
			$this->db->join('sub_menu_manage', 'deeper_menu_manage.sub_menu_id = sub_menu_manage.id', 'left');
		    $this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(5, $onset); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function countAll($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);			
			return $this->db->count_all_results();
		}	
		
		
		public function findByDetails($where)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		
		public function update($data, $id)
			{
				$this->db->update(self::TABLE, $data, array('id' => $id));        
			}
	
		public function destroy($id)
		{
		  $this->db->delete(self::TABLE, array('id' => $id));    
		}	
			
			
	}

