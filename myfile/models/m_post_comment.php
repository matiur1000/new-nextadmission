<?php

	class M_post_comment extends CI_Model {
	
		const TABLE	= 'post_comment';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		
		public function findAll($postId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('post_id', $postId);
			$this->db->order_by("id", "desc");
			$this->db->limit(2, 0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllInfo($where = array())
		{
			$this->db->select('post_comment.*, all_user_registration.name,all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'post_comment.user_id = all_user_registration.id', 'left');
			$this->db->where($where);
			$this->db->limit(2, 0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findByPost($postId)
		{
		
			$this->db->select('post_comment.*, all_user_registration.name,all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'post_comment.user_id = all_user_registration.id', 'left');
			$this->db->where('post_id', $postId);
			$this->db->order_by("id", "asc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		
		public function findAllDetailes($postId)
		{
			$List 		= $this->findByPost($postId);
			
			foreach($List as $model)
			{
				$reply 		  = $this->M_reply_comment->findAll(array("post_id" => $model->post_id,"comment_id" => $model->id));
				$model->reply = $reply;
			
			}
			return $List;
		}
		
		
			
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>