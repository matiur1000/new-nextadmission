<?php

	class M_country_manage extends CI_Model {
	
		const TABLE	= 'country_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		public function findAllNationality($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findAll($where = array(), $onset = 0)
		{
			$this->db->select('country_manage.*, region_manage.region_name');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('region_manage', 'country_manage.region_id = region_manage.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(10, $onset); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllCountry()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by('lower(country_name)', 'asc'); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function countAll($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);			
			return $this->db->count_all_results();
		}	
		
		
		public function findAllInfo($region_id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('region_id',$region_id);
			$this->db->order_by('lower(country_name)', 'asc'); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}

		public function findCountryId($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('country_name', $country_name);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>