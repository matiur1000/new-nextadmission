<style>
  .commentStyle {
	border-left:1px;
	border-right:1px;
	border-top:1px;
    border-style:solid;
	border-color:#CCCCCC;
	border-width:100%;
	padding:5px;
	
}

.commentStyle:last-child {
    border-bottom:1px solid #CCCCCC;	
} 

.commentStyle h5{
	font-size: 14px;
	font-weight: normal;
	margin-top: 5px;
	margin-bottom: 0px;
}
.commentStyle p{
	font-size: 14px;
	font-weight: normal;
	padding-bottom:10px;
	float:left;
}

.commentStyle img{
	background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    display: inline-block;
    line-height: 1.42857;
    margin-right: 10px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}

</style>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>




<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title" id="myModalLabel"><?php echo $UserPostInfo->title; ?></h3>
</div>
  
<div class="modal-body">
	<img src="<?php echo base_url("Images/Post_image/$UserPostInfo->image"); ?>" class="pull-left" style="margin:0 15px 0 0" width="200" height="200" />
    <p style="text-align:justify"><?php echo $UserPostInfo->description ?></p>
	
	<p style="text-align:right; padding-bottom:10px;">Posted By : <span style="font-size:14px; font-weight:bold"><?php echo $UserPostInfo->name; ?></span></p>

	<div class="row"  style="padding-bottom:20px">
	    <div class="col-lg-6"></div>
		<div class="col-lg-6 text-right"><div class="fb-share-button" data-href="<?php echo site_url('commentPopup/index/'.$post_id); ?>" data-layout="button"></div></div>
	</div>
	
	
	     <div class="row">
		    <div id="comntView" class="col-lg-12">
			    <?php 
				 	
			        if(!empty($UserCmntInfo)){
					 //print_r($UserCmntInfo);
					 foreach ($UserCmntInfo as $v){
					   $comment_id    = $v->id;
					   $post_id    = $v->post_id;
					   $multiId		 = array('commentPopup','replyStore', $post_id,$comment_id);
					   
					  
					   
				  ?>
			     <div class="col-md-12 commentStyle">
			         <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
					<h5><a><?php echo $v->name; ?></a></h5>
					<h6><?php echo $v->comment_date; ?></h6>	
					<p style="float:left"><?php echo $v->comment; ?></p> 
			     </div>
			  
			  <?php } } ?>
			   
			  
           </div>
		   
		    
            <div class="col-md-12" style="margin-top:20px;">
			<?php if( isActiveUser() ) { ?>
			   <form id="comment" action="<?php echo site_url('commentPopup/commentStore/'.$post_id); ?>" method="post" enctype="multipart/form-data">
		           <div class="form-group">
				    <label for="exampleInputEmail1">Write Your Comment</label>
					<textarea name="comment" id="comment" class="form-control" rows="3"></textarea>
				  </div>
				  <div class="form-group" style="float:left">
					<button class="btn btn-success" type="submit">Post Comment</button>
				  </div>
			   	</form>
			   <?php } else { ?>
                <h4 style="color:#FF0000">Please Login To Comment.</h4>
				
            <?php } ?>
			  
           </div>
		 </div>
		
</div>
		

<div class="modal-footer">
	<button class="btn btn-danger" data-dismiss="modal">Close</button>
</div>



<script>
  $(".comnt").click(function(){
   $(this).parents('#blockCmnt').find('#reply').css("display", "block");
   
  });
  
  
  $("#comment").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#comntView").html(data);				
					$("#comment input[type='text'], textarea").val("");
				}
			});
			
			e.preventDefault();
		});




   $("#replyForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#replayView").html(data);				
					$("#replyForm input[type='text']").val("");
				}
			});
			
			e.preventDefault();
		});
</script>