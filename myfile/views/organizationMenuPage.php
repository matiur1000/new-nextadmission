<script src="<?php echo base_url('resource/source/script.js'); ?>"></script>
<link href="<?php echo base_url('resource/source/styles.css'); ?>" rel="stylesheet">
<div id='cssmenu'>
	<ul>
		<li><a href="<?php echo site_url('home/adWiseCompanyDetail/'.$organizeId); ?>">Organization Home</a></li>
		<li><a href="<?php echo site_url('home/organizeAbout/'.$organizeId);?>">About Us</a></li>
		<li><a href="#">Course</a>
		 <ul>
		      <?php
			       foreach($orgProgramInfo as $v){
				    $courseLink  = array('home','organizeCourseDetails',$organizeId, $v->id);
			 ?>
		      <li><a href="<?php echo site_url($courseLink);?>"><?php echo $v->programe_name; ?></a></li>
			  <?php } ?>
		 </ul>
		</li>
		<li><a href="<?php echo site_url('home/organizeAdvertise/'.$organizeId);?>">Advertisement</a></li>
		<li><a href="<?php echo site_url('home/organizeContact/'.$organizeId);?>">Contact Us</a></li>
		<li><a href="<?php echo site_url('home/organizePhotogallery/'.$organizeId);?>">Gallery</a></li>
	</ul>
</div>