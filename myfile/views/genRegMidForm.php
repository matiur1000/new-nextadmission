<div class="col-lg-9">
	<div class="welllreg "> 
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12">
	     <span style="color:#000000; font-size:30px;">Create Account</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Create your Free Global Study account now </span>
	  </div>
	</div>
	<div class="row">
	   <div class="col-lg-3" style="font-size:16px;"><input type="radio" name="regType" value="organization" checked="checked" checkbox="checked" /> Organization</div>
	   <div class="col-lg-3" style="padding-left:0px; font-size:16px;"><input type="radio"  name="regType" value="other" />  Others</div>
	   <div class="col-lg-6"></div>
	 </div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div> 
		<div class="row">
		  
		     <div id="organize">
			   <form id="regForm" action="<?php echo site_url('registration/orgRegStore'); ?>" method="post" enctype="multipart/form-data">
			<div class="col-lg-12">
			<div class="col-lg-6">
			  <div class="form-group">
					<input type="text" class="form-control" id="account_name" name="account_name" 
					placeholder="Account Name" tabindex="1" value="<?php echo set_value('account_name'); ?>">
					<?php echo form_error('account_name'); ?>
				</div>
				
				<div class="form-group">
					<input type="email" class="form-control" id="user_id2" name="user_id" placeholder="Email / Login id" tabindex="2" 
					value="<?php echo set_value('user_id'); ?>"><?php echo form_error('user_id'); ?><span class="chkEmail"></span>
				</div>
				
				
				
			</div>
			<div class="col-lg-6">
			  <div class="form-group">
					<input type="text" class="form-control" id="organizationname" name="organizationname" placeholder="Organization Name" tabindex="1" >
				</div>
			
			<div class="row">
			    <div class="col-lg-6" style="padding-right:3px;">
				   <div class="form-group">
					<input type="password" class="form-control password" id="password" name="password" placeholder="Password" tabindex="2" value="<?php echo set_value('password'); ?>"><?php echo form_error('password'); ?>
					<span class="first"></span>
				    </div>
				  </div>
				<div class="col-lg-6" style="padding-left:3px;">
				  <div class="form-group">
					<input type="password" class="form-control conformpassword" id="con_password" name="con_password" placeholder="Confirm Password" tabindex="2">
					<span class="second"></span>
				  </div> 
				</div>
			</div>
				
			  
			  
			  
			</div>
				
				<div class="row">
				  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0;">
					<span style="color:#333333; font-size:20px;">Organization Details</span>
				  </div>
				</div>
				<div class="row">&nbsp;</div>
			
			<div class="col-lg-6">
			
                <div class="form-group">
					<div>
						<select class="form-control" id="country_id_org" name="country_id_org"  tabindex="2" >
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
                         
					   </select>
					 
					</div>
				</div>
                
                <div class="form-group">
					<div>
					   <select class="form-control" id="city_id_org" name="city_id"  tabindex="3">
							<option value="" selected>Sellect City Name</option>
					   </select>
					</div>
				</div>
                <div class="form-group">
					   <textarea class="form-control" name="address" id="address" cols="60" rows="" placeholder="Holding Details"></textarea>
				</div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Phone &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="phone_com2" name="phone_com2" placeholder="Phone" tabindex="2">
                      </div>
                	</div>
                </div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Mobile &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="mobile_com2" name="mobile_com2" placeholder="Mobile" tabindex="2">
                      </div>
                	</div>
                </div>
                
			   	<div class="form-group">
					<input type="text" class="form-control" id="email_com" name="email_com" placeholder="Email" tabindex="1"> 
				</div>
                <div class="form-group">
					<input type="text" class="form-control" id="location" name="location" placeholder="Location" tabindex="1">
				</div>
                
				<div class="form-group">
						<input name="founded" type="text" id="founded" class="form-control"
						 onClick="('#founded').datepicker({dateFormat: 'dd/mm/yy'});" placeholder="Founded Year"/>
				</div>
				
				
				<div class="row">&nbsp;</div>
                
            </div>
            
            <div class="col-lg-6">
               <div class="row">
                  <div class="col-lg-5" style="margin-top:5px; font-size:15px;">Any More Branches?</div>
                  <div class="col-lg-7" style="padding-left:0px;">
                      <div class="form-group">
                           <select class="form-control" id="any_more_campus" name="any_more_campus"  tabindex="2" onchange="nextSteap();">
                                <option value="" selected>Sellect Yes/No</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                           </select>
					      </div>
                	</div>
                </div>
                
               <div id="more" style="display:none;">  <!--Hide Start-->
					<div class="form-group">
					<input type="text" class="form-control" id="campus_name" name="campus_name" placeholder="Campus One" tabindex="1"> 
					
				</div>
                
                <div class="form-group">
					<div>
						<select class="form-control" id="country_id2" name="country_id2"  tabindex="2" >
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
					   </select>
					 
					</div>
				</div>
                
                <div class="form-group">
					<div>
					   <select class="form-control" id="city_id2" name="city_id2"  tabindex="3">
							<option value="" selected>Sellect City Name</option>
					   </select>
					</div>
				</div>
                <div class="form-group">
					   <textarea class="form-control" name="address2" id="address2" cols="60" rows="" placeholder="Holding Details"></textarea>
				</div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Phone &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="phone_com1" name="phone_com1" placeholder="Phone" tabindex="2"> 
                    </div>
                	</div>
                </div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Mobile &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="mobile_com1" name="mobile_com1" placeholder="Mobile" tabindex="2">
                      </div>
                	</div>
                </div>
                
			   	<div class="form-group">
					<input type="text" class="form-control" id="email_com2" name="email_com2" placeholder="Email" tabindex="1">
					
				</div>
                <div class="form-group">
					<input type="text" class="form-control" id="location_by_google" name="location_by_google" placeholder="Location" tabindex="1"> 
				</div>
				<div class="form-group">

						<input name="founded2" type="text" id="founded2" class="form-control"
						 onClick="('#founded2').datepicker({dateFormat: 'dd/mm/yy'});" placeholder="Founded Year"/>
				</div>
				
				 </div>  <!--Hide Stop-->
                
            </div>
			
			
		    </div> 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12 regsub" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-success btn-lg center-block" tabindex="1">Create Account</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>
			 </div>
			 
			 
		     <div id="other" style="display:none">
		       <form id="genReg" action="<?php echo site_url('registration/genRegStore1'); ?>" method="post" enctype="multipart/form-data">
			 <div class="col-lg-6">                    <!--First col Start-->
			
			 <div class="form-group">
					<div>
						<select class="form-control" id="country_id" name="country_id"  tabindex="2" >
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
					   </select>
					 
					</div>
				</div>
			  
			  <div class="form-group">
					<input type="text" class="form-control" id="name" placeholder="Full Name" name="name" tabindex="5" 
					value="<?php echo set_value('name'); ?>"> <?php echo form_error('name'); ?>
				</div>
			 
			    <div class="form-group">
					<input type="text" class="form-control" id="father_name" name="father_name" placeholder="Father Name" tabindex="7" 
					value="<?php echo set_value('father_name'); ?>"><?php echo form_error('father_name'); ?>
				</div>
				<div class="form-group">
						<select class="form-control" id="gender" name="gender"  tabindex="25" value="<?php echo set_value('gender'); ?>">
								<option value="" selected>Sellect Gender</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
						   </select>
					  </div>
			  <div class="form-group">
				   <div class="controls">
						<input type="file" id="image" name="image" tabindex="8" />
					</div>
			  </div>
			  <div class="form-group">
					<input type="text" class="form-control" id="city" name="city" placeholder="Present City" tabindex="12" 
					value="<?php echo set_value('city'); ?>"><?php echo form_error('city'); ?>
				</div>
			  <div class="form-group">
					<input type="text" class="form-control" id="country_phone" name="country_phone" placeholder="Present Country Phone"
					 tabindex="13" value="<?php echo set_value('country_phone'); ?>" ><?php echo form_error('country_phone'); ?>
				  </div>
			  <div class="form-group">
					<input type="email" class="form-control" id="email2" name="email2" placeholder="Present  Email" tabindex="15" 
					value="<?php echo set_value('email'); ?>" > <?php echo form_error('email'); ?>
				  </div>
			  <div class="form-group" style="margin-top:50px;">
						<label for="same"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Adress Same?</span></label>
						<input type="checkbox" id="same" name="same" placeholder="same" value="yes">
				  </div>
			  <div class="form-group">
						<input type="text" class="form-control" id="country_phone_permanent" name="country_phone_permanent" 
						placeholder="Permanent Country Phone" tabindex="19" value="<?php echo set_value('country_phone_permanent'); ?>" > <?php echo form_error('country_phone_permanent'); ?>
				 </div>
			  <div class="form-group">
				  <input type="email" class="form-control" id="email2_permanent" name="email2_permanent" placeholder="Permanent Email" tabindex="21"
				  value="<?php echo set_value('email_permanent'); ?>" ><?php echo form_error('email_permanent'); ?>
				</div>
			  <div class="form-group">
					<input type="text" class="form-control" id="user_id" name="user_id" placeholder="User Id/Email" tabindex="4" 
					value="<?php echo set_value('user_id'); ?>"><?php echo form_error('user_id'); ?><span class="chk"></span>
			   </div>
			  
				  
				  <div class="form-group">
				  <input type="password" class="form-control password3" id="password" name="password" placeholder="Password" tabindex="23" 
				  value="<?php echo set_value('password'); ?>" ><?php echo form_error('password'); ?>
				  <p class="third"></p>
			    </div>
			   
			</div>                                    <!--First col End-->
			
			
			<div class="col-lg-6">                     <!--2nd col Start-->
			   <div class="form-group">
					<div>
					   <select class="form-control" id="city_id" name="city_id"  tabindex="3" value="<?php echo set_value('city_id'); ?>">
							<option value="" selected>Sellect City Name</option>
					   </select>
					   <?php echo form_error('city_id'); ?>
						
					</div>
				</div>
			    
				<div class="form-group">
					<input name="date_of_birth" type="text" id="date_of_birth" placeholder="Date Of Birth" class="form-control date-picker" data-date-format="yyyy-mm-dd"  />
				</div>
			   <div class="form-group">
					<input type="text" class="form-control" id="mother_name" name="mother_name" placeholder="Mother Name"
					 tabindex="6" value="<?php echo set_value('mother_name'); ?>"><?php echo form_error('mother_name'); ?>
				</div>
			   <div class="form-group">
				<input type="text" class="form-control" id="religion" placeholder="Religion" name="religion" tabindex="5" 
				value="<?php echo set_value('religion'); ?>"> <?php echo form_error('religion'); ?>
			  </div>
			  <div class="form-group">
						<select class="form-control" id="marital_status" name="marital_status"  tabindex="25" value="<?php echo set_value('marital_status'); ?>">
								<option value="" selected>Sellect Marital Status</option>
								<option value="Married">Married</option>
								<option value="Unmarried">Unmarried</option>
						   </select>
					  </div>
			   <div class="form-group">
				<input type="text" class="form-control" id="nationality" name="nationality" 
				 placeholder="Nationality" tabindex="9"><?php echo form_error('nationality'); ?>
			</div>
			   <div class="form-group">
				   <textarea class="form-control" name="address" id="addressGen" cols="60" rows="" placeholder="Present Address" 
				   value="<?php echo set_value('address'); ?>"></textarea><?php echo form_error('address'); ?>
				</div>
			   <div class="form-group">
					<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Present Mobile" tabindex="12" 
					value="<?php echo set_value('mobile'); ?>"><?php echo form_error('mobile'); ?>
				  </div>
			   <div class="form-group">
					<textarea class="form-control" name="address_permanent" id="addressGen_permanent" cols="60" rows="" placeholder="Permanent Address" tabindex="17"
					 value="<?php echo set_value('address_permanent'); ?>"></textarea> <?php echo form_error('address_permanent'); ?>
				  </div>
			   <div class="form-group">
						<input type="text" class="form-control" id="city_permanent" name="city_permanent" placeholder="Permanent City" tabindex="16" 
						value="<?php echo set_value('city_permanent'); ?>"><?php echo form_error('city_permanent'); ?>
				  </div>
			   <div class="form-group">
					<input type="text" class="form-control" id="mobile_permanent" name="mobile_permanent" placeholder="Permanent Mobile" tabindex="18" 
					value="<?php echo set_value('mobile_permanent'); ?>"><?php echo form_error('mobile_permanent'); ?>
				</div>
			   <div class="form-group">
					<input type="password" class="form-control conformpassword3" id="conform_password" name="conform_password" placeholder="Conform Password" 
					tabindex="20" value="<?php echo set_value('conform_password'); ?>"><?php echo form_error('conform_password'); ?>
					<p class="fourth"></p>
				  </div>
			   
			  
			</div>                                     <!--2nd col End-->                                     
		 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			     <button type="submit" id="submit" name="submit" class="btn btn-success btn-lg center-block" tabindex="1">Create Account</button>
			  </div>
			</div>
			</form>
			 </div>
			 
			 
			 
			
			   
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			     
		</div>
	</div>                
</div>