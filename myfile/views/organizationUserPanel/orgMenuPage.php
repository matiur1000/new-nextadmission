<script src="<?php echo base_url('resource/source/script.js'); ?>"></script>
<link href="<?php echo base_url('resource/source/styles.css'); ?>" rel="stylesheet">
<div id='cssmenu'>
	<ul>
		<li><a href="<?php echo site_url('organizationUserHome/index');?>">Organization Home</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/addPost');?>">Blog</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/programeManage');?>">Program</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/addManageFinal');?>">Promotion</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/totalVisitor');?>">Total Visitor</a></li>
		<li><a href="#">Oorganization Profile</a>
		   <ul>
		      <li><a href="<?php echo site_url('organizationUserHome/organizeAbout');?>">About Us</a></li>
			  <li><a href="<?php echo site_url('organizationUserHome/organizeContact');?>">Contact Us</a></li>
			  <li><a href="<?php echo site_url('organizationUserHome/organizeSlide');?>">Latest News</a></li>
			  <li><a href="<?php echo site_url('organizationUserHome/organizeGallery');?>">Photo Gallery</a></li>
			 </ul>
		</li>
		<li><a target="_blank" href="<?php echo site_url('home/adWiseCompanyDetail/'.$userAutoId);?>">Organization Website</a></li>
		<li><a href="<?php echo site_url('loginForm/organizeLogout'); ?>">Logout</a></li>
	</ul>
</div>