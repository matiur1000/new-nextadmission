<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
    <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				  body{
				    background-color:#EEEEEE;
				
				}
				
				</style>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
         <?php //$this->load->view('jsLinkPage'); ?>  
		 <script src="<?php echo base_url("resource/assets/js/jquery.2.1.1.min.js"); ?>"></script>
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
 

    
    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->
  </head>
  <body>
        <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">&nbsp;</div> 
							  <div class="row">
						
									
							 <?php $this->load->view('organizationUserPanel/middleProfilePage'); ?>
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
	   <?php $this->load->view('footerTopPage'); ?>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script>
	
	$(document).ready(function(){
		$("#type_of_organization").trigger("change");
		$("#type_of_education_provider").trigger("change");
		$("#agency_type").trigger("change");
		$("#any_more_campus").trigger("change");
		
	});
	
	$("#region_id").change(function() {
		var region_id = $("#region_id").val();			
		$.ajax({
			url : SAWEB.getSiteAction('OrganizationUserHome/countryEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : region_id },
			dataType : "html",
			success : function(data) {			
				$("#country_id").html(data);
			}
		});
		
	});	
	$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('OrganizationUserHome/cityEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});	
   $("#country_id").change(function() {
		var nationality = $("#country_id option:selected").attr('data-nationality');
		var countryCode = $("#country_id option:selected").attr('data-country-code');
					
		
				$("#nationality").val(nationality);
				$("#mobile").val(countryCode);
			
		});
		
		
		// Region Wise Country
		$("#region_id2").change(function() {
			var region_id2 = $("#region_id2").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id2 },
				dataType : "html",
				success : function(data) {			
					$("#country_id2").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id2").change(function() {
		var country_id2 = $("#country_id2").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id2 },
			dataType : "html",
			success : function(data) {			
				$("#city_id2").html(data);
			}
		});
		
	});
	
	
	// Country Wise Nationality
	
	$("#country_id2").change(function() {
		var nationality2 	= $("#country_id2 option:selected").attr('data-nationality');
		var countryCodeMob2 = $("#country_id2 option:selected").attr('data-country-code');
		var countryCodePhon2 = $("#country_id2 option:selected").attr('data-country-code-phon');	
		
		 
		 $("#mobile_com1").val(countryCodeMob2);
		 $("#phone_com1").val(countryCodePhon2);
	
	});
	
		
		// Any More Campus
		
		$("#any_more_campus").change(function() {
			var anyCampus = $("#any_more_campus").val();
			 console.log(anyCampus);
			 if(anyCampus == 'Yes'){	
			 $("#more").css({"display":"block"}); 		
			} else {
			$("#more").css({"display":"none"}); 
			}
		});
		
		$("#type_of_organization").change(function(){
			var type = $("#type_of_organization option:selected").val();
			
			 if(type == 'Other') {
				$("#type_of_education_provider").css("display", "none");
				$("#school_college").css("display", "none");
				$("#agency_type").css("display", "none");
				$("#couching_centre").css("display", "none");
				$("#professional_taining").css("display", "none");
				$("#agency_type_other").css("display", "none");
				
				$("#type_of_organization_other").css("display", "block");
			} else if(type == 'EducationProvider') {
				$("#agency_type").css("display", "none");
				$("#couching_centre").css("display", "none");
				$("#professional_taining").css("display", "none");
				$("#type_of_organization_other").css("display", "none");
				$("#agency_type_other").css("display", "none");
				
				$("#type_of_education_provider").css("display", "block");
				$("#school_college").css("display", "block");
			
			
			} else if(type == 'Agency') {
				$("#type_of_education_provider").css("display", "none");
				$("#school_college").css("display", "none");
				$("#type_of_organization_other").css("display", "none");
				$("#couching_centre").css("display", "none");
				$("#professional_taining").css("display", "none");
				
				$("#agency_type").css("display", "block");
				
			} else if(type == 'CouchingCentre') {
			$("#type_of_education_provider").css("display", "none");
			$("#school_college").css("display", "none");
			$("#type_of_organization_other").css("display", "none");
			$("#agency_type").css("display", "none");
			$("#professional_taining").css("display", "none");
			$("#agency_type_other").css("display", "none");
			
			$("#couching_centre").css("display", "block");
			
			} else {
			$("#type_of_education_provider").css("display", "none");
			$("#school_college").css("display", "none");
			$("#type_of_organization_other").css("display", "none");
			$("#agency_type").css("display", "none");
			$("#couching_centre").css("display", "none");
			$("#agency_type_other").css("display", "none");
			
			 $("#professional_taining").css("display", "block");
			}
		});
		$("#agency_type").change(function(){
			var step = $("#agency_type option:selected").val();
			
			if(step != 'Other') {
				$("#agency_type_other").css("display", "none");
			} else {
				$("#agency_type_other").css("display", "block");
			}
			
		});
		$("#type_of_education_provider").change(function(){
			var step = $("#type_of_education_provider option:selected").val();
			
			if(step != 'School /College upto  12') {
				$("#school_college").css("display", "none");
			} else {
				$("#school_college").css("display", "block");
			}
			
		});
		
		
</script>

<script>
	resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
 </script>
    
  </body>
</html>