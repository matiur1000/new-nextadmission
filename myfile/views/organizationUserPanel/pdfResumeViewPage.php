<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
    <LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
    <link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->

</head>
<body>
    <div class="row"><!--row Start-->
        <div class="col-lg-12">
            <div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;">
               <br/>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="row" style="padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30%"></td>
    <td width="52%">&nbsp;</td>
    <td width="15%" rowspan="5" align="center" valign="middle"><img src="<?php echo base_url("/Images/Register_image/$resumeInfo->image"); ?>" height="120" width="120"></td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr>
    <td><?php echo $resumeInfo->name; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="color:#000 !important; font-size:14px">Adress : <?php echo $moreResumeInfo->address; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Phone   : <?php echo $moreResumeInfo->country_phone; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Mobile   : <?php echo $moreResumeInfo->mobile; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Email     : <?php echo $moreResumeInfo->email; ?></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
                        </table>
</div>
                        <?php
                             if(!empty($moreResumeInfo->degree_title)){
                          ?>
						   <div class="row" style="padding:10px;">
	                          <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;">
							   <span style="font-size:15px; font-weight:bold; padding-left:50px;">Education</span>
							  </div>
							</div>
							<br/><br/><br/>
							<table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin-top:20px;">
							  <tr>
								<td align="left" valign="middle">Exam Title</td>
								<td align="left" valign="middle">Concentration</td>
								<td align="left" valign="middle">Institute</td>
								<td align="left" valign="middle">Result</td>
								<td align="left" valign="middle">Pas.Year</td>
								<td align="left" valign="middle">Duration</td>
								<td align="left" valign="middle">Achievement</td>
							  </tr>
							  <tr>
								<td><?php echo $moreResumeInfo->degree_title; ?></td>
								<td><?php echo $moreResumeInfo->group; ?></td>
								<td><?php echo $moreResumeInfo->institute_name; ?></td>
								<td><?php echo $moreResumeInfo->result; ?></td>
								<td><?php echo $moreResumeInfo->pass_year; ?></td>
								<td><?php echo $moreResumeInfo->duration; ?></td>
								<td><?php echo $moreResumeInfo->achievement; ?></td>
							  </tr>
					  </table>


                            
                        <?php } ?>

                        <?php
                        if(!empty($moreResumeInfo->training_title)){
                            ?>
							 <div class="row" style="padding:10px;">
	                          <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;">
							   <span style="font-size:15px; font-weight:bold; padding-left:50px;">Training</span>
							  </div>
							</div>
                            <br/><br/><br/>
							 <table width="100%" cellspacing="0" cellpadding="0" border="1">
							  <tr>
								<td align="left" valign="middle">Training Title</td>
								<td align="left" valign="middle">Topic Covered</td>
								<td align="left" valign="middle">Institute</td>
								<td align="left" valign="middle">Country</td>
								<td align="left" valign="middle">Location</td>
								<td align="left" valign="middle">Training Year</td>
								<td align="left" valign="middle">Duration</td>
							  </tr>
							  <tr>
								<td><?php echo $moreResumeInfo->training_title; ?></td>
								<td><?php echo $moreResumeInfo->training_tropic; ?></td>
								<td><?php echo $moreResumeInfo->training_institute; ?></td>
								<td><?php echo $moreResumeInfo->training_country; ?></td>
								<td><?php echo $moreResumeInfo->training_location; ?></td>
								<td><?php echo $moreResumeInfo->training_year; ?></td>
								<td><?php echo $moreResumeInfo->training_duration; ?></td>
							  </tr>
					  </table>
                            
                        <?php } ?>
                        <?php
                        if(!empty($moreResumeInfo->objective)){
                            ?>

                            <div class="row" style="padding:10px;">
                                <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;"><span style="font-size:15px; font-weight:bold; padding-left:50px;">Career and Application Information</span></div><br/>
                                <div class="col-lg-6" align="left"><table width="100%" cellspacing="0" cellpadding="0" class="table-responsive">
                                        <tr>
                                            <td width="10%" height="30">&nbsp;</td>
                                            <td width="39%" align="left" valign="middle">Objective </td>
                                            <td width="5%" align="center" valign="middle">:</td>
                                            <td width="46%" align="left" valign="middle"><?php echo $moreResumeInfo->objective; ?></td>
                                        </tr>
                                        <tr>
                                            <td height="28">&nbsp;</td>
                                            <td align="left" valign="middle">Present Salary </td>
                                            <td align="center" valign="middle">:</td>
                                            <td align="left" valign="middle"><?php echo $moreResumeInfo->present_sallary; ?></td>
                                        </tr>
                                        <tr>
                                            <td height="30">&nbsp;</td>
                                            <td align="left" valign="middle">Expected Salary </td>
                                            <td align="center" valign="middle">:</td>
                                            <td align="left" valign="middle"><?php echo $moreResumeInfo->expected_sallary; ?></td>
                                        </tr>
                                        <tr>
                                            <td height="28">&nbsp;</td>
                                            <td align="left" valign="middle">Looking for  </td>
                                            <td align="center" valign="middle">:</td>
                                            <td align="left" valign="middle"><?php echo $moreResumeInfo->level; ?></td>
                                        </tr>
                                        <tr>
                                            <td height="30">&nbsp;</td>
                                            <td align="left" valign="middle">Available for </td>
                                            <td align="center" valign="middle">:</td>
                                            <td align="left" valign="middle"><?php echo $moreResumeInfo->available; ?></td>
                                        </tr>

                                        <tr>
                                            <td height="26">&nbsp;</td>
                                            <td align="left" valign="middle">&nbsp;</td>
                                            <td align="center" valign="middle">&nbsp;</td>
                                            <td align="left" valign="middle">&nbsp;</td>
                                        </tr>
                                    </table></div>
                                <div class="col-lg-6" align="right"></div>
                            </div>
                        <?php } ?>

                        <div class="row" style="padding:10px;">
                            <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;"><span style="font-size:15px; font-weight:bold; padding-left:50px;">Personal Details</span></div><br/>
                            <div class="col-lg-6" align="left"><table width="100%" cellspacing="0" cellpadding="0" class="table-responsive">
                                    <tr>
                                        <td width="10%" height="30">&nbsp;</td>
                                        <td width="39%" align="left" valign="middle">Father's Name </td>
                                        <td width="5%" align="center" valign="middle">:</td>
                                        <td width="46%" align="left" valign="middle"><?php echo $moreResumeInfo->father_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="28">&nbsp;</td>
                                        <td align="left" valign="middle">Mother's Name </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->mother_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="30">&nbsp;</td>
                                        <td align="left" valign="middle">Date Of Birth </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->date_of_birth; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="28">&nbsp;</td>
                                        <td align="left" valign="middle">Gender </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->gender; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="30">&nbsp;</td>
                                        <td align="left" valign="middle">Nationality </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->nationality; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="27">&nbsp;</td>
                                        <td align="left" valign="middle">Marital Status </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->marital_status; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="29">&nbsp;</td>
                                        <td align="left" valign="middle">Religion  </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->religion; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="26">&nbsp;</td>
                                        <td align="left" valign="middle">Permanent Adress </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->address_permanent; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="26">&nbsp;</td>
                                        <td align="left" valign="middle">Current Location </td>
                                        <td align="center" valign="middle">:</td>
                                        <td align="left" valign="middle"><?php echo $moreResumeInfo->address; ?></td>
                                    </tr>
                                    <tr>
                                        <td height="26">&nbsp;</td>
                                        <td align="left" valign="middle">&nbsp;</td>
                                        <td align="center" valign="middle">&nbsp;</td>
                                        <td align="left" valign="middle">&nbsp;</td>
                                    </tr>
                                </table></div>
                            <div class="col-lg-6" align="right"></div>
                        </div>



                    </div>
                </div>
                <div class="row">&nbsp;</div>

            </div>
        </div>

    </div>


</body>
</html>