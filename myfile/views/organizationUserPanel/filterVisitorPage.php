<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
	<style type="text/css">
		#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
		#menu1 a:active {color:black;text-decoration:none;}
		#menu1 a:hover {color:black;background-color:#FFFF99}
		#menu1 a:visited {color:black;text-decoration:none;}
		body{
		background-color:#EEEEEE;
		
		}


		.row-fluid.bs {
		    background-color: #fff;
		    border-radius: 4px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
		    margin: 0 0 15px;
		    padding: 15px 0;
		    transition: all 0.5s ease 0s;
		}
		
		.vertical-wrapper {
		    display: table;
		    vertical-align: middle;
		    width: 100%;
		}
		.vertical-content {
		    display: table-cell;
		    float: none;
		    margin: 0;
		    position: relative;
		    text-align: left;
		    vertical-align: middle;
		}
			
	</style>


        <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
		<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
				  
   
  </head>
  <body>
  
      <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       
       <div class="container"> 
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  

						  <!--CALENDER-->
    <div class="col-lg-12" align="right">
	   <div class="welll welll-lg">
          	<div class="row" style="padding-bottom:25px">
			  <div class="col-lg-7" align="center"></div>

			    <div class="col-lg-5" align="center">
				  <div class="comntorg2" style="color:#FFF; padding-top:5px;">Search CV Bank</div>
				  <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;"><a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a></div>
			    </div>
		    </div> 
			
		   <div class="row">
		     <div class="col-lg-12" style="padding:0 0 10px 0; font-size:15px">
		        <div class="col-lg-2 text-left"><strong>Today :</strong> <?php echo $totalVisitToday; ?></div>
		        <div class="col-lg-2 text-left" style="padding:0px;"><strong>Current Week :</strong><?php echo $totalCurWeekVisit ?></div>
		        <div class="col-lg-2 text-left" style="padding:0px;"><strong>Current Month :</strong><?php echo $totalCurMonthVisit ?></div>
		        <div class="col-lg-2 text-left" style="padding:0px;"><strong>Current Year :</strong><?php echo $totalYearVisit ?></div>
		        <div class="col-lg-4 text-left"></div>
		     </div>
	               <div class="col-lg-12 table-responsive" style="padding:0px;">
	                    <table id="inputTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="background-color:#FFFFFF">
	                      <input type="hidden" name="sendTo" id="sendTo" value="" />
						  <input type="hidden" name="msg" id="msg" value="" />
						  <input type="hidden" name="title2" id="title2" value="" />
	                      <input type="hidden" name="filter" value="yes"> 
	                      
		                  <tr class="">
		                    <td colspan="5">
		                    	<div class="col-md-10" style="padding:0px">

                                  <form class="form-inline" method="post" action="<?php echo site_url('organizationUserHome/visitorSort') ?>">
                                      <div class="form-group">
                                          <label for="select_category">Sellect Category &nbsp;&nbsp;</label>
                                            <select name="select_category" id="select_category" class="form-control">
											   <option value="" selected>Sellect Category</option>
											   <option <?php if($select_category == 'Blog'){ ?> selected="selected" <?php } ?> value="Blog">Blog</option>
											   <option <?php if($select_category == 'Advertisement'){ ?> selected="selected" <?php } ?> value="Advertisement">Advertisement</option>
											   <option <?php if($select_category == 'Programe'){ ?> selected="selected" <?php } ?> value="Programe">Programe</option>
											   <option <?php if($select_category == 'Job'){ ?> selected="selected" <?php } ?> value="Job">Job Post</option>
      										</select>&nbsp;&nbsp;										</div> 


									<div class="form-group">
									   <label  for="from_date">From Date &nbsp;&nbsp;</label>
									    <input type="text" class="form-control date-picker" name="from_date" id="from_date" placeholder="From Date" data-date-format="yyyy-mm-dd" value="<?php echo $from_date ?>">&nbsp;&nbsp;&nbsp;									</div>
										<div class="form-group">
										<label  for="to_date">To Date &nbsp;&nbsp;</label>
										<input type="text" class="form-control date-picker" name="to_date" id="to_date" placeholder="To Date" data-date-format="yyyy-mm-dd" value="<?php echo $to_date ?>">&nbsp;&nbsp;&nbsp;									    </div>
										    <button type="submit" class="btn btn-primary" style="width:80px">Filter</button>
								  </form>
                               </div>

                               <div class="col-md-2">
                                  <div class="btn-group">
									  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    Send Message <span class="caret"></span>									  </button>
									  <ul class="dropdown-menu">
									    <li><a  class="msg" data-value="all" href="#">All</a></li>
									    <li><a  class="msg" data-value="selected"  href="#">Sellected</a></li>
									  </ul>
									</div>
								</div>		                    </td>
						  </tr>
		                  <tr class="active" style="font-weight:bold;">
		                    <td width="6%"></td>
		                    <td width="19%">Date Tmie </td>
		                    <td width="42%">Visitor Info </td>
		                    <td width="15%">Visit Page </td>
		                    <td width="17%">Message Details </td>
	                      </tr>

	                      <?php
                            if(!empty($filterResult)){
                             foreach ($filterResult as $v){

                              if($v->user_type =="general"){
                              	$mobile = $v->mobile;

                              }else{
                                 $mobile = $v->mobile_com;
                              }

                               $msgViewLink  = array('organizationUserHome','allMessageViewDetails', $v->id);

                             
                           ?>
		                  
		                  <tr> 
		                    <td align="center">
		                       <input type="checkbox" name="count_id[]" value="<?php echo $v->id; ?>">		                    </td>
		                    <td><?php echo $v->date ?></td>
		                    <td>
		                       <h4 style="margin-top:0"><?php echo $v->name ?> </h4>
								<p><?php echo $v->user_id ?></p>
								<p><?php echo $mobile ?></p>							 
							</td>

							<?php if(!empty($v->blog_id)){ ?>
		                     <td><a style="color:#000000" class="blog" href="<?php echo site_url($v->visit_link); ?>"><?php echo $v->page_type ?></a></td>
		                     <?php }else{ ?>
		                     <td width="2%"><a style="color:#000000" target="_blank"  href="<?php echo site_url($v->visit_link); ?>"><?php echo $v->page_type ?></a></td>
		                      <?php } ?>
		                      <td><a class="msgDel" style="color:#000000"  href="<?php echo site_url($msgViewLink) ?>">Details</a></td>

	                      </tr> 

	                      <?php } } ?>
		                </table> 
	               </div>
           </div> 
	    </div>                
    </div>




      <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerTopPage'); ?>
	    <?php $this->load->view('footerPage'); ?>

	    <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>


		<div class="modal fade" id="msgForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	     <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Email And SMS Send</h4>
		      </div>
		      <div class="modal-body">
		          <div class="form-group">
		            <label for="title" class="control-label">Title:</label>
		            <input type="text" class="form-control" id="title" name="title" placeholder="Title" />
		          </div>
		          <div class="form-group">
		            <label for="message" class="control-label">Message:</label>
		            <textarea class="form-control" id="message" name="message"></textarea>
		          </div>
	         </div>
		      <div class="modal-footer">
		         <button class="btn btn-sm" data-dismiss="modal">
	                <i class="ace-icon fa fa-times"></i>
	                Cancel
	            </button>
		        <button id="sedAction" class="btn btn-sm btn-primary" type="submit">
	                <i class="ace-icon fa fa-check"></i>
	                Save
	            </button>
		      </div>
		    </div>
	    </div>
	</div>


	<div class="modal fade" id="msgDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">       
          
        </div>
      </div>
    </div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	 <script>
	 
	 
	     $('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		
				resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();




		$('.blog').on('click', function(e){
			var url = $(this).attr('href');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});


		$('.msg').on('click', function(e){
			var valueType   = $(this).attr('data-value');
			$("#msgForm").modal('show');
			var sendTo = $("#inputTable #sendTo").val(valueType);	
			console.log(sendTo);		
			e.preventDefault();
			
		});


		
		$('#sedAction').on('click', function(e) {
			var message 		= $("#message").val();
			var title 			= $("#title").val();
			$("#inputTable #title2").val(title);	
			$("#inputTable #msg").val(message);	
			var postData 		= $("#inputTable input, select").serializeArray();
			console.log(postData);
			$.ajax(
			{
				url: "<?php echo site_url('organizationUserHome/visitorSmsSendAction') ?>",
				method: "POST",	
				data: postData,
				success:function(data){
					$("#msgForm").find("input[type=text], textarea").val("");
					$("#inputTable").find('input[name="count_id[]"]').prop('checked', false);
					$("#msgForm").modal('hide');
				}
			});

			e.preventDefault();
		});


		$('.msgDel').on('click', function(e){
          var url = $(this).attr('href');
          
          $.ajax({
            url : url, // URL TO LOAD BEHIND THE SCREEN
            type : "GET",
            dataType : "html",
            success : function(data) {      
              $("#msgDetails .modal-content").html(data);
              $("#msgDetails").modal({
                keyboard: false,
                backdrop: 'static',
              });
              $("#msgDetails").modal('show');
            }
          });

          e.preventDefault();   
        });


 </script>
  </body>
</html>