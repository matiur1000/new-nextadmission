<!--CALENDER-->
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-1.7.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-1.8.16.custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-timepicker-addon.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-sliderAccess.js'); ?>"></script>
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('resource/css/jquery-ui-1.8.16.custom.css'); ?>" />
	<script type="text/javascript">
		
		$(function(){
			
	
			$('.example-container > pre').each(function(i){
				eval($(this).text());
			});
		});
		
	</script>
    <div class="col-lg-12" align="right">
	<div class="welll welll-lg">
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Online Payment for bKash </span></div>
	</div>  
	            	
                    <div class="row">
                    <div class="col-lg-12" style="padding:20px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                    <td align="center" style="padding:0px;"><img src="<?php echo base_url("Images/Basic_image/card_bkash.png"); ?>" height="50" width="130" /></td>
                    </tr>
                     <tr>
                    <td style="background:#f9f9f9; height:30px; font-size:16px;"><i style="color:#96F" class="glyphicon glyphicon-hand-right"></i> Please follow the steps given below to pay by bKash for getting this service : </td>
                    </tr>
                    <tr>
                    <td> <form id="form1" name="form1" method="post" action="<?php echo site_url('organizationUserHome/addManageTransactionBkaskAction');?>">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-hover">
                                <tr>
                                <td width="2%">01</td>
                                <td width="98%">Go to bKash Menu by dialing *247#</td>
                                </tr>
                                <tr>
                                <td>02</td>
                                <td>Choose 'Payment' by pressing '3' </td>
                                </tr>
                                <tr>
                                <td>03</td>
                                <td>Enter our business wallet number : 01781516700</td>
                                </tr>
                                 <tr>
                                <td>04</td>
                                <td>Enter the User Type you want to pay : <?php echo $premiumType; ?></td>
                                </tr>
                                <tr>
                                <td>05</td>
                                <td>Enter the amount you want to pay : <?php echo $amount; ?></td>
                                </tr>
                                <tr>
                                <td>06</td>
                                <td>Enter a reference against your payment : 47202</td>
                                </tr>
                                <tr>
                                <td>07</td>
                                <td>Enter the counter number : 11</td>
                                </tr>
                                <tr>
                                <td>08</td>
                                <td>Now enter your PIN to confirm </td>
                                </tr>
                                <tr>
                                <td>09</td>
                                <td>Done! You will get a confirmation SMS </td>
                                </tr>
                                <tr>
                                <td>10</td>
                                <td>After complete all of the steps mentioned above you will get a Transaction ID at message Inbox of your mobile set. Now you have to enter that Transaction ID to confirm the transaction.  </td>
                                </tr>
                                <tr>
                                <td>&nbsp;</td>
                                <td>
                                  <label for="textfield">Enter Transaction ID :</label>
                                  <input type="text" name="transaction_id" id="transaction_id" style="width:300px;" /> 
                                  <button type="submit" class="btn btn-info">Submit</button>
                                </td>
                                </tr>
                             </table>
                         </form></td>
                    </tr>
                    </table>
                    </div>
                    </div>
                    
                                <div class="row">
                                <div class="col-lg-12" style="padding:20px;"></div>
                                </div>
	</div>                
</div>
<script type="text/javascript">
		$('#deadline_date').datepicker({dateFormat: 'dd/mm/yy'});
</script>

