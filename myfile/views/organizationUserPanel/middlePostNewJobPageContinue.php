<div class="row" style="margin-left:-5px;">
 <form action="<?php echo site_url('organizationUserHome/nextJobStore/'.$updateId); ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
      <div class="col-lg-12 col-sm-12"> <div style="float:left">Step-[ 1 of 2 ]</div><br/>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover table-bordered" style="margin-top:10px;">
  <tr>
    <td width="34%">Age Range</td>
    <td width="66%">Form
    <select name="age_range_from" id="age_range_from">
    <option value="Any">Any</option>
    <option value="18">18</option>
    <option value="19">19 </option>
    <option value="20">20 </option>
    <option value="21">21 </option>
    <option value="22">22 </option>
    <option value="23">23 </option>
    <option value="24">24 </option>
    <option value="25">25 </option>
    <option value="26">26 </option>
    <option value="27">27 </option>
    <option value="28">28 </option>
    <option value="29">29 </option>
    <option value="30">30</option>
    <option value="31">31</option>
    <option value="32">32 </option>
    <option value="33">33</option>
    <option value="34">34</option>
    <option value="35">35 </option>
    <option value="36">36</option>
    </select>
    
    To
    <select name="age_range_to" id="age_range_to">
    <option value="Any">Any</option>
    <option value="18">18</option>
    <option value="19">19 </option>
    <option value="20">20 </option>
    <option value="21">21 </option>
    <option value="22">22 </option>
    <option value="23">23 </option>
    <option value="24">24 </option>
    <option value="25">25 </option>
    <option value="26">26 </option>
    <option value="27">27 </option>
    <option value="28">28 </option>
    <option value="29">29 </option>
    <option value="30">30</option>
    <option value="31">31</option>
    <option value="32">32 </option>
    <option value="33">33</option>
    <option value="34">34</option>
    <option value="35">35 </option>
    <option value="36">36</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Gender </td>
    <td style="font-weight:lighter; font-size:11px;" >
     
        <label>
          <input type="radio" name="gender" value="male" id="gender"/>
          Male Only</label>
       
        <label>
          <input type="radio" name="gender" value="female" id="gender" />
          Female Only</label>
       
        <label>
          <input type="radio" name="gender" value="any" id="gender" />
          Any </label></td>
  </tr>
  <tr>
    <td>Job Type </td>
    <td style="font-weight:lighter; font-size:11px;"><label>
          <input type="radio" name="job_type" value="Full Time" id="job_type" />
          Full Time</label>
       
        <label>
          <input type="radio" name="job_type" value="Part Time" id="job_type" />
          Part Time</label>
       
        <label>
          <input type="radio" name="job_type" value="Contract" id="job_type" />
          Contract  </label></td>
  </tr>
  <tr>
    <td>Job Level <span style="color:#F00; font-size:16px;">* </span></td>
    <td style="font-weight:lighter; font-size:11px;"><input type="radio" name="checkbox" id="checkbox"  value="Entry Level Job"/>
      <label>Entry Level Job </label>
      <label>
        <input name="job_level" id="job_level" value="Mid/Managerial Level Job" type="radio" />
      </label>
      <label for="checkbox">Mid/Managerial Level Job 
        <input name="job_level" id="job_level" value="Top Level Job" type="radio" />
        Top Level Job </label></td>
  </tr>
  <tr>
    <td>Educational Qualification <span style="color:#F00; font-size:16px;">* </span></td>
    <td><textarea name="education_qualification" id="education_qualification" cols="45" rows="5" style="width:70%; height:100px;" ></textarea></td>
  </tr>
  <tr>
    <td>Job Description/Responsibility <span style="color:#F00; font-size:16px;">* </span></td>
    <td><textarea name="job_description" id="job_description" cols="45" rows="5" style="width:70%; height:100px;" ></textarea></td>
  </tr>
  <tr>
    <td>
      Additional Job Requirements<span style="color:#F00; font-size:16px;"> *</span></td>
    <td><textarea name="job_requirements" id="job_requirements" cols="45" rows="5" style="width:70%; height:100px;" ></textarea></td>
  </tr>
  <tr>
    <td><label for="checkbox2"></label></td>
    <td style="font-weight:lighter; font-size:11px;">  <label>
        <input type="radio" name="job_experience" value="Experience Required" id="job_experience" />
        Experience Required </label>
      <label>
        <input type="radio" name="job_experience" value="No Experience Required" id="job_experience" />
        No Experience Required </label></td>
  </tr>
  <tr>
    <td><label for="checkbox3"></label>
      <span id="spnExperienceYear">Total Years of Experience</span></td>
    <td>Min
    <select name="exprience_min" id="exprience_min">
    <option value="Any">Any</option>
    <option value="1">1</option>
    <option value="2">2 </option>
    <option value="3">3 </option>
    <option value="4">4 </option>
    <option value="5">5 </option>
    <option value="6">6</option>
    </select>
    
   Max
    <select name="exprience_max" id="exprience_max">
    <option value="Any">Any</option>
    <option value="2">2</option>
    <option value="3">3 </option>
    <option value="4">4 </option>
    <option value="5">5 </option>
    <option value="6">6 </option>
    <option value="7">7 </option>
	<option value="8">8 </option>
    <option value="9">9 </option>
    <option value="10">10 </option>
    <option value="11">11 </option>
    <option value="12">12 </option>
    </select></td>
  </tr>
 
  
  <tr>
    <td>Compensation and Other Benefits </td>
    <td style="font-weight:lighter; font-size:11px;"><p><strong><u>Salary Range </u></strong></p>
      <p>
        <label>
          <input type="radio" name="sallary_range" value="Negotiable" id="sallary_range" />
          Negotiable </label>
        <br />
        <label>
          <input type="radio" name="sallary_range" value="Not Mention" id="sallary_range" />
          Don't want to mention </label>
        <br />
        <label>
          <input type="radio" name="sallary_range" value="Desplay" id="sallary_range" />
          Want to display the following range </label>
       
      </p>
      <div style="display:none;">
       Minimum <input type="text" name="sallary_money_min" id="sallary_money_min" style="width:20%; height:35px; margin-top:5px;" />
       Maximum <input type="text" name="sallary_money_max" id="sallary_money_max" style="width:20%; height:35px;" /> Monthly in BDT
      </div>
      </td>
  </tr>
  <tr>
    <td>Job Publish Status </td>
    <td style="font-weight:lighter; font-size:11px;"><label>
        <input type="radio" name="job_publish_status" value="Publish" id="job_publish_status" />
        Ready to publish</label>
      <label><input type="radio" name="job_publish_status" value="Publish later" id="job_publish_status" />
        Publish  later (drafted jobs)</label></td>
  </tr>
  </table>

<button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Submit</button>
   </div>
   </form>
      
     </div>
   

