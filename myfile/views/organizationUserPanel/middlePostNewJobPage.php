	<div class="row" style="margin-left:-5px;">
   <form id="form1" name="form1" method="post" action="<?php echo site_url('organizationUserHome/jobPostStore');?>">
      <div class="col-lg-12 col-sm-12"><div style="float:left">Post New Job Ads</div><br/>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover table-bordered" style="margin-top:10px;">
  <tr>
    <td width="34%">Ad Type <span style="color:#F00; font-size:16px;">* </span></td>
    <td width="66%">
      <select name="add_type" id="add_type" style="width:200px; height:30px;">
    <option value="">Select Ad Type</option>
    <option value="Basic Listing">Basic Listing</option>
    <option value="Stand Listing">Stand Listing </option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Job Category <span style="color:#F00; font-size:16px;">* </span> </td>
    <td><select name="job_category" id="job_category" style="width:200px; height:30px;">
    <option value="">Select Job Title</option>
    <option value="Education provider">Education provider</option>
    <option value="Agency">Agency </option>
    <option value="Couching Centre">Couching Centre</option>
    <option value="professional Training Centre">professional Training Centre</option>
    <option value="Other">Other</option>
    </select></td>
  </tr>
  <tr>
    <td>Organization Type <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="organization_type" id="organization_type" style="width:200px; height:35px;" value="<?php echo $orgType; ?>"/></td>
  </tr>
  <tr>
    <td>Job Title <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="title" id="title" style="width:70%; height:35px;" required /></td>
  </tr>
  <tr>
    <td>No. of Vacancies <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="vacancies" id="vacancies" style="width:200px; height:35px;" /></td>
  </tr>
  <tr>
    <td>How do you want to receive CV / Resume(s) ? <span style="color:#F00; font-size:16px;">* </span></td>
    <td>(User must select at least one from the given options)</td>
  </tr>
  <tr>
    <td><input type="checkbox" name="receive_online" id="receive_online" value="online" />
      <label for="checkbox"></label>
      Online CV/Resume</td>
    <td>[Bdjobs CV format; CV will be available in your corporate inbox.] </td>
  </tr>
  <tr>
    <td><input type="checkbox" name="receive_email" id="receive_email" value="email"/>
      <label for="checkbox2"></label>
      Email Attachment </td>
    <td>[Applicants can send their CV as an attachment in your given Email address.] </td>
  </tr>
  <tr>
    <td><input type="checkbox" name="receive_hardcopy" id="receive_hardcopy" value="hardcopy"/>
      <label for="checkbox3"></label>
      Hard Copy CV</td>
    <td>[Applicants can send hard copy of their CV in your corporate address.] </td>
  </tr>
  <tr>
    <td>Applicant should enclose Photograph with CV ? </td>
    <td style="font-weight:lighter; font-size:11px;"><p>
      <label>
        <input type="radio" name="cv_enclose" id="cv_enclose" value="yes" />
        Yes</label>
      
      <label>
        <input type="radio" name="cv_enclose" id="cv_enclose" value="no" />
        No</label></p></td>
  </tr>
  <tr>
    <td>Application Deadline <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="apllication_deadline" id="apllication_deadline" class="date-picker" style="width:70%; height:35px;" 
	data-date-format="yyyy-mm-dd" required /></td>
  </tr>
  <tr>
    <td>Billing Contact <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="contact_person" id="contact_person" style="width:200px; height:35px;" value="<?php echo $contPerson; ?>" /></option>
    </select></td>
  </tr>
  <tr>
    <td>Designation <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="designition" id="designition" style="width:200px; height:35px;" value="<?php echo $desinition; ?>" /></td>
  </tr>
  <tr>
    <td>Do you want to display Organization Name ?</td>
    <td style="font-weight:lighter; font-size:11px;"><p>
      <label>
        <input type="radio" name="desplay_orgname" value="yes" id="desplay_orgname" />
        Yes</label>
      
      <label>
        <input type="radio" name="desplay_orgname" value="no" id="desplay_orgname" />
        No</label>
    
    </p></td>
  </tr>
  <tr>
    <td>Do you want to hide Organization Address ? </td>
    <td style="font-weight:lighter; font-size:11px;"><p>
      <label>
        <input type="radio" name="desplay_address" value="yes" id="desplay_address" />
        Yes</label>
      
      <label>
        <input type="radio" name="desplay_address" value="no" id="desplay_address" />
        No</label>
    
    </p></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
  </tr>
  </table>

<button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Continue</button>

   </div>
   </form>
      
     </div>
	 <script type="text/javascript">
		$('.date-picker').datepicker({
      autoclose: true   
    }); 
	</script>
   

