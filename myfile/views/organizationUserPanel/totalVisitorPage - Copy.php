<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
	<style type="text/css">
		#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
		#menu1 a:active {color:black;text-decoration:none;}
		#menu1 a:hover {color:black;background-color:#FFFF99}
		#menu1 a:visited {color:black;text-decoration:none;}
		body{
		background-color:#EEEEEE;
		
		}


		.row-fluid.bs {
		    background-color: #fff;
		    border-radius: 4px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
		    margin: 0 0 15px;
		    padding: 15px 0;
		    transition: all 0.5s ease 0s;
		}
		
		.vertical-wrapper {
		    display: table;
		    vertical-align: middle;
		    width: 100%;
		}
		.vertical-content {
		    display: table-cell;
		    float: none;
		    margin: 0;
		    position: relative;
		    text-align: left;
		    vertical-align: middle;
		}
			
	</style>


        <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
		<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
				  
   
  </head>
  <body>
  
      <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       
       <div class="container"> 
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div>
							  <div class="row">
							  <div class="col-lg-7" align="center" style="font-size:18px;"><?php echo $updateText; ?></div>
                        
                                    <div class="col-lg-5" align="center">
                              <div class="comntorg2 link-stylejob" style="color:#FFF; padding-top:5px;">
							  <a href="<?php echo site_url('organizationUserHome/PostNewCV'); ?>">Search CV Bank</a></div>
                              <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
                              <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a></div></div>
								</div>
							  <div class="row">&nbsp;</div> 
							  <div class="row">
							  
									<div class="col-lg-12" align="center">
									    <div style="background:#f4f4f4; padding:0 20px; border-bottom:solid 1px #CCC; border-left:solid 1px #CCC; border-right:solid 1px #CCC;">
	  
		
		                          
		
							
									<div class="row-fluid vertical-wrapper" style="padding:15px 0">				
										
                                       <div class="col-md-10" style="padding:0px">

                                          <form class="form-inline" method="post" action="<?php echo site_url('organizationUserHome/visitorSort') ?>">
                                              <div class="form-group">
                                                  <label for="select_category">Sellect Category &nbsp;&nbsp;</label>
                                                    <select name="select_category" id="select_category" class="form-control">
      												  <option value="" selected>Sellect Category</option>
        											   <option value="Blog">Blog</option>
        											   <option value="Advertisement">Advertisement</option>
        											   <option value="Programe">Programe</option>
        											   <option value="Job Post">Job Post</option>
      												</select> &nbsp;&nbsp; 
    
  												</div> 
    
  
 												<div class="form-group">
    												<label  for="from_date">From Date &nbsp;&nbsp;</label>
    														<input type="text" class="form-control date-picker" name="from_date" id="from_date" placeholder="From Date" data-date-format="yyyy-mm-dd">&nbsp;&nbsp; &nbsp;
  												</div>
	  										<div class="form-group">
	    										<label  for="to_date">To Date &nbsp;&nbsp;</label>
	    										<input type="text" class="form-control date-picker" name="to_date" id="to_date" placeholder="To Date" data-date-format="yyyy-mm-dd"> &nbsp;&nbsp; &nbsp;
	  									    </div>
	  										    <button type="submit" class="btn btn-primary" style="width:80px">Filter</button>
										  </form>

	                                   </div>

	                                   <div class="col-md-2">
	                                      <div class="btn-group">
											  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											    Send Message <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu">
											    <li><a  class="msg" data-value="all" href="#">All</a></li>
											    <li><a  class="msg" data-value="selected"  href="#">Sellected</a></li>
											  </ul>
											</div>
										</div>

									
									</div>

                             

									<div class="row-fluid vertical-wrapper" style="padding:15px 0">				
										<div class="col-md-5 vertical-content">
											<div class="col-md-4 text-center"></div>
											<div class="col-md-8 text-center"><strong>Date Time</strong></div>
										</div>
										<div class="col-md-7 vertical-content">
											<div class="col-md-7 text-left"><strong>Visitor Info</strong></div>
											<div class="col-md-5 text-center"><strong>Visit Page</strong></div>
											
										</div>
									</div>


								  <?php 
                                     $i = 1;
		                            foreach ($totalVisitorDet as $v){

		                              if(!empty($v->blog_id)){
		                              	$linkPageName = "Blog";
		                              } elseif (!empty($v->ad_id)) {
		                              	$linkPageName = "Advertisement";
		                              }  elseif (!empty($v->programe_id)) {
		                              	$linkPageName = "Programe";
		                              } else {
                                        $linkPageName = "Job Post";
		                              }

		                              if($v->user_type =="general"){
		                              	$mobile = $v->mobile;

		                              }else{
                                         $mobile = $v->mobile_com;
		                              }

		                             
		                           ?>
								 
									   <div class="row-fluid bs vertical-wrapper">
											<div class="col-md-5 text-left vertical-content">
												<div class="col-md-4 text-center">
													<div class="checkbox">
													    <label>
													      <input type="checkbox">
													    </label>
													  </div>
												</div>
												<div class="col-md-8 text-center">
												    <?php echo $v->date ?>
													
											</div>					
										</div>
										<div class="col-md-7 vertical-content">
											<div class="col-md-7 text-left">
												<h4 style="margin-top:0"><?php echo $v->name ?> </h4>
												<p><?php echo $v->user_id ?></p>
												<p><?php echo $mobile ?></p>
											</div>
                                            <?php if(!empty($v->blog_id)){ ?>
											  <div class="col-md-5 vertical-content"><a class="blog" href="<?php echo site_url($v->visit_link); ?>"><p style="margin-top:25px; margin-left:10px; font-size:13px; text-align:right"><?php echo $linkPageName ?></p></a></div>
										   <?php }else{ ?>
											  <div class="col-md-5 vertical-content text-center" style="width:250px;"><a target="_blank"  href="<?php echo site_url($v->visit_link); ?>"><p style="margin-top:25px; font-size:13px;"><?php echo $linkPageName ?></p></a></div>
                                           <?php } ?> 
											
										  </div>
									    </div>
									
									
						                <?php } ?>


	                                </div>
							    </div>
						     </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerTopPage'); ?>
	    <?php $this->load->view('footerPage'); ?>

	    <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	 <script>
	 
	 
	     $('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		
				resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();




		$('.blog').on('click', function(e){
			var url = $(this).attr('href');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});


		$('.msg').on('click', function(e) {

			var valueType   = $(this).attr('data-value');
			console.log(valueType);
			/*$.ajax({
				url: "<?php echo site_url('organizationUserHome/shortListAction') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					alert('Short List');
					window.location.assign('<?php echo site_url('organizationUserHome') ?>');
				}
			});*/

			e.preventDefault();
			
		});


 </script>
  </body>
</html>