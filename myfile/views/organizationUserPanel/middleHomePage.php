	<style type="text/css">
		.row-fluid.bs {
		    background-color: #fff;
		    border-radius: 4px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);
		    margin: 0 0 15px;
		    padding: 15px 0;
		    transition: all 0.5s ease 0s;
		}
		.row-fluid.bs:hover, .row-fluid.bs:focus {
		    box-shadow: 0 1px 8px 0 rgba(0, 0, 0, 0.6);
		    transition: all 0.5s ease 0s;
		}
		.vertical-wrapper {
		    display: table;
		    vertical-align: middle;
		    width: 100%;
		}
		.vertical-content {
		    display: table-cell;
		    float: none;
		    margin: 0;
		    position: relative;
		    text-align: left;
		    vertical-align: middle;
		}		
	</style>

	<ul class="nav nav-tabs" style="background-color:#EEEEEE">
		<li class="active"><a href="#latest">Latest Job Ad( <?php echo $totalLatestJob; ?> )</a></li>
		<li><a href="#draft">Drafted Job ( <?php echo $totalLatestDraft; ?> )</a></li>
		<li><a href="#archive">Archived Job ( <?php echo $totalLatestAchived; ?> )</a></li>
	</ul>

	<div class="tab-content" style="background:#f4f4f4; padding:0 20px; border-bottom:solid 1px #CCC; border-left:solid 1px #CCC; border-right:solid 1px #CCC;">
	  
		<div id="latest" class="tab-pane fade in active">
			<div class="row-fluid vertical-wrapper" style="padding:15px 0">				
				<div class="col-md-4 vertical-content">
					<div class="col-md-2 text-center">#</div>
					<div class="col-md-10 text-left">Job Title</div>
				</div>
				<div class="col-md-8 vertical-content">
					<div class="col-md-2 text-center">Applications</div>
					<div class="col-md-2 text-center">Viewed</div>
					<div class="col-md-2 text-center">Not Viewed</div>
					<div class="col-md-2 text-center">Shortlist</div>
					<div class="col-md-2 text-center"> candidates</div>
					<div class="col-md-2 text-center">Action</div>
				</div>
			</div>
			
			 <?php 
			   $i = 1;
			  // print_r($OrganizePostInfo);
			   foreach($OrganizePostInfo as $v){
			   $id     			= $v->id;
			   $jobLink    		= array('organizationUserHome','jobWiseDetails', $id);
			   $viewJob    		= array('organizationUserHome','viewJob', $id);
			   $cvViewLink    	= array('organizationUserHome','cvViewDetails', $id);
			   $cvNotViewLink   = array('organizationUserHome','cvNotViewDetails', $id);
			   $shortListLink   = array('organizationUserHome','shortListDetails', $id);
			   $starCandLink    = array('organizationUserHome','starCandidateDetails', $id);
			  
			  ?>
			<div class="row-fluid bs vertical-wrapper">
				<div class="col-md-4 text-left vertical-content">
					<div class="col-md-2 text-center" style="margin-top:40px">
						<span class="badge"><?php echo $i++; ?></span>
					</div>
					<div class="col-md-10 text-left">
						<h4 style="margin-top:0"><a href="<?php echo site_url($viewJob); ?>"><?php echo $v->title; ?></a> </h4>
						<p>Posted: <?php echo $v->job_post_date ?></p>
						<p>Deadline: <?php echo $v->apllication_deadline ?></p>
					</div>					
				</div>
				<div class="col-md-8 vertical-content">
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($jobLink); ?>"><?php echo $v->totalApplicant; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($cvViewLink); ?>"><?php echo $v->viewed; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($cvNotViewLink); ?>"><?php echo $v->notviewed; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($shortListLink); ?>"><?php echo $v->sortlist; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($starCandLink); ?>"><?php echo $v->star; ?></a></div>
					<div class="col-md-2 text-center">
						<div class="btn-group-vertical">
							<a href="<?php echo site_url('organizationUserHome/jobPostEdit/'. $id); ?>"><button style="width:70px;" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Edit </button></a>
							<a class="red" href="#" data-id="<?php echo $id ?>"><button style="width:70px;"  class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Delete </button></a>
							<a class="draft" href="#" data-id="<?php echo $id ?>"><button style="width:70px;"  class="btn btn-default btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Draft </button></a>
						</div>
					</div>
				</div>
			</div>
			
			<?php } ?>
		</div>
		
		<div id="draft" class="tab-pane fade">
			<div class="row-fluid vertical-wrapper" style="padding:15px 0">				
				<div class="col-md-4 vertical-content">
					<div class="col-md-2 text-center">#</div>
					<div class="col-md-10 text-left">Job Title</div>
				</div>
				<div class="col-md-8 vertical-content">
					<div class="col-md-2 text-center">Applications</div>
					<div class="col-md-2 text-center">Viewed</div>
					<div class="col-md-2 text-center">Not Viewed</div>
					<div class="col-md-2 text-center">Shortlist</div>
					<div class="col-md-2 text-center"> candidates</div>
					<div class="col-md-2 text-center">Action</div>
				</div>
			</div>
			
			 <?php 
			   $i = 1;
			  // print_r($OrganizePostInfo);
			   foreach($draftPostInfo as $v){
			   $id     			= $v->id;
			   $jobLink    		= array('organizationUserHome','jobWiseDetails', $id);
			   $viewJob    		= array('organizationUserHome','viewJob', $id);
			   $cvViewLink    	= array('organizationUserHome','cvViewDetails', $id);
			   $cvNotViewLink   = array('organizationUserHome','cvNotViewDetails', $id);
			   $shortListLink   = array('organizationUserHome','shortListDetails', $id);
			   $starCandLink    = array('organizationUserHome','starCandidateDetails', $id);
			  
			  ?>
			<div class="row-fluid bs vertical-wrapper">
				<div class="col-md-4 text-left vertical-content">
					<div class="col-md-2 text-center" style="margin-top:40px">
						<span class="badge"><?php echo $i++; ?></span>
					</div>
					<div class="col-md-10 text-left">
						<h4 style="margin-top:0"><a href="<?php echo site_url($viewJob); ?>"><?php echo $v->title; ?></a> </h4>
						<p>Posted: 29 Aug 2015</p>
						<p>Deadline: 5 Sep 2015</p>
						<p>Pending Approval</p>
					</div>					
				</div>
				<div class="col-md-8 vertical-content">
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($jobLink); ?>"><?php echo $v->totalApplicant; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($cvViewLink); ?>"><?php echo $v->viewed; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($cvNotViewLink); ?>"><?php echo $v->notviewed; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($shortListLink); ?>"><?php echo $v->sortlist; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($starCandLink); ?>"><?php echo $v->star; ?></a></div>
					<div class="col-md-2 text-center">
						<div class="btn-group-vertical">
							<a href="<?php echo site_url('organizationUserHome/jobPostEdit/'. $id); ?>"><button style="width:70px;" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Edit </button></a>
							<a class="red" href="#" data-id="<?php echo $id ?>"><button style="width:70px;"  class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Delete </button></a>
							<a class="draft" href="#" data-id="<?php echo $id ?>"><button style="width:70px;"  class="btn btn-default btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Draft </button></a>
						</div>
					</div>
				</div>
			</div>
			
			<?php } ?>
		</div>
		
		
		<div id="archive" class="tab-pane fade">
			<div class="row-fluid vertical-wrapper" style="padding:15px 0">				
				<div class="col-md-4 vertical-content">
					<div class="col-md-2 text-center">#</div>
					<div class="col-md-10 text-left">Job Title</div>
				</div>
				<div class="col-md-8 vertical-content">
					<div class="col-md-2 text-center">Applications</div>
					<div class="col-md-2 text-center">Viewed</div>
					<div class="col-md-2 text-center">Not Viewed</div>
					<div class="col-md-2 text-center">Shortlist</div>
					<div class="col-md-2 text-center"> candidates</div>
					<div class="col-md-2 text-center">Action</div>
				</div>
			</div>
			
			 <?php 
			   $i = 1;
			  // print_r($OrganizePostInfo);
			   foreach($archivePostInfo as $v){
			   $id     			= $v->id;
			   $jobLink    		= array('organizationUserHome','jobWiseDetails', $id);
			   $viewJob    		= array('organizationUserHome','viewJob', $id);
			   $cvViewLink    	= array('organizationUserHome','cvViewDetails', $id);
			   $cvNotViewLink   = array('organizationUserHome','cvNotViewDetails', $id);
			   $shortListLink   = array('organizationUserHome','shortListDetails', $id);
			   $starCandLink    = array('organizationUserHome','starCandidateDetails', $id);
			  
			  ?>
			<div class="row-fluid bs vertical-wrapper">
				<div class="col-md-4 text-left vertical-content">
					<div class="col-md-2 text-center" style="margin-top:40px">
						<span class="badge"><?php echo $i++; ?></span>
					</div>
					<div class="col-md-10 text-left">
						<h4 style="margin-top:0"><a href="<?php echo site_url($viewJob); ?>"><?php echo $v->title; ?></a> </h4>
						<p>Posted: 29 Aug 2015</p>
						<p>Deadline: 5 Sep 2015</p>
						<p>Pending Approval</p>
					</div>					
				</div>
				<div class="col-md-8 vertical-content">
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($jobLink); ?>"><?php echo $v->totalApplicant; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($cvViewLink); ?>"><?php echo $v->viewed; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($cvNotViewLink); ?>"><?php echo $v->notviewed; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($shortListLink); ?>"><?php echo $v->sortlist; ?></a></div>
					<div class="col-md-2 text-center" style="margin-top:25px"><a href="<?php echo site_url($starCandLink); ?>"><?php echo $v->star; ?></a></div>
					<div class="col-md-2 text-center">
						<div class="btn-group-vertical">
							<a href="<?php echo site_url('organizationUserHome/jobPostEdit/'. $id); ?>"><button style="width:70px;" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Edit </button></a>
							<a class="red" href="#" data-id="<?php echo $id ?>"><button style="width:70px;"  class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Delete </button></a>
							<a class="draft" href="#" data-id="<?php echo $id ?>"><button style="width:70px;"  class="btn btn-default btn-xs"><i class="glyphicon glyphicon-pencil" title="Edit"></i> Draft </button></a>
						</div>
					</div>
				</div>
			</div>
			
			<?php } ?>
		</div>

	</div>

   <script>
			$('.draft').on('click', function() {
					var x = confirm('Are you sure to Draft?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('organizationUserHome/jobPostDraft/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
				
				$('.archived').on('click', function() {
					var x = confirm('Are you sure to Archived?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('organizationUserHome/jobPostArchived/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
				
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to Delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('organizationUserHome/jobPostDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
 </script>

