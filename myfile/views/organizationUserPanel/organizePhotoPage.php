<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
	<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
	<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "exact",
			elements : "ajaxfilemanager",
			theme : "advanced",
			// TO GET THE FULL IMAGE URL
			relative_urls: false,
    		remove_script_host: false,
			/****************/
			setup : function(ed) {
			      ed.onKeyUp.add(function(ed, l) {
			         tinyMCE.triggerSave();	                    
			      });
			},
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",

			theme_advanced_buttons1_add_before : "newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
			theme_advanced_buttons2_add_before: "cut,copy,separator,",
			theme_advanced_buttons3_add_before : "",
			theme_advanced_buttons3_add : "media",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "ajaxfilemanager",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php");
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php"),
                width: 482,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
		}
	</script>
               <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				body{
				  background-color:#EEEEEE;
				
				}
				
				</style>
	
         <?php $this->load->view('jsLinkPage'); ?>  
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
    
  </head>
  <body>
        <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
							  <div class="welll welll-lg">
   					             <div class="row">
									  <div class="col-lg-7" align="center"></div>

									  <div class="col-lg-5" align="center">
									  <div class="comntorg2" style="color:#FFF; padding-top:5px;">Search CV Bank</div>
									  <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
									            <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a>
										    </div>
										  </div>
    									</div> 
									<div class="row">&nbsp;</div>
									<div class="row">
									   <div class="col-lg-12" align="center">
										 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Organization Photo Gallery manage</span></div>
									</div>  
									<div class="row">&nbsp;</div>
									<div class="row">
									  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
									  </div>
									</div>
								<div class="row">&nbsp;</div>   
								<div class="row">
								  <div class="col-lg-11">
									<form action="<?php echo site_url('organizationUserHome/orgPhotoGalleryStore'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Title</label>
										<div class="col-sm-7">
										  <input type="text" class="form-control" id="title" name="title" placeholder="Title">
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Gallery Image</label>
										<div class="col-sm-7">
										  <input type="file" id="image" name="image" tabindex="8" />
										</div>
									  </div>
									  
									  <div class="form-group">
										<div class="col-sm-offset-3 col-sm-10">
										  <button type="submit" class="btn btn-primary">Submit</button>
										</div>
									  </div>
									</form>
									</div>
									<div class="col-lg-1"></div>
								         
								</div>
        							<div class="row">&nbsp;</div>
									<div class="row">&nbsp;</div>
                               <div class="row">
                               <div class="col-lg-12 table-responsive" style="padding:0px;">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                  <tr class="active" style="font-weight:bold;">
                                    <td width="8%">Sl No</td>
                                    <td width="70%">Title</td>
                                    <td width="10%">Image</td>
                                    <td width="12%" align="center" valign="middle">Action</td>
                                  </tr>
                                  <?php 
                                    //print_r($studentId);
                                    $i = 1;
                                    foreach($organizePhotoInfo as $v){
                                        $id		= $v->id;
                                  ?>
                                  <tr> 
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->title; ?></td>
                                    <td><img src="<?php echo base_url("/Images/photo_gallery_resize/$v->image"); ?>" height="50" width="100" /></td>
                                    <td align="center">
                                        <a href="<?php echo site_url('organizationUserHome/organizePhotoEdit/'. $id); ?>" name="Edit">
                                        Edit
                                        </a>
										||
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            Delete
                                        </a>
                                    </td>
                                  </tr> <?php } ?>
                                </table> 
                               </div>
                             </div>
							</div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>  
			    
             
       </div>
	   <?php $this->load->view('footerTopPage'); ?>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script>
		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('organizationUserHome/organizePhotoDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});
	</script>

	<script>
	resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
 </script>
    
  </body>
</html>