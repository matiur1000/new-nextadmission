<style>
  .commentStyle {
	border-left:1px;
	border-right:1px;
	border-top:1px;
    border-style:solid;
	border-color:#CCCCCC;
	border-width:100%;
	padding:5px;
	
}

.commentStyle:last-child {
    border-bottom:1px solid #CCCCCC;	
} 

.commentStyle h5{
	font-size: 14px;
	font-weight: normal;
	margin-top: 5px;
	margin-bottom: 0px;
}
.commentStyle p{
	font-size: 14px;
	font-weight: normal;
	padding-bottom:10px;
	float:left;
}

.commentStyle img{
	background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    display: inline-block;
    line-height: 1.42857;
    margin-right: 10px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}

</style>




<div class="modal-header">
	<h3 class="modal-title" id="myModalLabel">All Message Details</h3>
</div>
  
<div class="modal-body">

  <?php 
    foreach($msgWiseDetails as $v){
  ?>
  <div id="msgdelete" class="row">
  	<div class="col-lg-12" style="padding-bottom:10px;">
  	<button id="msgClose" type="button" style="color:#F40000" class="close" aria-label="Close" data-delete-id="<?php echo $v->id ?>"><span aria-hidden="true">&times;</span></button>
	<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" style="margin:0 15px 0 0" width="100" height="80" />
    <p style="text-align:justify"><?php echo $v->message ?> </p>
	
	
	
	
     <div class="row">
	    <div id="replyView" class="col-lg-12" style="padding-top:30px;">
		    <?php 
			 	
		        if(!empty($v->reply)){
				 foreach ($v->reply as $rv){
			  ?>
		     <div class="col-md-12 commentStyle">
		         <img src="<?php echo base_url("Images/Register_image/$rv->image"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
				<h5><a><?php echo $rv->name; ?></a></h5>
				<h6><?php echo $rv->date; ?></h6>	
				<p style="float:left"><?php echo $rv->reply; ?></p> 
		     </div>
		  
		  <?php } } ?>
        </div>

         <div id="replyText" class="col-md-12" style="margin-top:20px;">
		           <div class="form-group">
				    <label for="reply">Write Your Reply</label>
					<textarea name="reply" id="reply" class="form-control" rows="3"></textarea>
				  </div>
				  <div class="form-group" style="float:left">
					<button id="replyMsg" class="btn btn-success" data-reply-id="<?php echo $v->id ?>" >Post Reply</button>
				  </div>
         </div>
	   
	 </div>
	</div>
 </div>

	 <?php } ?>
	
		 
</div>
		

<div class="modal-footer">
	<button class="btn btn-danger" data-dismiss="modal">Close</button>
</div>



<script>
  
   $(document).on("click", "#replyMsg", function(e){
		var replyId 	= $(this).attr('data-reply-id');
        var reply 		= $(this).parents('#replyText').find("textarea[name='reply']").val(); 
	    var formURL  	= "<?php echo site_url('organizationUserHome/messageReplyAction') ?>";
		console.log(reply);
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : {replyId : replyId, reply : reply},
			success:function(data){
				$(this).parents('#replyText').find("input[type=text], textarea").val("");
				location.reload();
				$("#msgDetails").modal('hide');
			}
		});
		
		e.preventDefault();
	});


     $(document).on("click", "#msgClose", function(e)
		{
			var deleteId = $(this).attr('data-delete-id');
			var formURL  = "<?php echo site_url('organizationUserHome/msgDeleteAction') ?>";
			console.log(deleteId);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {deleteId : deleteId},
				success:function(data){
					$("#msgDetails").modal('hide');
					//var parent = $(this).parents('#msgdelete');
					//$(parent).remove();
				}
			});
			
			e.preventDefault();
		});

</script>