<div class="col-lg-9" align="right">
	<div class="welll welll-lg"> 
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Upload Interested Job</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		<div class="row">
		 <form action="<?php echo site_url('organizationUserHome/interestJobUpdate'); ?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="id" id="id" value="<?php echo $intrestEditInfo->id; ?>" />
			<div class="col-lg-12">
			   <div class="row">
			      <div class="col-lg-5">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Select Education provider Type</span> &nbsp;: 
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
					<select class="form-control" name="education_provider_type" id="education_provider_type" style="width:100%;">
						<option <?php if($intrestEditInfo->education_provider_type == 'School /College up to  12'){?> selected="selected" <?php } ?> value="School /College up to  12">School /College up to  12 </option>
						<option <?php if($intrestEditInfo->education_provider_type == 'University'){?> selected="selected" <?php } ?> value="University">University </option>
						<option <?php if($intrestEditInfo->education_provider_type == 'Other'){?> selected="selected" <?php } ?> value="Other">Other</option>
					 </select> 
					</div>
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-5">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Select Interested  Program</span> &nbsp;: 
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
					<select class="form-control" name="interested_program" id="interested_program" style="width:100%;">
						<option <?php if($intrestEditInfo->interested_program == 'K-8 years Class'){?> selected="selected" <?php } ?> value="K-8 years Class">K-8 years Class</option>
						<option <?php if($intrestEditInfo->interested_program == '9-10 years Class'){?> selected="selected" <?php } ?> value="9-10 years Class">9-10 years Class</option>
						<option <?php if($intrestEditInfo->interested_program == '10-12 years Class'){?> selected="selected" <?php } ?> value="10-12 years Class">10-12 years Class</option>
					 </select> 
					</div>
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-5">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Select Location</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
					<select class="form-control" name="location" id="location" style="width:100%;">
						<option <?php if($intrestEditInfo->location == 'Dhaka'){?> selected="selected" <?php } ?> value="Dhaka">Dhaka</option>
						<option <?php if($intrestEditInfo->location == 'Comilla'){?> selected="selected" <?php } ?> value="Comilla">Comilla</option>
						<option <?php if($intrestEditInfo->location == 'Rajsahi'){?> selected="selected" <?php } ?> value="Rajsahi">Rajsahi</option>
					 </select> 
					</div>
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-5">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Other</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
					<select class="form-control" name="other" id="other" style="width:100%;">
						<option <?php if($intrestEditInfo->other == 'Work'){?> selected="selected" <?php } ?> value="Work">Work</option>
						<option <?php if($intrestEditInfo->other == 'Couching'){?> selected="selected" <?php } ?> value="Couching">Couching</option>
						<option <?php if($intrestEditInfo->other == 'English'){?> selected="selected" <?php } ?> value="English">English</option>
						<option <?php if($intrestEditInfo->other == 'Training'){?> selected="selected" <?php } ?> value="Training">Training</option>
						<option <?php if($intrestEditInfo->other == 'Etc'){?> selected="selected" <?php } ?> value="Etc">Etc</option>
					 </select> 
					</div>
				  </div>
			   </div>
				   
					  
					  
						 
		   </div>  
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Update</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	</div>                
</div>
