<div class="col-lg-3">  <!--col 3 Start-->   
  <div class="row  content_box_right" style="padding-left:0px;">
     <div class="row" style="padding-left:10px;">
		<div class="col-lg-3"> 
		   <img src="<?php echo base_url("Images/Basic_image/ficon_01.png"); ?>" />
		</div>
		<div class="col-lg-9" style="padding-left:14px; padding-right:0px; padding-top:8px;"> 
		   <ul>
			   <li style="list-style:none; padding-right:0px; !important; font-size:14px; font-family:sans-serif">Get instant Notification about Find Global Study and More. </li>
		   </ul>
		</div>
	 </div>
		
		<div class="row">&nbsp;</div>
		
		
	    <div class="row" style="padding-left:10px;">
		<div class="col-lg-3"> 
		   <img src="<?php echo base_url("Images/Basic_image/ficon_02.png"); ?>" />
		</div>
		<div class="col-lg-9" style="padding-left:14px; padding-right:0px; padding-top:8px;"> 
		   <ul>
			   <li style="list-style:none; padding-right:0px; !important; font-size:14px; font-family:sans-serif">Your world of Study on the go. Search, View, Apply to any University from anywhere. </li>
		   </ul>
		</div>
	</div>
		
		
		<div class="row">&nbsp;</div>
		
		
	    <div class="row" style="padding-left:10px;">
		<div class="col-lg-3"> 
		   <img src="<?php echo base_url("Images/Basic_image/ficon_03.png"); ?>" />
		</div>
		<div class="col-lg-9" style="padding-left:14px; padding-right:0px; padding-top:8px;"> 
		   <ul>
			   <li style="list-style:none; padding-right:0px; !important; font-size:14px; font-family:sans-serif">Receive Messages from Organization and Increase your Possibility.  </li>
		   </ul>
		</div>
	</div>
	
	
	   <div class="row">&nbsp;</div>
		
		
	    <div class="row" style="padding-left:10px;">
		<div class="col-lg-3"> 
		   <img src="<?php echo base_url("Images/Basic_image/ficon_04.png"); ?>" />
		</div>
		<div class="col-lg-9" style="padding-left:14px; padding-right:0px; padding-top:8px;"> 
		   <ul>
			   <li style="list-style:none; padding-right:0px; !important; font-size:14px; font-family:sans-serif">Join Many  Organization who have posted more Information about study!.  </li>
		   </ul>
		</div>
	</div>
		
  </div>
	
	<div class="row">&nbsp;</div>
	<div class="row">
	 <div class="col-lg-12 content_box_right"> 
		   <ul>
		     <li style="list-style:none; padding-right:0px; padding-bottom:10px; !important; font-size:20px; font-family:sans-serif">Need Help? </li>
			 <li style="list-style:none; padding-right:0px; padding-bottom:10px; !important; font-size:14px; font-family:sans-serif">If you are facing any problem and have any query then feel free to ask. </li>
			 <li style="list-style:none; font-family:sans-serif; font-weight:bold; padding-bottom:5px; font-size:16px; color:#FF0000"><i class="glyphicon glyphicon-phone"></i>   01929588684</li>
			 <li style="list-style:none; font-family:sans-serif; font-weight:bold; font-size:16px; color:#1579AE; padding-bottom:5px;"><i class="glyphicon glyphicon-envelope"></i> findstudy@gmail.com</li>
		   </ul>
	  </div>
	</div>
	
	
	
	<div class="row">&nbsp;</div>
	<div class="row">
	 <div class="col-lg-12 content_box_right"> 
		     <ul>
		     <li style="list-style:none; padding-right:0px; padding-bottom:10px; !important; font-size:20px; font-family:sans-serif; text-align:center">Already Registered? </li>
			 <li style="list-style:none; padding-right:0px; padding-bottom:10px; !important; font-size:14px; font-family:sans-serif"><button type="submit" id="submit" name="submit" class="btn btn-primary  btn-lg center-block" tabindex="1">Sign In Now</button></li>
		   </ul>
	  </div>
	</div>
	
  </div>
  