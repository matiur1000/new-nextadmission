<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
 <style>
	body{ font-family: Tahoma; font-size:13px;}
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}
 </style>
	
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
     <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	 <script src="<?php echo base_url('resource/js/menu_script.js'); ?>"></script>
	 <script language="javascript" type="text/javascript" src="<?php echo base_url("resource/js/ajax.js"); ?>"></script>
     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

    <script src="<?php echo base_url('resource/js/script.js'); ?>"></script>
	<!--CALENDER-->
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-1.7.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-1.8.16.custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-timepicker-addon.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-sliderAccess.js'); ?>"></script>
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('resource/css/jquery-ui-1.8.16.custom.css'); ?>" />
	<script type="text/javascript">
		
		$(function(){
			
	
			$('.example-container > pre').each(function(i){
				eval($(this).text());
			});
		});
		
	</script>

<!--------------------------------------END CALENDER------------------------------------>

    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->

  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
        	 <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
            <div class="row">
				<?php $this->load->view('middleRegistrationSuccessPage'); ?>  
 
            </div>                           
       </div>
       <!--footer-->
        <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript">
		$('#date_of_birth').datepicker({dateFormat: 'dd/mm/yy'});
   </script>
	<script>
	   // Region Wise Country
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});
	
	
	// Country Wise Nationality
	
	$("#country_id").change(function() {
		var nationality = $("#country_id option:selected").attr('data-nationality');
		var countryCode = $("#country_id option:selected").attr('data-country-code');	
		
		 $("#nationality").val(nationality);
		 $("#mobile").val(countryCode);
	
	});
	
	
	
	// same data pass
	
	$('#same').on('change', function(){
		var inputFields = ['#address', '#city', '#country_phone','#mobile', '#email'];
		$.each(inputFields, function(index, value){
		
			var inputValue = $(value).val();
			
			console.log(inputValue)
			
			if( inputValue != '' && $('#same:checked').val()) {
				$(value+'_permanent').val(inputValue).attr('readonly', 'readonly');
			} else {
				$(value+'_permanent').val('').removeAttr('readonly');
			}
		});
	});	
	
	// Password Count
	
    $("#password").keyup (function(){
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".first").text("");
		$(".first").removeClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	  } else if(len<=4){
	    $(".first").text("Very Weak");
		$(".first").addClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	   } else if(len<=8){
	    $(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");
		
	   } else if(len<=9){
	   $(".first").text("Strong");
	   $(".first").addClass("yellow");
	   $(".first").removeClass("green");
	   $(".first").removeClass("red");
	   }
	});
	
	// Password and confirm password match
	
    $('#submit').click(function(event){
    
        data = $('.password').val();
        var pass = data.length;
        
        if(pass < 1) {
            alert("Password cannot be blank");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('.password').val() != $('.conformpassword').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         
    });

  
                
	</script>    
  </body>
</html>