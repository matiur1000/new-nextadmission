<style type="text/css">
            .marquee,
            .marquee-with-options,
            .marquee-vert-before,
            .marquee-vert {
              width: 100%;
              overflow: hidden;
                        }
        </style>
        <div class="col-lg-3 content_box">  <!--col 3 Start-->   
	       
   <div  class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-bottom:5px;">Latest News :</div>
     <div class='marquee-with-options'>
     <div class="row"><!--row start--> 
		    <?php 
				//print_r($newsAndEventInfo);
				 $i = 0;
				 foreach ($newsAndEventInfo as $v){
				     //$newsId		= $v->id;
			         //$newsLink		= array('home','newsDetails', $newsId);
					$title = $v->title; 
			 ?>
			 <div class="col-lg-12 col-sm-6 col-xs-12" align="justify" style="padding-bottom:10px;">
				<div class="col-lg-4 col-xs-5">
					<a href="#">
					<img src="<?php echo base_url("Images/slide_image_resize/$v->image"); ?>" width="70" height="50"  alt=""  style="border: 1px solid #457DED;"/>
					</a>
				 </div>   
				<div class="col-lg-8 col-xs-7" style="padding:0px !important">
				 <div class="row" style="padding-left:5px;">
					<div class="col-lg-12" style="font-size:12px; text-align:justify">
					  <a class="news_style2" href="#"><?php echo $v->news_date; ?></a>
					   </br>
					   <?php echo $title; ?>
					  </br>
							<div class="text-left">
								<a class="news_style1" href="#">more</a>
							</div>	
					</div>
				 </div>
			   </div>
			  </div>
              
			 <?php } ?>
		 </div>	
		</div>

    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding:20px 0 15px 0">Recent Photo :</div>	
        <?php foreach($recentPhotoInfo as $v){ ?>
	      <div class="col-lg-6 col-xs-6" style="padding:0 0 0 3px">
		    <img src="<?php echo base_url("Images/photo_gallery/$v->image"); ?>" width="125" height="80" style="border:1px solid;"  />
		</div>  
	    <?php } ?>  
</div>  


<script>
$(function(){
    var $mwo = $('.marquee-with-options');
    $('.marquee').marquee();
    $('.marquee-with-options').marquee({
        //speed in milliseconds of the marquee
        speed: 12000,
        //gap in pixels between the tickers
        gap: 5,
        //gap in pixels between the tickers
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'up',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
        pauseOnHover: true
    });
    //Direction upward
    $('.marquee-vert').marquee({
        direction: 'up',
        speed: 1500
    });
    //pause and resume links
    $('.pause').click(function(e){
        e.preventDefault();
        $mwo.trigger('pause');
    });
    $('.resume').click(function(e){
        e.preventDefault();
        $mwo.trigger('resume');
    });
    //toggle
    $('.toggle').hover(function(e){
        $mwo.trigger('pause');
    },function(){
        $mwo.trigger('resume');
    })
    .click(function(e){
        e.preventDefault();
    })
});
</script>
	