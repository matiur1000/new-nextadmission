<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
 <style>
	body{ font-family: Tahoma; font-size:13px; background-color:#F2F2F2}
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}
 </style>
     <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	 
	 <script language="javascript" type="text/javascript" src="<?php echo base_url("resource/js/ajax.js"); ?>"></script>
     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>

      

  </head>
  <body>
        <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">&nbsp;</div> 
        	 <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
            <div class="row">
				<?php $this->load->view('genRegMidForm'); ?>  
				<?php $this->load->view('regRightSidebarPage'); ?>  
            </div>                           
       </div>
       <!--footer-->
        <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
	<script>
	
	   // field value Empty
		window.onload = function() {
			 $("#genReg").find("input[type=text], input[type=password], textarea").val("");
		 }
	
	    $('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		$("input[name='regType']").change(function() {
		   var typeValue = $(this).val();
		   
		    console.log(typeValue);		
		   if(typeValue =='organization'){
		      $("#organize").css("display", "block"); 
			  $("#other").css("display", "none"); 
		   }else{
		      $("#organize").css("display", "none"); 
			  $("#other").css("display", "block"); 
		   }
		   
		  	
		});
		   
		   
	   // Region Wise Country
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});
	
	
	$("#country_id_org").change(function() {
		var country_id = $("#country_id_org").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id_org").html(data);
			}
		});
		
	});
	
	
	
	$("#country_id2").change(function() {
		var country_id2 = $("#country_id2").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id2 },
			dataType : "html",
			success : function(data) {			
				$("#city_id2").html(data);
			}
		});
		
	});
	
	
	// Country Wise Nationality
	
	$(document).on("change", "#country_id", function() {
		var nationality = $("#country_id option:selected").attr('data-nationality');
		var countryCode = $("#country_id option:selected").attr('data-country-code');	
		
		 $("#nationality").val(nationality);
		 $("#mobile").val(countryCode);
	
	});
	
	
	$(document).on("change", "#country_id_org", function() {
	
		var countryCodeMob 	= $("#country_id_org option:selected").attr('data-country-code');
		var countryCodePhon = $("#country_id_org option:selected").attr('data-country-code-phon');	
		
		
		 $("#mobile_com2").val(countryCodeMob);
		 $("#phone_com2").val(countryCodePhon);
	
	});
	
	
	
	$("#country_id2").change(function() {
		var countryCodeMob2 = $("#country_id2 option:selected").attr('data-country-code');
		var countryCodePhon2 = $("#country_id2 option:selected").attr('data-country-code-phon');	
		
		 
		 $("#mobile_com1").val(countryCodeMob2);
		 $("#phone_com1").val(countryCodePhon2);
	
	});
	
	
	$("#any_more_campus").change(function() {
			var anyCampus = $("#any_more_campus").val();
			 console.log(anyCampus);
			 if(anyCampus == 'Yes'){	
			 $("#more").css({"display":"block"}); 		
			} else {
			$("#more").css({"display":"none"}); 
			}
		});
		
	// same data pass
	
	$('#same').on('change', function(){
		var inputFields = ['#addressGen', '#city', '#country_phone','#mobile', '#email2'];
		$.each(inputFields, function(index, value){
		
			var inputValue = $(value).val();
			
			console.log(inputValue)
			
			if( inputValue != '' && $('#same:checked').val()) {
				$(value+'_permanent').val(inputValue).attr('readonly', 'readonly');
			} else {
				$(value+'_permanent').val('').removeAttr('readonly');
			}
		});
	});	
	
	// Password Count
	
    $(".password").keyup (function(){
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".first").text("");
		$(".first").removeClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	  } else if(len<=4){
	    $(".first").text("Very Weak");
		$(".first").addClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	   } else if(len<=8){
	    $(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");
		
	   } else if(len<=9){
	   $(".first").text("Strong");
	   $(".first").addClass("yellow");
	   $(".first").removeClass("green");
	   $(".first").removeClass("red");
	   }
	});
	
	
	$(".conformpassword").keyup (function(){
	 var conpass = $(".conformpassword").val();
	 var Pass = $(".password").val();

	 if(conpass){

		  if(conpass != Pass){
		   $(".second").text("Your New Password and Confirm Password donot match!");
		   $(".second").addClass("red");
		   $(".second").removeClass("green");
		    
		  } else {
		   	$(".second").text("Password Match");
		   	$(".second").removeClass("red");
		    $(".second").addClass("green");
		  }

		} else {
		   $(".second").text("");
		   $(".second").removeClass("red");
		   $(".second").removeClass("green");
		}
	});
	
	
	
	// Password Count
	
    $(".password3").keyup (function(){
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".third").text("");
		$(".third").removeClass("red");
		$(".third").removeClass("yellow");
		$(".third").removeClass("green");
		
	  } else if(len<=4){
	    $(".third").text("Very Weak");
		$(".third").addClass("red");
		$(".third").removeClass("yellow");
		$(".third").removeClass("green");
		
	   } else if(len<=8){
	    $(".third").text("Good");
		$(".third").addClass("green");
		$(".third").removeClass("yellow");
		$(".third").removeClass("red");
		
	   } else if(len<=9){
	   $(".third").text("Strong");
	   $(".third").addClass("yellow");
	   $(".third").removeClass("green");
	   $(".third").removeClass("red");
	   }
	});
	
	
	$(".conformpassword3").keyup (function(){
	 var conpass = $(".conformpassword3").val();
	 var Pass = $(".password3").val();

	 if(conpass){

		  if(conpass != Pass){
		   $(".fourth").text("Your New Password and Confirm Password donot match!");
		   $(".fourth").addClass("red");
		   $(".fourth").removeClass("green");
		    
		  } else {
		   	$(".fourth").text("Password Match");
		   	$(".fourth").removeClass("red");
		    $(".fourth").addClass("green");
		  }

		} else {
		   $(".fourth").text("");
		   $(".fourth").removeClass("red");
		   $(".fourth").removeClass("green");
		}
	});
	// Password and confirm password match
	
    $('#submit').click(function(event){
    
        data = $('.password').val();
        var pass = data.length;
        
        if(pass < 1) {
            alert("Password cannot be blank");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('.password').val() != $('.conformpassword').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         
    });
	
	
	$("#user_id").blur (function(){
		 var userId = $("#user_id").val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userId : userId },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				$(".chk").text("This Email Already Exit!");
	   			$(".chk").addClass("red");
				} else {
				 $(".chk").text("");
	   			 $(".chk").removeClass("red");
				}
		    }
		 });
			
	});
	
	
	//EMAIL CHECK INSERT
				$("#user_id2").blur (function(){
						 var user_id   		= $("#user_id2").val();
						$.ajax(
							{
								url :"<?php echo site_url('registration/userEmailChk'); ?>",
								type: "POST",
								data : {user_id: user_id},
								success:function(data){
								
									if(data==1)
									{
										$(".chkEmail").text("This Email/Login Id is alredy exist.Please Try Another");
										$(".chkEmail").addClass("red");
										$('.regsub button[type="submit"]').attr("disabled", "disabled");
									}  else {
										$(".chkEmail").text("");
										$(".chkEmail").addClass("");
										$('.regsub button[type="submit"]').removeAttr("disabled", "disabled");
									}
																	
								}
							});
									
					});
  
                
	</script>    
  </body>
</html>