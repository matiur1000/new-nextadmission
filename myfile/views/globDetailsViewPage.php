
<div class="modal-header" style="border-bottom:2px solid #FF0000">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title" id="myModalLabel"><?php echo $globValue; ?></h3>
</div>
  
<div class="modal-body">
   <div class="row">
      <div class="col-md-8">
        <img src="<?php echo base_url('resource/images/globtest.png'); ?>" border="0"  usemap="#Map"/>

        <map name="Map" id="Map" class="area">
			<area id="asia" class="glob_asia" shape="poly" coords="315,96,311,100,307,104,299,105,295,102,291,94,288,86,284,84,282,80,284,72,277,71,274,70,275,65,293,66,303,66,300,62,300,56,304,51,303,45,305,39,302,36,303,32,305,28,312,30,317,23,322,17,336,17,348,21,357,21,366,22,393,25,411,25,419,36,415,36,407,37,399,40,394,40,391,40,396,46,402,52,402,56,402,58,398,66,395,70,387,73,393,82,394,89,387,94,381,94,375,96,383,105,382,109,376,111,369,114,378,122,379,130,379,133,372,127,367,114,369,107,362,102,358,98,351,96,348,96,345,103,345,105,345,110,350,115,346,115,339,113,336,105,334,101,331,95,323,92,312,91,304,86,301,86" alt="Asia" href="#" data-value="Asia" alt="Asia" title="Asia" data-toggle="tooltip" data-placement="top" >
			<area shape="poly" coords="271,174,281,162,286,151,289,142,289,133,302,117,294,112,284,99,279,86,267,81,257,81,251,75,239,73,229,77,226,83,224,92,221,101,221,109,225,114,233,117,247,120,254,127,260,142,257,145,256,149,259,162,266,174" alt="Africa" href="#">
			<area shape="poly" coords="420,184,431,171,440,164,433,153,425,148,422,145,416,145,411,144,403,144,400,149,394,151,383,156,380,162,382,171,382,178,384,181,397,179,403,176,410,183,413,189" alt="Australlia" href="#">
			<area shape="poly" coords="164,200,163,189,169,184,173,178,179,172,179,160,190,158,192,147,192,143,197,135,187,129,182,126,177,126,177,116,170,113,161,111,155,111,150,111,142,111,136,119,136,121,133,128,133,130,140,141,142,147,150,158,150,163,150,170,150,183,158,193,168,199,165,201" alt="South America" href="#">
			<area shape="poly" coords="127,103,126,98,122,95,116,95,114,88,119,86,126,82,131,82,138,83,144,76,148,68,162,64,169,59,167,55,171,45,173,43,172,41,165,34,156,36,155,45,148,47,146,41,146,36,150,30,157,29,144,21,129,24,119,23,104,23,94,23,81,25,67,30,67,35,83,36,88,38,89,48,92,56,92,57,91,65,94,69,94,84,97,87,106,98,117,101,126,103,132,103" alt="North America" href="#">
			<area shape="poly" coords="240,65,251,60,255,62,265,67,271,67,273,60,282,60,289,60,295,62,301,49,300,39,298,35,298,28,296,27,276,29,268,26,261,25,252,30,250,33,245,40,246,49,244,52,240,52" alt="Europ" href="#">
		</map>
		
      </div>
      
      <div class="col-md-4" style="height:300px; overflow:scroll">
          <span style="font-size:16px; font-weight:bold;"><h4><ins>CountryList</ins></h4></span>
           <?php
               foreach ($regionWiseCountryList as $v){ 
               	$globeLink  = array('home','countryWiseData', $v->country_name);
            ?>
             <a target="_blank" href="<?php echo site_url($globeLink);?>"><div class="col-md-12 text-left" style="padding-left:0px;"><?php echo $v->country_name ?></div></a>
           <?php } ?>

       </div>
   	
   </div>

		
</div>
		

<div class="modal-footer">
	<button class="btn btn-danger" data-dismiss="modal">Close</button>
</div>



