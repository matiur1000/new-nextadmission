<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}
	</style>
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				
			<div class="row">  <!--row Start-->
			   	<div class="col-lg-9">  <!--col 9 Start-->
					<div class="col-lg-3 padding-0">
						<?php $this->load->view('leftSidebarPage'); ?>
					</div>
													
					<div class="col-lg-9">
					   <form id="fullSearch" method="post" action="<?php echo site_url('home/searchAll') ?>">

							<div class="form-group margin-0">				
								<div class="input-group">
									<input type="text" name="search_all" id="search_all" class="form-control"  placeholder="Search by keyword" value="<?php echo $search_all ?>">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit" id="search"><i class="glyphicon glyphicon-search"></i> Search</button>
									</span>												
							    </div><!-- /input-group -->		
							</div>	


						    <?php 
						         if(!empty($country_id) || !empty($organize_id) || !empty($program_id)){  

						         	$style = "display:block";
						         }else{
                                    $style = "display:none";

						         }
						      ?>
							    <div id="advancedSearch" style="<?php echo $style ?>">
									<div class="col-lg-4 padding-0">
										<div class="form-group margin-0">
											<select class="form-control" id="country_id" name="country_id"  tabindex="1">
												<option value="" selected>Select Country</option>
												<?php foreach ($countryInfo as $v){ ?>
												<option <?php if($country_id == $v->id){ ?> selected ="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group margin-0">
											<select class="form-control" id="organize_id" name="organize_id"  tabindex="2" >
												<option value="" selected>Select Organization</option>
												<?php foreach ($orgInfo as $v){ ?>
												<option <?php if($organize_id == $v->id){ ?> selected ="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-4 padding-0">
										<div class="form-group margin-0">
											<select class="form-control" id="program_id" name="program_id"  tabindex="3">
												<option value="" selected>Select Program</option>
												<?php foreach ($allProgramName as $v){ ?>
												 <option <?php if($program_id == $v->id){ ?> selected ="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->programe_name; ?></option>
                                                <?php } ?>
											</select>
										</div>
									</div>
								</div>

						      
					   </form>
						
						<p><a class="advanced">Advanced Search</a></p>


										

						 <?php 
						     if(!empty($searchUserResult)){

						     foreach($searchUserResult as $v){
						     	if($v->user_type =="General user"){
						     	  $mobile  = $v->mobile;
						     	}else{
                                  $mobile  = $v->mobile_com;
						     	}
						  ?>

						  <div class="col-lg-6 searchStyle" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">		
							<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="77" height="77"  />	
							<p style="text-decoration:none;color:#0a81ce; font-size:16px; padding:0px;"><?php echo $v->name; ?></p>
							<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->user_type; ?></p>
							<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->mobile; ?></p>
						  </div>

						<?php } } else if(!empty($searchTitleResult)) { ?> 


						<?php 	
							$i = 0;
							foreach ($searchTitleResult as $v){
								if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';
								

								$postId			= $v->blog_id;
								$description	= $v->description;
								$desPart	    = substr($description, 0,325);
								$postLink		= array('commentPopup','index', $postId);

								if($i % 2 == 0) {
									$class = "padding-left-0";
								} else {
									$class = "padding-right-0";
								}

								$i++;
					  	?>
					  	<div class="col-lg-6 post <?php echo $class ?>" data-url="<?php echo site_url($postLink); ?>">		
							<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="58" height="58" />	
							<h5><a><?php echo $v->blog_title; ?></a></h5>
							<p><?php echo $desPart; ?></p>	 
							
							<a class="pull-right">Read More</a>
						</div>
						<?php
							} } else if(!empty($searchProgramResult)) { 
						?>



						<?php 	
							$i = 0;
							foreach ($searchProgramResult as $v){
								if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';
								
								$programLink    = array('home','organizeCourseDetails', $v->organize_programe_id, $v->programe_id);

								if($i % 2 == 0) {
									$class = "padding-left-0";
								} else {
									$class = "padding-right-0";
								}

								$i++;
					  	?>
					  	<a href="<?php echo site_url($programLink);?>">
					  	<div class="col-lg-6 searchStyle <?php echo $class ?>" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">		
							<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="58" height="58" />	
							<h5><a href="<?php echo site_url($programLink);?>"><?php echo $v->regName; ?></a></h5>
							<a style="color:#000000" href="<?php echo site_url($programLink);?>"><p><?php echo $v->program_name; ?></p></a>	 
							
							<a href="<?php echo site_url($programLink);?>" class="pull-right">Read More</a>
						</div>
						</a>
						<?php
							} } else if(!empty($searchAdResult)) { 
						?>


						<?php 	
							$i = 0;
							foreach ($searchAdResult as $v){
								if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';
								
								$adLink    = array('home','orgWiseAdvritismentView', $v->ad_id, $v->ad_user_id);

								if($i % 2 == 0) {
									$class = "padding-left-0";
								} else {
									$class = "padding-right-0";
								}

								$i++;
					  	?>
					  	<a href="<?php echo site_url($adLink);?>">
					  	<div class="col-lg-6 searchStyle <?php echo $class ?>" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">		
							<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="58" height="58" />	
							<h5><a href="<?php echo site_url($adLink);?>"><?php echo $v->regName; ?></a></h5>
							<a style="color:#000000" href="<?php echo site_url($adLink);?>"><p><?php echo $v->ad_title; ?></p></a>	 
							
							<a href="<?php echo site_url($adLink);?>" class="pull-right">Read More</a>
						</div>
						</a>

                       <?php
							} } else if(!empty($searchUserAdvancedResult)) { 
						?>



						<?php 	
							$i = 0;
							foreach ($searchUserAdvancedResult as $v){
								if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';
								
								$programLink    = array('home','organizeCourseDetails', $v->organize_programe_id, $v->programe_id);

								if($i % 2 == 0) {
									$class = "padding-left-0";
								} else {
									$class = "padding-right-0";
								}

								$i++;
					  	?>
					  	<a href="<?php echo site_url($programLink);?>">
					  	<div class="col-lg-6 searchStyle <?php echo $class ?>" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">		
							<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="58" height="58" />	
							<h5><a href="<?php echo site_url($programLink);?>"><?php echo $v->regName; ?></a></h5>
							<a style="color:#000000" href="<?php echo site_url($programLink);?>"><p><?php echo $v->program_name; ?></p></a>	 
							<a href="<?php echo site_url($programLink);?>" class="pull-right">Read More</a>
						</div>
						</a>

                        <?php } } else if(!empty($searchAdvancedOrggENResult)) { ?> 
						 <?php 
						    

						     foreach($searchAdvancedOrggENResult as $v){
						     	if($v->user_type =="General user"){
						     	  $mobile  = $v->mobile;
						     	}else{
                                  $mobile  = $v->mobile_com;
						     	}
						  ?>

						  <div class="col-lg-6 searchStyle" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">		
							<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="77" height="77"  />	
							<p style="text-decoration:none;color:#0a81ce; font-size:16px; padding:0px;"><?php echo $v->name; ?></p>
							<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->user_type; ?></p>
							<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->mobile; ?></p>
						  </div>

						<?php } } else{ ?> 


                           <span style="color:#FF0000">Sorry result not fund!</span>

						 <?php } ?> 	
						
					</div>						   
		        </div>  <!--col 9 end--> 
				     
				<?php $this->load->view('rightSidebarPage'); ?>                 
				<!--col 3 end--> 
		  	</div>

             <div class="row">&nbsp;</div>     
              <div class="row">
              	<div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;">Advertisement</div>
              </div>  
			  <?php $this->load->view('advertisementManagePage'); ?>
             <div class="row">&nbsp;</div>     
              <div class="row">
			    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
			     <?php $this->load->view('newsEventManagePage'); ?>
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>