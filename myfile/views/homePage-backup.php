<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
    <script src="<?php echo base_url('resource/source/marquee.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
	  .area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
	</style>
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				
			<div class="row">  <!--row Start-->
			   	<div class="col-lg-9">  <!--col 9 Start-->
					<div class="col-lg-3 padding-0">
						<?php $this->load->view('leftSidebarPage'); ?>
					</div>
													
					<div class="col-lg-9 padding-0">

						<!-- <div class="col-lg-7" align="left" style="padding-left:13px;">
							<input type="text" name="search_all" id="search_all" class="form-control"  placeholder="Search by keyword"> 										      
						</div>
						<div class="col-lg-2" align="left" style="padding-left:2px;">
							<button type="submit" id="search" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Search</button>  
						</div>
						
						<div class="row" style="padding-bottom:5px;">
							<div class="col-lg-3"> </div>
							<div class="col-lg-7" align="left" style="padding-top:0px; font-size:11px; padding-bottom:5px;">
								<span class="advanced" style="cursor:pointer; color:#337AB7;">Advanced Search</span>								      
							</div>
							<div class="col-lg-2" align="left" ></div>
						</div>
						
						<div id="advancedSearch" class="row" style="padding-bottom:10px; display:none">
							<div class="col-lg-3"></div>
							<div class="col-lg-3" align="left">
								<select class="form-control" id="country_id" name="country_id"  tabindex="1">
									<option value="" selected>Select Country</option>
									<?php foreach ($countryInfo as $v){ ?>
									<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-lg-3" align="left" >
								<select class="form-control" id="organize_id" name="organize_id"  tabindex="2" >
									<option value="" selected>Select Organization</option>
									<?php foreach ($orgInfo as $v){ ?>
									<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-lg-3" align="left" >
								<select class="form-control" id="program_id" name="program_id"  tabindex="3">
									<option value="" selected>Select Program</option>
								</select>
							</div>
						</div> -->

						<div id="searchOrganization">
							<?php //$this->load->view('middlePage'); ?>
						</div>
					</div>						   
		        </div>  <!--col 9 end--> 
				     
				<?php $this->load->view('rightSidebarPage'); ?>                 
				<!--col 3 end--> 
		  	</div>

             <div class="row">&nbsp;</div>     
              <div class="row">
              	<div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;" title="South America" data-toggle="tooltip" data-placement="top">Advertisement</div>
              </div>  
			  <?php $this->load->view('advertisementManagePage'); ?>
             <div class="row">&nbsp;</div>     
              <div class="row">
			    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
			     <?php $this->load->view('newsEventManagePage'); ?>
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
	   <div id="comment_popup" class="comment_content" style="margin-left:350px; width:615px; padding-bottom:0px; padding-left:5px; padding-top:10px;"></div><!-- *******  Student Edit form **********-->
       <div id="fade" class="black_overlay" align="center"></div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
		
		 $(document).on("click", ".advanced", function(){
			$("#advancedSearch").toggle(); 
		 });
		 
		 //COUNTRY WISE SEARCH 
			$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});
			
		});	
			
			
			
			$("#country_id").change(function() {
				var country_id = $("#country_id").val();	
				console.log(country_id);
					
				$.ajax({
					url: "<?php echo site_url('home/searchAll') ?>",
					type : "POST",
					data: {country_id : country_id},
					dataType : "html",
					success : function(data) {			
						$("#searchOrganization").html(data);
					}
				});
				
				
			});	
			
			
			
			//ORGANIZATION WISE PROGRAM 
				$("#organize_id").change(function() {
				var organize_id = $("#organize_id").val();	
				console.log(organize_id);
					
				$.ajax({
					url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
					type : "POST",
					data : { id : organize_id },
					dataType : "html",
					success : function(data) {			
						$("#program_id").html(data);
					}
				});
				
			});	
			
			
	
	 	$("#nort")
		   	.mouseover(function() {
				$( ".effict" ).html( "<img  src='<?php echo base_url('resource/images/arrow.gif'); ?>' /> <p>North America</p>" );
		  	})
		  	.mouseout(function() {
				$( ".effict" ).html("");			 
		  	});
		  
		  //South America Effict
	
		$( "#south" )
		   	.mouseover(function() {
				$( ".effict" ).html( "<img  src='<?php echo base_url('resource/images/arrow.gif'); ?>' /> <p>South America</p>" );
		  	})
		  	.mouseout(function() {
				$( ".effict" ).html("");	
		  	});
		  
		  //Asia Effict
	
		$( "#africa" )
			.mouseover(function() {				
			 	$( ".effict" ).html( "<img  src='<?php echo base_url('resource/images/arrow.gif'); ?>' /> <p>Africa</p>" );
			})
			.mouseout(function() {
				$( ".effict" ).html("");	
			});
			  
		  //Africa Effict
	
		$( "#asia" )
			.mouseover(function() {				
				$( ".effict" ).html( "<img  src='<?php echo base_url('resource/images/arrow.gif'); ?>' /> <p>Asia</p>" );
			})
			.mouseout(function() {
				$( ".effict" ).html("");	
			});
			  
			  //Austrlia Effict
	
		$( "#australia" )
			.mouseover(function() {
				$( ".effict" ).html( "<img  src='<?php echo base_url('resource/images/arrow.gif'); ?>' /> <p>Australia</p>" );
			})
			.mouseout(function() {
				$( ".effict" ).html("");	
			});
			  
			  //Europ Effict
	
		$( "#europ" )
			.mouseover(function() {
				$( ".effict" ).html( "<img  src='<?php echo base_url('resource/images/arrow.gif'); ?>' /> <p>Europ</p>" );
			})
			.mouseout(function() {
				$( ".effict" ).html("");	
			});
			  
	  
	//Search all keyword
	
		$('#search').on('click', function(e) {
		 var search_all = $("#search_all").val();   
		
		$.ajax({
			url: "<?php echo site_url('home/searchAll') ?>",
			type : "POST",
			data: {search_all : search_all},
			dataType : "html",
			success : function(data) {			
				$("#searchOrganization").html(data);
			}
		});
		
		
		
	});	   
	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	
	</script>
	
    
  </body>
</html>