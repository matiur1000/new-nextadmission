<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
        <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
  <script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
 
    
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		     <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
                           <div class="row">&nbsp;</div>  		
                            <div class="row">
								<div class="col-lg-12 col-xs-3" align="justify">
						   			<div style="float:left; padding-right:20px;">
						   			<img class="img-rounded" style="border-radius:4px;" src="<?php echo base_url("Images/News_image/$UserNewsInfo->image"); ?>"
									 width="300" height="220"  alt=""/></div>
						   			<p class="text_app"><span style="font-size:24px; color:#000"> <?php echo $UserNewsInfo->title; ?></span><br/>
                                       <span style="color:#0e73a9"> <?php echo $UserNewsInfo->date; ?></span><br/>
									<?php echo $UserNewsInfo->description; ?></p>
								</div>
							 </div>	
							 			  
						   </div>
				        </div>             <!--col 9 end-->
			     </div>  
           <div class="row">
          <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
           <?php $this->load->view('newsEventManagePage'); ?>
            </div>                <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
       </div>


       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <!--SHARE THIS-->
	<script type="text/javascript">stLight.options({publisher: "7521b38c-5f2b-4808-b7b5-8057deecb289", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <script>
       $('.goto-news').on('click', function(){
      console.log($('#news'));
      console.log($('#news').offset().top);
      $('body,html').animate({
        scrollTop: ($('#news').offset().top - 50),
        }, 1200
      );
    }); 


       $(function(){
          var $mwo = $('.marquee-with-options');
          $('.marquee').marquee();
          $('.marquee-with-options').marquee({
              //speed in milliseconds of the marquee
              speed: 9000,
              //gap in pixels between the tickers
              gap: 5,
              //gap in pixels between the tickers
              delayBeforeStart: 0,
              //'left' or 'right'
              direction: 'up',
              //true or false - should the marquee be duplicated to show an effect of continues flow
              duplicated: true,
              //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
              pauseOnHover: true
          });
          //Direction upward
          $('.marquee-vert').marquee({
              direction: 'up',
              speed: 1500
          });
          //pause and resume links
          $('.pause').click(function(e){
              e.preventDefault();
              $mwo.trigger('pause');
          });
          $('.resume').click(function(e){
              e.preventDefault();
              $mwo.trigger('resume');
          });
          //toggle
          $('.toggle').hover(function(e){
              $mwo.trigger('pause');
          },function(){
              $mwo.trigger('resume');
          })
          .click(function(e){
              e.preventDefault();
          })
      });



    </script>
    
  </body>
</html>