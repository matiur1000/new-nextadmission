<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				
			<div class="row">  <!--row Start-->
			   	<div class="col-lg-9">  <!--col 9 Start-->
					<div class="col-lg-3 padding-0">
						<?php $this->load->view('leftSidebarPage'); ?>
					</div>
													
					<div class="col-lg-9">
					   <form id="fullSearch" method="post" action="<?php echo site_url('home/searchAll') ?>">

							<div class="form-group margin-0">				
								<div class="input-group">
									<input type="text" name="search_all" id="search_all" class="form-control"  placeholder="Search by keyword">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit" id="search"><i class="glyphicon glyphicon-search"></i> Search</button>
									</span>												
							    </div><!-- /input-group -->		
							</div>	 
						    
						    <div id="advancedSearch" style="display:none">
								<div class="col-lg-4 padding-0">
									<div class="form-group margin-0">
										<select class="form-control" id="country_id" name="country_id"  tabindex="1">
											<option value="" selected>Select Country</option>
											<?php foreach ($countryInfo as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group margin-0">
										<select class="form-control" id="organize_id" name="organize_id"  tabindex="2" >
											<option value="" selected>Select Organization</option>
											<?php foreach ($orgInfo as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-lg-4 padding-0">
									<div class="form-group margin-0">
										<select class="form-control" id="program_id" name="program_id"  tabindex="3">
											<option value="" selected>Select Program</option>
											<?php foreach ($allProgramName as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->programe_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
						
					   </form>
						
						<p><a class="advanced">Advanced Search</a></p>
										

						<div id="searchOrganization" class="blog-post">
							<?php $this->load->view('middlePage'); ?>
						</div>
					</div>						   
		        </div>  <!--col 9 end--> 
				     
				<?php $this->load->view('rightSidebarPage'); ?>                 
				<!--col 3 end--> 
		  	</div>

             <div class="row">&nbsp;</div>     
              <div class="row">
              	<div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;">Advertisement</div>
              </div>  
			  <?php $this->load->view('advertisementManagePage'); ?>
             <div class="row">&nbsp;</div>     
              <div class="row">
			    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
			     <?php $this->load->view('newsEventManagePage'); ?>
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>