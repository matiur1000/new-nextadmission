<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

   <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				
				</style>
        <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
		<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>     
			 <div class="row">
			   <div class="col-lg-12" align="center">
			   
			         
							  
			       
			   </div>
			 </div>    
			 
			 <div class="row">&nbsp;</div> 
			   
              <div class="row">  <!--row Start-->           
		      <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-9">  <!--col 9 Start--> 
					       <div class="welll welll-lg" style="padding-bottom:0px;">
							  	<div class="row" align="center">
									
									    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding:10px 0px 15px 12px; text-align:left"> Contact us</div>
									     <div class="col-lg-12" style="padding-left:15px; text-align:justify; line-height:normal">
										       <?php echo $orgContactInfo->details; ?>
										 </div>
						   		</div>
								
								<div class="row" style="padding-top:20px;">
								    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding:10px 0px 15px 12px; text-align:left"> Query form : </div>
									<div class="col-lg-12" style="padding-top:40px;">
									<form action="<?php echo site_url('home/webQuery/'.$organizeId); ?>" method="post"  class="form-horizontal">
									   <div class="col-lg-12">
											  <div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
												<div class="col-sm-7">
												  <input type="text" class="form-control" id="name" name="name" placeholder="Name">
												</div>
											  </div>
									   </div>
									   <div class="col-lg-12">
											 <div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Email</label>
												<div class="col-sm-7">
												  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
												</div>
											</div></div>
											
									   <div class="col-lg-12"><div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Subject</label>
												<div class="col-sm-7">
												  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
												</div>
											  </div></div>
											  
									   <div class="col-lg-12"><div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Message</label>
												<div class="col-sm-7">
												  <textarea rows="4" class="form-control" name="message" id="message" required></textarea>
												</div>
											  </div></div>
										 
										 
										<div class="col-lg-12 text-center"><div class="form-group">
											<div class="col-sm-offset-2 col-sm-7">
											  <button type="submit" class="btn btn-primary">Send query</button>
											</div>
										  </div></div> 
										 
									   </form>
									   
									</div>
								</div>
						   </div>
				        </div>             <!--col 9 end--> 
				     
				     <?php $this->load->view('orgRightSidebarPage'); ?>                 <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	   </div>                   <!--col end-->  
		     </div>
             <div class="row">&nbsp;</div>     
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    
  </body>
</html>