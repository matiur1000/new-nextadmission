<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

   <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<style type="text/css">
	  body{
	    font-size:12px;
		line-height:10px
	  
	  }
				
				
	</style>
        <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
		<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>     
			 <div class="row">
			   <div class="col-lg-12" align="center">
			   
			         
							  
			       
			   </div>
			 </div>    
			 
			 <div class="row">&nbsp;</div> 
			   
              <div class="row">  <!--row Start-->           
		      <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-9">  <!--col 9 Start--> 
					       <div class="welll welll-lg" style="padding-bottom:0px; height:1000px;">
							  	<div class="row" align="center">
									<div class="col-lg-12" align="center">
									  <div class="row">
									     <div class="col-lg-12" style="font-size:20px; padding-bottom:10px; text-align:left">
										      <?php echo $orgCouWiseDetail->programe_name; ?>
										 </div>
									     <div class="col-lg-12" style="padding-left:15px; text-align:justify; line-height:normal">
										       <?php echo $orgCouWiseDetail->programe_details; ?>
										 </div>
									   </div>  
									   
									   
									  
									</div>				
						   		</div>
						   </div>
				        </div>             <!--col 9 end--> 
				     
				     <?php $this->load->view('orgRightSidebarPage'); ?>                 <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	   </div>                   <!--col end-->  
		     </div>
             <div class="row">&nbsp;</div>     
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    
  </body>
</html>