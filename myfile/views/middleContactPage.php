   <style type="text/css">
     .contactStyle ul{
      padding:0px;
    }
    .contactStyle ul li{
      display:block;
      padding-bottom:4px;
    }

    .header {
        color: #36A0FF;
        font-size: 27px;
        padding: 10px;
    }

    .bigicon {
        font-size: 35px;
        color: #36A0FF;
    }

   </style>

    <link href="<?php echo base_url('resource/source/app.css'); ?>" rel="stylesheet">
    <div class="row margin-bottom-30">
        <div class="col-md-12 mb-margin-bottom-30" style="text-align:left">

            <div class="col-md-3 contactStyle" style="padding:20px 0 0 0">
                  
                <ul>
                      
                      <li style="height:24px; font-size:16px; list-style:none">Dhaka Office</li>
                      <li> 31 Malek Tower (9th floor- 2nd lift)</li>
                      <li> Farmgate, Dhaka, Bangladesh. </li>               
                      <li> Mobile: </li>
                      <li> +880 1911 342308, +880 1780 300059 </li>
                      <li> Email:smtbangladesh@yahoo.com</li>
                      <li> &nbsp; </li> 
                      <li style="height:24px; font-size:16px; list-style:none"> Chittagong Office </li>
                      <li> Walikhan  Mansion, 603 SK Mujib Road (3rd floor), </li>
                      <li> Agrabad, Choumuhuni Moar, Chittagong </li>
                      
                      <li> Mobile: </li> 
                      <li> +880 1911 342308, +880 01715 015972</li>
                      <li> Email:smtbangladesh@yahoo.com</li>
                      
                      
                    </ul>

                    
             </div>
            <div class="col-md-9">
              <div id="map" class="map map-box map-box-space margin-bottom-40">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.7392982609063!2d90.38725860000001!3d23.756674349999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8a426199b0d%3A0x6a2c655d06c88ec9!2sFarmgate%2C+Dhaka+1215!5e0!3m2!1sen!2sbd!4v1444028943655" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                 
             </div>
      
            </div>



            <div class="col-md-12" style="padding:0px;">
            <div class="well well-sm">
                <form action="<?php echo site_url('home/contactUsAction');?>" class="form-horizontal" method="post">
                    <fieldset>
                        <legend class="text-center header">QUERY FORM</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="name" name="name" type="text" placeholder="Full Name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">
                            </div>
                        </div>


                         <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="mobile" name="mobile" type="text" placeholder="Mobile" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="subject" name="subject" type="text" placeholder="Subject" class="form-control">
                            </div>
                        </div>

                        

                        

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
     </div><!--/col-md-9-->
</div><!--/row-->        
