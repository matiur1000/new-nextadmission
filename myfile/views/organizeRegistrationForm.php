<div class="col-lg-9">
	<div class="welll welll-lg" style="height:780px;"> 
	<div class="row">
	   <div class="col-lg-12">
	     <span style="color:#000000; font-size:30px;">Create Account</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0;">
		<span style="color:#333333; font-size:20px;">Account Information</span>
	  </div>
	</div>
	<div class="row">&nbsp;</div> 
		<div class="row">
		 <form id="regForm" action="<?php echo site_url('registration/orgRegStore'); ?>" method="post" enctype="multipart/form-data">
			<div class="col-lg-12">
			<div class="col-lg-6">
			  <div class="form-group">
					<input type="text" class="form-control" id="account_name" name="account_name" 
					placeholder="Account Name" tabindex="1" value="<?php echo set_value('account_name'); ?>">
					<?php echo form_error('account_name'); ?>
				</div>
				
				<div class="form-group">
					<input type="email" class="form-control" id="user_id" name="user_id" placeholder="Email / Login id" tabindex="2" 
					value="<?php echo set_value('user_id'); ?>"><?php echo form_error('user_id'); ?><span class="chkEmail"></span>
				</div>
				
				
				
			</div>
			<div class="col-lg-6">
			  <div class="form-group">
					<input type="text" class="form-control" id="organizationname" name="organizationname" placeholder="Organization Name" tabindex="1" >
				</div>
			
			<div class="row">
			    <div class="col-lg-6" style="padding-right:3px;">
				   <div class="form-group">
					<input type="password" class="form-control password" id="password" name="password" placeholder="Password" tabindex="2" value="<?php echo set_value('password'); ?>"><?php echo form_error('password'); ?>
					<span class="second"></span>
				    </div>
				  </div>
				<div class="col-lg-6" style="padding-left:3px;">
				  <div class="form-group">
					<input type="password" class="form-control conformpassword" id="con_password" name="con_password" placeholder="Confirm Password" tabindex="2">
					<span class="second"></span>
				  </div> 
				</div>
			</div>
				
			  
			  
			  
			</div>
				
				<div class="row">
				  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0;">
					<span style="color:#333333; font-size:20px;">Organization Details</span>
				  </div>
				</div>
				<div class="row">&nbsp;</div>
			
			<div class="col-lg-6">
			
                <div class="form-group">
					<div>
						<select class="form-control" id="country_id" name="country_id"  tabindex="2" >
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
                         
					   </select>
					 
					</div>
				</div>
                
                <div class="form-group">
					<div>
					   <select class="form-control" id="city_id" name="city_id"  tabindex="3">
							<option value="" selected>Sellect City Name</option>
					   </select>
					</div>
				</div>
                <div class="form-group">
					   <textarea class="form-control" name="address" id="address" cols="60" rows="" placeholder="Holding Details"></textarea>
				</div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Phone &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="phone_com2" name="phone_com2" placeholder="Phone" tabindex="2">
                      </div>
                	</div>
                </div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Mobile &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="mobile_com2" name="mobile_com2" placeholder="Mobile" tabindex="2">
                      </div>
                	</div>
                </div>
                
			   	<div class="form-group">
					<input type="text" class="form-control" id="email_com" name="email_com" placeholder="Email" tabindex="1"> 
				</div>
                <div class="form-group">
					<input type="text" class="form-control" id="location" name="location" placeholder="Location" tabindex="1">
				</div>
                
				<div class="form-group">
						<input name="founded" type="text" id="founded" class="form-control"
						 onClick="('#founded').datepicker({dateFormat: 'dd/mm/yy'});" placeholder="Founded Year"/>
				</div>
				
				
				<div class="row">&nbsp;</div>
                
            </div>
            
            <div class="col-lg-6">
               <div class="row">
                  <div class="col-lg-5" style="margin-top:5px; font-size:15px;">Any More Branches?</div>
                  <div class="col-lg-7" style="padding-left:0px;">
                      <div class="form-group">
                           <select class="form-control" id="any_more_campus" name="any_more_campus"  tabindex="2" onchange="nextSteap();">
                                <option value="" selected>Sellect Yes/No</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                           </select>
					      </div>
                	</div>
                </div>
                
               <div id="more" style="display:none;">  <!--Hide Start-->
					<div class="form-group">
					<input type="text" class="form-control" id="campus_name" name="campus_name" placeholder="Campus One" tabindex="1"> 
					
				</div>
                
                <div class="form-group">
					<div>
						<select class="form-control" id="country_id2" name="country_id2"  tabindex="2" >
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
					   </select>
					 
					</div>
				</div>
                
                <div class="form-group">
					<div>
					   <select class="form-control" id="city_id2" name="city_id2"  tabindex="3">
							<option value="" selected>Sellect City Name</option>
					   </select>
					</div>
				</div>
                <div class="form-group">
					   <textarea class="form-control" name="address2" id="address2" cols="60" rows="" placeholder="Holding Details"></textarea>
				</div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Phone &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="phone_com1" name="phone_com1" placeholder="Phone" tabindex="2"> 
                    </div>
                	</div>
                </div>
                
                <div class="row">
                  <div class="col-lg-3" style="margin-top:5px; font-size:15px;">Mobile &nbsp;&nbsp;:</div>
                  <div class="col-lg-9" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="text" class="form-control" id="mobile_com1" name="mobile_com1" placeholder="Mobile" tabindex="2">
                      </div>
                	</div>
                </div>
                
			   	<div class="form-group">
					<input type="text" class="form-control" id="email_com2" name="email_com2" placeholder="Email" tabindex="1">
					
				</div>
                <div class="form-group">
					<input type="text" class="form-control" id="location_by_google" name="location_by_google" placeholder="Location" tabindex="1"> 
				</div>
				<div class="form-group">
						<input name="founded2" type="text" id="founded2" class="form-control"
						 onClick="('#founded2').datepicker({dateFormat: 'dd/mm/yy'});" placeholder="Founded Year"/>
				</div>
				
				 </div>  <!--Hide Stop-->
                
            </div>
			
			
		    </div> 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12 regsub" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-primary  center-block" tabindex="1">Create Account</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	</div>                
</div>