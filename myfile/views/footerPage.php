<link href="<?php echo base_url('resource/style.css'); ?>" rel="stylesheet">
<div class="footer">
	  <div class="footer-grids">
		  <div class="container">
			  <div class="col-md-3 col-xs-6 footer-grid">
					
					<ul>
						 <?php 
							 $i = 0;
							 foreach ($footerMenuInfo1 as $v1){
							 	$menuId			= $v1->id;
								$footerMenuLink	= array('menuDetails','index', $menuId);
						?>
				            <li style="list-style:none;"><a href="<?php echo site_url($footerMenuLink);?>"  class="footerLink"><?php echo $v1->menu_name; ?></a></li>
						<?php } ?>
					</ul>
			  </div>
			 <div class="col-md-3 col-xs-6 footer-grid">
				
				  <ul>
						<?php 
							//print_r($footerMenuInfo2);
							 $i = 0;
							 foreach ($footerMenuInfo2 as $v2){
							 	$menuId			 = $v2->id;
								$footerMenuLink2 = array('menuDetails','index', $menuId);
						?>
				            <li style="list-style:none;"><a href="<?php echo site_url($footerMenuLink2);?>"  class="footerLink"><?php echo $v2->menu_name; ?></a></li>
						<?php } ?>
				  </ul>
			 </div>
			 <div class="col-md-3 col-xs-6 footer-grid">
					
					<ul>
						<?php 
							//print_r($footerMenuInfo3);
							 $i = 0;
							 foreach ($footerMenuInfo3 as $v3){
							 	$menuId			 = $v3->id;
								$footerMenuLink3 = array('menuDetails','index', $menuId);
						?>
				            <li style="list-style:none;"><a href="<?php echo site_url($footerMenuLink3);?>"  class="footerLink"><?php echo $v3->menu_name; ?></a></li>
						<?php } ?>
					</ul>
			 </div>
			 <div class="col-md-3 col-xs-6 footer-grid contact-grid">
					
					<ul>
						<?php 
							//print_r($footerMenuInfo4);
							 $i = 0;
							 foreach ($footerMenuInfo4 as $v4){
							 	$menuId			 = $v4->id;
								$footerMenuLink4 = array('menuDetails','index', $menuId);
						?>
				            <li style="list-style:none;"><a href="<?php echo site_url($footerMenuLink4);?>"  class="footerLink"><?php echo $v4->menu_name; ?></a></li>
						<?php } ?>
					
				</ul>
				  <ul class="social-icons">
						<li><a href="#"><span class="facebook"> </span></a></li>
						<li><a href="#"><span class="twitter"> </span></a></li>
						<li><a href="#"><span class="thumb"> </span></a></li>
					</ul>
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<div class="copy" style="background:#000000;">
		 <p style="color:#CCCCCC;"><?php echo $headerBasicInfo->title; ?></p>
 </div>