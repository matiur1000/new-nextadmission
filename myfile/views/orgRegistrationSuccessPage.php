<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
 <style>
	body{ font-family: Tahoma; font-size:13px;}
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}
 </style>
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
  <script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
  <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
        	 <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
            <div class="row">
				<?php $this->load->view('middleOrgRegistrationSuccessPage'); ?>  
				
            </div>                           
       </div>
       <!--footer-->
        <?php $this->load->view('footerPage'); ?>
   
   
	    
  </body>
</html>