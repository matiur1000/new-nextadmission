<link href="<?php echo base_url('resource/source/menu/style.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('resource/source/menu/jquerymenu.js'); ?>"></script>
		
    
<div id="ddmenuMst">
    <div class="menu-icon"></div>
    <ul style="background:#d9e3ef; text-align:start;">
       <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('home'); ?>">Home</a></span></li>
       <?php		   
	        foreach($menuInfo as $v){
			$menuId		= $v->id;
			$menuLink		 = array('menuDetails','index', $menuId);
        ?>
        <li class="full-width">
        
            <span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase"><a style="text-decoration:none" href="<?php echo site_url($menuLink); ?>"><?php echo $v->menu_name; ?></a></span>
             <?php if($v->sub){ ?>
              <i class="caret" style="color:#000;"></i>
                <div class="dropdown">
                    <div class="dd-inner" style="background:#FFF !important; text-align:left; border:0px;">
                     
    					<?php 
                        //print_r($allUserPostInfo);
                        $a = 0;
                        $b = 4;		
                        foreach($v->sub as $s){
                        $subCatId		 = $s->id;
                        $Name	 	 	 = $s->sub_menu_name;
                        $subLink		 = array('subMenuDetails','index',$menuId ,$subCatId);	
                        
                        if($a == 0) echo "<div class='row'>";
                        if($a <= $b)  
                        ?>
                        <div class="column" style="font-size:13px; font-weight:bold; text-transform:uppercase; text-align:left; width:400px;">
                           <a style="text-decoration:none;" href="<?php echo site_url($subLink);?>"><?php echo $Name; ?></a>
                           
    						<?php foreach($s->deeperSub as $dv){
                               $dsubCatId		 = $dv->id;
                               $deepSubLink		 = $subLink		 = array('deepSubMenuDetails','index',$menuId ,$subCatId,$dsubCatId);	
                            ?>	
                            <div style="font-size:12px; font-weight:normal; text-transform:uppercase;">
                            <a style="text-decoration:none" href="<?php echo site_url($deepSubLink); ?>"><i style="font-size:10px;" class="glyphicon glyphicon-menu-right"></i><?php echo $dv->deeper_sub_menu_name; ?></a>
                               
                            </div>
                             <?php } ?>
                        </div>
    					<?php 	$a++;	
                        if($a == $b) echo "</div>";				
                        if($a == $b) $a=0; } ?> 
                        
                    </div>
                    
                    
                </div>

            <?php } ?>
        </li>

         <?php } ?>
       <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('home/recruterAd'); ?>">Recruiter's Ad</a></span></li>
	   <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('home/contactUS'); ?>">Contact Us</a></span></li>

    </ul>
</div>