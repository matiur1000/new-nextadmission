<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}

				.adStyle{
					cursor: pointer;
					box-shadow: 0px 0px 5px 0px #fff;
					padding:10px !important
				}
				
				.adStyle img{
					background-color: #fff;
				    border: 1px solid #ddd;
				    border-radius: 4px;
				    display: inline-block;
				    line-height: 1.42857;
				    margin-right: 10px;
				    padding: 4px;
				    transition: all 0.2s ease-in-out 0s;
				}
				.adStyle h5{
					font-size: 16px;
					font-weight: bold;
					margin-top: 0;
					margin-bottom: 5px;
				}


				.adStyle p{
					text-align: justify; 
				}

				
				
				</style>
        <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
		<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
        
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div> 
			 
			   <div class="row" style=" padding-left:10px; padding-right:15px;">
	 			<div class="col-lg-12" style="border:solid 1px #e3e3e3; border-radius:3px; padding-top:0px; height:300px">
					  <div class="row"> <!--row start--> 
						<div class="col-lg-12 adStyle">		
								<img src="<?php echo base_url("Images/Add_image/$viewAdInfo->add_image"); ?>" class="pull-left" width="200" height="130" />
								<h5><?php echo $viewAdInfo->title; ?></h5>
								<h6><span style="font-weight:bold">Deadline : </span> <?php echo $viewAdInfo->deadline_date; ?></h6>
								<p style="text-align:justify"><?php echo $viewAdInfo->description; ?> </p>	 
						</div>
					   </div>
				 </div>
			  </div>   
			  
			  <div class="row">&nbsp;</div>   
			  
           </div>
          
         
		
	    <?php $this->load->view('footerPage'); ?>

	     
    
  </body>
</html>