<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
 
	
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
     <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	 <script src="<?php echo base_url('resource/js/menu_script.js'); ?>"></script>
	 <script language="javascript" type="text/javascript" src="<?php echo base_url("resource/js/ajax.js"); ?>"></script>
     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

    <script src="<?php echo base_url('resource/js/script.js'); ?>"></script>
	<!--CALENDER-->
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-1.7.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-1.8.16.custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-timepicker-addon.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-sliderAccess.js'); ?>"></script>
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('resource/css/jquery-ui-1.8.16.custom.css'); ?>" />
	<script type="text/javascript">
		
		$(function(){
			
	
			$('.example-container > pre').each(function(i){
				eval($(this).text());
			});
		});
		
	</script>

<!--------------------------------------END CALENDER------------------------------------>

    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->

  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
        	 <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
            <div class="row">
				<?php $this->load->view('organizeRegistration2Form'); ?>  
				<?php $this->load->view('rightSidebarPage'); ?>  
            </div>                           
       </div>
       <!--footer-->
        <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <!--SHARE THIS-->
	<script type="text/javascript">stLight.options({publisher: "7521b38c-5f2b-4808-b7b5-8057deecb289", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<script type="text/javascript">
		$('#date_of_birth').datepicker({dateFormat: 'dd/mm/yy'});
   </script>
    <script>
    var options={ "publisher": "7521b38c-5f2b-4808-b7b5-8057deecb289", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "email", "print"]}};
    var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
	</script>
	<script>
	   // Region Wise Country
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});
	
	// Country Wise Nationality
	
	$("#country_id").change(function() {
		var nationality 	= $("#country_id option:selected").attr('data-nationality');
		var countryCodeMob 	= $("#country_id option:selected").attr('data-country-code');
		var countryCodePhon = $("#country_id option:selected").attr('data-country-code-phon');	
		
		
		 $("#mobile_com2").val(countryCodeMob);
		 $("#phone_com2").val(countryCodePhon);
	
	});
	
	
	// Region Wise Country
		$("#region_id2").change(function() {
			var region_id2 = $("#region_id2").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id2 },
				dataType : "html",
				success : function(data) {			
					$("#country_id2").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id2").change(function() {
		var country_id2 = $("#country_id2").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id2 },
			dataType : "html",
			success : function(data) {			
				$("#city_id2").html(data);
			}
		});
		
	});
	
	
	// Country Wise Nationality
	
	$("#country_id2").change(function() {
		var nationality2 	= $("#country_id option:selected").attr('data-nationality');
		var countryCodeMob2 = $("#country_id option:selected").attr('data-country-code');
		var countryCodePhon2 = $("#country_id option:selected").attr('data-country-code-phon');	
		
		 
		 $("#mobile_com1").val(countryCodeMob2);
		 $("#phone_com1").val(countryCodePhon2);
	
	});
	
	
	
	
	   // Next Step
		$("#type_of_organization").change(function() {
			var type_of_organization = $("#type_of_organization").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/nextStep'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : type_of_organization },
				dataType : "html",
				success : function(data) {			
					$("#next_step").html(data);
				}
			});
			
		});
		
		// Next Step
		$("#next_step").change(function() {
			var next_step = $("#next_step").val();			
			$.ajax({
				url : SAWEB.getSiteAction('registration/nextStep2'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : next_step },
				dataType : "html",
				success : function(data) {			
					$("#next_step2").html(data);
				}
			});
			
		});
		
		// Any More Campus
		
		$("#any_more_campus").change(function() {
			var anyCampus = $("#any_more_campus").val();
			 console.log(anyCampus);
			 if(anyCampus == 'Yes'){	
			 $("#more").css({"display":"block"}); 		
			} else {
			$("#more").css({"display":"none"}); 
			}
		});
		
		$("#type_of_organization").change(function(){
			var type = $("#type_of_organization option:selected").val();
			
			 if(type == 'Other') {
				$("#next_step").css("display", "none");
				$("#next_step2").css("display", "none");
				
				$("#type_name_of_organization").css("display", "block");
			} else {
				$("#next_step").css("display", "block");
				$("#next_step2").css("display", "none");
				
				$("#type_name_of_organization").css("display", "none");
			}
			
			if(type == 'EducationProvider') {
				$("#next_step2").css("display", "block");
			} else {
				$("#next_step2").css("display", "none");
			}
		});
		
		$("#next_step").change(function(){
			var step = $("#next_step option:selected").val();
			
			if(step != 'School /College upto  12') {
				$("#next_step2").css("display", "none");
			} else {
				$("#next_step2").css("display", "block");
			}
		});      
	</script>    
  </body>
</html>