<div class="row">
	<div class="col-lg-3 col-xs-12">
	<div style="padding-top:25px; padding-left:55px;"><a href="<?php echo site_url("home"); ?>">
	  <img src="<?php echo base_url("Images/Basic_image/$headerBasicInfo->logo_image"); ?>" width="250" height="50" class="center-block"/></a>
	</div>
	</div>
	<?php 
	  
		 $topLink		 = array('home','orgWiseAdvritismentView', $topAddInfo1->id, $topAddInfo1->user_id);
		 $topLink2		 = array('home','orgWiseAdvritismentView', $topAddInfo2->id, $topAddInfo2->user_id);
	
	
	?>
	<div class="col-lg-5 col-xs-12" align="center">
		<a target="_blank" href="<?php echo site_url($topLink);?>">
		    <img src="<?php echo base_url("Images/Add_image/$topAddInfo1->add_image"); ?>" width="255" height="90" style="border:1px solid;"  />
		  </a>&nbsp;&nbsp;
		   
		   
		<a target="_blank" href="<?php echo site_url($topLink2);?>">
		    <img src="<?php echo base_url("Images/Add_image/$topAddInfo2->add_image"); ?>" width="255" height="90" style="border:1px solid;"  />
		 </a>
		
	</div>
	
	<div class="col-lg-4 col-xs-12">
	  <table width="85%" height="100" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="315" align="right" valign="top">
		  <table width="330" height="55" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
			  	<?php 
					$regLink  	 = array('registration','index');
					$orgRegLink  = array('registration','organizeRegistration');
					
			  	?>
                <td width="58" align="left" valign="middle">
					<?php if(! isActiveUser()) { ?>
				   	<!-- <input type="button" name="Button2" value="Login" class="login_reg" style="height:35px;" data-toggle="modal" data-target="#myModal" /> -->
					<button type="button" id="customePostion" class="login_reg" style="height:35px;" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-target-content="#myModal" data-title="Login">
					  	Login
					</button>
					<?php } ?>
				</td>
				
                <td width="80" align="left" valign="middle">
				  	<?php if(! isActiveUser()) { ?>
				   	<a href="<?php echo site_url($regLink);?>"><input type="button" name="Button2" value="Registration" class="login_reg" style="height:35px;"/></a>
				   	<?php } ?>
				 </td>
				 
                <td width="70" align="left" valign="middle"><input type="button" name="Button22" class="login_reg" value="Live Chat" style="height:35px;"/></td>
                <td width="60" align="left" valign="middle"><input type="button" name="Button3" value="News" class="login_reg goto-news" style="height:35px;"/></td>
              </tr>
          </table>
		  </td>
        </tr>
		<tr>
          <td align="center">
		   <?php if( isActiveUser() ) {
		      if($userType == 'Organization User'){
			    $userPanelLink = array('organizationUserHome');  
			  }else{
			    $userPanelLink = array('generalUserHome');
			  }
		    ?>   
		        Welcome!&nbsp;<span style="color:#0000FF;"><?php if(!empty($activeUser)){ echo $activeUser; } ?></span>&nbsp;<a href="<?php echo site_url($userPanelLink); ?>">My Account</a>
			<?php } ?>
		  </td>
        </tr>
      </table>
	</div>
</div>



<!-- Modal -->
 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<form id="logInForm"  action="<?php echo site_url('loginForm/loginAuthenticate'); ?>" method="post" class="form-horizontal">
	  <div class="col-md-12">
	  	<span class="logInFail"></span>
	  </div>
	    
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="type" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="type" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1">		
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2">
		</div>
	  </div>
	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Sign in</button> &nbsp; &nbsp; <a href="<?php echo site_url('registration/genForgotPaword'); ?>"> Forgot Password?</a></label>
		</div>
	  </div>
	</form>
</div>


<script>
  	$(document).on("submit", "#logInForm", function(e){
  		
		var postData = $(this).serializeArray();
		var formURL  = $(this).attr("action");
		
		var type 	= $(this).find("input[name='type']:checked").val();
		
		if(type =='organization'){
		   var successUrl	= "<?php echo site_url('organizationUserHome'); ?>";
		}else{
		  var successUrl	= "<?php echo site_url('generalUserHome'); ?>";			
		}
	
		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data){
				if(data==1){
					$("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='hidden'], #logInForm input[type='password']").val("");
					$(".logInFail").text("Wrong email or password. Please try again!");
					$(".logInFail").css("color", "red");
					$("#msg").css("display", "block"); 
				} else if(data==2){
					 $(".logInFail").text("Please Select a type Organization or Other");
					 $(".logInFail").css("color", "red"); 
					 $("#msg").css("display", "block"); 					
				}else{
				 location.replace(successUrl);
				}
			}					
		});
	
	   	e.preventDefault();
	});
	
	$(".close").click(function(){
	   $("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='password']").val("");
	   $("#logInForm input[type='radio']").prop("checked",false);
	});

	$(window).ready(function(){
			$('[data-toggle="popover"]').popover({				
				content: function(){
					var target = $(this).attr('data-target-content');
					return $(target).html();
				}
			})/*.on('shown.bs.popover', function () {
				$('.popover').css('left', $(this).offset().left);
			});*/
		});
</script>

