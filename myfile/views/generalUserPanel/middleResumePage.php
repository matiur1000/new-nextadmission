<link href="<?php echo base_url('resource/source/font-awesome.css'); ?>" rel="stylesheet">
<div class="col-lg-9">
	<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
    <div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:16px; font-weight:bold;"><i class="glyphicon glyphicon-eye-open icon-padding"></i>  View Resume <span style="float:right; padding-right:10px;">
		 <a href="<?php echo site_url('generalUserHome/profileUpdate/'.$id);?>"><i class="glyphicon glyphicon-edit"></i>Edit</a></span></span></span>
	   </div>
	</div><br/>
	<div class="row">
	  <div class="col-lg-12">
	 
	<div class="row" style="padding:10px;">
	
      
      <div class="col-lg-6" align="left"><table width="100%" cellspacing="0" cellpadding="0" class="table-responsive" align="left">
             
             <tr>
                <td width="11%" height="25">&nbsp;</td>
                <td width="87%" align="left" valign="middle"> <span style="font-size:16px; padding-left:0px; font-weight:bold">Download Resume :</span><a target="_blank" href="<?php echo site_url('generalUserHome/pdfResume/'.$userDetails->id);?>"> <i style="color:#F00; font-size:18px;" class="fa fa-file-pdf-o fa-2x"></i>
</a></td>
                <td width="2%">&nbsp;</td>
              </tr>
              <tr>
                <td width="11%" height="35">&nbsp;</td>
                <td width="87%" align="left" valign="middle"><span style="color:#000099; font-size:18px"><?php echo $resumeInfo->name; ?></span></td>
                <td width="2%">&nbsp;</td>
              </tr>
              <tr>
                <td height="29">&nbsp;</td>
                <td align="left" valign="middle">Adress : <?php echo $moreResumeInfo->address; ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td height="27">&nbsp;</td>
                <td align="left" valign="middle">Phone   : <?php echo $moreResumeInfo->country_phone; ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td height="32">&nbsp;</td>
                <td align="left" valign="middle">Mobile   : <?php echo $moreResumeInfo->mobile; ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td height="28">&nbsp;</td>
                <td align="left" valign="middle">Email     : <?php echo $moreResumeInfo->email; ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table></div>
      <div class="col-lg-6" align="right"><img src="<?php echo base_url("/Images/Register_image/$resumeInfo->image"); ?>" height="150" width="150"></div>  
	</div>
    <?php
	    if(!empty($moreResumeInfo->degree_title)){
	 ?>
      
    <div class="row" style="padding:10px;">
	  <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;"><span style="font-size:15px; font-weight:bold; padding-left:50px;">Education</span></div><br/>
      <div class="row" style="padding:20px; padding-left:90px; padding-right:40px;">
      <div class="row" style="border:solid 1px #cccccc; padding:15px; padding-top:0px; padding-bottom:0px;">
      
      <div class="row" style="padding:0px; padding-left:10px; padding-right:40px; font-weight:bold; border-bottom:solid 1px #cccccc;">
       <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Exam Title</div>
      <div class="col-lg-3" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Concentration</div>
       <div class="col-lg-3" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Institute</div>
      <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Result</div>
       <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Pas.Year</div>
      <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Duration</div>
      <div class="col-lg-1" style="padding:5px; height:40px; padding-top:10px;">Achievement</div>
      </div>
      
      <div class="row" style="padding:0px; padding-left:10px; padding-right:40px;">
      
       <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->degree_title; ?></div>
      <div class="col-lg-3" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->group; ?></div>
       <div class="col-lg-3" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->institute_name; ?></div>
      <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->result; ?></div>
       <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->pass_year; ?></div>
      <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->duration; ?></div>
      <div class="col-lg-1" style="padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->achievement; ?></div>
       </div>
      </div>
      </div>
       
	</div>
	<?php } ?>
	
    <?php
	    if(!empty($moreResumeInfo->training_title)){
	 ?>
    <div class="row" style="padding:10px;">
	<div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;"><span style="font-size:15px; font-weight:bold; padding-left:50px;">Training</span></div><br/>
      <div class="row" style="padding:20px; padding-left:90px; padding-right:40px;">
      <div class="row" style="border:solid 1px #cccccc; padding:15px; padding-top:0px; padding-bottom:0px;">
      
      <div class="row" style="padding:0px; padding-left:10px; padding-right:40px; font-weight:bold; border-bottom:solid 1px #cccccc;">
       <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Training Title</div>
      <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Topic Covered</div>
       <div class="col-lg-3" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Institute</div>
      <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Country</div>
       <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Location</div>
      <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;">Training Year</div>
      <div class="col-lg-1" style="padding:5px; height:40px; padding-top:10px;">Duration</div>
      </div>
      
      <div class="row" style="padding:0px; padding-left:10px; padding-right:40px;">
      
       <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_title; ?></div>
      <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_tropic; ?></div>
       <div class="col-lg-3" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_institute; ?></div>
      <div class="col-lg-2" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_country; ?></div>
       <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_location; ?></div>
      <div class="col-lg-1" style="border-right:solid 1px #cccccc; padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_year; ?></div>
      <div class="col-lg-1" style="padding:5px; height:40px; padding-top:10px;"><?php echo $moreResumeInfo->training_duration; ?></div>
       </div>
      </div>
      </div>
       
	</div>
	<?php } ?>
	<?php
	    if(!empty($moreResumeInfo->objective)){
	 ?>
    
    <div class="row" style="padding:10px;">
	  <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;"><span style="font-size:15px; font-weight:bold; padding-left:50px;">Career and Application Information</span></div><br/>
      <div class="col-lg-6" align="left"><table width="100%" cellspacing="0" cellpadding="0" class="table-responsive">
              <tr>
                <td width="10%" height="30">&nbsp;</td>
                <td width="39%" align="left" valign="middle">Objective </td>
                <td width="5%" align="center" valign="middle">:</td>
                <td width="46%" align="left" valign="middle"><?php echo $moreResumeInfo->objective; ?></td>
              </tr>
              <tr>
                <td height="28">&nbsp;</td>
                <td align="left" valign="middle">Present Salary </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->present_sallary; ?></td>
              </tr>
              <tr>
                <td height="30">&nbsp;</td>
                <td align="left" valign="middle">Expected Salary </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->expected_sallary; ?></td>
              </tr>
              <tr>
                <td height="28">&nbsp;</td>
                <td align="left" valign="middle">Looking for  </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->level; ?></td>
              </tr>
              <tr>
                <td height="30">&nbsp;</td>
                <td align="left" valign="middle">Available for </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->available; ?></td>
              </tr>
              
			  <tr>
                <td height="26">&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
                <td align="center" valign="middle">&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
              </tr>
            </table></div>
      <div class="col-lg-6" align="right"></div>  
	</div>
	<?php } ?>
    
    <div class="row" style="padding:10px;">
	  <div class="col-lg-12" style="background:#CCCCCC; padding:3px; margin-bottom:30px;"><span style="font-size:15px; font-weight:bold; padding-left:50px;">Personal Details</span></div><br/>
      <div class="col-lg-6" align="left"><table width="100%" cellspacing="0" cellpadding="0" class="table-responsive">
              <tr>
                <td width="10%" height="30">&nbsp;</td>
                <td width="39%" align="left" valign="middle">Father's Name </td>
                <td width="5%" align="center" valign="middle">:</td>
                <td width="46%" align="left" valign="middle"><?php echo $moreResumeInfo->father_name; ?></td>
              </tr>
              <tr>
                <td height="28">&nbsp;</td>
                <td align="left" valign="middle">Mother's Name </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->mother_name; ?></td>
              </tr>
              <tr>
                <td height="30">&nbsp;</td>
                <td align="left" valign="middle">Date Of Birth </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->date_of_birth; ?></td>
              </tr>
              <tr>
                <td height="28">&nbsp;</td>
                <td align="left" valign="middle">Gender </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->gender; ?></td>
              </tr>
              <tr>
                <td height="30">&nbsp;</td>
                <td align="left" valign="middle">Nationality </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->nationality; ?></td>
              </tr>
              <tr>
                <td height="27">&nbsp;</td>
                <td align="left" valign="middle">Marital Status </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->marital_status; ?></td>
              </tr>
              <tr>
                <td height="29">&nbsp;</td>
                <td align="left" valign="middle">Religion  </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->religion; ?></td>
              </tr>
              <tr>
                <td height="26">&nbsp;</td>
                <td align="left" valign="middle">Permanent Adress </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->address_permanent; ?></td>
              </tr>
			  <tr>
                <td height="26">&nbsp;</td>
                <td align="left" valign="middle">Current Location </td>
                <td align="center" valign="middle">:</td>
                <td align="left" valign="middle"><?php echo $moreResumeInfo->address; ?></td>
              </tr>
			  <tr>
                <td height="26">&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
                <td align="center" valign="middle">&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
              </tr>
            </table></div>
      <div class="col-lg-6" align="right"></div>  
	</div>
    
   
    
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		
	</div>                
</div>
