<div class="col-lg-9">
	<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
	<div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:18px; font-weight:bold;"><i class="glyphicon glyphicon-lock icon-padding"></i> Change Password</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>   
		<div class="row">
		 <form action="<?php echo site_url('generalUserHome/updatePass'); ?>" method="post" enctype="multipart/form-data">
		  <input type="hidden" name="id" id="id" value="<?php echo $profileEditInfo->id; ?>" />
			<div class="col-lg-12">
				<div class="space-4"></div>  
					  <div class="form-group">
						<label for="education_institute"><span style="color: #333; font-size: 16px; font-weight: normal;">User Name</span></label>
						<input type="text" class="form-control" id="user_id" placeholder="User Name" name="user_id" tabindex="5" 
						value="<?php echo $profileEditInfo->user_id; ?>" readonly="readonly">
					  </div>
		   </div> 
		   	<div class="col-lg-12">
				<div class="space-4"></div>  
					  <div class="form-group">
						<label for="password"><span style="color: #333; font-size: 16px; font-weight: normal;">Old Password</span></label>
						<input type="password" class="form-control" id="password" placeholder="Old Password" name="password" tabindex="5">
					  </div>
		   </div>
		    <div class="col-lg-12">
				<div class="space-4"></div>  
					  <div class="form-group">
						<label for="newPassword"><span style="color: #333; font-size: 16px; font-weight: normal;">New Password</span></label>
						<input type="password" class="form-control newPassword" id="newPassword" placeholder="New Password" name="newPassword" tabindex="5"><span class="first"></span>
					  </div>
		   </div> 
		  	<div class="col-lg-12">
				<div class="space-4"></div>  
					  <div class="form-group">
						<label for="education_institute"><span style="color: #333; font-size: 16px; font-weight: normal;">Confirm Password</span></label>
						<input type="password" class="form-control conPassword" id="conpass" placeholder="Confirm Password" name="conpass" tabindex="5"><span class="second"></span>
					  </div>
		   </div>
			 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Password</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	</div>                
</div>
