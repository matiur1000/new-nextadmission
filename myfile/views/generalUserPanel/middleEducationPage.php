<div class="col-lg-9">
	<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
	<div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:18px; font-weight:bold;"><i class="glyphicon glyphicon-edit icon-padding"></i> Update Resume</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
  
	  <div class="col-lg-2 col-sm-1 comntorg3" style="height:30px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/profileUpdate/'.$userDetails->id);?>">Personal Details</a></div>
      <div class="col-lg-4 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/careerInformationUpdate/'.$userDetails->id);?>">Career and Application Information</a></div>
      <div class="col-lg-2 col-sm-1  active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/educationUpdate/'.$userDetails->id);?>">Education</a></div>
		 <div class="col-lg-2 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/traningUpdate/'.$userDetails->id);?>">Training</a></div>
     
   </div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 18px; font-weight: bold; margin: 0; padding: 0 12px 0px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
                                <div class="row">
                                <form action="<?php echo site_url('generalUserHome/educationAction'); ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="id" value="<?php echo $moreprofileEditInfo->user_id; ?>" />
                                <div class="col-lg-12">
                                <div class="form-group">
                                
                                <div>
                                <div class="form-group">
									<label for="education_level"><span style="color: #333; font-size: 16px; font-weight: normal;">Level of Education</span></label>        
									<div>
									   <select class="form-control" id="education_level" name="education_level"  tabindex="2" value="<?php echo set_value('education_level'); ?>">
											<option <?php if($moreprofileEditInfo->education_level == 'Secondary'){?> selected="selected" <?php } ?> value="Secondary">Secondary</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Higher Secondary'){?> selected="selected" <?php } ?>  value="Higher Secondary">Higher Secondary</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Diploma'){?> selected="selected" <?php } ?> value="Diploma">Diploma</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Bachelor/Honors'){?> selected="selected" <?php } ?>  value="Bachelor/Honors">Bachelor/Honors</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Masters'){?> selected="selected" <?php } ?> value="Masters">Masters</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Doctoral'){?> selected="selected" <?php } ?>  value="Doctoral">Doctoral</option>
									   </select>
									</div>
								</div>
								
								
                                </div>
                                </div> 
                                
                                <div class="space-4"></div>  
                                
                                <div class="form-group">
                                <label for="degree_title"><span style="color: #333; font-size: 16px; font-weight: normal;">Exam/Degree Title</span></label>
                                <input type="text" class="form-control" id="degree_title" name="degree_title" placeholder="Exam/Degree Title" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->degree_title; ?>">
                                
                                
                                </div>
                                
                                <div class="space-4"></div>  
                                <div class="form-group">
                                <label for="group"><span style="color: #333; font-size: 16px; font-weight: normal;">Concentration/ Major/Group</span></label>
                                <input type="text" class="form-control" id="group" name="group" placeholder="Concentration/ Major/Group" tabindex="7" value="<?php echo $moreprofileEditInfo->group; ?>">
                                </div>
                                <div class="form-group">
                                <label for="institute_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Institute Name</span></label>
                                <input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="Institute Name" tabindex="7" value="<?php echo $moreprofileEditInfo->institute_name; ?>">
                                </div>
                                
                                <div class="form-group">
									<label for="result"><span style="color: #333; font-size: 16px; font-weight: normal;">Result</span></label>        
									<div>
									   <select class="form-control" id="result" name="result"  tabindex="2" value="<?php echo set_value('result'); ?>">
											<option <?php if($moreprofileEditInfo->result == 'First Division'){?> selected="selected" <?php } ?> value="First Division">First Division</option>
											<option <?php if($moreprofileEditInfo->result == 'Second Division'){?> selected="selected" <?php } ?>  value="Second Division">Second Division</option>
											<option <?php if($moreprofileEditInfo->result == 'Third Division'){?> selected="selected" <?php } ?> value="Third Division">First Division</option>
											<option <?php if($moreprofileEditInfo->result == 'Grade'){?> selected="selected" <?php } ?>  value="Grade">Grade</option>
											
									   </select>
									</div>
								</div>
                                
                                <div id="cgpa" class="row" style="display:none;">
								 <div class="col-lg-6">
									<div class="form-group">
									<label for="cgpa"><span style="color: #333; font-size: 16px; font-weight: normal;">CGPA</span></label>
										<input type="text" class="form-control" id="cgpa" name="cgpa" placeholder="CGPA" tabindex="7" value="<?php echo $moreprofileEditInfo->cgpa; ?>">
								  </div> 
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
									<label for="scale"><span style="color: #333; font-size: 16px; font-weight: normal;">Scale</span></label>
										<input type="text" class="form-control" id="scale" name="scale" placeholder="Scale" tabindex="7" value="<?php echo $moreprofileEditInfo->scale; ?>">
									</div> 
									</div>
                                </div>
                                
                                <div class="form-group">
                                <label for="pass_year"><span style="color: #333; font-size: 16px; font-weight: normal;">Year of Passing</span></label>
                                <input type="text" class="form-control" id="pass_year" name="pass_year" placeholder="Year of Passing " tabindex="7" value="<?php echo $moreprofileEditInfo->pass_year; ?>">
                                </div>
                                <div class="form-group">
                                <label for="duration"><span style="color: #333; font-size: 16px; font-weight: normal;">Duration</span></label>
                                <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration" tabindex="7" value="<?php echo $moreprofileEditInfo->duration; ?>">
                                </div>
                                <div class="form-group">
                                <label for="achievement"><span style="color: #333; font-size: 16px; font-weight: normal;">Achievement </span></label>
                                <input type="text" class="form-control" id="achievement" name="achievement" placeholder="Achievement" tabindex="7" value="<?php echo $moreprofileEditInfo->achievement; ?>">
                                </div>
                                </div>   
                                <div class="row">&nbsp;</div>  
                                <div class="row">
                                <div class="col-lg-12" align="center">
                                <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Resume</button>
                                </div>
                                </div>
                                <div class="row">&nbsp;</div>   
                                <div class="row">&nbsp;</div>  
                                </form>        
                                </div>
                                </div>                
                                </div>
