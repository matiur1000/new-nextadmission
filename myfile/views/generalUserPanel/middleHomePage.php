	<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
    <div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:16px; font-weight:bold;"><i class="glyphicon glyphicon-home icon-padding"></i> User Home</span><span style="float:right; padding-right:50px;font-size:18px;"><?php echo $updateText ?></span>
	   </div>
	</div><br/>
	<div class="row">
	  <div class="col-lg-12">
	    <table width="100%" cellspacing="0" cellpadding="0" style="padding:0px;">
		
		  <tr>
			<td align="center" valign="middle">
			<table width="100%" cellspacing="0" cellpadding="0" class="table">
              <tr>
                <td width="5%" height="">&nbsp;</td>
                <td width="61%" align="left" valign="middle" style="padding-top:14px; padding-bottom:14px;">Total Number of <strong>Companies viewed</strong> my Resume</td>
                <td width="14%" align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><strong><?php echo $totalViewApp; ?></strong></td>
                <td width="20%" align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><a href="<?php echo site_url('generalUserHome/viewCompany'); ?>">View</a></td>
              </tr>
              <tr>
                <td height="">&nbsp;</td>
                <td align="left" valign="middle" style="padding-top:14px; padding-bottom:14px;">Number of <strong>Online Application</strong></td>
                <td align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><strong><?php echo $totalJobApp; ?></strong></td>
                <td align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><a href="<?php echo site_url('generalUserHome/jobApplied'); ?>">View</a></td>
              </tr>
              <tr>
                <td height="">&nbsp;</td>
                <td align="left" valign="middle" style="padding-top:14px; padding-bottom:14px;">Number of <strong>Emailed Resume</strong></td>
                <td align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;">&nbsp;</td>
                <td style="padding-top:14px; padding-bottom:14px;">&nbsp;</td>
              </tr>
              <tr>
                <td height="">&nbsp;</td>
                <td align="left" valign="middle" style="padding-top:14px; padding-bottom:14px;">Number of <strong>Shortlisted Jobs</strong></td>
                <td align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><strong><?php echo $totalShortApp; ?></strong></td>
                <td align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><a href="<?php echo site_url('generalUserHome/shortListCom'); ?>">View</a></td>
              </tr>
              <tr>
                <td height="">&nbsp;</td>
                <td align="left" valign="middle" style="padding-top:14px; padding-bottom:14px;">Resume <strong>last updated</strong> on</td>
                <td align="center" valign="middle" style="padding-top:14px; padding-bottom:14px;"><strong><i class="glyphicon glyphicon-calendar"></i>&nbsp;<?php echo $resumeLastUpdate->resume_update_date; ?></strong></td>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
		  </tr>
	</table>
 
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		
	</div>                
