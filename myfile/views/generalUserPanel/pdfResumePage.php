<?php
$this->load->view('generalUserPanel/html2fpdf');
$pdf=new HTML2FPDF();
$pdf->AddPage();

ob_start();
$this->load->view('generalUserPanel/pdfResumeViewPage');
$content = ob_get_contents();
ob_end_clean();

//echo $content;

$pdf->WriteHTML($content);
$pdf->Output('./Images/Pdf_resume/resume_'.$id.'.pdf');

$redirect_url = str_replace('.html', '', site_url('Images/Pdf_resume/resume_'.$id.'.pdf'));
?>
<script type="text/javascript">location.replace("<?php echo $redirect_url; ?>");</script>