<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
    
  <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  

 
  </head>
  <body>
        <div class="container">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('generalUserPanel/generalHeaderPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('generalUserPanel/generalMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  
							  <div class="row">&nbsp;</div> 
							  <div class="row">
							  <?php $this->load->view('generalUserPanel/leftMenuPage'); ?>
									
							 <?php $this->load->view('generalUserPanel/middleProfilePage'); ?>
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
   
    <script>
  	   $(document).ready(function(){
  			$(".nav-tabs a").click(function(){
  				$(this).tab('show');
  			});
  		   
  		});


       $('.date-picker').datepicker({
        autoclose: true   
      }); 
		
   
    </script>
    
  </body>
</html>