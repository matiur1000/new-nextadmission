<div class="col-lg-9">
	<div class="welll welll-lg"> 
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12">
<table width="100%" height="41" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="33%" align="center"><img src="<?php echo base_url("Images/Basic_image/st1-blk.png"); ?>" class="center-block"/></td>
    <td width="33%" align="center"><img src="<?php echo base_url("Images/Basic_image/st2-red.png"); ?>" class="center-block"/></td>
    <td width="33%" align="center"><img src="<?php echo base_url("Images/Basic_image/st3-blk.png"); ?>" class="center-block"/></td>
  </tr>
</table>


	  </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
		<span style="color:#0e73a9; font-size:24px; font-weight:bold;">Organization Details</span>
	  </div>
	</div>
	<div class="row">&nbsp;</div> 
		<div class="row">
		 <form action="<?php echo site_url('registration/organizeRegistration3'); ?>" method="post" enctype="multipart/form-data">
			<div class="col-lg-12">
			
			
			   	<div class="form-group">
					<input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="Organization Name" tabindex="1" 
					value="<?php echo set_value('organization_name'); ?>"><?php echo form_error('organization_name'); ?>
				</div>
				<div class="form-group">
					   <textarea class="form-control" name="address" id="address" cols="60" rows="" placeholder="Organization Address" 
					   value="<?php echo set_value('address'); ?>"></textarea><?php echo form_error('address'); ?>
					  </div>
				<div class="form-group">
					<input type="text" class="form-control" id="city" name="city" placeholder="City" tabindex="2" 
					value="<?php echo set_value('city'); ?>"><?php echo form_error('city'); ?>
				</div>
				
				<div class="form-group">
					<input type="text" class="form-control" id="province" name="province" placeholder="Province" tabindex="2"
					value="<?php echo set_value('province'); ?>"><?php echo form_error('province'); ?>
				</div>
				
			   	<div class="form-group">
					<input type="text" class="form-control" id="country" name="country" placeholder="Country" tabindex="1" 
					value="<?php echo set_value('country'); ?>"><?php echo form_error('country'); ?>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="phone_com" name="phone_com" placeholder="Phone" tabindex="2" 
					value="<?php echo set_value('phone_com'); ?>"><?php echo form_error('phone_com'); ?>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="mobile_com" name="mobile_com" placeholder="Mobile" tabindex="2" 
					value="<?php echo set_value('mobile_com'); ?>"><?php echo form_error('mobile_com'); ?>
				</div>
				
			   	<div class="form-group">
					<input type="text" class="form-control" id="email_com" name="email_com" placeholder="Email" tabindex="1" 
					value="<?php echo set_value('email_com'); ?>"><?php echo form_error('email_com'); ?>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="location " name="location " placeholder="Location" tabindex="1" 
					value="<?php echo set_value('location'); ?>"><?php echo form_error('location'); ?>
				</div>
				
				<div class="form-group">
						<input name="founded" type="text" id="founded" class="form-control"
						 onClick="('#founded').datepicker({dateFormat: 'dd/mm/yy'});" placeholder="Founded" value="<?php echo set_value('founded'); ?>"/>
						 <?php echo form_error('founded'); ?>
					   
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="type_of_organization" name="type_of_organization"  tabindex="2">
							<option value="" selected>Sellect Type Of Organization</option>
							<option value="Education provider">Education provider </option>
							<option value="Agency">Agency</option>
							<option value="Couching Centre">Couching Centre</option>
							<option value="Professional Training Centre">Professional Training Centre </option>
							<option value="Other">Other</option>
					   </select>
					</div>
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="type_of_ownership" name="type_of_ownership"  tabindex="2">
							<option value="" selected>Sellect Type Of Ownership</option>
							<option value="Government">Government</option>
							<option value="Private">Private</option>
					   </select>
					</div>
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="type_of_education_provider" name="type_of_education_provider"  tabindex="2">
							<option value="" selected>Sellect Type of Education provider </option>
							<option value="School /College upto  12">School /College upto  12</option>
							<option value="University">University</option>
					   </select>
					</div>
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="agency_type" name="agency_type"  tabindex="2">
							<option value="" selected>Sellect Agency Type</option>
							<option value="Travel Agent ( Air Ticket)">Travel Agent ( Air Ticket)</option>
							<option value="Education Tour Agent">Education Tour Agent</option>
							<option value="Package Tour Agent">Package Tour Agent</option>
							<option value="Visa process">Visa process</option>
							<option value="Hotel Booking">General user</option>
							<option value="Job Agent">Job Agent</option>
							<option value="Other">Other</option>
					   </select>
					</div>
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="school_college" name="school_college"  tabindex="2">
							<option value="" selected>Sellect School /College</option>
							<option value="KG-8 years Class">KG-8 years Class</option>
							<option value="K-10 years Class">K-10 years Class</option>
							<option value="9-10 years Class">9-10 years Class</option>
							<option value="10-12 years Class">10-12 years Class</option>
							<option value="Bachelor">Bachelor</option>
							<option value="Masters">Masters</option>
					   </select>
					</div>
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="education_consultant" name="education_consultant"  tabindex="2">
							<option value="" selected>Sellect Education Consultant</option>
							<option value="Foreign Admission">Foreign Admission</option>
							<option value="Local Admission support">Local Admission support</option>
							<option value="Visa support">Visa support</option>
							<option value="Medical Admission">Medical Admission</option>
					   </select>
					</div>
				</div>
				<div class="form-group">
					<div>
					   <select class="form-control" id="couching_centre" name="couching_centre"  tabindex="2">
							<option value="" selected>Sellect Type Of Couching Centre</option>
							<option value="College Admission">College Admission </option>
							<option value="Medical Admission">Medical Admission</option>
							<option value="University Admission">University Admission</option>
							<option value="English Training : IELTS-GRE-GMET etc">English Training : IELTS-GRE-GMET etc</option>
							<option value="Foreign Language : Germany-France-Courier etc">Foreign Language : Germany-France-Courier etc</option>
							<option value="Professional">Professional</option>
							<option value="Tutor Services">Tutor Services</option>
					   </select>
					</div>
				</div>
				<div class="form-group">
					<div>
					   <select class="form-control" id="professional_taining" name="professional_taining"  tabindex="2">
							<option value="" selected>Sellect Type Of Professional Training </option>
							<option value="Government Job">Government Job</option>
							<option value="BCS Training">BCS Training</option>
							<option value="Marine Training">Marine Training</option>
							<option value="Defence Training">Defence Training</option>
					   </select>
					</div>
				</div>
				
				<div class="form-group">
					<div>
					   <select class="form-control" id="scholarship" name="scholarship"  tabindex="2">
							<option value="" selected>Sellect Type Of Scholarship </option>
							<option value="Full Scholarship">Full Scholarship</option>
							<option value="Half Scholarship">Half Scholarship</option>
							<option value="Tuition fees waver">Tuition fees waver</option>
							<option value="Monthly Scholarship">Monthly Scholarship</option>
							<option value="Year Scholarship">Year Scholarship</option>
					   </select>
					</div>
				</div>
				<div class="form-group">
					<div>
					   <select class="form-control" id="accademic_exam_system" name="accademic_exam_system"  tabindex="2">
							<option value="" selected>Sellect Type Of Academic Exam system  </option>
							<option value="Quarter Exam">Quarter Exam</option>
							<option value="Half Year semester">Half Year semester</option>
							<option value="Yearly semester">Yearly semester</option>
					   </select>
					</div>
				</div>
				
				
		    </div> 
			
			 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Go Step 3</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	</div>                
</div>