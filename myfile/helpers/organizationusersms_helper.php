<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('organizationSendSms')) 
{
	function organizationSendSms()
	{
		$_CI = &get_instance();
		
		$userId		     	= $_CI->input->post('org_user');
		$message		 	= $_CI->input->post('message');
      	$smsOrgUser	 		= $_CI->M_organization_user_registration->findAllorganizationUserMobile($userId);
	  	$orgMobile          = $smsOrgUser->mobile;	
		  
		
		
			$client = new SoapClient('http://180.210.190.230:6500/Services/SmsClient.asmx?WSDL',
					array(						
						"stream_context" => stream_context_create(
							array(
								'ssl' => array(
									'verify_peer'       => false,
									'verify_peer_name'  => false,
								)
							)
						)
					)
					 
				);
			
			// Set the parameters
			$requestParams = array(
				'userName' => 'gstudy', // Use your user-id here
				'password' => 'Gstudy123',
				'smsText'  => $message,
				'commaSeparatedReceiverNumbers' =>$orgMobile, // you can use multiple mobile numbers here; e.g: 01810000000,01710000000,0191000000,015...
				'nameToShowAsSender' => 'Global Study' // Use your mask text here if you want (and you have masking enabled)
			);
			
			// Call to send sms
			$response = $client->SendSms($requestParams)->SendSmsResult;
		
		
		
		if($response->IsError){
			//echo "<h2 style='color:red'>FAILED!</h3>";
			
			// Know the reason for failure (and the error code)
			//echo sprintf("Reason: %s [%d]", $response->ErrorMessage, $response->ErrorCode);
			return false;
		} else{
			
			return true;
		}	
		
		
	}
}













// ------------------------------------------------------------------------
/* End of file authentication_helper.php */
/* Location: ./application/helpers/authentication_helper.php */