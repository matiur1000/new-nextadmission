<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('generalLoginUser')) 
{
	function generalLoginUser()
	{
		$_CI = &get_instance();
		
		$UserName		 = $_CI->input->post('email');
		$UserPass		 = $_CI->input->post('password');
		
		$UserDetails	 = $_CI->M_all_user_registration->findByUserName($UserName);
		
		 if( !empty($UserDetails) && $UserDetails->password == $UserPass && $UserDetails->user_type == 'General user' && $UserDetails->status == 'Active'){
		    setUserData($UserName, $UserDetails->user_type );	
			   redirect('generalUserHome'); 
		
		} else {
			echo "1";
		}
	}
}

// TO SET DATA IN UserName SESSION FIELD
if ( ! function_exists('setUserData'))
{
	function setUserData($UserName,$userType)
	{
		$_CI = &get_instance();
		$userData	= array(
			'UserId'    	=> $UserName,
			'userType'    	=> $userType
		);
		$_CI->session->set_userdata($userData);
	}
}


if ( ! function_exists('getUserName'))
{
	function getUserName()
	{
		$_CI = &get_instance();
		return $_CI->session->userdata('UserId');
	}
}

if ( ! function_exists('getUserType'))
{
	function getUserType()
	{
		$_CI = &get_instance();
		return $_CI->session->userdata('userType');
	}
}


if ( ! function_exists('invalidUserData'))
{
	function invalidUserData()
	{
		setUserData(NULL, NULL);
	}
}


if ( ! function_exists('isActiveUser'))
{
	function isActiveUser()
	{
		return getUserName() != NULL;
	}
}

if ( ! function_exists('isAuthenticate'))
{
	function isAuthenticate()
	{
		if(!isActiveUser()) {
			redirect('home');	
		} else {
			return true;	
		}
	}
}


if ( ! function_exists('genlogoutUser'))
{
	function genlogoutUser()
	{
		invalidUserData();
		if(!isActiveUser()){
			redirect('home');
		}
	}
}


