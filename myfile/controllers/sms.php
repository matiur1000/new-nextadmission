<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends MX_Controller {

		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_news_and_event_manage',
		'M_all_user_registration','M_organization_user_registration','M_add_manage','M_general_user_registration');
		static $helper	= array('url', 'userauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
	
		}
	

		public function index()	
		{
		  $client = new SoapClient('http://180.210.190.230:6500/Services/SmsClient.asmx?WSDL',
					array(						
						"stream_context" => stream_context_create(
							array(
								'ssl' => array(
									'verify_peer'       => false,
									'verify_peer_name'  => false,
								)
							)
						)
					)
					 
				);
			
			// Set the parameters
			$requestParams = array(
				'userName' => 'gstudy', // Use your user-id here
				'password' => 'Gstudy123',
				'smsText'  => "test",
				'commaSeparatedReceiverNumbers' =>01929588684, // you can use multiple mobile numbers here; e.g: 01810000000,01710000000,0191000000,015...
				'nameToShowAsSender' => 'Global Study' // Use your mask text here if you want (and you have masking enabled)
			);
			
			// Call to send sms
			$response = $client->SendSms($requestParams)->SendSmsResult;
			 print_r($response);
		
		}
	

}

