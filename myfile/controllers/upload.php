<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	static $model 	 = array();
	static $helper   = array('form');
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model(self::$model);
		$this->load->helper(self::$helper);
		$this->load->library('upload');
	}
	
	public function index($type = 0)
	{	
		$output = array();

		if(($type == 'logo_image')||($type == 'globe_image')){
			$config = array('upload_path' => './Images/Basic_image/');
			$fileLocation = "Images/Basic_image/";
			
		} else if($type == 'menu_image') {
			$config = array('upload_path' => './Images/Menu_image/');
			$fileLocation = "Images/Menu_image/";
			
		} else if($type == 'register_image') {
		$config = array('upload_path' => './Images/Register_image/');
		$fileLocation = "Images/Register_image/";
		
		} else if($type == 'sub_image') {
		$config = array('upload_path' => './Images/Sub_menu_image/');
		$fileLocation = "Images/Sub_menu_image/";
		
		} else if($type == 'deeper_image') {
		$config = array('upload_path' => './Images/Deeper_image/');
		$fileLocation = "Images/Deeper_image/";
		
		} else if($type == 'news_image') {
		$config = array('upload_path' => './Images/News_image/');
		$fileLocation = "Images/News_image/";
		
		} else if($type == 'com_logo') {
		$config = array('upload_path' => './Images/Add_manage/');
		$fileLocation = "Images/Add_manage/";
		
		} else if($type == 'add_img') {
		$config = array('upload_path' => './Images/Add_image/');
		$fileLocation = "Images/Add_image/";

		} else if($type == 'add_image') {
		$config = array('upload_path' => './Images/Add_image/');
		$fileLocation = "Images/Add_image/";
		
		
		} else  {
			$config = array('upload_path' => './uploads/', 'max_size' => '200');
			$fileLocation = "uploads/";
		}

		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name'] 	 = time();

		$this->upload->initialize($config);

		if($this->upload->do_upload('attachment')) {
			$uploadData = $this->upload->data();
			$output['status'] = 'success';
			$output['fileLocation'] = $fileLocation.$uploadData['file_name'];
			$output['fileName'] 	= $uploadData['file_name'];
		} else {
			$output['status'] = 'failed';
			$output['message'] = $this->upload->display_errors('', '');
		}

		echo json_encode($output);
	}


	public function remove($type = 0)
	{	
		$fileName = $this->input->post('attachment');

		if(($type == 'logo_image')&&($type == 'globe_image')) {
			$fileLink = './Images/Basic_image/'.$fileName;
		}
		
		if($type == 'menu_image') {
			$fileLink = './Images/Menu_image/'.$fileName;
		}
		
		if($type == 'sub_image') {
			$fileLink = './Images/Sub_menu_image/'.$fileName;
		}
		if($type == 'deeper_image') {
			$fileLink = './Images/Deeper_image/'.$fileName;
		}
		
		if($type == 'news_image') {
			$fileLink = './Images/News_image/'.$fileName;
		}
		if($type == 'com_logo') {
			$fileLink = './Images/Add_manage/'.$fileName;
		}
		if($type == 'add_img') {
			$fileLink = './Images/Add_image/'.$fileName;
		}

		if($type == 'add_image') {
			$fileLink = './Images/Add_image/'.$fileName;
		}
		
		unlink($fileLink);
	}
	
	
	


}