<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GeneralUserHome extends CI_Controller {
	static $model 	= array('M_basic_manage','M_advertisement_manage','M_region_manage','M_country_manage','M_city_manage',
	'M_all_user_registration','M_interested_job_post_manage','M_user_post_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_organization_user_registration',
	'M_general_user_registration','M_job_application_details','M_search_table','M_organize_profile_manage');
	static $helper   = array('url','generalauthentication');
	
	const  Title	 = 'Next Admission';
	
	public function __construct(){
		parent::__construct();
		$this->load->database(); // TO LOAD DATABASE CONNECTIVITY
		$this->load->model(self::$model); // TO LOAD ALL MODEL
		$this->load->helper(self::$helper);// TO LOAD URL BASE HELPER LIKE BASE_URL, SITE_URL
		$this->load->library('form_validation'); // LOAD THIS LIBRARY TO USE FORM VALIDATION 
		$this->load->library('pagination'); // LOAD THIS LIBRARY TO USE PAGINATION
		$this->load->library('upload');  // LOAD THIS LIBRARY TO FILE UPLOAD
		$this->load->library('image_lib');
		$this->load->library('session');
		isAuthenticate();
	}

	public function index()
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;	
			$data['totalJobApp']		= $this->M_job_application_details->findByUser($userAutoId);
			$data['totalViewApp']		= $this->M_job_application_details->viewApplication($userAutoId);
			$data['totalShortApp']		= $this->M_job_application_details->shortList($userAutoId);
			$data['resumeLastUpdate'] 	= $this->M_general_user_registration->findByUserName($userAutoId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
			$updateText       		    = $this->session->userdata('updateText');
			if(!empty($updateText)){     
			  $data['updateText']       = $updateText;
			}
			 $this->session->set_userdata(array('updateText' => ""));
			 
			$this->load->view('generalUserPanel/generalUserHomePage', $data);
			
		
		}
	}




	   public function viewMessage($onset = 0)
		{   
		    if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userAutoId   		        = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWiseMsgInfo']	= $this->M_organize_profile_manage->findbyByGenUser('send_message_to_user', $userAutoId, $onset);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['onset'] 				= $onset;
				$config['base_url'] 		= base_url('generalUserHome/viewMessage');
				$config['total_rows'] 		= $this->M_organize_profile_manage->countAll('send_message_to_user', $userAutoId);
				$config['uri_segment'] 		= 3;
				$config['per_page'] 		= 10;
				$config['num_links'] 		= 7;
				$config['first_link']		= FALSE;
				$config['last_link'] 		= FALSE;
				$config['prev_link']		= 'Prev';
				$config['next_link'] 		= 'Next';
		
				$this->pagination->initialize($config); 
				
				$this->load->view('generalUserPanel/messageViewPage', $data);
				
			
			}
		}


		public function messageReplyAction($msgViewId)
		{
			if(isAuthenticate()){
               $user_id             		= getUserName();
               $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			}

			   $data['message_id']   		= $msgViewId;
			   $data['rep_user_id']   		= $userDetails->id;
			   $data['reply']   			= $this->input->post('reply');
			   $data['date']   				= date('Y-m-d');

			   $this->M_organize_profile_manage->save('message_reply', $data);

			   redirect('generalUserHome/replyListView/'.$msgViewId);
			



		}


		public function messageDetails($msgViewId)
		{
			
			  $data['msgViewId']            = $msgViewId;
			  $data['msgWiseDetails']   	= $this->M_organize_profile_manage->findByMessageId('send_message_to_user', $msgViewId); 
			  $data['msgWiseRep']   	    = $this->M_organize_profile_manage->findByReply('message_reply', $msgViewId); 


              $this->load->view('generalUserPanel/msgDetailsViewPage', $data);
				
		}

		public function replyListView($msgViewId)
	
		{	
			$data['page_title']  		= self::Title;	
			$data['msgWiseRep']   	    = $this->M_organize_profile_manage->findByReply('message_reply', $msgViewId); 
			$data['msgViewId']			= $msgViewId;
			
			$this->load->view('generalUserPanel/replyListPage', $data);
	
		}
		
	   
	   
	   
	   
	   public function jobApplied()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userAutoId   		        = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['totalJobApp']		= $this->M_job_application_details->findAllByUser($userAutoId);
				$data['countJobApp']		= $this->M_job_application_details->findByUser($userAutoId);
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('generalUserPanel/jobApplyPage', $data);
				
			
			}
		}
	   
	   
	   public function shortListCom()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userAutoId   		        = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['shortListCom']		= $this->M_job_application_details->shortListCom($userAutoId);
				$data['countShortList']		= $this->M_job_application_details->shortList($userAutoId);
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('generalUserPanel/shortListComPage', $data);
				
			
			}
		}
	   
	   public function viewCompany()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userAutoId   		        = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['viewCompany']		= $this->M_job_application_details->findByView($userAutoId);
				$data['totalViewApp']		= $this->M_job_application_details->viewApplication($userAutoId);
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('generalUserPanel/viewCompanyPage', $data);
				
			
			}
		}
	   
	   
	 public function interestedJobPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('generalUserPanel/interestedJobPostPage', $data);
			
			}
		}
		
		
		
		public function addPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('generalUserPanel/addPostPage', $data);
			
			}
		}
		
		public function addPostStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$regionId                         	= $userDetails->region_id;
		  	$regionName   		        		= $this->M_region_manage->findById($regionId);
		  	$data['region'] 					= $regionName->region_name;
			$data['user_id']     				= $userDetails->id;
			$data['title'] 						= $this->input->post('title');
			$data['status'] 	    			= "aprove";
			$data['image_title'] 				= $this->input->post('image_title');
			$data['date_and_time'] 				= $this->input->post('date_and_time');
			$data['description'] 				= $this->input->post('description');
			
				
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				$file_field					= 'image';
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')) {
				$uploadData = $this->upload->data();
				$data['image'] = $uploadData['file_name'];
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadData['full_path'];
				$config['maintain_ratio'] = TRUE;
				$config['new_image'] = './Images/Post_image/';
				$config['width'] = 50;
				$config['height'] = 50;
				
				$this->image_lib->initialize($config); 
				
				$this->image_lib->resize();
				}
				
			
			
			    $blog_id = $this->M_user_post_manage->save($data);
			    $blogDetails 					   = $this->M_user_post_manage->findById($blog_id);

				$datas['blog_id'] 				   = $blog_id;
				$datas['blog_user_id']     		   = $blogDetails->user_id;
				$datas['blog_title'] 			   = $blogDetails->title;

				$this->M_search_table->save($datas);

			redirect('generalUserHome/addPost');
		  }
	  }
	  
	
	 public function profileUpdate($id)
	   {
		   $model   					= $this->M_all_user_registration->findById($id);
		   $data['profileEditInfo'] 	= $model;
		   $data['moreprofileEditInfo'] = $this->M_general_user_registration->findByUserName($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']	    	= $this->M_country_manage->findAllCountry();
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/profileUpdatePage', $data);
		 }
	   }
	   
	   
	   
	    public function careerUpdate($id)
	   {
		   
		   $data['moreprofileEditInfo'] = $this->M_general_user_registration->findByUserName($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']			= $this->M_country_manage->findAll();	
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/careerUpdatePage', $data);
		 }
	   }
	   
	   public function careerInformationUpdate($id)
	   {
		   
		   $data['moreprofileEditInfo'] = $this->M_general_user_registration->findByUserName($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']			= $this->M_country_manage->findAll();	
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/careerInformationUpdatePage', $data);
		 }
	   }
	   
	   public function carrerAction()
			{
				$userId							= $this->input->post('id');
				$data['objective']				= $this->input->post('objective');
				$data['present_sallary']		= $this->input->post('present_sallary');
				$data['expected_sallary']		= $this->input->post('expected_sallary');
				$data['level'] 					= $this->input->post('level');
				$data['available'] 				= $this->input->post('available');
				$currentDate 					= date("Y/m/d");
				$data['resume_update_date'] 	= $currentDate;

				$valueChk 						= $this->M_general_user_registration->findByEmail($userId);	
				if(!empty($valueChk)){
				  $this->M_general_user_registration->update($data, $userId);		
				}else{
				  $data['user_id'] 				= $userId;
				  $this->M_general_user_registration->save($data);		
				}
				 
				 redirect('generalUserHome/profileUpdate/'.$userId);	
			 }
	   
	   public function educationUpdate($id)
	   {
		   
		   $data['moreprofileEditInfo'] = $this->M_general_user_registration->findByUserName($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']			= $this->M_country_manage->findAll();	
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/educationUpdatePage', $data);
		 }
	   }
	   
	   public function educationAction()
			{
				$userId								= $this->input->post('id');
				$data['education_level']			= $this->input->post('education_level');
				$data['degree_title']				= $this->input->post('degree_title');
				$data['group']						= $this->input->post('group');
				$data['institute_name'] 			= $this->input->post('institute_name');
				$result 							= $this->input->post('result');
				$data['result'] 					= $this->input->post('result');
				if($result == 'Grade'){
				    $data['cgpa']						= $this->input->post('cgpa');
					$data['scale']						= $this->input->post('scale');
				}else{
					$data['cgpa']						= "";
					$data['scale']						= "";
				}
				$data['pass_year']					= $this->input->post('pass_year');
				$data['duration'] 					= $this->input->post('duration');
				$data['achievement'] 				= $this->input->post('achievement');
				$currentDate 						= date("Y/m/d");
				$data['resume_update_date'] 		= $currentDate;

				$valueChk 							= $this->M_general_user_registration->findByEmail($userId);	
				if(!empty($valueChk)){
				  $this->M_general_user_registration->update($data, $userId);		
				}else{
				  $data['user_id'] 					= $userId;
				  $this->M_general_user_registration->save($data);		
				}
				 redirect('generalUserHome/profileUpdate/'.$userId);	
			 }
	   
	   public function traningUpdate($id)
	   {
		   
		   $data['moreprofileEditInfo'] = $this->M_general_user_registration->findByUserName($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']			= $this->M_country_manage->findAll();	
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/traningUpdatePage', $data);
		 }
	   }
	   
	   
	    public function trainingAction()
			{
				$userId									= $this->input->post('id');
				$data['training_title']					= $this->input->post('training_title');
				$data['training_tropic']				= $this->input->post('training_tropic');
				$data['training_institute']				= $this->input->post('training_institute');
				$data['training_country'] 				= $this->input->post('training_country');
				$data['training_location']				= $this->input->post('training_location');
				$data['training_year'] 					= $this->input->post('training_year');
				$data['training_duration'] 				= $this->input->post('training_duration');
				$currentDate 							= date("Y/m/d");
				$data['resume_update_date'] 			= $currentDate;

				$valueChk 								= $this->M_general_user_registration->findByEmail($userId);	
				if(!empty($valueChk)){
				  $this->M_general_user_registration->update($data, $userId);		
				}else{
				  $data['user_id'] 						= $userId;
				  $this->M_general_user_registration->save($data);		
				}
				   
				   redirect('generalUserHome/profileUpdate/'.$userId);	
			 }
	   
	   public function changPassword($id)
	   {
		   $model   					= $this->M_all_user_registration->findById($id);
		   $data['profileEditInfo'] 	= $model;
		  
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']			= $this->M_country_manage->findAll();	
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/changPasswordPage', $data);
		 }
	   }
	   
	   
	    public function viewResume($id)
	     {
		   $model   					= $this->M_all_user_registration->findById($id);
		   $data['id']                  = $id;
		   $data['resumeInfo'] 			= $model;
		   $data['moreResumeInfo'] 		= $this->M_general_user_registration->findByUserName($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
		   $data['page_title']  		= self::Title;
		   $data['regionInfo']			= $this->M_region_manage->findAll();
		   $data['countryInfo']			= $this->M_country_manage->findAll();	
		   $data['cityInfo']			= $this->M_city_manage->findAll();	
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/viewResumePage', $data);
		 }
	   }
	   public function pdfResume($id)
		{
			$data['id']     				= $id;
			$model   						= $this->M_all_user_registration->findById($id);
			$data['resumeInfo'] 			= $model;
			$data['moreResumeInfo'] 		= $this->M_general_user_registration->findByUserName($id);
	
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
				$data['page_title']  		= self::Title;
				$data['regionInfo']			= $this->M_region_manage->findAll();
				$data['countryInfo']			= $this->M_country_manage->findAll();
				$data['cityInfo']			= $this->M_city_manage->findAll();
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	
				$this->load->view('generalUserPanel/pdfResumePage', $data);
			}
		}
	   
	   
	   public function postEdit($id)
	   {
		   $data['postEditInfo']  		= $this->M_user_post_manage->findById($id);
		   
		   if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
		   $data['userDetails']   		= $userDetails;
		   $userId                      = $userDetails->id;
		   $data['page_title']  		= self::Title;
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                       =  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['UserWisePostInfo']	    = $this->M_user_post_manage->findByUser($userId);
		   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('generalUserPanel/postEditPage', $data);
		 }
	   }
	   
	   
	    public function intrestEdit($id)
		   {
			   $data['intrestEditInfo']  	= $this->M_interested_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   
			   $this->load->view('generalUserPanel/interestJobEditPage', $data);
			 }
		   }
		   
		    public function updatePass()
			{
				$id						= $this->input->post('id');
				$oldPass				= $this->input->post('password');
				$newPassword			= $this->input->post('newPassword');
				$conPassword			= $this->input->post('conpass');
				$data['password'] 		= $this->input->post('newPassword');
				$userDetails   		    = $this->M_all_user_registration->findById($id);
				$dbPass                = $userDetails->password;
				 if($newPassword == $conPassword){
					if($oldPass == $dbPass){
						$this->M_all_user_registration->update($data, $id);	
						redirect('generalUserHome/changSuccessPassword');	
					}else{
					 $this->load->view('generalUserPanel/changPasswordAlertPage', $data);
					}
				}else{
				   $this->load->view('generalUserPanel/changPasswordAlert2Page', $data);
				}
			 }
			 
			 public function changSuccessPassword()
			   {
				   
				   
				   if(isAuthenticate()){
				   $user_id             		= getUserName();
				   $data['user_id']     		= $user_id;
				   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				   $data['userDetails']   		= $userDetails;
				   $userId                      = $userDetails->id;
				   $data['page_title']  		= self::Title;
				   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
				   $where                       =  array('position_top' => 'Yes','status' => 'Available');
				   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				   $data['menuInfo']			= $menuInfo;
				   $data['UserWisePostInfo']	    = $this->M_user_post_manage->findByUser($userId);
				   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				   
				   $this->load->view('generalUserPanel/changSuccessPasswordPage', $data);
				 }
			   }
			   
			   
	   
	   public function postUpdate()
	    {
			$id						= $this->input->post('id');
			$data['title'] 			= $this->input->post('title');
			$data['image_title'] 	= $this->input->post('image_title');
			$data['date_and_time'] 	= $this->input->post('date_and_time');
			$data['description'] 	= $this->input->post('description');
		
			$config['upload_path'] 		= './Images/Post_image/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '0';
			$config['max_width']  		= '0';
			$config['max_height']  		= '0';
			$config['file_name']  		= time();
			$file_field					= 'image';
		
			/// IMAGE UPLOAD ACTION
			$this->upload->initialize($config);
			if ($this->upload->do_upload($file_field))
			{
				/// ERORR SHOW FOR IMAGE UPLOAD
			
				$image_data 			= $this->upload->data();				
				$data['image']  		= $image_data['file_name'];
				$model					= $this->M_user_post_manage->findById($id);
				$only_image    			= $model->image;
				
				if(!empty($only_image)){
				unlink('./Images/Post_image/'.$only_image);
				}
			}

			$this->M_user_post_manage->update($data, $id);	

			$blogDetails 			    = $this->M_user_post_manage->findById($id);


				$datas['blog_title'] 	    = $blogDetails->title;

			    $blogDetailsChk 			= $this->M_search_table->findByBlogId($id);

			    if(!empty($blogDetailsChk)){
			       $this->M_search_table->updateBlog($datas, $id);
			    } else{
				  $datas['blog_id'] 	    = $id;
				  $datas['blog_user_id']    = $blogDetails->user_id;

			      $this->M_search_table->save($datas);	
			    }
	
			redirect('generalUserHome/addPost');
			
	     }
		 
		 
		 
		  public function interestJobUpdate()
			{
				$id										= $this->input->post('id');
				$data['education_provider_type'] 		= $this->input->post('education_provider_type');
				$data['interested_program'] 			= $this->input->post('interested_program');
				$data['location'] 						= $this->input->post('location');
				$data['other'] 							= $this->input->post('other');
	
				$this->M_interested_job_post_manage->update($data, $id);		
				redirect('generalUserHome/interestedJobPost');
				
			 }
			 
			 
	    public function postDelete($id)
		 {	
			
			$model					= $this->M_user_post_manage->findById($id);
			$only_image    			= $model->image;
			
			if(!empty($only_image)){
			unlink('./Images/Post_image/'.$only_image);
			}
			$this->M_user_post_manage->destroy($id);
			$this->M_search_table->destroyByBlockId($id);				
		 	redirect('generalUserHome/addPost');
		 }
		 
		 public function intJobDelete($id)
		 {	
			$this->M_interested_job_post_manage->destroy($id);		
		 	redirect('generalUserHome/interestedJobPost');
		 }
	   
	   public function interestJobStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['education_provider_type'] 	= $this->input->post('education_provider_type');
			$data['interested_program'] 	    = $this->input->post('interested_program');
			$data['location'] 					= $this->input->post('location');
			$data['other'] 						= $this->input->post('other');
			
			$this->M_interested_job_post_manage->save($data);
			redirect('generalUserHome/interestedJobPost');
		  }
		  
		}
	  
	  
	  public function countryEdit()
		{
			$region_id 	   = $this->input->post('id');
			$countryList   = $this->M_country_manage->findAllInfo($region_id);				
		
			echo '<option value="">Sellect Country Name</option>';
			foreach($countryList as $v) {
				echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';
			}
			
		}
		
		
		 public function cityEdit()
			{
				$country_id 	= $this->input->post('id');
				$cityList   	= $this->M_city_manage->findAllCity($country_id);				
			
				echo '<option value="">Sellect City Name</option>';
				foreach($cityList as $v) {
					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';
				}
				
			}
			
			
			
			
			public function update()
			{
				$id								= $this->input->post('id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['name'] 					= $this->input->post('name');
				$data['status'] 				= $this->input->post('status');
				
				
					$config['upload_path']		= './Images/Register_image/';
					$config['allowed_types'] 	= 'gif|jpg|png';
					$config['max_size']			= '0';
					$config['max_width']  		= '0';
					$config['max_height']  		= '0';
					$config['file_name']  		= time();
					$file_field					= 'image';
				
					/// IMAGE UPLOAD ACTION
					$this->upload->initialize($config);
					if ($this->upload->do_upload($file_field))
					{
						
					
						$image_data 			= $this->upload->data();				
						$data['image']  		= $image_data['file_name'];
						$model					= $this->M_all_user_registration->findById($id);
						$only_image    			= $model->image;
						
						if(!empty($only_image)){
						unlink('./Images/Register_image/'.$only_image);
						}
					}
				$this->M_all_user_registration->update($data, $id);	
				
				$currentDate 					= date("Y/m/d");
				$data2['resume_update_date'] 	= $currentDate;
				$data2['education_institute']   = $this->input->post('education_institute');
				$data2['date_of_birth'] 		= $this->input->post('date_of_birth');
				$data2['father_name'] 			= $this->input->post('father_name');
				$data2['mother_name'] 			= $this->input->post('mother_name');
				$data2['nationality'] 			= $this->input->post('nationality');
				$data2['gender'] 				= $this->input->post('gender');
				$data2['marital_status'] 		= $this->input->post('marital_status');
				$data2['religion'] 				= $this->input->post('religion');
				$data2['address'] 				= $this->input->post('address');
				$data2['city'] 					= $this->input->post('city');
				$data2['country_phone'] 		= $this->input->post('country_phone');
				$data2['mobile'] 				= $this->input->post('mobile');
				$data2['email'] 				= $this->input->post('email');
				$data2['address_permanent'] 	= $this->input->post('address_permanent');
				$data2['city_permanent'] 		= $this->input->post('city_permanent');
				$data2['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data2['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data2['email_permanent'] 		= $this->input->post('email_permanent');
		        
				$valueChk 						= $this->M_general_user_registration->findByEmail($id);	
				if(!empty($valueChk)){
				  $this->M_general_user_registration->update($data2, $id);		
				}else{
				  $data2['user_id'] 			= $id;
				  $this->M_general_user_registration->save($data2);		
				}

				redirect('generalUserHome');
				
			 }	
			 
			 
		
	public function logout()
	{	
		logoutUser();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */