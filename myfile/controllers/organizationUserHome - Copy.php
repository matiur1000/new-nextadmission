<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OrganizationUserHome extends CI_Controller {
	static $model 	= array('M_basic_manage','M_advertisement_manage','M_region_manage','M_country_manage','M_city_manage','M_more_campus_details',
	'M_all_user_registration','M_interested_job_post_manage','M_user_post_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_payment_gateaway','M_payment_by_user','M_organization_user_registration','M_general_user_registration','M_programe_manage','M_job_post_manage','M_job_application_details','M_add_manage','M_payment_manage');
	static $helper   = array('url','userauthentication');
	
	const  Title	 = 'FIND GLOBAL STUDY';
	
	public function __construct(){
		parent::__construct();
		$this->load->database(); // TO LOAD DATABASE CONNECTIVITY
		$this->load->model(self::$model); // TO LOAD ALL MODEL
		$this->load->helper(self::$helper);// TO LOAD URL BASE HELPER LIKE BASE_URL, SITE_URL
		$this->load->library('form_validation'); // LOAD THIS LIBRARY TO USE FORM VALIDATION 
		$this->load->library('pagination'); // LOAD THIS LIBRARY TO USE PAGINATION
		$this->load->library('upload');  // LOAD THIS LIBRARY TO FILE UPLOAD
		$this->load->library('image_lib');
		$this->load->library('session');
		isAuthenticate();
	}

	public function index()
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;
			$data['totalApplication']   = $this->M_job_application_details->countApplicationJob($userAutoId);
			$data['totalViewJob']       = $this->M_job_application_details->countViewJob($userAutoId);
			$data['totalShortViewJob']   = $this->M_job_application_details->countShortViewJob($userAutoId);
			$data['totalNotViewJob']   = $this->M_job_application_details->countNotViewJob($userAutoId);	
			$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
			$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
			$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);
			$data['OrganizePostInfo']	= $this->M_job_post_manage->findAllDetailes($userAutoId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/organizationUserHomePage', $data);
		
		}
	}
	   
	   
	public function jobWiseDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['jobId']     			= $jobId;
			$data['page_title']  		= self::Title;	
			$data['instituteSub']	    = $this->M_general_user_registration->findByAllInstitute();
			$data['subName']	    	= $this->M_general_user_registration->findByAllSubject();
			$data['jobTitle']			= $this->M_job_post_manage->findById($jobId);
			$data['totalApp']			= $this->M_job_application_details->countJobWiseApplication($jobId);
			$data['totalViewList']		= $this->M_job_application_details->countJobWiseView($jobId);
			$data['totalNotViewApp']    = $this->M_job_application_details->countJobWiseNotView($jobId);
			$data['totalShortList']		= $this->M_job_application_details->countJobWiseShortList($jobId);
			$data['totalStarCandidets']	= $this->M_job_application_details->countJobWiseStarCandidates($jobId);
			$data['jobWiseApplication']	= $this->M_job_application_details->findByJobId($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/jobWiseDetailsPage', $data);
		
		}
	}
	
	public function pdfResume($id)
		{
			$data['id']     				= $id;
			$model   						= $this->M_all_user_registration->findById($id);
			$data['resumeInfo'] 			= $model;
			$data['moreResumeInfo'] 		= $this->M_general_user_registration->findByUserName($id);
	
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
				$data['page_title']  		= self::Title;
				$data['regionInfo']			= $this->M_region_manage->findAll();
				$data['countryInfo']		= $this->M_country_manage->findAll();
				$data['cityInfo']			= $this->M_city_manage->findAll();
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	
				$this->load->view('generalUserPanel/pdfResumePage', $data);
			}
		}
	
	public function cvViewDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;	
			$data['cvViewApplicant']	= $this->M_job_application_details->findByCvView($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/cvViewDetailsPage', $data);
		
		}
	}
	
	   
	   
	public function cvNotViewDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;	
			$data['cvNotViewApplicant']	= $this->M_job_application_details->findByCvNotView($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/cvNotViewDetailsPage', $data);
		
		}
	}
	  
	public function shortListDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;	
			$data['shortListApplicant']	= $this->M_job_application_details->findByshortList($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/shortListDetailsPage', $data);
		
		}
	}
	
	public function starCandidateDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;	
			$data['starCanApplicant']	= $this->M_job_application_details->findBystarCan($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/starCandidateDetailsPage', $data);
		
		}
	}
	   public function changPassword($id)
	   {
		   $model   					= $this->M_all_user_registration->findById($id);
		   $data['profileEditInfo'] 	= $model;
		  
		   
		if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
		   $data['userDetails']   		= $userDetails;
		   $data['activeUser']   		= $userDetails->name;
		   $userAutoId   		        = $userDetails->id;
		   $data['page_title']  		= self::Title;
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      	=  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('organizationUserPanel/changPasswordPage', $data);
		 }
	   }
	   
	    public function updatePass()
			{
				$id						= $this->input->post('id');
				$oldPass				= $this->input->post('password');
				$newPassword			= $this->input->post('newPassword');
				$conPassword			= $this->input->post('conpass');
				$data['password'] 		= $this->input->post('newPassword');
				$userDetails   		    = $this->M_all_user_registration->findById($id);
				$dbPass                = $userDetails->password;
				 if($newPassword == $conPassword){
					if($oldPass == $dbPass){
						$this->M_all_user_registration->update($data, $id);	
						redirect('organizationUserHome/changSuccessPassword');	
					}else{
					 $this->load->view('organizationUserPanel/changPasswordAlertPage', $data);
					}
				}else{
				   $this->load->view('organizationUserPanel/changPasswordAlert2Page', $data);
				}
			 }
			 
	   
	    public function changSuccessPassword()
	   {
		  
		if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
		   $data['userDetails']   		= $userDetails;
		   $data['activeUser']   		= $userDetails->name;
		   $userAutoId   		        = $userDetails->id;
		   $data['page_title']  		= self::Title;
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      	=  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('organizationUserPanel/changSuccessPasswordPage', $data);
		 }
	   }
	  
	  
	public function shortListAction()
	{ 		
		$applicantList = $this->input->post('applicant_id');
		
		foreach($applicantList as $applicantId) {
			if($applicantId){
				$data2['shortlist_status']	= "yes";
				$data2['short_list_date']	= date("d/m/Y");
				$this->M_job_application_details->update($data2, $applicantId);
			}
		}
		//redirect('organizationUserHome'); 
	}
	
	public function starCandidateAction()
	{ 		
		$applicantList = $this->input->post('applicant_id');
		
		foreach($applicantList as $applicantId) {
			if($applicantId){
				$data2['star_candidate_status']	= "yes";
				$this->M_job_application_details->update($data2, $applicantId);
			}
		}
		//redirect('organizationUserHome'); 
	}
	
	
	 public function interestedJobPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$data['activeUser']   		= $userDetails->name;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('organizationUserPanel/interestedJobPostPage', $data);
			
			}
		}
		
		public function wantAdd()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$data['activeUser']   		= $userDetails->name;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['userType']	        = $this->M_payment_gateaway->findAll();
				$data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('organizationUserPanel/wantAddPage', $data);
			
			}
		}
		
		 public function wantAddStore()
			{	
			 if(isAuthenticate()){
				$user_id             				= getUserName();
				$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
				$user_autoId                        = $userDetails->id;
				$data['activeUser']   				= $userDetails->name;
				$data['user_id']                    = $userDetails->id;
				
				$data['premium_user_type'] 			= $this->input->post('premium_user_type');
				$data['amount'] 	    			= $this->input->post('amount');
				$data['payment_by'] 				= $this->input->post('payment_by');
				
				$data1['premium_status'] 			= '1';
				
				$this->M_all_user_registration->updatStatus($data1, $user_autoId);
				$this->M_payment_by_user->save($data);
				redirect('premiumUserHome/wantAdd'); 
			  }
			  
			}
		
		
		
		public function addPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$data['activeUser']   		= $userDetails->name;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findByUser($userId);
				$this->load->view('organizationUserPanel/addPostPage', $data);
			
			}
		}
		
		
		 public function postEdit($id)
		   {
			   $data['postEditInfo']  		= $this->M_user_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWisePostInfo']	= $this->M_user_post_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/postEditPage', $data);
			 }
		   }
		   
		   public function programeManage()
			{   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['activeUser']   		= $userDetails->name;
					$data['userDetails']   		= $userDetails;
					$userId                     = $userDetails->id;
					$data['page_title']  		= self::Title;
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
					$this->load->view('organizationUserPanel/programeManagePage', $data);
				
				}
			}
			
			
		
		public function programStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['orgnization_id']     		= $userDetails->id;
			$data['programe_name'] 				= $this->input->post('programe_name');
			$data['course_duration'] 			= $this->input->post('course_duration');
			$data['programe_details'] 			= $this->input->post('programe_details');
			
			$this->M_programe_manage->save($data);
			redirect('organizationUserHome/programeManage');
		  }
		  
		}
		
		
		 public function programeEdit($id)
		   {
			   $data['progEditInfo']  		= $this->M_programe_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/programeEditPage', $data);
			 }
		   }
		   
		    public function jobPostEdit($id)
		   {
			   $data['jobPostEditInfo']  		= $this->M_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/jobPostEditPage', $data);
			 }
			 
			 
			 
		   }
		   
		   public function jobPostDraft($id)
		   {
			  
			   $data['draft_job_status']    = "yes";
			     $this->M_job_post_manage->update($data, $id);		
				redirect('organizationUserHome');
			
		   }
		   
		   public function jobPostArchived($id)
		   {
			  
			   $data['archive_status']    = "yes";
			     $this->M_job_post_manage->update($data, $id);		
				redirect('organizationUserHome');
			
		   }
		   
		   public function jobPostDelete($id)
		   {
			
			   $this->M_job_post_manage->destroy($id);		
				redirect('organizationUserHome');
			
		   }
		   
		   public function programUpdate()
			{
				$id									= $this->input->post('id');
				$data['programe_name'] 				= $this->input->post('programe_name');
				$data['course_duration'] 			= $this->input->post('course_duration');
				$data['programe_details'] 			= $this->input->post('programe_details');
	
				$this->M_programe_manage->update($data, $id);		
				redirect('organizationUserHome/programeManage');
				
			 }
			 
			 
			 public function jobPostUpdateAction()
			{
				$id									= $this->input->post('id');
				$data['add_type'] 				    = $this->input->post('add_type');
				$data['job_category'] 			    = $this->input->post('job_category');
				$data['organization_type'] 			= $this->input->post('organization_type');
				$data['title'] 			            = $this->input->post('title');
				$data['vacancies'] 			        = $this->input->post('vacancies');
				$receive_online			            = $this->input->post('receive_online');
				$receive_email 			            = $this->input->post('receive_email');
				$receive_hardcopy 		        	= $this->input->post('receive_hardcopy');
				if(!empty($receive_online)){
				  $data['receive_online'] 			= $receive_online;
				}else if(!empty($receive_email)){
				  $data['receive_online'] 			= $receive_email;
				}else{
				  $data['receive_online'] 			= $receive_hardcopy;
				}
				$data['cv_enclose'] 			    = $this->input->post('cv_enclose');
				$data['apllication_deadline'] 		= $this->input->post('apllication_deadline');
				$data['contact_person'] 			= $this->input->post('contact_person');
				$data['desplay_orgname'] 			= $this->input->post('desplay_orgname');
				$data['desplay_address'] 			= $this->input->post('desplay_address');
				$data['age_range_from'] 			= $this->input->post('age_range_from');
				$data['age_range_to'] 			    = $this->input->post('age_range_to');
				$data['gender'] 			        = $this->input->post('gender');
				$data['job_type'] 			        = $this->input->post('job_type');
				$data['job_level'] 			        = $this->input->post('job_level');
				$data['education_qualification'] 	= $this->input->post('education_qualification');
				$data['job_description'] 			= $this->input->post('job_description');
				$data['job_requirements'] 			= $this->input->post('job_requirements');
				$data['job_experience'] 			= $this->input->post('job_experience');
				$data['exprience_min'] 			    = $this->input->post('exprience_min');
				$data['exprience_max'] 			    = $this->input->post('exprience_max');
				$data['sallary_range'] 			    = $this->input->post('sallary_range');
				$data['sallary_money_min'] 			= $this->input->post('sallary_money_min');
				$data['sallary_money_max'] 			= $this->input->post('sallary_money_max');
				$data['job_publish_status'] 		= $this->input->post('job_publish_status');
				
				
	
				$this->M_job_post_manage->update($data, $id);		
				redirect('organizationUserHome');
				
			 }
			 
			 
		 public function viewJob($id)
		   {
			   $data['jobPostEditInfo']  		= $this->M_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/viewJobPage', $data);
			 }
			 
		   }
		
		  public function applyCvView($applyUserId, $postId)
		   {
			   $model   					= $this->M_all_user_registration->findById($applyUserId);
			   $data['resumeInfo'] 			= $model;
			   $data['moreResumeInfo'] 		= $this->M_general_user_registration->findByUserName($applyUserId);
			   
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['applyUserId']  		= $applyUserId;
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $data2['application_view_status']  		= "view";
			   $data2['cv_view_date']  					= date("d/m/Y");
			   $this->M_job_application_details->update2($data2, $applyUserId, $postId);	
			   
			   $this->load->view('organizationUserPanel/applyCvViewPage', $data);
			 }
			 
		   }
		
		   
		   
		    public function postUpdate()
			{
				$id						= $this->input->post('id');
				$data['title'] 			= $this->input->post('title');
				$data['image_title'] 	= $this->input->post('image_title');
				$data['date_and_time'] 	= $this->input->post('date_and_time');
				$data['description'] 	= $this->input->post('description');
			
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['max_size']			= '0';
				$config['max_width']  		= '0';
				$config['max_height']  		= '0';
				$config['file_name']  		= time();
				$file_field					= 'image';
			
				/// IMAGE UPLOAD ACTION
				$this->upload->initialize($config);
				if ($this->upload->do_upload($file_field))
				{
					/// ERORR SHOW FOR IMAGE UPLOAD
				
					$image_data 			= $this->upload->data();				
					$data['image']  		= $image_data['file_name'];
					$model					= $this->M_user_post_manage->findById($id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					unlink('./Images/Post_image/'.$only_image);
					}
				}
	
				$this->M_user_post_manage->update($data, $id);		
				redirect('organizationUserHome/addPost');
				
			 }
		   
		
		public function orgAddPostStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$regionId                         = $userDetails->region_id;
		  	$regionName   		        	= $this->M_region_manage->findById($regionId);
		  	$data['region'] 					= $regionName->region_name;
			$data['user_id']     				= $userDetails->id;
			$data['title'] 						= $this->input->post('title');
			$data['status'] 	    			= "aprove";
			$data['image_title'] 				= $this->input->post('image_title');
			$data['date_and_time'] 				= $this->input->post('date_and_time');
			$data['description'] 				= $this->input->post('description');
			
				
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				$file_field					= 'image';
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')) {
				$uploadData = $this->upload->data();
				$data['image'] = $uploadData['file_name'];
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadData['full_path'];
				$config['maintain_ratio'] = TRUE;
				$config['new_image'] = './Images/Post_image/';
				$config['width'] = 50;
				$config['height'] = 50;
				
				$this->image_lib->initialize($config); 
				
				$this->image_lib->resize();
				}
				
			
			
			$this->M_user_post_manage->save($data);
			redirect('organizationUserHome/addPost');
		  }
	  }
	  
	  
	  public function intrestEdit($id)
		   {
			   $data['intrestEditInfo']  	= $this->M_interested_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $userId                      = $userDetails->id;
			   $data['activeUser']   		= $userDetails->name;
			   $data['page_title']  		= self::Title;
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/interestJobEditPage', $data);
			 }
		   }
		   
		   
		   public function interestJobUpdate()
			{
				$id										= $this->input->post('id');
				$data['education_provider_type'] 		= $this->input->post('education_provider_type');
				$data['interested_program'] 			= $this->input->post('interested_program');
				$data['location'] 						= $this->input->post('location');
				$data['other'] 							= $this->input->post('other');
	
				$this->M_interested_job_post_manage->update($data, $id);		
				redirect('organizationUserHome/interestedJobPost');
				
			 }
	   
	  
	
		 public function profileUpdate($id)
		   {
			   $model   					= $this->M_all_user_registration->findById($id);
			   $data['activeUser']   		= $model->name;
			   $data['profileEditInfo'] 	= $model;
			   $data['moreEditInfo']   		= $this->M_organization_user_registration->findByUserName($id);
			   $data['anyMoreEditInfo']   	= $this->M_more_campus_details->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
			   $data['page_title']  		= self::Title;
			   $data['regionInfo']			= $this->M_region_manage->findAll();
			   $data['countryInfo']			= $this->M_country_manage->findAll();	
			   $data['cityInfo']			= $this->M_city_manage->findAll();	
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   
			   $this->load->view('organizationUserPanel/profileUpdatePage', $data);
			 }
		   }
	   
	   public function interestJobStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['education_provider_type'] 	= $this->input->post('education_provider_type');
			$data['interested_program'] 	    = $this->input->post('interested_program');
			$data['location'] 					= $this->input->post('location');
			$data['other'] 						= $this->input->post('other');
			
			$this->M_interested_job_post_manage->save($data);
			redirect('organizationUserHome/interestedJobPost');
		  }
		  
		}
	  
	  
	  public function countryEdit()
		{
			$region_id 	   = $this->input->post('id');
			$countryList   = $this->M_country_manage->findAllInfo($region_id);				
		
			echo '<option value="">Sellect Country Name</option>';
			foreach($countryList as $v) {
				echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';
			}
			
		}
		
		
		 public function cityEdit()
			{
				$country_id 	= $this->input->post('id');
				$cityList   	= $this->M_city_manage->findAllCity($country_id);				
			
				echo '<option value="">Sellect City Name</option>';
				foreach($cityList as $v) {
					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';
				}
				
			}
			
			 public function viewAmount()
				{
					$premium_user_type 	   = $this->input->post('id');
					
					//$where 			   = array('region_id' => $region_id);		
					$amount   	   		   = $this->M_payment_gateaway->findById($premium_user_type);				
				
					echo $amount->amount;
				}
			
			
			
			
			public function update()
			{
				$id								= $this->input->post('id');
				$data['name'] 	    			= $this->input->post('organizationname');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['user_type'] 				= "Organization User";
				$data['status'] 				= "Active";
				$data['city_id'] 				= $this->input->post('city_id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				
				$config['upload_path'] 		= './Images/Register_image/';
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['max_size']			= '0';
				$config['max_width']  		= '0';
				$config['max_height']  		= '0';
				$config['file_name']  		= time();
				$file_field					= 'image';
			
				/// IMAGE UPLOAD ACTION
				$this->upload->initialize($config);
				if ($this->upload->do_upload($file_field))
				{
					/// ERORR SHOW FOR IMAGE UPLOAD
				
					$image_data 			= $this->upload->data();				
					$data['image']  		= $image_data['file_name'];
					$model					= $this->M_all_user_registration->findById($id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					unlink('./Images/Register_image/'.$only_image);
					}
				}
				$this->M_all_user_registration->update($data, $id);	
				
				
				
				$data3['address'] 				= $this->input->post('address');
				$data3['phone_com'] 			= $this->input->post('phone_com2');
				$data3['mobile_com'] 			= $this->input->post('mobile_com2');
				$data3['email_com'] 			= $this->input->post('email_com');
				$data3['location_by_google'] 	= $this->input->post('location_by_google');
				$data3['founded'] 				= $this->input->post('founded');
				$data3['any_more_campus'] 		= $this->input->post('any_more_campus');
				$any_more_campus 				= $this->input->post('any_more_campus');
				
				$data3['contact_persons'] 		= $this->input->post('contact_persons');
				$data3['designation'] 			= $this->input->post('designation');
				$data3['phone'] 				= $this->input->post('phone');
				$data3['mobile'] 				= $this->input->post('mobile');
				$data3['email'] 				= $this->input->post('email');
				$data3['skype'] 				= $this->input->post('skype');
				$data3['web_site'] 				= $this->input->post('web_site');
				$data3['facebook'] 				= $this->input->post('facebook');
				$data3['tuiter'] 				= $this->input->post('tuiter');
				
				
				$type_of_organization 					= $this->input->post('type_of_organization');
				if($type_of_organization =='Other'){
				  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
				  $data3['type_of_organization_other'] 	= $this->input->post('type_of_organization_other');
				  $data3['type_of_education_provider'] 	= "";
				  $data3['agency_type_other'] 			= "";
				  $data3['agency_type'] 			    = "";
				  $data3['couching_centre'] 			= "";
				  $data3['professional_taining'] 		= "";
				  $data3['school_college'] 				= "";
					  
				  
				}else if($type_of_organization =='EducationProvider'){
				  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
				  $data3['type_of_education_provider'] 	= $this->input->post('type_of_education_provider');
				  $data3['type_of_organization_other'] 	= "";
				  $data3['agency_type_other'] 			= "";
				  $data3['agency_type'] 			    = "";
				  $data3['couching_centre'] 			= "";
				  $data3['professional_taining'] 		= "";
				  $type_of_education_provider 			= $this->input->post('type_of_education_provider');
					  if($type_of_education_provider =='School /College upto  12'){
						$data3['school_college'] 		= $this->input->post('school_college');
					  }else{
						$data3['school_college'] 		= "";
					  }
				  
				
				}else if($type_of_organization =='Agency'){
				 $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
				 $data3['agency_type'] 					= $this->input->post('agency_type');
				 $data3['type_of_education_provider'] 	= "";
				 $data3['type_of_organization_other'] 	= "";
				 $data3['couching_centre'] 				= "";
				 $data3['professional_taining'] 		= "";
				 $data3['school_college'] 				= "";
				 $agency_type 							= $this->input->post('agency_type');
				     if($agency_type =='Other'){
					    $data3['agency_type_other'] 			= $this->input->post('agency_type_other');
					 }else{
					    $data3['agency_type_other'] 			= "";
					 }
				  
				
				}else if($type_of_organization =='CouchingCentre'){
				  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
				  $data3['couching_centre'] 			= $this->input->post('couching_centre');
				  $data3['type_of_organization_other'] 	= "";
				  $data3['type_of_education_provider'] 	= "";
				  $data3['agency_type_other'] 			= "";
				  $data3['agency_type'] 			    = "";
				  $data3['professional_taining'] 		= "";
				  $data3['school_college'] 				= "";
					  
				}else{
				  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
				  $data3['professional_taining'] 		= $this->input->post('professional_taining');
				  $data3['type_of_organization_other'] 	= "";
				  $data3['type_of_education_provider'] 	= "";
				  $data3['agency_type_other'] 			= "";
				  $data3['agency_type'] 			    = "";
				  $data3['couching_centre'] 			= "";
				  $data3['school_college'] 				= "";
				
				}
				
				  
				$data3['type_of_ownership'] 		= $this->input->post('type_of_ownership');
				$data3['scholarship'] 				= $this->input->post('scholarship');
				$data3['accademic_exam_system'] 	= $this->input->post('accademic_exam_system');
					
				$this->M_organization_user_registration->update($data3, $id);
				
				if($any_more_campus == 'Yes'){
					
						$data2['organization_name'] 	= $this->input->post('campus_name');
						$data2['address'] 				= $this->input->post('address2');
						$data2['city'] 					= $this->input->post('city_id2');
						$data2['province'] 				= $this->input->post('region_id2');
						$data2['country'] 				= $this->input->post('country_id2');
						$data2['phone_com'] 			= $this->input->post('phone_com1');
						$data2['mobile_com'] 			= $this->input->post('mobile_com1');
						$data2['email_com'] 			= $this->input->post('email_com');
						$data2['location_by_google'] 	= $this->input->post('location_by_google');
						$data2['founded'] 				= $this->input->post('founded2');
						$this->M_more_campus_details->update($data2, $id);
					
					}	
				 				
				  redirect('organizationUserHome');
				
			 }
			 
			 
			 
			 public function draftedJob()
				{   
					if(isAuthenticate()){
						$user_id             		= getUserName();
						$data['user_id']     		= $user_id;
						$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
						$data['userDetails']   		= $userDetails;
						$data['activeUser']   		= $userDetails->name;
						$userAutoId   		        = $userDetails->id;
						$data['page_title']  		= self::Title;
						$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
						$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
						$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);	
						$data['draftPostInfo']	    = $this->M_job_post_manage->findAllDraft($userAutoId);
						$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
						$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
						$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
						$where                      =  array('position_top' => 'Yes','status' => 'Available');
						$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
						$data['menuInfo']			= $menuInfo;
						$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
						$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
						$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
						$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
						$this->load->view('organizationUserPanel/draftedJobPage', $data);
					
					}
				}
							
				
				public function ArchivedJob()
				{   
					if(isAuthenticate()){
						$user_id             		= getUserName();
						$data['user_id']     		= $user_id;
						$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
						$data['userDetails']   		= $userDetails;
						$data['activeUser']   		= $userDetails->name;
						$userAutoId   		        = $userDetails->id;
						$data['page_title']  		= self::Title;
						$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
						$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
						$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);	
						$data['draftPostInfo']	    = $this->M_job_post_manage->findAllArchived($userAutoId);
						$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
						$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
						$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
						$where                      =  array('position_top' => 'Yes','status' => 'Available');
						$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
						$data['menuInfo']			= $menuInfo;
						$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
						$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
						$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
						$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
						$this->load->view('organizationUserPanel/ArchivedJobPages', $data);
					
					}
				}
			 
			 
	public function PostNewJob()
			{   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['userDetails']   		= $userDetails;
					$data['activeUser']   		= $userDetails->name;
					$userAutoId   		        = $userDetails->id;
					$jobUserDetails   		    = $this->M_organization_user_registration->findByEmail($userAutoId);
					$data['orgType']   			= $jobUserDetails->type_of_organization;
					$data['contPerson']   		= $jobUserDetails->contact_persons;
					$data['desinition']   		= $jobUserDetails->designation;
					$data['page_title']  		= self::Title;	
					$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
					$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
					$where                      =  array('position_top' => 'Yes','status' => 'Available');
					$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
					$data['menuInfo']			= $menuInfo;
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$this->load->view('organizationUserPanel/PostNewJobPage', $data);
				
					}
				}
	
	
	        public function jobPostStore()
			{	
			 if(isAuthenticate()){
				$user_id             				= getUserName();
				$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
				$regionId                         	= $userDetails->region_id;
			  	$regionName   		        		= $this->M_region_manage->findById($regionId);
			  	$data['region'] 					= $regionName->region_name;
				$data['user_id']     				= $userDetails->id;
				$data['add_type'] 					= $this->input->post('add_type');
				$data['job_category'] 	    		= $this->input->post('job_category');
				$data['organization_type'] 			= $this->input->post('organization_type');
				$data['title'] 						= $this->input->post('title');
				$data['vacancies'] 					= $this->input->post('vacancies');
				$receive_online 					= $this->input->post('receive_online');
				$receive_email 						= $this->input->post('receive_email');
				$receive_hardcopy 					= $this->input->post('receive_hardcopy');
				if(!empty($receive_online)){
				  $data['receive_online'] 			= $receive_online;
				}else if(!empty($receive_email)){
				  $data['receive_online'] 			= $receive_email;
				}else{
				  $data['receive_online'] 			= $receive_hardcopy;
				}
				
				$data['cv_enclose'] 	   			= $this->input->post('cv_enclose');
				$data['apllication_deadline'] 	    = $this->input->post('apllication_deadline');
				$data['contact_person'] 			= $this->input->post('contact_person');
				$data['desplay_orgname'] 			= $this->input->post('desplay_orgname');
				$data['desplay_address'] 	    	= $this->input->post('desplay_address');
				
				
				$lastId  = $this->M_job_post_manage->save($data);
				$nextData	= array('lastId' => $lastId);
				$this->session->set_userdata($nextData);
				
				redirect('organizationUserHome/PostNewJobContinue');
			  }
			  
			}
	   
	   
	      
			 
			 
			 public function PostNewJobContinue()
			 {   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['userDetails']   		= $userDetails;
					$data['activeUser']   		= $userDetails->name;
					$userAutoId   		        = $userDetails->id;
					$data['updateId']   		= $this->session->userdata('lastId');
					$data['page_title']  		= self::Title;	
					$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
					$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
					$where                      =  array('position_top' => 'Yes','status' => 'Available');
					$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
					$data['menuInfo']			= $menuInfo;
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$this->load->view('organizationUserPanel/PostNewJobContinuePage', $data);
				
				}
			}
			
			
			public function PostNewCV()
			{   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['userDetails']   		= $userDetails;
					$data['activeUser']   		= $userDetails->name;
					$userAutoId   		        = $userDetails->id;
					$jobUserDetails   		    = $this->M_organization_user_registration->findByEmail($userAutoId);
					$data['orgType']   			= $jobUserDetails->type_of_organization;
					$data['contPerson']   		= $jobUserDetails->contact_persons;
					$data['desinition']   		= $jobUserDetails->designation;
					$data['page_title']  		= self::Title;	
					$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
					$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
					$where                      =  array('position_top' => 'Yes','status' => 'Available');
					$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
					$data['menuInfo']			= $menuInfo;
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$this->load->view('organizationUserPanel/PostNewCVPage', $data);
				
					}
				}
			
			
			
			
			 public function nextJobStore($updateId)
				{	
				 
					$data['age_range_from'] 			= $this->input->post('age_range_from');
					$data['age_range_to'] 	    		= $this->input->post('age_range_to');
					$data['gender'] 					= $this->input->post('gender');
					$data['job_type'] 					= $this->input->post('job_type');
					$data['job_level'] 					= $this->input->post('job_level');
					$data['education_qualification'] 	= $this->input->post('education_qualification');
					$data['job_description'] 	    	= $this->input->post('job_description');
					$data['job_requirements'] 			= $this->input->post('job_requirements');
					$data['job_experience'] 			= $this->input->post('job_experience');
					$data['exprience_min'] 	    		= $this->input->post('exprience_min');
					$data['exprience_max'] 	    		= $this->input->post('exprience_max');
					$data['sallary_range'] 				= $this->input->post('sallary_range');
					$data['sallary_money_min'] 	    	= $this->input->post('sallary_money_min');
					$data['sallary_money_max'] 	    	= $this->input->post('sallary_money_max');
					$data['job_publish_status'] 	    = $this->input->post('job_publish_status');
					
					
					 $this->M_job_post_manage->update($data,$updateId);
					redirect('organizationUserHome/PostNewJobContinue');
				  }
				  
				   public function addManage()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$this->load->view('organizationUserPanel/addManagePage', $data);
						
						}
					}
					
					public function addManageFinal()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$data['allRegion']	        = $this->M_region_manage->findAll();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$this->load->view('organizationUserPanel/addManageFinalPage', $data);
						
						}
					}
					
					public function addStore()
						{	
						 if(isAuthenticate()){
							$user_id             				= getUserName();
							$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
							$data['user_id']                    = $userDetails->id;
							$positon 							= $this->input->post('positon');
							$serialNo 	    					= $this->input->post('serial_no');
							$data['title'] 						= $this->input->post('title');
							$data['add_link'] 					= $this->input->post('add_link');
							$data['deadline_date'] 	    	    = $this->input->post('deadline_date');
							$data['description'] 	    	    = $this->input->post('description');
							$data['status'] 	    	    	= "notavailable";
							
							
							$config['upload_path'] 		= './Images/Add_image/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['file_name']		= time();
							$file_field					= 'add_image';
							
							$this->upload->initialize($config);
							if($this->upload->do_upload('add_image')) {
							$uploadData = $this->upload->data();
							$data['add_image'] = $uploadData['file_name'];
							}
				
							
							$this->M_add_manage->updateAdd($data, $positon, $serialNo);
							 $this->session->unset_userdata('payment');
							
							redirect('organizationUserHome/addManageSuccess'); 
						  }
						  
						}
						
						
						
						public function regionAddStore()
						{	
						 if(isAuthenticate()){
							$user_id             				= getUserName();
							$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
							$data['user_id']                    = $userDetails->id;
							$region 							= $this->input->post('region');
							$positon 							= $this->input->post('regionPositon');
							$serialNo 	    					= $this->input->post('region_serial_no');
							$data['title'] 						= $this->input->post('title');
							$data['add_link'] 					= $this->input->post('add_link');
							$data['deadline_date'] 	    	    = $this->input->post('deadline_date');
							$data['description'] 	    	    = $this->input->post('description');
							$data['status'] 	    	    	= "notavailable";
							
							
							$config['upload_path'] 		= './Images/Add_image/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['file_name']		= time();
							$file_field					= 'add_image';
							
							$this->upload->initialize($config);
							if($this->upload->do_upload('add_image')) {
							$uploadData = $this->upload->data();
							$data['add_image'] = $uploadData['file_name'];
							}
				
							
							$this->M_add_manage->updateRegionAdd($data, $region, $positon, $serialNo);
							 $this->session->unset_userdata('payment');
							
							redirect('organizationUserHome/addManageSuccess'); 
						  }
						  
						}
						
						public function addManageSuccess()
						{   
							if(isAuthenticate()){
								$user_id             		= getUserName();
								$data['user_id']     		= $user_id;
								$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
								$data['activeUser']   		= $userDetails->name;
								$data['userDetails']   		= $userDetails;
								$userId                     = $userDetails->id;
								$data['page_title']  		= self::Title;
								$data['userType']	        = $this->M_payment_gateaway->findAll();
								$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
								$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
								$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
								$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
								$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
								$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
								$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
								$this->load->view('organizationUserPanel/addManageSuccessPage', $data);
							
							}
						}
						
						
						public function addEdit($id)
						{   
							if(isAuthenticate()){
								$user_id             		= getUserName();
								$data['user_id']     		= $user_id;
								$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
								$data['activeUser']   		= $userDetails->name;
								$data['userDetails']   		= $userDetails;
								$userId                     = $userDetails->id;
								$data['page_title']  		= self::Title;
								$data['addEditInfo']		= $this->M_add_manage->findById($id);	
								$data['userType']	        = $this->M_payment_gateaway->findAll();
								$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
								$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
								$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
								$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
								$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
								$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
								$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
								$this->load->view('organizationUserPanel/addEditPage', $data);
							
							}
						}
						
						
						public function addUpdate($id)
						{	
						 if(isAuthenticate()){
							$user_id             				= getUserName();
							$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
							$data['user_id']                    = $userDetails->id;
							$data['title'] 						= $this->input->post('title');
							$data['add_link'] 					= $this->input->post('add_link');
							$data['deadline_date'] 	    	    = $this->input->post('deadline_date');
							$data['description'] 	    	    = $this->input->post('description');
							
							
							
							$config['upload_path'] 		= './Images/Add_image/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['file_name']		= time();
							$file_field					= 'add_image';
							
							$this->upload->initialize($config);
							if($this->upload->do_upload('add_image')) {
							$uploadData = $this->upload->data();
							$data['add_image'] = $uploadData['file_name'];
							}
				
							 $this->M_add_manage->update2($data, $id);
							
							redirect('organizationUserHome/addManageSuccess'); 
						  }
						  
						}
					
					public function addManageBkask()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$premiumType 			    = $this->input->post('premium_user_type');
							$amount 	        		= $this->input->post('amount');
							$paymentBy	                = $this->input->post('paymentBy');
							$usertypePament	    		= $this->M_payment_gateaway->findById($premiumType);
							$userTypeName               = $usertypePament->premium_user_type; 	
							$paymentUserData			= array('premiumType' => $userTypeName, 'amount' => $amount, 'paymentBy' => $paymentBy);
							$this->session->set_userdata($paymentUserData);
							
							$data['paymentBy'] 	        = $paymentBy;
							$data['usertypePament']	    = $this->M_payment_gateaway->findById($premiumType);	
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							if($paymentBy == 'bkask'){
								$this->load->view('organizationUserPanel/addManageBkaskPage', $data);
								
								} else if($paymentBy == 'mastercard') {
									$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManageMasterPage', $data);

									
								} else if($paymentBy == 'paypal') {
									$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManagePaypalPage', $data);
									
									
								} else if($paymentBy == 'visa') {
									$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManageVisaPage', $data);
									
									
								} else if($paymentBy == 'bank') {
									$this->load->view('organizationUserPanel/addManageBankPage', $data);
								
									
									} else {
										$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManageAmericanPage', $data);
									}						
						}
					}
					
					public function adManageTransactionBkask()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							
							$data['premiumType'] 		= $this->session->userdata('premiumType');
						    $data['amount'] 			= $this->session->userdata('amount');
							$data['paymentBy'] 			= $this->session->userdata('paymentBy');
							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							$this->load->view('organizationUserPanel/adManageTransactionBkaskPage', $data);			
						}
					}
					
					public function paymentStatus()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$userId                     = $userDetails->id;
							$data['userPayment']        = $this->M_payment_manage->findByUserPayment($userId);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							$this->load->view('organizationUserPanel/paymentStatusPage', $data);			
						}
					}
					
					public function adManageTransactionBank()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							
							$data['premiumType'] 		= $this->session->userdata('premiumType');
						    $data['amount'] 			= $this->session->userdata('amount');
							$data['paymentBy'] 			= $this->session->userdata('paymentBy');
							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							$this->load->view('organizationUserPanel/adManageTransactionBankPage', $data);			
						}
					}
					
					public function addManageTransactionBkaskAction()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							
							$data2['user_type'] 		= $this->session->userdata('premiumType');
						    $data2['amount'] 			= $this->session->userdata('amount');
							$data2['payment_by'] 		= $this->session->userdata('paymentBy');
							$data2['user_id'] 			= $userId;
							$data2['transaction_id'] 	= $this->input->post('transaction_id');
							$data2['payment_status'] 	= "Paid";
							$paymentConfirm             = array('payment' => "yes");
							$this->session->set_userdata($paymentConfirm);
							
							$this->M_payment_manage->save($data2);
			                 $this->load->view('organizationUserPanel/paymentSuccessAlertPage');	
						}
					}
					
					public function addManageBankTransVisaFinalAction()                                                                    
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							}
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
						 
							$data2['user_type'] 		= $this->session->userdata('premiumType');
						    $data2['amount'] 			= $this->session->userdata('amount');
							$data2['payment_by'] 		= "bank";
							$data2['user_id'] 			= $userId;
							$data2['transaction_id'] 	= $this->input->post('transaction_id');
							$data2['card_naumber']  	= $this->input->post('card_naumber');
							$data2['security_code'] 	= $this->input->post('security_code');
							$data2['expiry_date']   	= $this->input->post('expiry_date');
							$data2['card_holder_name'] 	= $this->input->post('card_holder_name');
							$data2['payment_status'] 	= "Paid";
							$paymentConfirm             = array('payment' => "yes");
							$this->session->set_userdata($paymentConfirm);
							
							$this->M_payment_manage->save($data2);
							$this->load->view('organizationUserPanel/paymentSuccessAlertPage');	
							
						
					}
					
					public function addManageTransactionBankAction()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['page_title']  		= self::Title;
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$premiumType 			    = $this->input->post('premium_user_type');
							$data['user_type'] 		    = $this->session->userdata('premiumType');
						    $data['amount'] 			= $this->session->userdata('amount');
							$data['payment_by'] 		= $this->session->userdata('paymentBy');
							$paymentbank	            = $this->input->post('payment_bank');
							$usertypePament	    		= $this->M_payment_gateaway->findById($premiumType);
							$userTypeName               = $usertypePament->premium_user_type; 	
							
							$data['paymentbank'] 	    = $paymentbank;
							$data['usertypePament']	    = $this->M_payment_gateaway->findById($premiumType);	
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							if($paymentbank == 'visab'){
							    $data['paymentbank'] 	    = $paymentbank;
								$this->load->view('organizationUserPanel/addManageBankFinalActionPage', $data);
								
								}else{
								    $data['paymentbank'] 	    = $paymentbank;
									$this->load->view('organizationUserPanel/addManageBankFinalActionMasterPage', $data);
							}						
						}
					}
					 
			   public function searchAllApplicant()

				{	
					$postId      		= $this->input->post('id');
					$applicantName      = $this->input->post('applicantName');
					$ageFrom      		= $this->input->post('ageFrom');
					$ageTo     			= $this->input->post('ageTo');
					$gender      		= $this->input->post('gender');
					$starcandidates     = $this->input->post('starcandidates');
					
					$degree      		= $this->input->post('degree');
					$degreeTitle      	= $this->input->post('degreeTitle');
					$subject     		= $this->input->post('subject');
					$result      		= $this->input->post('result');
					$institute     		= $this->input->post('institute');
					
					$where = array('job_application_details.post_id' => $postId);
					if(!empty($applicantName) || !empty($ageFrom) || !empty($ageTo) || !empty($gender) || !empty($starcandidates)) {
						if(!empty($applicantName)) 	$where['job_application_details.applicant_name'] = $applicantName;
						if(!empty($ageFrom)) 		$where['job_application_details.age'] = $ageFrom;
						if(!empty($ageTo)) 			$where['job_application_details.age'] = $ageTo;
						if(!empty($gender)) 		$where['job_application_details.gender'] = $gender;
						if(!empty($starcandidates)) $where['job_application_details.star_candidate_status'] = $starcandidates;
						
						$data['searchQuery'] = $this->M_job_application_details->search($where);
						
						$this->load->view('organizationUserPanel/searchResultPage', $data);
					}
					
					
					/*if(!empty($starcandidates) && !empty($gender))  // !empty a ,b start
					{
						if(!empty($applicantName) && !empty($ageFrom) && !empty($ageTo))// c d e
						{
							$data['searchQuery'] = $this->M_job_application_details->findByAbcde($starcandidates, $gender, $applicantName, $ageFrom, $ageTo, $postId);
							$this->load->view('organizationUserPanel/searchResultPage', $data);
						}
						else if(!empty($applicantName) && !empty($ageFrom))
						{
						    $data['searchQuery'] = $this->M_job_application_details->findByAbcd($starcandidates, $gender, $applicantName, $ageFrom, $postId);  
							$this->load->view('organizationUserPanel/searchResultPage', $data);
						}
						else if(!empty($ageFrom) && !empty($ageTo))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByAbde($starcandidates, $gender, $ageFrom, $ageTo, $postId);
							 $this->load->view('organizationUserPanel/searchResultPage', $data);  
						}
						else if(!empty($applicantName) && !empty($ageTo))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByAbce($starcandidates, $gender, $applicantName, $ageTo, $postId);  
							 $this->load->view('organizationUserPanel/searchResultPage', $data);
						}
						else if(!empty($applicantName))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByAbc($starcandidates, $gender, $applicantName, $postId);  
							 $this->load->view('organizationUserPanel/searchResultPage', $data);
						}
						else if(!empty($ageFrom))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByAbd($starcandidates, $gender, $ageFrom, $postId);  
							 $this->load->view('organizationUserPanel/searchResultPage', $data);
						}
						else if(!empty($ageTo))
						{
						    $data['searchQuery'] = $this->M_job_application_details->findByAbe($starcandidates, $gender, $ageTo, $postId); 
							$this->load->view('organizationUserPanel/searchResultPage', $data);   
						}else{
						    $data['searchQuery'] = $this->M_job_application_details->findByStarGender($starcandidates, $gender, $postId);   
							$this->load->view('organizationUserPanel/searchResultPage', $data); 
						}						
										
					}                                                   // a ,b end
					else if(!empty($starcandidates))					
					{
					if(!empty($applicantName) && !empty($ageFrom) && !empty($ageTo))
						{
						   $data['searchQuery'] = $this->M_job_application_details->findByAcde($starcandidates, $applicantName, $ageFrom, $ageTo, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName) && !empty($ageFrom))
						{
						   $data['searchQuery'] = $this->M_job_application_details->findByAcd($starcandidates, $applicantName, $ageFrom, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageFrom) && !empty($ageTo))
						{
						  $data['searchQuery'] = $this->M_job_application_details->findByAde($starcandidates, $ageFrom, $ageTo, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName) && !empty($ageTo))
						{
						  $data['searchQuery'] = $this->M_job_application_details->findByAce($starcandidates, $applicantName, $ageTo, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName))
						{
						   $data['searchQuery'] = $this->M_job_application_details->findByAc($starcandidates, $applicantName, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageFrom))
						{
						   $data['searchQuery'] = $this->M_job_application_details->findByAd($starcandidates, $ageFrom, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageTo))
						{
						  $data['searchQuery'] = $this->M_job_application_details->findByAe($starcandidates, $ageTo, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}else{
						   $data['searchQuery'] = $this->M_job_application_details->findByonlyStar($starcandidates, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						
					}
					else if(!empty($gender))					
					{
					if(!empty($applicantName) && !empty($ageFrom) && !empty($ageTo))
						{
						 	$data['searchQuery'] = $this->M_job_application_details->findByBcde($gender, $applicantName, $ageFrom, $ageTo, $postId);
							$this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName) && !empty($ageFrom))
						{
						 	$data['searchQuery'] = $this->M_job_application_details->findByBcd($gender, $applicantName, $ageFrom, $postId);
							$this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageFrom) && !empty($ageTo))
						{
						  	$data['searchQuery'] = $this->M_job_application_details->findByBde($gender, $ageFrom, $ageTo, $postId);
							$this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName) && !empty($ageTo))
						{
						    $data['searchQuery'] = $this->M_job_application_details->findByBce($gender, $applicantName, $ageTo, $postId);
							$this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByBc($gender, $applicantName, $postId);
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageFrom))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByBd($gender, $ageFrom, $postId);
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageTo))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByBe($gender, $ageTo, $postId);
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}else{
						     $data['searchQuery'] = $this->M_job_application_details->findByOnlyGender($gender, $postId); 
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						
					} else {  // a ,b empty start
					
						if(!empty($applicantName) && !empty($ageFrom) && !empty($ageTo))// c d e
						{
							$data['searchQuery'] = $this->M_job_application_details->findByCde($applicantName, $ageFrom, $ageTo, $postId);
							$this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName) && !empty($ageFrom))
						{
						    $data['searchQuery'] = $this->M_job_application_details->findByCd($applicantName, $ageFrom, $postId);  
							$this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageFrom) && !empty($ageTo))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByDe($ageFrom, $ageTo, $postId);
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 
 
						}
						else if(!empty($applicantName) && !empty($ageTo))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByCe($applicantName, $ageTo);  
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($applicantName))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByC($applicantName, $postId);  
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageFrom))
						{
						     $data['searchQuery'] = $this->M_job_application_details->findByD($ageFrom, $postId); 
							 $this->load->view('organizationUserPanel/searchResultPage', $data); 

						}
						else if(!empty($ageTo))
						{
						    $data['searchQuery'] = $this->M_job_application_details->findByE($ageTo, $postId);   
							$this->load->view('organizationUserPanel/searchResultPage', $data); 
 
						}				
										
					}
					 */
					
					
					 
					 
					/*if(!empty($degree) && !empty($degreeTitle))  // !empty f ,g start
					{
						if(!empty($subject) && !empty($result) && !empty($institute))// hij
						{
							$data['acSearchQuery'] = $this->M_job_application_details->findByFghij($degree, $degreeTitle, $subject, $result, $institute, $postId);
							$this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($result))
						{
						    $data['acSearchQuery'] = $this->M_job_application_details->findByFghi($degree, $degreeTitle, $subject, $result, $postId);  
							$this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result) && !empty($institute))
						{
						     $data['acSearchQuery'] = $this->M_job_application_details->findByFgij($degree, $degreeTitle, $result, $institute, $postId); 
							 $this->load->view('organizationUserPanel/searchResultPage2', $data);  
						}
						else if(!empty($subject) && !empty($institute))
						{
						     $data['acSearchQuery'] = $this->M_job_application_details->findByFghj($degree, $degreeTitle, $subject, $institute, $postId); 
							 $this->load->view('organizationUserPanel/searchResultPage2', $data);  
						}
						else if(!empty($subject))
						{
						     $data['acSearchQuery'] = $this->M_job_application_details->findByFgh($degree, $degreeTitle, $subject, $postId); 
							 $this->load->view('organizationUserPanel/searchResultPage2', $data);  
						}
						else if(!empty($result))
						{
						     $data['acSearchQuery'] = $this->M_job_application_details->findByFgi($degree, $degreeTitle, $result, $postId); 
							 $this->load->view('organizationUserPanel/searchResultPage2', $data);  
						}
						else if(!empty($institute))
						{
						    $data['acSearchQuery'] = $this->M_job_application_details->findByFgj($degree, $degreeTitle, $institute, $postId);
							$this->load->view('organizationUserPanel/searchResultPage2', $data);     
						} else {
						   $data['acSearchQuery'] = $this->M_job_application_details->findByDegreeTitle($degree, $degreeTitle, $postId);
							$this->load->view('organizationUserPanel/searchResultPage2', $data);     
						}						
										
					}                                                   // a ,b end
					else if(!empty($degree))					
					{
					if(!empty($subject) && !empty($result) && !empty($institute))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByFhij($degree, $subject, $result, $institute, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($result))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByFhi($degree, $subject, $result, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result) && !empty($institute))
						{
						  $data['acSearchQuery'] = $this->M_job_application_details->findByFij($degree, $result, $institute, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($institute))
						{
						  $data['acSearchQuery'] = $this->M_job_application_details->findByFhj($degree, $subject, $institute, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByFh($degree, $subject, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByFi($degree, $result, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($institute))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByFj($degree, $institute, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						} else {
						    $data['acSearchQuery'] = $this->M_job_application_details->findByDegree($degree, $postId);
							$this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
					}
					else if(!empty($degreeTitle))					
					{
					if(!empty($subject) && !empty($result) && !empty($institute))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByGhij($degreeTitle, $subject, $result, $institute, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($result))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByGhi($degreeTitle, $subject, $result, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result) && !empty($institute))
						{
						  $data['acSearchQuery'] = $this->M_job_application_details->findByGij($degreeTitle, $result, $institute, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($institute))
						{
						  $data['acSearchQuery'] = $this->M_job_application_details->findByGhj($degreeTitle, $subject, $institute, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByGh($degreeTitle, $subject, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByGi($degreeTitle, $result, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($institute))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByGj($degreeTitle, $institute, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						} else {
						   $data['acSearchQuery'] = $this->M_job_application_details->findByDegreeTitleOnly($degreeTitle, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
					} else {  // a ,b empty start
					
						if(!empty($subject) && !empty($result) && !empty($institute))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByHij($subject, $result, $institute, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($result))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByHi($subject, $result, $postId); 
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result) && !empty($institute))
						{
						  $data['acSearchQuery'] = $this->M_job_application_details->findByIj($result, $institute, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject) && !empty($institute))
						{
						  $data['acSearchQuery'] = $this->M_job_application_details->findByHj($subject, $institute, $postId); 
						  $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($subject))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByH($subject, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($result))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByI($result, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}
						else if(!empty($institute))
						{
						   $data['acSearchQuery'] = $this->M_job_application_details->findByJ($institute, $postId);
						   $this->load->view('organizationUserPanel/searchResultPage2', $data); 
						}	
					}*/
					  
				}
				
				
				
					public function homePageSerialNo()
					{
						$positon 	      = $this->input->post('id');
						$positionWiseSl   = $this->M_add_manage->findSlNo($positon);				
					
						echo '<option value="">Sellect Serial No</option>';
						foreach($positionWiseSl as $v) {
							echo '<option value="'.$v->serial_no.'">'.$v->serial_no.'</option>';
						}
										
					}
					
					public function regionWisePosition()
					{
						$region 	      		= $this->input->post('id');
						$regionWisePosition   	= $this->M_add_manage->findByRegionWisePosition($region);				
					
						echo '<option value="">Sellect Position</option>';
						foreach($regionWisePosition as $v) {
							echo '<option value="'.$v->positon.'">'.$v->positon.'</option>';
						}
										
					}
						
				
				
					public function regionWiseSlNo()
					{
						$region 	      		= $this->input->post('region');
						$regionPositon 	      	= $this->input->post('regionPositon');
						$regionWiseSlNo   		= $this->M_add_manage->findByRegionWiseSl($region, $regionPositon);				
					
						echo '<option value="">Sellect Sl No</option>';
						foreach($regionWiseSlNo as $v) {
							echo '<option value="'.$v->serial_no.'">'.$v->serial_no.'</option>';
						}
										
					}  
						  
				
		   
	   
			 
				 public function addDelete($id)
				 {	
					
					$model					= $this->M_add_manage->findById($id);
					$only_image    			= $model->add_image;
					
					if(!empty($only_image)){
					unlink('./Images/Add_image/'.$only_image);
					}
					
				  	if(empty($model->region)){
						$data['region']         = "";
					}
					$data['user_id']            = "";
					$data['title']              = "";
					$data['description']        = "";
					$data['add_image']          = "";
					$data['add_link']           = "";
					$data['status']             = "available";
					$data['manage_date']        = "";
					$data['deadline_date']      = "";
					
					$this->M_add_manage->update2($data, $id);		
					redirect('organizationUserHome/addManageSuccess');
				 }
				 
				 public function postDelete($id)
				 {	
					
					$model					= $this->M_user_post_manage->findById($id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					unlink('./Images/Post_image/'.$only_image);
					}
					$this->M_user_post_manage->destroy($id);		
					redirect('organizationUserHome/addPost');
				 }
				 
				 public function intJobDelete($id)
				 {	
					$this->M_interested_job_post_manage->destroy($id);		
					redirect('organizationUserHome/interestedJobPost');
				 }
		   
				public function programeDelete($id)
				 {	
					$this->M_programe_manage->destroy($id);		
					redirect('organizationUserHome/programeManage');
				 }
		   
				 
			
				public function logout()
				{	
					logoutUser();
				}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */