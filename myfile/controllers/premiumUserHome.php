<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PremiumUserHome extends CI_Controller {
	static $model 	= array('M_basic_manage','M_advertisement_manage','M_region_manage','M_country_manage','M_city_manage',
	'M_all_user_registration','M_interested_job_post_manage','M_user_post_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_payment_gateaway',
	'M_payment_by_user','M_organization_user_details','M_more_campus_details','M_user_menu_manage','M_photo_gallery_manage','M_organization_user_registration');
	
	static $helper   = array('url','userauthentication');
	
	const  Title	 = 'Next Admission';
	
	public function __construct(){
		parent::__construct();
		$this->load->database(); // TO LOAD DATABASE CONNECTIVITY
		$this->load->model(self::$model); // TO LOAD ALL MODEL
		$this->load->helper(self::$helper);// TO LOAD URL BASE HELPER LIKE BASE_URL, SITE_URL
		$this->load->library('form_validation'); // LOAD THIS LIBRARY TO USE FORM VALIDATION 
		$this->load->library('pagination'); // LOAD THIS LIBRARY TO USE PAGINATION
		$this->load->library('upload'); // LOAD THIS LIBRARY TO FILE UPLOAD
		$this->load->library('image_lib');
		$this->load->library('session');
		isAuthenticate();
	}

	public function index()
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$userAutoId   		        = $userDetails->id;
			$data['page_title']  		= self::Title;	
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      = array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   	$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   	$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   	$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('premiumUserPanel/premiumUserHomePage', $data);
		
		}
	}
	   
	   
	 public function interestedJobPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('premiumUserPanel/interestedJobPostPage', $data);
			
			}
		}
		
		
		public function addPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findByUser($userId);
				$this->load->view('premiumUserPanel/addPostPage', $data);
			
			}
		}
		
		
		public function menuManage()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['UserWiseMenuInfo']	= $this->M_user_menu_manage->findByUser($userId);
				$this->load->view('premiumUserPanel/menuPage', $data);
			
			}
		}
		
		
		public function menuStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['user_menu_name'] 			= $this->input->post('user_menu_name');
			$data['user_menu_title'] 			= $this->input->post('user_menu_title');
			$data['user_menu_description'] 		= $this->input->post('user_menu_description');
			
				
				$config['upload_path'] 		= './Images/User_menu_image/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				$file_field					= 'image';
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')) {
				$uploadData = $this->upload->data();
				$data['image'] = $uploadData['file_name'];
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadData['full_path'];
				$config['maintain_ratio'] = TRUE;
				$config['new_image'] = './Images/User_menu_image/';
				$config['width'] = 150;
				$config['height'] = 130;
				
				$this->image_lib->initialize($config); 
				
				$this->image_lib->resize();
				}
				
			
			
			$this->M_user_menu_manage->save($data);
			redirect('premiumUserHome/menuManage');
		  }
	  }
	  
	   public function menuEdit($id)
		   {
			   $data['menuEditInfo']  		= $this->M_user_menu_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseMenuInfo']	= $this->M_user_menu_manage->findByUser($userId);
			   
			   $this->load->view('premiumUserPanel/menuEditPage', $data);
			 }
		   }
		   
		   
		    public function menuUpdate()
			{
				$id									= $this->input->post('id');
				$data['user_menu_name'] 			= $this->input->post('user_menu_name');
				$data['user_menu_title'] 			= $this->input->post('user_menu_title');
				$data['user_menu_description'] 		= $this->input->post('user_menu_description');
			
			
				$config['upload_path'] 		= './Images/User_menu_image/';
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['max_size']			= '0';
				$config['max_width']  		= '0';
				$config['max_height']  		= '0';
				$config['file_name']  		= time();
				$file_field					= 'image';
			
				/// IMAGE UPLOAD ACTION
				$this->upload->initialize($config);
				if ($this->upload->do_upload($file_field))
				{
					/// ERORR SHOW FOR IMAGE UPLOAD
				
					$image_data 			= $this->upload->data();				
					$data['image']  		= $image_data['file_name'];
					$model					= $this->M_user_menu_manage->findById($id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					unlink('./Images/User_menu_image/'.$only_image);
					}
				}
	
				$this->M_user_menu_manage->update($data, $id);		
				redirect('premiumUserHome/menuManage');
				
			 }
		   
		   
	  
		public function photoGallery()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$userId                     = $userDetails->id;
				$data['page_title']  		= self::Title;	
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['UserWisePhotoInfo']	= $this->M_photo_gallery_manage->findByUser($userId);
				$this->load->view('premiumUserPanel/photoPage', $data);
			
			}
		}
		
		public function photoStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['title'] 						= $this->input->post('title');
			
				
				$config['upload_path'] 		= './Images/photo_gallery/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				$file_field					= 'image';
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')) {
				$uploadData = $this->upload->data();
				$data['image'] = $uploadData['file_name'];
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadData['full_path'];
				$config['maintain_ratio'] = TRUE;
				$config['new_image'] = './Images/User_menu_image/';
				$config['width'] = 150;
				$config['height'] = 130;
				
				$this->image_lib->initialize($config); 
				
				$this->image_lib->resize();
				}
				
			
			
			$this->M_user_menu_manage->save($data);
			redirect('premiumUserHome/menuManage');
		  }
	  }
		
		
		
		public function organizationDetails()
		{   
			if(isAuthenticate()){
				$user_id             				= getUserName();
				$data['user_id']     				= $user_id;
				$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   				= $userDetails;
				$userId                     		= $userDetails->id;
				$data['page_title']  				= self::Title;	
				$data['headerAddInfo']				= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']			= $this->M_basic_manage->findHeaderView();
				$where                      		= array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        		= $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']					= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['UserWiseOrganizationInfo']	= $this->M_organization_user_details->findByUser($userId);
				$this->load->view('premiumUserPanel/organizationPage', $data);
			
			}
		}
		
		 public function postEdit($id)
		   {
			   $data['postEditInfo']  		= $this->M_user_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWisePostInfo']	    = $this->M_user_post_manage->findByUser($userId);
			   
			   $this->load->view('premiumUserPanel/postEditPage', $data);
			 }
		   }
		   
		   
		    public function postUpdate()
			{
				$id						= $this->input->post('id');
				$data['title'] 			= $this->input->post('title');
				$data['image_title'] 	= $this->input->post('image_title');
				$data['date_and_time'] 	= $this->input->post('date_and_time');
				$data['description'] 	= $this->input->post('description');
			
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['max_size']			= '0';
				$config['max_width']  		= '0';
				$config['max_height']  		= '0';
				$config['file_name']  		= time();
				$file_field					= 'image';
			
				/// IMAGE UPLOAD ACTION
				$this->upload->initialize($config);
				if ($this->upload->do_upload($file_field))
				{
					/// ERORR SHOW FOR IMAGE UPLOAD
				
					$image_data 			= $this->upload->data();				
					$data['image']  		= $image_data['file_name'];
					$model					= $this->M_user_post_manage->findById($id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					unlink('./Images/Post_image/'.$only_image);
					}
				}
	
				$this->M_user_post_manage->update($data, $id);		
				redirect('premiumUserHome/addPost');
				
			 }
		   
		
		public function orgAddPostStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['title'] 						= $this->input->post('title');
			$data['status'] 	    			= "aprove";
			$data['image_title'] 				= $this->input->post('image_title');
			$data['date_and_time'] 				= $this->input->post('date_and_time');
			$data['description'] 				= $this->input->post('description');
			
				
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				$file_field					= 'image';
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')) {
				$uploadData = $this->upload->data();
				$data['image'] = $uploadData['file_name'];
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadData['full_path'];
				$config['maintain_ratio'] = TRUE;
				$config['new_image'] = './Images/Post_image/';
				$config['width'] = 50;
				$config['height'] = 50;
				
				$this->image_lib->initialize($config); 
				
				$this->image_lib->resize();
				}
				
			
			
			$this->M_user_post_manage->save($data);
			redirect('premiumUserHome/addPost');
		  }
	  }
	  
	  
	  public function intrestEdit($id)
		   {
			   $data['intrestEditInfo']  	= $this->M_interested_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
			   
			   $this->load->view('premiumUserPanel/interestJobEditPage', $data);
			 }
		   }
		   
		   
		   public function interestJobUpdate()
			{
				$id										= $this->input->post('id');
				$data['education_provider_type'] 		= $this->input->post('education_provider_type');
				$data['interested_program'] 			= $this->input->post('interested_program');
				$data['location'] 						= $this->input->post('location');
				$data['other'] 							= $this->input->post('other');
	
				$this->M_interested_job_post_manage->update($data, $id);		
				redirect('premiumUserHome/interestedJobPost');
				
			 }
	   
	  
	
		 public function profileUpdate($id)
		   {
			   $model   					= $this->M_all_user_registration->findById($id);
			   $data['profileEditInfo'] 	= $model;
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
			   $data['page_title']  		= self::Title;
			   $data['regionInfo']			= $this->M_region_manage->findAll();
			   $data['countryInfo']			= $this->M_country_manage->findAll();	
			   $data['cityInfo']			= $this->M_city_manage->findAll();	
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   
			   $this->load->view('premiumUserPanel/profileUpdatePage', $data);
			 }
		   }
	   
	   public function interestJobStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['education_provider_type'] 	= $this->input->post('education_provider_type');
			$data['interested_program'] 	    = $this->input->post('interested_program');
			$data['location'] 					= $this->input->post('location');
			$data['other'] 						= $this->input->post('other');
			
			$this->M_interested_job_post_manage->save($data);
			redirect('premiumUserHome/interestedJobPost');
		  }
		  
		}
	  
	  
	  public function countryEdit()
		{
			$region_id 	   = $this->input->post('id');
			$countryList   = $this->M_country_manage->findAllInfo($region_id);				
		
			echo '<option value="">Sellect Country Name</option>';
			foreach($countryList as $v) {
				echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';
			}
			
		}
		
		
		 public function cityEdit()
			{
				$country_id 	= $this->input->post('id');
				$cityList   	= $this->M_city_manage->findAllCity($country_id);				
			
				echo '<option value="">Sellect City Name</option>';
				foreach($cityList as $v) {
					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';
				}
				
			}
			
			
			public function update()
			{
				$id								= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				 
					$config['upload_path']		= './Images/Register_image/';
					$config['allowed_types'] 	= 'gif|jpg|png';
					$config['max_size']			= '0';
					$config['max_width']  		= '0';
					$config['max_height']  		= '0';
					$config['file_name']  		= time();
					$file_field					= 'image';
				
					/// IMAGE UPLOAD ACTION
					$this->upload->initialize($config);
					if ($this->upload->do_upload($file_field))
					{
						
					
						$image_data 			= $this->upload->data();				
						$data['image']  		= $image_data['file_name'];
						$model					= $this->M_all_user_registration->findById($id);
						$only_image    			= $model->image;
						
						if(!empty($only_image)){
						unlink('./Images/Register_image/'.$only_image);
						}
					}
		
				$this->M_all_user_registration->update($data, $id);		
				redirect('premiumUserHome');
				
			 }
			 
			 public function postDelete($id)
			 {	
				
				$model					= $this->M_user_post_manage->findById($id);
				$only_image    			= $model->image;
				
				if(!empty($only_image)){
				unlink('./Images/Post_image/'.$only_image);
				}
				$this->M_user_post_manage->destroy($id);		
				redirect('premiumUserHome/addPost');
			 }
			 
			 public function intJobDelete($id)
			 {	
				$this->M_interested_job_post_manage->destroy($id);		
				redirect('premiumUserHome/interestedJobPost');
			 }
	   
			 
			 public function menuDelete($id)
			 {	
				
				$model					= $this->M_user_menu_manage->findById($id);
				$only_image    			= $model->image;
				
				if(!empty($only_image)){
				unlink('./Images/User_menu_image/'.$only_image);
				}
				$this->M_user_menu_manage->destroy($id);		
				redirect('premiumUserHome/menuManage');
			 }
			 
	   
		
			public function logout()
			{	
				logoutUser();
			}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */