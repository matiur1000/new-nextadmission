<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {

		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub',
		'M_news_and_event_manage','M_user_post_manage','M_all_user_registration','M_post_comment','M_reply_comment','M_organization_user_registration',
		'M_add_manage','M_general_user_registration','M_programe_manage','M_job_post_manage','M_job_application_details', 'M_country_manage', 'M_organize_profile_manage','M_search_table','M_region_manage','M_visitor_count');
		static $helper   = array('url','userauthentication','generalauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
			$this->load->library('email');
			$this->load->helper('cookie');
	
		}
	

		public function index()
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		  						
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllHome();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$data['allProgramName']	    = $this->M_programe_manage->findAll();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->leftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->leftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();


			$regionList = $this->M_region_manage->findAll();
			foreach ($regionList as $v) {
				$v->countryList = $this->M_country_manage->findAllInfo($v->id);
			}
			$data['regionList'] = $regionList;

			$this->load->view('homePage', $data);
	
		}


		public function countryWiseData($country_name)
	    {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   	$data['page_title']  		= self::Title;
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
		   	$data['country_name']  		= $country_name;	
		   	$countryInfo	    		= $this->M_country_manage->findCountryId($country_name);
		   	$country_id                 = $countryInfo->id;	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllByCountry($country_name);
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganizeByCountry($country_id);
			$region                     = "Asia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['asiaCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->countryTopAddInfo1($country_name);
			$data['topAddInfo2']	    = $this->M_add_manage->countryTopAddInfo2($country_name);
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->countryleftAddInfo1($country_name);
			$data['leftAddInfo2']	    = $this->M_add_manage->countryleftAddInfo2($country_name);
			$data['leftAddInfo3']	    = $this->M_add_manage->countryleftAddInfo3($country_name);
			$data['leftAddInfo4']	    = $this->M_add_manage->countryleftAddInfo4($country_name);
			$data['leftAddInfo5']	    = $this->M_add_manage->countryleftAddInfo3($country_name);
			$data['leftAddInfo6']	    = $this->M_add_manage->countryleftAddInfo4($country_name);
			
			$data['rightAddInfo1']	    = $this->M_add_manage->countryRightAddInfo1($country_name);
			$data['rightAddInfo2']	    = $this->M_add_manage->countryRightAddInfo2($country_name);
			$data['rightAddInfo3']	    = $this->M_add_manage->countryRightAddInfo3($country_name);
			$data['rightAddInfo4']	    = $this->M_add_manage->countryRightAddInfo4($country_name);
			$data['rightAddInfo5']	    = $this->M_add_manage->countryRightAddInfo5($country_name);
			$data['rightAddInfo6']	    = $this->M_add_manage->countryRightAddInfo6($country_name);
			$data['rightAddInfo7']	    = $this->M_add_manage->countryRightAddInfo7($country_name);
			$data['rightAddInfo8']	    = $this->M_add_manage->countryRightAddInfo8($country_name);
			$data['rightAddInfo9']	    = $this->M_add_manage->countryRightAddInfo9($country_name);
			$data['rightAddInfo10']	    = $this->M_add_manage->countryRightAddInfo10($country_name);
			$data['rightAddInfo11']	    = $this->M_add_manage->countryRightAddInfo11($country_name);
			$data['rightAddInfo12']	    = $this->M_add_manage->countryRightAddInfo12($country_name);
			$data['rightAddInfo13']	    = $this->M_add_manage->countryRightAddInfo13($country_name);
			$data['rightAddInfo14']	    = $this->M_add_manage->countryRightAddInfo14($country_name);
			$data['rightAddInfo15']	    = $this->M_add_manage->countryRightAddInfo15($country_name);
			$data['rightAddInfo16']	    = $this->M_add_manage->countryRightAddInfo16($country_name);
			$data['rightAddInfo17']	    = $this->M_add_manage->countryRightAddInfo17($country_name);
			$data['rightAddInfo18']	    = $this->M_add_manage->countryRightAddInfo18($country_name);
			$data['rightAddInfo19']	    = $this->M_add_manage->countryRightAddInfo19($country_name);
			$data['rightAddInfo20']		= $this->M_add_manage->countryRightAddInfo20($country_name);
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllCountryWiseInfo($country_name);
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

			$regionList = $this->M_region_manage->findAll();
			foreach ($regionList as $v) {
				$v->countryList = $this->M_country_manage->findAllInfo($v->id);
			}
			$data['regionList'] = $regionList;


	     	$this->load->view('countryWiseDataPage', $data);
	   
	   }



	   public function searchByCountry($country_name)
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }


			$organize_id      			= $this->input->post('organize_id');
			$program_id      			= $this->input->post('program_id');

			$data['organize_id']        = $organize_id;
			$data['program_id']         = $program_id;


			$data['searchCountryResult']   = $this->M_search_table->findByAllCountrySearchData($organize_id, $program_id);



		   	$data['page_title']  		= self::Title;
		   	$data['country_name']  		= $country_name;	
		   	$countryInfo	    		= $this->M_country_manage->findCountryId($country_name);
		   	$country_id                 = $countryInfo->id;	
			$data['orgWiseProInfo']		= $this->M_programe_manage->findByUser($organize_id);
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllByCountry($country_name);
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganizeByCountry($country_id);
			$region                     = "Asia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['asiaCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->countryTopAddInfo1($country_name);
			$data['topAddInfo2']	    = $this->M_add_manage->countryTopAddInfo2($country_name);
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->countryleftAddInfo1($country_name);
			$data['leftAddInfo2']	    = $this->M_add_manage->countryleftAddInfo2($country_name);
			$data['leftAddInfo3']	    = $this->M_add_manage->countryleftAddInfo3($country_name);
			$data['leftAddInfo4']	    = $this->M_add_manage->countryleftAddInfo4($country_name);
			$data['leftAddInfo5']	    = $this->M_add_manage->countryleftAddInfo3($country_name);
			$data['leftAddInfo6']	    = $this->M_add_manage->countryleftAddInfo4($country_name);
			
			$data['rightAddInfo1']	    = $this->M_add_manage->countryRightAddInfo1($country_name);
			$data['rightAddInfo2']	    = $this->M_add_manage->countryRightAddInfo2($country_name);
			$data['rightAddInfo3']	    = $this->M_add_manage->countryRightAddInfo3($country_name);
			$data['rightAddInfo4']	    = $this->M_add_manage->countryRightAddInfo4($country_name);
			$data['rightAddInfo5']	    = $this->M_add_manage->countryRightAddInfo5($country_name);
			$data['rightAddInfo6']	    = $this->M_add_manage->countryRightAddInfo6($country_name);
			$data['rightAddInfo7']	    = $this->M_add_manage->countryRightAddInfo7($country_name);
			$data['rightAddInfo8']	    = $this->M_add_manage->countryRightAddInfo8($country_name);
			$data['rightAddInfo9']	    = $this->M_add_manage->countryRightAddInfo9($country_name);
			$data['rightAddInfo10']	    = $this->M_add_manage->countryRightAddInfo10($country_name);
			$data['rightAddInfo11']	    = $this->M_add_manage->countryRightAddInfo11($country_name);
			$data['rightAddInfo12']	    = $this->M_add_manage->countryRightAddInfo12($country_name);
			$data['rightAddInfo13']	    = $this->M_add_manage->countryRightAddInfo13($country_name);
			$data['rightAddInfo14']	    = $this->M_add_manage->countryRightAddInfo14($country_name);
			$data['rightAddInfo15']	    = $this->M_add_manage->countryRightAddInfo15($country_name);
			$data['rightAddInfo16']	    = $this->M_add_manage->countryRightAddInfo16($country_name);
			$data['rightAddInfo17']	    = $this->M_add_manage->countryRightAddInfo17($country_name);
			$data['rightAddInfo18']	    = $this->M_add_manage->countryRightAddInfo18($country_name);
			$data['rightAddInfo19']	    = $this->M_add_manage->countryRightAddInfo19($country_name);
			$data['rightAddInfo20']		= $this->M_add_manage->countryRightAddInfo20($country_name);
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllCountryWiseInfo($country_name);
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('searchCountryWiseDataPage', $data);
	   
	   }

		
		
		public function advritismentView($viewId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
			  $visitorId       			= $userDetails->id;
		   }
		    $data['viewAddInfo']	    = $this->M_job_post_manage->findById($viewId);
		    $organizeId    				= $data['viewAddInfo']->user_id;


		    $linkUrl     = uri_string();
		    $current_url = current_url();



			 if ($userDetails->user_type =='General user') {
			    $datav['user_type']    = "general";
			} else {
			    $datav['user_type']    = "organize";
			}

			   $datav['organize_id']    = $organizeId;
			if (!empty($visitorId)) {
				$datav['visitor_id']    = $visitorId;
			} else {
			    $datav['visitor_id']    = "unknown";	
			}
                 $datav['job_id']    	= $viewId;
                 $datav['visit_link']   = $linkUrl;
                 $datav['page_type']   	= "Job";
                 $datav['date_time']   	= date("Y-m-d H:i:s");
                 $datav['date']    		= date("Y-m-d");


			$urlArray = unserialize(get_cookie('visitor_tracking'));

			if(!in_array($current_url, $urlArray)) {
				$urlArray[] = $current_url;
			
			  	$cookieValue = array(
			        'name'   => 'visitor_tracking',
			        'value'  => serialize($urlArray),
			        'expire' => '86400',
			        'domain' => '.localhost',
			        'path'   => '/'
		        );
               
               if ($organizeId > 0) {
                 $this->M_visitor_count->save($datav);
               }
	          
	           set_cookie($cookieValue);
			}
		   
		   
		    
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['page_title']  		= self::Title;	
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
			$this->load->view('advritismentViewPage', $data);
	
		}
		
		
		public function advritismentViewApply($jobId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   
		    
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['viewAddInfo']	    = $this->M_job_post_manage->findById($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('advritismentViewApplyPage', $data);
	
		}
		
		public function applyJobView($jobId, $applyId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   
		    
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['page_title']  		= self::Title;
			$data['jobId']  			= $jobId;
			$data['applyId']  			= $applyId;	
			$data['viewAddInfo']	    = $this->M_job_post_manage->findById($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('applyJobViewPage', $data);
	
		}
		
		public function addApplyChk($jobId, $applyId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   
		    
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['page_title']  		= self::Title;	
			$chkApplication	    		= $this->M_job_application_details->findByIdApplyId($jobId, $applyId);
			$data['viewAddInfo']	    = $this->M_job_post_manage->findById($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			if(!empty($chkApplication)){
				 $this->load->view('jobApplyAttentionPage', $data);
			}else{
			   $this->load->view('advritismentViewApplyPage', $data);
			}
	
		}
		
		
		public function searchByOrganization($organization = '',$program = '')
	
		{	
		    $organizationId      = $organization;
			$program      		 = $program;
			
			$where           	 = array('orgnization_id' => $organizationId, 'programe_name' => $program);
			if(!empty($organizationId)&&($program)){
			  $searchinfo	 = $this->M_programe_manage->searchProgram($where);
			  if(!empty($searchinfo)){
			    $data['searchinfo']	 = $this->M_programe_manage->searchProgram($where);
			  	$this->load->view('searchByOrgProgPage', $data);
				}else{
			  	$this->load->view('searchByEmptyOrgProgPage', $data);
				}
			}
			if(!empty($organizationId)&& empty($program)){
			    $orgSearchInfo	 = $this->M_programe_manage->searchOrgAllProgram($organizationId);
				if(!empty($orgSearchInfo)){
				 $data['orgSearchInfo']	 = $this->M_programe_manage->searchOrgAllProgram($organizationId);
				 $data['orgInfo']	 	 = $this->M_all_user_registration->findById($organizationId);
				 $data['orgMorInfo']	 = $this->M_organization_user_registration->findByUserName($organizationId);
				 $this->load->view('searchByOrganizationPage', $data);
				}else{
			  	 $this->load->view('searchByEmptyOrgProgPage', $data);
				}
			
			} 
		  
	
		}
		
		
		public function programDetails($programId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['page_title']  		= self::Title;
			$data['programInfo']		= $this->M_programe_manage->findOrgWiseProDetails($programId);	
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAll();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('programDetailsPage', $data);
	
		}
		
		public function applyLogin()
		{	
			$applyId							= $this->input->post('id');
			$userCvId							= $this->input->post('userId');
			$password							= $this->input->post('password');
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($userCvId);
			
			if(!empty($userDetails) && $userDetails->password == $password){
			$userData	= array('userCvId' => $userCvId);
			  $this->session->set_userdata($userData);
			  redirect('home/finalApply/'.$applyId);
			}else{
			  redirect('home/applyLoginFail/'.$applyId);
			}
			
	  }
	  
	  
	  public function finalApply($applyId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   
		    $data['countryInfo']	    = $this->M_country_manage->findAllCountry();
		    $data['viewAddInfo']	    = $this->M_job_post_manage->findById($applyId);
		    $applyUser                  = $this->session->userdata('userCvId');  
			$data['applyUser']          = $this->session->userdata('userCvId');  
			$resumeInfo                 = $this->M_all_user_registration->findByUserName($applyUser);
			$data['resumeInfo']         = $this->M_all_user_registration->findByUserName($applyUser);
			$userAutoId                 = $resumeInfo->id;
			 $data['moreResumeInfo'] 	= $this->M_general_user_registration->findByUserName($userAutoId);
			//$data['viewAddInfo']	    = $this->M_job_post_manage->findById($jobId);
			$data['page_title']  		= self::Title;	
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('finalApplyPage', $data);
	
		}
	  
	  
	   public function applyLoginFail($applyId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   
		    
			$data['applyId']  			= $applyId;
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('applyLoginFailPage', $data);
	
		}
		
		public function applyFinaly()
		{	
			$data['post_id']					= $this->input->post('id');
			$applyUser                  		= $this->session->userdata('userCvId'); 
			$resumeInfo                 		= $this->M_all_user_registration->findByUserName($applyUser);
			$userAutoId                 		= $resumeInfo->id;
			$moreResumeInfo 					= $this->M_general_user_registration->findByUserName($userAutoId);
			$postUserInfo                 		= $this->M_job_post_manage->findById($data['post_id']);
			$dateOfBirth                        = $moreResumeInfo->date_of_birth;
			$curDate   							= date("d/m/Y");
			$curntOrderDate = explode('/', $curDate);
			$curday 		= $curntOrderDate[0];
			$curmonth   	= $curntOrderDate[1];
			$curyear  		= $curntOrderDate[2];
			
			$birthOrderDate = explode('/', $dateOfBirth);
			$birthday 		= $birthOrderDate[0];
			$birthmonth   	= $birthOrderDate[1];
			$birthyear  	= $birthOrderDate[2];
			
			if($curday < $birthday){
			 $curdayValu = $curday + 30;
			}else{
			   $curdayValu = $curday;	
			  }	
			 
			 if($curday < $birthday){ 
			   $birthmonthValu  = $birthmonth + 1;  
			  }else{
			   $birthmonthValu  = $birthmonth;
			   }
			  if($birthmonthValu > $curmonth){
			     $curmonthValuMain  = $curmonth + 12;
				}else{
				  $curmonthValuMain  = $curmonth;
				  }
				  
				if($birthmonthValu > $curmonth){
				      $birthyearValu  = $birthyear + 1;
					}else{
					 $birthyearValu  = $birthyear;
					}
					
				
			$ageYear  = $curyear - $birthyearValu; 
			$data['age']             = $ageYear;
			$data['applicant_name']  = $resumeInfo->name;
			$data['gender']          = $moreResumeInfo->gender;
			$data['degree']          = $moreResumeInfo->education_level;
			$data['degree_title']    = $moreResumeInfo->degree_title;
			$data['subject']         = $moreResumeInfo->group;
			$data['result']          = $moreResumeInfo->result;
			$data['institute_name']  = $moreResumeInfo->institute_name;
				
			
			
			$data['post_user_name']             = $postUserInfo->name;
			$data['post_user_id']             	= $postUserInfo->user_id;
			$data['app_user_id']                = $resumeInfo->id;
			
			
			$data['shortlist_status']           = "no";
			$data['application_view_status']    = "notview";
			$data['star_candidate_status']    	= "no";
			$data['count_applicant']    		= "1";
			$currentDate 						= date("Y/m/d");
			$data['job_apply_date'] 			= $currentDate;
			
			$chkApplication	    		= $this->M_job_application_details->findByIdApplyId($data['post_id'], $data['app_user_id']);
			if(!empty($chkApplication)){
			   if( isActiveUser() ) {
				  $userId  					= getUserName();
				  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
				  $data['activeUser']       = $userDetails->name;
				  $data['userType']       	= $userDetails->user_type;
		   		}
			    $data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
				$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
				$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
				$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
				 $data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				 $this->load->view('jobApplyAttentionPage', $data);
			}else{
			   $this->M_job_application_details->save($data);
			   redirect('home/applySuccess');
			}
			
		 
	  }
	  
	  
	  public function applySuccess()
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   
		    
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('applySuccessPage', $data);
	
		}
	  
		
		
		public function postDetails($postId)
	
		{	
			if( isActiveUser() ) {
				  $userId  					= getUserName();
				  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
				  $data['activeUser']       = $userDetails->name;
				  $data['userType']       	= $userDetails->user_type;
			   }
			   
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAll();
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$data['UserCmntInfo']	    = $this->M_post_comment->findAllDetailes($postId);
			$data['UserPostInfo']	    = $this->M_user_post_manage->findByUserWiseDetails($postId);
			$data['postId']				= $postId;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['menuInfo']			= $menuInfo;
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('postDetailsPage', $data);
	
		}
		
		public function newsDetails($newsId)
	
		{	
		   if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$data['UserNewsInfo']	    = $this->M_news_and_event_manage->findByUserWiseDetails($newsId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['menuInfo']			= $menuInfo;
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('newsDetailsPage', $data);
	
		}
		
		
		public function viewAllPostDeatials()
	
		{	
		   if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo2();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$data['menuInfo']			= $menuInfo;
			$data['menuInfo']			= $menuInfo;
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('viewAllPostDeatialsPage', $data);
	
		}
		
	  
	  public function commentDetails()
		{
			$id 				= $this->input->post('id');		
			$postInfo 	        = $this->M_user_post_manage->findById($id);
					
			echo json_encode($postInfo);
		}
		
		
		
	  
	  public function commentStore2($postId)
		{	
		  
		   $UserId    = $this->session->userdata('UserId');
		   $UserType  = $this->session->userdata('userType');
		    if($UserType == 'General user'){
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($UserId);
			  $data['user_id'] 			= $userDetails->id;
			  $data['user_type'] 		= $UserType;
			  }else{
			  $userDetails   		    = $this->M_organization_user_registration->findByUserName($UserId);
		      $data['user_id'] 			= $userDetails->id;
			  $data['user_type'] 		= $UserType;
			  }
			
		   $data['comment'] 			= $this->input->post('comment');
		   $data['post_id'] 			= $postId;
			$this->M_post_comment->save($data);
			redirect('home');
			
		 
	  }
	  
	 
	  
	   public function searchAll()
	   {
	     $search_all      					= $this->input->post('search_all');
		 $country_id      					= $this->input->post('country_id');
		 $organize_id      					= $this->input->post('organize_id');
		 $program_id      					= $this->input->post('program_id');
		 $data['search_all']                = $search_all;
		 $data['country_id']                = $country_id;
		 $data['organize_id']               = $organize_id;
		 $data['program_id']                = $program_id;
		 
		 if(!empty($country_id) || !empty($organize_id) || !empty($program_id)){
			 if(!empty($country_id) && !empty($organize_id) &&  !empty($program_id)) {
			   	 $data['searchUserAdvancedResult']     = $this->M_search_table->findByAllAdvanceSearchData($search_all, $country_id, $organize_id, $program_id);
			 } else if(!empty($country_id) && !empty($organize_id)) {
			   	 $data['searchUserAdvancedResult']     = $this->M_search_table->findByAllAdvanceSearchData2($search_all, $country_id, $organize_id);
			 } else if(!empty($organize_id) &&  !empty($program_id)) {
			   	 $data['searchUserAdvancedResult']     = $this->M_search_table->findByAllAdvanceSearchData3($search_all, $organize_id, $program_id);
			 } else if(!empty($country_id) &&  !empty($program_id)) {
			   	 $data['searchUserAdvancedResult']     = $this->M_search_table->findByAllAdvanceSearchData4($search_all, $country_id, $program_id);

			 }else if(!empty($country_id)) {
			   	 $data['searchAdvancedOrggENResult']   = $this->M_search_table->findByAllOrgGenAdvanceSearchData($search_all, $country_id);
			   	 $data['searchProgramResult']     	   = $this->M_search_table->findByAllProgramAdvanceSearchData($search_all, $country_id);
			   	 $data['searchTitleResult']     	   = $this->M_search_table->findByAllTitleAdvanceSearchData($search_all, $country_id);
			   	 $data['searchAdResult']     	   	   = $this->M_search_table->findByAllAdAdvanceSearchData($search_all, $country_id);

			 }else if(!empty($organize_id)) {
			   	 $data['searchProgramResult']     	   = $this->M_search_table->findByAllProgramAdvanceSearchByOrg($search_all, $organize_id);
			   	 $data['searchTitleResult']     	   = $this->M_search_table->findByAllTitleAdvanceSearchByOrg($search_all, $organize_id);
			   	 $data['searchAdResult']     	   	   = $this->M_search_table->findByAllAdAdvanceSearchByOrg($search_all, $organize_id);
			 } else {
                 $data['searchProgramResult']     	   = $this->M_search_table->findByAllProgramAdvanceSearchByProId($search_all, $program_id);

			 }


		    
		 }else{
			 $data['searchUserResult']     	= $this->M_search_table->findByAllSearchData($search_all);
			 $data['searchTitleResult']     = $this->M_search_table->findByAllSearchTitle($search_all);
			 $data['searchProgramResult']   = $this->M_search_table->findByAllSearchProgram($search_all);
			 $data['searchAdResult']        = $this->M_search_table->findByAllSearchAd($search_all);
		 }


		 if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		  						
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();				
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAll();
			$data['allProgramName']	    = $this->M_programe_manage->findAll();

			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->leftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->leftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

	         $this->load->view('viewAllSearchResultPage', $data);
	   
	   }
	   public function southAmerica()
	   {    
	        if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
	     	$data['page_title']  		= self::Title;
	     	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllSoutAm();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "South America";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['southAmCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->soutAmTopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->soutAmTopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->soutAmLeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->soutAmLeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->soutAmLeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->soutAmLeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->soutAmLeftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->soutAmLeftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->soutAmRightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->soutAmRightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->soutAmRightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->soutAmRightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->soutAmRightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->soutAmRightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->soutAmRightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->soutAmRightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->soutAmRightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->soutAmRightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->soutAmRightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->soutAmRightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->soutAmRightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->soutAmRightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->soutAmRightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->soutAmRightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->soutAmRightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->soutAmRightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->soutAmRightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->soutAmRightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllsoutAmInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	        $this->load->view('southAmericaPage', $data);
	   
	   }



	    public function searchSouthAm()
	     {    
	        if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }

		     $country_id      		    = $this->input->post('country_id');
			 $organize_id      			= $this->input->post('organize_id');
			 $program_id      			= $this->input->post('program_id');
			
			 $data['country_id']        = $country_id;
			 $data['organize_id']       = $organize_id;
			 $data['program_id']        = $program_id;


			$data['searchSouthAmResult'] = $this->M_search_table->findByAllAsiaSearchData($country_id, $organize_id, $program_id);


	     	$data['page_title']  		= self::Title;
	     	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllSoutAm();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "South America";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['southAmCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			$data['southAmOrgan']		= $this->M_all_user_registration->findAllInfoByCountryId($country_id);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->soutAmTopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->soutAmTopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->soutAmLeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->soutAmLeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->soutAmLeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->soutAmLeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->soutAmLeftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->soutAmLeftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->soutAmRightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->soutAmRightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->soutAmRightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->soutAmRightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->soutAmRightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->soutAmRightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->soutAmRightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->soutAmRightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->soutAmRightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->soutAmRightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->soutAmRightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->soutAmRightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->soutAmRightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->soutAmRightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->soutAmRightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->soutAmRightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->soutAmRightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->soutAmRightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->soutAmRightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->soutAmRightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllsoutAmInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	        $this->load->view('viewAllSouthAmSearchResultPage', $data);
	   
	      }
	   
	   
	   
	   
	   
	   public function northAmerica()
	   {    
	        if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
	     	$data['page_title']  		= self::Title;	
	     	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllnorthAm();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();

			$region                     = "North America";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['nortAmCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->northAmTopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->northAmTopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->northAmLeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->northAmLeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->northAmLeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->northAmLeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->northAmLeftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->northAmLeftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->northAmRightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->northAmRightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->northAmRightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->northAmRightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->northAmRightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->northAmRightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->northAmRightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->northAmRightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->northAmRightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->northAmRightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->northAmRightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->northAmRightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->northAmRightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->northAmRightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->northAmRightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->northAmRightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->northAmRightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->northAmRightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->northAmRightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->northAmRghtAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllNorthAmInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	        $this->load->view('northAmericaPage', $data);
	   
	   }





	   public function searchNorthAm()
	   {    
	        if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }


		     $country_id      		    = $this->input->post('country_id');
			 $organize_id      			= $this->input->post('organize_id');
			 $program_id      			= $this->input->post('program_id');
			
			 $data['country_id']        = $country_id;
			 $data['organize_id']       = $organize_id;
			 $data['program_id']        = $program_id;


			$data['searchNorthAmResult'] = $this->M_search_table->findByAllAsiaSearchData($country_id, $organize_id, $program_id);


	     	$data['page_title']  		= self::Title;	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllnorthAm();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();

			$region                     = "North America";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['nortAmCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			$data['northAmOrgan']		= $this->M_all_user_registration->findAllInfoByCountryId($country_id);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->northAmTopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->northAmTopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->northAmLeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->northAmLeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->northAmLeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->northAmLeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->northAmLeftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->northAmLeftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->northAmRightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->northAmRightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->northAmRightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->northAmRightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->northAmRightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->northAmRightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->northAmRightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->northAmRightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->northAmRightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->northAmRightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->northAmRightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->northAmRightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->northAmRightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->northAmRightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->northAmRightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->northAmRightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->northAmRightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->northAmRightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->northAmRightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->northAmRghtAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllNorthAmInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	        $this->load->view('viewAllNorthAmSearchResultPage', $data);
	   
	   }
	   
	   
	   
	   
	   public function africa()
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   	$data['page_title']  		= self::Title;	
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllAfrica();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Africa";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['africaCountry']	    = $this->M_country_manage->findAllInfo($regionId);


			
			
			$data['topAddInfo1']	    = $this->M_add_manage->africatopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->africatopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->africaleftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->africaleftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->africaleftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->africaleftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->africaleftAddInfo3();
			$data['leftAddInfo6']	    = $this->M_add_manage->africaleftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->africarightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->africarightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->africarightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->africarightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->africarightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->africarightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->africarightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->africarightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->africarightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->africarightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->africarightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->africarightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->africarightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->africarightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->africarightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->africarightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->africarightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->africarightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->africarightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->africarightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllAfricaInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	    	$this->load->view('africaPage', $data);
	   
	   }



	   public function searchAfrica()
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }

		     $country_id      		    = $this->input->post('country_id');
			 $organize_id      			= $this->input->post('organize_id');
			 $program_id      			= $this->input->post('program_id');
			
			 $data['country_id']        = $country_id;
			 $data['organize_id']       = $organize_id;
			 $data['program_id']        = $program_id;


			$data['searchAfricaResult'] = $this->M_search_table->findByAllAsiaSearchData($country_id, $organize_id, $program_id);


		   	$data['page_title']  		= self::Title;	
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllAfrica();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Africa";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['africaCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			$data['africaOrganization']	= $this->M_all_user_registration->findAllInfoByCountryId($country_id);


			
			
			$data['topAddInfo1']	    = $this->M_add_manage->africatopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->africatopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->africaleftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->africaleftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->africaleftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->africaleftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->africaleftAddInfo3();
			$data['leftAddInfo6']	    = $this->M_add_manage->africaleftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->africarightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->africarightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->africarightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->africarightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->africarightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->africarightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->africarightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->africarightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->africarightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->africarightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->africarightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->africarightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->africarightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->africarightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->africarightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->africarightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->africarightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->africarightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->africarightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->africarightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllAfricaInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	    	$this->load->view('viewAllAfricaSearchResultPage', $data);
	   
	   }
	   
	   
	   
	   
	   
	   public function asia()
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   	$data['page_title']  		= self::Title;	
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllAsia();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Asia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['asiaCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->asiaTopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->asiaTopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->asialeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->asialeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->asialeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->asialeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->asialeftAddInfo3();
			$data['leftAddInfo6']	    = $this->M_add_manage->asialeftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->asiaRightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->asiaRightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->asiaRightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->asiaRightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->asiaRightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->asiaRightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->asiaRightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->asiaRightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->asiaRightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->asiaRightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->asiaRightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->asiaRightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->asiaRightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->asiaRightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->asiaRightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->asiaRightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->asiaRightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->asiaRightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->asiaRightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->asiaRightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllAsiaInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('asiaPage', $data);
	   
	   }



	   public function searchAsia()
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }


			 $country_id      		    = $this->input->post('country_id');
			 $organize_id      			= $this->input->post('organize_id');
			 $program_id      			= $this->input->post('program_id');
			
			 $data['country_id']        = $country_id;
			 $data['organize_id']       = $organize_id;
			 $data['program_id']        = $program_id;


			$data['searchAsiaResult']   = $this->M_search_table->findByAllAsiaSearchData($country_id, $organize_id, $program_id);


		   	$data['page_title']  		= self::Title;	
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllAsia();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Asia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['asiaCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			$data['asiaOrganization']	= $this->M_all_user_registration->findAllInfoByCountryId($country_id);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->asiaTopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->asiaTopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->asialeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->asialeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->asialeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->asialeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->asialeftAddInfo3();
			$data['leftAddInfo6']	    = $this->M_add_manage->asialeftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->asiaRightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->asiaRightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->asiaRightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->asiaRightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->asiaRightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->asiaRightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->asiaRightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->asiaRightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->asiaRightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->asiaRightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->asiaRightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->asiaRightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->asiaRightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->asiaRightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->asiaRightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->asiaRightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->asiaRightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->asiaRightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->asiaRightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->asiaRightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllAsiaInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('viewAllAsiaSearchResultPage', $data);
	   
	   }
	   
	   
	   
	   
	   
	   public function australlia()
	   {
	        if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }


		   	$data['page_title']  		= self::Title;	
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllAustrallia();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Australia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['australiaCountry']	= $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->australliatopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->australliatopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->australlialeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->australlialeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->australlialeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->australlialeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->australlialeftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->australlialeftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->australliarightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->australliarightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->australliarightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->australliarightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->australliarightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->australliarightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->australliarightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->australliarightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->australliarightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->australliarightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->australliarightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->australliarightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->australliarightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->australliarightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->australliarightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->australliarightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->australliarightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->australliarightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->australliarightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->australliarightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllAustralliaInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('australiaPage', $data);
	   }




	    public function searchAustrallia()
	   {
	        if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }


		     $country_id      		    = $this->input->post('country_id');
			 $organize_id      			= $this->input->post('organize_id');
			 $program_id      			= $this->input->post('program_id');
			
			 $data['country_id']        = $country_id;
			 $data['organize_id']       = $organize_id;
			 $data['program_id']        = $program_id;


			$data['searchAusResult']   = $this->M_search_table->findByAllAsiaSearchData($country_id, $organize_id, $program_id);

		   	$data['page_title']  		= self::Title;	
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllAustrallia();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Australia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['australiaCountry']	= $this->M_country_manage->findAllInfo($regionId);
			$data['ausOrganization']	= $this->M_all_user_registration->findAllInfoByCountryId($country_id);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->australliatopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->australliatopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->australlialeftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->australlialeftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->australlialeftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->australlialeftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->australlialeftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->australlialeftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->australliarightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->australliarightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->australliarightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->australliarightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->australliarightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->australliarightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->australliarightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->australliarightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->australliarightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->australliarightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->australliarightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->australliarightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->australliarightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->australliarightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->australliarightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->australliarightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->australliarightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->australliarightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->australliarightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->australliarightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllAustralliaInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('viewAllAustralliaSearchResultPage', $data);
	   }



	   public function contactUS()
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		   $sujestList		= $this->M_programe_manage->findAll();	
			
			$programList 	= array();					
			foreach($sujestList as $k => $v){
				$programList[] = $v->programe_name;
			}
			
			$data['programList'] 	= $programList;
		    
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAll();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('contactUsPage', $data);
	
		}
		
		
		public function contactUsAction()
	
		{		
			$name				= $this->input->post('name');
			$email			    = $this->input->post('email');
			$mobile				= $this->input->post('mobile');
			$message			= $this->input->post('message');
			$subject			= $this->input->post('subject');

			
			$userEmail	        = "info@nextadmission.com";
			
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset']  = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			
			$this->email->from($email, $name);
			$this->email->to($userEmail);			
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->reply_to($senderEmail);
			
			 if($this->email->send())
				 {
				  $this->load->view('contact_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}
	   
	   
	   public function europ()
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		    $data['page_title']  		= self::Title;	
		    $data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllEurop();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Europ";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['europCountry']		= $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->europtopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->uroptopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->uropleftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->uropleftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->uropleftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->uropleftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->uropleftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->uropleftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->uroprightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->uroprightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->uroprightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->uroprightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->uroprightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->uroprightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->uroprightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->uroprightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->uroprightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->uroprightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->uroprightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->uroprightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->uroprightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->uroprightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->uroprightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->uroprightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->uroprightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->uroprightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->uroprightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->uroprightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllEuropInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('europPage', $data);
	     	
	   
	   }




	    public function searchEurop()
	   {
	     	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }


		     $country_id      		    = $this->input->post('country_id');
			 $organize_id      			= $this->input->post('organize_id');
			 $program_id      			= $this->input->post('program_id');
			
			 $data['country_id']        = $country_id;
			 $data['organize_id']       = $organize_id;
			 $data['program_id']        = $program_id;


			$data['searchEuropResult']   	= $this->M_search_table->findByAllAsiaSearchData($country_id, $organize_id, $program_id);


		    $data['page_title']  		= self::Title;	
		    $data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllEurop();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$region                     = "Europ";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['europCountry']		= $this->M_country_manage->findAllInfo($regionId);
			$data['europOrganization']	= $this->M_all_user_registration->findAllInfoByCountryId($country_id);

			
			
			$data['topAddInfo1']	    = $this->M_add_manage->europtopAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->uroptopAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->uropleftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->uropleftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->uropleftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->uropleftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->uropleftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->uropleftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->uroprightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->uroprightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->uroprightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->uroprightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->uroprightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->uroprightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->uroprightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->uroprightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->uroprightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->uroprightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->uroprightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->uroprightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->uroprightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->uroprightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->uroprightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->uroprightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->uroprightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->uroprightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->uroprightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->uroprightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllEuropInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	     	$this->load->view('viewAllEuropSearchResultPage', $data);
	     	
	   
	   }
	   
	   
	   
	   
	   
	   public function recruterAd()
	
		{	
		   if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
		   }
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();	
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$data['middleRecruAdInfo']	= $this->M_job_post_manage->findAll();
			
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			
			
			$data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
		    $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
		    $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
		    $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$this->load->view('recruterAdPage', $data);
	
		}
		
		public function adWiseCompanyDetail($organizeId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		    
			$data['page_title']  		= self::Title;
			$data['newsAndEventInfo']	= $this->M_organize_profile_manage->findAllNewsByOrganize('organize_slide_manage', $organizeId);
			$data['organizeId']       	= $organizeId;	
			$data['recentPhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoByOrganize('organize_photo_gallery_manage', $organizeId);
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findAboutByUser('organize_contact_about_manage', $organizeId);
			$data['orgDetailInfo']		= $this->M_all_user_registration->findById($organizeId);
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['orgAdvertiseInfo']   = $this->M_add_manage->findAllCurrentAdByOrganize($organizeId);
			
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfoforweb1($organizeId);
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfoforweb2($organizeId);
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('adWiseCompanyDetailPage', $data);
	
		}
		
		
		
		public function organizeAdvertise($organizeId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		    
			$data['page_title']  		= self::Title;
			$data['organizeId']       	= $organizeId;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['organizeSlideInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $organizeId);
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['orgAdvertiseInfo']   = $this->M_add_manage->findAllByOrganize($organizeId);
			
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('orgWiseAdvertiseDetailPage', $data);
	
		}
		
		
		
		public function organizeAbout($organizeId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		    
			$data['page_title']  		= self::Title;
			$data['organizeId']       	= $organizeId;
			$data['newsAndEventInfo']	= $this->M_organize_profile_manage->findAllNewsByOrganize('organize_slide_manage', $organizeId);
			$data['recentPhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoByOrganize('organize_photo_gallery_manage', $organizeId);
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findAboutByUser('organize_contact_about_manage', $organizeId);
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['orgAdvertiseInfo']   = $this->M_job_post_manage->findAllByOrganize($organizeId);
			
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('orgAboutPage', $data);
	
		}
		
		
		
		public function organizeContact($organizeId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		    
			$data['page_title']  		= self::Title;
			$data['organizeId']       	= $organizeId;	
			$data['organizeId']       	= $organizeId;
			$data['newsAndEventInfo']	= $this->M_organize_profile_manage->findAllNewsByOrganize('organize_slide_manage', $organizeId);
			$data['recentPhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoByOrganize('organize_photo_gallery_manage', $organizeId);
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['organizeSlideInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $organizeId);
			$data['orgContactInfo']		= $this->M_organize_profile_manage->findContactByUser('organize_contact_about_manage', $organizeId);
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['orgAdvertiseInfo']   = $this->M_job_post_manage->findAllByOrganize($organizeId);
			
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('orgContactPage', $data);
	
		}
		
		
		public function organizePhotogallery($organizeId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		    
			$data['page_title']  		= self::Title;
			$data['organizeId']       	= $organizeId;	
			$data['organizeId']       	= $organizeId;
			$data['newsAndEventInfo']	= $this->M_organize_profile_manage->findAllNewsByOrganize('organize_slide_manage', $organizeId);
			$data['recentPhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoByOrganize('organize_photo_gallery_manage', $organizeId);
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['orgPhotoInfo']		= $this->M_organize_profile_manage->findAllPhotoDataByUser('organize_photo_gallery_manage', $organizeId);
			$data['orgContactInfo']		= $this->M_organize_profile_manage->findContactByUser('organize_contact_about_manage', $organizeId);
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['orgAdvertiseInfo']   = $this->M_job_post_manage->findAllByOrganize($organizeId);
			
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('orgPhotogalleryPage', $data);
	
		}
		
		
		public function organizeCourseDetails($organizeId, $courseId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
			  $visitorId       			= $userDetails->id;

		   }

		     $linkUrl     = uri_string();
		     $current_url = current_url();

		    if ($userDetails->user_type =='General user') {
			    $datav['user_type']    = "general";
			} else {
			    $datav['user_type']    = "organize";
			}

			   $datav['organize_id']    = $organizeId;
			if (!empty($visitorId)) {
				$datav['visitor_id']    = $visitorId;
			} else {
			    $datav['visitor_id']    = "unknown";	
			}
                 $datav['programe_id']  = $courseId;
                 $datav['visit_link']   = $linkUrl;
                 $datav['page_type']   	= "Programe";
                 $datav['date_time']    = date("Y-m-d H:i:s");
                 $datav['date']    		= date("Y-m-d");


			$urlArray = unserialize(get_cookie('visitor_tracking'));

			if(!in_array($current_url, $urlArray)) {
				$urlArray[] = $current_url;
			
			  	$cookieValue = array(
			        'name'   => 'visitor_tracking',
			        'value'  => serialize($urlArray),
			        'expire' => '86400',
			        'domain' => '.localhost',
			        'path'   => '/'
		        );
               
               if ($organizeId > 0) {
                 $this->M_visitor_count->save($datav);
               }
	          
	           set_cookie($cookieValue);
			}

		   
		    
			$data['page_title']  		= self::Title;
			$data['organizeId']       	= $organizeId;	
			$data['newsAndEventInfo']	= $this->M_organize_profile_manage->findAllNewsByOrganize('organize_slide_manage', $organizeId);
			$data['recentPhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoByOrganize('organize_photo_gallery_manage', $organizeId);
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['organizeSlideInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $organizeId);
			$data['orgCouWiseDetail']	= $this->M_programe_manage->findByOrgId($courseId, $organizeId);
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['orgAdvertiseInfo']   = $this->M_job_post_manage->findAllByOrganize($organizeId);
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizeCourseDetailsPage', $data);
	
		}
		
		
		
		
		public function webQuery($organizeId)
		{
			$name		         = $this->input->post('name');
			$senderEmail		 = $this->input->post('email');
			$subject 		     = $this->input->post('subject');
			$messageBody 		 = $this->input->post('message');
			$organizeDetails     = $this->M_all_user_registration->findById($organizeId);
			$userEmail           = $organizeDetails->user_id;
			
			$message = 'Name : ' . $name;
	
			$message .= "\r\n";
		
			$message .= 'Email : ' . $senderEmail;
		
			$message .= "\r\n";
	
	
			$message .= "\r\n";
		
			$message .= "\r\n";
		
			$message .= 'Details : ' . $messageBody;
		
			
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			
			$this->email->from($senderEmail, $name);
			$this->email->to($userEmail);			
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->reply_to($senderEmail);
			$this->email->send();
			
			redirect('home/adWiseCompanyDetail/'.$organizeId);
				
		}
		
		
		
		public function orgWiseAdvritismentView($adId, $organizeId)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $visitorId       			= $userDetails->id;
		   }

			 $linkUrl     = uri_string();
			 $current_url = current_url();

			 if ($userDetails->user_type =='General user') {
			    $datav['user_type']    = "general";
			} else {
			    $datav['user_type']    = "organize";
			}

			   $datav['organize_id']    = $organizeId;
			if (!empty($visitorId)) {
				$datav['visitor_id']    = $visitorId;
			} else {
			    $datav['visitor_id']    = "unknown";	
			}
                 $datav['ad_id']    	= $adId;
                 $datav['visit_link']   = $linkUrl;
                 $datav['page_type']   	= "Advertisement";
                 $datav['date_time']   	= date("Y-m-d H:i:s");
                 $datav['date']    		= date("Y-m-d");


			$urlArray = unserialize(get_cookie('visitor_tracking'));

			if(!in_array($current_url, $urlArray)) {
				$urlArray[] = $current_url;
			
			  	$cookieValue = array(
			        'name'   => 'visitor_tracking',
			        'value'  => serialize($urlArray),
			        'expire' => '86400',
			        'domain' => '.localhost',
			        'path'   => '/'
		        );
               
               if ($organizeId > 0) {
                 $this->M_visitor_count->save($datav);
               }
	          
	           set_cookie($cookieValue);
			}


           //	print_r(unserialize(get_cookie('visitor_tracking')));		   
		   
		    $data['organizeId']       	= $organizeId;	
			$data['orgProgramInfo']		= $this->M_programe_manage->findByUser($organizeId);
			$data['viewAdInfo']	    	= $this->M_add_manage->findById($adId);
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfoforweb1($organizeId);
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfoforweb2($organizeId);
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
			$this->load->view('orgWiseAdvritismentViewPage', $data);
	
		}

		public function globDetails()
		{	
		  		 
		   $data['globValue'] 					= $this->input->post('globValue');
		   $regionDetails 						= $this->M_region_manage->findByRegionName($data['globValue']);
		   $regionId 							= $regionDetails->id;
		   $data['regionWiseCountryList'] 		= $this->M_country_manage->findAllInfo($regionId);

		   $this->load->view('globDetailsViewPage', $data);
		 
	  	}		
		
		 
		  public function countryWiseOrganize()
			{
				$country_id 	   = $this->input->post('id');
				
				$organizeList      = $this->M_all_user_registration->findAllOrganizeByCountry($country_id);				
			
				echo '<option value="">Sellect Organization</option>';
				foreach($organizeList as $v) {
					echo '<option value="'.$v->id.'">'.$v->name.'</option>';
				}
				
			}
			
			
			
			public function OrganizeWiseProgram()
			{
				$organize_id 	   = $this->input->post('id');
				
				$organizeList      = $this->M_programe_manage->findByUser($organize_id);				
			
				echo '<option value="">Sellect Program</option>';
				foreach($organizeList as $v) {
					echo '<option value="'.$v->id.'">'.$v->programe_name.'</option>';
				}
				
			}
		
	
		
	

}

