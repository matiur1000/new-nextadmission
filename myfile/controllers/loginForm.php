<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginForm extends MX_Controller {

		static $model 	 = array('M_all_user_registration','M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub',
		'M_organization_user_registration','M_add_manage','M_general_user_registration');
		static $helper   = array('url','generalauthentication','userauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
	
		}
	

		public function index()	
		{		
			$ref = $this->input->get('ref');
			
			if(!empty($ref)) {
				$this->session->set_userdata(array('ref' => $ref));	
			}
			
			if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
			
			$data['msg']  				= '';	
			$data['page_title']  		= self::Title;	
			$data['page_title']  		= self::Title;	
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		   	$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		   	$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		   	$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$this->load->view('loginPage', $data);
	
		}
		
		
		  public function loginAuthenticate()
			{
			   $type		     = $this->input->post('type');
			     if($type != ''){
				   if($type == 'organization'){
					  organizationLoginUser();
				   }else{
					  generalLoginUser();
					}
					
				}else{
				   echo "2";
				}
			}
	
		
		
		 public function organizeAuthenticate()
			{
				organizationLoginUser();
			}
	
	     public function generalAuthenticate()
			{
				generalLoginUser();
			}
	
			public function organizeLogout()
		
			{	
		
				orglogoutUser();
		
			}
			
			public function generalLogout()
		
			{	
		
				genlogoutUser();
		
			}

	

}

