<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

	static $model 	 = array('M_admin');
	static $helper   = array('url', 'authentication');

	public function __construct(){

		parent::__construct();
		$this->load->database();
		$this->load->model(self::$model);
		$this->load->helper(self::$helper);
		$this->load->library('session');

	}

	public function index()

	{		

		$data['msg']		= '';
		$this->load->view('loginPage', $data);

	}
	public function authenticate()
	{
		loginUser();
	}
	
	public function logout() {
		logoutUser();
	}

}

