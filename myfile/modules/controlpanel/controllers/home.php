<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Home extends MX_Controller {

	static $model 	 = array('M_admin');
	static $helper   = array('url', 'authentication');

	const  Title	 = 'Next Admission';

	public function __construct(){

		parent::__construct();

		$this->load->database();

		$this->load->model(self::$model);

		$this->load->helper(self::$helper);

		$this->load->library('upload');

		$this->load->library('session');

		$this->load->library('form_validation');

	}

	

	

	public function index()

	{	

		//if(isActiveAdmin()){

			

			$data['page_title']  = self::Title;			

			$this->load->view('controlpanel/dashboard', $data);

		//}

	}

	

	public function logout()

	{	

		logoutAdmin();

	}

	

	

}

