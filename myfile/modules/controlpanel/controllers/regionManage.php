<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RegionManage extends MX_Controller {

		static $model 	 = array('M_admin','M_region_manage','M_country_manage','M_city_manage');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('pagination');
			$this->load->library('upload');
			$this->load->library('session');
	
		}
	

		public function index()
	
		{		
			$data['page_title']  = self::Title;	
			$data['regionInfo']	= $this->M_region_manage->findAll();
			$this->load->view('controlpanel/rigionManage/rigionManagePage', $data);
	
		}
	
		public function regionList_()
		{
			$data['regionInfo']	= $this->M_region_manage->findAll();
			$this->load->view('controlpanel/rigionManage/rigionList', $data);
		}
		
		
		public function countrylist_()
		{
			$data['countryInfo']	= $this->M_country_manage->findAll(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/regionManage/countryManage');
			$config['total_rows'] 	= $this->M_country_manage->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 10;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config); 	
			$this->load->view('controlpanel/rigionManage/countrylist', $data);
		}
		
		public function citylist_()
		{
			
			$data['cityInfo']		= $this->M_city_manage->findAll(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/regionManage/cityManage');
			$config['total_rows'] 	= $this->M_city_manage->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 10;
			$config['num_links'] 	= 20;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config); 		
			$this->load->view('controlpanel/rigionManage/citylist', $data);
		}
		
		
		public function regionEdit()
		{
			$id 				= $this->input->post('id');		
			$regionEditInfo 	= $this->M_region_manage->findById($id);
					
			echo json_encode($regionEditInfo);
		}
		
		public function regionStore()
			{	
				$id		    		    = $this->input->post('id');
				$data['region_name'] 	= $this->input->post('region_name');
				
				if(!empty($id)) {
					$this->M_region_manage->update($data,$id);
					}else{
					$this->M_region_manage->save($data);
					}
					$this->regionList_();	
				
			}
		
	
		public function countryManage($onset = 0)
	
		{	
			$data['page_title']  	= self::Title;	
			$data['regionInfo']		= $this->M_region_manage->findAll();
			$data['countryInfo']	= $this->M_country_manage->findAll(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/regionManage/countryManage');
			$config['total_rows'] 	= $this->M_country_manage->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 10;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config); 	
			$this->load->view('controlpanel/rigionManage/countryManagePage', $data);	
	
		}
		
		public function cityManage($onset = 0)
	
		{	
			$data['page_title']  	= self::Title;	
			$data['regionInfo']		= $this->M_region_manage->findAll();
			$data['countryInfo']	= $this->M_country_manage->findAll();	
			$data['cityInfo']		= $this->M_city_manage->findAll(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/regionManage/cityManage');
			$config['total_rows'] 	= $this->M_city_manage->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 10;
			$config['num_links'] 	= 20;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config); 		
			$this->load->view('controlpanel/rigionManage/cityManagePage', $data);	
	
		}
		  
		  
		  public function countryName()
			{
				$region_id 	   = $this->input->post('id');
				
				//$where 			   = array('region_id' => $region_id);		
				$countryList   	   = $this->M_country_manage->findAllInfo($region_id);				
			
				echo '<option value="">Sellect Country Name</option>';
				foreach($countryList as $v) {
					echo '<option value="'.$v->id.'">'.$v->country_name.'</option>';
				}
				
			}
		
		
		 public function cityStore()
			{	
				$id		    		    = $this->input->post('id');
				$data['region_id'] 		= $this->input->post('region_id');
				$data['country_id'] 	= $this->input->post('country_id');
				$data['city_name'] 		= $this->input->post('city_name');
				$data['city_code'] 		= $this->input->post('city_code');
				
				if(!empty($id)) {
					$this->M_city_manage->update($data,$id);
					}else{
					$this->M_city_manage->save($data);
					}
					$this->citylist_();	
				
			}
			
			
			
		 public function countryStore()
			{	
				$id		    		    = $this->input->post('id');
				$data['region_id'] 		= $this->input->post('region_id');
				$data['country_name'] 	= $this->input->post('country_name');
				$data['country_code'] 	= $this->input->post('country_code');
				$data['nationality'] 	= $this->input->post('nationality');
				
				if(!empty($id)) {
					$this->M_country_manage->update($data,$id);
					}else{
					$this->M_country_manage->save($data);
					}
					$this->countrylist_();	
				
			}
		
		
		
		
		 public function regiondelete($id)
		 {	
			$this->M_region_manage->destroy($id);
			$this->index();
		}
		
		
		public function countrydelete($id)
		{	
			$this->M_country_manage->destroy($id);
			$this->countryManage();
		}
		
		
		
		
	      public function countryEdit()
			{
				$id 				= $this->input->post('id');		
				$countryEditInfo 	= $this->M_country_manage->findById($id);
						
				echo json_encode($countryEditInfo);
			}
			
			public function cityEdit()
			{
				$id 						= $this->input->post('id');		
				$cityEditInfo 				= $this->M_city_manage->findById($id);
				$cityEditInfo->countryList 	= $this->M_country_manage->findAllInfo($cityEditInfo->region_id);
						
				echo json_encode($cityEditInfo);
			}
			
		  public function citydelete($id)
			{	
				$this->M_city_manage->destroy($id);
				$this->cityManage();
			}
		
			
		

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

