<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RegisterMember extends MX_Controller {

		static $model 	 = array('M_admin','M_region_manage','M_country_manage','M_city_manage','M_all_user_registration',
		'M_general_user_registration','M_organization_user_registration','M_search_table');
		static $helper	= array('url', 'authentication','allusersms','generalusersms','organizationusersms');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('session');
			$this->load->library('email');
			$this->load->library('pagination');
	
		}
	

		public function index($onset = 0)
	
		{		
			$data['page_title']  	= self::Title;	
			$data['regionInfo']		= $this->M_region_manage->findAll();
			$data['allRegterInfo']	= $this->M_all_user_registration->findAll(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/registerMember/index');
			$config['total_rows'] 	= $this->M_all_user_registration->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 5;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
	
			$this->pagination->initialize($config); 
			$this->load->view('controlpanel/registrationView/organizationRegisterPage', $data);
	
		}


		public function genRegStore1()
		{	
			
			$data['country_id'] 			= $this->input->post('country_id');
			$data['user_type'] 				= "General user";
			$data['name'] 					= $this->input->post('name');
			$data['user_id'] 				= $this->input->post('user_id');
			$data['password'] 				= $this->input->post('password');
			$user_id 						= $this->input->post('user_id');
			$data['status'] 				= "In Active";
			$regionDetail 					= $this->M_country_manage->findById($data['country_id']);
			$data['region_id'] 				= $regionDetail->region_id;
			
			if($data['user_id']) {
		           $emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
					if(!empty($emil_chk)){
					$this->load->view('email_alert');
				 }else{
				 
					$userData	= array('lastId' => $user_id);
					$this->session->set_userdata($userData);
				
					$gen_id = $this->M_all_user_registration->save($data);

					$orgazeDetails 						= $this->M_all_user_registration->findById($gen_id);
					$datas['res_user_id'] 				= $gen_id;
					$datas['user_type'] 				= $orgazeDetails->user_type;
					$datas['name'] 						= $orgazeDetails->name;
					$datas['region_id'] 				= $regionDetail->region_id;
					$datas['country_id'] 				= $data['country_id'];
					

					$this->M_search_table->save($datas);
						
			
					
				}
				
				  	$title 							= "Verify Account";
					$message						= '<a href="'.site_url('registration/emailVerification/'.$gen_id).'">.Your Account need to verify go to the link for verify.</a>';
					$name					   	 	= "Next Admission";
					$senderEmail	            	= "info@nextadmission.com";
					$userEmail	                	= $user_id;
					
					$config['protocol'] 			= 'sendmail';
					$config['mailpath'] 			= '/usr/sbin/sendmail';
					$config['charset'] 				= 'iso-8859-1';
					$config['wordwrap'] 			= TRUE;
					$config['mailtype'] 			='html';
					
					$this->email->initialize($config);
					
					
					$this->email->from($senderEmail, $name);
					$this->email->to($userEmail);			
					$this->email->subject($title);
					$this->email->message($message);
					$this->email->reply_to($senderEmail);
					$this->email->send();
					
				    $this->registration_list();	
				
			  }
		   }



		   public function orgRegStore()
		 {	
				
			$data['account_name'] 	    	= $this->input->post('account_name');
			$data['name'] 	    			= $this->input->post('organizationname');
			$data['user_id'] 				= $this->input->post('user_id');
			$user_id 						= $this->input->post('user_id');
			$data['password'] 				= $this->input->post('password');
			$data['user_type'] 				= "Organization User";
			$data['status'] 				= "In Active";
			$data['country_id'] 			= $this->input->post('country_id_org');
			$regionDetail 					= $this->M_country_manage->findById($data['country_id']);
			$data['region_id'] 				= $regionDetail->region_id;
			
			
			
			
			 
			$userData	= array('orgLastId' => $user_id);
			$this->session->set_userdata($userData);
		
			$orgnize_id = $this->M_all_user_registration->save($data);

			$orgazeDetails 					= $this->M_all_user_registration->findById($orgnize_id);
			$datas['res_user_id'] 			= $orgnize_id;
			$datas['user_type'] 			= $orgazeDetails->user_type;
			$datas['name'] 					= $orgazeDetails->name;
			$datas['region_id'] 			= $regionDetail->region_id;
			$datas['country_id'] 			= $data['country_id'];

			$this->M_search_table->save($datas);
			
			
			  
			  	$title 							= "Verify Account";
				$message						= '<a href="'.site_url('registration/emailVerification/'.$orgnize_id).'">.Your Account need to verify go to the link for verify.</a>';
				$name					   	 	= "Next Admission";
				$senderEmail	            	= "info@nextadmission";
				$userEmail	                	= $user_id;
				
				$config['protocol'] 			= 'sendmail';
				$config['mailpath'] 			= '/usr/sbin/sendmail';
				$config['charset'] 				= 'iso-8859-1';
				$config['wordwrap'] 			= TRUE;
				$config['mailtype'] 			='html';
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				$this->email->send();
					
	        
				
				$this->registration_list();	
		  
	   }

	   public function registration_list()
	
		{		
			$data['page_title']  	= self::Title;	
			$data['countryInfo']	= $this->M_country_manage->findAllCountry();
			$data['allRegterInfo']	= $this->M_all_user_registration->findAllRegData();	
			$this->load->view('controlpanel/registrationView/memberRegistrationListPage', $data);
	
	
		}
		
		   
		
		public function generalRegister($onset = 0)
	
		{		
			$data['page_title']  	= self::Title;	
			$data['regionInfo']		= $this->M_region_manage->findAll();
			$data['allRegterInfo']	= $this->M_all_user_registration->findAllGeneral(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/registerMember/generalRegister');
			$config['total_rows'] 	= $this->M_all_user_registration->countAll2();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 5;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
	
			$this->pagination->initialize($config); 
			$this->load->view('controlpanel/registrationView/generalRegisterPage', $data);
		}
		
		
		public function userWiseDetails($id)
	
		{		
			$data['page_title']  		= self::Title;	
			$data['userWiseFullInfo']	= $this->M_all_user_registration->findAllValu($id);	
			$this->load->view('controlpanel/registrationView/userWiseDetailsPage', $data);
	
		}
		
		public function orgUserWiseDetails($id)
	
		{		
			$data['page_title']  		= self::Title;	
			$data['userWiseFullInfo']	= $this->M_all_user_registration->findAllorgValu($id);	
			$this->load->view('controlpanel/registrationView/orgUserWiseDetailsPage', $data);
	
		}
		
		public function orgUserWiseMail($id)
	
		{		
			$userWiseFullInfo	        = $this->M_all_user_registration->findAllValu($id);	
			$userEmail	                = $userWiseFullInfo->user_id;	
			$title 						= $this->input->post('title');
			$message					= $this->input->post('message');
			$name					    = "Find Global Study";
			$senderEmail	            = "ayenal754@gmail.com";
			
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			
			$this->email->from($senderEmail, $name);
			$this->email->to($userEmail);			
			$this->email->subject($title);
			$this->email->message($message);
			$this->email->reply_to($senderEmail);
			
				if($this->email->send())
				{
				  $this->load->view('controlpanel/registrationView/email_send_alert');
				}
				 else
				{
				  show_error($this->email->print_debugger());
				} 
	        
		}


		public function genUserWiseMail($id)
	
		{		
			$userWiseFullInfo	        = $this->M_all_user_registration->findAllValu($id);	
			$userEmail	                = $userWiseFullInfo->user_id;	
			$title 						= $this->input->post('title');
			$message					= $this->input->post('message');
			$name					    = "Find Global Study";
			$senderEmail	            = "ayenal754@gmail.com";
			
			$name					    = "Find Global Study";
			$senderEmail	            = "ayenal754@gmail.com";
			
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			
			$this->email->from($senderEmail, $name);
			$this->email->to($userEmail);			
			$this->email->subject($title);
			$this->email->message($message);
			$this->email->reply_to($senderEmail);
	      
		 if($this->email->send())
			 {
			  $this->load->view('controlpanel/registrationView/general_email_send_alert');
			 }
			 else
			{
			  show_error($this->email->print_debugger());
			} 
	        
		}
		
		public function organizationUserMailAction()
	
		{		
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');
			$mailList					= $this->M_all_user_registration->findAllOrganizeUser();	
			
			foreach($mailList as $v){
				$userEmail  = $v->user_id;

				$name					= "Next Admission";
				$senderEmail	        = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] 	= 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
		      }
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/registrationView/email_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
	        
		}



		public function generalUserMailAction()
	
		{		
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');
			$mailList					= $this->M_all_user_registration->findAllGeneralUserEmail();	
			
			foreach($mailList as $v){
				$userEmail  = $v->user_id;

				$name					    = "Find Global Study";
				$senderEmail	            = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				}
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/registrationView/general_email_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}

		public function allUserMailAction()
	
		{		
			$userType					= $this->input->post('user_type');
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');

			if($userType == 'general'){
			 $mailList					= $this->M_all_user_registration->findAllGeneralUserEmail();					
			}else{
			 $mailList					= $this->M_all_user_registration->findAllOrganizeUser();					
			}
			
			foreach($mailList as $v){
				$userEmail  = $v->user_id;

				$name					    = "Find Global Study";
				$senderEmail	            = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				}
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/mail/all_mail_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}

		public function generalUserMailAction2()
	
		{		
			$userEmail					= $this->input->post('user_type');
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');


				$name					= "Find Global Study";
				$senderEmail	        = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] 	= 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/mail/general_mail_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}


		public function organizationUserMailAction2()
	
		{		
			$userEmail					= $this->input->post('user_type');
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');

				$name					    = "Find Global Study";
				$senderEmail	            = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/mail/organization_mail_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}

		public function allSmsSend()
	
		{		
			$data['page_title']  	= self::Title;	
			$this->load->view('controlpanel/sms/allUsertSmsPage', $data);
	
		}
		
		public function smsSendGeneralUserWise()
	
		{		
			$data['page_title']  	= self::Title;	
			$data['smsinfo']   		= $this->M_all_user_registration->findByGeneralUser($user_id);
			
			$this->load->view('controlpanel/sms/smsSendGeneralUserWisePage', $data);
	
		}
		
		public function smsSendOrganizationUserWise()
	
		{		
			$data['page_title']  	= self::Title;
			$data['smsinfo']   		= $this->M_all_user_registration->findByOrganizatinUser($user_id);	
			$this->load->view('controlpanel/sms/smsSendOrganizationUserWisePage', $data);
	
		}
		public function allUserMailSend()
	
		{		
			$data['page_title']  	= self::Title;	
			$this->load->view('controlpanel/mail/allUsertMailPage', $data);
	
		}
		public function generalUserMailSend()
	
		{		
			$data['page_title']  	= self::Title;
			$data['mailList']	    = $this->M_all_user_registration->findAllGeneralUserEmail();					
			$this->load->view('controlpanel/mail/generalUserMailPage', $data);
	
		}
		public function organizationUserMailSend()
	
		{		
			$data['page_title']  	= self::Title;
			$data['mailList']		= $this->M_all_user_registration->findAllOrganizeUser();					
			$this->load->view('controlpanel/mail/organizationUserMailPage', $data);
	
		}
		
		public function allUserSmsAction()
			{
				sendSms();
			}
			
		public function smsSendGeneralWiseAction()
			{
				generalSendSms();
			}
			
		public function organizationUserSmsAction()
			{
				organizationSendSms();
			}
		
		public function memberRegistration()
	
		{		
			$data['page_title']  	= self::Title;	
			$data['regionInfo']		= $this->M_region_manage->findAll();
			$data['countryInfo']	= $this->M_country_manage->findAllCountry();
			$data['allRegterInfo']	= $this->M_all_user_registration->findAllRegData();	
			$this->load->view('controlpanel/registrationView/memberRegistrationPage', $data);
	
		}
		
		public function store()
			{	
				$id		    		    		= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				$image		 				= $this->input->post('register_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
					$register_info = $this->M_all_user_registration->findById($id);
					
					if( !empty($register_info->image) && file_exists('./Images/Register_image/'.$register_info->image) && !empty($image) ) {					
						unlink('./Images/Register_image/'.$register_info->image);	
					}
					$this->M_all_user_registration->update($data, $id);
					$this->registerList_();
					} else {	
					if($data['user_id']) {
							$emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
							if(!empty($emil_chk)){
							$this->load->view('controlpanel/registrationView/email_alert', $data);
							}else{			
						$this->M_all_user_registration->save($data);
						$this->registerList_();
				  }
				
				}
			   }
				 
			}
			
			
			
			public function orgUpdate()
			{	
				$id		    		    		= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				$image		 				= $this->input->post('register_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
					$register_info = $this->M_all_user_registration->findById($id);
					
					if( !empty($register_info->image) && file_exists('./Images/Register_image/'.$register_info->image) && !empty($image) ) {					
						unlink('./Images/Register_image/'.$register_info->image);	
					}
					$this->M_all_user_registration->update($data, $id);
					$this->orgList_();
					} else {	
					if($data['user_id']) {
							$emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
							if(!empty($emil_chk)){
							$this->load->view('controlpanel/registrationView/email_alert', $data);
							}else{			
						$this->M_all_user_registration->save($data);
						$this->orgList_();
				  }
				
				}
			   }
				 
			}
			
			
			public function genUpdate()
			{	
				$id		    		    		= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				$image		 				= $this->input->post('register_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
					$register_info = $this->M_all_user_registration->findById($id);
					
					if( !empty($register_info->image) && file_exists('./Images/Register_image/'.$register_info->image) && !empty($image) ) {					
						unlink('./Images/Register_image/'.$register_info->image);	
					}
					$this->M_all_user_registration->update($data, $id);
					$this->genList_();
					} else {	
					if($data['user_id']) {
							$emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
							if(!empty($emil_chk)){
							$this->load->view('controlpanel/registrationView/email_alert', $data);
							}else{			
						$this->M_all_user_registration->save($data);
						$this->genList_();
				  }
				
				}
			   }
				 
			}
			
			public function genList_()
			{
				$data['allRegterInfo']	= $this->M_all_user_registration->findAllGeneral($where);
				$this->load->view('controlpanel/registrationView/genList', $data);
			}
			
			public function registerList_()
			{
				$data['allRegterInfo']	= $this->M_all_user_registration->findAll();	
				$this->load->view('controlpanel/registrationView/registerList', $data);
			}
			
			public function orgList_()
			{
				$data['allRegterInfo']	= $this->M_all_user_registration->findAll($where);
				$this->load->view('controlpanel/registrationView/orgList', $data);
			}
			
			public function edit()
			{
				$id 						= $this->input->post('id');		
				$regEditInfo 				= $this->M_all_user_registration->findById($id);
				$regEditInfo->countryList 	= $this->M_country_manage->findAllInfo($regEditInfo->region_id);
				$regEditInfo->cityList 		= $this->M_city_manage->findAllCity($regEditInfo->country_id);
						
				echo json_encode($regEditInfo);
			}
			
			
			public function organizEdit()
			{
				$id 						= $this->input->post('id');		
				$regEditInfo 				= $this->M_all_user_registration->findById($id);
				$regEditInfo->countryList 	= $this->M_country_manage->findAllInfo($regEditInfo->region_id);
				$regEditInfo->cityList 		= $this->M_city_manage->findAllCity($regEditInfo->country_id);
						
				echo json_encode($regEditInfo);
			}
			
			public function genEdit()
			{
				$id 						= $this->input->post('id');		
				$regEditInfo 				= $this->M_all_user_registration->findById($id);
				$regEditInfo->countryList 	= $this->M_country_manage->findAllInfo($regEditInfo->region_id);
				$regEditInfo->cityList 		= $this->M_city_manage->findAllCity($regEditInfo->country_id);
						
				echo json_encode($regEditInfo);
			}
			
		

		    
		  public function countryName()
			{
				$region_id 	   = $this->input->post('id');
				
				//$where 			   = array('region_id' => $region_id);		
				$countryList   	   = $this->M_country_manage->findAllInfo($region_id);				
			
				echo '<option value="">Sellect Country Name</option>';
				foreach($countryList as $v) {
					echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';
				}
				
			}
			
			public function cityName()
			{
				$country_id 	      = $this->input->post('id');
				
				//$where 			   	  = array('country_id' => $country_id);		
			 	$countryWiseCityList   = $this->M_city_manage->findAllCity($country_id);				
			
				echo '<option value="">Sellect City Name</option>';
				foreach($countryWiseCityList as $v) {
					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';
				}
								
			}
			
	      
		  public function regDelete($id)
			{
				$model	= $this->M_all_user_registration->findById($id);
				$only_image  = $model->image;
							
				if(!empty($only_image)){
				unlink('./Images/Register_image/'.$only_image);
				}	
				$this->M_all_user_registration->destroy($id);
				$this->M_search_table->destroyUser($id);
				redirect ('controlpanel/registerMember/memberRegistration');
			}
		
		
		 public function orgDelete($id)
			{
				$model	= $this->M_all_user_registration->findById($id);
				$only_image  = $model->image;
							
				if(!empty($only_image)){
				unlink('./Images/Register_image/'.$only_image);
				}	
				$this->M_all_user_registration->destroy($id);
				 redirect ('controlpanel/registerMember');
			}
			
			
			public function genDelete($id)
			{
				$model	= $this->M_all_user_registration->findById($id);
				$only_image  = $model->image;
							
				if(!empty($only_image)){
				unlink('./Images/Register_image/'.$only_image);
				}	
				$this->M_all_user_registration->destroy($id);
				 redirect ('controlpanel/registerMember/generalRegister');
			}
		

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

