<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span></label></th>
			<th>Company Name </th>														
			<th class="hidden-480">Title</th>
			<th class="hidden-480">Company Logo </th>
			<th class="hidden-480">Position</th>
			<th class="hidden-480">Sl No </th>
			<th class="hidden-480">Add Url</th>
			<th class="hidden-480">User Id </th>
			<th class="hidden-480">Status</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
	<?php 
	// print_r($addInfo);
	 $i = 0;
	  foreach ($addInfo as $v){
		 $id  		    = $v->id;
	?>
		<tr>
			<td class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span></label></td>

			<td>
				<a href="#"><?php echo $v->company_name; ?></a></td>														
			<td class="hidden-480"><a href="#"><?php echo $v->title; ?></a></td>
			<td class="hidden-480"><img src="<?php echo base_url("Images/Add_image/$v->add_image"); ?>" height="50" width="50" /></td>
			<td class="hidden-480"><?php echo $v->positon; ?></td>
			<td class="hidden-480"><?php echo $v->serial_no; ?></td>
			<td class="hidden-480"><?php echo $v->add_link; ?></td>
			<td class="hidden-480"><?php echo $v->user_id; ?></td>
			<td class="hidden-480"><?php echo $v->status; ?></td>
			<td>
				<div class="hidden-sm hidden-xs action-buttons">
					<a class="green" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-pencil bigger-130"></i></a>

					<a class="red" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>

				<div class="hidden-md hidden-lg">
					<div class="inline pos-rel">
						<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
							<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>

						<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
							<li>
								<a class="green" href="#" data-id="<?php echo $id ?>">
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>

							<li>
								<a class="red" href="#" data-id="<?php echo $id ?>">
									<span class="red">
										<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
						</ul>
					</div>
				</div></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<script>
        $('#date').datepicker({dateFormat: 'dd/mm/yy'});
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#company_name').val(data.company_name);
					$('#title').val(data.title);
					$('#add_link').val(data.add_link);
					$('#description').val(data.description);
					$('#date').val(data.date);
					$('#status').val(data.status);
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/newsAndAdvertiseManage/newsDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>