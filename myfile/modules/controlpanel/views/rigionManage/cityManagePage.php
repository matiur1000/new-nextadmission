<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">City Manage </li>
						</ul><!-- /.breadcrumb -->

					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<span class="lbl"></span>															</label>														</th>
														<th>Sl</th>
														<th>Region  Name</th>
														<th>Country  Name</th>
														<th>City Name  </th>
														<th>City Code</th>														
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($cityInfo);
												 if(isset($cityInfo)) {
                                                 	$i = $onset + 1;
												  foreach ($cityInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>															</label>														</td>

														<td><a href="#"><?php echo $i++; ?></a></td>
														<td><a href="#"><?php echo $v->region_name; ?></a></td>
														<td><a href="#"><?php echo $v->country_name; ?></a></td>
														<td><a href="#"><?php echo $v->city_name; ?></a></td>
														<td>
															<a href="#"><?php echo $v->city_code; ?></a>														</td>														
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

																<a class="red" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>																</a>															</div>

															<div class="hidden-md hidden-lg">
																<div class="inline pos-rel">
																	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>																	</button>

																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																				<span class="green">
																					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>																				</span>																			</a>																		</li>

																		<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="ace-icon fa fa-trash-o bigger-120"></i>																				</span>																			</a>																		</li>
																	</ul>
																</div>
															</div>														</td>
													</tr>
													<?php } } ?>
													<tr>
														<th height="43" class="center">
															<label class="pos-rel"><span class="lbl"></span></label></th>
														<th colspan="9"><?php echo $this->pagination->create_links(); ?> </th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
								<form id="addForm" action="<?php echo site_url('controlpanel/regionManage/cityStore'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="border-bottom:3px solid #FF0000">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">City Manage</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                                <div class="col-xs-12 col-sm-12">
																
																<div class="form-group">
                                                                        <label for="region_id">Sellect Region Name</label>        
                                                                        <div>
                                                                           <select class="form-control" id="region_id" name="region_id"  tabindex="1" required>
                                                                                <option value="" selected>Sellect Region Name</option>
																				<?php foreach ($regionInfo as $v){?>
																				<option value="<?php echo $v->id; ?>"><?php echo $v->region_name; ?></option>
																				<?php } ?>
                                                                           </select>
                                                                        </div>
                                                                    </div> 
																	
																	<div class="space-4"></div>  
																	
                                                                    <div class="form-group">
                                                                        <label for="country_id">Sellect Country Name</label>        
                                                                        <div>
                                                                           <select class="form-control" id="country_id" name="country_id"  tabindex="2" required>
                                                                                <option value="" selected>Sellect Country Name</option>
                                                                           </select>
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>  
																	
																	<div class="form-group">
                                                                        <label for="city_name">City Name</label>        
                                                                        <div>
                                                                           <input type="text" id="city_name" placeholder="City Name" name="city_name"
                                                                            tabindex="3" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	<div class="space-4"></div>  
																	
																	<div class="form-group">
                                                                        <label for="city_code">City Code</label>        
                                                                        <div>
                                                                           <input type="text" id="city_code" placeholder="City Code" name="city_code"
                                                                            tabindex="4" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>  
                                                                </div>
                                                            </div>
                                                        </div>
            
                                                        <div class="modal-footer">
                                                            <button class="btn btn-sm" data-dismiss="modal">
                                                                <i class="ace-icon fa fa-times"></i>
                                                                Cancel
                                                            </button>
            
                                                            <button class="btn btn-sm btn-primary" type="submit">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                <span class="update">Save</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	<script>
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('controlpanel/regionManage/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});	

		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/regionManage/cityEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#city_name').val(data.city_name);
					$('#city_code').val(data.city_code);
					
					$('#country_id').html('<option value="">Select Country Name</option>');
					
					$.each(data.countryList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.country_name+'</option>';
						$('#country_id').append(option);						
					});
					
					$('#country_id').val(data.country_id);
					$('.update').text("Update");
					
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/regionManage/citydelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});

				$(document).on("click", ".blue", function(e){
                   $("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>
	</body>
</html>
