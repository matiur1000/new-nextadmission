<table  class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="center">
						<label class="pos-rel">
							<input type="checkbox" class="ace" />
							<span class="lbl"></span></label></th>
					<th>Sl</th>
					<th>Region  Name</th>
					<th>Country  Name</th>
					<th>City Name  </th>
					<th>City Code</th>														
					<th></th>
				</tr>
			</thead>

			<tbody>
			<?php 
			// print_r($cityInfo);
			 if(isset($cityInfo)) {
             	$i = $onset + 1;
			  foreach ($cityInfo as $v){
			     $id  		    = $v->id;
			?>
				<tr>
					<td class="center">
						<label class="pos-rel">
							<input type="checkbox" class="ace" />
							<span class="lbl"></span></label>td>

					<td><a href="#"><?php echo $i++; ?></a></td>
					<td><a href="#"><?php echo $v->region_name; ?></a></td>
					<td><a href="#"><?php echo $v->country_name; ?></a></td>
					<td><a href="#"><?php echo $v->city_name; ?></a></td>
					<td>
						<a href="#"><?php echo $v->city_code; ?></a>														</td>														
					<td>
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="green" href="#" data-id="<?php echo $id ?>">
								<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

							<a class="red" href="#" data-id="<?php echo $id ?>">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>																</a>															</div>

						<div class="hidden-md hidden-lg">
							<div class="inline pos-rel">
								<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
									<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>																	</button>

								<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
									<li>
										<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
											<span class="green">
												<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>																				</span>																			</a>																		</li>

									<li>
										<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
											<span class="red">
												<i class="ace-icon fa fa-trash-o bigger-120"></i>																				</span>																			</a>																		</li>
								</ul>
							</div>
						</div></td>
				</tr>
				<?php } } ?>
				<tr>
					<th height="43" class="center">
						<label class="pos-rel"><span class="lbl"></span></label></th>
					<th colspan="9"><?php echo $this->pagination->create_links(); ?> </th>
					<th></th>
				</tr>
			</tbody>
		</table>
	
	<script>
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('controlpanel/regionManage/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});	

		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/regionManage/cityEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#country_id').val(data.country_id);
					$('#city_name').val(data.city_name);
					$('#city_code').val(data.city_code);
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/regionManage/citydelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>