<table  class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="24" class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span></label></th>
				<th width="18">Sn</th>
				<th width="88">Menu Name </th>
				<th width="101">Sub Menu Name </th>
				<th width="143">Deeper Sub Name </th>
				<th width="61">Title</th>
				<th width="57">Position  </th>
				<th width="50">Image</th>
				<th width="58">Status</th>														
				<th width="54">Action</th>
			</tr>
		</thead>
	
		<tbody>
		<?php 
		// print_r($deeperSubInfo);
		if(isset($deeperSubInfo)) {
         $i = $onset + 1;
		  foreach ($deeperSubInfo as $v){
			 $id  		    = $v->id;
		?>
			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span></label></td>
	
				<td><a href="#"><?php echo $i++; ?></a></td>
				<td><a href="#"><?php echo $v->menu_name; ?></a></td>
				<td><a href="#"><?php echo $v->sub_menu_name; ?></a></td>
				<td><a href="#"><?php echo $v->deeper_sub_menu_name; ?></a></td>
				<td><a href="#"><?php echo $v->deeper_sub_title; ?></a></td>
				<td><a href="#"><?php echo $v->position; ?></a></td>
				<td><img src="<?php echo base_url("Images/Deeper_image/$v->image"); ?>" height="50" width="50" /></td>
				<td>
					<a href="#"><?php echo $v->status; ?></a></td>														
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="#" data-id="<?php echo $id ?>">
							<i class="ace-icon fa fa-pencil bigger-130"></i></a>
	
						<a class="red" href="#" data-id="<?php echo $id ?>">
							<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>
	
					<div class="hidden-md hidden-lg">
						<div class="inline pos-rel">
							<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
								<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>
	
							<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
								<li>
									<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
										<span class="green">
											<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>
	
								<li>
									<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
							</ul>
						</div>
					</div></td>
			</tr>
			<?php } } ?>
			<tr>
				<th height="43" class="center">
					<label class="pos-rel"><span class="lbl"></span></label></th>
				<th colspan="6"><?php echo $this->pagination->create_links(); ?> </th>
				<th></th>
			</tr>
		</tbody>
	</table>
<script>
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/menuManage/deeperSubEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#sl_no').val(data.sl_no);
					$('#menu_id').val(data.menu_id);
					$('#sub_menu_name').val(data.sub_menu_name);
					$('#sub_menu_title').val(data.sub_menu_title);
					$('#deeper_sub_status').val(data.deeper_sub_status);
					$('#sub_menu_description').val(data.sub_menu_description);
					$('#position_top').val(data.position_top);
					$('#position_footer').val(data.position_footer);
					$('#status').val(data.status);
				}
			});
			
			e.preventDefault();
		});
		
		//Onchang for Sub menu 
		$("#menu_id").change(function() {
		var menu_id = $("#menu_id").val();			
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/menuManage/subMenu'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : menu_id },
			dataType : "html",
			success : function(data) {			
				$("#sub_menu_id").html(data);
			}
		});
		
	});	
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/menuManage/deeperSubdelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>