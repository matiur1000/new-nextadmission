<table  class="table table-striped table-bordered table-hover">
											<thead>
											<tr>
											<th class="center">
											<label class="pos-rel">
											<span class="lbl"></span></label></th>
											<th>User Type </th>														
											<th class="hidden-480">Name </th>
											<th class="hidden-480">Country</th>
											<th class="hidden-480">User Id</th>
											<th class="hidden-480">Status</th>
											<th>Action</th>
											</tr>
											</thead>
											
											<tbody>
											<?php 
											// print_r($allRegterInfo);
											$i = 0;
											foreach ($allRegterInfo as $v){
											$id  		    = $v->id;
											?>
											<tr>
											<td class="center">
											<label class="pos-rel">
											<input type="checkbox" class="ace" />
											<span class="lbl"></span></label></td>
											
											<td><?php echo $v->user_type; ?></td>		
											<td class="hidden-480"><?php echo $v->name; ?></td>
											<td class="hidden-480"><?php echo $v->country_name; ?></td>
											<td class="hidden-480"><?php echo $v->user_id; ?></td>
											<td class="hidden-480"><?php echo $v->status; ?></td>	
											<td>
											<div class="hidden-sm hidden-xs action-buttons">
											<a class="red" href="#" data-id="<?php echo $id ?>">
											<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>
											
											<div class="hidden-md hidden-lg">
											<div class="inline pos-rel">
											<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>
											
											<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
											<li>
											<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
											<span class="blue">
											<i class="ace-icon fa fa-search-plus bigger-120"></i></span></a></li>
											
											<li>
											<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
											<span class="green">
											<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span>																			</a>																		</li>
											
											<li>
											<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
											<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>																				</span>																			</a>																		</li>
											</ul>
											</div>
											</div>														</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											<script type="text/javascript">
											  $('.red').on('click', function() {
													var x = confirm('Are you sure to delete?');
													
													if(x){
														var id = $(this).attr('data-id');
														console.log(id);
														var url = SAWEB.getSiteAction('controlpanel/registerMember/regDelete/'+id);
														location.replace(url);
													} else {
														return false;
													}
												});
											</script>