<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		
 <style>
	body{ font-family: Tahoma; font-size:13px;}
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}
 </style>
 <style>
			/* FILE UPLOAD */
			.attachmentbody{
				width: 114px;
				height: 110px;
				float: left;
			}
			.attachmentbody .progress{
				height: 12px;
				margin-top: 45px;
			}
			.attachmentbody ul {
				border-radius: 5px;
				list-style: outside none none;
				position: relative;
				float: left;
				padding: 0;
			}
			.attachmentbody ul li.remove{
				top: 0;
				right: 5px;
				position: absolute;
			}
			.attachmentbody img {
				border-radius: 5px;
				height: 100px;
				width: 100px;
				padding: 5px;
			}
			.attachmentbody ul.success {
				border: 1px solid #339933;   
			}
			.attachmentbody ul.img_error {
				background: #f0c6c3 none repeat scroll 0 0;
				border: 1px solid #cc6622;
			}
		</style>
	

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Website Manage </a>
							</li>
							<li class="active">All Register</li>
						</ul><!-- /.breadcrumb -->

					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table  class="table table-striped table-bordered table-hover">
											<thead>
											<tr>
											<th class="center">
											<label class="pos-rel">
											<span class="lbl"></span></label></th>
											<th>User Type </th>														
											<th class="hidden-480">Name </th>
											<th class="hidden-480">Country</th>
											<th class="hidden-480">User Id</th>
											<th class="hidden-480">Status</th>
											<th>Action</th>
											</tr>
											</thead>
											
											<tbody>
											<?php 
											// print_r($allRegterInfo);
											$i = 0;
											foreach ($allRegterInfo as $v){
											$id  		    = $v->id;
											?>
											<tr>
											<td class="center">
											<label class="pos-rel">
											<input type="checkbox" class="ace" />
											<span class="lbl"></span></label></td>
											
											<td><?php echo $v->user_type; ?></td>		
											<td class="hidden-480"><?php echo $v->name; ?></td>
											<td class="hidden-480"><?php echo $v->country_name; ?></td>
											<td class="hidden-480"><?php echo $v->user_id; ?></td>
											<td class="hidden-480"><?php echo $v->status; ?></td>	
											<td>
											<div class="hidden-sm hidden-xs action-buttons">
											<a class="red" href="#" data-id="<?php echo $id ?>">
											<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>
											
											<div class="hidden-md hidden-lg">
											<div class="inline pos-rel">
											<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>
											
											<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
											<li>
											<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
											<span class="blue">
											<i class="ace-icon fa fa-search-plus bigger-120"></i></span></a></li>
											
											<li>
											<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
											<span class="green">
											<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span>																			</a>																		</li>
											
											<li>
											<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
											<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>																				</span>																			</a>																		</li>
											</ul>
											</div>
											</div>														</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>


										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="border-bottom:3px solid #FF0000">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">All User Registration</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                              <div class="col-xs-12 col-sm-12">
                                                                  <label class="radio-inline">
																        <input type="radio" name="regType" value="organization" checked="checked" checkbox="checked" style="margin-top:5px" /> Organization
																	</label>
																	<label class="radio-inline">
																		<input type="radio"  name="regType" value="other" style="margin-top:5px" />  Others
																	</label>

																	<div class="col-md-12 verify" style="padding:5px">
	                                                                 </div>

																	<div id="organize_reg">
      																  <form id="regForm" action="<?php echo site_url('controlpanel/registerMember/orgRegStore'); ?>" method="post" enctype="multipart/form-data">


																		<div class="form-group" style="padding-top:10px">
	                                                                        <label for="account_name">Account Name</label>        
	                                                                        <div>
	                                                                           <input type="text" id="account_name" placeholder="Account Name" name="account_name"
	                                                                            tabindex="1" class="form-control" required /> 
																				
	                                                                        </div>
	                                                                    </div>

	                                                                    <div class="form-group">
	                                                                        <label for="organizationname">Organization Name</label>        
	                                                                        <div>
	                                                                           <input type="text" id="organizationname" placeholder="Organization Name" name="organizationname"
	                                                                            tabindex="2" class="form-control" required /> 
																				
	                                                                        </div>
	                                                                    </div>


	                                                                    <div class="form-group">
	                                                                        <label for="email">UserId</label>        
	                                                                        <div>
	                                                                           <input type="email" id="user_id2" placeholder="Email" name="user_id"
	                                                                            tabindex="3" class="form-control" required /><span class="chkEmail"></span> 
																				
	                                                                        </div>
	                                                                    </div>


	                                                                    <div class="form-group">
	                                                                        <label for="country_id_org">Sellect Country Name</label>        
	                                                                        <div>
	                                                                           <select class="form-control" id="country_id_org" name="country_id_org"  tabindex="4">
	                                                                                <option value="" selected>Sellect Country Name</option>
																					<?php foreach ($countryInfo as $v){?>
																					<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
																					<?php } ?>
	                                                                           </select>
	                                                                        </div>
	                                                                    </div> 


	                                                                    <div class="form-group">
	                                                                        <label for="password">Password</label>        
	                                                                        <div>
	                                                                           <input type="password" id="password" placeholder="Password" name="password"
	                                                                            tabindex="5" class="form-control password" required /> 
	                                                                            <span class="first"></span>
																				
	                                                                        </div>
	                                                                    </div>


	                                                                    <div class="form-group">
	                                                                        <label for="con_password">Conform password</label>        
	                                                                        <div>
	                                                                           <input type="password" class="form-control conformpassword" id="con_password" name="con_password" placeholder="Confirm Password" tabindex="6">
	                                                                           <span class="second"></span>
	                                                                        </div>
	                                                                    </div>

	                                                                    <div class="form-group">
																			<div class="col-sm-12">
																			  <button type="submit" class="btn btn-primary">Create Account</button>
																		    </div>
																		</div>
																		</form>

                                                                    </div>



                                                                    <div id="other_reg" style="display:none">
	     																<form id="genReg" action="<?php echo site_url('controlpanel/registerMember/genRegStore1'); ?>" method="post" enctype="multipart/form-data">


																		<div class="form-group" style="padding-top:10px">
	                                                                        <label for="name">Account Name</label>        
	                                                                        <div>
	                                                                           <input type="text" id="name" placeholder="Full Name" name="name"
	                                                                            tabindex="1" class="form-control" required /> 
																				
	                                                                        </div>
	                                                                    </div>

	                                                                    <div class="form-group">
	                                                                        <label for="user_id">UserId</label>        
	                                                                        <div>
	                                                                           <input type="text" id="user_id" placeholder="Email" name="user_id"
	                                                                            tabindex="2" class="form-control" required />
	                                                                            <span class="chk"></span> 
	                                                                        </div>
	                                                                    </div>



	                                                                    <div class="form-group">
	                                                                        <label for="country_id">Sellect Region Name</label>        
	                                                                        <div>
	                                                                           <select class="form-control" id="country_id" name="country_id"  tabindex="3">
	                                                                                <option value="" selected>Sellect Country Name</option>
																					<?php foreach ($countryInfo as $v){?>
																					<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
																					<?php } ?>
	                                                                           </select>
	                                                                        </div>
	                                                                    </div> 


	                                                                    <div class="form-group">
	                                                                        <label for="password">Password</label>        
	                                                                        <div>
	                                                                           <input type="password" id="password" placeholder="Password" name="password"
	                                                                            tabindex="5" class="form-control password3" required /> 
	                                                                            <span class="third"></span>
																				
	                                                                        </div>
	                                                                    </div>


	                                                                    <div class="form-group">
	                                                                        <label for="con_password">Conform password</label>        
	                                                                        <div>
	                                                                           <input type="password" class="form-control conformpassword3" id="conform_password" name="conform_password" placeholder="Confirm Password" tabindex="6">
	                                                                           <span class="fourth"></span>
	                                                                        </div>
	                                                                    </div>

	                                                                    <div class="form-group">
																			<div class="col-sm-12">
																			  <button type="submit" class="btn btn-primary">Create Account</button>
																		    </div>
																		</div>
                                                                       </form>

                                                                    </div>


                                                              </div>
														   </div>
														</div>
													
						
												</div><!-- /.col -->
											</div><!-- /.row -->
										</div><!-- /.page-content -->
									</div>
								</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	
	<script>


	$(document).on("blur", "#user_id", function() {
		var userId 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userId : userId },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				$(".chk").text("This Email Already Exit!");
				$(".chk").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				$(".chk").text("");
				$(".chk").removeClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});



	$(document).on("blur", "#user_id2", function() {
		var user_id 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/userEmailChk'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { user_id : user_id },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				$(".chkEmail").text("This Email Already Exit!");
				$(".chkEmail").addClass("red");
				} else {
				$(".chkEmail").text("");
				$(".chkEmail").addClass("red");
	   			 
			  }
		    }
		 });
			
	});


	$(document).on("change", "input[name='regType']", function(){
		   var typeValue = $(this).val();

		   if(typeValue =='organization'){
	    	 $("#organize_reg").css("display", "block");
	    	 $("#other_reg").css("display", "none");
		   }else{
		     $("#organize_reg").css("display", "none");
		     $("#other_reg").css("display", "block");
		   }
		   
		  	
	});
	
	
	
	
	// Password Count	
   
   $(document).on("keyup", ".password", function() {
	 var len = $(this).val().length;
		if(len<=1){
		$(".first").text("");
		$(".first").removeClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	  } else if(len<=4){
	  	$(".first").text("Very Weak");
		$(".first").addClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
	   } else if(len<=8){

	   	$(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");

	   } else if(len<=9){

	   	$(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword", function() {
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		= $(".password").val();

	 if(conpass){

		  if(conpass != Pass){
		  	$(".second").text("Your New Password and Confirm Password donot match!");
			$(".second").addClass("red");
			$(".second").removeClass("green");
		  
		  } else {
		  	$(".second").text("Password Match");
			$(".second").removeClass("red");
			$(".second").addClass("green");
		   	
		  }

		} else {
		   $(".second").text("Password Match");
		   $(".second").removeClass("red");
		   $(".second").removeClass("green");
		  
		}
	});
	
	
	
	// Password Count


	$(document).on("keyup", ".password3", function() {
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".third").text("");
		$(".third").removeClass("red");
		$(".third").removeClass("yellow");
		$(".third").removeClass("green");
		
	  } else if(len<=4){
	  	$(".third").text("Very Weak");
		$(".third").addClass("red");
		$(".third").removeClass("yellow");
		$(".third").removeClass("green");
	   } else if(len<=8){

	   	$(".third").text("Good");
		$(".third").addClass("green");
		$(".third").removeClass("yellow");
		$(".third").removeClass("red");

	   } else if(len<=9){

	   	$(".third").text("Good");
		$(".third").addClass("green");
		$(".third").removeClass("yellow");
		$(".third").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword3", function() {
	 var conpass 	= $(this).val();
	 var Pass 		= $(".password3").val();

	 if(conpass){

		  if(conpass != Pass){
		  	$(".fourth").text("Your New Password and Confirm Password donot match!");
			$(".fourth").addClass("red");
			$(".fourth").removeClass("green");
		  
		  } else {
		  	$(".fourth").text("Password Match");
			$(".fourth").removeClass("red");
			$(".fourth").addClass("green");
		   	
		  }

		} else {
		   $(".fourth").text("Password Match");
		   $(".fourth").removeClass("red");
		   $(".fourth").removeClass("green");
		  
		}
	});
	
	// Password and confirm password match
		//callback handler for form submit
		$("#regForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			console.log(formURL);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
				  $("#listView").html(data);		
				  $("#organize_reg input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
				  $(".verify").text("Registration success go email for verify account");
				  $(".verify").addClass("green");
				}
			});
			
			e.preventDefault();
		});


		$("#genReg").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			console.log(formURL);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
				   $("#listView").html(data);		
				   $("#other_reg input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
				   $(".verify").text("Registration success go email for verify account");
				   $(".verify").addClass("green");
				}
			});
			
			e.preventDefault();
		});
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/registerMember/edit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#status').val(data.status);
					$('#user_type').val(data.user_type);
					$('#name').val(data.name);
					$('#date_of_birth').val(data.date_of_birth);
					$('#father_name').val(data.father_name);
					$('#mother_name').val(data.mother_name);
					$('#register_image').val(data.register_image);
					$('#nationality').val(data.nationality);
					$('#address').val(data.address);
					$('#city').val(data.city);
					$('#country_phone').val(data.country_phone);
					$('#mobile').val(data.mobile);
					$('#email').val(data.email);
					$('#address_permanent').val(data.address_permanent);
					$('#city_permanent').val(data.city_permanent);
					$('#country_phone_permanent').val(data.country_phone_permanent);
					$('#mobile_permanent').val(data.mobile_permanent);
					$('#email_permanent').val(data.email_permanent);
					$('#user_id').val(data.user_id);
					$('#password').val(data.password);
					$('#conform_password').val(data.password);
					$('#status').val(data.status);
					$('.update').text("Update");
					
					$('#country_id').html('<option value="">Select Country Name</option>');
					
					$.each(data.countryList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.country_name+'</option>';
						$('#country_id').append(option);						
					});
					
					$('#country_id').val(data.country_id);
					
					
					$('#city_id').html('<option value="">Select City Name</option>');
					
					$.each(data.cityList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.city_name+'</option>';
						$('#city_id').append(option);						
					});
					
					$('#city_id').val(data.city_id);
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/registerMember/regDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});

				$(document).on("click", ".blue", function(e){
                   $('.update').text("Save");
				});
				
	</script>
	</body>
</html>
