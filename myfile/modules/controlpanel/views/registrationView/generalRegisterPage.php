<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Registration View </a>
							</li>
							<li class="active">All General User Registration Details</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">
                                        <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">Send Mail</a>											
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table  class="table table-striped table-bordered table-hover">
											<thead>
											<tr>
											<th class="center">
											<label class="pos-rel">
											<span class="lbl"></span></label></th>
											<th>Sl</th>
											<th>User Type </th>														
											<th class="hidden-480">Name </th>
											<th class="hidden-480">Country</th>
											<th class="hidden-480">City</th>
											<th class="hidden-480">User Id</th>
											<th class="hidden-480">Image</th>
											<th class="hidden-480">Status</th>
											<th align="center" valign="middle">All Details</th>
											<th align="center" valign="middle">Action </th>
											</tr>
											</thead>
											
											<tbody>
											<?php 
											// print_r($allRegterInfo);
											if(isset($allRegterInfo)) {
                                              	$i = $onset + 1;
												foreach ($allRegterInfo as $v){
												$id  		    = $v->id;
											?>
											<tr>
											<td class="center">
											<label class="pos-rel">
											<input type="checkbox" class="ace" />
											<span class="lbl"></span></label></td>
											
											
											<td><?php echo $i++; ?></td>
											<td> <?php echo $v->user_type; ?></td>														
											<td class="hidden-480"><?php echo $v->name; ?></td>
											<td class="hidden-480"><?php echo $v->country_name; ?></td>
											<td class="hidden-480"><?php echo $v->city_name; ?></td>
											<td class="hidden-480"><?php echo $v->user_id; ?></td>
											<td class="hidden-480"><img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" height="50" width="50" /></td>
											<td class="hidden-480"><?php echo $v->status; ?></td>	
											<td align="center" valign="middle"><a href="<?php echo site_url('controlpanel/registerMember/userWiseDetails/'.$id);?>">View</a></td>
											<td align="center" valign="middle">
											 <div class="hidden-sm hidden-xs action-buttons">
											<a class="red" href="#" data-id="<?php echo $id ?>">
											<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>
											
											<div class="hidden-md hidden-lg">
											<div class="inline pos-rel">
											<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>
											
											<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
											<li>
											<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
											<span class="blue">
											<i class="ace-icon fa fa-search-plus bigger-120"></i></span></a></li>
											
											<li>
											<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
											<span class="green">
											<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span>																			</a>																		</li>
											
											<li>
											<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
											<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>																				</span>																			</a>																		</li>
											</ul>
											</div>
											</div>											</td>
											</tr>
											<?php } } ?>
											<tr>
												<th height="43" class="center">
													<label class="pos-rel"><span class="lbl"></span></label></th>
												<th colspan="8"><?php echo $this->pagination->create_links(); ?> </th>
												<th></th>
											</tr>
											</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
							<form id="addForm" action="<?php echo site_url('controlpanel/registerMember/generalUserMailAction'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
	                                <div id="modal-form" class="modal" tabindex="-1">
	                                    <div class="modal-dialog">
	                                        <div class="modal-content">
	                                            <div class="modal-header">
	                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                                <h4 class="blue bigger">All General User Email Send</h4>
	                                            </div>

	                                            <div class="modal-body">
	                                                <div class="row"> 
	                                                    <div class="col-xs-12 col-sm-12">
	                                                        <div class="space-4"></div>

	                                                        <div class="form-group">
	                                                            <label for="amount">Tittle</label>

	                                                           <div>
	                                                               <input type="text" id="tittle" placeholder="Tittle" name="tittle"
                                                                            tabindex="2" class="form-control" required /> 
																	
	                                                            </div>
	                                                        </div>
	                                                        <div class="form-group">
	                                                            <label for="amount">Message</label>

	                                                           <div>
                                                                        <textarea class="form-control" rows="15" cols="1" placeholder="*..Message" 
																	   tabindex="8" name="message" id="message" ></textarea>
																	
	                                                            </div>
	                                                        </div>              
	                                                    </div>
	                                                   
	                                                </div>
	                                            </div>

	                                            <div class="modal-footer">
	                                                <button class="btn btn-sm" data-dismiss="modal">
	                                                    <i class="ace-icon fa fa-times"></i>
	                                                    Cancel
	                                                </button>

	                                                <button class="btn btn-sm btn-primary" type="submit">
	                                                    <i class="ace-icon fa fa-check"></i>
	                                                    Save
	                                                </button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </form>
								
								<!-- from -->
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>

	<script type="text/javascript">
		$('#date_of_birth').datepicker({dateFormat: 'dd/mm/yy'});
   	</script>
     <script>
	// Region Wise Country
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('controlpanel/registerMember/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});
	
	
	// Country Wise Nationality
	
	$("#country_id").change(function() {
	
		var nationality = $("#country_id option:selected").attr('data-nationality');	
		var countryCode = $("#country_id option:selected").attr('data-country-code');
		
		$("#nationality").val(nationality);
		$("#mobile").val(countryCode);
		
	});	
	
	// same data pass
	
	$('#same').on('change', function(){
		var inputFields = ['#address', '#city', '#country_phone','#mobile', '#email'];
		$.each(inputFields, function(index, value){
		
			var inputValue = $(value).val();
			
			console.log(inputValue)
			
			if( inputValue != '' && $('#same:checked').val()) {
				$(value+'_permanent').val(inputValue).attr('readonly', 'readonly');
			} else {
				$(value+'_permanent').val('').removeAttr('readonly');
			}
		});
	});	
	
	// Password Count
	
    $("#password").keyup (function(){
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".first").text("");
		$(".first").removeClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	  } else if(len<=4){
	    $(".first").text("Very Weak");
		$(".first").addClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	   } else if(len<=8){
	    $(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");
		
	   } else if(len<=9){
	   $(".first").text("Strong");
	   $(".first").addClass("yellow");
	   $(".first").removeClass("green");
	   $(".first").removeClass("red");
	   }
	});
	
	// Password and confirm password match
	
    $('#submit').click(function(event){
    
        data = $('.password').val();
        var pass = data.length;
        
        if(pass < 1) {
            alert("Password cannot be blank");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('.password').val() != $('.conformpassword').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         
    });
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/registerMember/genEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#status').val(data.status);
					$('#user_type').val(data.user_type);
					$('#name').val(data.name);
					$('#date_of_birth').val(data.date_of_birth);
					$('#father_name').val(data.father_name);
					$('#mother_name').val(data.mother_name);
					$('#register_image').val(data.register_image);
					$('#nationality').val(data.nationality);
					$('#address').val(data.address);
					$('#city').val(data.city);
					$('#country_phone').val(data.country_phone);
					$('#mobile').val(data.mobile);
					$('#email').val(data.email);
					$('#address_permanent').val(data.address_permanent);
					$('#city_permanent').val(data.city_permanent);
					$('#country_phone_permanent').val(data.country_phone_permanent);
					$('#mobile_permanent').val(data.mobile_permanent);
					$('#email_permanent').val(data.email_permanent);
					$('#user_id').val(data.user_id);
					$('#password').val(data.password);
					$('#conform_password').val(data.password);
					$('#status').val(data.status);
					
					$('#country_id').html('<option value="">Select Country Name</option>');
					
					$.each(data.countryList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.country_name+'</option>';
						$('#country_id').append(option);						
					});
					
					$('#country_id').val(data.country_id);
					
					
					$('#city_id').html('<option value="">Select City Name</option>');
					
					$.each(data.cityList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.city_name+'</option>';
						$('#city_id').append(option);						
					});
					
					$('#city_id').val(data.city_id);
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/registerMember/genDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>
	
	</body>
</html>
