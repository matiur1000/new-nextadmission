<form id="addForm" action="<?php echo site_url('controlpanel/adminManage/store'); ?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id" id="id" value="" />
				<div id="modal-form" class="modal" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="blue bigger">Admin Type Manage</h4>
							</div>

							<div class="modal-body">
								<div class="row"> 
									<div class="col-xs-12 col-sm-11">
										<div class="form-group">
											<label for="admin_type">Admin Type</label>        
											<div>
											   <input type="text" id="admin_type" placeholder="Admin Name" name="admin_type"
												tabindex="1" class="form-control" value="<?php echo set_value('admin_type'); ?>" /> 
												<?php echo form_error('admin_type'); ?>
											</div>
										</div>

										<div class="space-4"></div>

										<div class="form-group">
											<label for="status">Status</label>

											<div>
											   <select class="form-control" id="status" name="status"  tabindex="2" 
											   value="<?php echo set_value('status'); ?>">
											   <?php echo form_error('status'); ?>
													<option value="" selected>Select Status</option>
													<option value="Active">Active</option>
													<option value="Inactive">Inactive</option>
											   </select>
											</div>
										</div>        
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button class="btn btn-sm" data-dismiss="modal">
									<i class="ace-icon fa fa-times"></i>
									Cancel
								</button>

								<button class="btn btn-sm btn-primary" type="submit">
									<i class="ace-icon fa fa-check"></i>
									Save
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>
<script>
	$( document ).ready(function() {
		<?php if($status == 'reset') { ?>
			$("#addForm input").val('');
		<?php } ?>
	});
	
	//callback handler for form submit
	//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
</script>