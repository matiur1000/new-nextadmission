	<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo base_url("resource/assets/css/bootstrap.min.css"); ?>" />
		<link rel="stylesheet" href="<?php echo base_url("resource/assets/font-awesome/4.2.0/css/font-awesome.min.css"); ?>" />
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo base_url("resource/assets/fonts/fonts.googleapis.com.css"); ?>" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url("resource/assets/css/ace.min.css"); ?>" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo base_url("resource/assets/css/ace-rtl.min.css"); ?>" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo base_url("resource/assets/css/ace-ie.min.css"); ?>" />
		<![endif]-->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="<?php echo base_url("resource/assets/js/html5shiv.min.js"); ?>"></script>
		<script src="<?php echo base_url("resource/assets/js/respond.min.js"); ?>"></script>
		<![endif]-->