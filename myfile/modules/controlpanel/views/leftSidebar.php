<link href="<?php echo base_url('resource/source/font-awesome.css'); ?>" rel="stylesheet">
          <div id="sidebar" class="sidebar responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<?php
					 $menuUrl     = uri_string();
			     ?>
            

				<ul class="nav nav-list">
					<li class="active">
						<a href="<?php echo site_url('controlpanel/home');?>">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

					 <li class="<?php if($menuUrl == 'controlpanel/adminManage' || $menuUrl == 'controlpanel/adminManage/createAndPermission' || $menuUrl == 'controlpanel/adminManage/paymentGetaway' ) echo 'active' ?>">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text"> Admin Manage </span>

                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">
                            <li class="<?php if($menuUrl == 'controlpanel/adminManage') echo 'active' ?>">
                                <a href="<?php echo site_url('controlpanel/adminManage');?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Admin Type Manage
                                </a>

                                <b class="arrow"></b>
                            </li>

                            <li class="<?php if($menuUrl == 'controlpanel/adminManage/createAndPermission') echo 'active' ?>">
                                <a href="<?php echo site_url('controlpanel/adminManage/createAndPermission');?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Admin Create &amp; Permission 
                                </a>

                                <b class="arrow"></b>
                            </li>
                            
                            <li class="<?php if($menuUrl == 'controlpanel/adminManage/paymentGetaway') echo 'active' ?>">
                                <a href="<?php echo site_url('controlpanel/adminManage/paymentGetaway');?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Ad Payment Getaway
                                </a>

                                <b class="arrow"></b>
                            </li>
                        </ul>
                    </li>

					<li class="<?php if($menuUrl == 'controlpanel/basicManage' || $menuUrl == 'controlpanel/menuManage' || $menuUrl == 'controlpanel/regionManage' 
					|| $menuUrl == 'controlpanel/newsAndAdvertiseManage' || $menuUrl == 'controlpanel/menuManage/subMenuManage' || $menuUrl == 'controlpanel/menuManage/deeperSubManage' || $menuUrl == 'controlpanel/regionManage/countryManage'
					|| $menuUrl == 'controlpanel/regionManage/cityManage' || $menuUrl == 'controlpanel/registerMember/memberRegistration' || $menuUrl == 'controlpanel/newsAndAdvertiseManage/addManage' || $menuUrl == 'controlpanel/newsAndAdvertiseManage/advertisement' || 
					$menuUrl == 'controlpanel/newsAndAdvertiseManage/careerConsultant' || $menuUrl == 'controlpanel/newsAndAdvertiseManage/programeManage') echo 'active' ?>">
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa fa-desktop"></i>
                            <span class="menu-text">
                              Web Site Manage
                            </span>
                    
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                    
                        <b class="arrow"></b>
                    
                        <ul class="submenu">
							<li class="<?php if($menuUrl == 'controlpanel/basicManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/basicManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
										Basic Manage
								</a>

								<b class="arrow"></b>
							</li>
                            
                            <li class="<?php if($menuUrl == 'controlpanel/menuManage' || $menuUrl == 'controlpanel/menuManage/subMenuManage' || $menuUrl == 'controlpanel/menuManage/deeperSubManage') echo 'active' ?>">
								<a href="#" class="dropdown-toggle">
									<i class="menu-icon fa fa-caret-right"></i>

									Menu Manage
									<b class="arrow fa fa-angle-down"></b>
								</a>

								<b class="arrow"></b>

								<ul class="submenu">
									<li class="<?php if($menuUrl == 'controlpanel/menuManage') echo 'active' ?>">
										<a href="<?php echo site_url('controlpanel/menuManage'); ?>">
											<i class="menu-icon fa fa-caret-right"></i>
											Menu Manage
										</a>

										<b class="arrow"></b>
									</li>

									<li class="<?php if($menuUrl == 'controlpanel/menuManage/subMenuManage') echo 'active' ?>">
										<a href="<?php echo site_url('controlpanel/menuManage/subMenuManage'); ?>">
											<i class="menu-icon fa fa-caret-right"></i>
											Sub Menu Manage
										</a>

										<b class="arrow"></b>
									</li>

									<li class="<?php if($menuUrl == 'controlpanel/menuManage/deeperSubManage') echo 'active' ?>">
										<a href="<?php echo site_url('controlpanel/menuManage/deeperSubManage'); ?>">
											<i class="menu-icon fa fa-caret-right"></i>
											Deeper Sub Menu Manage
										</a>

										<b class="arrow"></b>
									</li>
								</ul>
							</li>

							<li class="<?php if($menuUrl == 'controlpanel/regionManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/regionManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									   Region Manage 
								</a>

								<b class="arrow"></b>
							</li>

							<li class="<?php if($menuUrl == 'controlpanel/regionManage/countryManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/regionManage/countryManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
										Country Manage
								</a>

								<b class="arrow"></b>
							</li>

							<li class="<?php if($menuUrl == 'controlpanel/regionManage/cityManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/regionManage/cityManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
										City Manage
								</a>

								<b class="arrow"></b>
							</li>

							<li class="<?php if($menuUrl == 'controlpanel/registerMember/memberRegistration') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/memberRegistration'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
										User Create 
								</a>

								<b class="arrow"></b>
							</li>
							<li class="<?php if($menuUrl == 'controlpanel/newsAndAdvertiseManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/newsAndAdvertiseManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									News And Event Manage
								</a>


								<b class="arrow"></b>
							</li>
							<li class="<?php if($menuUrl == 'controlpanel/newsAndAdvertiseManage/addManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/newsAndAdvertiseManage/addManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
										Promotion Manage
								</a>
							</li>
							
							<li class="<?php if($menuUrl == 'controlpanel/newsAndAdvertiseManage/careerConsultant') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/newsAndAdvertiseManage/careerConsultant'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
										Career Consultant Management
								</a>
							</li>
							
							<li class="<?php if($menuUrl == 'controlpanel/newsAndAdvertiseManage/programeManage') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/newsAndAdvertiseManage/programeManage'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Higher Education Programe Manage
								</a>
							</li>
						</ul>
                    </li>

					
					

					<li class="<?php if($menuUrl == 'controlpanel/registerMember' || $menuUrl == 'controlpanel/registerMember/generalRegister') echo 'active' ?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-registered"></i>
							<span class="menu-text"> Registration View </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="<?php if($menuUrl == 'controlpanel/registerMember') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									All Organization User Registration Details
								</a>

								<b class="arrow"></b>
							</li>
                            
                            <li class="<?php if($menuUrl == 'controlpanel/registerMember/generalRegister') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/generalRegister'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									All Other User Registration Details
								</a>

								<b class="arrow"></b>
							</li>

						</ul>
					</li>
					<li class="<?php if($menuUrl == 'controlpanel/registerMember/allUserMailSend' || $menuUrl == 'controlpanel/registerMember/generalUserMailSend' || $menuUrl == 'controlpanel/registerMember/organizationUserMailSend') echo 'active' ?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon glyphicon glyphicon-envelope"></i>
							<span class="menu-text"> Mail Send </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="<?php if($menuUrl == 'controlpanel/registerMember/allUserMailSend') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/allUserMailSend'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Send Mail All User 
								</a>

								<b class="arrow"></b>
							</li>
							<li class="<?php if($menuUrl == 'controlpanel/registerMember/generalUserMailSend') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/generalUserMailSend'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Other User Wise  
								</a>

								<b class="arrow"></b>
							</li>
							<li class="<?php if($menuUrl == 'controlpanel/registerMember/organizationUserMailSend') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/organizationUserMailSend'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Organization User Wise  
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					
					<li class="<?php if($menuUrl == 'controlpanel/registerMember/allSmsSend' || $menuUrl == 'controlpanel/registerMember/smsSendGeneralUserWise' || $menuUrl == 'controlpanel/registerMember/smsSendOrganizationUserWise') echo 'active' ?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> SMS Send </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="<?php if($menuUrl == 'controlpanel/registerMember/allSmsSend') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/allSmsSend'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									 All User Wise
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
                        <ul class="submenu">
							<li class="<?php if($menuUrl == 'controlpanel/registerMember/smsSendGeneralUserWise') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/smsSendGeneralUserWise'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Other User Wise 
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
                        
                        <ul class="submenu">
							<li class="<?php if($menuUrl == 'controlpanel/registerMember/smsSendOrganizationUserWise') echo 'active' ?>">
								<a href="<?php echo site_url('controlpanel/registerMember/smsSendOrganizationUserWise'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Organization User Wise 
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
                        
					</li>
					
					
					
					<li class="<?php if($menuUrl == 'controlpanel/newsAndAdvertiseManage/approvedAd') echo 'active' ?>">
						<a href="<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedAd'); ?>">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> Promotion Ad </span>
						</a>
					</li>
					
					
					<li class="">
						<a href="<?php echo site_url('controlpanel/login/logout'); ?>">
							<i class="menu-icon glyphicon glyphicon-off"></i>
							<span class="menu-text">Log Out </span>
						</a>
					</li>

				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>