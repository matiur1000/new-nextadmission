<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Mail Send </a>
							</li>
							<li class="active">General User Wise</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  Send Mail  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
						<form id="addForm" action="<?php echo site_url('controlpanel/registerMember/generalUserMailAction2'); ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" id="id" value="" />
	                                <div id="modal-form" class="modal" tabindex="-1">
	                                    <div class="modal-dialog">
	                                        <div class="modal-content">
	                                            <div class="modal-header">
	                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                                <h4 class="blue bigger">General User Mail Send</h4>
	                                            </div>

	                                            <div class="modal-body">
	                                                <div class="row"> 
	                                                    <div class="col-xs-12 col-sm-11">
	                                                        <div class="form-group">
	                                                            <label for="user_type">Select General User</label>  
																<div>
	                                                               <select class="form-control" id="user_type" name="user_type"  tabindex="1" required >
	                                                                    <option value="" selected>Select User</option>
	                                                                    <?php 
	                                                                        foreach ($mailList as $v) {
	                                                                        
	                                                                    ?>
	                                                                    <option value="<?php echo $v->user_id; ?>"><?php echo $v->name; ?></option>
	                                                                    <?php } ?>
	                                                               </select>
	                                                            </div>      
	                                                            
	                                                        </div>

	                                                        <div class="space-4"></div>
	                                                        <div class="form-group">
	                                                            <label for="amount">Tittle</label>

	                                                           <div>
	                                                               <input type="text" id="tittle" placeholder="Tittle" name="tittle"
                                                                            tabindex="2" class="form-control" required /> 
																	
	                                                            </div>
	                                                        </div>

	                                                        <div class="form-group">
	                                                            <label for="amount">Message</label>

	                                                           <div>
	                                                               <textarea class="form-control" rows="10" cols="1" placeholder="*..Message" 
																   tabindex="8" name="message" id="message"></textarea>
																	
	                                                            </div>
	                                                        </div>        
	                                                    </div>
	                                                </div>
	                                            </div>

	                                            <div class="modal-footer">
	                                                <button class="btn btn-sm" data-dismiss="modal">
	                                                    <i class="ace-icon fa fa-times"></i>
	                                                    Cancel
	                                                </button>

	                                                <button class="btn btn-sm btn-primary" type="submit">
	                                                    <i class="ace-icon fa fa-check"></i>
	                                                    Save
	                                                </button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </form>
						
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
	</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	<script>
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/adminManage/paymentEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#premium_user_type').val(data.premium_user_type);
					$('#amount').val(data.amount);
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/adminManage/paymentDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>
	</body>
</html>
