<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OrganizationUserHome extends CI_Controller {
	static $model 	= array('M_basic_manage','M_advertisement_manage','M_region_manage','M_country_manage','M_city_manage','M_more_campus_details',
	'M_all_user_registration','M_interested_job_post_manage','M_user_post_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_payment_gateaway','M_payment_by_user','M_organization_user_registration',
	'M_general_user_registration','M_programe_manage','M_job_post_manage','M_job_application_details','M_add_manage','M_payment_manage','M_organize_profile_manage','M_add_booking','M_search_table','M_visitor_count','M_news_and_event_manage');
	static $helper   = array('url','userauthentication','download', 'img_resize_helper');
	
	const  Title	 = 'Next Admission';
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model(self::$model);
		$this->load->helper(self::$helper);
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->library('upload');
		//$this->load->library('image_lib');
		$this->load->library( array('image_lib') );
		$this->load->library('session');
		$this->load->library('email');
		isAuthenticate();
	}

	public function index()
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['page_title']  		= self::Title;
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			$data['totalApplication']   = $this->M_job_application_details->countApplicationJob($userAutoId);
			$data['totalViewJob']       = $this->M_job_application_details->countViewJob($userAutoId);
			$data['totalShortViewJob']  = $this->M_job_application_details->countShortViewJob($userAutoId);
			$data['totalNotViewJob']   	= $this->M_job_application_details->countNotViewJob($userAutoId);	
			$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
			$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
			$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);
			$data['OrganizePostInfo']	= $this->M_job_post_manage->findAllDetailes($userAutoId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$data['draftPostInfo']	    = $this->M_job_post_manage->findAllDraft($userAutoId);
			$data['archivePostInfo']	= $this->M_job_post_manage->findAllArchived($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

			
			$updateText       		    = $this->session->userdata('updateText');
			if(!empty($updateText)){     
			  $data['updateText']       = $updateText;
			}
			 $this->session->set_userdata(array('updateText' => ""));
			 
			$this->load->view('organizationUserPanel/organizationUserHomePage', $data);
		
		}
	}
	   






	public function org_logo(){

		$config['upload_path'] 		= './Images/org_logo/';
		$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
		$config['max_size']			= '0';
		$config['max_width']  		= '0';
		$config['max_height']  		= '0';
		$config['file_name']  		= time();
		$file_field					= 'ogr_logo';
	
		$this->load->library('upload', $config);
		if ($this->upload->do_upload($file_field))
		{
			/*$image_data 			= $this->upload->data();				
			$data['image']  		= $image_data['file_name'];
			
			$source_image = 'Images/org_logo/'.$data['image'];
			$model					= $this->M_user_post_manage->findById($id);
			$only_image    			= $model->image;
			
			if(!empty($only_image)){
				unlink('./Images/org_logo/'.$only_image);
			}*/
		}
	}


	public function all_org_user($id){
		$userDetails   		          = $this->M_all_user_registration->findById($id);
		$userId                       = $userDetails->user_id;
		$userType                     = $userDetails->user_type;
		
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['page_title']  		= self::Title;
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			$data['totalApplication']   = $this->M_job_application_details->countApplicationJob($userAutoId);
			$data['totalViewJob']       = $this->M_job_application_details->countViewJob($userAutoId);
			$data['totalShortViewJob']  = $this->M_job_application_details->countShortViewJob($userAutoId);
			$data['totalNotViewJob']   	= $this->M_job_application_details->countNotViewJob($userAutoId);	
			$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
			$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
			$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);
			$data['OrganizePostInfo']	= $this->M_job_post_manage->findAllDetailes($userAutoId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$data['draftPostInfo']	    = $this->M_job_post_manage->findAllDraft($userAutoId);
			$data['archivePostInfo']	= $this->M_job_post_manage->findAllArchived($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

			
			$updateText       		    = $this->session->userdata('updateText');
			if(!empty($updateText)){     
			  $data['updateText']       = $updateText;
			}
			 $this->session->set_userdata(array('updateText' => ""));
			 
			$this->load->view('organizationUserPanel/organizationUserHomePage', $data);
					
	}

	
	public function jobWiseDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['jobId']     			= $jobId;
			$data['page_title']  		= self::Title;	
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			$data['instituteSub']	    = $this->M_general_user_registration->findByAllInstitute();
			$data['subName']	    	= $this->M_general_user_registration->findByAllSubject();
			$data['jobTitle']			= $this->M_job_post_manage->findById($jobId);
			$data['totalApp']			= $this->M_job_application_details->countJobWiseApplication($jobId);
			$data['totalViewList']		= $this->M_job_application_details->countJobWiseView($jobId);
			$data['totalNotViewApp']    = $this->M_job_application_details->countJobWiseNotView($jobId);
			$data['totalShortList']		= $this->M_job_application_details->countJobWiseShortList($jobId);
			$data['totalStarCandidets']	= $this->M_job_application_details->countJobWiseStarCandidates($jobId);
			$data['jobWiseApplication']	= $this->M_job_application_details->findByJobId($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/jobWiseDetailsPage', $data);
		
		}
	}
	
	public function pdfResume($id)
		{
			$data['id']     				= $id;
			$model   						= $this->M_all_user_registration->findById($id);
			$data['resumeInfo'] 			= $model;
			$data['moreResumeInfo'] 		= $this->M_general_user_registration->findByUserName($id);
	
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
				$data['page_title']  		= self::Title;
				$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
				$data['regionInfo']			= $this->M_region_manage->findAll();
				$data['countryInfo']		= $this->M_country_manage->findAll();
				$data['cityInfo']			= $this->M_city_manage->findAll();
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
	
				$this->load->view('generalUserPanel/pdfResumePage', $data);
			}
		}
	
	public function cvViewDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['page_title']  		= self::Title;	
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			$data['cvViewApplicant']	= $this->M_job_application_details->findByCvView($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/cvViewDetailsPage', $data);
		
		}
	}
	
	   
	   
	public function cvNotViewDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['page_title']  		= self::Title;	
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			$data['cvNotViewApplicant']	= $this->M_job_application_details->findByCvNotView($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/cvNotViewDetailsPage', $data);
		
		}
	}
	  
	public function shortListDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['page_title']  		= self::Title;	
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			$data['shortListApplicant']	= $this->M_job_application_details->findByshortList($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/shortListDetailsPage', $data);
		
		}
	}
	
	public function starCandidateDetails($jobId)
	{   
	    if(isAuthenticate()){
			$user_id             		= getUserName();
			$data['user_id']     		= $user_id;
			$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			$data['userDetails']   		= $userDetails;
			$data['activeUser']   		= $userDetails->name;
			$userAutoId   		        = $userDetails->id;
			$data['userAutoId']     	= $userAutoId;
			$data['page_title']  		= self::Title;
			$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');	
			$data['starCanApplicant']	= $this->M_job_application_details->findBystarCan($jobId);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			$this->load->view('organizationUserPanel/starCandidateDetailsPage', $data);
		
		}
	}
	   public function changPassword($id)
	   {
		   $model   					= $this->M_all_user_registration->findById($id);
		   $data['profileEditInfo'] 	= $model;
		  
		   
		if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
		   $data['userDetails']   		= $userDetails;
		   $data['activeUser']   		= $userDetails->name;
		   $userAutoId   		        = $userDetails->id;
		   $data['userAutoId']     		= $userAutoId;
		   $data['page_title']  		= self::Title;
		   $data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      	=  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('organizationUserPanel/changPasswordPage', $data);
		 }
	   }
	   
	    public function updatePass()
			{
				$id						= $this->input->post('id');
				$oldPass				= $this->input->post('password');
				$newPassword			= $this->input->post('newPassword');
				$conPassword			= $this->input->post('conpass');
				$data['password'] 		= $this->input->post('newPassword');
				$userDetails   		    = $this->M_all_user_registration->findById($id);
				$dbPass                = $userDetails->password;
				 if($newPassword == $conPassword){
					if($oldPass == $dbPass){
						$this->M_all_user_registration->update($data, $id);	
						redirect('organizationUserHome/changSuccessPassword');	
					}else{
					 $this->load->view('organizationUserPanel/changPasswordAlertPage', $data);
					}
				}else{
				   $this->load->view('organizationUserPanel/changPasswordAlert2Page', $data);
				}
			 }
			 
	   
	    public function changSuccessPassword()
	   {
		  
		if(isAuthenticate()){
		   $user_id             		= getUserName();
		   $data['user_id']     		= $user_id;
		   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
		   $data['userDetails']   		= $userDetails;
		   $data['activeUser']   		= $userDetails->name;
		   $userAutoId   		        = $userDetails->id;
		   $data['userAutoId']     		= $userAutoId;
		   $data['page_title']  		= self::Title;
		   $data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
		   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
		   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
		   $where                      	=  array('position_top' => 'Yes','status' => 'Available');
		   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
		   $data['menuInfo']			= $menuInfo;
		   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
		   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
		   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
		   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
		   
		   $this->load->view('organizationUserPanel/changSuccessPasswordPage', $data);
		 }
	   }
	  
	  
	public function shortListAction()
	{ 		
		$applicantList = $this->input->post('applicant_id');
		
		foreach($applicantList as $applicantId) {
			if($applicantId){
				$data2['shortlist_status']	= "yes";
				$data2['short_list_date']	= date("d/m/Y");
				$this->M_job_application_details->update($data2, $applicantId);
			}
		}
		//redirect('organizationUserHome'); 
	}
	
	public function starCandidateAction()
	{ 		
		$applicantList = $this->input->post('applicant_id');
		
		foreach($applicantList as $applicantId) {
			if($applicantId){
				$data2['star_candidate_status']	= "yes";
				$this->M_job_application_details->update($data2, $applicantId);
			}
		}
		//redirect('organizationUserHome'); 
	}
	
	
	 public function interestedJobPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$data['activeUser']   		= $userDetails->name;
				$userId                     = $userDetails->id;
				$data['userAutoId']     	= $userId;
				$data['page_title']  		= self::Title;	
				$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('organizationUserPanel/interestedJobPostPage', $data);
			
			}
		}
		
		public function wantAdd()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$data['activeUser']   		= $userDetails->name;
				$userId                     = $userDetails->id;
				$data['userAutoId']     	= $userId;
				$data['page_title']  		= self::Title;	
				$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$data['userType']	        = $this->M_payment_gateaway->findAll();
				$data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$this->load->view('organizationUserPanel/wantAddPage', $data);
			
			}
		}
		
		 public function wantAddStore()
			{	
			 if(isAuthenticate()){
				$user_id             				= getUserName();
				$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
				$user_autoId                        = $userDetails->id;
				$data['activeUser']   				= $userDetails->name;
				$data['user_id']                    = $userDetails->id;
				
				$data['premium_user_type'] 			= $this->input->post('premium_user_type');
				$data['amount'] 	    			= $this->input->post('amount');
				$data['payment_by'] 				= $this->input->post('payment_by');
				
				$data1['premium_status'] 			= '1';
				
				$this->M_all_user_registration->updatStatus($data1, $user_autoId);
				$this->M_payment_by_user->save($data);
				redirect('premiumUserHome/wantAdd'); 
			  }
			  
			}
		
		
		
		public function addPost()
		{   
			if(isAuthenticate()){
				$user_id             		= getUserName();
				$data['user_id']     		= $user_id;
				$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
				$data['userDetails']   		= $userDetails;
				$data['activeUser']   		= $userDetails->name;
				$userId                     = $userDetails->id;
				$data['userAutoId']     	= $userId;
				$data['page_title']  		= self::Title;
			    $data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
				$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
				$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
				$where                      =  array('position_top' => 'Yes','status' => 'Available');
				$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
				$data['menuInfo']			= $menuInfo;
				$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
				$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
				$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
				$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
				$data['UserWisePostInfo']	= $this->M_user_post_manage->findByUser($userId);
				$this->load->view('organizationUserPanel/addPostPage', $data);
			
			}
		}
		
		
		 public function postEdit($id)
		   {
			   $data['postEditInfo']  		= $this->M_user_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['userAutoId']     		= $userId;
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWisePostInfo']	= $this->M_user_post_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/postEditPage', $data);
			 }
		   }
		   
		   public function programeManage()
			{   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['activeUser']   		= $userDetails->name;
					$data['userDetails']   		= $userDetails;
					$userId                     = $userDetails->id;
					$data['userAutoId']     	= $userId;
					$data['page_title']  		= self::Title;
					$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
					$this->load->view('organizationUserPanel/programeManagePage', $data);
				
				}
			}


			public function programCsvStore()
			{
			   if(isAuthenticate()){
				$user_id             				= getUserName();
				$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
				

			  	$config['upload_path'] 		= './resource/csvUpload/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= 'programeData';
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('programCsv')){
					$uploadData = $this->upload->data();
				}
		
				// open the csv file
				$fp = fopen(base_url('resource/csvUpload/programeData.csv'),"r");

				//parse the csv file row by row
				$i = 0;
				while(($row = fgetcsv($fp,"500",",")) != FALSE)
				{
					
				      $data2['programe_name']    = $row[0];
				      //echo "<br>";
					  $data2['programe_details'] = $row[1];
					  //echo "<br>";
					  $data2['orgnization_id']   	  = $userDetails->id;
					
	                    if($i>0){
						  if($data2['programe_name'] != ''){
						  	$programe_id = $this->M_programe_manage->save($data2);

							$progDetails 					   	= $this->M_programe_manage->findById($programe_id);

							$datas['programe_id'] 				= $programe_id;
							$datas['organize_programe_id']      = $progDetails->orgnization_id;
							$datas['program_name'] 			   	= $progDetails->programe_name;
							$datas['program_details'] 			= $progDetails->programe_details;
						     $this->M_search_table->save($datas);
						  }
						}
						
					      $this->session->set_userdata(array('suceess' => "Csv file uploade successfully"));
						$i++;
					 

				}
		          fclose($fp);
				  unlink('./resource/csvUpload/reggistrationData.csv');
				  
				  redirect('organizationUserHome/programeManage');
			  }
			}

			
			
		
		public function programStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['orgnization_id']     		= $userDetails->id;
			$data['programe_name'] 				= $this->input->post('programe_name');
			$data['course_duration'] 			= $this->input->post('course_duration');
			$data['programe_details'] 			= $this->input->post('programe_details');
			
			
			$programe_id 						= $this->M_programe_manage->save($data);
		    $progDetails 					   	= $this->M_programe_manage->findById($programe_id);

			$datas['programe_id'] 				= $programe_id;
			$datas['organize_programe_id']      = $progDetails->orgnization_id;
			$datas['program_name'] 			   	= $progDetails->programe_name;
			$datas['program_details'] 			= $progDetails->programe_details;

			$this->M_search_table->save($datas);

			redirect('organizationUserHome/programeManage');
		  }
		  
		}
		
		
		 public function programeEdit($id)
		   {
			   $data['progEditInfo']  		= $this->M_programe_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['userAutoId']     		= $userId;
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']      	= $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/programeEditPage', $data);
			 }
		   }
		   
		    public function jobPostEdit($id)
		   {
			   $data['jobPostEditInfo']  		= $this->M_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/jobPostEditPage', $data);
			 }
			 
			 
			 
		   }


		   public function downloadCsvFile($type) {
	   	    $fileDetails   = $this->M_organize_profile_manage->findFileByType('csv_file_upload', $type);
			$data = file_get_contents("./resource/csvUpload/$fileDetails->csv_file");
		   	  if($type == 'contact'){
				$name = 'contact.csv';
			  } else if($type == 'organizeType') {
				$name = 'organizeType.csv';
			  } else {
				$name = 'programe.csv';
			  }
			  force_download($name, $data);
		   }
		   
		   public function jobPostDraft($id)
		   {
			  
			   $data['draft_job_status']    = "yes";
			     $this->M_job_post_manage->update($data, $id);		
				redirect('organizationUserHome');
			
		   }
		   
		   public function jobPostArchived($id)
		   {
			  
			   $data['archive_status']    = "yes";
			     $this->M_job_post_manage->update($data, $id);		
				redirect('organizationUserHome');
			
		   }
		   
		   public function jobPostDelete($id)
		   {
			
			   $this->M_job_post_manage->destroy($id);		
				redirect('organizationUserHome');
			
		   }
		   
		   public function programUpdate()
			{
				$id							= $this->input->post('id');
				$data['programe_name'] 		= $this->input->post('programe_name');
				$data['course_duration'] 	= $this->input->post('course_duration');
				$data['programe_details'] 	= $this->input->post('programe_details');

				$this->M_programe_manage->update($data, $id);		
				$progDetails 			    = $this->M_programe_manage->findById($id);

				$datas['program_name'] 	    = $progDetails->programe_name;
				$datas['program_details'] 	= $progDetails->programe_details;

			    $progDetailsChk 			= $this->M_search_table->findByProgId($id);

			    if(!empty($progDetailsChk)){
			      $this->M_search_table->updateProg($datas, $id);
			    } else {
				  $datas['programe_id'] 	      = $id;
				  $datas['organize_programe_id']  = $progDetails->orgnization_id;

			      $this->M_search_table->save($datas);	
			    }
				redirect('organizationUserHome/programeManage');
			 }
			 
			 
			 public function jobPostUpdateAction()
			{
				$id									= $this->input->post('id');
				$data['add_type'] 				    = $this->input->post('add_type');
				$data['job_category'] 			    = $this->input->post('job_category');
				$data['organization_type'] 			= $this->input->post('organization_type');
				$data['title'] 			            = $this->input->post('title');
				$data['vacancies'] 			        = $this->input->post('vacancies');
				$receive_online			            = $this->input->post('receive_online');
				$receive_email 			            = $this->input->post('receive_email');
				$receive_hardcopy 		        	= $this->input->post('receive_hardcopy');
				if(!empty($receive_online)){
				  $data['receive_online'] 			= $receive_online;
				}else if(!empty($receive_email)){
				  $data['receive_online'] 			= $receive_email;
				}else{
				  $data['receive_online'] 			= $receive_hardcopy;
				}
				$data['cv_enclose'] 			    = $this->input->post('cv_enclose');
				$data['apllication_deadline'] 		= $this->input->post('apllication_deadline');
				$data['contact_person'] 			= $this->input->post('contact_person');
				$data['desplay_orgname'] 			= $this->input->post('desplay_orgname');
				$data['desplay_address'] 			= $this->input->post('desplay_address');
				$data['age_range_from'] 			= $this->input->post('age_range_from');
				$data['age_range_to'] 			    = $this->input->post('age_range_to');
				$data['gender'] 			        = $this->input->post('gender');
				$data['job_type'] 			        = $this->input->post('job_type');
				$data['job_level'] 			        = $this->input->post('job_level');
				$data['education_qualification'] 	= $this->input->post('education_qualification');
				$data['job_description'] 			= $this->input->post('job_description');
				$data['job_requirements'] 			= $this->input->post('job_requirements');
				$data['job_experience'] 			= $this->input->post('job_experience');
				$data['exprience_min'] 			    = $this->input->post('exprience_min');
				$data['exprience_max'] 			    = $this->input->post('exprience_max');
				$data['sallary_range'] 			    = $this->input->post('sallary_range');
				$data['sallary_money_min'] 			= $this->input->post('sallary_money_min');
				$data['sallary_money_max'] 			= $this->input->post('sallary_money_max');
				$data['job_publish_status'] 		= $this->input->post('job_publish_status');
				
				
	
				$this->M_job_post_manage->update($data, $id);		
				redirect('organizationUserHome');
				
			 }
			 
			 
		 public function viewJob($id)
		   {
			   $data['jobPostEditInfo']  		= $this->M_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['userAutoId']     		= $userId;
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']       = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/viewJobPage', $data);
			 }
			 
		   }
		
		  public function applyCvView($applyUserId, $postId)
		   {
			   $model   					= $this->M_all_user_registration->findById($applyUserId);
			   $data['resumeInfo'] 			= $model;
			   $data['moreResumeInfo'] 		= $this->M_general_user_registration->findByUserName($applyUserId);
			   
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $data['activeUser']   		= $userDetails->name;
			   $userId                      = $userDetails->id;
			   $data['userAutoId']     		= $userId;
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']       = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['applyUserId']  		= $applyUserId;
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
			   
			   $data2['application_view_status']  		= "view";
			   $data2['cv_view_date']  					= date("d/m/Y");
			   $this->M_job_application_details->update2($data2, $applyUserId, $postId);	
			   
			   $this->load->view('organizationUserPanel/applyCvViewPage', $data);
			 }
			 
		   }
		
		   
		   
		    public function postUpdate()
			{
				$id						= $this->input->post('id');
				$data['title'] 			= $this->input->post('title');
				$data['image_title'] 	= $this->input->post('image_title');
				$data['date_and_time'] 	= $this->input->post('date_and_time');
				$data['description'] 	= $this->input->post('description');
			
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['max_size']			= '0';
				$config['max_width']  		= '0';
				$config['max_height']  		= '0';
				$config['file_name']  		= time();
				$file_field					= 'image';
			
				/// IMAGE UPLOAD ACTION
				$this->upload->initialize($config);
				if ($this->upload->do_upload($file_field))
				{
					/// ERORR SHOW FOR IMAGE UPLOAD
				
					$image_data 			= $this->upload->data();				
					$data['image']  		= $image_data['file_name'];
					
					$source_image = 'Images/Post_image/'.$data['image'];
					$destination  = 'Images/Post_image500_347/'.$data['image'];
					$frame_width  = 500;
					$frame_height = 347;
					resize_with_white_space($source_image, $destination, $frame_width, $frame_height);
					
					$model					= $this->M_user_post_manage->findById($id);
					$only_image    			= $model->image;
					if(!empty($only_image)){
					unlink('./Images/Post_image/'.$only_image);
					unlink('./Images/Post_image500_347/'.$only_image);
					}
				}


	
			    $this->M_user_post_manage->update($data, $id);	
			    $blogDetails 			    = $this->M_user_post_manage->findById($id);
			    $userDetails                = $this->M_all_user_registration->findById($blogDetails->user_id);



				$datas['blog_title'] 	    = $blogDetails->title;

			    $blogDetailsChk 			= $this->M_search_table->findByBlogId($id);

			    if(!empty($blogDetailsChk)){
			       $this->M_search_table->updateBlog($datas, $id);
			    } else{
				  $datas['blog_id'] 	    = $id;
				  $datas['blog_user_id']    = $blogDetails->user_id;
				  $datas['region_id'] 	    = $userDetails->region_id;
				  $datas['country_id']      = $userDetails->country_id;
				  $datas['city_id'] 	    = $userDetails->city_id;

			      $this->M_search_table->save($datas);	
			    }

				redirect('organizationUserHome/addPost');
				
			 }
		   
		
		public function orgAddPostStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$regionId                         	= $userDetails->region_id;
		  	$regionName   		        		= $this->M_region_manage->findById($regionId);
		  	$countryDetails   		            = $this->M_country_manage->findById($userDetails->country_id);
		  	$data['country_name'] 				= $countryDetails->country_name;
		  	$data['region'] 					= $regionName->region_name;
			$data['user_id']     				= $userDetails->id;
			$data['title'] 						= $this->input->post('title');
			$data['status'] 	    			= "aprove";
			$data['image_title'] 				= $this->input->post('image_title');
			$data['date_and_time'] 				= $this->input->post('date_and_time');
			$data['description'] 				= $this->input->post('description');

			
			
				
				$config['upload_path'] 		= './Images/Post_image/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				$file_field					= 'image';
				
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')) {
				$uploadData = $this->upload->data();
				$data['image'] = $uploadData['file_name'];
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadData['full_path'];
				$config['maintain_ratio'] = TRUE;
				$config['new_image'] = './Images/Post_image/';
				
				
				$source_image = 'Images/Post_image/'.$data['image'];
				$destination  = 'Images/Post_image500_347/'.$data['image'];
				$frame_width  = 500;
				$frame_height = 347;
				resize_with_white_space($source_image, $destination, $frame_width, $frame_height);
				
				}
				
			    $blog_id = $this->M_user_post_manage->save($data);
				
			    $blogDetails 					   = $this->M_user_post_manage->findById($blog_id);
			    $userDetails                       = $this->M_all_user_registration->findById($blogDetails->user_id);


				$datas['region_id'] 			   = $userDetails->region_id;
				$datas['country_id']     		   = $userDetails->country_id;
				$datas['city_id'] 			   	   = $userDetails->city_id;
				$datas['blog_id'] 				   = $blog_id;
				$datas['blog_user_id']     		   = $blogDetails->user_id;
				$datas['blog_title'] 			   = $blogDetails->title;

				$this->M_search_table->save($datas);


			   redirect('organizationUserHome/addPost');

		  }
	  }
	  
	  
	  public function intrestEdit($id)
		   {
			   $data['intrestEditInfo']  	= $this->M_interested_job_post_manage->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
			   $data['userDetails']   		= $userDetails;
			   $userId                      = $userDetails->id;
			   $data['userAutoId']     		= $userId;
			   $data['activeUser']   		= $userDetails->name;
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']       = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();
			   $data['UserWiseIntJobInfo']	= $this->M_interested_job_post_manage->findByUser($userId);
			   
			   $this->load->view('organizationUserPanel/interestJobEditPage', $data);
			 }
		   }
		   
		   
		   public function interestJobUpdate()
			{
				$id										= $this->input->post('id');
				$data['education_provider_type'] 		= $this->input->post('education_provider_type');
				$data['interested_program'] 			= $this->input->post('interested_program');
				$data['location'] 						= $this->input->post('location');
				$data['other'] 							= $this->input->post('other');
	
				$this->M_interested_job_post_manage->update($data, $id);		
				redirect('organizationUserHome/interestedJobPost');
				
			 }
	   
	  
	
		 public function profileUpdate($id)
		   {
			   $model   					= $this->M_all_user_registration->findById($id);
			   $data['activeUser']   		= $model->name;
			   $data['profileEditInfo'] 	= $model;
			   $data['moreEditInfo']   		= $this->M_organization_user_registration->findByUserName($id);
			   $data['anyMoreEditInfo']   	= $this->M_more_campus_details->findById($id);
			   
			   if(isAuthenticate()){
			   $user_id             		= getUserName();
			   $data['user_id']     		= $user_id;
			   $data['allYearInfo']   		= $this->M_organize_profile_manage->findAllYear('year_manage');
			   $data['userDetails']   		= $this->M_all_user_registration->findByUserName($user_id);
			   $data['page_title']  		= self::Title;
			   $data['panelMenuInfo']       = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
			   $data['regionInfo']			= $this->M_region_manage->findAll();
			   $data['countryInfo']			= $this->M_country_manage->findAllCountry();	
			   $data['cityInfo']			= $this->M_city_manage->findAll();	
			   $data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			   $data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();
			   $where                       =  array('position_top' => 'Yes','status' => 'Available');
			   $menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			   $data['menuInfo']			= $menuInfo;
			   $data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			   $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			   $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			   $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

			   $data['suceess']        = $this->session->userdata('suceess');
		       $this->session->set_userdata(array('suceess' => ""));

			   $this->load->view('organizationUserPanel/profileUpdatePage', $data);
			 }
		   }
	   
	   public function interestJobStore()
		{	
		 if(isAuthenticate()){
			$user_id             				= getUserName();
			$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
			$data['user_id']     				= $userDetails->id;
			$data['education_provider_type'] 	= $this->input->post('education_provider_type');
			$data['interested_program'] 	    = $this->input->post('interested_program');
			$data['location'] 					= $this->input->post('location');
			$data['other'] 						= $this->input->post('other');
			
			$this->M_interested_job_post_manage->save($data);
			redirect('organizationUserHome/interestedJobPost');
		  }
		  
		}
	  
	  
	  public function countryEdit()
		{
			$region_id 	   = $this->input->post('id');
			$countryList   = $this->M_country_manage->findAllInfo($region_id);				
		
			echo '<option value="">Sellect Country Name</option>';
			foreach($countryList as $v) {
				echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';
			}
			
		}
		
		
		 public function cityEdit()
			{
				$country_id 	= $this->input->post('id');
				$cityList   	= $this->M_city_manage->findAllCity($country_id);				
			
				echo '<option value="">Sellect City Name</option>';
				foreach($cityList as $v) {
					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';
				}
				
			}
			
			 public function viewAmount()
				{
					$premium_user_type 	   = $this->input->post('id');
					
					//$where 			   = array('region_id' => $region_id);		
					$amount   	   		   = $this->M_payment_gateaway->findById($premium_user_type);				
				
					echo $amount->amount;
				}
			
			
			
			
			public function update()
			{
				$id							= $this->input->post('id');
				$datas['user_id']           = $id;

				$config['upload_path'] 		= './resource/csvUpload/';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= time();
				
				$this->upload->initialize($config);
				if($this->upload->do_upload('contactCsv')){
					$uploadData = $this->upload->data();
				    $datas['contactCsv'] = $uploadData['file_name'];
				
                
               
                  $this->M_organize_profile_manage->save('csv', $datas);	

                  $viewCsvData      =  $this->M_organize_profile_manage->findCsvDtaById('csv', $id);
                
		            // open the csv file
					$fp = fopen(base_url("resource/csvUpload/$viewCsvData->contactCsv"),"r");

					//parse the csv file row by row
					$i = 0;
					while(($row = fgetcsv($fp,"500",",")) != FALSE)
					{
						
					      if($i == 2 || $i == 4){

					      	if($i == 2){

						       $namecsv       = $row[0];
						       if(!empty($namecsv)){
						         $data['name'] 	    		= $namecsv;
						       } else {
						         $data['name'] 	    		= $this->input->post('organizationname');

						       }
							   $account_namecsv    = $row[1];
							   if(!empty($account_namecsv)){
							   	$data['account_name'] 	    = $account_namecsv;	
							   }else{
							   	 $data['account_name'] 	    = $this->input->post('account_name');	
							   }
							   $country_idcsv = $row[2];
							   if(!empty($country_idcsv)){
							   	$data['country_id'] 	    = $country_idcsv;	
							   }else{
							   	 $data['country_id'] 	    = $this->input->post('country_id');	
							   }
							   $city_idcsv = $row[3];
							   if(!empty($city_idcsv)){
							   	$data['city_id'] 	    	= $city_idcsv;	
							   }else{
							   	 $data['city_id'] 	    	= $this->input->post('city_id');	
							   }
							   $district_idcsv = $row[4];
							   if(!empty($district_idcsv)){
							   	$data['district_id'] 	    = $district_idcsv;	
							   }else{
							   	 $data['district_id'] 	    = $this->input->post('account_name');	
							   }
							  //echo"<br>";
							  $police_stationcsv = $row[5];
							  if(!empty($police_stationcsv)){
							   	$data['police_station'] 	= $police_stationcsv;	
							   }else{
							   	 $data['police_station'] 	= $this->input->post('account_name');	
							   }
							  $post_codecsv          = $row[6];
							  if(!empty($post_codecsv)){
							   	$data3['post_code'] 	    = $post_codecsv;	
							   }else{
							   	 $data3['post_code'] 	    = $this->input->post('post_code');	
							   }

							  $addresscsv = $row[7];
							  if(!empty($addresscsv)){
							   	$data3['address'] 	    	= $addresscsv;	
							   }else{
							   	$data3['address'] 	    	= $this->input->post('address');	
							   }
						   } else {
						   	   $phonecsv       = $row[0];
						   	   if(!empty($phonecsv)){
						          $data3['phone'] 			= $phonecsv;
						   	   } else {
								$data3['phone'] 			= $this->input->post('phone');
						   	   }
							   $mobilecsv             		= $row[1];

							   if(!empty($mobilecsv)){
						          $data3['mobile'] 			= $mobilecsv;
						   	   } else {
								$data3['mobile'] 			= $this->input->post('phone');
						   	   }
							   $emailcsv = $row[2];

							   if(!empty($emailcsv)){
						          $data3['email'] 		    = $emailcsv;
						   	   } else {
								$data3['email'] 			= $this->input->post('phone');
						   	   }
							   $skypecsv = $row[3];
							   if(!empty($skypecsv)){
						          $data3['skype'] 			= $skypecsv;
						   	   } else {
								$data3['skype'] 			= $this->input->post('skype');
						   	   }
							   $web_sitecsv = $row[4];
							   if(!empty($web_sitecsv)){
						          $data3['web_site'] 		= $web_sitecsv;
						   	   } else {
								$data3['web_site'] 			= $this->input->post('web_site');
						   	   }
							  $facebookcsv = $row[5];
							  if(!empty($facebookcsv)){
						          $data3['facebook'] 		= $facebookcsv;
						   	   } else {
								$data3['facebook'] 			= $this->input->post('facebook');
						   	   }
							  $tuitercsv = $row[6];
							  if(!empty($tuitercsv)){
						          $data3['tuiter'] 			= $tuitercsv;
						   	   } else {
								$data3['tuiter'] 			= $this->input->post('tuiter');
						   	   }
							  $any_more_campuscsv = $row[7];
							  if(!empty($any_more_campuscsv)){
						          $data3['any_more_campus'] = $any_more_campuscsv;
						   	   } else {
								$data3['any_more_campus'] 	= $this->input->post('any_more_campus');
						   	   }
							

						   }
							  
						}
						     
						 $i++;

					  }

					  //fclose($fp);
			         // unlink('./resource/csvUpload/'.$viewCsvData->contactCsv);
			          //$this->M_organize_profile_manage->destroy_csv('csv', $id);

					} else {           //without csv file

					  $data['name'] 	    		= $this->input->post('organizationname');
					  $data['account_name'] 	    = $this->input->post('account_name');	
					  $data['country_id'] 			= $this->input->post('country_id');
					  $data['city_id'] 				= $this->input->post('city_id');
					  $data['district_id'] 			= $this->input->post('district_id');
					  $data['police_station'] 		= $this->input->post('police_station');

					  $data3['post_code'] 			= $this->input->post('post_code');
					  $data3['address'] 			= $this->input->post('address');
					  $data3['phone'] 				= $this->input->post('phone');
					  $data3['mobile'] 				= $this->input->post('mobile');
					  $data3['email'] 				= $this->input->post('email');
					  $data3['skype'] 				= $this->input->post('skype');
					  $data3['web_site'] 			= $this->input->post('web_site');
					  $data3['facebook'] 			= $this->input->post('facebook');
					  $data3['tuiter'] 				= $this->input->post('tuiter');

                    }   //without csv file end

						
						$data['user_id'] 			= $this->input->post('user_id');
						$data['password'] 			= $this->input->post('password');
						$data['user_type'] 			= "Organization User";
						$data['status'] 			= "Active";
						
						
						$config['upload_path'] 		= './Images/Register_image/';
						$config['allowed_types'] 	= 'gif|jpg|png';
						$config['max_size']			= '0';
						$config['max_width']  		= '0';
						$config['max_height']  		= '0';
						$config['file_name']  		= time();
						$file_field					= 'image';
					
						/// IMAGE UPLOAD ACTION
						$this->upload->initialize($config);
						if ($this->upload->do_upload($file_field))
						{
							/// ERORR SHOW FOR IMAGE UPLOAD
						
							$image_data 			= $this->upload->data();				
							$data['image']  		= $image_data['file_name'];
							$model					= $this->M_all_user_registration->findById($id);
							if(!empty($model)){
							unlink('Images/Register_image/'.$model->image);
							
							}
						}
						
						$this->M_all_user_registration->update($data, $id);	
						
						
						
						$data3['phone_com'] 			= $this->input->post('phone_com2');
						$data3['mobile_com'] 			= $this->input->post('mobile_com2');
						$data3['email_com'] 			= $this->input->post('email_com');
						$data3['location_by_google'] 	= $this->input->post('location_by_google');
						$data3['founded'] 				= $this->input->post('founded');
						$data3['any_more_campus'] 		= $this->input->post('any_more_campus');
						$any_more_campus 				= $this->input->post('any_more_campus');
						$data3['designation'] 			= $this->input->post('designation');
						
						
						
						$type_of_organization 					= $this->input->post('type_of_organization');
						if($type_of_organization =='Other'){
						  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
						  $data3['type_of_organization_other'] 	= $this->input->post('type_of_organization_other');
						  $data3['type_of_education_provider'] 	= "";
						  $data3['agency_type_other'] 			= "";
						  $data3['agency_type'] 			    = "";
						  $data3['couching_centre'] 			= "";
						  $data3['professional_taining'] 		= "";
						  $data3['school_college'] 				= "";
							  
						  
						}else if($type_of_organization =='EducationProvider'){
						  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
						  $data3['type_of_education_provider'] 	= $this->input->post('type_of_education_provider');
						  $data3['type_of_organization_other'] 	= "";
						  $data3['agency_type_other'] 			= "";
						  $data3['agency_type'] 			    = "";
						  $data3['couching_centre'] 			= "";
						  $data3['professional_taining'] 		= "";
						  $type_of_education_provider 			= $this->input->post('type_of_education_provider');
							  if($type_of_education_provider =='School /College upto  12'){
								$data3['school_college'] 		= $this->input->post('school_college');
							  }else{
								$data3['school_college'] 		= "";
							  }
						  
						
						}else if($type_of_organization =='Agency'){
						 $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
						 $data3['agency_type'] 					= $this->input->post('agency_type');
						 $data3['type_of_education_provider'] 	= "";
						 $data3['type_of_organization_other'] 	= "";
						 $data3['couching_centre'] 				= "";
						 $data3['professional_taining'] 		= "";
						 $data3['school_college'] 				= "";
						 $agency_type 							= $this->input->post('agency_type');
						     if($agency_type =='Other'){
							    $data3['agency_type_other'] 			= $this->input->post('agency_type_other');
							 }else{
							    $data3['agency_type_other'] 			= "";
							 }
						  
						
						}else if($type_of_organization =='CouchingCentre'){
						  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
						  $data3['couching_centre'] 			= $this->input->post('couching_centre');
						  $data3['type_of_organization_other'] 	= "";
						  $data3['type_of_education_provider'] 	= "";
						  $data3['agency_type_other'] 			= "";
						  $data3['agency_type'] 			    = "";
						  $data3['professional_taining'] 		= "";
						  $data3['school_college'] 				= "";
							  
						}else{
						  $data3['type_of_organization'] 		= $this->input->post('type_of_organization');
						  $data3['professional_taining'] 		= $this->input->post('professional_taining');
						  $data3['type_of_organization_other'] 	= "";
						  $data3['type_of_education_provider'] 	= "";
						  $data3['agency_type_other'] 			= "";
						  $data3['agency_type'] 			    = "";
						  $data3['couching_centre'] 			= "";
						  $data3['school_college'] 				= "";
						
						}
						
						  
						$data3['type_of_ownership'] 		= $this->input->post('type_of_ownership');
						$data3['scholarship'] 				= $this->input->post('scholarship');
						$data3['accademic_exam_system'] 	= $this->input->post('accademic_exam_system');

						$chkOrgValu                         = $this->M_organization_user_registration->findByEmail($id);  

						if(!empty($chkOrgValu)){
						  $this->M_organization_user_registration->update($data3, $id);
						} else{
						  $data3['user_id'] 			= $id;
						  $this->M_organization_user_registration->save($data3);
						}

					

							
						
						if($any_more_campus == 'Yes'){
							
								$data2['organization_name'] 	= $this->input->post('campus_name');
								$data2['address'] 				= $this->input->post('address2');
								$data2['city'] 					= $this->input->post('city_id2');
								$data2['province'] 				= $this->input->post('region_id2');
								$data2['country'] 				= $this->input->post('country_id2');
								$data2['district_id'] 			= $this->input->post('district_id2');
					  			$data2['police_station'] 		= $this->input->post('police_station2');
								$data2['phone_com'] 			= $this->input->post('phone_com1');
								$data2['mobile_com'] 			= $this->input->post('mobile_com1');
								$data2['email_com'] 			= $this->input->post('email_com');
								$data2['location_by_google'] 	= $this->input->post('location_by_google2');
								$data2['founded'] 				= $this->input->post('founded2');
								
								$chkValu                        = $this->M_more_campus_details->findById($id);  
								
								if(!empty($chkValu)){
								   $this->M_more_campus_details->update($data2, $id); 
								
								}else{
						          $data2['organization_id']     = $id;
								  $this->M_more_campus_details->save($data2);
								}
								
							
							}	

					 $this->session->set_userdata(array('suceess' => "Update successfully"));
				     redirect('organizationUserHome/profileUpdate/'.$id);
				
			 }
			 
			 
			 
			 public function draftedJob()
				{   
					if(isAuthenticate()){
						$user_id             		= getUserName();
						$data['user_id']     		= $user_id;
						$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
						$data['userDetails']   		= $userDetails;
						$data['activeUser']   		= $userDetails->name;
						$userAutoId   		        = $userDetails->id;
						$data['userAutoId']     	= $userAutoId;
						$data['page_title']  		= self::Title;
						$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
						$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
						$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
						$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);	
						$data['draftPostInfo']	    = $this->M_job_post_manage->findAllDraft($userAutoId);
						$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
						$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
						$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
						$where                      =  array('position_top' => 'Yes','status' => 'Available');
						$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
						$data['menuInfo']			= $menuInfo;
						$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
						$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
						$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
						$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
						$this->load->view('organizationUserPanel/draftedJobPage', $data);
					
					}
				}
							
				
				public function ArchivedJob()
				{   
					if(isAuthenticate()){
						$user_id             		= getUserName();
						$data['user_id']     		= $user_id;
						$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
						$data['userDetails']   		= $userDetails;
						$data['activeUser']   		= $userDetails->name;
						$userAutoId   		        = $userDetails->id;
						$data['userAutoId']     	= $userAutoId;
						$data['page_title']  		= self::Title;
						$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
						$data['totalLatestJob']	    = $this->M_job_post_manage->countLatestJob($userAutoId);
						$data['totalLatestDraft']	= $this->M_job_post_manage->countLatestDraft($userAutoId);
						$data['totalLatestAchived']	= $this->M_job_post_manage->countLatestAchived($userAutoId);	
						$data['draftPostInfo']	    = $this->M_job_post_manage->findAllArchived($userAutoId);
						$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
						$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
						$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
						$where                      =  array('position_top' => 'Yes','status' => 'Available');
						$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
						$data['menuInfo']			= $menuInfo;
						$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
						$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
						$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
						$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
						$this->load->view('organizationUserPanel/ArchivedJobPages', $data);
					
					}
				}
			 
			 
	        public function PostNewJob()
			{   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['userDetails']   		= $userDetails;
					$data['activeUser']   		= $userDetails->name;
					$userAutoId   		        = $userDetails->id;
					$data['userAutoId']     	= $userAutoId;
					$jobUserDetails   		    = $this->M_organization_user_registration->findByEmail($userAutoId);
					$data['orgType']   			= $jobUserDetails->type_of_organization;
					$data['contPerson']   		= $jobUserDetails->contact_persons;
					$data['desinition']   		= $jobUserDetails->designation;
					$data['page_title']  		= self::Title;	
					$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
					$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
					$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
					$where                      =  array('position_top' => 'Yes','status' => 'Available');
					$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
					$data['menuInfo']			= $menuInfo;
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

					$this->load->view('organizationUserPanel/PostNewJobPage', $data);
				
					}
				}
	
	
	        public function jobPostStore()
			{	
			 if(isAuthenticate()){
				$user_id             				= getUserName();
				$userDetails   	        			= $this->M_all_user_registration->findByUserName($user_id);
				$regionId                         	= $userDetails->region_id;
			  	$regionName   		        		= $this->M_region_manage->findById($regionId);
			  	$data['region'] 					= $regionName->region_name;
			  	$countryDetails   		            = $this->M_country_manage->findById($userDetails->country_id);
				$data['country_name'] 				= $countryDetails->country_name;

				$data['user_id']     				= $userDetails->id;
				$data['add_type'] 					= $this->input->post('add_type');
				$data['job_category'] 	    		= $this->input->post('job_category');
				$data['organization_type'] 			= $this->input->post('organization_type');
				$data['title'] 						= $this->input->post('title');
				$data['vacancies'] 					= $this->input->post('vacancies');
				$receive_online 					= $this->input->post('receive_online');
				$receive_email 						= $this->input->post('receive_email');
				$receive_hardcopy 					= $this->input->post('receive_hardcopy');
				if(!empty($receive_online)){
				  $data['receive_online'] 			= $receive_online;
				}else if(!empty($receive_email)){
				  $data['receive_online'] 			= $receive_email;
				}else{
				  $data['receive_online'] 			= $receive_hardcopy;
				}
				
				$data['cv_enclose'] 	   			= $this->input->post('cv_enclose');
				$data['apllication_deadline'] 	    = $this->input->post('apllication_deadline');
				$data['job_post_date'] 	    		= date("Y-m-d");
				$data['contact_person'] 			= $this->input->post('contact_person');
				$data['desplay_orgname'] 			= $this->input->post('desplay_orgname');
				$data['desplay_address'] 	    	= $this->input->post('desplay_address');

			
				$lastId     = $this->M_job_post_manage->save($data);
				$nextData	= array('lastId' => $lastId);
				$this->session->set_userdata($nextData);
				
				redirect('organizationUserHome/PostNewJobContinue');
			  }
			  
			}
	   
	   
	      
			 
			 
			 public function PostNewJobContinue()
			 {   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['userDetails']   		= $userDetails;
					$data['activeUser']   		= $userDetails->name;
					$userAutoId   		        = $userDetails->id;
					$data['userAutoId']     	= $userAutoId;
					$data['updateId']   		= $this->session->userdata('lastId');
					$data['page_title']  		= self::Title;	
					$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
					$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
					$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
					$where                      =  array('position_top' => 'Yes','status' => 'Available');
					$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
					$data['menuInfo']			= $menuInfo;
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$this->load->view('organizationUserPanel/PostNewJobContinuePage', $data);
				
				}
			}
			
			
			public function PostNewCV()
			{   
				if(isAuthenticate()){
					$user_id             		= getUserName();
					$data['user_id']     		= $user_id;
					$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
					$data['userDetails']   		= $userDetails;
					$data['activeUser']   		= $userDetails->name;
					$userAutoId   		        = $userDetails->id;
					$data['userAutoId']     	= $userAutoId;
					$jobUserDetails   		    = $this->M_organization_user_registration->findByEmail($userAutoId);
					$data['orgType']   			= $jobUserDetails->type_of_organization;
					$data['contPerson']   		= $jobUserDetails->contact_persons;
					$data['desinition']   		= $jobUserDetails->designation;
					$data['page_title']  		= self::Title;	
					$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
					$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
					$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
					$data['UserWisePostInfo']	= $this->M_user_post_manage->findbyUserInfo($userAutoId);
					$where                      =  array('position_top' => 'Yes','status' => 'Available');
					$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
					$data['menuInfo']			= $menuInfo;
					$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
					$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
					$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
					$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
					$this->load->view('organizationUserPanel/PostNewCVPage', $data);
				
					}
				}
			
			
			
			
			 public function nextJobStore($updateId)
				{	
				 
					$data['age_range_from'] 			= $this->input->post('age_range_from');
					$data['age_range_to'] 	    		= $this->input->post('age_range_to');
					$data['gender'] 					= $this->input->post('gender');
					$data['job_type'] 					= $this->input->post('job_type');
					$data['job_level'] 					= $this->input->post('job_level');
					$data['education_qualification'] 	= $this->input->post('education_qualification');
					$data['job_description'] 	    	= $this->input->post('job_description');
					$data['job_requirements'] 			= $this->input->post('job_requirements');
					$data['job_experience'] 			= $this->input->post('job_experience');
					$data['exprience_min'] 	    		= $this->input->post('exprience_min');
					$data['exprience_max'] 	    		= $this->input->post('exprience_max');
					$data['sallary_range'] 				= $this->input->post('sallary_range');
					$data['sallary_money_min'] 	    	= $this->input->post('sallary_money_min');
					$data['sallary_money_max'] 	    	= $this->input->post('sallary_money_max');
					$data['job_publish_status'] 	    = $this->input->post('job_publish_status');
					
					
					 $this->M_job_post_manage->update($data,$updateId);
					 $this->session->set_userdata(array('lastId' => ""));
					 redirect('organizationUserHome/PostNewJobContinue');
				  }
				  
				   public function addManage()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$this->load->view('organizationUserPanel/addManagePage', $data);
						
						}
					}
					
					public function addManageFinal()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
			                $data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$data['allRegion']	        = $this->M_region_manage->findAll();
							$data['allCountry']	        = $this->M_country_manage->findAllCountry();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
							$data['addMsg']     		= $this->session->userdata('addMsg');
							$this->session->set_userdata(array('addMsg' => ""));
							
					         $this->load->view('organizationUserPanel/addManageFinalPage', $data);
						
						}
					}





					public function adBookingChk()
					{   
						if(isAuthenticate()){
							$user_id               = getUserName();
							$data['user_id']       = $user_id;
							$userDetails   		   = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']    = $userDetails->name;
							$data['userDetails']   = $userDetails;
							$userId                = $userDetails->id;
							$data['userAutoId']    = $userId;


							$select_website 		= $this->input->post('select_website');
							$country_name	    	= $this->input->post('country_name');
							$positon 				= $this->input->post('positon');
							$serial_no 				= $this->input->post('serial_no');
							$publish_date 			= $this->input->post('publish_date');
							$expire_date 			= $this->input->post('expire_date');
							
							$where    				= array('position' => $positon, 'serial_no' => $serial_no);
							
							$adValueChk	        	= $this->M_add_booking->adValueChkByDate($where, $publish_date, $expire_date, $select_website, $country_name);

							if(!empty($adValueChk)){
							    echo"1";

							 }
							
						
						}
					}


					
					
					public function advertisementMennage()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$data['allRegion']	        = $this->M_region_manage->findAll();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
							$data['select_website']     = $this->session->userdata('select_website');
							$data['region']     		= $this->session->userdata('region');
							$data['serial_no']     		= $this->session->userdata('serial_no');
							$data['positon']     		= $this->session->userdata('positon');
							$data['publish_date']     	= $this->session->userdata('publish_date');
							$data['exp_date']     		= $this->session->userdata('expire_date');

							
                            $this->load->view('organizationUserPanel/advertisementMennagePage', $data);
							
						
						}
					}
					
					
					public function addStore()
						{	
						 if(isAuthenticate()){
							$user_id             				= getUserName();
							$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
							$country_name 		    			= $this->input->post('country_name');
							$regionDetails   		            = $this->M_country_manage->findCountryId($country_name);
							$regionName   		            	= $this->M_region_manage->findById($regionDetails->region_id);

							$data['user_id']                    = $userDetails->id;
							$data['website'] 					= $this->input->post('select_website');
							$data['positon'] 					= $this->input->post('positon');
							if ($data['website'] == 'country') {
							   $data['region'] 		    		= $regionName->region_name;
							   $data['country_name'] 		    = $country_name;
							}
							$data['serial_no'] 				    = $this->input->post('serial_no');
							$data['publish_date'] 				= $this->input->post('publish_date');
							$data['expire_date'] 				= $this->input->post('expire_date');
							$data['title'] 						= $this->input->post('title');
							$data['manage_date'] 				= date('Y-m-d');
							//$data['deadline_date'] 	    	    = $this->input->post('deadline_date');
							
							$data['description'] 	    	= $this->input->post('description');

							$data['status'] 	    	    	= "notavailable";
							$data['published_status'] 	    	= "unaproave";
							$data['payment_status'] 	    	= "unpaid";



						   $selectvedi                           = $this->input->post("selectvedi");

							if($selectvedi== "yes")
							{
								$data['video_embed']			= $this->input->post("video_embed");
							} else {
							
							$data['add_image'] 		 	= $this->input->post('add_image');
							$data['add_image1'] 		= $this->input->post('add_image1');
						    $data['add_image2'] 		= $this->input->post('add_image2');
						    $data['add_image3'] 		= $this->input->post('add_image3');
							
					      }
																
							
							$lastAdId                      		= $this->M_add_manage->save($data);
							
							$this->session->set_userdata(array('addMsg' => "Submitted Successfully"));
							$promotionDetails      				= $this->M_add_manage->findByLastAdId($lastAdId);
							$datas['ad_id'] 					= $promotionDetails->id;
							$datas['ad_user_id'] 				= $promotionDetails->user_id;
							$datas['ad_title'] 					= $promotionDetails->title;
						    $this->M_search_table->save($datas);
							
						  }

						  redirect('organizationUserHome/addManageSuccess');
						  
						}
						
						
						
						public function regionAddStore()
						{	
						 if(isAuthenticate()){
							$user_id             				= getUserName();
							$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
							$data['user_id']                    = $userDetails->id;
							$region 							= $this->input->post('region');
							$positon 							= $this->input->post('regionPositon');
							$serialNo 	    					= $this->input->post('region_serial_no');
							$data['title'] 						= $this->input->post('title');
							$data['add_link'] 					= $this->input->post('add_link');
							$data['deadline_date'] 	    	    = $this->input->post('deadline_date');
							$data['description'] 	    	    = $this->input->post('description');
							$data['status'] 	    	    	= "notavailable";
							
							
							$config['upload_path'] 		= './Images/Add_image/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['file_name']		= time();
							$file_field					= 'add_image';
							
							$this->upload->initialize($config);
							if($this->upload->do_upload('add_image')) {
							$uploadData = $this->upload->data();
							$data['add_image'] = $uploadData['file_name'];
							}
				
							
							$this->M_add_manage->updateRegionAdd($data, $region, $positon, $serialNo);
							 $this->session->unset_userdata('payment');
							
							redirect('organizationUserHome/addManageFinal'); 
						  }
						  
						}
						
						
						
						
						public function addManageSuccess()
						{   
							if(isAuthenticate()){
								$user_id             		= getUserName();
								$data['user_id']     		= $user_id;
								$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
								$data['activeUser']   		= $userDetails->name;
								$data['userDetails']   		= $userDetails;
								$userId                     = $userDetails->id;
								$data['userAutoId']     	= $userId;
								$data['page_title']  		= self::Title;
								$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
								$data['userType']	        = $this->M_payment_gateaway->findAll();
								$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
								$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
								$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
								$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
								$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
								$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							    $data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
							    $data['addMsg']     		= $this->session->userdata('addMsg');
							    $this->session->set_userdata(array('addMsg' => ""));
							
								$this->load->view('organizationUserPanel/addManageSuccessPage', $data);
							
							}
						}
						
						
						public function addEdit($id)
						{   
							if(isAuthenticate()){
								$user_id             		= getUserName();
								$data['user_id']     		= $user_id;
								$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
								$data['activeUser']   		= $userDetails->name;
								$data['userDetails']   		= $userDetails;
								$userId                     = $userDetails->id;
								$data['userAutoId']     	= $userId;
								$data['page_title']  		= self::Title;
								$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
								$data['addEditInfo']		= $this->M_add_manage->findById($id);
								$data['allCountry']	        = $this->M_country_manage->findAllCountry();	
								$data['userType']	        = $this->M_payment_gateaway->findAll();
								$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
								$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
								$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
								$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
								$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
								$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
								$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
								$this->load->view('organizationUserPanel/addEditPage', $data);
							
							}
						}
						
						
						public function addUpdate($id)
						{	
						 if(isAuthenticate()){
							$user_id             				= getUserName();
							$userDetails   		        		= $this->M_all_user_registration->findByUserName($user_id);
							$data['user_id']                    = $userDetails->id;
							$country_name 		    			= $this->input->post('country_name');
							$regionDetails   		            = $this->M_country_manage->findCountryId($country_name);
							$regionName   		            	= $this->M_region_manage->findById($regionDetails->region_id);
							$data['website'] 					= $this->input->post('select_website');
							if ($data['website'] == 'country') {
							   $data['region'] 		    		= $regionName->region_name;
							   $data['country_name'] 		    = $country_name;
							} else {
							   $data['region'] 		    		= "";
							   $data['country_name'] 		    = "";
							}
							$data['positon'] 					= $this->input->post('positon');
							$data['serial_no'] 					= $this->input->post('serial_no');
							$data['publish_date'] 				= $this->input->post('publish_date');
							$data['expire_date'] 				= $this->input->post('expire_date');
							$data['serial_no'] 					= $this->input->post('serial_no');
							$data['title'] 						= $this->input->post('title');
							$add_link 							= $this->input->post('add_link');
							$description 	    	    		= $this->input->post('description');
							if(!empty($add_link)){
							   $data['add_link'] 			    = $add_link;							   	
							} else {
							   $data['add_link'] 				= $this->input->post('add_link_edit');
							}
                            if(!empty($description)){
							   $data['description'] 			= $description;							   	
							} else {
							   $data['description'] 	    	= $this->input->post('description');							   
							}

							$add_image 		 			        = $this->input->post('add_image');
							if(!empty($add_image)){
								$data['add_image'] = $add_image;	
							}

							$adEditinfo = $this->M_add_manage->findById($id);

							if( !empty($adEditinfo->add_image) && file_exists('./Images/Add_image/'.$adEditinfo->add_image) && !empty($add_image) ) {					
								unlink('./Images/Add_image/'.$adEditinfo->add_image);	
							}
							
							$this->M_add_manage->update2($data, $id);

							$adDetails 			    	= $this->M_add_manage->findById($id);

							$datas['ad_title'] 	    	= $adDetails->title;

						    $adDetailsChk 				= $this->M_search_table->findByAdId($id);

						    if(!empty($adDetailsChk)){
						       $this->M_search_table->updateAd($datas, $id);
						    } else{
							  $datas['ad_id'] 	    	= $id;
							  $datas['ad_user_id']    	= $adDetails->user_id;

						      $this->M_search_table->save($datas);	
						    }
										
							redirect('organizationUserHome/addManageFinal'); 
						  }
						  
						}
					
					
					
					
					public function addManageBkask()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$premiumType 			    = $this->input->post('premium_user_type');
							$amount 	        		= $this->input->post('amount');
							$paymentBy	                = $this->input->post('paymentBy');
							$usertypePament	    		= $this->M_payment_gateaway->findById($premiumType);
							$userTypeName               = $usertypePament->premium_user_type; 	
							$paymentUserData			= array('premiumType' => $userTypeName, 'amount' => $amount, 'paymentBy' => $paymentBy);
							$this->session->set_userdata($paymentUserData);
							
							$data['paymentBy'] 	        = $paymentBy;
							$data['usertypePament']	    = $this->M_payment_gateaway->findById($premiumType);	
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							if($paymentBy == 'bkask'){
								$this->load->view('organizationUserPanel/addManageBkaskPage', $data);
								
								} else if($paymentBy == 'mastercard') {
									$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManageMasterPage', $data);

									
								} else if($paymentBy == 'paypal') {
									$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManagePaypalPage', $data);
									
									
								} else if($paymentBy == 'visa') {
									$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManageVisaPage', $data);
									
									
								} else if($paymentBy == 'bank') {
									$this->load->view('organizationUserPanel/addManageBankPage', $data);
								
									
									} else {
										$data['msg']	= "Under Construction";
									$this->load->view('organizationUserPanel/addManageAmericanPage', $data);
									}						
						}
					}
					
					public function adManageTransactionBkask()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							
							$data['premiumType'] 		= $this->session->userdata('premiumType');
						    $data['amount'] 			= $this->session->userdata('amount');
							$data['paymentBy'] 			= $this->session->userdata('paymentBy');
							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							$this->load->view('organizationUserPanel/adManageTransactionBkaskPage', $data);			
						}
					}
					
					public function paymentStatus()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['userPayment']        = $this->M_payment_manage->findByUserPayment($userId);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							$this->load->view('organizationUserPanel/paymentStatusPage', $data);			
						}
					}
					
					
					
					
					
					
					public function paymentdetails()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['userPayment']        = $this->M_payment_manage->findByUserPayment($userId);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							
							$data['page_title']  		= "Payment Details";
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$data['pamymentdetail']      = $this->M_organize_profile_manage->paymentdetails('payment_details');
							
							$this->load->view('organizationUserPanel/orgpaymentdetailsPage', $data);			
						}
					}
					
					
					
					
					public function paymentmanage()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['userPayment']        = $this->M_payment_manage->findByUserPayment($userId);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							
							$data['page_title']  		= "Payment Manage";
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$data['pamymentdetail']      = $this->M_organize_profile_manage->paymentdetails('payment_details');
							
							$this->load->view('organizationUserPanel/orgpaymentManagePage', $data);			
						}
					}
					
					
					
					
				public function paymentstore()
				{
			
				   $paymentType            = $this->input->post("paymentType");
				   if($paymentType == 'BKash'){
				   	  $user_id             		      = getUserName();
					  $userDetails   		          = $this->M_all_user_registration->findByUserName($user_id);
					 $data1['user_id']                 = $userDetails->id;
					 $data1['user_type']		      = $userDetails->user_type;
				     $data1['bashphoneNumber']        = $this->input->post("bashphoneNumber");
					 $data1['amount'] 	   			  = $this->input->post("amount");
				     $data1['paymentdate'] 	          = $this->input->post("paymentdate");
					 $data1['payment_status'] 	      = "unpaid";
					 $data1['payment_by'] 	          = $paymentType;
					 
					 $this->db->insert('payment_manage', $data1);
					 redirect("organizationUserHome");
				   } else {
				   
				   
				    $user_id             		  = getUserName();
					$userDetails   		          = $this->M_all_user_registration->findByUserName($user_id);
					$data['user_id']              = $userDetails->id;
					$data['user_type']		      = $userDetails->user_type;
				   $data['receiptNumber']          = $this->input->post("receiptNumber");
				   $data['bankaccountnumber']      = $this->input->post("bankaccountnumber");
				   $data['bankaccountname'] 	   = $this->input->post("bankaccountname");
				   $data['bankname']    		   = $this->input->post("bankname");
				   $data['amount'] 	   			   = $this->input->post("amount");
				   $data['paymentdate'] 	       = $this->input->post("paymentdate");
				   $data['payment_status'] 	       = "unpaid";
				   $data['payment_by'] 	          = $paymentType;
				   $this->db->insert('payment_manage', $data);
				  }
				  
			 redirect("organizationUserHome");  
				   
			}
					
					
					
					
					
					
					
					
					
					public function adManageTransactionBank()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							
							$data['premiumType'] 		= $this->session->userdata('premiumType');
						    $data['amount'] 			= $this->session->userdata('amount');
							$data['paymentBy'] 			= $this->session->userdata('paymentBy');
							
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							$this->load->view('organizationUserPanel/adManageTransactionBankPage', $data);			
						}
					}
					
					public function addManageTransactionBkaskAction()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							
							$data2['user_type'] 		= $this->session->userdata('premiumType');
						    $data2['amount'] 			= $this->session->userdata('amount');
							$data2['payment_by'] 		= $this->session->userdata('paymentBy');
							$data2['user_id'] 			= $userId;
							$data2['transaction_id'] 	= $this->input->post('transaction_id');
							$data2['payment_status'] 	= "Paid";
							$paymentConfirm             = array('payment' => "yes");
							$this->session->set_userdata($paymentConfirm);
							
							$this->M_payment_manage->save($data2);
			                 $this->load->view('organizationUserPanel/paymentSuccessAlertPage');	
						}
					}
					
					public function addManageBankTransVisaFinalAction()                                                                    
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							}
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
						 
							$data2['user_type'] 		= $this->session->userdata('premiumType');
						    $data2['amount'] 			= $this->session->userdata('amount');
							$data2['payment_by'] 		= "bank";
							$data2['user_id'] 			= $userId;
							$data2['transaction_id'] 	= $this->input->post('transaction_id');
							$data2['card_naumber']  	= $this->input->post('card_naumber');
							$data2['security_code'] 	= $this->input->post('security_code');
							$data2['expiry_date']   	= $this->input->post('expiry_date');
							$data2['card_holder_name'] 	= $this->input->post('card_holder_name');
							$data2['payment_status'] 	= "Paid";
							$paymentConfirm             = array('payment' => "yes");
							$this->session->set_userdata($paymentConfirm);
							
							$this->M_payment_manage->save($data2);
							$this->load->view('organizationUserPanel/paymentSuccessAlertPage');	
							
						
					}
					
					public function addManageTransactionBankAction()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['userType']	        = $this->M_payment_gateaway->findAll();
							$premiumType 			    = $this->input->post('premium_user_type');
							$data['user_type'] 		    = $this->session->userdata('premiumType');
						    $data['amount'] 			= $this->session->userdata('amount');
							$data['payment_by'] 		= $this->session->userdata('paymentBy');
							$paymentbank	            = $this->input->post('payment_bank');
							$usertypePament	    		= $this->M_payment_gateaway->findById($premiumType);
							$userTypeName               = $usertypePament->premium_user_type; 	
							
							$data['paymentbank'] 	    = $paymentbank;
							$data['usertypePament']	    = $this->M_payment_gateaway->findById($premiumType);	
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['addPermissionInfo']	= $this->M_add_manage->findAllAvailable();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							
							if($paymentbank == 'visab'){
							    $data['paymentbank'] 	    = $paymentbank;
								$this->load->view('organizationUserPanel/addManageBankFinalActionPage', $data);
								
								}else{
								    $data['paymentbank'] 	    = $paymentbank;
									$this->load->view('organizationUserPanel/addManageBankFinalActionMasterPage', $data);
							}						
						}
					}
					 
			   public function searchAllApplicant()

				{	
					$postId      		= $this->input->post('postId');
					$applicantName      = $this->input->post('applicantName');
					$ageFrom      		= $this->input->post('ageFrom');
					$ageTo     			= $this->input->post('ageTo');
					$gender      		= $this->input->post('gender');
					$starcandidates     = $this->input->post('starcandidates');
					
					$degree      		= $this->input->post('degree');
					$degreeTitle      	= $this->input->post('degreeTitle');
					$subject     		= $this->input->post('subject');
					$result      		= $this->input->post('result');
					$institute     		= $this->input->post('institute');
					
					$where = array('job_application_details.post_id' => $postId);
					if(!empty($applicantName) || !empty($ageFrom) || !empty($ageTo) || !empty($gender) || !empty($starcandidates)) {
						
						if(!empty($applicantName)) 	$where['job_application_details.applicant_name'] = $applicantName;
						if(!empty($ageFrom) && !empty($ageTo)) {		
							$where['job_application_details.age >= '] = $ageFrom;
							$where['job_application_details.age <= '] = $ageTo;
						} else if(!empty($ageFrom)) {
							$where['job_application_details.age'] = $ageFrom;
						} else if(!empty($ageTo)) {
							$where['job_application_details.age'] = $ageTo;
						}
						if(!empty($gender)) 		$where['job_application_details.gender'] = $gender;
						if(!empty($starcandidates)) $where['job_application_details.star_candidate_status'] = $starcandidates;
						
						$data['searchQuery'] = $this->M_job_application_details->applySearch($where);						

						$this->load->view('organizationUserPanel/searchResultPage', $data);
					}


					$where2 = array('job_application_details.post_id' => $postId);
					if(!empty($degree) || !empty($degreeTitle) || !empty($subject) || !empty($result) || !empty($institute)) {
						if(!empty($degree)) 			$where2['job_application_details.degree'] = $degree;
						if(!empty($degreeTitle)) 		$where2['job_application_details.degree_title'] = $degreeTitle;
						if(!empty($subject)) 			$where2['job_application_details.subject'] = $subject;
						if(!empty($result)) 			$where2['job_application_details.result'] = $result;
						if(!empty($institute)) 			$where2['job_application_details.institute_name'] = $institute;
						
						$data['searchQuery2'] = $this->M_job_application_details->applySearch2($where2);
						
						$this->load->view('organizationUserPanel/searchResultPage2', $data);
					}
					
					
				}
				
				   public function organizeProfile()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeHomeInfo']	= $this->M_organize_profile_manage->findHomeByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeHomePage', $data);
						
						}
					}

					
				  
				   public function organizeAbout()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findAboutByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeAboutPage', $data);
						
						}
					}
					
					
					public function organizeaboutindia()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findAboutindaiByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeAboutIndaiPage', $data);
						
						}
					}
					
					
					public function organizeCoursefree()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$where = array('organize_id' => $userId);
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findorgallByUser('orgnize_docsfileupload', $where);
							
							$this->load->view('organizationUserPanel/organizeCoursefreePage', $data);
						
						}
					}
					
					
					public function organizeligiblity()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findeliegiblityByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeegiblityPage', $data);
						
						}
					}
					
					
					public function organizehostelandordercost()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findhostelandothercostByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeHostelandorthercostPage', $data);
						
						}
					}
					
					
					public function organizeScholarship()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findscholshiperByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeScholarshipPage', $data);
						
						}
					}
					
					public function travelitinerary()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findstravelitineraryByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeTravelItineraryPage', $data);
						
						}
					}
					
					public function howtoapply()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findhowtoapplyByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizehowtoapplyPage', $data);
						
						}
					}
					
					public function whentoapply()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findwhentoapplyByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizewhentoapplyPage', $data);
						
						}
					}
					
					public function applyforVisa()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$where = array('organize_id' => $userId, 'page_type' => 'applyforVisa');
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findorgallByUser('organize_contact_about_manage', $where);
							$this->load->view('organizationUserPanel/organizeapplyforVisaPage', $data);
						
						}
					}
					
					public function applyonline()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							
							$where = array('organize_id' => $userId, 'page_type' => 'applyonline');
							$data['organizeAboutInfo']	= $this->M_organize_profile_manage->findorgallByUser('organize_contact_about_manage', $where);
							$this->load->view('organizationUserPanel/organizeapplyOnlinePage', $data);
						
						}
					}
					
					
					
					public function organizeContact()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeContInfo']	= $this->M_organize_profile_manage->findContactByUser('organize_contact_about_manage', $userId);
							$this->load->view('organizationUserPanel/organizeContactPage', $data);
						
						}
					}
					
					
					public function organizeSlide()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeContInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $userId);
							$this->load->view('organizationUserPanel/organizeSlidePage', $data);
						
						}
					}


					public function newsEventManage()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllNewsByOrgId($userId);
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeContInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $userId);
							$this->load->view('organizationUserPanel/organizeNewsEventPage', $data);
						
						}
					}


					public function organizeNewsEdit($id)
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['newsEventEditInfo']	= $this->M_news_and_event_manage->findById($id);
							$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllNewsByOrgId($userId);
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeContInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $userId);
							$this->load->view('organizationUserPanel/organizeNewsEventEditPage', $data);
						
						}
					}

					public function orgNewEventStore()
					{ 

					   if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['user_id']            = $userDetails->id;
							$id              			= $this->input->post('id');
							$data['title']              = $this->input->post('title');
							$data['date']               = $this->input->post('date');
							$data['description']        = $this->input->post('description');
							$data['news_type']          = $this->input->post('news_type');
							$data['video_type']         = $this->input->post('video_type');
							$data['embeded_code']       = $this->input->post('embeded_code');


                           
							$config['upload_path'] 		= './Images/News_image/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['max_width']  		= '0';
							$config['max_height']  		= '0';
							$config['file_name']		= time();
							
							$this->upload->initialize($config);

							if($data['news_type'] == 'image'){
							  if($this->upload->do_upload('image')) {
								$uploadData = $this->upload->data();
								$data['image'] = $uploadData['file_name'];

								if(!empty($id)){
								   $model		= $this->M_news_and_event_manage->findById($id);
								   
								   if(!empty($only_image)){
									   unlink('./Images/News_image/'.$model->image);
									   unlink('./Images/News_image_resize/'.$model->image);
									}
								
								}
								
								$config['image_library'] = 'gd2';
								$config['source_image'] = $uploadData['full_path'];
								$config['maintain_ratio'] = TRUE;
								$config['new_image'] = './Images/News_image_resize/';
								$config['width'] = 600;
								$config['height'] = 315;
								
								$this->image_lib->initialize($config); 
								
								$this->image_lib->resize();
							  }
							} else {
							 	if($this->upload->do_upload('video')) {
								 $uploadData = $this->upload->data();
								 $data['video'] = $uploadData['file_name'];
								
									if(!empty($id)){
								       $model		= $this->M_news_and_event_manage->findById($id);
									   if(!empty($model->video)){
										   unlink('./Images/News_image/'.$model->video);									   
										}
									}
								}
							}


                            if(!empty($id)){
						      $model				= $this->M_news_and_event_manage->findById($id);
						      $data['status']       = $model->status;
							  $this->M_news_and_event_manage->update($data, $id);
							}else{
							  $data['status']       = "unaproave";
							  $this->M_news_and_event_manage->save($data);
							}
							redirect('organizationUserHome/newsEventManage');
				
						}

					}
					
					
					
					public function organizeSlideEdit($id)
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['orgSlideEditInfo']	= $this->M_organize_profile_manage->findById('organize_slide_manage', $id);
							$data['organizeContInfo']	= $this->M_organize_profile_manage->findAllSlideDataByUser('organize_slide_manage', $userId);
							$this->load->view('organizationUserPanel/organizeSlideEditPage', $data);
						
						}
					}
					
					
					public function organizeGallery()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizePhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoDataByUser('organize_photo_gallery_manage', $userId);
							$this->load->view('organizationUserPanel/organizePhotoPage', $data);
						
						}
					}





					public function organizePhotoEdit($id)
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['orgPhotoEditInfo']	= $this->M_organize_profile_manage->findById('organize_photo_gallery_manage', $id);
							$data['organizePhotoInfo']	= $this->M_organize_profile_manage->findAllPhotoDataByUser('organize_photo_gallery_manage', $userId);
							$this->load->view('organizationUserPanel/organizePhotoEditPage', $data);
						
						}
					}
					



					public function organizeVideoGallery()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeVideoInfo']	= $this->M_organize_profile_manage->findAllVideoDataByUser('video_manage', $userId);
							$this->load->view('organizationUserPanel/organizeVideoPage', $data);
						
						}
					}


					public function organizeVideoEdit($id)
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['orgVideoEditInfo']	= $this->M_organize_profile_manage->findById(' video_manage', $id);
							$data['organizeVideoInfo']	= $this->M_organize_profile_manage->findAllVideoDataByUser('video_manage', $userId);
							$this->load->view('organizationUserPanel/organizeVideoEditPage', $data);
						
						}
					}



					public function gifImageCreate()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['organizeGifInfo']	= $this->M_organize_profile_manage->findAllGifImage('gif_image_manage', $userId);
							$this->load->view('organizationUserPanel/organizeGifImagePage', $data);
						
						}
					}


					public function organizeGifEdit($id)
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['orgGifEditInfo']		= $this->M_organize_profile_manage->findById('gif_image_manage', $id);
							$data['organizeGifInfo']	= $this->M_organize_profile_manage->findAllGifImage('gif_image_manage', $userId);
							$this->load->view('organizationUserPanel/organizeGifEditPage', $data);
						
						}
					}


				   public function orgGifImageStore()
				   {

				   	   if(isAuthenticate()){
							$user_id                = getUserName();
							$userDetails   		    = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   	= $userDetails->id;
						
						}

						$id		    		    	= $this->input->post('id');
						$data['image_position'] 	= $this->input->post('image_position');
						$image1						= $this->input->post('image1');
						$image2 					= $this->input->post('image2');
						$image3						= $this->input->post('image3');
						$image4 					= $this->input->post('image4');
						
						if(!empty($image1)) $data['image1'] = $image1;	
						if(!empty($image2)) $data['image2'] = $image2;	
						if(!empty($image3)) $data['image3'] = $image3;	
						if(!empty($image4)) $data['image4'] = $image4;	
						
						if(!empty($id)) {
							$editInfo = $this->M_organize_profile_manage->findById('gif_image_manage', $id);
							
							if( !empty($editInfo->image1) && file_exists('./resource/gif_image/'.$editInfo->image1) && !empty($image1) ) {					
								unlink('./resource/gif_image/'.$editInfo->image1);	
							}
							if( !empty($editInfo->image2) && file_exists('./resource/gif_image/'.$editInfo->image2) && !empty($image2) ) {					
								unlink('./resource/gif_image/'.$editInfo->image2);	
							}
							if( !empty($editInfo->image3) && file_exists('./resource/gif_image/'.$editInfo->image3) && !empty($image3) ) {					
								unlink('./resource/gif_image/'.$editInfo->image3);	
							}
							if( !empty($editInfo->image4) && file_exists('./resource/gif_image/'.$editInfo->image4) && !empty($image4) ) {					
								unlink('./resource/gif_image/'.$editInfo->image4);	
							}
							
							$this->M_organize_profile_manage->update2('gif_image_manage', $id, $data);
						} else {
							$this->M_organize_profile_manage->save('gif_image_manage', $data);
						}
			 			
						redirect('organizationUserHome/gifImageCreate');
						 
				    }

				    public function makeGif($id)
				    {
				      $data['gifData']	    = $this->M_organize_profile_manage->findById('gif_image_manage', $id);
					  
				     $this->load->view('slideshow', $data);	  

				    }



				public function orgAboutindaiContactStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					
					
					public function orgcoursefreesStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						}
					
					
						$id	    		    		= $this->input->post('id');
						$data['title'] 	    		= $this->input->post('title');
						$config['upload_path'] 		= './Images/docfileUpload/';
						$config['allowed_types']    = 'docx|doc|DOC|DOCX|pdf|PDF|htm|html';
						$config['file_name']  		= time();
						$file_field					= 'docs_pdf_file';
						
				
						if(!empty($id)){
							$where = array('id' => $id, 'organize_id' => $data['organize_id']);
						
							$result = $this->M_organize_profile_manage->findorgallByUser('orgnize_docsfileupload', $where);
						
							if(!empty($result)){
							
								$onliidoc  = $result->docs_pdf_file;
								if(!empty($onliidoc)){
									unlink('Images/docfileUpload/' . $onliidoc);
								}
								/// IMAGE UPLOAD ACTION
								$this->upload->initialize($config);
								if ($this->upload->do_upload($file_field))
								{
								/// ERORR SHOW FOR IMAGE UPLOAD
							
								$image_data 			= $this->upload->data();				
								$data['docs_pdf_file']  = $image_data['file_name'];
								
								 $this->M_organize_profile_manage->update2('orgnize_docsfileupload', $id, $data);	
								}
							
							
							}else {
								
								/// IMAGE UPLOAD ACTION
								$this->upload->initialize($config);
								if ($this->upload->do_upload($file_field))
								{
								/// ERORR SHOW FOR IMAGE UPLOAD
								$image_data 			= $this->upload->data();				
								$data['docs_pdf_file']  = $image_data['file_name'];
								
								 $this->M_organize_profile_manage->save('orgnize_docsfileupload', $id, $data);	
								}
							}

						}else{
							
							/// IMAGE UPLOAD ACTION
							$this->upload->initialize($config);
							if ($this->upload->do_upload($file_field))
							{
								/// ERORR SHOW FOR IMAGE UPLOAD
							
								$image_data 			= $this->upload->data();				
								$data['docs_pdf_file']  = $image_data['file_name'];
								$this->M_organize_profile_manage->save('orgnize_docsfileupload', $data);	
							}
						
						}

						redirect('organizationUserHome');
					}
							

					public function orgeligiblityStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					public function contactUsStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						}

						$data['page_type'] 	    		= $this->input->post('page_type');
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeContactInfo			= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					
					public function orghostelandorthercostStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					
					public function orgscloshiperStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					public function orgtravelitineraryStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					public function orghowtoapplyStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					
					public function orgwhentoapplyStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
						$data['page_type'] 	    		= $this->input->post('page_type');
						
						$data['details'] 	    		= $this->input->post('details');
						
						$organizeAboutContactInfo		= $this->M_organize_profile_manage->findAboutContactByUser('organize_contact_about_manage', $data['organize_id'], $data['page_type']);
						
						if(!empty($organizeAboutContactInfo)){
						    $this->M_organize_profile_manage->update('organize_contact_about_manage', $data['organize_id'], $data['page_type'], $data);	
						}else{
						   $this->M_organize_profile_manage->save('organize_contact_about_manage', $data);	
						}
						
									
					     redirect('organizationUserHome');
							
					}
					
					
					
					
					public function orgSlideStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
							$id 	    				= $this->input->post('edit_id');
							
							$data['title'] 	    		= $this->input->post('title');
							$data['details'] 	    		= $this->input->post('details');
							$data['news_date'] 	    	= $this->input->post('news_date');
							$config['upload_path'] 		= './Images/slide_image/';
							$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
							$config['max_size']			= '0';
							$config['file_name']		= time();
							$file_field					= 'image';
							
							$this->upload->initialize($config);
							if($this->upload->do_upload('image')) {
							$uploadData = $this->upload->data();
							$data['image'] = $uploadData['file_name'];
							
							if(!empty($id)){
							   $model		= $this->M_organize_profile_manage->findById('organize_slide_manage', $id);
							   $only_image  = $model->image;
							   
							   if(!empty($only_image)){
								   unlink('./Images/slide_image/'.$only_image);
								   unlink('./Images/slide_image_resize/'.$only_image);
								}
							
							}
							
							$config['image_library'] = 'gd2';
							$config['source_image'] = $uploadData['full_path'];
							$config['maintain_ratio'] = TRUE;
							$config['new_image'] = './Images/slide_image_resize/';
							$config['width'] = 70;
							$config['height'] = 50;
							
							$this->image_lib->initialize($config); 
							
							$this->image_lib->resize();
							}
							
						     if(!empty($id)){
							    $this->M_organize_profile_manage->update2('organize_slide_manage', $id, $data);	  
							 }else{
							   $this->M_organize_profile_manage->save('organize_slide_manage', $data);	 
							 }
						      
							  
					       redirect('organizationUserHome/organizeSlide');
							
					}
					
					
					
					
					public function orgPhotoGalleryStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
							$id 	    				= $this->input->post('edit_id');
							$data['title'] 	    		= $this->input->post('title');
							$config['upload_path'] 		= './Images/photo_gallery/';
							$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
							$config['max_size']			= '0';
							$config['file_name']		= time();
							$file_field					= 'image';
							
							$this->upload->initialize($config);
							if($this->upload->do_upload('image')) {
							$uploadData = $this->upload->data();
							$data['image'] = $uploadData['file_name'];
							
							if(!empty($id)){
							   $model		= $this->M_organize_profile_manage->findById('organize_photo_gallery_manage', $id);
							   $only_image  = $model->image;
							   
							   if(!empty($only_image)){
								   unlink('./Images/photo_gallery/'.$only_image);
								   unlink('./Images/photo_gallery_resize/'.$only_image);
								}
							
							}
							
							$config['image_library'] = 'gd2';
							$config['source_image'] = $uploadData['full_path'];
							$config['maintain_ratio'] = TRUE;
							$config['new_image'] = './Images/photo_gallery_resize/';
							$config['width'] = 300;
							$config['height'] = 250;
							
							$this->image_lib->initialize($config); 
							
							$this->image_lib->resize();
							}
							
						     if(!empty($id)){
							    $this->M_organize_profile_manage->update2('organize_photo_gallery_manage', $id, $data);	  
							 }else{
							  $this->M_organize_profile_manage->save('organize_photo_gallery_manage', $data);	 
							 }
						      
							  
					      redirect('organizationUserHome/organizeGallery');
							
					}



					public function orgVideoStore()
					{
					    if(isAuthenticate()){
							$user_id             		= getUserName();
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
						}
							$id 	    				= $this->input->post('edit_id');
							
							
							$data['video_type'] 	    = $this->input->post('video_type');
							$data['video_embede_code'] 	= $this->input->post('video_embede_code');
							
							
							$config['upload_path'] 		= './resource/video_upload/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['max_width']  		= '0';
							$config['max_height']  		= '0';
							
							$this->upload->initialize($config);
							
							
							if($this->upload->do_upload('video')) {
							$uploadData = $this->upload->data();
							
							 $data['video'] = $uploadData['file_name'];
							
								if(!empty($id)){
								   $model		= $this->M_organize_profile_manage->findById('organize_photo_gallery_manage', $id);
								   
								   if(!empty($model->video)){
									   unlink('./resource/video_upload/'.$model->video);									   
									}
								
								}
								
							
							}
							
						     if(!empty($id)){
							   $this->M_organize_profile_manage->update2('video_manage', $id, $data);	  
							 }else{
							   $this->M_organize_profile_manage->save('video_manage', $data);	 
							 }
						      
							  
					       redirect('organizationUserHome/organizeVideoGallery');
							
					}


					public function orgVideoStore_fromAdminpanel()
					{
					 
							$user_id             		= $this->input->post('user_id');
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['organize_id']   		= $userDetails->id;
						
				
							$id 	    				= $this->input->post('edit_id');
							
							
							$data['video_type'] 	    = $this->input->post('video_type');
							$data['video_embede_code'] 	= $this->input->post('video_embede_code');
							
							
							$config['upload_path'] 		= './resource/video_upload/';
							$config['allowed_types'] 	= '*';
							$config['max_size']			= '0';
							$config['max_width']  		= '0';
							$config['max_height']  		= '0';
							
							$this->upload->initialize($config);
							
							
							if($this->upload->do_upload('video')) {
							$uploadData = $this->upload->data();
							
							 $data['video'] = $uploadData['file_name'];
							
								if(!empty($id)){
								   $model		= $this->M_organize_profile_manage->findById('organize_photo_gallery_manage', $id);
								   
								   if(!empty($model->video)){
									   unlink('./resource/video_upload/'.$model->video);									   
									}
								
								}
								
							
							}
							
						     if(!empty($id)){
							   $this->M_organize_profile_manage->update2('video_manage', $id, $data);	  
							 }else{
							   $this->M_organize_profile_manage->save('video_manage', $data);	 
							 }
						      
							  
					       redirect('organizationUserHome/organizeVideoGallery');
							
					}

					public function manager_modal(){
						$data['single_userId'] =  $this->input->post('user_id');

						print_r($data['single_userId']);
						exit;
						
						$this->load->view('all_profile_panel_menu_page', $data);
					}

					public function totalVisitor($onset = 0)
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;
							$data['page_title']  		= self::Title;
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['totalVisitorDet']    = $this->M_visitor_count->findByOrganize($userId, $onset);

							$data['onset'] 				= $onset;
							$config['base_url'] 		= base_url('organizationUserHome/totalVisitor');
							$config['total_rows'] 		= $this->M_visitor_count->countAll($userId);
							$config['uri_segment'] 		= 3;
							$config['per_page'] 		= 6;
							$config['num_links'] 		= 7;
							$config['first_link']		= FALSE;
							$config['last_link'] 		= FALSE;
							$config['prev_link']		= 'Prev';
							$config['next_link'] 		= 'Next';
					
							$this->pagination->initialize($config); 

							$month = array('01','02','03','04','05','06','07','08','09','10','11','12');
							 $tatalYearResult = 0;          
							 foreach($month as $m){
							  $startDate 			   = date("Y-m-d", strtotime(date("Y-$m-01")));
							  $endDate 	 			   = date("Y-m-t", strtotime(date("Y-$m-01")));
							
							  $where4['date >=']       = $startDate;
		                      $where4['date <=']       = $endDate;
		                      $where4['organize_id']   = $userId;
							  $totalCurMonthVisit	   = $this->M_visitor_count->findAllBycurrentMonth($where4);
                              $tatalYearResult 		   = $tatalYearResult + $totalCurMonthVisit;
							 
							}

							$data['totalYearVisit'] = $tatalYearResult;


							date_default_timezone_set('Australia/Brisbane');
							$first_day_of_the_week = 'Saturday'; 
							$start_of_the_week     = strtotime("Last $first_day_of_the_week");
							
							if ( strtolower(date('l')) === strtolower($first_day_of_the_week) )
							
							{
							
								$start_of_the_week = strtotime('today');
							
							}
							
							$end_of_the_week = $start_of_the_week + (60 * 60 * 24 * 7) - 1;
							
							$date_format =  'Y-m-d';
							
						    $satrtWeekDate 				= date($date_format, $start_of_the_week);
						    $endWeekDate 				= date($date_format, $end_of_the_week);
							$startDate 					= date("Y-m-d", strtotime(date("Y-m-01")));
	                        $endDate 	 	        	= date("Y-m-t", strtotime(date("Y-m-01"))); 
	                        $where['date >=']           = $startDate;
	                        $where['date <=']           = $endDate;
	                        $where['organize_id']       = $userId;
	                        $whereWeek['date >=']       = $satrtWeekDate;
	                        $whereWeek['date <=']       = $endWeekDate;
	                        $whereWeek['organize_id']   = $userId;
							$data['totalVisitToday']	= $this->M_visitor_count->findAllBycurrentDate($userId);
							$data['totalCurWeekVisit']	= $this->M_visitor_count->findAllBycurrentWeek($whereWeek);
							$data['totalCurMonthVisit']	= $this->M_visitor_count->findAllBycurrentMonth($where);
							$data['allRegion']	        = $this->M_region_manage->findAll();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
							
							$this->load->view('organizationUserPanel/totalVisitorPage', $data);
						
						}
					}



					public function visitorSort()
					{   
						if(isAuthenticate()){
							$user_id             		= getUserName();
							$data['user_id']     		= $user_id;
							$userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
							$data['activeUser']   		= $userDetails->name;
							$data['userDetails']   		= $userDetails;
							$userId                     = $userDetails->id;
							$data['userAutoId']     	= $userId;

							$data['select_category'] 	= $this->input->post('select_category');
							$data['from_date'] 	    	= $this->input->post('from_date');
							$data['to_date'] 	        = $this->input->post('to_date');

							$where = array('total_post_visitor_count.organize_id' => $userId, 'total_post_visitor_count.visitor_id !=' => $userId);
							if (!empty($data['select_category']) || !empty($data['from_date']) || !empty($data['to_date'])) {
								 
							   if(!empty($data['select_category']))   $where['total_post_visitor_count.page_type'] 	= $data['select_category'];
                               if(!empty($data['from_date']))   	  $where['total_post_visitor_count.date >= '] 	= $data['from_date'];
							   if(!empty($data['to_date']))           $where['total_post_visitor_count.date <= '] 	= $data['to_date'];


							     $data['filterResult'] 			      = $this->M_visitor_count->visitorSort($where);				
							}


							$data['page_title']  	   = self::Title;
							$month = array('01','02','03','04','05','06','07','08','09','10','11','12');
							 $tatalYearResult = 0;          
							 foreach($month as $m){
							  $startDate 			   = date("Y-m-d", strtotime(date("Y-$m-01")));
							  $endDate 	 			   = date("Y-m-t", strtotime(date("Y-$m-01")));
							
							  $where4['date >=']       = $startDate;
		                      $where4['date <=']       = $endDate;
		                      $where4['organize_id']   = $userId;
							  $totalCurMonthVisit	   = $this->M_visitor_count->findAllBycurrentMonth($where4);
                              $tatalYearResult 		   = $tatalYearResult + $totalCurMonthVisit;
							 
							}

							$data['totalYearVisit'] = $tatalYearResult;


							date_default_timezone_set('Australia/Brisbane');
							$first_day_of_the_week = 'Saturday'; 
							$start_of_the_week     = strtotime("Last $first_day_of_the_week");
							
							if ( strtolower(date('l')) === strtolower($first_day_of_the_week) )
							
							{
							
								$start_of_the_week = strtotime('today');
							
							}
							
							$end_of_the_week = $start_of_the_week + (60 * 60 * 24 * 7) - 1;
							
							$date_format =  'Y-m-d';
							
						    $satrtWeekDate 				= date($date_format, $start_of_the_week);
						    $endWeekDate 				= date($date_format, $end_of_the_week);
							$startDate 					= date("Y-m-d", strtotime(date("Y-m-01")));
	                        $endDate 	 	        	= date("Y-m-t", strtotime(date("Y-m-01"))); 
	                        $wherem['date >=']           = $startDate;
	                        $wherem['date <=']           = $endDate;
	                        $wherem['organize_id']       = $userId;
	                        $whereWeek['date >=']       = $satrtWeekDate;
	                        $whereWeek['date <=']       = $endWeekDate;
	                        $whereWeek['organize_id']   = $userId;
							$data['totalVisitToday']	= $this->M_visitor_count->findAllBycurrentDate($userId);
							$data['totalCurWeekVisit']	= $this->M_visitor_count->findAllBycurrentWeek($whereWeek);
							$data['totalCurMonthVisit']	= $this->M_visitor_count->findAllBycurrentMonth($wherem);
							$data['allRegion']	        = $this->M_region_manage->findAll();
							$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();	
							$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
							$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
							$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
							$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
							$data['panelMenuInfo']      = $this->M_organize_profile_manage->findAllPanelMenu('panel_profile_menu_manage');
							$data['UserWiseProgInfo']	= $this->M_programe_manage->findByUser($userId);
							$data['UserWiseAddInfo']	= $this->M_add_manage->findAllByUser($userId);
							
							$this->load->view('organizationUserPanel/filterVisitorPage', $data);
						
						}
					}



					public function visitorSmsSendAction()
					{ 		
						 $sendTo 		 	= $this->input->post('sendTo');
						 $title 	 		= $this->input->post('title2');
						 $msg 	 		 	= $this->input->post('msg');
						 $select_category 	= $this->input->post('select_category');
						 $countId 	 	 	= $this->input->post('count_id');
						 $filter 	 		= $this->input->post('filter');


						 if(isAuthenticate()){
							$user_id            = getUserName();
							$userDetails   		= $this->M_all_user_registration->findByUserName($user_id);
							$userId             = $userDetails->id;
							$senderName         = $userDetails->name;

						}

                        $where = array('organize_id' => $userId);
                        $where['visitor_id !=']       = "unknown";
                        if(!empty($select_category)) $where['page_type']     = $select_category;


						if(!empty($select_category)){

							if(!empty($filter)){
								if($sendTo == 'all'){
								 	$allTotalVisitorBySortData    	= $this->M_visitor_count->findByOrganizeId($where);

			                       	foreach($allTotalVisitorBySortData as $v){
									  $data['send_to_user_id']		= $v->visitor_id;
									  $data['count_id']				= $v->id;
									  $data['page_type']			= $v->page_type;
									  $data['title']				= $title;
									  $data['message']				= $msg;
									  $data['send_from_user_id']	= $userId;
									  $data['date_time']			= date("Y-m-d H:i:s");
									  $data['date']					= date('Y-m-y');
									  $this->M_organize_profile_manage->save('send_message_to_user', $data); 

									  	$name					   	= $senderName;
										$senderEmail	            = $user_id;
										$userEmail	                = $v->user_id;
										
										$config['protocol'] 		= 'sendmail';
										$config['mailpath'] 		= '/usr/sbin/sendmail';
										$config['charset'] 			= 'iso-8859-1';
										$config['wordwrap'] 		= TRUE;
										$config['mailtype'] 		='html';
										
										$this->email->initialize($config);
										
										
										$this->email->from($senderEmail, $name);
										$this->email->to($userEmail);			
										$this->email->subject($title);
										$this->email->message($msg);
										$this->email->reply_to($senderEmail);
										$this->email->send();

			                        }

							

								} else {
								    foreach($countId as $k => $count) {
								 	    if($countId[$k]){
											$data['count_id']			= $countId[$k];
											$data['title']				= $title;
											$data['message']			= $msg;
											$data['send_from_user_id']	= $userId;
											$data['date_time']			= date("Y-m-d H:i:s");
											$data['date']				= date('Y-m-y');
											$visitInfo	 	            = $this->M_organize_profile_manage->findById('total_post_visitor_count', $data['count_id']);
											$data['send_to_user_id']	= $visitInfo->visitor_id;
											$data['page_type']			= $visitInfo->page_type;

											$this->M_organize_profile_manage->save('send_message_to_user', $data);

											$userDetails 				=  $this->M_all_user_registration->findById($data['send_to_user_id']);
											$name					   	= $senderName;
											$senderEmail	            = $user_id;
											$userEmail	                = $userDetails->user_id;
											
											$config['protocol'] 		= 'sendmail';
											$config['mailpath'] 		= '/usr/sbin/sendmail';
											$config['charset'] 			= 'iso-8859-1';
											$config['wordwrap'] 		= TRUE;
											$config['mailtype'] 		='html';
											
											$this->email->initialize($config);
											
											
											$this->email->from($senderEmail, $name);
											$this->email->to($userEmail);			
											$this->email->subject($title);
											$this->email->message($msg);
											$this->email->reply_to($senderEmail);
											$this->email->send();


										}
								    }

								}


							} else {
	                          $allTotalVisitorBySortData2    = $this->M_visitor_count->findByOrganizeId($where);

		                       	foreach($allTotalVisitorBySortData2 as $v){
								  $data['send_to_user_id']		= $v->visitor_id;
								  $data['count_id']				= $v->id;
								  $data['page_type']			= $v->page_type;
								  $data['title']				= $title;
								  $data['message']				= $msg;
								  $data['send_from_user_id']	= $userId;
								  $data['date_time']			= date("Y-m-d H:i:s");
								  $data['date']					= date('Y-m-y');
								  $this->M_organize_profile_manage->save('send_message_to_user', $data);  

								  	$name					   	= $senderName;
									$senderEmail	            = $user_id;
									$userEmail	                = $v->user_id;
									
									$config['protocol'] 		= 'sendmail';
									$config['mailpath'] 		= '/usr/sbin/sendmail';
									$config['charset'] 			= 'iso-8859-1';
									$config['wordwrap'] 		= TRUE;
									$config['mailtype'] 		='html';
									
									$this->email->initialize($config);
									
									
									$this->email->from($senderEmail, $name);
									$this->email->to($userEmail);			
									$this->email->subject($title);
									$this->email->message($msg);
									$this->email->reply_to($senderEmail);
									$this->email->send(); 
								 
		                        }
	                        }

						} else {


							if($sendTo == 'selected'){
							
								foreach($countId as $k => $count) {
								 	if($countId[$k]){
										$data['count_id']			= $countId[$k];
										$data['title']				= $title;
										$data['message']			= $msg;
										$data['send_from_user_id']	= $userId;
										$data['date_time']			= date("Y-m-d H:i:s");
										$data['date']				= date('Y-m-y');
										$visitInfo	 	            = $this->M_organize_profile_manage->findById('total_post_visitor_count', $data['count_id']);
										$data['send_to_user_id']	= $visitInfo->visitor_id;
										$data['page_type']			= $visitInfo->page_type;
										$this->M_organize_profile_manage->save('send_message_to_user', $data);
										$userDetails 				=  $this->M_all_user_registration->findById($data['send_to_user_id']);


										$name					   	= $senderName;
										$senderEmail	            = $user_id;
										$userEmail	                = $userDetails->user_id;
										
										$config['protocol'] 		= 'sendmail';
										$config['mailpath'] 		= '/usr/sbin/sendmail';
										$config['charset'] 			= 'iso-8859-1';
										$config['wordwrap'] 		= TRUE;
										$config['mailtype'] 		='html';
										
										$this->email->initialize($config);
										
										
										$this->email->from($senderEmail, $name);
										$this->email->to($userEmail);			
										$this->email->subject($title);
										$this->email->message($msg);
										$this->email->reply_to($senderEmail);
										$this->email->send();
									}
								}

							} else {
	                           $allTotalVisitorData    = $this->M_visitor_count->findByOrganizeId($where);

		                        foreach($allTotalVisitorData as $v){
								  $data['send_to_user_id']		= $v->visitor_id;
								  $data['count_id']				= $v->id;
								  $data['page_type']			= $v->page_type;
								  $data['title']				= $title;
								  $data['message']				= $msg;
								  $data['send_from_user_id']	= $userId;
								  $data['date_time']			= date("Y-m-d H:i:s");
								  $data['date']					= date('Y-m-y');
								  $this->M_organize_profile_manage->save('send_message_to_user', $data);  


								  	$name					   	= $senderName;
									$senderEmail	            = $user_id;
									$userEmail	                = $v->user_id;
									
									$config['protocol'] 		= 'sendmail';
									$config['mailpath'] 		= '/usr/sbin/sendmail';
									$config['charset'] 			= 'iso-8859-1';
									$config['wordwrap'] 		= TRUE;
									$config['mailtype'] 		='html';
									
									$this->email->initialize($config);
									
									
									$this->email->from($senderEmail, $name);
									$this->email->to($userEmail);			
									$this->email->subject($title);
									$this->email->message($msg);
									$this->email->reply_to($senderEmail);
									$this->email->send();


								 

		                        }

						    }
							
                        }

					}




					public function allMessageViewDetails($countId)
					{
						
						  $data['countId']            	= $countId;
						  $data['msgWiseDetails']   	= $this->M_organize_profile_manage->findByVisitorCountId('send_message_to_user', $countId); 
						  //$data['msgWiseRep']   	    = $this->M_organize_profile_manage->findByReply('message_reply', $msgViewId); 


			              $this->load->view('organizationUserPanel/allMsgDetailsViewPage', $data);
							
					}

					public function msgDeleteAction()
					{
						
						$deleteId            	        = $this->input->post('deleteId');
						$this->M_organize_profile_manage->destroy('send_message_to_user', $deleteId); 
							
					}




					public function messageReplyAction()
					{
						if(isAuthenticate()){
			               $user_id             		= getUserName();
			               $userDetails   		        = $this->M_all_user_registration->findByUserName($user_id);
						}

						   $data['message_id']   		= $this->input->post('replyId');
						   $data['rep_user_id']   		= $userDetails->id;
						   $data['reply']   			= $this->input->post('reply');
						   $data['date']   				= date('Y-m-d');

						   $this->M_organize_profile_manage->save('message_reply', $data);

					}



					public function replyListView($msgViewId)
	
					{	
						$data['page_title']  		= self::Title;	
						$data['msgWiseRep']   	    = $this->M_organize_profile_manage->findByReply('message_reply', $msgViewId); 
						$data['msgViewId']			= $msgViewId;
						
						$this->load->view('organizationUserPanel/replyListPage', $data);
				
					}
					





				
				
				
				
					public function homePageSerialNo()
					{
						$positon 	      = $this->input->post('id');
						/*$positionWiseSl   = $this->M_add_manage->findSlNo($positon);				
					
						echo '<option value="">Sellect Serial No</option>';
						foreach($positionWiseSl as $v) {
							echo '<option value="'.$v->serial_no.'">'.$v->serial_no.'</option>';
						}*/

						  echo '<option value="">Sellect Serial No</option>';
						      if($positon == 'Top'){
								echo '<option value="1">1</option>';
								echo '<option value="2">2</option>';
                             }elseif($positon == 'Left'){
                              
								echo '<option value="1">1</option>';
								echo '<option value="2">2</option>';		
								echo '<option value="3">3</option>';	
								echo '<option value="4">4</option>';	
								echo '<option value="5">5</option>';	
								echo '<option value="6">6</option>';	
                                
							 }else{ 

							    echo '<option value="1">1</option>';
								echo '<option value="2">2</option>';		
								echo '<option value="3">3</option>';	
								echo '<option value="4">4</option>';	
								echo '<option value="5">5</option>';	
								echo '<option value="6">6</option>';
								echo '<option value="7">7</option>';
								echo '<option value="8">8</option>';		
								echo '<option value="9">9</option>';	
								echo '<option value="10">10</option>';	
								echo '<option value="11">11</option>';	
								echo '<option value="12">12</option>';
								echo '<option value="13">13</option>';	
								echo '<option value="14">14</option>';	
								echo '<option value="15">15</option>';	
								echo '<option value="16">16</option>';	
								echo '<option value="17">17</option>';
								echo '<option value="18">18</option>';	
								echo '<option value="19">19</option>';	
								echo '<option value="20">20</option>';				

							  }  					
					}



					public function adManageSerialNoTrigger()
					{
						$positon 	      = $this->input->post('positon');
						$slNo 	      	  = $this->input->post('slNo');
						$positionWiseSl   = $this->M_add_manage->findSlNo($positon);				
					
						echo '<option value="">Sellect Serial No</option>';
						foreach($positionWiseSl as $v) {
							if($slNo == $v->serial_no){
								echo '<option selected = "selected" value="'.$v->serial_no.'">'.$v->serial_no.'</option>';
							} else {
								echo '<option value="'.$v->serial_no.'">'.$v->serial_no.'</option>';
							}
							
						}
										
					}
					
					public function regionWisePosition()
					{
						$region 	      		= $this->input->post('id');
						$regionWisePosition   	= $this->M_add_manage->findByRegionWisePosition($region);				
					
						echo '<option value="">Sellect Position</option>';
						foreach($regionWisePosition as $v) {
							echo '<option value="'.$v->positon.'">'.$v->positon.'</option>';
						}
										
					}
						
				
				
					public function regionWiseSlNo()
					{
						$region 	      		= $this->input->post('region');
						$regionPositon 	      	= $this->input->post('regionPositon');
						$regionWiseSlNo   		= $this->M_add_manage->findByRegionWiseSl($region, $regionPositon);				
					
						echo '<option value="">Sellect Sl No</option>';
						foreach($regionWiseSlNo as $v) {
							echo '<option value="'.$v->serial_no.'">'.$v->serial_no.'</option>';
						}
										
					}  
						  
				
		   
	   
			 
				 public function addDelete($id)
				 {	
					
					$model					= $this->M_add_manage->findById($id);
					$only_image    			= $model->add_image;
					
					if(!empty($only_image)){
					unlink('./Images/Add_image/'.$only_image);
					}

					$where   				= array('booking_date >=' =>$model->publish_date, 'booking_date <=' =>$model->expire_date);
					$bookingValue			= $this->M_add_booking->findAllDeleteData($where);

					foreach ($bookingValue as $v){
				       $this->M_add_booking->destroy($v->add_id);
					}
				  	
					$this->M_add_manage->destroy($id);
					$this->M_search_table->destroyOPromotion($id);

					redirect('organizationUserHome/addManageFinal');
				 }
				 
				 public function postDelete($id)
				 {	
					
					$model					= $this->M_user_post_manage->findById($id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					unlink('./Images/Post_image/'.$only_image);
					unlink('./Images/Post_image500_347/'.$only_image);
					}
					$this->M_user_post_manage->destroy($id);
					$this->M_search_table->destroyByBlockId($id);				
					redirect('organizationUserHome/addPost');
				 }
				 

			     public function organizeNewsEventDelete($id)
				 {	
					
					$model					= $this->M_news_and_event_manage->findById($id);
					
					if(!empty($model->image) || !empty($model->video)){
					   unlink('./Images/News_image/'.$model->image);
					   unlink('./Images/News_image_resize/'.$model->image);
					   unlink('./Images/News_image/'.$model->video);
					   
					}
					$this->M_news_and_event_manage->destroy($id);
					redirect('organizationUserHome/newsEventManage');
				 }


				 public function intJobDelete($id)
				 {	
					$this->M_interested_job_post_manage->destroy($id);		
					redirect('organizationUserHome/interestedJobPost');
				 }
		   
				public function programeDelete($id)
				 {	
					$this->M_programe_manage->destroy($id);	
					$this->M_search_table->destroyByProId($id);		
					redirect('organizationUserHome/programeManage');
				 }
				 
				  public function userOldPassChk()

				{	
					$user_id             = getUserName();
					$password 		     = $this->input->post('password');
					$userOldPassChk       = $this->M_all_user_registration->userOldPassChk($user_id, $password);
					
					if(empty($userOldPassChk)){
						echo "1"; 
						} else {
						echo "0"; 
					 }
					
				} 
				
				
				 public function organizeSlideDelete($id)
				 {	
					
					$model					= $this->M_organize_profile_manage->findById('organize_slide_manage', $id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					  unlink('./Images/slide_image/'.$only_image);
					  unlink('./Images/slide_image_resize/'.$only_image);
					}
					$this->M_organize_profile_manage->destroy('organize_slide_manage', $id);		
					redirect('organizationUserHome/organizeSlide');
				 }
		   
		        public function organizePhotoDelete($id)
				 {	
					
					$model					= $this->M_organize_profile_manage->findById('organize_photo_gallery_manage', $id);
					$only_image    			= $model->image;
					
					if(!empty($only_image)){
					  unlink('./Images/photo_gallery/'.$only_image);
					  unlink('./Images/photo_gallery_resize/'.$only_image);
					}
					$this->M_organize_profile_manage->destroy('organize_photo_gallery_manage', $id);		
					redirect('organizationUserHome/organizeGallery');
				 }



		        public function gifDelete($id)
				 {	
					
					$delDetails					= $this->M_organize_profile_manage->findById('gif_image_manage', $id);
					
					if(!empty($delDetails->image1)){
					  unlink('./resource/gif_image/'.$delDetails->image1);
					}

					if(!empty($delDetails->image2)){
					  unlink('./resource/gif_image/'.$delDetails->image2);
					}
					if(!empty($delDetails->image3)){
					  unlink('./resource/gif_image/'.$delDetails->image3);
					}
					if(!empty($delDetails->image4)){
					  unlink('./resource/gif_image/'.$delDetails->image4);
					}


					$this->M_organize_profile_manage->destroy('gif_image_manage', $id);		
					redirect('organizationUserHome/gifImageCreate');
				 }

				 public function organizeVideoDelete($id)
				 {	
					
					$model	= $this->M_organize_profile_manage->findById('video_manage', $id);
					if(!empty($model->video)){
					  unlink('./resource/video_upload/'.$model->video);
					}
					$this->M_organize_profile_manage->destroy('video_manage', $id);		
					redirect('organizationUserHome/organizeVideoGallery');
				 }
		   
		   
				 
				 
			
				public function logout()
				{	
					logoutUser();
				}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */