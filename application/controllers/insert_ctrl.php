<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Registration extends MX_Controller {



		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_region_manage','M_country_manage','M_city_manage','M_all_user_registration',

		'M_menu_manage','M_submenu_manage','M_deeper_sub','M_organization_user_registration','M_more_campus_details','M_add_manage','M_general_user_registration','M_programe_manage','M_search_table','M_live_chat');

		static $helper	= array('url', 'userauthentication');

	

		const  Title	 = 'Next Admission';

		

	

		public function __construct(){

	

			parent::__construct();

	

			$this->load->database();

			$this->load->model(self::$model);

			$this->load->helper(self::$helper);

			$this->load->library('form_validation');

			$this->load->library('upload');

			$this->load->library('image_lib');

			$this->load->library('session');

			$this->load->library('email');

	

		}

	



		public function index()

	

		{	

		

		 if( isActiveUser() ) {

		      $userId  					= getUserName();

			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);

			  $data['activeUser']       = $userDetails->name;

			  $data['userType']       	= $userDetails->user_type;

			  $data['userDetails']      = $userDetails;

		   }

			

			$data['page_title']  		= self::Title;	

			$data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);

			$data['regionInfo']				= $this->M_region_manage->findAll();

			$data['countryInfo']			= $this->M_country_manage->findAllCountry();	

			$data['cityInfo']				= $this->M_city_manage->findAll();	

			$data['headerAddInfo']			= $this->M_advertisement_manage->findHeaderView();

			$data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();

			$data['rightAddInfo']	    	= $this->M_advertisement_manage->findRightView();

			$where                      	=  array('position_top' => 'Yes','status' => 'Available');

			$menuInfo			        	= $this->M_menu_manage->findAllDetailes($where);

			$data['menuInfo']				= $menuInfo;

			$data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();

		    $data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();

		    $data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();

		    $data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();

			

			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();

			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();

			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();

			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();

			

			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();

			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();

			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();

			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();

			

			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();

			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();

			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();

			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();

			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();

			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();

			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();

			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();

			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();

			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();

			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();

			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();

			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();

			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();

			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();

			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();

			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();

			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();

			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();

			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();

			

			$this->load->view('genRegistrationPage', $data);

	

		}

		

		public function genRegStore1()

		{	

			

				$data['country_id'] 			= $this->input->post('country_id');

				$data['user_type'] 				= "General user";

				$data['name'] 					= $this->input->post('name');

				$data['user_id'] 				= $this->input->post('user_id');

				$data['password'] 				= $this->input->post('password');

				$user_id 						= $this->input->post('user_id');

				$data['status'] 				= "In Active";

				$regionDetail 					= $this->M_country_manage->findById($data['country_id']);

				$data['region_id'] 				= $regionDetail->region_id;

				

				$this->form_validation->set_rules('user_id', 'Email', 'required');

				$this->form_validation->set_rules('password', 'password', 'required');

				

				if($this->form_validation->run() == FALSE) {

				$this->index();

				

				} else {

				

				if($data['user_id']) {

			           $emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);

						if(!empty($emil_chk)){

						$this->load->view('email_alert');

					 }else{

					 

						$userData	= array('lastId' => $user_id);

						$this->session->set_userdata($userData);

					

						$gen_id = $this->M_all_user_registration->save($data);



						$orgazeDetails 						= $this->M_all_user_registration->findById($gen_id);

						$countryDetail 						= $this->M_country_manage->findById($orgazeDetails->country_id);



						$datas['res_user_id'] 				= $gen_id;

						$datas['user_type'] 				= $orgazeDetails->user_type;

						$datas['name'] 						= $orgazeDetails->name;

						$datas['region_id'] 				= $regionDetail->region_id;

						$datas['country_id'] 				= $data['country_id'];

						$datas['country_university'] 		= $countryDetail->country_name ." university";

						



						$this->M_search_table->save($datas);

						

					}

					

					  	$title 							= "Verify Account";

						$message = ' Hello and welcome ' . $data['name'];

						$message .= "\r\n";

						$message .= 'Thank you for registering with Nextadmission Services.';

						$message .= "\r\n";

						$message .= '<a href="'.site_url('registration/emailVerification/'.$gen_id).'">.Your Account need to verify go to the link for verify.</a>';

						$name					   	 	= "Next Admission";

						$senderEmail	            	= "info@nextadmission.com";

						$userEmail	                	= $user_id;

						

						$config['protocol'] 			= 'sendmail';

						$config['mailpath'] 			= '/usr/sbin/sendmail';

						$config['charset'] 				= 'iso-8859-1';

						$config['wordwrap'] 			= TRUE;

						$config['mailtype'] 			='html';

						

						

						$this->email->initialize($config);

						

						

						$this->email->from($senderEmail, $name);

						$this->email->to($userEmail);			

						$this->email->subject($title);

						$this->email->message($message);

						$this->email->reply_to($senderEmail);

						$this->email->send();

						

					  redirect('registration/orgRegistrationSuccess'); 

					}

				  }

			   }

			   

			   public function registrationSuccess()

	

				{	

				

				 if( isActiveUser() ) {

					  $userId  					= getUserName();

					  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);

					  $data['activeUser']       = $userDetails->name;

					  $data['userType']       	= $userDetails->user_type;

					  $data['userDetails']      = $userDetails;

				   }

					

					$data['page_title']  			= self::Title;

			        $data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);

					$currentUser                    = $this->session->userdata('lastId');

					$data['currentUserInfo']	    = $this->M_all_user_registration->findByCurrentGenUser($currentUser);	

					$data['regionInfo']				= $this->M_region_manage->findAll();

					$data['countryInfo']			= $this->M_country_manage->findAll();	

					$data['cityInfo']				= $this->M_city_manage->findAll();	

					$data['headerAddInfo']			= $this->M_advertisement_manage->findHeaderView();

					$data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();

					$data['rightAddInfo']	    	= $this->M_advertisement_manage->findRightView();

					$where                      	=  array('position_top' => 'Yes','status' => 'Available');

					$menuInfo			        	= $this->M_menu_manage->findAllDetailes($where);

					$data['menuInfo']				= $menuInfo;

					$data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();

					$data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();

					$data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();

					$data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();

					

					$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();

					$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();

					$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();

					$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();

					

					$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();

					$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();

					$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();

					$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();

					

					$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();

					$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();

					$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();

					$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();

					$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();

					$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();

					$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();

					$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();

					$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();

					$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();

					$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();

					$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();

					$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();

					$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();

					$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();

					$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();

					$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();

					$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();

					$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();

					$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();

					

					$this->load->view('registrationSuccessPage', $data);

			

				}

				

				

		 public function organizeRegistration()

	

		{	

			if( isActiveUser() ) {

		      $userId  					= getUserName();

			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);

			  $data['activeUser']       = $userDetails->name;

			  $data['userType']       	= $userDetails->user_type;

			  $data['userDetails']      = $userDetails;

		   }

			$data['page_title']  		= self::Title;	

			$data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);

			$data['regionInfo']			= $this->M_region_manage->findAll();

			$data['countryInfo']		= $this->M_country_manage->findAllCountry();	

			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();

			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();

			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();

			$data['progInfo']	    	= $this->M_programe_manage->findAll();

			$where                      =  array('position_top' => 'Yes','status' => 'Available');

			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);

			$data['menuInfo']			= $menuInfo;

			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();

		    $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();

		    $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();

		    $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

			

			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();

			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();

			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();

			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();

			

			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();

			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();

			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();

			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();

			

			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();

			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();

			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();

			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();

			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();

			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();

			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();

			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();

			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();

			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();

			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();

			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();

			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();

			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();

			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();

			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();

			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();

			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();

			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();

			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();

			

			$this->load->view('organizeRegistrationPage', $data);

	

		}

		

		

		

		public function orgRegStore()

		 {	

				

			$data['account_name'] 	    	= $this->input->post('account_name');

			$data['name'] 	    			= $this->input->post('organizationname');

			$data['user_id'] 				= $this->input->post('user_id');

			$user_id 						= $this->input->post('user_id');

			$data['password'] 				= $this->input->post('password');

			$data['user_type'] 				= "Organization User";

			$data['status'] 				= "In Active";

			$data['country_id'] 			= $this->input->post('country_id_org');

			$regionDetail 					= $this->M_country_manage->findById($data['country_id']);

			$data['region_id'] 				= $regionDetail->region_id;

			

			

			

			$this->form_validation->set_rules('user_id', 'Email', 'required');

			$this->form_validation->set_rules('password', 'password', 'required');

			$this->form_validation->set_rules('account_name', 'Account Name', 'required');

			if($this->form_validation->run() == FALSE) {

			$this->index();

			

			} else {

			 

			$userData	= array('orgLastId' => $user_id);

			$this->session->set_userdata($userData);

		

			$orgnize_id = $this->M_all_user_registration->save($data);



			$orgazeDetails 					= $this->M_all_user_registration->findById($orgnize_id);

			$countryDetail 					= $this->M_country_manage->findById($orgazeDetails->country_id);



			$datas['res_user_id'] 			= $orgnize_id;

			$datas['user_type'] 			= $orgazeDetails->user_type;

			$datas['name'] 					= $orgazeDetails->name;

			$datas['region_id'] 			= $regionDetail->region_id;

			$datas['country_id'] 			= $data['country_id'];

			$datas['country_university'] 	= $countryDetail->country_name ." university";





			$this->M_search_table->save($datas);

			

			/*$data3['user_id'] 				= $orgnize_id;

			$data3['address'] 				= $this->input->post('address');

			$data3['phone_com'] 			= $this->input->post('phone_com2');

			$data3['mobile_com'] 			= $this->input->post('mobile_com2');

			$data3['email_com'] 			= $this->input->post('email_com');

			$data3['location_by_google'] 	= $this->input->post('location');

			$data3['founded'] 				= $this->input->post('founded');

			$data3['any_more_campus'] 		= $this->input->post('any_more_campus');

			$any_more_campus 				= $this->input->post('any_more_campus');

			

			$this->M_organization_user_registration->save($data3);

			*/

			

				

		   /* if($any_more_campus == 'Yes'){

			

				$data2['organization_id'] 		= $orgnize_id;

				$data2['organization_name'] 	= $this->input->post('campus_name');

				$data2['address'] 				= $this->input->post('address2');

				$data2['city'] 					= $this->input->post('city_id2');

				$data2['country'] 				= $this->input->post('country_id2');

				$data2['phone_com'] 			= $this->input->post('phone_com1');

				$data2['mobile_com'] 			= $this->input->post('mobile_com1');

				$data2['email_com'] 			= $this->input->post('email_com');

				$data2['location_by_google'] 	= $this->input->post('location_by_google');

				$data2['founded'] 				= $this->input->post('founded2');

				$this->M_more_campus_details->save($data2);

			

			  }

			   */

			  

			  	$title 							= "Verify Account";

				$message  = ' Hello and welcome ' . $data['name'];

				$message .= "\r\n";

				$message .= 'Thank you for registering with Nextadmission Services.';

				$message .= "\r\n";

				$message .= '<a href="'.site_url('registration/emailVerification/'.$orgnize_id).'">.Your Account need to verify go to the link for verify.</a>';

				$name					   	 	= "Next Admission";

				$senderEmail	            	= "info@nextadmission.com";

				$userEmail	                	= $user_id;

				

				$config['protocol'] 			= 'sendmail';

				$config['mailpath'] 			= '/usr/sbin/sendmail';

				$config['charset'] 				= 'iso-8859-1';

				$config['wordwrap'] 			= TRUE;

				$config['mailtype'] 			='html';

				

				$this->email->initialize($config);

				

				

				$this->email->from($senderEmail, $name);

				$this->email->to($userEmail);			

				$this->email->subject($title);

				$this->email->message($message);

				$this->email->reply_to($senderEmail);

				$this->email->send();

					

	        

				

				 redirect('registration/orgRegistrationSuccess');  

		  }

	   }

			   

			    public function orgRegistrationSuccess()

	

				{	

				

				 if( isActiveUser() ) {

					  $userId  					= getUserName();

					  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);

					  $data['activeUser']       = $userDetails->name;

					  $data['userType']       	= $userDetails->user_type;

					  $data['userDetails']      = $userDetails;

				   }

					

					$data['page_title']  			= self::Title;

					$data['totalNewMsg']	    	= $this->M_live_chat->findAllNewMsg($userDetails->id);

					$currentUser                    = $this->session->userdata('orgLastId');

					$data['currentUserInfo']	    = $this->M_all_user_registration->findByCurrentOrgUser($currentUser);	

					$data['regionInfo']				= $this->M_region_manage->findAll();

					$data['countryInfo']			= $this->M_country_manage->findAll();	

					$data['cityInfo']				= $this->M_city_manage->findAll();	

					$data['headerAddInfo']			= $this->M_advertisement_manage->findHeaderView();

					$data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();

					$data['rightAddInfo']	    	= $this->M_advertisement_manage->findRightView();

					$where                      	=  array('position_top' => 'Yes','status' => 'Available');

					$menuInfo			        	= $this->M_menu_manage->findAllDetailes($where);

					$data['menuInfo']				= $menuInfo;

					$data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();

					$data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();

					$data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();

					$data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();

					

					$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();

					$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();

					$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();

					$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();

					

					$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();

					$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();

					$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();

					$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();

					

					$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();

					$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();

					$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();

					$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();

					$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();

					$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();

					$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();

					$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();

					$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();

					$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();

					$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();

					$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();

					$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();

					$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();

					$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();

					$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();

					$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();

					$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();

					$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();

					$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();

					

					$this->load->view('orgRegistrationSuccessPage', $data);

			

				}

				

				

				

				public function userEmailChk()

	

				{	

					$user_id 					  = $this->input->post('user_id');

					$userEmailRegChk     		  = $this->M_all_user_registration->findByEmail($user_id);

					

					if(!empty($userEmailRegChk)) {

						echo "1"; } else {

						echo "0"; }

					

				} 

				

				public function emailVerification($orgnize_id)

	

				{

					$data['status']               = "Active";

					$userDetails   		          = $this->M_all_user_registration->findById($orgnize_id);

					$userId                       = $userDetails->user_id;

					$userType                     = $userDetails->user_type;

					$userAccountUpdate     		  = $this->M_all_user_registration->updatStatus($data, $orgnize_id);

					$this->session->set_userdata(array('UserId' => $userId, 'userType' => $userType, 'updateText' => "Your account verify successfull"));

					

					if($userType =='Organization User'){

					  redirect('organizationUserHome'); 

					}else{

					   redirect('generalUserHome'); 

					}

					

					 

					

				} 

				

			

				 

		

		public function genForgotPaword()

	

			{

				if( isActiveUser() ) {

				  $userId  					= getUserName();

				  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);

				  $data['activeUser']       = $userDetails->name;

				  $data['userType']       	= $userDetails->user_type;

				  $data['userDetails']      = $userDetails;

				 }		

				$data['page_title']  			= self::Title;	

			    $data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);

				$data['headerAddInfo']			= $this->M_advertisement_manage->findHeaderView();

				$data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();

				$where                      	=  array('position_top' => 'Yes','status' => 'Available');

				$menuInfo			        	= $this->M_menu_manage->findAllDetailes($where);

				$data['menuInfo']				= $menuInfo;

				$data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();

				$data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();

				$data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();

				$data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();

				

				$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();

				$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();

				$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();

				$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();

				

				

				$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();

				$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();

				$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();

				$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();

				$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();

				$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();

				$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();

				$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();

				$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();

				$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();

				$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();

				$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();

				$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();

				$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();

				$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();

				$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();

			

				$this->load->view('forgotPassPage', $data);

		

			}

			

			

		

		 public function sendMail()

	

			{		

				if( isActiveUser() ) {

					  $userId  					= getUserName();

					  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);

					  $data['activeUser']       = $userDetails->name;

					  $data['userType']       	= $userDetails->user_type;

					  $data['userDetails']      = $userDetails;

				  }

				

				$data['page_title']  			= self::Title;

				$data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);

				$data['headerAddInfo']			= $this->M_advertisement_manage->findHeaderView();

				$data['headerBasicInfo']		= $this->M_basic_manage->findHeaderView();

				$where                      	=  array('position_top' => 'Yes','status' => 'Available');

				$menuInfo			        	= $this->M_menu_manage->findAllDetailes($where);

				$data['menuInfo']				= $menuInfo;

				$data['user_id'] 				= $this->input->post('user_id');

				$forGotInfo			            = $this->M_all_user_registration->findByEmail($data['user_id']);

				$sendEmail                      = $forGotInfo->user_id;

				$password                      	= $forGotInfo->password;

				$data['forGotInfo']			    = $this->M_all_user_registration->findByEmail($data['user_id']);

				$data['footerMenuInfo1']		= $this->M_menu_manage->findAllFooterMenu1();

				$data['footerMenuInfo2']		= $this->M_menu_manage->findAllFooterMenu2();

				$data['footerMenuInfo3']		= $this->M_menu_manage->findAllFooterMenu3();

				$data['footerMenuInfo4']		= $this->M_menu_manage->findAllFooterMenu4();	

				

				$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();

				$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();

				$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();

				$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();

				

				

				$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();

				$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();

				$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();

				$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();

				$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();

				$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();

				$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();

				$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();

				$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();

				$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();

				$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();

				$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();

				$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();

				$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();

				$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();

				$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();

			

				    

			if(!empty($forGotInfo)){

				$name					    = "Next Admission";

				$senderEmail	            = "info@nextadmission.com";

				

				$config['protocol'] = 'sendmail';

				$config['mailpath'] = '/usr/sbin/sendmail';

				$config['charset'] = 'iso-8859-1';

				$config['wordwrap'] = TRUE;

				

				$this->email->initialize($config);

				

				

				$this->email->from($senderEmail, $name);

				$this->email->to($sendEmail);			

				$this->email->subject("Your Password");

				$this->email->message($password);

				

				$this->email->reply_to($senderEmail);

				

				 if($this->email->send())

					 {

					 $this->load->view('forGotInfoViewPage', $data);

					 }

					 else

					{

					  show_error($this->email->print_debugger());

					} 

			 

				}

				 

			}

			

			

		  

		  public function countryName()

			{

				$region_id 	   = $this->input->post('id');

				

				//$where 			   = array('region_id' => $region_id);		

				$countryList   	   = $this->M_country_manage->findAllInfo($region_id);				

			

				echo '<option value="">Sellect Country Name</option>';

				foreach($countryList as $v) {

					echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-country-code-phon="'.$v->country_code_phon.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';

				}

				

			}

			public function countryName2()

			{

				$region_id 	   = $this->input->post('id');

				

				//$where 			   = array('region_id' => $region_id);		

				$countryList   	   = $this->M_country_manage->findAllInfo($region_id);				

			

				echo '<option value="">Sellect Country Name</option>';

				foreach($countryList as $v) {

					echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-country-code-phon="'.$v->country_code_phon.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';

				}

				

			}

			

			public function cityName()

			{

				$country_id 	      = $this->input->post('id');

				

				//$where 			   	  = array('country_id' => $country_id);		

			 	$countryWiseCityList   = $this->M_city_manage->findAllCity($country_id);				

			

				echo '<option value="">Sellect City Name</option>';

				foreach($countryWiseCityList as $v) {

					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';

				}

								

			}

			

			public function cityName2()

			{

				$country_id 	      = $this->input->post('id');

				

				//$where 			   	  = array('country_id' => $country_id);		

			 	$countryWiseCityList   = $this->M_city_manage->findAllCity($country_id);				

			

				echo '<option value="">Sellect City Name</option>';

				foreach($countryWiseCityList as $v) {

					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';

				}

								

			}

			

			public function chkUserId()

			{

				$userId 	      = $this->input->post('userId');

				$emil_chk = $this->M_all_user_registration->findByEmail($userId);

				

				if(!empty($emil_chk)){

				  echo "1";

				}			

								

			}

			

			public function nextStep()

			{

				$type_of_organization  = $this->input->post('id');

				

				if($type_of_organization == 'EducationProvider'){

					$optionValu                  = 'School /College upto  12';

			

				    echo '<option value="">Sellect Type of Education provider</option>';

				    echo '<option value="'.$optionValu.'">'.$optionValu.'</option>';

					echo '<option value="University">University</option>';

					

			    } else if($type_of_organization == 'Agency'){

				    $optionValu1                  = 'Travel Agent ( Air Ticket)';

					$optionValu2                  = 'Education Tour Agent';

					$optionValu3                  = 'Package Tour Agent';

					$optionValu4                  = 'Visa process';

					$optionValu5                  = 'Hotel Booking';

					$optionValu6                  = 'Job Agent';

			

				    echo '<option value="">Sellect Agency Type</option>';

				    echo '<option value="'.$optionValu1.'">'.$optionValu1.'</option>';

					echo '<option value="'.$optionValu2.'">'.$optionValu2.'</option>';

					echo '<option value="'.$optionValu3.'">'.$optionValu3.'</option>';

					echo '<option value="'.$optionValu4.'">'.$optionValu4.'</option>';

					echo '<option value="'.$optionValu5.'">'.$optionValu5.'</option>';

					echo '<option value="'.$optionValu6.'">'.$optionValu6.'</option>';

					echo '<option value="Other">Other</option>';

				  

				} else if($type_of_organization == 'CouchingCentre'){

				    $optionValu1                  = 'College Admission';

					$optionValu2                  = 'Medical Admission';

					$optionValu3                  = 'University Admission';

					$optionValu4                  = 'English Training : IELTS-GRE-GMET etc';

					$optionValu5                  = 'Foreign Language : Germany-France-Courier etc';

					$optionValu6                  = 'Professional';

					$optionValu7                  = 'Tutor Services';

			

				    echo '<option value="">Sellect Type Of Couching Centre</option>';

				    echo '<option value="'.$optionValu1.'">'.$optionValu1.'</option>';

					echo '<option value="'.$optionValu2.'">'.$optionValu2.'</option>';

					echo '<option value="'.$optionValu3.'">'.$optionValu3.'</option>';

					echo '<option value="'.$optionValu4.'">'.$optionValu4.'</option>';

					echo '<option value="'.$optionValu5.'">'.$optionValu5.'</option>';

					echo '<option value="'.$optionValu6.'">'.$optionValu6.'</option>';

					echo '<option value="'.$optionValu7.'">'.$optionValu7.'</option>';

					

					} else if($type_of_organization == 'ProfessionalTrainingCentre'){

				    $optionValu1                  = 'Government Job';

					$optionValu2                  = 'BCS Training';

					$optionValu3                  = 'Marine Training';

					$optionValu4                  = 'Defence Training';

			

				    echo '<option value="">Sellect Type Of Professional Training</option>';

				    echo '<option value="'.$optionValu1.'">'.$optionValu1.'</option>';

					echo '<option value="'.$optionValu2.'">'.$optionValu2.'</option>';

					echo '<option value="'.$optionValu3.'">'.$optionValu3.'</option>';

					echo '<option value="'.$optionValu4.'">'.$optionValu4.'</option>';

					

					}

				

								

			}

			

			public function nextStep2()

			{

				$next_step2  = $this->input->post('id');

				

				$optionValu1                  = 'KG-8 years Class';

				$optionValu2                  = 'K-10 years Class';

				$optionValu3                  = '9-10 years Class';

				$optionValu4                  = '10-12 years Class';

				$optionValu5                  = 'Bachelor';

				$optionValu6                  = 'Masters';

				

				echo '<option value="">Sellect School /College</option>';

				echo '<option value="'.$optionValu1.'">'.$optionValu1.'</option>';

				echo '<option value="'.$optionValu2.'">'.$optionValu2.'</option>';

				echo '<option value="'.$optionValu3.'">'.$optionValu3.'</option>';

				echo '<option value="'.$optionValu4.'">'.$optionValu4.'</option>';

				echo '<option value="'.$optionValu5.'">'.$optionValu5.'</option>';

				echo '<option value="'.$optionValu6.'">'.$optionValu6.'</option>';

				echo '<option value="'.Other.'">'.Other.'</option>';

				

								

			}

			

			public function logout()

		

			{	

		

				logoutUser();

		

			}



	



}



