<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Form extends MX_Controller {



		static $model 	 = array('M_organize_profile_manage');

		static $helper   = array('url','generalauthentication','userauthentication');

	

		const  Title	 = 'Next Admission';

		

	

		public function __construct(){

	

			parent::__construct();

	

			$this->load->database();

			$this->load->model(self::$model);

			$this->load->helper(self::$helper);

			$this->load->library('form_validation');

			$this->load->library('upload');

			$this->load->library('image_lib');

			$this->load->library('session');

			//$this->load->helper(array('url','pdf'));

		}

	



		public function index()	

		{		

			$data['alert']  = $this->session->userdata('alert');

			$this->session->set_userdata(array('alert' => ""));

			

			

			$id = 1;

			$data['notices']		    = $this->M_organize_profile_manage->findById('notice_update', $id);

			

			

			

			$where = array('id' => '13');

			$data['formslide1']	= $this->M_organize_profile_manage->admisionforad('adminssion_form_slide', $where);



			$where = array('id' => '12');

			$data['formslide2']	= $this->M_organize_profile_manage->admisionforad('adminssion_form_slide', $where);

			

			$where = array('id' => '11');

			$data['formslide3']	= $this->M_organize_profile_manage->admisionforad('adminssion_form_slide', $where);

			

			

			$where = array('id' => '10');

			$data['formslide4']	= $this->M_organize_profile_manage->admisionforad('adminssion_form_slide', $where);



			

			

	

		

			$this->load->view('formPage', $data);

	

		}



		public function admission(){



			$this->load->view('admissionForm');

		}



		public function mobile_check(){

				$mobile_no	= $this->input->post('mobile_no');

				$result 	= $this->M_organize_profile_manage->mobile_check('student_form', $mobile_no);

				if($result){

					echo true;

				}else{

					echo false;

				}

				

		}



		public function store()

			{	

				$data['name'] 				= $this->input->post('name');

				$data['mobile_no']			= $this->input->post('mobile');

				$data['interested_country'] = $this->input->post('country');

				$data['purposes']			= $this->input->post('purpose');

				$data['interested_course'] 	= $this->input->post('course');

				$data['city']				= $this->input->post('city');

				$data['dhaka_dist']				= $this->input->post('dhaka_dist');

				$data['chittagong_dist']				= $this->input->post('chittagong_dist');

				$data['sylhet_dist']				= $this->input->post('sylhet_dist');

				$data['rangpur_dist']				= $this->input->post('rangpur_dist');

				$data['rajshahi_dist']				= $this->input->post('rajshahi_dist');

				$data['khulna_dist']				= $this->input->post('khulna_dist');

				$data['barisal_dist']				= $this->input->post('barisal_dist');

				$data['mymensingh_dist']				= $this->input->post('mymensingh_dist');

				$data['edu_qualification'] 	= $this->input->post('education');

				//$data['fil_up_date']		= date('Y-m-d H:i:s');

				$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));

				$data['fil_up_date']  = $dt->format('Y-m-d g:i:s');

				$this->session->set_userdata(array('alert' =>"<h2 align='center' style='color:#00a9e7'>Your data has been submitted successfully!</h2>"));

				

			    $result = $this->M_organize_profile_manage->save('student_form', $data);

				if($result){

			    	redirect('form/success');

				}

				else{

					redirect('form/failure');

				}

			}

			

		public function success (){

			$this->load->view('success');

		}

		

		public function failure (){

			$this->load->view('failure');

		}

		

		public function view(){


			$data['viewdata']       = $this->M_organize_profile_manage->view();

			$data['logInUser']      = $this->session->userdata('name');

		//	$data['viewcountry']    = $this->M_organize_profile_manage->inted_country();

			//print_r($data['viewcountry'] );


			if(!empty($data['logInUser'])){

			  $this->load->view('view_form_data',$data);	

			}else {

			  redirect('form/panel');	

			}			

		}





		public function filter_view($id){

			$data['viewcountry']      = $this->M_organize_profile_manage->view();

			$data['logInUser']      = $this->session->userdata('name');

			//$data['viewcountry']    = $this->M_organize_profile_manage->inted_country();

			


			$this->data['edit_data']= $this->M_organize_profile_manage->inted_country($id);

			

    		$this->load->view('view_form_data',  $this->data,FALSE);


    		echo "<pre>";

			print_r($this->data);


			if(!empty($data['logInUser'])){

			  $this->load->view('view_form_data',$data);	

			}else {

			  redirect('form/panel');	

			}	

			
		    //print_r($data['records']); //All 
		   // echo $data['records']['interested_country']; // Field name full_name

/*


			$data['viewdata']       = $this->M_organize_profile_manage->view_search();

			$data['logInUser']      = $this->session->userdata('name');



			if(!empty($data['logInUser'])){

			  $this->load->view('view_form_data',$data);	

			}else {

			  redirect('form/panel');	

			}		*/

		}



		/*public function admission_panel(){

			$this->load->view('admission_panel');

		}



		public function admission_login(){

			$data['name']       	= $this->input->post('name');

		    $data['pass']      		= $this->input->post('pass');

		    $logInDetails  			= $this->M_organize_profile_manage->admission_login('data_admin', $data);

		    if($logInDetails){

		        $this->session->set_userdata($data);

		        redirect('form/admission_view'); 

		    }else{

		        $this->session->set_userdata(array('msg' => "You are not valid user"));

		        redirect('form/admission_panel');

			}

		}*/



		/*public function admission_view(){

			

			$data['viewdata']       = $this->M_organize_profile_manage->admission_view();



			if(!empty($this->session->userdata("name") and !empty($this->session->userdata("pass")))){

			  	$this->load->view('admission_view_form_data',$data);	

			}else {

			  redirect('form/admission_panel');	

			}			

		}*/





		public function panel(){

			$this->load->view('data_admin');

		}





		public function login(){

		      $name       = $this->input->post('name');

		      $pass       = $this->input->post('pass');

		      $logInDetails    = $this->M_organize_profile_manage->login('data_admin', $name);

		      if( !empty($logInDetails) && $logInDetails->pass == $pass){

		        $this->session->set_userdata(array('name' => $name));

		        redirect('form/view'); 

		      } else {

		         $this->session->set_userdata(array('msg' => "You are not valid user"));

		        redirect('form/panel');

		     }

		   }





		 /*  public function admission_logout (){

			$this->session->unset_userdata("name");

			$this->session->unset_userdata("pass");

		    redirect('form/admission_panel'); 

		  }*/

		  



		   public function logout (){

			 $this->session->set_userdata(array('name' => ""));

		     redirect('form/panel'); 

		  }

		  



		  /*public function admission_changepasword (){

			$data['logInUser']      = $this->session->userdata('name');

		     $this->load->view('admissionChangepasswordPage', $data);

		  }

*/

		 /* public function std_details ($id){

			$where = array('id' => $id);

			$data['stdData'] = $this->M_organize_profile_manage->std_details('online_admission_form', $where);*/

			

			//print_r($result['stdData']);

				//exit;



		/*	if (isset($data['stdData'])) {

				$this->load->view('admissionDetailedInfoPage', $data);

			}else

				redirect('form/admission_view');

		  }*/



		 /* public function std_details_download ($id){



			$where = array('id' => $id);

			$result = $this->M_organize_profile_manage->std_details('online_admission_form', $where);

			if ($result) {

				$html = $this->load->view("admissionDetailedInfoPage_pdf", '', true);

				

			}else

				redirect('form/admission_view');

		  }*/







		   public function changepasword (){

			$data['logInUser']      = $this->session->userdata('name');

		     $this->load->view('changepasswordPage', $data);

		  }







		/*public function admission_passwordupdate (){

		

			$logInUser      = $this->session->userdata('name');

			$data['pass']    = $this->input->post('newpassword');	

			$where = array('name' => $logInUser);

			$this->db->update('data_admin', $data, $where);

			

		   redirect('form/admission_view');

		  }*/

		  

		public function passwordupdate (){

		

			$logInUser      = $this->session->userdata('name');

			$data['pass']    = $this->input->post('newpassword');	

			$where = array('name' => $logInUser);

			$this->db->update('data_admin', $data, $where);

			

		   redirect('form/view');

		  }

		  

		  

		   public function formslide (){

			

			$data['logInUser']      = $this->session->userdata('name');

			

			$data['admissonformslide'] = $this->M_organize_profile_manage->findAll('adminssion_form_slide', 'id DESC');

			

		     $this->load->view('admissionformslidePage', $data);

		  }

		  

		  

		  

		  public function delete($id){

		  

			$result = $this->M_organize_profile_manage->findById('adminssion_form_slide', $id);

			

		  	if(!empty($result->sl_image1)){

					  unlink('./Images/admission_form_image/'.$result->sl_image1);

					}

					

				if(!empty($result->sl_image2)){

				  unlink('./Images/admission_form_image/'.$result->sl_image2);

				}

				if(!empty($result->sl_image3)){

				 unlink('./Images/admission_form_image/'.$result->sl_image3);

				}

				

				$this->M_organize_profile_manage->destroy('adminssion_form_slide', $id);

					

				redirect('form/formslide');	

					

		  

		  }

		  

		  

		  

		   public function formslidestore (){

			

			

			$data['sl_image1']      = $this->input->post('sl_image1');

			$data['sl_image2']      = $this->input->post('sl_image2');

			$data['sl_image3']      = $this->input->post('sl_image3');

			$this->db->insert('adminssion_form_slide', $data);

			

 			redirect('form/formslide');

			

		  }



		  

		  

		  

		 public function edit($id)

			{

				$data['logInUser']      = $this->session->userdata('name');

				$data['id']              = $id;

				$this->load->view('admissionformslideEditPage', $data);

			}

		  

		  public function admisUpdate()

		  {

		  

		   $id = $this->input->post('edit_id');

		   

		   $result = $this->M_organize_profile_manage->findById('adminssion_form_slide', $id);

		   

		   $sl_image1 = $this->input->post('sl_image1');

		   $sl_image2 = $this->input->post('sl_image2');

		   $sl_image3 = $this->input->post('sl_image3');

		   

		   if(!empty($sl_image1)){

		   	unlink('./Images/admission_form_image/'.$result->sl_image1);

			$data['sl_image1']	= $sl_image1;

		   }

		   if(!empty($sl_image2)){

		   unlink('./Images/admission_form_image/'.$result->sl_image2);

		   $data['sl_image2']	= $sl_image2;

		   }

		   

		   if(!empty($sl_image3)){

		    unlink('./Images/admission_form_image/'.$result->sl_image3);

			$data['sl_image3']	= $sl_image3;

		   }

		   

		   

		   $this->db->update('adminssion_form_slide', $data, array('id' => $id));

		   

		   redirect('form/formslide'); 

		   

		  }

		  

		  

		  

		  

		  public function notice(){

		  	$data['logInUser']      = $this->session->userdata('name');

			$id = 1;

			$data['notices']		    = $this->M_organize_profile_manage->findById('notice_update', $id);

			$this->load->view('admissionformsNoticePage', $data);

		  }

		  

		  

		  public function noticeUpdate()

		  {

		  		$id						= $this->input->post('edit_id');

		  		$data['text_detail']	= $this->input->post('text_detail');

				$this->db->update('notice_update', $data, array('id' => $id));

				

				redirect('form/notice');

		  }

		  

		  

		  

		  

		  

		  

		  

		  

		  public function chck()

		  {

		  	$oldpass = $this->input->post('oldpass');

			$where = array("pass" => $oldpass);

			$result = $this->M_organize_profile_manage->findorgallByUser('data_admin', $where);

			if($result){

				echo TRUE;

			}

			else {

				echo FALSE;

			}

			

		 }





}		



		  







