<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends MX_Controller {

		
		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub',
		'M_news_and_event_manage','M_user_post_manage','M_all_user_registration','M_post_comment','M_reply_comment','M_organization_user_registration',
		'M_add_manage','M_general_user_registration','M_programe_manage','M_job_post_manage','M_job_application_details', 'M_country_manage', 'M_organize_profile_manage','M_search_table','M_region_manage','M_visitor_count','M_live_chat');
		static $helper	= array('url', 'userauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
	
		}
	

		public function index($country_name)
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
			  $data['userDetails']      = $userDetails;
		   }
		   	$data['page_title']  		= self::Title;
		   	$data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);
		   	$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
		   	$data['country_name']  		= $country_name;	
		   	$countryInfo	    		= $this->M_country_manage->findCountryId($country_name);
		   	$country_id                 = $countryInfo->id;	
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllByCountry($country_name);
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganizeByCountry($country_id);
			$region                     = "Asia";
			$regionDetails   		    = $this->M_region_manage->findByRegionName($region);
			$regionId                   = $regionDetails->id;
			$data['asiaCountry']	    = $this->M_country_manage->findAllInfo($regionId);
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->countryTopAddInfo1($country_name);
			$data['topAddInfo2']	    = $this->M_add_manage->countryTopAddInfo2($country_name);
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->countryleftAddInfo1($country_name);
			$data['leftAddInfo2']	    = $this->M_add_manage->countryleftAddInfo2($country_name);
			$data['leftAddInfo3']	    = $this->M_add_manage->countryleftAddInfo3($country_name);
			$data['leftAddInfo4']	    = $this->M_add_manage->countryleftAddInfo4($country_name);
			$data['leftAddInfo5']	    = $this->M_add_manage->countryleftAddInfo3($country_name);
			$data['leftAddInfo6']	    = $this->M_add_manage->countryleftAddInfo4($country_name);
			
			$data['rightAddInfo1']	    = $this->M_add_manage->countryRightAddInfo1($country_name);
			$data['rightAddInfo2']	    = $this->M_add_manage->countryRightAddInfo2($country_name);
			$data['rightAddInfo3']	    = $this->M_add_manage->countryRightAddInfo3($country_name);
			$data['rightAddInfo4']	    = $this->M_add_manage->countryRightAddInfo4($country_name);
			$data['rightAddInfo5']	    = $this->M_add_manage->countryRightAddInfo5($country_name);
			$data['rightAddInfo6']	    = $this->M_add_manage->countryRightAddInfo6($country_name);
			$data['rightAddInfo7']	    = $this->M_add_manage->countryRightAddInfo7($country_name);
			$data['rightAddInfo8']	    = $this->M_add_manage->countryRightAddInfo8($country_name);
			$data['rightAddInfo9']	    = $this->M_add_manage->countryRightAddInfo9($country_name);
			$data['rightAddInfo10']	    = $this->M_add_manage->countryRightAddInfo10($country_name);
			$data['rightAddInfo11']	    = $this->M_add_manage->countryRightAddInfo11($country_name);
			$data['rightAddInfo12']	    = $this->M_add_manage->countryRightAddInfo12($country_name);
			$data['rightAddInfo13']	    = $this->M_add_manage->countryRightAddInfo13($country_name);
			$data['rightAddInfo14']	    = $this->M_add_manage->countryRightAddInfo14($country_name);
			$data['rightAddInfo15']	    = $this->M_add_manage->countryRightAddInfo15($country_name);
			$data['rightAddInfo16']	    = $this->M_add_manage->countryRightAddInfo16($country_name);
			$data['rightAddInfo17']	    = $this->M_add_manage->countryRightAddInfo17($country_name);
			$data['rightAddInfo18']	    = $this->M_add_manage->countryRightAddInfo18($country_name);
			$data['rightAddInfo19']	    = $this->M_add_manage->countryRightAddInfo19($country_name);
			$data['rightAddInfo20']		= $this->M_add_manage->countryRightAddInfo20($country_name);
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllCountryWiseInfo($country_name);
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();

			$regionList = $this->M_region_manage->findAll();
			foreach ($regionList as $v) {
				$v->countryList = $this->M_country_manage->findAllInfo($v->id);
			}
			$data['regionList'] = $regionList;


	     	$this->load->view('countryWiseDataPage', $data);
		}
		
	

}

