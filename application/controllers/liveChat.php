<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LiveChat extends MX_Controller {

		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub',
		'M_news_and_event_manage','M_user_post_manage','M_all_user_registration','M_post_comment','M_reply_comment','M_organization_user_registration',
		'M_add_manage','M_general_user_registration','M_programe_manage','M_job_post_manage','M_job_application_details', 'M_country_manage', 'M_organize_profile_manage','M_search_table','M_region_manage','M_visitor_count','M_live_chat');
		static $helper   = array('url','userauthentication','generalauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
	
		}
	

		public function index()
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['userAutoId']       = $userDetails->id;
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
			  $data['totalActiveUser']  = $this->M_all_user_registration->countAllActiveUser($userDetails->id);  
			  $data['allUser']   		= $this->M_all_user_registration->findAllLoginUser($userDetails->id); 

		   }
		   
		  						
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllHome();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$data['allProgramName']	    = $this->M_programe_manage->findAll();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->leftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->leftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
			$regionList = $this->M_region_manage->findAll();
			foreach ($regionList as $v) {
				$v->countryList = $this->M_country_manage->findAllInfo($v->id);
			}
			$data['regionList'] = $regionList;

			$this->load->view('liveChatPage', $data);
	
		}
		
		
		public function chatWiseDetail()
		{
			  if( isActiveUser() ) {
			      $userId  			= getUserName();
				  $userDetails      = $this->M_all_user_registration->findByUserName($userId);
		     }
			  $id   				= $this->input->post('id');
		      $allData              = $this->M_live_chat->findAllSmsById($id, $userDetails->id);
			  $allMsg   			= $this->M_all_user_registration->findById($id); 
			  $allMsg->sendUser     = $userDetails->id;
			  $allMsg->senderImg    = $userDetails->image;
			  $allMsg->countMsg     = $this->M_live_chat->countAllMsg($id, $userDetails->id);
			  $allMsg->msg   		= array_reverse($allData);
			  if(empty($allMsg->msg)){
			  	$allMsg->alert      = "nomsg";
			  } 
              echo json_encode($allMsg);
		}
		
		
		public function sendMessageAction()
		{
			 if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
		     }

		       date_default_timezone_set("Asia/Dhaka");
			   $data['send_to']   	= $this->input->post('id');
			   $data['send_from']   = $userDetails->id;
			   $data['date_time']   = date('Y-m-d H:i:s');
			   $data['msg_status']  = "1";
			   $data['message']   	= $this->input->post('message');

			   if(!empty($data['message'])){
			      $this->M_live_chat->save($data);
			   }

			   redirect('liveChat/msgListViewMain/'.$data['send_to']);
		}
		
		
		public function msgListViewMain($send_to)
		{
		  if( isActiveUser() ) {
		      $userId  			= getUserName();
			  $userDetails      = $this->M_all_user_registration->findByUserName($userId);
	      }
	      $data['userDetails']  = $userDetails;
		  $allMsg   			= $this->M_live_chat->findAllSmsById($send_to, $userDetails->id);
		  $data['allMsg']   	= array_reverse($allMsg);
          $this->load->view('allmsgViewPage', $data);
				
		}


		public function oldMsgView()
		{
		  if( isActiveUser() ) {
		      $userId  			= getUserName();
			  $userDetails      = $this->M_all_user_registration->findByUserName($userId);
	      }
	      $data['userDetails']  = $userDetails;
		  $sendToId   			= $this->input->post('sendToId');
		  $allMsg   			= $this->M_live_chat->findAllOldMsg($sendToId, $userDetails->id);
		  $data['allMsg']   	= array_reverse($allMsg);
          $this->load->view('oldMsgViewPage', $data);
				
		}


		public function searchUser()
		{
		  if( isActiveUser() ) {
		      $userId  			= getUserName();
			  $userDetails      = $this->M_all_user_registration->findByUserName($userId);
	      }
	      $data['user']  		= $this->input->post('user');
		  $data['allUser']   	= $this->M_all_user_registration->findAllSearchLiveUser($data['user'], $userDetails->name); 
		  
          $this->load->view('searhLiveUserViewPage', $data);
				
		}


		public function getMsgFromUser()
		{
		    if( isActiveUser() ) {
		      $userId  				= getUserName();
			  $userDetails      	= $this->M_all_user_registration->findByUserName($userId);
	        }
			  $sendTo   			= $this->input->post('activeUser');
			  $sendFromdetails   	= $this->M_live_chat->findBySenderId($sendTo); 

			  if(!empty($sendFromdetails)){
				  $alldata   			= $this->M_live_chat->getAllSmsById($sendFromdetails->send_from, $userDetails->id);
			      $allMsg   			= $this->M_all_user_registration->findById($sendFromdetails->send_from); 
				  $allMsg->sendUser     = $userDetails->id;
				  $allMsg->senderImg    = $userDetails->image;
				  $allMsg->msgId    	= $sendFromdetails->id;
			      $allMsg->countMsg     = $this->M_live_chat->countAllMsg($sendFromdetails->send_from, $userDetails->id);
				  $allMsg->allmsg   	= array_reverse($alldata);
				  if(empty($allMsg->allmsg)){
				  	$allMsg->alert      = "nomsg";
			      } 
				  echo json_encode($allMsg);  
			 } else {
			   echo "noNewMsg";
			 }
             
		}


		public function updateMsgStatus()
		{
		  
           $msgId   			= $this->input->post('msgId');
           $data['msg_status']  = "0";
           $this->M_live_chat->update($data, $msgId);
				
        }

		public function getAllActiveUser()
		{
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['totalActiveUser']  = $this->M_all_user_registration->countAllActiveUser($userDetails->id);  
			  $data['allUser']   		= $this->M_all_user_registration->findAllLoginUser($userDetails->id); 

		   }
		  
           $this->load->view('getAllActiveUserPage', $data);
				
		}


		public function getAllUnreadMsg()
		{

		   if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
		      $data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);
		   }

           $this->load->view('getAllUnreadMsgPage', $data);
				
		}


	

}

