<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usa extends MX_Controller {

		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub',
		'M_news_and_event_manage','M_user_post_manage','M_all_user_registration','M_post_comment','M_reply_comment','M_organization_user_registration',
		'M_add_manage','M_general_user_registration','M_programe_manage','M_job_post_manage','M_job_application_details', 'M_country_manage', 'M_organize_profile_manage','M_search_table','M_region_manage','M_visitor_count');
		static $helper   = array('url','userauthentication','generalauthentication','html');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
			$this->load->helper('html','url');
	
			$this->load->database();
			$this->load->model(self::$model);
			
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
			$this->load->library('email');
			$this->load->helper('cookie');
	
		}
	

		public function index()
	
		{	
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		  						
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllHome();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$data['allProgramName']	    = $this->M_programe_manage->findAll();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->leftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->leftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();


			$regionList = $this->M_region_manage->findAll();
			foreach ($regionList as $v) {
				$v->countryList = $this->M_country_manage->findAllInfo($v->id);
			}
			$data['regionList'] = $regionList;

			$this->load->view('offer/usa', $data);
	
		}

		public function aboutUsa(){
			
		  if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
		   }
		   
		  						
			$data['page_title']  		= self::Title;	
			$data['countryInfo']	    = $this->M_country_manage->findAllCountry();
			$data['orgInfo']	    	= $this->M_all_user_registration->findAllOrganizeUser();		
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['middleAddInfo']	    = $this->M_job_post_manage->findAllHome();
			$data['allOrgName']	    	= $this->M_all_user_registration->findAllOrganize();
			$data['allProgramName']	    = $this->M_programe_manage->findAll();
			
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			
			
			$data['leftAddInfo1']	    = $this->M_add_manage->leftAddInfo1();
			$data['leftAddInfo2']	    = $this->M_add_manage->leftAddInfo2();
			$data['leftAddInfo3']	    = $this->M_add_manage->leftAddInfo3();
			$data['leftAddInfo4']	    = $this->M_add_manage->leftAddInfo4();
			$data['leftAddInfo5']	    = $this->M_add_manage->leftAddInfo5();
			$data['leftAddInfo6']	    = $this->M_add_manage->leftAddInfo6();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$allUserPostInfo	        = $this->M_user_post_manage->findAllInfo();
			$data['allUserPostInfo']	= $allUserPostInfo;
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
			$data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
			$data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
			$data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();


			$regionList = $this->M_region_manage->findAll();
			foreach ($regionList as $v) {
				$v->countryList = $this->M_country_manage->findAllInfo($v->id);
			}
			$data['regionList'] = $regionList;

			$this->load->view('offer/aboutUsaPage', $data);
		}		
}
