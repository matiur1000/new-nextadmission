<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SubMenuDetails extends MX_Controller {

		static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_news_and_event_manage',
		'M_all_user_registration','M_organization_user_registration','M_add_manage','M_general_user_registration','M_live_chat');
		static $helper	= array('url', 'userauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
	
		}
	

		public function index($menuId,$subCatId)
	
		{	
		
		  	if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $data['activeUser']       = $userDetails->name;
			  $data['userType']       	= $userDetails->user_type;
			  $data['userDetails']      = $userDetails;
		   }
			$data['page_title']  		= self::Title;
			$data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);
			$data['headerAddInfo']		= $this->M_advertisement_manage->findHeaderView();
			$data['headerBasicInfo']	= $this->M_basic_manage->findHeaderView();
			$data['rightAddInfo']	    = $this->M_advertisement_manage->findRightView();
			$data['middleAddInfo']	    = $this->M_advertisement_manage->findMiddleView();
			$data['newsAndEventInfo']	= $this->M_news_and_event_manage->findAllView();
			$where                      =  array('position_top' => 'Yes','status' => 'Available');
			$menuInfo			        = $this->M_menu_manage->findAllDetailes($where);
			$data['menuInfo']			= $menuInfo;
			$where						= array('menu_id'=>$menuId, 'id'=>$subCatId,'status' => 'Available');
			$data['subMenuDetails']		= $this->M_submenu_manage->findByDetails($where);
			$data['footerMenuInfo1']	= $this->M_menu_manage->findAllFooterMenu1();
		    $data['footerMenuInfo2']	= $this->M_menu_manage->findAllFooterMenu2();
		    $data['footerMenuInfo3']	= $this->M_menu_manage->findAllFooterMenu3();
		    $data['footerMenuInfo4']	= $this->M_menu_manage->findAllFooterMenu4();
			
			$data['topAddInfo1']	    = $this->M_add_manage->topAddInfo1();
			$data['topAddInfo2']	    = $this->M_add_manage->topAddInfo2();
			$data['topAddInfo3']	    = $this->M_add_manage->topAddInfo3();
			$data['topAddInfo4']	    = $this->M_add_manage->topAddInfo4();
			
			$data['rightAddInfo1']	    = $this->M_add_manage->rightAddInfo1();
			$data['rightAddInfo2']	    = $this->M_add_manage->rightAddInfo2();
			$data['rightAddInfo3']	    = $this->M_add_manage->rightAddInfo3();
			$data['rightAddInfo4']	    = $this->M_add_manage->rightAddInfo4();
			$data['rightAddInfo5']	    = $this->M_add_manage->rightAddInfo5();
			$data['rightAddInfo6']	    = $this->M_add_manage->rightAddInfo6();
			$data['rightAddInfo7']	    = $this->M_add_manage->rightAddInfo7();
			$data['rightAddInfo8']	    = $this->M_add_manage->rightAddInfo8();
			$data['rightAddInfo9']	    = $this->M_add_manage->rightAddInfo9();
			$data['rightAddInfo10']	    = $this->M_add_manage->rightAddInfo10();
			$data['rightAddInfo11']	    = $this->M_add_manage->rightAddInfo11();
			$data['rightAddInfo12']	    = $this->M_add_manage->rightAddInfo12();
			$data['rightAddInfo13']	    = $this->M_add_manage->rightAddInfo13();
			$data['rightAddInfo14']	    = $this->M_add_manage->rightAddInfo14();
			$data['rightAddInfo15']	    = $this->M_add_manage->rightAddInfo15();
			$data['rightAddInfo16']	    = $this->M_add_manage->rightAddInfo16();
			$data['rightAddInfo17']	    = $this->M_add_manage->rightAddInfo17();
			$data['rightAddInfo18']	    = $this->M_add_manage->rightAddInfo18();
			$data['rightAddInfo19']	    = $this->M_add_manage->rightAddInfo19();
			$data['rightAddInfo20']		= $this->M_add_manage->rightAddInfo20();
			
			$this->load->view('subMenuDetails', $data);
	
		}
		
	

}

