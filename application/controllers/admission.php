<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admission extends MX_Controller {
	
		
		static $model 	 = array('M_organize_profile_manage');
		static $helper   = array('url','generalauthentication','userauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
			//$this->load->helper(array('url','pdf'));
		}
	
	

		public function index()	
		{
			$this->load->view('admissionForm');
		
		}
}
?>
<?php if(isset($stdData->given_name)) echo $stdData->given_name;?>