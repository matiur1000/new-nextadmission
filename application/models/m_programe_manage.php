<?php

	class M_programe_manage extends CI_Model {
	
		const TABLE	= 'higher_education_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);    
			return $this->db->insert_id();       
		}
		
		
		
		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findByOrgId($courseId, $organizeId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('id' => $courseId, 'orgnization_id' => $organizeId));
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findByUser($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('orgnization_id', $userId);
			$query = $this->db->get();
			return $query->result();
		}



		public function findAllProByOrgId($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result();
		}

		
		public function searchOrgAllProgram($organizationId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('orgnization_id', $organizationId);
			$query = $this->db->get();
			return $query->result();
		}
		
		public function searchProgram(Array $where = array())
		{
			$this->db->select('higher_education_manage.*, all_user_registration.name,all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'higher_education_manage.orgnization_id = all_user_registration.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findOrgWiseProDetails($programId)
		{
			$this->db->select('higher_education_manage.*, all_user_registration.name,all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'higher_education_manage.orgnization_id = all_user_registration.id', 'left');
			$this->db->where('higher_education_manage.id', $programId);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>