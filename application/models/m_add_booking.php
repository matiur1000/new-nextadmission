<?php

	class M_add_booking extends CI_Model {
	
		const TABLE	= 'add_manage_booking';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		public function adValueChkByDate($where, $publish_date, $expire_date, $select_website, $country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->where('booking_date >= "'.$publish_date.'" and booking_date <= "'.$expire_date.'"');
			if($select_website =='main'){
			 $this->db->where(array('country_name' => ""));
			}else{
			  $this->db->where(array('country_name' => $country_name));	
			}
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function adValueChkByDateadd($where, $publish_date, $expire_date, $select_website, $country_name)
		{
			
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->where('booking_date >=' , $publish_date);
			$this->db->where('booking_date <= ' ,$expire_date);
			if($select_website == 'main'){
			 $this->db->where('country_name', "");
			}else{
			  $this->db->where('country_name',$country_name);
			} 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		
		
		
		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	



		public function findAllDeleteData($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);
			$this->db->order_by("add_id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		
		
		public function update2($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		


		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('add_id' => $id));        
		}
		
		
		
		public function destroyad($id)
		{
			$this->db->delete(self::TABLE, array('ad_mId' => $id));        
		}
		
			
	}
?>