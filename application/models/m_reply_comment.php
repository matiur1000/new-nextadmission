<?php

	class  M_reply_comment extends CI_Model {
	
		const TABLE	= 'reply_comment';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		
		public function findAll($where = array())
		{
			$this->db->select('reply_comment.*, all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'reply_comment.user_id = all_user_registration.id', 'left');
			$this->db->where($where);
			//$this->db->limit(2, 0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findByPost($postId)
		{
		
			$this->db->select('post_comment.*, all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'post_comment.user_id = all_user_registration.id', 'left');
			$this->db->where('post_id', $postId);
			$query = $this->db->get();
			return $query->result();
		}	
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>