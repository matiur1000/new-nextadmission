<?php

	class M_live_chat extends CI_Model {
	
		const TABLE	= 'live_chat';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		
		public function findAllSmsById($sendTo, $sendFrom)
		{
			$this->db->select('*');
			$this->db->select('live_chat.*, all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'live_chat.send_from = all_user_registration.id', 'left');
			$where ="(send_to =$sendTo AND send_from=$sendFrom) OR (send_to =$sendFrom AND send_from=$sendTo)";
			$this->db->where($where);
			$this->db->order_by("live_chat.id", "desc");
			$this->db->limit(20, 0);
			$query = $this->db->get();
			return $query->result();
		}

		public function getAllSmsById($sendFrom, $sendTo)
		{
			$this->db->select('*');
			$this->db->select('live_chat.*, all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'live_chat.send_from = all_user_registration.id', 'left');
			$where ="(send_to =$sendTo AND send_from=$sendFrom) OR (send_to =$sendFrom AND send_from=$sendTo)";
			$this->db->where($where);
			$this->db->order_by("live_chat.id", "desc");
			$this->db->limit(20, 0);
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllNewSms($sendTo)
		{
			$this->db->select('*');
			$this->db->select('live_chat.*, all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'live_chat.send_from = all_user_registration.id', 'left');
			$this->db->where(array('live_chat.send_to' => $sendTo, 'live_chat.msg_status' => "1"));
			$this->db->order_by("live_chat.id", "asc");
			$query = $this->db->get();
			return $query->result();
		}

		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	

		public function findAllOldMsg($sendTo, $sendFrom)
		{
			$this->db->select('*');
			$this->db->select('live_chat.*, all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'live_chat.send_from = all_user_registration.id', 'left');
			$where ="(send_to =$sendTo AND send_from=$sendFrom) OR (send_to =$sendFrom AND send_from=$sendTo)";
			$this->db->where($where);
			$this->db->order_by("live_chat.id", "desc");
			$this->db->limit(200, 20);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findHeaderView()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}



		public function countAllMsg($sendTo, $sendFrom)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$where ="(send_to =$sendTo AND send_from=$sendFrom) OR (send_to =$sendFrom AND send_from=$sendTo)";
			$this->db->where($where);
			return $this->db->count_all_results();
		}

		public function findAllNewMsg($sendTo)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('live_chat.send_to' => $sendTo, 'live_chat.msg_status' => "1"));
			return $this->db->count_all_results();
		}


		public function findBySenderId($sendTo)
		{
			$this->db->select('*');
			$this->db->select('live_chat.*, all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'live_chat.send_from = all_user_registration.id', 'left');
			$this->db->where(array('live_chat.send_to' => $sendTo, 'live_chat.msg_status' => "1"));
			$query = $this->db->get();
			return $query->row();
		}

		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>