<?php

	class M_organize_profile_manage extends CI_Model {
	
		const TABLE	= '';
	
		public function __construct()
		{
			parent::__construct();
		}
			

		
             public function mobile_check($table, $mobile_no){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('mobile_no', $mobile_no);
			$query = $this->db->get();
			return $query->row();
		}

		public function org_logo($table, $org_logo){
			$this->db->where(array('org_logo' => $org_logo));
			$this->db->insert($table, $org_logo);
		}
		
		public function paymentdetails($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function std_details($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}

		public function findAboutByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "about"));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findAboutindaiByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "About India"));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findcusefreesByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('organize_id', $userId);
			$query = $this->db->get();
			return $query->row();
		}
		public function findeliegiblityByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "eligiblity"));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findhostelandothercostByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "hostelothercost"));
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findscholshiperByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "scholarship"));
			$query = $this->db->get();
			return $query->row();
		}
		public function findstravelitineraryByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "travel itinerary"));
			$query = $this->db->get();
			return $query->row();
		}
		public function findhowtoapplyByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "howtoapply"));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findwhentoapplyByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "whentoapply"));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findorgallByUser($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		

		public function findFileByType($table, $type)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('type_name' => $type));
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findContactByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "contact"));
			$query = $this->db->get();
			return $query->row();
		}


		public function findHomeByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => "home"));
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function orgIdbyaboutus($table, $organizeId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $organizeId, 'page_type' => "about"));
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findById($table, $id)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('id' => $id));
			$query = $this->db->get();
			return $query->row();
		}


		public function findAll($table, $orderby)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->order_by($orderby); 
			$query = $this->db->get();
			return $query->result();
		}


		public function findCsvDtaById($table, $id)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('user_id' => $id));
			$query = $this->db->get();
			return $query->row();
		}

		

		public function findbyByGenUser($table, $sendToUser, $onset)
		{
			$this->db->select('send_message_to_user.*, all_user_registration.name');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'send_message_to_user.send_from_user_id = all_user_registration.id', 'left');
			$this->db->where(array('send_to_user_id' => $sendToUser));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(10, $onset); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findByMessageId($table, $msgViewId)
		{
			$this->db->select('send_message_to_user.*, all_user_registration.name,all_user_registration.image');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'send_message_to_user.send_from_user_id = all_user_registration.id', 'left');
			$this->db->where(array('send_message_to_user.id' => $msgViewId));
			$query = $this->db->get();
			return $query->row();
		}



		public function findByReply($table, $msgViewId)
		{
			$this->db->select('message_reply.*, all_user_registration.name,all_user_registration.image');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'message_reply.rep_user_id = all_user_registration.id', 'left');
			$this->db->where(array('message_reply.message_id' => $msgViewId));
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllReplyByMsg($where = array())
		{
			$this->db->select('message_reply.*, all_user_registration.name,all_user_registration.image');
			$this->db->from('message_reply');
			$this->db->join('all_user_registration', 'message_reply.rep_user_id = all_user_registration.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllGifImage($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllByCountId($table, $countId)
		{
			$this->db->select('send_message_to_user.*, all_user_registration.name,all_user_registration.image');
			$this->db->from($table);
			$this->db->join('all_user_registration', 'send_message_to_user.send_from_user_id = all_user_registration.id', 'left');
			$this->db->where(array('send_message_to_user.count_id' => $countId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findByVisitorCountId($table, $countId)
		{
			$List 		= $this->findAllByCountId($table, $countId);
			
			foreach($List as $model)
			{
				$model->reply 		= $this->findAllReplyByMsg(array("message_reply.message_id" => $model->id));
				
			}
			return $List;
		}



		
		public function countAll($table, $sendToUser)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('send_to_user_id' => $sendToUser));			
			return $this->db->count_all_results();
		}
		
		
		public function findAllNewsByOrganize($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(5,0);
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllPhotoByOrganize($table, $organizeId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $organizeId));
			$this->db->limit(1,0);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function orghomeLast($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->limit(1,0);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function admisionforad($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findorgAllPhotoByOrganize($table, $organizeId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $organizeId));
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
			public function findorgAlladdimgOrganize($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findfindbyorgByUser($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}


		
		
		public function findvideogalleryByOrganize($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllSlideDataByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllPhotoDataByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllVideoDataByUser($table, $userId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findAboutContactByUser($table, $userId, $pageType)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('organize_id' => $userId, 'page_type' => $pageType));
			$query = $this->db->get();
			return $query->row();
		}

		public function findMaxSl($table)
		{
			$this->db->select('max(sl_no) as sl_no');
			$this->db->from($table);
			$query = $this->db->get();
			return $query->row()->sl_no+1;
		}


		public function findAllMenu($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllYear($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllData($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

        public function findBySubMenuStatus($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('sub_menu_status' => "Yes"));
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllManagerMenuInfo($table)
		{
			$this->db->select('manager_submanu_manage.*, panel_profile_menu_manage.menu_name');
			$this->db->from($table);
			$this->db->join('panel_profile_menu_manage', 'manager_submanu_manage.menu_id = panel_profile_menu_manage.id', 'left');
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		

		public function findAllSubByMenuId($table, $menuId)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('menu_id' => $menuId));
			$this->db->order_by("id", "asc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function AllPanelMenu($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('menu_position' => "Panel"));
			$this->db->order_by("id", "asc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllPanelMenu($table)
		{
			$List 		= $this->AllPanelMenu($table);
			
			foreach($List as $model)
			{
				$model->sub 		= $this->findAllSubByMenuId('manager_submanu_manage', $model->id);
			}
			return $List;
		}

		public function findAllProfileMenu($table)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where(array('menu_position' => "Profile"));
			$this->db->order_by("id", "asc"); 
			$query = $this->db->get();
			return $query->result();
		}


		 public function admission_view(){
		    $data = $this->db->query("SELECT * from online_admission_form order by id DESC"); 
		    return $data->result();   
		 }

		 public function view(){
		    $data=$this->db->query("SELECT *from student_form order by id DESC "); 


		    return $data->result();   


		   
		 }

		 public function inted_country($id){

		   /* $data=$this->db->query("SELECT interested_country from student_form order by id DESC "); 


		    return $data->result();  


		   /* $this->db->select('student_form');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			
			$this->db->where(array('student_form.interested_country' => $country_name));
 
			$this->db->order_by("id", "desc"); 

			$query = $this->db->get(); 

			return $query->result();


			*/


				//$data=$this->db->query("SELECT * FROM student_form  WHERE  id= $id");

						//return $query->result_array();

				//return $data->result();

$query=$this->db->query("SELECT * FROM student_form WHERE id = $id");

        return $query->result_array();



		   
		 }

		


		 public function view_search(){
		    $data=$this->db->query("SELECT *from student_form order by id DESC "); 
		    return $data->result();   
		 }

		 
  
		public function admission_login($table, $data)
		  {
		   $this->db->select('*');
		   $this->db->from($table);
		   $this->db->where('name',$data['name']);
		   $this->db->where('pass',$data['pass']);
		   $query = $this->db->get();
		   return $query->row();
		  }

		public function login($table, $name)
		  {
		   $this->db->select('*');
		   $this->db->from($table);
		   $this->db->where(array('name' => $name));
		   $query = $this->db->get();
		   return $query->row();
		  }

		
		public function update($table, $id, $type, $data)
		{
			$this->db->update($table, $data, array('organize_id' => $id, 'page_type' => $type));        
		}

		public function save($table, $data)
		{
			$this->db->insert($table, $data); 
			return $this->db->insert_id();        
		}
		
		public function update2($table, $id, $data)
		{
			$this->db->update($table, $data, array('id' => $id));        
		}
		
		
		
		public function destroy($table, $id)
		{
			$this->db->delete($table, array('id' => $id));        
		}

		public function destroy_csv($table, $id)
		{
			$this->db->delete($table, array('user_id' => $id));        
		}
			
	}
