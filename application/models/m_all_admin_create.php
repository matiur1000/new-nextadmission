<?php

	class M_all_admin_create extends CI_Model {
	
		const TABLE	= 'all_admin_create';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		public function findAll(Array $where = array())
		{
			$this->db->select('all_admin_create.*, admin_type_manage.admin_type');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('admin_type_manage', 'all_admin_create.admin_type_id = admin_type_manage.id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "asc"); 
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findAddinId($table, $where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
	
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>