<?php

	class M_news_and_event_manage extends CI_Model {
	
		const TABLE	= 'news_and_event_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		
		
		public function findAll()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		public function findAllNewsByOrgId($userId)
		{
			$this->db->select('news_and_event_manage.*, all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'news_and_event_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('news_and_event_manage.user_id' => $userId));
			$this->db->order_by("id", "desc");			
			$query = $this->db->get();
			return $query->result();
		}	
			
		public function findAllView()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "aprove"));
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAllbreknews()
		{
			$this->db->select('*');
			$this->db->from('breaking_news_manage');
			//$this->db->where(array('status' => "aprove"));
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		

		public function findAllUpapprovedNews()
		{
			$this->db->select('news_and_event_manage.*, all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'news_and_event_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('news_and_event_manage.status' => "unaproave"));
			$this->db->order_by("news_and_event_manage.id", "desc");
			$query = $this->db->get();
			return $query->result();
		}		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findByUserWiseDetails($newsId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $newsId);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>