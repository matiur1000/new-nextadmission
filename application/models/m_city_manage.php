<?php

	class M_city_manage extends CI_Model {
	
		const TABLE	= 'city_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		public function findAllCity($country_id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('country_id', $country_id);
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAll($where = array(), $onset = 0)
		{
			$this->db->select('city_manage.*, region_manage.region_name, country_manage.country_name');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('region_manage', 'city_manage.region_id = region_manage.id', 'left');
			$this->db->join('country_manage', 'city_manage.country_id = country_manage.id', 'left');
			$this->db->where($where);
			$this->db->limit(10, $onset); 
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		public function countAll($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);			
			return $this->db->count_all_results();
		}	
		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
