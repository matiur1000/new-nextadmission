<?php

	class M_user_post_manage extends CI_Model {
	
		const TABLE	= 'user_post_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);   
			return $this->db->insert_id();            
		}
		
		
		
		public function findByUser($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->where('user_id', $userId);
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAll2(Array $where = array())
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image as regimage,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			//$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllInfo2($where = array())
		{
			$List 		= $this->findAll2();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		public function findAll(Array $where = array())
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image as regimage,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where($where);
			$this->db->order_by("id", "desc"); 
			$this->db->limit(8,0);
			$query = $this->db->get();
			return $query->result();
		}


		public function findAllCountryWiseInfo($country_name)
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('country_name' => $country_name));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
			
		
		
		public function findAllAsia()
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('region' => "Asia"));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllsoutAm()
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('region' => "South America"));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllnorthAm()
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('region' => "North America"));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findAllAfrica()
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('region' => "Africa"));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllAustralia()
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('region' => "Australia"));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllEurop()
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name,all_user_registration.user_type,general_user_details.education_institute');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('general_user_details', 'user_post_manage.user_id = general_user_details.user_id', 'left');
			$this->db->where(array('region' => "Europ"));
			$this->db->order_by("id", "desc"); 
			$this->db->limit(14,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		
		
		public function findAllInfo($where = array())
		{
			$List 		= $this->findAll();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		public function findAllAsiaInfo()
		{
			$List 		= $this->findAllAsia();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		
		public function findAllsoutAmInfo()
		{
			$List 		= $this->findAllsoutAm();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		public function findAllNorthAmInfo()
		{
			$List 		= $this->findAllnorthAm();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		public function findAllAfricaInfo()
		{
			$List 		= $this->findAllAfrica();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		
		public function findAllAustralliaInfo()
		{
			$List 		= $this->findAllAustralia();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		public function findAllEuropInfo()
		{
			$List 		= $this->findAllEurop();
			
			foreach($List as $model)
			{
				$com 		= $this->M_post_comment->findAllInfo(array("post_id" => $model->id));
				$model->com = $com;
			
			}
			return $List;
		}
		
		
		public function findByPostInfo($postId)
		{
		
			$this->db->select('user_post_manage.*,all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where('user_post_manage.id', $postId);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function findByUserWiseDetails($postId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $postId);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		
		public function findbyUserInfo($userAutoId)
		{
		
			$this->db->select('user_post_manage.*, all_user_registration.image,all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'user_post_manage.user_id = all_user_registration.id', 'left');
			$this->db->where('user_post_manage.user_id', $userAutoId);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findHeaderView()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>