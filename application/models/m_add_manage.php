<?php

	class M_add_manage extends CI_Model {
	
		const TABLE	= 'add_manage';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);    
			return $this->db->insert_id();        
		}
		
		
		public function findByLastAdId($lastAdId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $lastAdId);
			$query = $this->db->get();
			return $query->row();
		}		
		
		
		public function findAllData()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}

		public function findAll($setLimit, $onset = 0)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->order_by("id", "desc"); 
			$this->db->limit($setLimit, $onset); 
			$query = $this->db->get();
			return $query->result();
		}



		public function countAll($where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where($where);			
			return $this->db->count_all_results();
		}	


		public function findAllAustraliaAd()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('region' => "Australia"));
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllUpapprovedAd()
		{
			$this->db->select('add_manage.*, all_user_registration.name');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->where(array('published_status' => "unaproave"));
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		
		public function adsmanage($onset)
		{
			$this->db->select('add_manage.*, all_user_registration.user_id, all_user_registration.name, d1.country_name d1name, d2.country_name d2name');
			$this->db->from('add_manage');
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('country_manage as d1', 'd1.id = all_user_registration.country_id', 'left');
			$this->db->join('country_manage as d2', 'd2.id = add_manage.country_name', 'left');
			$this->db->limit(20, $onset);
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function adIdbyDetails($where)
		{
			$this->db->select('add_manage.*, all_user_registration.user_id, all_user_registration.name, d1.country_name d1name, d2.country_name d2name');
			$this->db->from('add_manage');
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('country_manage as d1', 'd1.id = all_user_registration.country_id', 'left');
			$this->db->join('country_manage as d2', 'd2.id = add_manage.country_name', 'left');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function total_rows()
		   {

				$this->db->select('*');
				$this->db->from('add_manage');
				return $this->db->count_all_results();
		   }
		
		
		
		
		public function findAllByUser($userId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('user_id' => $userId, 'status' => "notavailable"));
			$this->db->order_by("id", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		
		
		public function findAllAvailable()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "available", 'region' => ""));
			$this->db->group_by("positon", "desc");
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findByRegionWisePosition($region)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "available", 'region' => $region));
			$this->db->group_by("positon", "desc");
			$query = $this->db->get();
			return $query->result();
		}
		
		public function findByRegionWiseSl($region, $regionPositon)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "available", 'region' => $region, 'positon' => $regionPositon));
			$this->db->order_by("id", "asc");
			$query = $this->db->get();
			return $query->result();
		}	
			
		public function findSlNo($positon)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "asc");
			$this->db->where(array('positon' => $positon, 'region' => ""));
			$query = $this->db->get();
			return $query->result();
		}
		public function findAllByPosition($positon)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->where('positon', $positon);
			$query = $this->db->get();
			return $query->result();
		}



		public function topAddInfoforweb1($organizeId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'serial_no' => "1", 'region' => ""));
			$this->db->where('user_id', $organizeId);
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		


		public function topAddInfoforweb2($organizeId)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'serial_no' => "1", 'region' => ""));
			$this->db->where('user_id', $organizeId);
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
				
		
		public function topAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'serial_no' => "1", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function topAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'serial_no' => "2", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function topAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'region' => ""));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function topAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'region' => ""));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function leftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "1", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function leftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "2", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function leftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "3", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			//$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function leftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "4", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function leftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "5", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function leftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "6", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function leftAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "7", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		public function leftAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "8", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function leftAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "9", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}				
			
		
		
		public function rightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "1", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function rightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "2", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "3", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "4", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function rightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "5", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function rightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "6", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "7", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "8", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "9", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function rightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "10", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "11", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "12", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "13", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function rightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "14", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "15", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "16", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function rightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "17", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "18", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function rightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "19", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function rightAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "20", 'region' => ""));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}


      //COUNTRY DATA START




		public function countryTopAddInfo1($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'serial_no' => "1", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}



		public function countryTopAddInfo2($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Top", 'serial_no' => "2", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}




		public function countryleftAddInfo1($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "1", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function countryleftAddInfo2($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "2", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryleftAddInfo3($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "3", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryleftAddInfo4($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "4", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}		
			

		public function countryleftAddInfo5($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "5", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}



		public function countryleftAddInfo6($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Left", 'serial_no' => "6", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	



		//RIGHT AD START


		public function countryRightAddInfo1($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "1", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function countryRightAddInfo2($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "2", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryRightAddInfo3($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "3", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryRightAddInfo4($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "4", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}		
			

		public function countryRightAddInfo5($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "5", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}



		public function countryRightAddInfo6($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "6", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}




		public function countryRightAddInfo7($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "7", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function countryRightAddInfo8($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "8", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryRightAddInfo9($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "9", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryRightAddInfo10($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "10", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}		
			

		public function countryRightAddInfo11($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "11", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}



		public function countryRightAddInfo12($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "12", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}





		public function countryRightAddInfo13($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "13", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}




		public function countryRightAddInfo14($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "14", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function countryRightAddInfo15($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "15", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryRightAddInfo16($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "16", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function countryRightAddInfo17($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "17", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}		
			

		public function countryRightAddInfo18($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "18", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}



		public function countryRightAddInfo19($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "19", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}


		public function countryRightAddInfo20($country_name)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$currentDate  = date("Y-m-d");
			$this->db->where(array('status' => "notavailable", 'positon' => "Right", 'serial_no' => "20", 'country_name' => $country_name));
			$this->db->where('published_status', "approve");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$query = $this->db->get();
			return $query->row();
		}	
			
		
	


		//RIGHT AD END		
			
		
		


		//COUNTRY DATA END

		
		//ADD ASIA
		
		
		public function asiaTopAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Asia"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaTopAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Asia"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		
		public function asialeftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Asia"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asialeftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Asia"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asialeftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Asia"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asialeftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Asia"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}		
			

		public function asialeftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Asia"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}



		public function asialeftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Asia"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}			
			
		
		
		public function asiaRightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaRightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function asiaRightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaRightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,6);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,7);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,8);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaRightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,9);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,10);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,11);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,12);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaRightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,13);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,14);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,15);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaRightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,16);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,17);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function asiaRightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,18);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function asiaRightAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Asia"));
			$this->db->limit(1,19);
			$query = $this->db->get();
			return $query->row();
		}	
		
		// South America
		
		
		public function soutAmTopAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "South America"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmTopAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "South America"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		
		public function soutAmLeftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "South America"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmLeftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "South America"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmLeftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "South America"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmLeftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "South America"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}



		public function soutAmLeftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "South America"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmLeftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "South America"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}				
			
		
		
		public function soutAmRightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmRightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function soutAmRightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmRightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,6);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,7);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,8);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmRightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,9);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,10);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,11);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,12);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmRightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,13);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,14);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,15);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmRightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,16);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,17);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function soutAmRightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,18);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function soutAmRightAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "South America"));
			$this->db->limit(1,19);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		// NORT AMERICA
		
		public function northAmTopAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "North America"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmTopAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "North America"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		
		
		public function northAmLeftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "North America"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmLeftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "North America"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmLeftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "North America"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmLeftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "North America"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}



		public function northAmLeftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "North America"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmLeftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "North America"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}		
					
			
		
		
		public function northAmRightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmRightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function northAmRightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmRightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,6);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,7);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,8);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmRightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,9);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,10);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,11);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,12);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmRightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,13);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,14);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,15);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmRightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,16);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,17);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function northAmRightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,18);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function northAmRghtAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "North America"));
			$this->db->limit(1,19);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		
		// AFRICA
		
		public function africatopAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Africa"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africatopAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Africa"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		
		
		public function africaleftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Africa"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africaleftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Africa"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africaleftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Africa"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africaleftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Africa"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	



		public function africaleftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Africa"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africaleftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Africa"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}			
			
		
		
		public function africarightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africarightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function africarightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africarightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,6);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,7);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,8);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africarightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,9);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,10);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,11);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,12);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africarightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,13);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,14);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,15);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africarightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,16);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,17);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function africarightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,18);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function africarightAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Africa"));
			$this->db->limit(1,19);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		
		//AUSTRALIA
		
		public function australliatopAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Australia"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliatopAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Australia"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		
		
		public function australlialeftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Australia"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australlialeftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Australia"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australlialeftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Australia"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australlialeftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Australia"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	



		public function australlialeftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Australia"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australlialeftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Australia"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}		
				
			
		
		
		public function australliarightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliarightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function australliarightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliarightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,6);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,7);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,8);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliarightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,9);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,10);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,11);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,12);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliarightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,13);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,14);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,15);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliarightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,16);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,17);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function australliarightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,18);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function australliarightAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Australia"));
			$this->db->limit(1,19);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		//EUROP
		
		public function europtopAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Europ"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroptopAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "top", 'region' => "Europ"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		
		
		public function uropleftAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Europ"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uropleftAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Europ"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uropleftAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Europ"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uropleftAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Europ"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	




		public function uropleftAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Europ"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uropleftAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "left", 'region' => "Europ"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}			
			
		
		
		public function uroprightAddInfo1()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,0);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroprightAddInfo2()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,1);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo3()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,2);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo4()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,3);
			$query = $this->db->get();
			return $query->row();
		}	
		
		
		public function uroprightAddInfo5()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,4);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroprightAddInfo6()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,5);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo7()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,6);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo8()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,7);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo9()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,8);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroprightAddInfo10()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,9);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo11()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,10);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo12()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,11);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo13()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,12);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroprightAddInfo14()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,13);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo15()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,14);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo16()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,15);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroprightAddInfo17()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,16);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo18()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,17);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function uroprightAddInfo19()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,18);
			$query = $this->db->get();
			return $query->row();
		}	
		
		public function uroprightAddInfo20()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('status' => "notavailable", 'positon' => "right", 'region' => "Europ"));
			$this->db->limit(1,19);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		//
		
		
		public function findHeaderView()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->where(array('status' => "notavailable"));
			$this->db->limit(4,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findRightView()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->where(array('status' => "aprove"));
			$this->db->limit(16,4);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findMiddleView()
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc");
			$this->db->where(array('status' => "aprove"));
			$this->db->limit(15,0);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		public function findAllByOrganize($organizeId)
		{
			$this->db->select('add_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->where('add_manage.user_id', $organizeId);
			$this->db->order_by("add_manage.id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}

		public function findAllSearchInfo($organizeId, $title)
		{
			$this->db->select('add_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			// tabel name, joining condition, left/right/inner/selt. for inner no need to write
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->where('add_manage.user_id', $organizeId);
			$this->db->like('add_manage.title', $title);
			$this->db->order_by("add_manage.id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}


		public function findadvesrch($where)
		{
			$this->db->select('add_manage.*, all_user_registration.*, d1.country_name d1name, d2.country_name d2name');
			$this->db->from('add_manage');
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('country_manage as d1', 'd1.id = all_user_registration.country_id', 'left');
			$this->db->join('country_manage as d2', 'd2.id = add_manage.country_name', 'left');
			$this->db->like($where);
			$this->db->order_by("add_manage.id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findAdaprove($where)
		{
			$this->db->select('add_manage.*, all_user_registration.*, d1.country_name d1name, d2.country_name d2name');
			$this->db->from('add_manage');
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$this->db->join('country_manage as d1', 'd1.id = all_user_registration.country_id', 'left');
			$this->db->join('country_manage as d2', 'd2.id = add_manage.country_name', 'left');
			$this->db->where($where);
			$this->db->order_by("add_manage.id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}




		public function findAllCurrentAdByOrganize($organizeId)
		{
			$this->db->select('add_manage.*, all_user_registration.name, all_user_registration.image');
			$this->db->from(self::TABLE);
			$this->db->join('all_user_registration', 'add_manage.user_id = all_user_registration.id', 'left');
			$currentDate  = date("Y-m-d");
			$this->db->where('expire_date >', $currentDate);
			$this->db->where('expire_date >=', $currentDate);
			$this->db->where('publish_date <=', $currentDate);
			$this->db->where('add_manage.user_id', $organizeId);
			$this->db->order_by("add_manage.id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}


		public function findBypoSlRe($positon, $serial_no, $region)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where(array('positon' => $positon, 'serial_no' => $serial_no));
			
			if($region){
			 $this->db->where(array('region' => $region));	
			}else{
			 $this->db->where(array('region' => ""));
			}
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function update2($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		
		public function update($table, $data, $where)
		{
			$this->db->update($table, $data, $where);        
		}
		
		
		
		
		public function updateAdd($data, $positon, $serialNo, $region)
		{
			if(!empty($region)){
			 $this->db->update(self::TABLE, $data, array('positon' => $positon, 'serial_no' => $serialNo, 'region' => $region));        
			} else {
			 $this->db->update(self::TABLE, $data, array('positon' => $positon, 'serial_no' => $serialNo, 'region' => ""));        

			}
		}
		
		public function updateRegionAdd($data, $region, $positon, $serialNo)
		{
			$this->db->update(self::TABLE, $data, array('region' => $region, 'positon' => $positon, 'serial_no' => $serialNo));        
		}
		
		public function upadate22($data)
		{
			$this->db->update(self::TABLE, $data, array('region' => ""));        
		}

		public function updateTest($data, $positon, $serial_no)
		{
			$this->db->update(self::TABLE, $data, array('region' => "Europ", 'positon' => $positon, 'serial_no' => $serial_no));        
		}
		
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
			
	}
?>