<?php



	class M_all_user_registration extends CI_Model {

	

		const TABLE	= 'all_user_registration';

	

		public function __construct()

		{

			parent::__construct();

		}

			

		public function save($data)

		{

			$this->db->insert(self::TABLE, $data); 

			return $this->db->insert_id();       

		}

		

		public function findAll($where = array(), $setLimit, $onset = 0)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->where(array('user_type' => "Organization User"));

			$this->db->where($where);

			$this->db->order_by("id", "desc"); 

			$this->db->limit($setLimit, $onset); 

			$query = $this->db->get();

			return $query->result();

		}



		public function findAllOrganizeData()

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->where(array('user_type' => "Organization User"));

			$this->db->order_by("id", "desc"); 

			$query = $this->db->get();

			return $query->result();

		}



		public function countryWiseData($country_id)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->where(array('all_user_registration.country_id' => $country_id, 'all_user_registration.user_type' => "Organization User"));

			$this->db->order_by("id", "desc"); 

			$query = $this->db->get();

			return $query->result();

		}





		public function findByOrgUserName($userName)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->where(array('all_user_registration.user_type' => "Organization User"));

			$this->db->like('all_user_registration.name', $userName); 

			$this->db->order_by("id", "desc"); 

			$query = $this->db->get();

			return $query->result();

		}









		public function findAllRegData()

		{

			$this->db->select('all_user_registration.*, country_manage.country_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->order_by("id", "desc"); 

			$query = $this->db->get();

			return $query->result();

		}

		

		

		public function findAllorgIdbyData($where)

		{

			$this->db->select('all_user_registration.*, country_manage.country_name');

			$this->db->from('all_user_registration');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->where($where);

			$query = $this->db->get();

			return $query->row();

		}

		

		

		public function findallorg($table, $where)

		{

			$this->db->select('*');

			$this->db->from($table);

			$this->db->where($where);

			$this->db->order_by("user_id", "ABCD"); 

			$query = $this->db->get();

			return $query->result();

		}

		

		public function findallcontry($table)

		{

			$this->db->select('*');

			$this->db->from($table);

			$this->db->order_by("id", "desc"); 

			$query = $this->db->get();

			return $query->result();

		}

		



		public function findAllUserRegData($where = array(), $setLimit, $onset = 0)

		{

			$this->db->select('all_user_registration.*, country_manage.country_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->where($where);

			$this->db->order_by("id", "desc"); 

			$this->db->limit($setLimit, $onset); 

			$query = $this->db->get();

			return $query->result();

		}

		

		

		

		public function findAllLoginUser($id)

		{

			$this->db->select('all_user_registration.*, country_manage.country_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->where_not_in('all_user_registration.id', $id);

			$this->db->where('all_user_registration.online_status', "active");

			$this->db->order_by("id", "asc"); 

			$query = $this->db->get();

			return $query->result();

		}





		public function findAllSearchLiveUser($user, $skipUser)

		{

			$this->db->select('all_user_registration.*, country_manage.country_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			//$this->db->where('all_user_registration.name', $user);

			$this->db->like('all_user_registration.name', $user);

			$this->db->where_not_in('all_user_registration.name', $skipUser);

			$this->db->order_by("id", "asc"); 

			$query = $this->db->get();

			return $query->result();

		}

		

		

		public function countAll($where = array())

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where($where);			

			return $this->db->count_all_results();

		}



		public function countAllActiveUser($activeUserId)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where_not_in('all_user_registration.id', $activeUserId);	

			$this->db->where('all_user_registration.online_status', "active");	

			return $this->db->count_all_results();

		}	

			

		

		public function findByCurrentGenUser($currentUser)

		{

			$this->db->select('all_user_registration.*, general_user_details.father_name, general_user_details.mother_name,

			 general_user_details.nationality, general_user_details.gender, general_user_details.religion, general_user_details.mobile, general_user_details.address, general_user_details.address_permanent, general_user_details.email_permanent, general_user_details.city, general_user_details.city_permanent');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('general_user_details', 'all_user_registration.id = general_user_details.user_id', 'left');

			$this->db->order_by("id", "desc"); 

			$this->db->where('all_user_registration.user_id', $currentUser);

			$query = $this->db->get();

			return $query->row();

		}

		

		public function findByCurrentOrgUser($currentUser)

		{

			$this->db->select('all_user_registration.*, organization_user_details.address, organization_user_details.mobile_com,

			 organization_user_details.contact_persons, organization_user_details.designation, organization_user_details.mobile, organization_user_details.founded, 

			 organization_user_details.type_of_organization, organization_user_details.type_of_ownership, organization_user_details.location_by_google');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('organization_user_details', 'all_user_registration.id = organization_user_details.user_id', 'left');

			$this->db->order_by("id", "desc"); 

			$this->db->where('all_user_registration.user_id', $currentUser);

			$query = $this->db->get();

			return $query->row();

		}

		

		public function findAllGeneralUser()

		{

			$this->db->select('all_user_registration.*, general_user_details.mobile');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('general_user_details', 'all_user_registration.id = general_user_details.user_id', 'left');

			$this->db->order_by("id", "desc"); 

			$this->db->where(array('user_type' => "General user"));

			$query = $this->db->get();

			return $query->result();

		}

		public function findAllOrganizeUser()

		{

			$this->db->select('all_user_registration.*, organization_user_details.mobile');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('organization_user_details', 'all_user_registration.id = organization_user_details.user_id', 'left');

			$this->db->order_by("id", "desc"); 

			$this->db->where(array('user_type' => "Organization User"));

			$query = $this->db->get();

			return $query->result();

		}



		public function findAllGeneralUserEmail()

		{

			$this->db->select('all_user_registration.*');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->order_by("id", "desc"); 

			$this->db->where(array('user_type' => "General user"));

			$query = $this->db->get();

			return $query->result();

		}

		

		public function findAllGeneral($where = array(), $onset = 0)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name');

			$this->db->from(self::TABLE);

			// tabel name, joining condition, left/right/inner/selt. for inner no need to write

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->where(array('user_type' => "General user"));

			$this->db->where($where);

			$this->db->order_by("id", "desc"); 

			$this->db->limit(5, $onset); 

			$query = $this->db->get();

			return $query->result();

		}

		

		public function countAll2($where = array())

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where($where);			

			return $this->db->count_all_results();

		}	

		

		

		

		public function findAllValu($id)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name, 

			general_user_details.father_name, general_user_details.mother_name,

			general_user_details.nationality, general_user_details.gender, general_user_details.religion, general_user_details.mobile, general_user_details.address, general_user_details.address_permanent, general_user_details.email_permanent, general_user_details.city, general_user_details.city_permanent, general_user_details.date_of_birth,

			general_user_details.country_phone, general_user_details.email, general_user_details.country_phone_permanent, general_user_details.mobile_permanent');

			$this->db->from(self::TABLE);

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->join('general_user_details', 'all_user_registration.id = general_user_details.user_id', 'left');

			$this->db->where('all_user_registration.id', $id);

			$query = $this->db->get();

			return $query->row();

		}

		

		public function findAllorgValu($id)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name, city_manage.city_name, 

			organization_user_details.address, organization_user_details.mobile_com, organization_user_details.phone_com, organization_user_details.email_com,

			organization_user_details.contact_persons, organization_user_details.designation,  organization_user_details.mobile, organization_user_details.founded, 

			organization_user_details.type_of_organization, organization_user_details.type_of_ownership, organization_user_details.email,

			organization_user_details.scholarship, organization_user_details.location_by_google, organization_user_details.phone, organization_user_details.mobile');

			$this->db->from(self::TABLE);

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->join('city_manage', 'all_user_registration.city_id = city_manage.id', 'left');

			$this->db->join('organization_user_details', 'all_user_registration.id = organization_user_details.user_id', 'left');

			$this->db->where('all_user_registration.id', $id);

			$query = $this->db->get();

			return $query->row();

		}

		

		public function findByEmail($user_id)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where('user_id', $user_id);

			$query = $this->db->get();

			return $query->row();

		}	

		

		

		public function findAllOrganizeByCountry($country_id)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where(array('country_id' => $country_id, 'user_type' => "Organization User"));

			$query = $this->db->get();

			return $query->result();

		}	

		



		public function findAllCsvData($onset = 0)

		{

			$this->db->select('all_user_registration.*, region_manage.region_name, country_manage.country_name');

			$this->db->join('region_manage', 'all_user_registration.region_id = region_manage.id', 'left');

			$this->db->join('country_manage', 'all_user_registration.country_id = country_manage.id', 'left');

			$this->db->from(self::TABLE);

			$this->db->where(array('dataCsv' => "yes"));

			$this->db->limit(10, $onset); 

			$query = $this->db->get();

			return $query->result();

		}



		public function countAllCsv()

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where(array('dataCsv' => "yes"));			

			return $this->db->count_all_results();

		}

			

		

		

		public function userOldPassChk($user_id, $password)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where(array('user_id' => $user_id, 'password' => $password));

			$query = $this->db->get();

			return $query->row();

		}	

		

		public function findById($id)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where('id', $id);

			$query = $this->db->get();

			return $query->row();

		}

		

		

		

		public function findorgIddeteail($organizeId)

		{

			$this->db->select('all_user_registration.*, country_manage.country_name');

			$this->db->from(self::TABLE);

			$this->db->join('country_manage', 'country_manage.id = all_user_registration.country_id', 'left');

			$this->db->where('all_user_registration.id', $organizeId);

			$query = $this->db->get();

			return $query->row();

		}

		

	







		public function findAllByRegion($country_id)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where('country_id', $country_id);

			$query = $this->db->get();

			return $query->result();

		}

		



        



        public function findAllInfoByCountryId($country_id)

		{

			$List 		= $this->findAllByRegion($country_id);

			

			foreach($List as $model)

			{

				$model->programList     = $this->M_programe_manage->findAllProByOrgId(array("orgnization_id" => $model->id));

			

			}

			return $List;

		}

		







		

		public function findByUserName($UserName)

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where('user_id', $UserName);

			$query = $this->db->get();

			return $query->row();

		}

		

		public function findByGeneralUser()

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where(array('user_type' => "General user"));

			$query = $this->db->get();

			return $query->result();

		}

		

		public function findByOrganizatinUser()

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where(array('user_type' => "Organization User"));

			$query = $this->db->get();

			return $query->result();

		}

		

		public function findAllOrganize()

		{

			$this->db->select('*');

			$this->db->from(self::TABLE);

			$this->db->where(array('user_type' => "Organization User"));

			$query = $this->db->get();

			return $query->result();

		}

		

		

		

		 public function findByAllUserSearch($search_all)

		{

			

			$query = $this->db->query("SELECT all_user_registration.*, 

			general_user_details.mobile, organization_user_details.mobile_com,

			MATCH (all_user_registration.name) AGAINST (+'$search_all') AS R1,

			MATCH (all_user_registration.user_type) AGAINST (+'$search_all') AS R2

			FROM all_user_registration

			LEFT JOIN general_user_details ON all_user_registration.id = general_user_details.user_id

			LEFT JOIN organization_user_details ON all_user_registration.id = organization_user_details.user_id

			WHERE (MATCH (all_user_registration.name) AGAINST (+'$search_all') OR MATCH ( all_user_registration.user_type) AGAINST (+'$search_all'))

			HAVING (R1+R2) > 0

			ORDER BY (R1+R2) DESC");

 			return $query->result();

	

		}

		

		

		public function findByAllJobSearch($search_all)

		{

			

			$query = $this->db->query("SELECT job_post_manage.*, all_user_registration.name, all_user_registration.image, organization_user_details.mobile_com,

			MATCH (job_post_manage.title) AGAINST (+'$search_all') AS R1

			FROM job_post_manage

			LEFT JOIN all_user_registration ON job_post_manage.user_id = all_user_registration.id

			LEFT JOIN organization_user_details ON job_post_manage.user_id = organization_user_details.user_id

			WHERE (MATCH (job_post_manage.title) AGAINST (+'$search_all'))

			HAVING (R1) > 0

			ORDER BY (R1) DESC");

 			return $query->result();

	

		}

		

		public function update($data, $id)

		{

			$this->db->update(self::TABLE, $data, array('id' => $id));        

		}

		

		public function updateGeneral1($data, $current_user_id)

		{

			$this->db->update(self::TABLE, $data, array('id' => $current_user_id));  

		}

		

		public function updateGeneral2($data, $current_user_id)

		{

			$this->db->update(self::TABLE, $data, array('id' => $current_user_id));    

		}

		

		public function updatStatus($data1, $user_autoId)

		{

			$this->db->update(self::TABLE, $data1, array('id' => $user_autoId));        

		}

		

		

		

		

		

		public function destroy($id)

		{

			$this->db->delete(self::TABLE, array('id' => $id));        

		}

			

	}


















