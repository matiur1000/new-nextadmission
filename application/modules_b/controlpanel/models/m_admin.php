<?php

	class M_admin extends CI_Model {
	
		const TABLE	= 'admin';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		public function findByUserName($userName)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('user_name', $userName);
			$query = $this->db->get();
			return $query->row();
		}
		
		
			
	}
?>