<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MenuManage extends MX_Controller {

		static $model 	 = array('M_admin','M_menu_manage', 'M_submenu_manage', 'M_deeper_sub','M_organize_profile_manage');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('session');
			$this->load->library('pagination');
			isAuthenticate();
	
		}
	

		public function index($onset = 0)
	
		{		
			
		 if(isAuthenticate()){
			$data['userName']    = getUserName();
			$data['page_title']  = self::Title;		
			$data['maxSlNo']	= $this->M_menu_manage->findMaxSl();
			$data['menuInfo']	= $this->M_menu_manage->findAllmenu();
			
			/*$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/menuManage/index');
			$config['total_rows'] 	= $this->M_menu_manage->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 5;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config);*/ 
			$this->load->view('controlpanel/menuManage/menuManagePage', $data);
	      }
		}
	
		public function menuList_()
		{
			$data['menuInfo']	= $this->M_menu_manage->findAllmenu();
			$this->load->view('controlpanel/menuManage/menuList', $data);
		}
		
		
		public function subMenuList_()
		{
		   $data['menuInfo']		= $this->M_menu_manage->findAllAdmin();
		   $data['subMenuInfo']		= $this->M_submenu_manage->findAllSubMenu();
		   $this->load->view('controlpanel/menuManage/subMenuList', $data);
		}
		
		public function deeperSubList_()
		{
		   	$data['deeperSubInfo']	= $this->M_deeper_sub->findAllDeeperSub();
			$this->load->view('controlpanel/menuManage/deeperSubList', $data);
		}
		
		
		public function menuEdit()
		{
			$id 			= $this->input->post('id');		
			$menuEditInfo 	= $this->M_menu_manage->findById($id);
					
			echo json_encode($menuEditInfo);
		}
		
		public function menuStore()
			{	
				$id		    		    	= $this->input->post('id');
				$data['sl_no'] 				= $this->input->post('sl_no');
				$data['menu_name'] 			= $this->input->post('menu_name');
				$data['sub_menu_status'] 	= $this->input->post('sub_menu_status');
				$data['menu_title'] 		= $this->input->post('menu_title');
				$data['menu_description'] 	= $this->input->post('menu_description');
				$data['position_top'] 		= $this->input->post('position_top');
				$data['position_footer'] 	= $this->input->post('position_footer');
				$data['status'] 			= $this->input->post('status');
				$image		 				= $this->input->post('menu_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
				$menu_info = $this->M_menu_manage->findById($id);
				
				if( !empty($menu_info->image) && file_exists('./Images/Menu_image/'.$menu_info->image) && !empty($image) ) {					
					unlink('./Images/Menu_image/'.$menu_info->image);	
				}
				   $this->M_menu_manage->update($data, $id);

				} else {	
				   if($data['menu_name'] !=''){			
					 $this->M_menu_manage->save($data);
				   }
				}
				
				$this->menuList_();
			 
			}
		
	
		public function subMenuManage($onset = 0)
	
		{
		 if(isAuthenticate()){
		   $data['userName']    	= getUserName();
		   $data['page_title']  	= self::Title;	
		   $data['maxSlNo']			= $this->M_submenu_manage->findMaxSl();
		   $data['subMenuInfo']		= $this->M_submenu_manage->findAllSubMenu();
		   
		   	/*$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/menuManage/subMenuManage');
			$config['total_rows'] 	= $this->M_submenu_manage->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 5;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config); */
		    $this->load->view('controlpanel/menuManage/subMenuPage', $data);
		   }
		 }
		
		public function deeperSubManage($onset = 0)
	
		{	
		  if(isAuthenticate()){
		    $data['userName']    	= getUserName();
		    $data['page_title']  	= self::Title;	
			$data['maxSlNo']		= $this->M_deeper_sub->findMaxSl();
		   	$data['menuInfo']		= $this->M_menu_manage->findAllAdmin();
		   	$data['deeperSubInfo']	= $this->M_deeper_sub->findAllDeeperSub();
			
			/*$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/menuManage/deeperSubManage');
			$config['total_rows'] 	= $this->M_deeper_sub->countAll();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 5;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
			
			$this->pagination->initialize($config);*/ 
			$this->load->view('controlpanel/menuManage/deeperSubPage', $data);	
	      }
		}
		  
		  
		  public function subMenu()
			{
				$menu_id 	   = $this->input->post('id');
			
				$subMenuList   	   = $this->M_submenu_manage->findAll($menu_id);				
			
				echo '<option value="">Sellect Sub Menu</option>';
				foreach($subMenuList as $v) {
					echo '<option value="'.$v->id.'">'.$v->	sub_menu_name.'</option>';
				}
				
			}
		
		
		 public function deeperSubStore()
			{	
				$id		    		    		= $this->input->post('id');
				$data['sl_no'] 					= $this->input->post('sl_no');
				$data['menu_id'] 				= $this->input->post('menu_id');
				$data['sub_menu_id'] 			= $this->input->post('sub_menu_id');
				$data['deeper_sub_menu_name'] 	= $this->input->post('deeper_sub_menu_name');
				$data['deeper_sub_title'] 		= $this->input->post('deeper_sub_title');
				$data['deeper_sub_description'] = $this->input->post('deeper_sub_description');
				$data['position'] 				= $this->input->post('select_position');
				$data['status'] 				= $this->input->post('status');
				$image		 					= $this->input->post('deeper_image');
				
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
				$deeper_info = $this->M_deeper_sub->findById($id);
				
				if( !empty($deeper_info->image) && file_exists('./Images/Deeper_image/'.$deeper_info->image) && !empty($image) ) {					
					unlink('./Images/Deeper_image/'.$deeper_info->image);	
				}
				$this->M_deeper_sub->update($data, $id);
				} else {
				  if($data['deeper_sub_menu_name'] !=''){				
					$this->M_deeper_sub->save($data);
				  }
				}
				
				$this->deeperSubList_();
			 
			}


			public function userMenuManage()
			{		
				
			 if(isAuthenticate()){
				$data['userName']    = getUserName();
				$data['page_title']  = self::Title;	
				$data['maxSlNo']	    = $this->M_organize_profile_manage->findMaxSl('panel_profile_menu_manage');
				$data['menuInfo']	    = $this->M_organize_profile_manage->findAllMenu('panel_profile_menu_manage');
				
				$this->load->view('controlpanel/menuManage/userMenuPage', $data);
		      }
			}




			public function userMenuList_()
			{
				$data['menuInfo']	    = $this->M_organize_profile_manage->findAllMenu('panel_profile_menu_manage');
				$this->load->view('controlpanel/menuManage/userMenuList', $data);
			}
		

			public function userMenustore()
			{	
				$id		    		    		= $this->input->post('id');
				$data['sl_no'] 					= $this->input->post('sl_no');
				$data['menu_position'] 			= $this->input->post('menu_position');
				$data['menu_name'] 				= $this->input->post('menu_name');
				$data['sub_menu_status'] 		= $this->input->post('sub_menu_status');
				
				
				if(!empty($id)) {
				    $this->M_organize_profile_manage->update2('panel_profile_menu_manage', $id, $data);
				} else {
			      if($data['menu_name'] !=''){				
					$this->M_organize_profile_manage->save('panel_profile_menu_manage', $data);
				   }
				}
				
				$this->userMenuList_();
			 
			}

			public function userMenuedit()
			{
				$id 			= $this->input->post('id');		
				$menuEditInfo 	= $this->M_organize_profile_manage->findById('panel_profile_menu_manage', $id);
						
				echo json_encode($menuEditInfo);
			}


			public function managerSubManage()
			{		
				
			  if(isAuthenticate()){
				$data['userName']    = getUserName();
				$data['page_title']  = self::Title;	
				$data['maxSlNo']	    	= $this->M_organize_profile_manage->findMaxSl('manager_submanu_manage');
				$data['managerMenuInfo']	= $this->M_organize_profile_manage->findBySubMenuStatus('panel_profile_menu_manage');
				$data['menuInfo']	        = $this->M_organize_profile_manage->findAllManagerMenuInfo('manager_submanu_manage');
				
				$this->load->view('controlpanel/menuManage/managerSubMenuPage', $data);
		      }
			}


			public function managerSubMenustore()
			{	
				$id		    		    		= $this->input->post('id');
				$data['sl_no'] 					= $this->input->post('sl_no');
				$data['menu_id'] 				= $this->input->post('menu_id');
				$data['sub_menu_name'] 		    = $this->input->post('sub_menu_name');
				$data['status'] 		        = $this->input->post('stauts');
				if(!empty($id)) {
				    $this->M_organize_profile_manage->update2('manager_submanu_manage', $id, $data);
				} else {
				   if($data['sub_menu_name'] !=''){				
					$this->M_organize_profile_manage->save('manager_submanu_manage', $data);
				   }
				}
				
				$this->managerSubMenuList_();
			 
			}


			public function managerSubMenuList_()
			{
				$data['menuInfo']	        = $this->M_organize_profile_manage->findAllManagerMenuInfo('manager_submanu_manage');
				$this->load->view('controlpanel/menuManage/managerSubMenuList', $data);
			}


			public function managerSubMenuedit()
			{
				$id 			= $this->input->post('id');		
				$menuEditInfo 	= $this->M_organize_profile_manage->findById('manager_submanu_manage', $id);
						
				echo json_encode($menuEditInfo);
			}


				
			public function positioWiseMenu()
			{
				$select_position 	   = $this->input->post('id');
				if($select_position == 'top'){
			
					$topMenuList   	   = $this->M_menu_manage->findAllTopMenu();	
				
					echo '<option value="">Sellect Menu</option>';
					foreach($topMenuList as $v) {
						echo '<option value="'.$v->id.'">'.$v->menu_name.'</option>';
						}
					} else {
						$foooterMenuList   	   = $this->M_menu_manage->findAllFooterMenu();	
						echo '<option value="">Sellect Menu</option>';
						foreach($foooterMenuList as $v) {
							echo '<option value="'.$v->id.'">'.$v->menu_name.'</option>';
						}
					}
			}
			
			
		public function subMenuStore()
			{	
				$id		    		    		= $this->input->post('id');
				$data['sl_no'] 					= $this->input->post('sl_no');
				$data['menu_id'] 				= $this->input->post('menu_id');
				$data['sub_menu_name'] 			= $this->input->post('sub_menu_name');
				$data['deeper_sub_status'] 		= $this->input->post('deeper_sub_status');
				$data['sub_menu_title'] 		= $this->input->post('sub_menu_title');
				$data['sub_menu_description'] 	= $this->input->post('sub_menu_description');
				$data['position'] 				= $this->input->post('select_position');
				$data['status'] 				= $this->input->post('status');
				$image		 					= $this->input->post('sub_image');
				
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
				$sub_info = $this->M_submenu_manage->findById($id);
				
				if( !empty($sub_info->image) && file_exists('./Images/Sub_menu_image/'.$sub_info->image) && !empty($image) ) {					
					unlink('./Images/Sub_menu_image/'.$sub_info->image);	
				}
				$this->M_submenu_manage->update($data, $id);
				} else {
				  if($data['sub_menu_name'] !=''){				
					$this->M_submenu_manage->save($data);
				  }
				}
				
				$this->subMenuList_();
			 
			}
		
		public function userMenudelete()
		{ 		
			$deleteDataList = $this->input->post('user_menu_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				  $this->M_organize_profile_manage->destroy('panel_profile_menu_manage', $deleteId);
				}
			}
		}


		public function managerSubMenudelete()
		{ 		
			$deleteDataList = $this->input->post('manager_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				  $this->M_organize_profile_manage->destroy('manager_submanu_manage', $deleteId);
				}
			}
		}
		
		public function menudelete()
		{ 		
			$deleteDataList = $this->input->post('menu_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){
				  $model	= $this->M_menu_manage->findById($deleteId);
				  if(!empty($model->image)){
					unlink('./Images/Menu_image/'.$only_image);
				  }	  					
				  $this->M_menu_manage->destroy($deleteId);
				}
			}
		}


		public function subMenudelete()
		{ 		
			$deleteDataList = $this->input->post('sub_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){
				  $model	= $this->M_submenu_manage->findById($deleteId);
				  if(!empty($model->image)){
				  	 unlink('./Images/Sub_menu_image/'.$only_image);
				  }	  					
				  $this->M_submenu_manage->destroy($deleteId);
				}
			}
		}
		
	      public function subMenuEdit()
			{
				$id 				= $this->input->post('id');		
				$subMenuEditInfo 	= $this->M_submenu_manage->findById($id); 
				$position           = $subMenuEditInfo->position;
				
				if($position == 'top'){
				  $subMenuEditInfo->menuList =  $this->M_menu_manage->findAllTopMenu();
				  }else{
				  $subMenuEditInfo->menuList =  $this->M_menu_manage->findAllFooterMenu();
				 }
						
				echo json_encode($subMenuEditInfo);
			}
			
			public function deeperSubEdit()
			{
				$id 				= $this->input->post('id');		
				$deeperSubEditInfo 	= $this->M_deeper_sub->findById($id);
				
				$position           = $deeperSubEditInfo->position;
				
				if($position == 'top'){
				  $deeperSubEditInfo->menuList2 =  $this->M_menu_manage->findAllTopMenu();
				  }else{
				  $deeperSubEditInfo->menuList2 =  $this->M_menu_manage->findAllFooterMenu();
				 }
				
				$deeperSubEditInfo->subList =  $this->M_submenu_manage->findAll($deeperSubEditInfo->menu_id);
				
				echo json_encode($deeperSubEditInfo);
			}
			
	   public function deeperSubdelete()
		{ 		
			$deleteDataList = $this->input->post('deep_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){
				  $model	= $this->M_deeper_sub->findById($deleteId);
				  if(!empty($model->image)){
				  	 unlink('./Images/Deeper_image/'.$model->image);
				  }	  					
				  $this->M_deeper_sub->destroy($deleteId);
				}
			}
		}
		
		
			
		

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

