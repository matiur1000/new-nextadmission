<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BasicManage extends MX_Controller {

		static $model 	 = array('M_admin','M_basic_manage');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
	
		}
	

		public function index()
	
		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			$data['page_title']  = self::Title;			

			$data['basicInfo']	= $this->M_basic_manage->findAll();
			$this->load->view('controlpanel/BasicManage/basicManagePage', $data);
	      }
		}
		
		public function basicList_()
		{
			$data['basicInfo']	= $this->M_basic_manage->findAll();
			$this->load->view('controlpanel/BasicManage/basicListPage', $data);
		}
		
		
		
		public function store()
		{	
			$id		    		    	= $this->input->post('id');
			$data['title'] 				= $this->input->post('title');
			$data['footer_description']	= $this->input->post('footer_description');
			$data['image_title']		= $this->input->post('image_title');
			$data['image_alt'] 			= $this->input->post('image_alt');
			$logo_image					= $this->input->post('logo_image');
			$globe_image 				= $this->input->post('globe_image');
			
			if(!empty($logo_image)) $data['logo_image'] = $logo_image;	
			if(!empty($globe_image)) $data['globe_image'] = $globe_image;	
			
			if(!empty($id)) {
				$besic_info = $this->M_basic_manage->findById($id);
				
				if( !empty($besic_info->logo_image) && file_exists('./Images/Basic_image/'.$besic_info->logo_image) && !empty($logo_image) ) {					
					unlink('./Images/Basic_image/'.$besic_info->logo_image);	
				}
				
				if( !empty($besic_info->globe_image) && file_exists('./Images/Basic_image/'.$besic_info->globe_image) && !empty($globe_image) ) {					
					unlink('./Images/Basic_image/'.$besic_info->globe_image);	
				}				
				
				$this->M_basic_manage->update($data, $id);
			} else {
				$this->M_basic_manage->save($data);
			}
 			
			$this->basicList_();
			 
		}
		
		
	
		
		
		
		public function edit()
		{
			$id 				= $this->input->post('id');		
			$basicEditInfo 		= $this->M_basic_manage->findById($id);
					
			echo json_encode($basicEditInfo);
		}
		
		
		
			
		
		public function delete($id)
		{	
			$model	= $this->M_basic_manage->findById($id);
			$only_image  = $model->image;
						
			if(!empty($only_image)){
			unlink('./Images/Basic_image/'.$only_image);
			}
			$this->M_basic_manage->destroy($id);
			 redirect ('controlpanel/basicManage/index');
		}
		

		public function logout()
		{	
			logoutUser();
		}

	

	

}

