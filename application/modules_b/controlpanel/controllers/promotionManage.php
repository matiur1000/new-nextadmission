<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PromotionManage extends MX_Controller {

		static $model 	 = array('M_admin','M_news_and_event_manage','M_advertisement_manage','M_carrer_consultant_manage','M_add_manage','M_programe_manage',
			'M_add_booking','M_region_manage','M_country_manage','M_search_table','M_organize_profile_manage', 'M_all_user_registration');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
			$this->load->library('pagination');
			isAuthenticate();
	
		}
		
		public function index($onset = 0)
	
		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			$data['page_title']  = self::Title;
				
				
			$perPageLimit        	= $this->input->post('perPageLimit');
					
			$data['newsInfo']	 = $this->M_news_and_event_manage->findAll();
			
			$whwre = array('status' => 'Active', 'user_type' => 'Organization User');
			$data['allorguser']	 = $this->M_all_user_registration->findallorg('all_user_registration', $whwre);
			$data['contryname']	 = $this->M_all_user_registration->findallcontry('country_manage');
		
		if($perPageLimit !='all'){
		   if(!empty($perPageLimit)){
				$setLimit = $perPageLimit; 
			} else {
				$setLimit = "30"; 
			}
			
			$data['perPageLimit']   = $perPageLimit;
			$data['addinfo']	    = $this->M_add_manage->adsmanage($setLimit, $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/promotionManage/index');
			$config['total_rows'] 	= $this->M_add_manage->total_rows();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 7;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
			$this->pagination->initialize($config);
				
		 } else {
			    	$data['perPageLimit']   = $perPageLimit; 	
					$data['addInfo']	 	= $this->M_add_manage->findAllData();
					//$data['addinfo']	    = $this->M_add_manage->adsmanage();
			    }
				
			$this->load->view('controlpanel/newsAndAdvertise/promotionPage', $data);
	       }
		   
		}


		
		public function newsList_()
		{
			$data['newsInfo']	= $this->M_news_and_event_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/newsListPage', $data);
		}
		
		
		
		public function store()
		{	
			
			$data['user_id'] 		    = $this->input->post('orgId');
			$data['website']		    = $this->input->post('website');
			$data['country_name'] 		= $this->input->post('pubcount');
			$data['title'] 			    = $this->input->post('addtitle');
			$data['positon'] 			= $this->input->post('position');
			
			$data['add_link'] 			= $this->input->post('addlink');
			
			
			
			$positionSerial             = $this->input->post('positionSerial');
			$positionSerialR			= $this->input->post('positionSerialR');
			$positionSerialL			= $this->input->post('positionSerialL');
			
			if(!empty($positionSerial)){
				$data['serial_no'] 			= $positionSerial;
			}
			else if(!empty($positionSerialR)){
				$data['serial_no'] 			= $positionSerialR;
			}else {
			$data['serial_no'] 			= $positionSerialL;
			}
			
			
			$data['publish_date'] 	    = $this->input->post('startDeta');
			$data['expire_date'] 	    = $this->input->post('experdate');
			$data['description'] 	    = $this->input->post('description');
			$data['manage_date'] 	    = date('Y-m-d');
			$data['status'] 	    	= "notavailable";
			$data['published_status'] 	= "unaproave";
			$data['payment_status'] 	= "unpaid";
			
			$data['add_image']		 	= $this->input->post('add_image');
			$data['add_image1']		 	= $this->input->post('add_image1');
			$data['add_image2']		 	= $this->input->post('add_image2');
			$data['add_image3']		 	= $this->input->post('add_image3');
			
			
			$config['upload_path'] 		= './Images/Add_image/';
			//$config['allowed_types'] = 'mp4|3gp|webm|wmv';
			$config['allowed_types'] 	= '*';
			$config['max_size']			= '0';
			$config['max_width']  		= '0';
			$config['max_height']  		= '0';
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('video_embed')) {
				$uploadData = $this->upload->data();
				echo $data['video_embed'] = $uploadData['file_name'];
			}
				
		  $lastAdId       = $this->M_add_manage->save($data);
				
		redirect('controlpanel/promotionManage'); 
			 
	     }
			
			
			
			
			
			
			
		public function update()
		{
		
			$id		    		    	= $this->input->post('id');
			$data['user_id'] 		    = $this->input->post('orgId');
			$data['title'] 			    = $this->input->post('addtitle');
			$data['positon'] 			= $this->input->post('position');
			$video_embed	    		= $this->input->post('embed_link');
			
			
			$image		 				= $this->input->post('add_image');
			
			if(!empty($image)){
				$data['add_image']  = $image; 
			}
			else {
				$data['video_embed']  = $video_embed; 
			}
			
			if(!empty($id)) {
				$add_info = $this->M_add_manage->findById($id);
				
				if( !empty($add_info->add_image) && file_exists('./Images/News_image/'.$add_info->add_image) && !empty($image) ) {					
					unlink('./Images/Add_image/'.$add_info->add_image);	
				}
			
			
			
			$weblink				= $this->input->post('addlink');
			$details	  			    = $this->input->post('ajaxfilemanagerupdate');
		
			if(!empty($details)){
				$data['description']   = $details; 
			}else {
				$data['add_link']    =   $weblink;
			}
			
			
			
			$webs		                = $this->input->post('website');
			$country		= $this->input->post('publishcounty');
			
			if(!empty($webs)){
				$data['website']     =  $webs;
			} else {
				$data['country_name']   = $country; 
			}
			
			$positionSerial             = $this->input->post('positionSerial');
			$positionSerialR			= $this->input->post('positionSerialR');
			$positionSerialL			= $this->input->post('positionSerialL');
			
			if(!empty($positionSerial)){
				$data['serial_no'] 			= $positionSerial;
			}
			else if(!empty($positionSerialR)){
				$data['serial_no'] 			= $positionSerialR;
			}else {
			$data['serial_no'] 			    = $positionSerialL;
			}
			
			
			$data['publish_date'] 	    = $this->input->post('startDeta');
			$data['expire_date'] 	    = $this->input->post('endDate');
			
		 	  $this->M_add_manage->update2($data, $id);
			
			}
			
			
		redirect('controlpanel/promotionManage'); 
			
			
}
		
						
			
			
			
			
		
		public function proEdit()
		{
			$id 				= $this->input->post('id');
			$result 		    = $this->M_add_manage->findById($id);
			$where 				= array('all_user_registration.id' =>$result->user_id);
			$result->country    =  $this->M_all_user_registration->findAllorgIdbyData($where);
			
			
			echo json_encode($result);
		}
		
		
		

		public function latestNewsManage()
	
		{		
		  if(isAuthenticate()){
			$data['userName']    		= getUserName();
			$data['page_title']  		= self::Title;			

			$data['latestNewsInfo']	 = $this->M_organize_profile_manage->findAllData('breaking_news_manage');
			$this->load->view('controlpanel/newsAndAdvertise/latestNewsManagePage', $data);
	       }
		}






		// org name id by start
		
		public function orgidbyname(){
			$orgId  = $this->input->post('orgId');
			
			$where = array('all_user_registration.id' => $orgId);
			$result = $this->M_all_user_registration->findAllorgIdbyData($where);
			echo $result->name;
		}
		// org name id by end
		
		
		// org contry name id by start
		public function orgidbycontryname(){
			$orgId  = $this->input->post('orgId');
			$where = array('all_user_registration.id' => $orgId);
			$result = $this->M_all_user_registration->findAllorgIdbyData($where);
			echo $result->country_name;
		}
	


		public function chckadddate(){
			$publish_date       = $this->input->post('startDeta');
			$expire_date        = $this->input->post('experdate');
			$select_website     = $this->input->post('website');
		    $country_name       = $this->input->post('pubcount');
			$position           = $this->input->post('position');
			
			$positionSerial  = $this->input->post('positionSerial');
			$positionSerialL  = $this->input->post('positionSerialL');
			$positionSerialR  = $this->input->post('positionSerialR');
			
			if(!empty($positionSerial)){
				$final  = $positionSerial;
			} else if(!empty($positionSerialL)){
				$final   = $positionSerialL;
			}
			else  {
				$final  = $positionSerialR;
			}

			$final;
			$where    				= array('position' => $position, 'serial_no' => $final);
			$adValueChk	        	= $this->M_add_booking->adValueChkByDateadd($where, $publish_date, $expire_date, $select_website, $country_name);
			
			if(!empty($adValueChk))
			{
				echo 1;
			}
			else {
				echo 0;
			}
		
			
	}
		
		
		
		
		
		
		
		
		
		
		public function upDatechckadddate(){
			  $publish_date    = $this->input->post('startDeta');
		      $expire_date     = $this->input->post('endDate');
			 $select_website  = $this->input->post('website');
		     $country_name    = $this->input->post('pubcount');
			 $position        = $this->input->post('position');
			
			$positionSerial  = $this->input->post('positionSerial');
			$positionSerialL  = $this->input->post('positionSerialL');
			$positionSerialR  = $this->input->post('positionSerialR');
			
			if(!empty($positionSerial)) {
				$final  = $positionSerial;
			} else if(!empty($positionSerialL)){
				$final   = $positionSerialL;
			} else  {
				$final  = $positionSerialR;
			}
			
		   $final;
			
			$where    				= array('position' => $position, 'serial_no' => $final);
			$adValueChk	        	= $this->M_add_booking->adValueChkByDateadd($where, $publish_date, $expire_date, $select_website, $country_name);
		
			
			
		
			
			if(!empty($adValueChk))
			{
				echo 1;
			}
			else {
				echo 0;
			}
			
			
		}
		
		
		
		
		public function serchforad()
		{
			$serch = $this->input->post('serch');
			$where = array('all_user_registration.user_id' => $serch);
			$data['result'] = $this->M_add_manage->findadvesrch($where);
			$this->load->view('controlpanel/newsAndAdvertise/admanagSerchPage', $data);
			
		}
		
		public function apprupun()
		{
			$apprustatus = $this->input->post('apprustatus');
			
			$where = array('published_status' => $apprustatus);
			$data['addvierind'] = $this->M_add_manage->findAdaprove($where);
			
			$this->load->view("controlpanel/newsAndAdvertise/admanageByapprovePage", $data);
			
		}
		
		
		public function typedelete()
		{	
			$deleteDataList = $this->input->post('add_id');
			
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				 $this->M_add_manage->destroy($deleteId);
				}
			}
		}
		
		
		public function adIdbyDetails()
		{
			$id 				= $this->input->post('id');
			$where = array('add_manage.id' => $id);
			$data['result'] = $this->M_add_manage->adIdbyDetails($where);
			$this->load->view('controlpanel/newsAndAdvertise/adIdbyDetailPage', $data);
		
		}
		
	

		public function logout()
		{	
			logoutUser();
		}

	

	

}

