<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminManage extends MX_Controller {

		static $model 	 = array('M_admin','M_admin_type_manage','M_all_admin_create','M_payment_gateaway');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('session');
			isAuthenticate();
	
		}
	

		public function index()
	
		{	
		   if(isAuthenticate()){
			$data['page_title']  = self::Title;	
		    $data['userName']    = getUserName();	
			$data['adminTypeInfo']	= $this->M_admin_type_manage->findAll();
			$this->load->view('controlpanel/adminManage/adminManagePage', $data);
	       } 
		}
	
		public function list_()
		{
			$data['adminTypeInfo']	= $this->M_admin_type_manage->findAll();
			$this->load->view('controlpanel/adminManage/adminTypeList', $data);
		}
		
		
		public function adminlist_()
		{
			$data['adminTypeInfo']	= $this->M_admin_type_manage->findAll();
			$data['adminCreatInfo']	= $this->M_all_admin_create->findAll();	
			$this->load->view('controlpanel/adminManage/adminList', $data);
		}
		
		public function paymentlist_()
		{
			$data['paymentInfo']	= $this->M_payment_gateaway->findAll();	
			$this->load->view('controlpanel/adminManage/paymentList', $data);
		}
		
		
		public function edit()
		{
			$id 				= $this->input->post('id');		
			$adminTypeEditInfo 	= $this->M_admin_type_manage->findById($id);
					
			echo json_encode($adminTypeEditInfo);
		}
		
		public function store()
			{	
				$id		    		    = $this->input->post('id');
				$data['admin_type'] 	= $this->input->post('admin_type');
				$data['status']			= $this->input->post('status');
				
				
				if(!empty($id)) {
					$this->M_admin_type_manage->update($data,$id);
					}else{
                     if($data['admin_type'] !=''){
					     $this->M_admin_type_manage->save($data);
					  }
					}
					$this->list_();	
				
			}
		
	
		public function createAndPermission()
	
		{	
		  if(isAuthenticate()){
			$data['page_title']  = self::Title;	
		    $data['userName']    = getUserName();
			$data['adminTypeInfo']	= $this->M_admin_type_manage->findAll();
			$data['adminCreatInfo']	= $this->M_all_admin_create->findAll();	
			$this->load->view('controlpanel/adminManage/createAndPermissionPage', $data);	
	      }
		}
		
		
		public function adminCreatStore()
			{	
				$id		    		    	= $this->input->post('id');
				$data['admin_type_id'] 		= $this->input->post('admin_type_id');
				$data['admin_name']			= $this->input->post('admin_name');
				$data['contract_no'] 		= $this->input->post('contract_no');
				$data['email']				= $this->input->post('email');
				$data['password'] 			= $this->input->post('password');
				$data['permission_menu']	= $this->input->post('permission_menu');
				
				
				if(!empty($id)) {
					$this->M_all_admin_create->update($data,$id);
					}else{
				    if($data['admin_name'] !='' && $data['email'] !=''){
					  $this->M_all_admin_create->save($data);
				     }
					}
					$this->adminlist_();	
				
			}
			
		public function adminCreatedit()
		{
			$id 					= $this->input->post('id');		
			$adminCreatEditInfo 	= $this->M_all_admin_create->findById($id);
					
			echo json_encode($adminCreatEditInfo);
		}		

		public function admindelete()
		{ 		
			$deleteDataList = $this->input->post('creat_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				  $this->M_all_admin_create->destroy($deleteId);
				}
			}
		}
		
		
		public function typedelete()
		{	
			$deleteDataList = $this->input->post('admin_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				  $this->M_admin_type_manage->destroy($deleteId);
				}
			}
		}
		
		public function paymentGetaway()
	
		{	
		  if(isAuthenticate()){
			$data['page_title']  = self::Title;	
		    $data['userName']    = getUserName();
			$data['paymentInfo']	= $this->M_payment_gateaway->findAll();
			$this->load->view('controlpanel/adminManage/paymentGetawayPage', $data);	
	       }
		}
		
		public function paymentStore()
			{	
				$id		    		    	= $this->input->post('id');
				$data['premium_user_type'] 	= $this->input->post('premium_user_type');
				$data['amount']				= $this->input->post('amount');
				
				
				if(!empty($id)) {
					$this->M_payment_gateaway->update($data,$id);
					}else{
					$this->M_payment_gateaway->save($data);
					}
					$this->paymentlist_();	
				
			}
		
	      public function paymentEdit()
			{
				$id 				= $this->input->post('id');		
				$paymentEditInfo 	= $this->M_payment_gateaway->findById($id);
						
				echo json_encode($paymentEditInfo);
			}
			
		  public function paymentDelete($id)
			{	
				$this->M_payment_gateaway->destroy($id);
				$this->paymentGetaway();
			}
		
			
		

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

