<table id="inputTable" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace allCheck" />
					<span class="lbl"></span></label>
					<label class="pos-rel">
					<a class="red" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					<span class="lbl"></span></label>
				</label>														</th>
			<th>Admin Type Name</th>														
			<th class="hidden-480">Admin Name </th>
			<th class="hidden-480">Email</th>
			<th class="hidden-480">Mobile</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
	<?php 
	// print_r($adminCreatInfo);
	 $i = 0;
	  foreach ($adminCreatInfo as $v){
	     $id  		    = $v->id;
	?>
		<tr>
			<td class="center">
				<label class="pos-rel">
					<input type="checkbox" name="creat_id[]" class="ace" value="<?php echo $id ?>" />
					<span class="lbl"></span>															</label>														</td>

			<td>
				<a href="#"><?php echo $v->admin_type; ?></a>														</td>														
			<td class="hidden-480"><?php echo $v->admin_name; ?></td>
			<td class="hidden-480"><?php echo $v->email; ?></td>
			<td class="hidden-480"><?php echo $v->contract_no; ?></td>	
			<td>
				<div class="hidden-sm hidden-xs action-buttons">
					<a class="green" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

																				</div>
																		</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<script>
	
	 //check row For delete
	    $(document).on("click", ".allCheck", function(e){
	   	   var allChecked = this.checked; 
	       var parent = $(this).parents('#inputTable');
	       if(allChecked){
	         parent.find('input[name="creat_id[]"]').prop('checked', true);
	       } else {
	         parent.find('input[name="creat_id[]"]').prop('checked', false);
	       }
	    });
    
	//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/adminManage/adminCreatedit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#admin_type_id').val(data.admin_type_id);
					$('#admin_name').val(data.admin_name);
					$('#contract_no').val(data.contract_no);
					$('#email').val(data.email);
					$('#password').val(data.password);
					$('#permission_menu').val(data.permission_menu);
				}
			});
			
			e.preventDefault();
		});
		
		
		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/adminManage/admindelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="creat_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});
	
	</script>