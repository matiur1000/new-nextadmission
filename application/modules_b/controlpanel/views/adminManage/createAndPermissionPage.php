<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<style>
			body{ font-family: Tahoma; font-size:13px;}
			.red{
			font:Arial, Helvetica, sans-serif;
			font-size:13px;
			font-weight:normal;
			color:#FF0000;
			}
			.green{
			font:Arial, Helvetica, sans-serif;
			font-size:13px;
			font-weight:normal;
			color:#3300FF;
			}
			.yellow{
			font:Arial, Helvetica, sans-serif;
			font-size:13px;
			font-weight:normal;
			color:#003300;
			}
		 </style>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Admin Manage </a>
							</li>
							<li class="active">All Admin Creat Manage </li>
						</ul><!-- /.breadcrumb -->
						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>
															</label>														</th>
														<th>Admin Type Name</th>														
														<th class="hidden-480">Admin Name </th>
														<th class="hidden-480">Email</th>
														<th class="hidden-480">Mobile</th>
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($adminCreatInfo);
												 $i = 0;
												  foreach ($adminCreatInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="creat_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span>															</label>														</td>

														<td>
															<a href="#"><?php echo $v->admin_type; ?></a>														</td>														
														<td class="hidden-480"><?php echo $v->admin_name; ?></td>
														<td class="hidden-480"><?php echo $v->email; ?></td>
														<td class="hidden-480"><?php echo $v->contract_no; ?></td>	
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

																															</div>
																													</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
								<form id="addForm" action="<?php echo site_url('controlpanel/adminManage/adminCreatStore'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="border-bottom:3px solid #FF0000">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">Admin create And PermissionPage</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                                <div class="col-xs-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label for="admin_type">Sellect Admin Type</label>        
                                                                        <div>
                                                                           <select class="form-control" id="admin_type_id" name="admin_type_id"  tabindex="1" required>
                                                                                <option value="" selected>Sellect Admin Type</option>
																				<?php foreach ($adminTypeInfo as $v){?>
																				<option value="<?php echo $v->id; ?>"><?php echo $v->admin_type; ?></option>
																				<?php } ?>
                                                                           </select>
                                                                        </div>
                                                                    </div>
            
                                                                    <div class="space-4"></div>
																	
																	<div class="form-group">
                                                                        <label for="admin_name">Admin Name</label>        
                                                                        <div>
                                                                           <input type="text" id="admin_name" placeholder="Admin Name" name="admin_name"
                                                                            tabindex="2" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>
																	
																	<div class="form-group">
                                                                        <label for="contract_no">Contact No</label>        
                                                                        <div>
                                                                           <input type="text" id="contract_no" placeholder="Contact No" name="contract_no"
                                                                            tabindex="3" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>
																	
																	<div class="form-group">
                                                                        <label for="email">Email</label>        
                                                                        <div>
                                                                           <input type="text" id="email" placeholder="Email" name="email"
                                                                            tabindex="4" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>
																	
																	<div class="form-group">
                                                                        <label for="permission_menu">Password</label>
            
                                                                        <div>
                                                                           <input type="password" id="password" placeholder="Password" name="password"
                                                                            tabindex="5" class="form-control password" required /> 
                                                                            <p class="first"></p>
                                                                        </div>
                                                                    </div>      
																	
																	<div class="space-4"></div>
																	
																	<div class="form-group">
                                                                        <label for="permission_menu">Confirm Password</label>
            
                                                                        <div>
                                                                           <input type="password" id="conform_password" placeholder="Conform Password" name="conform_password" 
																		   tabindex="6" class="form-control conformpassword" /> 
																		   <p class="second"></p>
                                                                        </div>
                                                                    </div>      
																	
																	<div class="space-4"></div>
            
                                                                    <div class="form-group">
                                                                        <label for="permission_menu">Permission Menu</label>
            
                                                                        <div>
                                                                           <input type="text" id="permission_menu" placeholder="Permission Menu" name="permission_menu"
                                                                            tabindex="7" class="form-control" required /> 
                                                                        </div>
                                                                    </div>        
                                                                </div>
                                                            </div>
                                                        </div>
            
                                                        <div class="modal-footer">
                                                            <button class="btn btn-sm" data-dismiss="modal">
                                                                <i class="ace-icon fa fa-times"></i>
                                                                Cancel
                                                            </button>
            
                                                            <button id="submit" class="btn btn-sm btn-primary" type="submit">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                <span class="update">Save</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
<script>
    //check row For delete
    $(document).on("click", ".allCheck", function(e){
   	   var allChecked = this.checked; 
       var parent = $(this).parents('#inputTable');
       if(allChecked){
         parent.find('input[name="creat_id[]"]').prop('checked', true);
       } else {
         parent.find('input[name="creat_id[]"]').prop('checked', false);
       }
    });
	
	//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], input[type=password], textarea").val("");
				}
			});
			
			e.preventDefault();
		});

     // COUNT PASS
		$(".password").keyup (function(){
			var len = $(this).val().length;
		    
			if(len<=1){
			$(".first").text("");
			$(".first").removeClass("red");
			$(".first").removeClass("yellow");
			$(".first").removeClass("green");
			
		  } else if(len<=4){
		    $(".first").text("Very Weak");
			$(".first").addClass("red");
			$(".first").removeClass("yellow");
			$(".first").removeClass("green");
			
		   } else if(len<=8){
		    $(".first").text("Good");
			$(".first").addClass("green");
			$(".first").removeClass("yellow");
			$(".first").removeClass("red");
			
		   } else if(len<=9){
		   $(".first").text("Strong");
		   $(".first").addClass("yellow");
		   $(".first").removeClass("green");
		   $(".first").removeClass("red");
		   }
	});
		
		//pass match
		
		$(".conformpassword").keyup (function(){
		 var conpass = $(".conformpassword").val();
		 var Pass = $(".password").val();

		 if(conpass)
		    {
			  if(conpass != Pass){
			   $(".second").text("Your New Password and Confirm Password donot match!");
			   $(".second").addClass("red");
			   $(".second").removeClass("green");
			    
			  } else {
			   $(".second").text("Password Match");
			   $(".second").removeClass("red");
			   $(".second").addClass("green");
			  }
		    } else {
		       $(".second").text("");
			   $(".second").removeClass("red");
			   $(".second").removeClass("green");
		    }
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/adminManage/adminCreatedit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#admin_type_id').val(data.admin_type_id);
					$('#admin_name').val(data.admin_name);
					$('#contract_no').val(data.contract_no);
					$('#email').val(data.email);
					$('#password').val(data.password);
					$('#permission_menu').val(data.permission_menu);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});


		$('.red').on('click', function(e) {
		       var deleteDta = $("#inputTable input").serializeArray();
		       console.log(deleteDta);
				$.ajax({
				url: "<?php echo site_url('controlpanel/adminManage/admindelete') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					  $("#inputTable").find('input[name="creat_id[]"],.allCheck').prop('checked', false);
					  location.reload();
				   }
			    });

			   e.preventDefault();
				
			});
				
				$(document).on("click", ".blue", function(e){
                   $("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
				
	</script>
	</body>
</html>
