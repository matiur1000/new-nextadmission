<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">Ad Manage </li>
						</ul><!-- /.breadcrumb -->
						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">
										    <form action="<?php echo site_url('controlpanel/newsAndAdvertiseManage/addManage'); ?>" method="POST">
										    <div style="float:left; display:inline">Display &nbsp;
										      
											    <select name="perPageLimit" id="perPageLimit" onChange="this.form.submit()">
											        <option <?php if($perPageLimit =='30'){ ?> selected ="selected" <?php } ?> value="30">30</option>
											        <option <?php if($perPageLimit =='50'){ ?> selected ="selected" <?php } ?> value="50">50</option>
											        <option <?php if($perPageLimit =='100'){ ?> selected ="selected" <?php } ?> value="100">100</option>
											        <option <?php if($perPageLimit =='all'){ ?> selected ="selected" <?php } ?> value="all">all</option>
											    </select> &nbsp; records
											</div>	
											</form>												
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i> 	 </a>
										</div>
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="24" class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>	</th>
														<th width="207" class="hidden-480">Title</th>
														<th width="65" class="hidden-480">Ad Image </th>
														<th width="53" class="hidden-480">Position</th>
														<th width="37" class="hidden-480">Sl No </th>
														<th width="42" class="hidden-480">Status</th>
														<th width="56">Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($addInfo);
												if(isset($addInfo)) {
                                                 $i = $onset + 1;
												  foreach ($addInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="add_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span></label></td>

														<td class="hidden-480"><a href="#"><?php echo $v->	title; ?></a></td>
														<td class="hidden-480"><img src="<?php echo base_url("Images/Add_image/$v->add_image"); ?>" height="50" width="50" /></td>
														<td class="hidden-480"><?php echo $v->positon; ?></td>
														<td class="hidden-480"><?php echo $v->serial_no; ?></td>
														<td class="hidden-480"><?php echo $v->status; ?></td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i></a>

																</div>

															</td>
													</tr>
													<?php } } ?>
													<tr>
													  <td colspan="7" align="right" class="text-right">
													       <label class="pos-rel"><span class="lbl"></span>
													        <ul class="pager">
															    <li><?php echo $this->pagination->create_links(); ?></li>
															</ul>
													   </td>
												  </tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
									
									<div id="modal-form" class="modal" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content" style="z-index:10000">
												<div class="modal-header" style="border-bottom:3px solid #FF0000">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="blue bigger">Promotion Manage</h4>
												</div>

												<div id="inputTable" class="modal-body">
												   <input type="hidden" name="id" id="id" value="" />
													<div class="row">
														<div class="col-xs-12 col-sm-12">
															<div id="sweb" class="form-group">
																<label for="company_name">Select Website</label>

																<div>
																	<select class="form-control" id="select_website" name="select_website"  tabindex="1"  required>
																		<option value="" selected>Select Website</option>
																		<option value="main">Main Website</option>
																		<option value="country">Country Website</option>
																	</select>
																</div>
															</div>
															
															<div id="selectCountry" class="form-group" style="display:none">
																<label for="country_name">Select Country</label>

																<div>
																	<select class="form-control" id="country_name" name="country_name"  tabindex="2">
																		<option value="" selected>Select Country</option>
																	  <?php 
																			foreach($allCountry as $v){
																  
																		 ?>
																		  <option value="<?php echo $v->country_name; ?>"><?php echo $v->country_name; ?></option>
																		<?php } ?>
																	</select>
																</div>
															</div>

															<div id="spos" class="form-group">
																<label for="title">Select Position</label>

																<div>
																	<select class="form-control" id="positon" name="positon"  tabindex="3" required>
																		<option value="" selected>Select Position</option>
																		<option value="Top">Top</option>
																		<option value="Left">Left</option>
																		<option value="Right">Right</option>
																    </select>
																</div>
															</div>


															<div id="sl" class="form-group">
															  <label for="positon">Select Serial</label>        
																<div>
																   <select class="form-control" id="serial_no" name="serial_no"  tabindex="4" required>
																	   <option value="" selected>Select Serial</option>
																   </select>
																</div>
															</div>
															
															<div id="ingroup" class="form-group">
															  <label for="serial_no">Select Date</label>        
																<div>
																   <div class="input-group">
																      <input type="text" class="form-control date-picker" name="publish_date" id="publish_date" placeholder="Published Date" aria-describedby="basic-addon1" tabindex="5" data-date-format="yyyy-mm-dd" required>
																	  <span class="input-group-addon" id="basic-addon1">To</span>
																	  <input type="text" class="form-control date-picker" name="expire_date" id="expire_date" placeholder="Expire Date" aria-describedby="basic-addon1" tabindex="6" data-date-format="yyyy-mm-dd" required>
																    </div>
																</div>
															</div>


															<div id="searchHide" class="form-group">
																<div>
																  <button id="next" class="btn btn-sm btn-primary">
																	<i class="glyphicon glyphicon-search"></i> 
																	Search
																  </button> &nbsp; <span class="bookMsg"></span> 
																</div>
															</div>

                                                        


                                                        <div id="adSubmit" style="display:none">
															<div class="form-group">
																<label for="title">Title</label>

																<div>
																	<input type="text" class="form-control" id="title" name="title" placeholder="Title" tabindex="11">
																</div>
															</div>
															
															<div class="row">
																<div class="col-sm-12">
																  <div class="form-group">
																	<label for="com_logo">Ad Image</label>
																	   <div>
																		 <div class="attachmentbody" data-target="#add_image" data-type="add_image">
																			<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
																		  </div> 
																		   <input name="add_image" id="add_image" type="hidden" value="" required />                                                                                
																		</div>
																	 </div>
																  </div>
															 </div>

															<div class="space-4"></div>


															<div id="mng" class="form-group">
																<label for="deadline_date">Manage Date</label>

																<div>
							  										<input type="text" class="form-control date-picker" id="manage_date" name="manage_date" placeholder="Manage Date" tabindex="9" data-date-format="yyyy-mm-dd" >
																	
																</div>
															</div>


															<div class="form-group">
																<label for="deadline_date">Deadline</label>

																<div>
																	<input type="text" class="form-control date-picker" id="deadline_date" name="deadline_date" placeholder="Deadline" tabindex="10" data-date-format="yyyy-mm-dd">
																</div>
															</div>

															
															<div class="form-group">
																<label for="title">Add Url</label>

																<div>
																	<input type="text" name="add_link" id="add_link" placeholder="Add Url" tabindex="1" 
																	class="form-control" />
																</div>
															</div>

															<div class="form-group">
															  <label for="description">Description</label>        
																<div>
																   <textarea class="form-control" rows="10" placeholder="*..Description" 
																   tabindex="8" name="description" id="description"></textarea>
																</div>
															  </div>
														    </div>


                                                       </div>



													</div>
												</div>

												<div id="footer" class="modal-footer" style="display:none">
													<button class="btn btn-sm" data-dismiss="modal">
														<i class="ace-icon fa fa-times"></i>
														Cancel
													</button>

													<button id="finalBooking"  class="btn btn-sm btn-primary">
														<i class="ace-icon fa fa-check"></i>
														Save
													</button>
												</div>


												<div id="footer2" class="modal-footer" style="display:none">
													<button class="btn btn-sm" data-dismiss="modal">
														<i class="ace-icon fa fa-times"></i>
														Cancel
													</button>

													<button id="adUpdate"  class="btn btn-sm btn-primary">
														<i class="ace-icon fa fa-check"></i>
														Update
													</button>
												</div>
												
											</div>
										</div>
									</div>
							
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>

<script>

       $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="add_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="add_id[]"]').prop('checked', false);
           }
       });

       $('.date-picker').datepicker({
			autoclose: true	  
		});	
		


        // Positon Wise Serial No
		$("#positon").change(function() {
		var positon = $("#positon").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('organizationUserHome/homePageSerialNo'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : positon },
			dataType : "html",
			success : function(data) {			
				$("#serial_no").html(data);
			}
		  });
		
	  });


		// Positon Wise Serial No
		$("#select_website").change(function(){
		  var selectValue = $("#select_website").val();	
		  
		  if(selectValue == 'country'){
		    $("#selectCountry").css("display", "block"); 
		  } else {
		     $("#selectCountry").css("display", "none"); 
		  }
		
	  });



		$("#next").click(function(e)
		{
			
			var postData 	= $("#inputTable input, select, textarea").serializeArray();
			var searchuRL  = "<?php echo site_url('organizationUserHome/adBookingChk'); ?>";

			console.log(postData);
			$.ajax(
			{
				url : searchuRL,
				type: "POST",
				data : postData,
				success:function(data){
					 if(data==1){
						$(".bookMsg").text("Sorry! the ad is booked");
						$(".bookMsg").css("color", "red");
					} else {
						$("#searchHide").css("display", "none");
						$("#adSubmit").css("display", "block");
						$("#footer").css("display", "block");
						$("#select_website").attr('readonly', true);
						$("#region").attr('readonly', true);
						$("#positon").attr('readonly', true);
						$("#serial_no").attr('readonly', true);
						$("#publish_date").attr('readonly', true);	
						$("#expire_date").attr('readonly', true);	
						 
					}
			  	}


			});
			e.preventDefault();
			
		});	




       $("#finalBooking").click(function(e)
		{
			
			var postData 	= $("#inputTable input, select, textarea").serializeArray();
			var searchuRL  	= "<?php echo site_url('organizationUserHome/addStore'); ?>";
			//var successuRL  = "<?php echo site_url('organizationUserHome/addManageFinal'); ?>";
			console.log(postData);
			$.ajax(
			{
				url : searchuRL,
				type: "POST",
				data : postData,
				success:function(data){				
					 $("#inputTable input[type='text'], #inputTable input[type='hidden'], #inputTable textarea").val("");
					 location.reload();
					 
			  	}

			});

			e.preventDefault();
			
		});	
	
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/addEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					$('#id').val(data.id);
					$('#title').val(data.title);
					$('#deadline_date').val(data.deadline_date);
					$('#add_link').val(data.add_link);
					$('#description').val(data.description);

					$("#adSubmit").css("display", "block"); 
					$("#selectCountry").css("display", "none"); 
					$("#sweb").css("display", "none"); 
					$("#spos").css("display", "none"); 
					$("#sl").css("display", "none"); 
					$("#ingroup").css("display", "none"); 
					$("#mng").css("display", "none");  
					$("#searchHide").css("display", "none"); 
					$("#footer2").css("display", "block"); 
					
				}
			});
			
			e.preventDefault();
		});



       $("#adUpdate").click(function(e)
		{
			
			var postData 	= $("#inputTable input, select, textarea").serializeArray();
			var searchuRL  	= "<?php echo site_url('controlpanel/newsAndAdvertiseManage/addUpdate'); ?>";
			//var successuRL  = "<?php echo site_url('organizationUserHome/addManageFinal'); ?>";
			console.log(postData);
			console.log(searchuRL);
			$.ajax(
			{
				url : searchuRL,
				type: "POST",
				data : postData,
				success:function(data){				
					 $("#inputTable input[type='text'], #inputTable input[type='hidden'], #inputTable textarea").val("");
					 location.reload();
					 
			  	}

			});

			e.preventDefault();
			
		});	



       $('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/addDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="add_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});				
				
	</script>
	</body>
</html>
