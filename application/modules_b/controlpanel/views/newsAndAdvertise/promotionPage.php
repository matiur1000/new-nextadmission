<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">

		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script> 
		 <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "exact",
			elements : "ajaxfilemanager",
			theme : "advanced",
			// TO GET THE FULL IMAGE URL
			relative_urls: false,
    		remove_script_host: false,
			/****************/
			setup : function(ed) {
			      ed.onKeyUp.add(function(ed, l) {
			         tinyMCE.triggerSave();	                    
			      });
			},
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",

			theme_advanced_buttons1_add_before : "newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
			theme_advanced_buttons2_add_before: "cut,copy,separator,",
			theme_advanced_buttons3_add_before : "",
			theme_advanced_buttons3_add : "media",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "ajaxfilemanager",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php");
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php"),
                width: 482,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
		}
	</script>
	
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "exact",
			elements : "ajaxfilemanagerupdate",
			theme : "advanced",
			// TO GET THE FULL IMAGE URL
			relative_urls: false,
    		remove_script_host: false,
			/****************/
			setup : function(ed) {
			      ed.onKeyUp.add(function(ed, l) {
			         tinyMCE.triggerSave();	                    
			      });
			},
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",

			theme_advanced_buttons1_add_before : "newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
			theme_advanced_buttons2_add_before: "cut,copy,separator,",
			theme_advanced_buttons3_add_before : "",
			theme_advanced_buttons3_add : "media",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "ajaxfilemanagerupdate",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php");
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php"),
                width: 482,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
		}
	</script>

		<style>
			.bookMsg{
				font-family:Arial, Helvetica, sans-serif;
				font-weight:800;
				color:red;
			}
		</style>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?>	
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Promotion Manage </a>
							</li>
							
						</ul><!-- /.breadcrumb -->
						
					</div>
<style>
.pagi a {
	color: black;
 	border: 1px solid #ddd;
	padding: 8px 16px 8px !important;
	border-radius : 0px !important;
}

.pagi strong {
	background-color: #4CAF50;
    color: white;
	padding: 8px 16px;
    border: 1px solid #4CAF50;
}
</style>
					<div class="page-content">
					
					
					
					
					
					<div class="col-md-10">
						<div class="container well" style="display:none;">
						<form id="addForm" action="<?php echo site_url('controlpanel/promotionManage/store'); ?>" method="post" enctype="multipart/form-data">
							<div class="row">
							
								<div class="col-md-4 form-group">     
									<div>
									   <select class="form-control orgId" id="orgId" class="orgId" name="orgId"  tabindex="7" required>
										<option value="" selected>Select Organization Id</option>
										<?php foreach($allorguser as $v){?> 
										<option value="<?php echo $v->id; ?>"><?php echo $v->user_id; ?></option>
										<?php }?>
									   </select>
									</div>
								</div>
										
										
												
								<div class="col-md-4 form-group">
									<div>
						<input type="text" name="orgname" id="orgname" placeholder="Organization Name" tabindex="1" class="form-control orgname" required="" disabled="disabled">
									</div>
								</div>
								
								<div class="col-md-3 form-group">
									<div>
					<input type="text" name="orgcounty" id="orgcounty" placeholder="Origin Country" tabindex="1" class="form-control orgcounty" required="" disabled="disabled">
									</div>
								</div>
							</div> <!--End form row 1-->
							<hr>
							
							<div class="row">
								<div class="col-md-12 form-group">
									<div>
										<h3 class="position_text text-center">Position</h3>
									</div>
								</div>
							</div> <!--End form row 2-->
							
							<div class="row">
								<div class="col-md-4 form-group"> 
									<div>
									   <select class="form-control website" id="website" name="website" tabindex="7" required="">
										<option value="" selected="">Select Website position</option>
										 <option value="main">Main Website</option>
										 <option value="country">Country Website</option>
									   </select>
									</div>
								</div>
								
								<div class="col-md-4 form-group">
										<div>
										   <select class="form-control" id="pubcount" name="pubcount"  tabindex="7">
											<option value="" selected>Select Publish Country</option>
												<?php foreach($contryname as $v){?> 
												  <option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
												<?php } ?>
										   </select>
										</div>
									</div>
								
								<div class="col-md-3 form-group"> 
									<div>
									   <select class="form-control posishon" id="position" name="position" tabindex="7" required="">
											<option value="" selected="">Select Position</option>
											<option value="top">Top</option>
											<option value="left">Left</option>
											<option value="right">Right</option>
									   </select>
									</div>
								</div>
								
							
								
								
								<div class="col-md-3 form-group top" style="display:none;">    
									<div>
									   <select class="form-control" id="positionSerial" name="positionSerial" tabindex="7">
											<option value="" selected="">Position Serial</option>
											<option value="1">1</option>
											<option value="2">2</option>
									   </select>
									</div>
								</div>
								
								
								
								
								<div class="col-md-3 form-group left" style="display:none;">    
									<div>
									   <select class="form-control" id="positionSerialL" name="positionSerialL" tabindex="7">
											<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
									   </select>
									</div>
								</div>
								
								
								<div class="col-md-3 form-group right" style="display:none;">    
									<div>
									   <select class="form-control" id="positionSerialR" name="positionSerialR" tabindex="7">
											<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
													<option value="13">13</option>
													<option value="14">14</option>
													<option value="15">15</option>
													<option value="16">16</option>
													<option value="17">17</option>
													<option value="18">18</option>
													<option value="19">19</option>
													<option value="20">20</option>
									   </select>
									</div>
								</div>
							
								
								
								
							</div> <!--End form row 3-->
							<hr>
							
							
							
							<div class="row">
								<div class="col-sm-2">
								  <div class="form-group images">
									<label for="news_image">Image one</label>
									   <div>
										 <div class="attachmentbody" data-target="#add_image" data-type="add_image">
											<img class="upload" src="<?php echo base_url('Images/Add_image/img.png') ?>" />
										  </div> 
										   <input name="add_image" id="add_image" type="hidden" value="" required />                                                                                
										</div>
									 </div>
								  </div>
								  
								 <div class="col-sm-2">
								  <div class="form-group images">
									<label for="news_image">Image two</label>
									   <div>
										 <div class="attachmentbody" data-target="#add_image1" data-type="add_image1">
											<img class="upload" src="<?php echo base_url('Images/Add_image/img.png') ?>" />
										  </div> 
										   <input name="add_image1" id="add_image1" type="hidden" value=""  />                                                                                
										</div>
									 </div>
								  </div>
								<div class="col-sm-2">
								  <div class="form-group images">
									<label for="news_image">Image three</label>
									   <div>
										 <div class="attachmentbody" data-target="#add_image2" data-type="add_image2">
											<img class="upload" src="<?php echo base_url('Images/Add_image/img.png') ?>" />
										  </div> 
										   <input name="add_image2" id="add_image2" type="hidden" value="" required />                                                                                
										</div>
									 </div>
								  </div>
								 <div class="col-sm-2">
								  <div class="form-group images">
									<label for="news_image">Image four</label>
									   <div>
										 <div class="attachmentbody" data-target="#add_image3" data-type="add_image3">
											<img class="upload" src="<?php echo base_url('Images/Add_image/img.png') ?>" />
										  </div> 
										   <input name="add_image3" id="add_image3" type="hidden" value="" required />                                                                                
										</div>
									 </div>
								  </div>
								  
								  
								  <div class="col-sm-2">
								  <div class="form-group images">
									<label for="news_image">Video Upload</label>
									   <div>
										   <input type="file" id="video_embed" name="video_embed" tabindex="8" />                                                                                
										</div>
									 </div>
								  </div>
								  
								  
								  
								  
							 </div>
							 
							 
							 
							 
														 
														 
							<hr>
							
							<div class="row">
							
								<div class="col-md-4 form-group">
									<div>
										<input type="text" name="addtitle" id="addtitle" placeholder="Ad Title" tabindex="1" class="form-control" required="">
									</div>
								</div>
				
								
								
								
								
								
								
								
								
								
								
								
								<div class="col-md-4 form-group">   
									<input name="startDeta" type="text" id="startDeta" placeholder="Start date" class="form-control date-picker" data-date-format="yyyy-mm-dd">
								</div>
								
								<div class="col-md-3 form-group">
									<input name="experdate" type="text" id="experdate" placeholder="End date" class="form-control date-picker experdate" data-date-format="yyyy-mm-dd"><span class="bookMsg"></span>
								</div>
								
								
								
								
								
								
							</div> <!--End form row 6-->
							<hr>
							
							<div class="col-md-10 form-group">
								  <label for="status">Ad url?</label>        
									<div>
									   <select class="form-control addurl" id="addurl" name="addurl"  tabindex="7">
											<option value="" selected>Select Yes or No</option>
											<option value="yes">Yes</option>
											<option value="no">No</option>
									   </select>
									</div>
								</div>
							
							
							
							<div class="col-md-10 form-group weblink" style="display:none;">
								<label for="title">Web link</label>
								<div>
								<input type="text" name="addlink" id="addlink" placeholder="Web link" tabindex="1" 
									class="form-control"/>
								</div>
							</div><!--End form row 7-->
							
							
							<div class="col-md-10 form-group description" style="display:none;">
							  <label for="description">Description</label>        
								<div>
								   <textarea class="form-control" placeholder="*..Description" 
								   name="description" id="ajaxfilemanager" style="height:350px; width:600px;" ></textarea>
								</div>
							  </div>
							
		
							
							
							
							<div class="row">
								<div class="col-md-12 form-group"> 
									<button class="btn btn-sm btn-primary submit" type="submit">
										<i class="ace-icon fa fa-check"></i>
										<span>Submit</span>
									</button>
								</div>
							</div><!--End form row 8-->
							
						</form>
					</div>
					</div>
					<div class="col-md-2">
					
					</div>
					
					
					
					
					
					
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">
									
										
										<form action="<?php echo site_url("controlpanel/promotionManage"); ?>" method="POST">
										    <div style="float:left; display:inline">Display &nbsp;
										      
											    <select name="perPageLimit" id="perPageLimit" onChange="this.form.submit()">
											        <option <?php if($perPageLimit =='30'){ ?> selected ="selected" <?php } ?> value="30">30</option>
											        <option <?php if($perPageLimit =='50'){ ?> selected ="selected" <?php } ?> value="50">50</option>
											        <option <?php if($perPageLimit =='100'){ ?> selected ="selected" <?php } ?> value="100">100</option>
											    </select> &nbsp; records &nbsp;&nbsp;
											</div>	
											</form>
											<div class="col-md-6">
											
											<div style="float:left; display:inline">
										      <div class="input-group btn-xs">
												  <input type="text" id="serch" class="form-control" placeholder="Search for..." style="min-width:300px;">
												  <span class="input-group-btn">
													<button class="btn btn-primary btn-sm serchbutton" type="button"><i class="fa fa-search"></i></button>
												  </span>
												</div>
											</div>
											</div>
											
											<div class="col-md-3">
												 <select name="apprustatus" id="apprustatus" onChange="this.form.submit()">
											      	<option value="" selected="selected">Select Status</option>
												    <option value="unaproave">Approve</option>
													<option value="approve">Approved</option>
											    </select>
										   </div>
																					
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i> 	 </a>
										</div>
										
										
										
										
										
										
										
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>															</label>														</th>
																<th class="center" width="2">SN</th>
																<th width="10">Organization ID</th>														
																<th class="hidden-480">Org name</th>
																<th class="hidden-480">Org Country</th>
																<th class="hidden-480">Title</th>
																<th class="hidden-480" width="5">position</th>
																<th class="hidden-480" width="5">Serial</th>
																<th class="hidden-480">start date</th>
																<th class="hidden-480">end date</th>
																<th class="hidden-480" width="8">Images</th>
																<th class="hidden-480" width="8">Video</th>
																<th>Status</th>
															</tr>
														</thead>

												<tbody>
												
													<?php 
														$i=1;
													
													if(isset($addinfo)) {
													 $sn = $onset + 1;
													 foreach($addinfo as $v)
													 {
													 	$addimage = $v->add_image;
														$published_status = $v->published_status;
														$video_embed = $v->video_embed;
														$id  = $v-id;
													 ?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="admin_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span>															
															</label>														</td>
													
														<td class="center"><?php echo $i++;?></td>
														
														<td><a href="#" class="green" data-id="<?php echo $v->id; ?>"><?php echo $v->user_id; ?></a></td>														
														<td class="hidden-480"><?php echo $v->name; ?></td>
														<td class="hidden-480"><?php echo $v->country_name; ?></td>
														<td class="hidden-480"><?php echo $v->title; ?></td>
														<td class="hidden-480"><?php echo $v->positon; ?></td>
														<td class="hidden-480"><?php echo $v->serial_no; ?></td>
														<td class="hidden-480"><?php echo $v->publish_date; ?></td>
														<td class="hidden-480"><?php echo $v->expire_date; ?></td>
												<td class="hidden-480">
													<?php if(!empty($addimage)){?> 
													<img src="<?php echo base_url("Images/Add_image/" . $addimage); ?>" style="max-height:50px; max-width:50px;">
													<?php } else {?> 
													<img src="<?php echo base_url("Images/Add_image/img.png"); ?>" style="max-height:50px; max-width:50px;">
													</div>
													<?php }?>
												</td>
												
												<td class="hidden-480">
												<?php if(!empty($video_embed)){?> 
													<div class="embed-responsive embed-responsive-4by3">
							<iframe class="embed-responsive-item" src="<?php echo base_url("Images/Add_image/".$video_embed); ?>" style="max-height:50px; max-width:50px;"></iframe>
													</div>
												<?php } else {?> 
												<img src="<?php echo base_url("Images/Add_image/video.png"); ?>" style="max-height:50px; max-width:50px;">
												<?php }?>
												</td>
														
														
														<td>
															<?php if($published_status == 'unaproave') {?> 
															 <div>
																<a  class="approves" href="#" data-id="<?php echo $id ?>">Approve</a>
														    </div>
															<?php } else {?> <div>Approved</div><?php }?>
														</td>
													</tr>
													<?php } } ?>
												</tbody>
											</table>
											
											<div class="row" align="right" style="padding-right:20px;">
												 <div id="page">
														<label class="pos-rel"><span class="lbl"></span></label>
														<ul class="pagination-sm list-inline text-center">
															  <li class="pagi"><?php echo $this->pagination->create_links(); ?></li>
														</ul>
												 </div>
											</div>
											
										</div>
									</div>
								</div>
								
							
								
								
							<!--edit form-->
								<div id="inputTable">
								<form id="editForm" action="<?php echo site_url('controlpanel/promotionManage/update'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" id="id" name="id">
								<div id="modal-edit" class="modal" tabindex="-1">
									<div class="modal-dialog" style="width:700px;">
										<div class="modal-content">
											<div class="modal-header" style="border-bottom:3px solid #FF0000">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Promotion Edit</h4>
											</div>

											<div class="modal-body" style="height:490px; overflow:scroll;">
												<div class="row">
													<div class="col-xs-12 col-sm-12">
														

														
														<div class="form-group">
														  <label for="status">Organization Id</label>        
															<div>
															   <select class="form-control orgId" id="orgId" class="orgId" name="orgId"  tabindex="7" required>
																<option value="" selected>Select Organization Id</option>
																<?php foreach($allorguser as $v){?> 
																<option value="<?php echo $v->id; ?>"><?php echo $v->user_id; ?></option>
																<?php }?>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group">
															<label for="title">Organization Name</label>
															<div>
																<input type="text" name="orgname" id="orgname" placeholder="" tabindex="1" 
																class="form-control orgname" required readonly="readonly" />
															</div>
														</div>
														
														
														<div class="form-group">
															<label for="title">Organization Country</label>
															<div>
															<input type="text" name="orgcounty" id="orgcounty" placeholder="Organization Country" tabindex="1" 
																class="form-control orgcounty" required  readonly="readonly"/>
															</div>
														</div>
														
														
														<div class="form-group">
														  <label for="status">Select Website</label>        
															<div>
															   <select class="form-control website" id="website" name="website"  tabindex="7" required>
																<option value="" selected>Select Website</option>
																 <option value="main">Main Website</option>
																 <option value="country">Country Website</option>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group countryweb" style="display:none;">
														  <label for="status">Publish Country</label>        
															<div>
															   <select class="form-control" id="publishcounty" name="publishcounty"  tabindex="7">
																<option value="" selected>Select Publish Country</option>
																	<?php foreach($contryname as $v){?> 
																	  <option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
																	<?php } ?>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group">
															<label for="title">Ad Title</label>
															<div>
															<input type="text" name="addtitle" id="addtitle" placeholder="Add Title" tabindex="1" 
																class="form-control" required/>
															</div>
														</div>
														
														
														<div class="form-group">
														  <label for="status">Select position</label>        
															<div>
															   <select class="form-control posishon" name="position"  id="position" tabindex="7" required>
																	<option value="" selected="selected">Select Position</option>
																	<option value="top">Top</option>
																	<option value="left">Left</option>
																	<option value="right">Right</option>
															   </select>
															</div>
														</div>
														
														
														
														<div class="form-group top" style="display:none;">
														  <label for="status">Position Serial</label>        
															<div>
															   <select class="form-control" id="positionSerial" name="positionSerial"  tabindex="7">
																	<option value="" selected>Position Serial</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group left" style="display:none;">
														  <label for="status">Position Serial</label>        
															<div>
															   <select class="form-control" id="positionSerialL" name="positionSerialL"  tabindex="7">
																	<option value="" selected>Position Serial</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group right" style="display:none;">
														  <label for="status">Position Serial</label>        
															<div>
															   <select class="form-control" id="positionSerialR" name="positionSerialR"  tabindex="7">
																	<option value="" selected>Position Serial</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																	<option value="13">13</option>
																	<option value="14">14</option>
																	<option value="15">15</option>
																	<option value="16">16</option>
																	<option value="17">17</option>
																	<option value="18">18</option>
																	<option value="19">19</option>
																	<option value="20">20</option>
															   </select>
															</div>
														</div>
														
														
														
														<div class="form-group">
															<label for="date">Ad Start Date</label>
															<input name="startDeta" type="text" id="startDeta" placeholder="Date" class="form-control date-picker"
															 data-date-format="yyyy-mm-dd" />
														  </div>
														  
														  <div class="form-group">
															<label for="date">Ad End Date</label>
															<input name="endDate" type="text" id="endDate" placeholder="Date" class="form-control date-picker"
															 data-date-format="yyyy-mm-dd" /><span class="bookMsg"></span>
														  </div>
														  
														  
														  
														 <div class="form-group">
														  <label for="status">Ad Video?</label>        
															<div>
															   <select class="form-control embednadimage" id="embednadimage" name="embednadimage"  tabindex="7">
																	<option value="" selected>Select Yes or No</option>
																	<option value="yes">Yes</option>
																	<option value="no">No</option>
															   </select>
															</div>
														</div>
														  
														  
														<div class="row">
															<div class="col-sm-12">
															  <div class="form-group images" style="display:none;">
																<label for="news_image">Image Upload</label>
																   <div>
																	 <div class="attachmentbody" data-target="#add_image" data-type="add_image">
																		<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
																	  </div> 
																	   <input name="add_image" id="add_image" type="hidden" value="" required />                                                                                
																	</div>
																 </div>
															  </div>
														 </div>


														<div class="form-group embedcode" style="display:none;">
														  <label for="description">Embed Code</label>        
															<div>
															   <textarea class="form-control" rows="5" placeholder="Embed Code" 
															   tabindex="3" name="embed_link" id="embed_link"></textarea>
															</div>
														  </div>
														

													
														<div class="form-group">
														  <label for="status">Ad url?</label>        
															<div>
															   <select class="form-control addurl" id="addurl" name="addurl"  tabindex="7">
																	<option value="" selected>Select Yes or No</option>
																	<option value="yes">Yes</option>
																	<option value="no">No</option>
															   </select>
															</div>
														</div>




														<div class="form-group weblink" style="display:none;">
															<label for="title">Web link</label>
															<div>
															<input type="text" name="addlink" id="addlink" placeholder="Web link" tabindex="1" 
																class="form-control"/>
															</div>
														</div>


														<div class="space-4"></div>
														
														
														<div class="form-group description" style="display:none;">
														  <label for="description">Description</label>        
															<div>
															   <textarea class="form-control" placeholder="*..Description" 
															    name="ajaxfilemanagerupdate" id="ajaxfilemanagerupdate" style="height:350px;"></textarea>
															</div>
														  </div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Cancel
												</button>

												<button class="btn btn-sm btn-primary submit" type="submit">
													<i class="ace-icon fa fa-check"></i>
													<span class="update">Submit</span>
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
								</form>
								</div>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div>
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
<script>

      /*  $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="news_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="news_id[]"]').prop('checked', false);
           }
        });
   
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
*/

		$('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		
		
		
		
		
		$(".blue").on('click', function(){
			$(".well").css("display", "block");
		});
		
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/promotionManage/proEdit'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-edit').modal('show');
					$('#id').val(data.id);
					$('#modal-edit #orgId').val(data.user_id);
					$('#modal-edit #orgname').val(data.country.name);
					$('#modal-edit #orgcounty').val(data.country.country_name);
					
					$('#modal-edit #website').val(data.website);
					$('#modal-edit #addtitle').val(data.title);
					
					$('#modal-edit #startDeta').val(data.publish_date);
					$('#modal-edit #endDate').val(data.expire_date);
				  	
					$('#modal-edit #position').val(data.positon);
			
					
					
					if(data.positon == "top"){
						$('#modal-edit #positionSerial').val(data.serial_no);
						$('.top').css('display', 'block');
						$('.left').css('display', 'none');
						$('.right').css('display', 'none');
					} else if(data.positon == "left"){
						
						$('#modal-edit #positionSerialL').val(data.serial_no);
						$('.left').css('display', 'block');
						$('.top').css('display', 'none');
						$('.right').css('display', 'none');
					}
					else {
						$('#modal-edit #positionSerialR').val(data.serial_no);
						$('.right').css('display', 'block');
						$('.top').css('display', 'none');
						$('.left').css('display', 'none');
					}
					
					$('#modal-edit #embed_link').val(data.video_embed);
					
					if(data.video_embed){
						$('#modal-edit #embed_link').val(data.video_embed);
						$('.embedcode').css('display', 'block');
						$('.images').css('display', 'none');
					}else {
						$('.images').css('display', 'block');
						$('.embedcode').css('display', 'none');
					}
					
				   $('#modal-edit #addlink').val(data.add_link);
					if(data.add_link){
						 $('#modal-edit #addlink').val(data.add_link);
						 $('.weblink').css('display', 'block');
						 $('.description').css('display', 'none');
					} else {
						
						 $('#ajaxfilemanagerupdate').val(data.description);
						tinyMCE.get('ajaxfilemanagerupdate').setContent(data.description);
						 $('.description').css('display', 'block');
						 $('.weblink').css('display', 'none');
					
					}
					
					$(".update").text("Update");
					
					
				}
			});
			
			e.preventDefault();
		});


		
		
		
		
		$("#experdate").on('change', function(){
		var startDeta        = $("#startDeta").val();
		var experdate         = $("#experdate").val();
		var website          = $("#website").val();
		var pubcount         = $("#pubcount").val();
		var position         = $("#position").val();
		var positionSerial   = $("#positionSerial").val();
		var positionSerialL  = $("#positionSerialL").val();
		var positionSerialR  = $("#positionSerialR").val();
		var formURL = "<?php echo site_url('controlpanel/promotionManage/chckadddate'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {startDeta : startDeta, experdate : experdate, website : website, pubcount : pubcount, position : position, positionSerial : positionSerial, positionSerialL : positionSerialL, positionSerialR : positionSerialR},
				success:function(data){
					if(data == 1){
						$(".bookMsg").text("Sorry! the ad is booked");
						$('.submit').attr('disabled', 'disabled');
					}else {
					$(".bookMsg").text("");
					$('.submit').removeAttr('disabled');
					}
				}
			});

	});


		
		
		
		
		
		$(".orgId").on('change', function(){
			var orgId = $(this).val();
			console.log(orgId);
			var formURL = "<?php echo site_url('controlpanel/promotionManage/orgidbyname'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {orgId: orgId},
				success:function(data){
					$('.orgname').val(data);
						
				}
			});
			
		});
		
		
		$(".orgId").on('change', function(){
			var orgId = $(this).val();
			console.log(orgId);
			var formURL = "<?php echo site_url('controlpanel/promotionManage/orgidbycontryname'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {orgId: orgId},
				success:function(data){
					$('.orgcounty').val(data);
						
				}
			});
			
		});
		

	//start position

		$(".posishon").on('change', function(){
			var posishon = $(this).val();
			if(posishon == 'top'){
				$('.top').css("display","block");
				$('.left').css("display","none");
	
				$('.right').css("display","none");
			} else if(posishon == 'left'){
				$('.left').css("display","block");
				$('.top').css("display","none");
				$('.right').css("display","none");
			}else{
			$('.right').css("display","block");
			$('.top').css("display","none");
			$('.left').css("display","none");
			}
	});
	
	
	$(".addurl").on('change', function(){
		var addurl = $(this).val();
		if(addurl == 'yes'){
			$('.weblink').css('display', 'block');
			$('.description').css('display', 'none');
		}else {
			$('.description').css('display', 'block');
			$('.weblink').css('display', 'none');
		}
		
	});
	
	
	$(".embednadimage").on('change', function(){
		var embednadimage = $(this).val();
		if(embednadimage == 'yes'){
			$(".embedcode").css('display', 'block');
			$(".images").css('display', 'none');
		}
		else {
			$(".images").css('display', 'block');
			$(".embedcode").css('display', 'none');
		}
		
	});
	
	
	
	
	$(".website").on('change', function(){
		var website = $(this).val();
		
		if(website == 'country'){
			$('.countryweb').css("display","block");
		}else if(website == 'main'){
			$('.countryweb').css("display","none");
		}
	
		
	})
	
	
//end position



	/*$("#endDeta").on('blur', function(){
		var post 	= $("#addfrm input, select, textarea").serializeArray();
		var formURL = "<?php echo site_url('controlpanel/promotionManage/chckadddate'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data :post,
				success:function(data){
					if(data == 1){
						$(".bookMsg").text("Sorry! the ad is booked");
						$('.submit').attr('disabled', 'disabled');
					}else {
					$(".bookMsg").text("");
					$('.submit').removeAttr('disabled');
					}
				}
			});

	});*/





	



	$("#endDate").on('blur', function(){
		var postData 	= $("#inputTable input, select, textarea").serializeArray();
		var formURL = "<?php echo site_url('controlpanel/promotionManage/upDatechckadddate'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					if(data == 1){
						$(".bookMsg").text("Sorry! the ad is booked");
						$('.submit').attr('disabled', 'disabled');
					}else {
					$(".bookMsg").text("");
					$('.submit').removeAttr('disabled');
					}
				}
			});

	});










	
	$(document).on("click", ".approves", function(e)
		{
			var id 			= $(this).attr("data-id");
			var formURL 	= "<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedAdEdit'); ?>";
			var successUrl  = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedAd'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				success:function(data){
				  location.reload(); 
				  location.replace(successUrl);
				}
			});
			e.preventDefault();
		});


	$(".serchbutton").click(function(){
		var serch = $("#serch").val();
		var serchURL = "<?php echo site_url('controlpanel/promotionManage/serchforad'); ?>";
		$.ajax(
			{
				url : serchURL,
				type: "POST",
				data : {serch:serch},
				success:function(data){
					$("#listView").html(data);
				}
			});

		
	});


	$("#apprustatus").on('change', function(){
		var apprustatus = $(this).val();
		var statusform = "<?php echo site_url('controlpanel/promotionManage/apprupun'); ?>";
		$.ajax(
			{
				url : statusform,
				type: "POST",
				data: {apprustatus:apprustatus},
				success:function(data){
					$("#listView").html(data);
				}	
			
			});
		
	});


	



		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="news_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

		$(document).on("click", ".blue", function(e){
           $("#addForm").find("input[type=text], textarea").val("");
           
		});
		
		
		
		
		
		
		
		
		
		$(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="admin_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="admin_id[]"]').prop('checked', false);
           }
        });
		
		
		 $('.red').on('click', function(e) {
		       var deleteDta = $("#inputTable input").serializeArray();
		       console.log(deleteDta);
				$.ajax({
				url: "<?php echo site_url('controlpanel/adminManage/typedelete') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					  $("#inputTable").find('input[name="admin_id[]"],.allCheck').prop('checked', false);
					  location.reload();
				   }
			    });

			   e.preventDefault();
				
			});
		
		
		
		
		
		
		
	</script>
	</body>
</html>
