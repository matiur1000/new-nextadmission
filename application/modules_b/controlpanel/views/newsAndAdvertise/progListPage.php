	<table id="inputTable" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="center">
						<label class="pos-rel">
							<input type="checkbox" class="ace allCheck" />
							<span class="lbl"></span></label>
							<label class="pos-rel">
							<a class="red" href="#">
								<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
							<span class="lbl"></span></label></th>
					<th>Sl No </th>														
					<th class="hidden-480">Programe Name </th>
					<th></th>
				</tr>
			</thead>

			<tbody>
			<?php 
			// print_r($programeInfo);
			 $i = 1;
			  foreach ($programeInfo as $v){
			     $id  		    = $v->id;
			?>
				<tr>
					<td class="center">
						<label class="pos-rel">
							<input type="checkbox" name="programe_id[]" class="ace" value="<?php echo $id ?>" />
							<span class="lbl"></span></label></td>

					<td>
						<a href="#"><?php echo $i++; ?></a></td>														
					<td class="hidden-480"><a href="#"><?php echo $v->programe_name; ?></a></td>
					<td>
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="green" href="#" data-id="<?php echo $id ?>">
								<i class="ace-icon fa fa-pencil bigger-130"></i></a>

							</div>

						</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

<script>

       $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="programe_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="programe_id[]"]').prop('checked', false);
           }
       });

        /*// Position Wise Serial
		$("#positon").change(function() {
			var positon = $("#positon").val();			
			$.ajax({
				url : SAWEB.getSiteAction('controlpanel/newsAndAdvertiseManage/positonWiseSerial'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : positon },
				dataType : "html",
				success : function(data) {			
					$("#serial_no").html(data);
				}
			});
			
		});*/
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/progEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#programe_name').val(data.programe_name);
					$('#programe_details').val(data.programe_details);
					$('.update').text("Update");
					
				}
			});
			
			e.preventDefault();
		});


		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/progDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="programe_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});
		
				$(document).on("click", ".blue", function(e){
                   $("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>