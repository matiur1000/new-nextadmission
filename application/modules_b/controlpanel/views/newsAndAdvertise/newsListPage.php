<table id="inputTable" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace allCheck" />
						<span class="lbl"></span></label>
						<label class="pos-rel">
						<a class="red" href="#">
							<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
						<span class="lbl"></span></label>	</th>
				<th>Company Name </th>														
				<th class="hidden-480">News Title </th>
				<th class="hidden-480">Image</th>
				<th class="hidden-480">User Id </th>
				<th class="hidden-480">Status</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
		<?php 
		// print_r($newsInfo);
		 $i = 0;
		  foreach ($newsInfo as $v){
		     $id  		    = $v->id;
		?>
			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" name="news_id[]" class="ace" value="<?php echo $id ?>" />
						<span class="lbl"></span></label></td>

				<td>
					<a href="#"><?php echo $v->company_name; ?></a></td>														
				<td class="hidden-480"><?php echo $v->title; ?></td>
				<td class="hidden-480"><img src="<?php echo base_url("Images/News_image/$v->image"); ?>" height="50" width="50" /></td>
				<td class="hidden-480"><?php echo $v->user_id; ?></td>
				<td class="hidden-480"><?php echo $v->status; ?></td>
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="#" data-id="<?php echo $id ?>">
							<i class="ace-icon fa fa-pencil bigger-130"></i></a>
						</div>

					</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
<script>

    $(document).on("click", ".allCheck", function(e){
   	   var allChecked = this.checked; 
       var parent = $(this).parents('#inputTable');
       if(allChecked){
         parent.find('input[name="news_id[]"]').prop('checked', true);
       } else {
         parent.find('input[name="news_id[]"]').prop('checked', false);
       }
    });
    
	//callback handler for form submit
	$("#addForm").submit(function(e)
	{
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		console.log(postData);
		$.ajax(
		{
			url : formURL,
			type: "POST",
			async: false,
			data : postData,
			success:function(data){
				$("#listView").html(data);				
				//$("#addForm").find("input[type=text], textarea").val("");
				$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
				
				$.each($('.attachmentbody'), function(i, attachment) {
					attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
					reInitiateFileUpload(attachment);                        
				});
			}
		});
		
		e.preventDefault();
	});


	$('.date-picker').datepicker({
		autoclose: true	  
	});	
	
	
	//callback handler for form submit
	//$(".edit").click(function(e)
	$(document).on("click", ".green", function(e)
	{
		var id 		= $(this).attr("data-id");
		var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsEdit'); ?>";
		
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : {id: id},
			dataType: "json",
			success:function(data){
				$('#modal-form').modal('show');
				
				$('#id').val(data.id);
				$('#company_name').val(data.company_name);
				$('#title').val(data.title);
				$('#description').val(data.description);
				$('#date').val(data.date);
				$('#status').val(data.status);
				$('.update').text("Update");
			}
		});
		
		e.preventDefault();
	});



	$('.red').on('click', function(e) {
       var deleteDta = $("#inputTable input").serializeArray();
       console.log(deleteDta);
		$.ajax({
		url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsDelete') ?>",
		method: "POST",	
		data: $("#inputTable input").serializeArray(),
		dataType: "html",
		success: function(data){
			  $("#inputTable").find('input[name="news_id[]"],.allCheck').prop('checked', false);
			  location.reload();
		   }
	    });

	   e.preventDefault();
		
	});

	$(document).on("click", ".blue", function(e){
       $("#addForm").find("input[type=text], textarea").val("");
       $('.update').text("Save");
	});
</script>