<table id="inputTable" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="24" class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace allCheck" />
					<span class="lbl"></span></label>
					<label class="pos-rel">
					<a class="red" href="#">
						<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					<span class="lbl"></span></label>	</th>
			<th width="207" class="hidden-480">Title</th>
			<th width="65" class="hidden-480">Ad Image </th>
			<th width="53" class="hidden-480">Position</th>
			<th width="37" class="hidden-480">Sl No </th>
			<th width="42" class="hidden-480">Status</th>
			<th width="56">Action</th>
		</tr>
	</thead>

	<tbody>
	<?php 
	// print_r($addInfo);
	if(isset($addInfo)) {
     $i = $onset + 1;
	  foreach ($addInfo as $v){
	     $id  		    = $v->id;
	?>
		<tr>
			<td class="center">
				<label class="pos-rel">
					<input type="checkbox" name="add_id[]" class="ace" value="<?php echo $id ?>" />
					<span class="lbl"></span></label></td>

			<td class="hidden-480"><a href="#"><?php echo $v->	title; ?></a></td>
			<td class="hidden-480"><img src="<?php echo base_url("Images/Add_image/$v->add_image"); ?>" height="50" width="50" /></td>
			<td class="hidden-480"><?php echo $v->positon; ?></td>
			<td class="hidden-480"><?php echo $v->serial_no; ?></td>
			<td class="hidden-480"><?php echo $v->status; ?></td>
			<td>
				<div class="hidden-sm hidden-xs action-buttons">
					<a class="green" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-pencil bigger-130"></i></a>

					</div>

				</td>
		</tr>
		<?php } } ?>
		<tr>
		  <td colspan="7" align="right" class="text-right">
		       <label class="pos-rel"><span class="lbl"></span>
		        <ul class="pager">
				    <li><?php echo $this->pagination->create_links(); ?></li>
				</ul>
		   </td>
	  </tr>
	</tbody>
</table>
<script>

       $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="add_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="add_id[]"]').prop('checked', false);
           }
       });

       $('.date-picker').datepicker({
			autoclose: true	  
		});	
		


        // Positon Wise Serial No
		$("#positon").change(function() {
		var positon = $("#positon").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('organizationUserHome/homePageSerialNo'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : positon },
			dataType : "html",
			success : function(data) {			
				$("#serial_no").html(data);
			}
		  });
		
	  });


		// Positon Wise Serial No
		$("#select_website").change(function(){
		  var selectValue = $("#select_website").val();	
		  
		  if(selectValue == 'country'){
		    $("#selectCountry").css("display", "block"); 
		  } else {
		     $("#selectCountry").css("display", "none"); 
		  }
		
	  });



		$("#next").click(function(e)
		{
			
			var postData 	= $("#inputTable input, select, textarea").serializeArray();
			var searchuRL  = "<?php echo site_url('organizationUserHome/adBookingChk'); ?>";

			console.log(postData);
			$.ajax(
			{
				url : searchuRL,
				type: "POST",
				data : postData,
				success:function(data){
					 if(data==1){
						$(".bookMsg").text("Sorry! the ad is booked");
						$(".bookMsg").css("color", "red");
					} else {
						$("#searchHide").css("display", "none");
						$("#adSubmit").css("display", "block");
						$("#footer").css("display", "block");
						$("#select_website").attr('readonly', true);
						$("#region").attr('readonly', true);
						$("#positon").attr('readonly', true);
						$("#serial_no").attr('readonly', true);
						$("#publish_date").attr('readonly', true);	
						$("#expire_date").attr('readonly', true);	
						 
					}
			  	}


			});
			e.preventDefault();
			
		});	




       $("#finalBooking").click(function(e)
		{
			
			var postData 	= $("#inputTable input, select, textarea").serializeArray();
			var searchuRL  	= "<?php echo site_url('organizationUserHome/addStore'); ?>";
			//var successuRL  = "<?php echo site_url('organizationUserHome/addManageFinal'); ?>";
			console.log(postData);
			$.ajax(
			{
				url : searchuRL,
				type: "POST",
				data : postData,
				success:function(data){				
					 $("#inputTable input[type='text'], #inputTable input[type='hidden'], #inputTable textarea").val("");
					 location.reload();
					 
			  	}

			});

			e.preventDefault();
			
		});	
	
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/addEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					$('#id').val(data.id);
					$('#title').val(data.title);
					$('#deadline_date').val(data.deadline_date);
					$('#add_link').val(data.add_link);
					$('#description').val(data.description);

					$("#adSubmit").css("display", "block"); 
					$("#selectCountry").css("display", "none"); 
					$("#sweb").css("display", "none"); 
					$("#spos").css("display", "none"); 
					$("#sl").css("display", "none"); 
					$("#ingroup").css("display", "none"); 
					$("#mng").css("display", "none");  
					$("#searchHide").css("display", "none"); 
					$("#footer2").css("display", "block"); 
					
				}
			});
			
			e.preventDefault();
		});



       $("#adUpdate").click(function(e)
		{
			
			var postData 	= $("#inputTable input, select, textarea").serializeArray();
			var searchuRL  	= "<?php echo site_url('controlpanel/newsAndAdvertiseManage/addUpdate'); ?>";
			//var successuRL  = "<?php echo site_url('organizationUserHome/addManageFinal'); ?>";
			console.log(postData);
			console.log(searchuRL);
			$.ajax(
			{
				url : searchuRL,
				type: "POST",
				data : postData,
				success:function(data){				
					 $("#inputTable input[type='text'], #inputTable input[type='hidden'], #inputTable textarea").val("");
					 location.reload();
					 
			  	}

			});

			e.preventDefault();
			
		});	



       $('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/addDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="add_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});				
				
	</script>