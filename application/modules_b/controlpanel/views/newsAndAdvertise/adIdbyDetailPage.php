<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Promotion Details  <span style="padding-left:100px;"><?php echo $result->user_id; ?></span></h4>
		  </div>
		  <div class="modal-body">
			<div class="row">
			
				<div class="col-md-3">
					<div class="row">
					
					<?php if(!empty($result->add_image)){?> 
						<img src="<?php echo base_url("Images/Add_image/" . $result->add_image); ?>" class="thumbnail" style="max-height:200px;">
					<?php } else {?>
						<?php echo $result->video_embed; ?>
					 <?php }?>
						
					</div>
				</div>
				
				<div class="col-md-9">
					 <div class="row">
					 	<table width="100%" border="0" align="left" class="table table-bordered">
						  <tr>
							<td width="22%">Organization ID :</td>
							<td width="78%"><?php echo $result->user_id; ?></td>
						  </tr>
						  <tr>
							<td>Organization name :</td>
							<td><?php echo $result->name; ?></td>
						  </tr>
						  <tr>
							<td>Organization Country :</td>
							<td><?php echo $result->d1name; ?></td>
						  </tr>
						  <tr>
							<td>Website :</td>
							<td><?php echo $result->website; ?></td>
						  </tr>
						  <tr>
							<td>Publish Country :</td>
							<td><?php echo $result->d2name; ?></td>
						  </tr>
						  <tr>
							<td>position :</td>
							<td><?php echo $result->positon; ?></td>
						  </tr>
						  <tr>
							<td>Serial :</td>
							<td><?php echo $result->serial_no; ?></td>
						  </tr>
						  <tr>
							<td>Title :</td>
							<td><?php echo $result->title; ?></td>
						  </tr>
						  <tr>
							<td>start date</td>
							<td><?php echo $result->publish_date; ?></td>
						  </tr>
						  <tr>
							<td>end date</td>
							<td><?php echo $result->expire_date; ?></td>
						  </tr>
						  <tr>
							<td>Status</td>
							<td><?php echo $result->payment_status; ?></td>
						  </tr>
						  <tr>
							<td height="219" align="left" valign="top">Details</td>
							
							<td>
							
							<?php if(!empty($result->description)){?> 
							<?php echo $result->description; ?>
							<?php } else {?>
							<?php echo $result->add_link; ?>
							 <?php }?>
							</td>
							
						  </tr>
					   </table>

					 </div>
				</div>
				
			</div>
		  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		
	  </div>
	</div>