<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">Country Manage </li>
						</ul><!-- /.breadcrumb -->

					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">
										   <form action="<?php echo site_url('controlpanel/regionManage/countryManage'); ?>" method="POST">
										    <div style="float:left; display:inline">Display &nbsp;
										      
											    <select name="perPageLimit" id="perPageLimit" onchange="this.form.submit()">
											        <option <?php if($perPageLimit =='30'){ ?> selected ="selected" <?php } ?> value="30">30</option>
											        <option <?php if($perPageLimit =='50'){ ?> selected ="selected" <?php } ?> value="50">50</option>
											        <option <?php if($perPageLimit =='100'){ ?> selected ="selected" <?php } ?> value="100">100</option>
											        <option <?php if($perPageLimit =='all'){ ?> selected ="selected" <?php } ?> value="all">all</option>
											    </select> &nbsp; records
											</div>	
											</form>	

											<div style="float:left; display:inline; padding-left:100px">Select Region &nbsp;
										      
											    <select id="region_id_sort" name="region_id_sort"  tabindex="1" required>
                                                    <option value="" selected>Sellect Region Name</option>
													<?php foreach ($regionInfo as $v){?>
													<option value="<?php echo $v->id; ?>"><?php echo $v->region_name; ?></option>
													<?php } ?>
                                               </select>
											</div>									
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable"  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>															</th>
														<th>Sl</th>
														<th>Region  Name</th>
														<th>Country  Name</th>
														<th>Country Code  </th>
														<th>Nationality</th>														
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($regionInfo);
												if(isset($countryInfo)) {
                                                 $i = $onset + 1;
												  foreach ($countryInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="country_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span>															</label>														</td>

														<td><a href="#"><?php echo $i++; ?></a></td>
														<td><a href="#"><?php echo $v->region_name; ?></a></td>
														<td><a href="#"><?php echo $v->country_name; ?></a></td>
														<td><a href="#"><?php echo $v->country_code; ?></a></td>
														<td>
															<a href="#"><?php echo $v->nationality; ?></a>														</td>														
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

																															</div>

																													</td>
													</tr>
													<?php } } ?>
													<tr>
														<th height="43" colspan="10" class="center">
															<label class="pos-rel"><span class="lbl"></span>
															<ul class="pager pull-right">
															    <li><?php echo $this->pagination->create_links(); ?></li>
															</ul>
														</th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
								<form id="addForm" action="<?php echo site_url('controlpanel/regionManage/countryStore'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">Country Manage</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                                <div class="col-xs-12 col-sm-11">
																
																<div class="form-group">
                                                                        <label for="region_id">Sellect Region Name</label>        
                                                                        <div>
                                                                           <select class="form-control" id="region_id" name="region_id"  tabindex="1" required>
                                                                                <option value="" selected>Sellect Region Name</option>
																				<?php foreach ($regionInfo as $v){?>
																				<option value="<?php echo $v->id; ?>"><?php echo $v->region_name; ?></option>
																				<?php } ?>
                                                                           </select>
                                                                        </div>
                                                                    </div> 
																	
																	<div class="space-4"></div>  
																	
                                                                    <div class="form-group">
                                                                        <label for="country_name">Country Name</label>        
                                                                        <div>
                                                                           <input type="text" id="country_name" placeholder="Country Name" name="country_name"
                                                                            tabindex="2" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>  
																	
																	<div class="form-group">
                                                                        <label for="country_code">Country Code</label>        
                                                                        <div>
                                                                           <input type="text" id="country_code" placeholder="Country Code" name="country_code"
                                                                            tabindex="3" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	<div class="space-4"></div>  
																	
																	<div class="form-group">
                                                                        <label for="nationality">Nationality</label>        
                                                                        <div>
                                                                           <input type="text" id="nationality" placeholder="Nationality" name="nationality"
                                                                            tabindex="5" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	<div class="space-4"></div>  
                                                                </div>
                                                            </div>
                                                        </div>
            
                                                        <div class="modal-footer">
                                                            <button class="btn btn-sm" data-dismiss="modal">
                                                                <i class="ace-icon fa fa-times"></i>
                                                                Cancel
                                                            </button>
            
                                                            <button class="btn btn-sm btn-primary" type="submit">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                <span class="update">Save</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	<script>
	   $("#region_id_sort").change(function() {
			var regionId = $("#region_id_sort").val();			
			$.ajax({
				url : SAWEB.getSiteAction('controlpanel/regionManage/regionWiseData'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { regionId : regionId },
				dataType : "html",
				success : function(data) {			
					$("#listView").html(data);
				}
			});
			
		});	

	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="country_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="country_id[]"]').prop('checked', false);
           }
       });

		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], select, textarea").val("");
					
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/regionManage/countryEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#country_name').val(data.country_name);
					$('#country_code').val(data.country_code);
					$('#nationality').val(data.nationality);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});


		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/regionManage/countrydelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="country_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

		$(document).on("click", ".blue", function(e){
           $("#addForm").find("input[type=text], textarea").val("");
           $('.update').text("Save");
		});
	</script>
	</body>
</html>
