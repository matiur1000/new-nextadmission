	<div id="myModal" class="modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="blue bigger">Basic Information Manage</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-11">
							

							<div class="form-group">
								<label for="title">Title</label>

								<div>
									<input type="text" name="title" id="title" placeholder="Title" tabindex="1" 
									class="form-control" required  />
								</div>
							</div>

							<div class="space-4"></div>

							<div class="form-group">
								<label for="footer_description">Footer Description</label>

								<div>
									<textarea name="footer_description" id="footer_description" placeholder="Footer Description" tabindex="2" 
									class="form-control" required></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="image_type">Image Type</label>

								<div>
								   <select class="form-control" id="image_type" name="image_type"  tabindex="3" required >
										<option value="" selected>Select Image Type</option>
										<option value="Logo">Logo Image</option>
										<option value="Glob">Glob Image</option>
								   </select>
								</div>
							</div>

							<div class="space-4"></div>
							
							<div class="col-xs-12 col-sm-12">
							<label for="image">Image Upload</label>
							   <input type="file" name="image" id="image" tabindex="4" />
							 </div>
						  <div class="form-group">
								<label for="image_title">Image Title</label>

								<div>
									<input type="text" name="image_title" id="image_title" placeholder="Image Title" tabindex="5" 
									class="form-control" required  />
								</div>
							</div>
						
						   <div class="form-group">
								<label for="image_alt">Image Alt</label>

								<div>
									<input type="text" name="image_alt" id="image_alt" placeholder="Image Alt" tabindex="6" 
									class="form-control" required  />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-sm" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>
						Cancel
					</button>

					<button class="btn btn-sm btn-primary" type="submit">
						<i class="ace-icon fa fa-check"></i>
						Save
					</button>
				</div>
			</div>
		</div>
	</div><!-- PAGE CONTENT ENDS -->
	
	<script
	  $( document ).ready(function() {
		<?php if($status == 'reset') { ?>
			$("#addForm input").val('');
		<?php } ?>
	});
	
	</script>