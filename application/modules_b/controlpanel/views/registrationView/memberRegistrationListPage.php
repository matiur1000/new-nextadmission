<table id="inputTable"  class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th class="center">
<label class="pos-rel">
<input type="checkbox" class="ace allCheck" />
<span class="lbl"></span></label>
<label class="pos-rel">
<a class="red" href="#">
	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
<span class="lbl"></span></label></th>
<th>User Type </th>														
<th class="hidden-480">Name </th>
<th class="hidden-480">Country</th>
<th class="hidden-480">User Id</th>
<th class="hidden-480">Status</th>
</tr>
</thead>

<tbody>
<?php 
if(isset($allRegterInfo)) {
 $i = $onset + 1;
foreach ($allRegterInfo as $v){
$id  		    = $v->id;
?>
<tr>
<td class="center">
<label class="pos-rel">
<input type="checkbox" name="user_id[]" class="ace" value="<?php echo $id ?>" />
<span class="lbl"></span></label></td>

<td><?php echo $v->user_type; ?></td>		
<td class="hidden-480"><?php echo $v->name; ?></td>
<td class="hidden-480"><?php echo $v->country_name; ?></td>
<td class="hidden-480"><?php echo $v->user_id; ?></td>
<td class="hidden-480"><?php echo $v->status; ?></td>	
</tr>
<?php } } ?>
<tr>
  <td colspan="6" align="center" valign="middle" class="center">
    <ul class="pager">
	    <li><?php echo $this->pagination->create_links(); ?></li>
	</ul>
  </td>
</tr>
</tbody>
</table>

<script>

	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="user_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="user_id[]"]').prop('checked', false);
           }
       });


	$(document).on("blur", "#user_id", function() {
		var userId 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userId : userId },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				$(".chk").text("This Email Already Exit!");
				$(".chk").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				$(".chk").text("");
				$(".chk").removeClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});



	$(document).on("blur", "#user_id2", function() {
		var user_id 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/userEmailChk'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { user_id : user_id },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				$(".chkEmail").text("This Email Already Exit!");
				$(".chkEmail").addClass("red");
				} else {
				$(".chkEmail").text("");
				$(".chkEmail").addClass("red");
	   			 
			  }
		    }
		 });
			
	});


	$(document).on("change", "input[name='regType']", function(){
		   var typeValue = $(this).val();

		   if(typeValue =='organization'){
	    	 $("#organize_reg").css("display", "block");
	    	 $("#other_reg").css("display", "none");
		   }else{
		     $("#organize_reg").css("display", "none");
		     $("#other_reg").css("display", "block");
		   }
		   
		  	
	});
	
	
	
	
	// Password Count	
   
   $(document).on("keyup", ".password", function() {
	 var len = $(this).val().length;
		if(len<=1){
		$(".first").text("");
		$(".first").removeClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	  } else if(len<=4){
	  	$(".first").text("Very Weak");
		$(".first").addClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
	   } else if(len<=8){

	   	$(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");

	   } else if(len<=9){

	   	$(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword", function() {
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		= $(".password").val();

	 if(conpass){

		  if(conpass != Pass){
		  	$(".second").text("Your New Password and Confirm Password donot match!");
			$(".second").addClass("red");
			$(".second").removeClass("green");
		  
		  } else {
		  	$(".second").text("Password Match");
			$(".second").removeClass("red");
			$(".second").addClass("green");
		   	
		  }

		} else {
		   $(".second").text("Password Match");
		   $(".second").removeClass("red");
		   $(".second").removeClass("green");
		  
		}
	});
	
	
	
	// Password Count


	$(document).on("keyup", ".password3", function() {
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".third").text("");
		$(".third").removeClass("red");
		$(".third").removeClass("yellow");
		$(".third").removeClass("green");
		
	  } else if(len<=4){
	  	$(".third").text("Very Weak");
		$(".third").addClass("red");
		$(".third").removeClass("yellow");
		$(".third").removeClass("green");
	   } else if(len<=8){

	   	$(".third").text("Good");
		$(".third").addClass("green");
		$(".third").removeClass("yellow");
		$(".third").removeClass("red");

	   } else if(len<=9){

	   	$(".third").text("Good");
		$(".third").addClass("green");
		$(".third").removeClass("yellow");
		$(".third").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword3", function() {
	 var conpass 	= $(this).val();
	 var Pass 		= $(".password3").val();

	 if(conpass){

		  if(conpass != Pass){
		  	$(".fourth").text("Your New Password and Confirm Password donot match!");
			$(".fourth").addClass("red");
			$(".fourth").removeClass("green");
		  
		  } else {
		  	$(".fourth").text("Password Match");
			$(".fourth").removeClass("red");
			$(".fourth").addClass("green");
		   	
		  }

		} else {
		   $(".fourth").text("Password Match");
		   $(".fourth").removeClass("red");
		   $(".fourth").removeClass("green");
		  
		}
	});
	
	// Password and confirm password match
		//callback handler for form submit
		$("#regForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			console.log(formURL);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
				  $("#listView").html(data);		
				  $("#regForm input[type='text'], input[type='hidden'], input[type='email'], input[type='password'], #regForm select, textarea").val("");
				  $(".verify").text("Registration success go email for verify account");
				  $(".verify").addClass("green");
				}
			});
			
			e.preventDefault();
		});


		$("#genReg").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			console.log(formURL);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
				   $("#listView").html(data);		
				   $("#genReg input[type='text'], input[type='hidden'], input[type='email'], input[type='password'], #genReg select, textarea").val("");
				   $(".verify").text("Registration success go email for verify account");
				   $(".verify").addClass("green");
				}
			});
			
			e.preventDefault();
		});
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/registerMember/edit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#status').val(data.status);
					$('#user_type').val(data.user_type);
					$('#name').val(data.name);
					$('#date_of_birth').val(data.date_of_birth);
					$('#father_name').val(data.father_name);
					$('#mother_name').val(data.mother_name);
					$('#register_image').val(data.register_image);
					$('#nationality').val(data.nationality);
					$('#address').val(data.address);
					$('#city').val(data.city);
					$('#country_phone').val(data.country_phone);
					$('#mobile').val(data.mobile);
					$('#email').val(data.email);
					$('#address_permanent').val(data.address_permanent);
					$('#city_permanent').val(data.city_permanent);
					$('#country_phone_permanent').val(data.country_phone_permanent);
					$('#mobile_permanent').val(data.mobile_permanent);
					$('#email_permanent').val(data.email_permanent);
					$('#user_id').val(data.user_id);
					$('#password').val(data.password);
					$('#conform_password').val(data.password);
					$('#status').val(data.status);
					$('.update').text("Update");
					
					$('#country_id').html('<option value="">Select Country Name</option>');
					
					$.each(data.countryList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.country_name+'</option>';
						$('#country_id').append(option);						
					});
					
					$('#country_id').val(data.country_id);
					
					
					$('#city_id').html('<option value="">Select City Name</option>');
					
					$.each(data.cityList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.city_name+'</option>';
						$('#city_id').append(option);						
					});
					
					$('#city_id').val(data.city_id);
				}
			});
			
			e.preventDefault();
		});

        $('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/regionManage/countrydelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="user_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/registerMember/regDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="user_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});
		
		
				
		$(document).on("click", ".blue", function(e){
           $('.update').text("Save");
		});
		
	</script>