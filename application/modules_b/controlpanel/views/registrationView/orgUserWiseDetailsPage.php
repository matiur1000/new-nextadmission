<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script></head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Registration View </a>
							</li>
							<li class="active">User Wise Details</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
                                        <div class="row table-header">
                                           <div class="col-lg-9" align="center">
                                           Find Global Study Organization Wise Registration Details
                                           </div>
                                           <div class="col-lg-3" align="right">
                                             <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">Send Mail</a>	
                                           </div>
                                        </div>
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 										
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12" style="padding:0px;">
									   <table width="100%" align="left" cellpadding="0" cellspacing="0" class="table" style="height:400px; background:#FFFFFF; font-size:14px; margin-bottom:0px; border:solid 1px #CCCCCC; margin:0px !important;">
									<tr>
									<td width="6%">&nbsp;</td>
									<td width="25%" height="26"><strong>Organization Name</strong></td>
									<td width="5%" align="center" valign="middle">:</td>
									<td width="49%"><?php echo $userWiseFullInfo->name; ?></td>
									<td width="15%" rowspan="4"><img src="<?php echo base_url("Images/Register_image/$userWiseFullInfo->image"); ?>"
									 width="150" height="150"  alt=""/></td>
									</tr>
									<tr>
									  <td>&nbsp;</td>

									  <td height="26"><strong>Region</strong></td>
									  <td align="center" valign="middle">:</td>
									  <td><?php echo $userWiseFullInfo->region_name;?></td>
									  </tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="26"><strong>Country</strong></td>
									  <td align="center" valign="middle">:</td>
									  <td><?php echo $userWiseFullInfo->country_name;?></td>
									  </tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="26"><strong>City</strong></td>
									  <td align="center" valign="middle">:</td>
									  <td><?php echo $userWiseFullInfo->city_name;?></td>
									  </tr>
									<tr>
									<td>&nbsp;</td>
									<td height="26"><strong>User Type </strong></td>
									<td align="center" valign="middle">:</td>
									<td><?php echo $userWiseFullInfo->user_type;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="26"><strong>Address</strong></td>
									  <td align="center" valign="middle">:</td>
									  <td><?php echo $userWiseFullInfo->address;?></td>
									  <td>&nbsp;</td>
									  </tr>
									<tr>
									<td>&nbsp;</td>
									<td height="26"><strong>Organization Phone </strong></td>
									<td align="center" valign="middle">:</td>
									<td><?php echo $userWiseFullInfo->phone_com;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td>&nbsp;</td>
									<td height="26"><strong>Organization Mobile</strong></td>
									<td align="center" valign="middle">:</td>
									<td><?php echo $userWiseFullInfo->mobile_com;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td>&nbsp;</td>
									<td height="26"><strong>Organization</strong><strong> Email</strong></td>
									<td align="center" valign="middle">:</td>
									<td><?php echo $userWiseFullInfo->email_com;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td>&nbsp;</td>
									<td height="26"><strong>Location</strong></td>
									<td align="center" valign="middle">:</td>
									<td><?php echo $userWiseFullInfo->location_by_google;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td>&nbsp;</td>
									<td height="26"><strong>Founded Year</strong></td>
									<td align="center" valign="middle">:</td>
									<td><?php echo $userWiseFullInfo->founded;?></td>
									<td>&nbsp;</td>
									</tr>
									
									<tr>
									<td>&nbsp;</td>
									<td height="31"><strong>Contact Person </strong></td>
									<td align="center">:</td>
									<td><?php echo $userWiseFullInfo->contact_persons;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="28"><strong>Designition</strong></td>
									  <td align="center">:</td>
									  <td><?php echo $userWiseFullInfo->designation;?></td>
									  <td>&nbsp;</td>
									  </tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="28"><strong> Contact Person  Mobile </strong></td>
									  <td align="center">:</td>
									  <td><?php echo $userWiseFullInfo->mobile;?></td>
									  <td>&nbsp;</td>
									  </tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="28"><strong>Contact Person  Phone </strong></td>
									  <td align="center">:</td>
									  <td><?php echo $userWiseFullInfo->phone;?></td>
									  <td>&nbsp;</td>
									  </tr>
									<tr>
									  <td>&nbsp;</td>
									  <td height="28"><strong>Contact Person  Email </strong></td>
									  <td align="center">;</td>
									  <td><?php echo $userWiseFullInfo->email;?></td>
									  <td>&nbsp;</td>
									  </tr>
									<tr>
									<td>&nbsp;</td>
									<td height="28"><strong>Organization Type </strong></td>
									<td align="center">:</td>
									<td><?php echo $userWiseFullInfo->type_of_organization;?></td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									  <td height="28">&nbsp;</td>
									  <td><strong> Type Of Ownership </strong></td>
									  <td align="center">:</td>
									  <td><?php echo $userWiseFullInfo->type_of_ownership;?></td>
									  <td>&nbsp;</td>
									  </tr>
									<tr>
									  <td height="28">&nbsp;</td>
									  <td><strong>Type Of Scholarship </strong></td>
									  <td align="center">:</td>
									  <td><?php echo $userWiseFullInfo->scholarship;?></td>
									  <td>&nbsp;</td>
									  </tr>
									
									<tr>
									<td height="28">&nbsp;</td>
									<td><strong>Status</strong></td>
									<td align="center">:</td>
									<td><?php echo $userWiseFullInfo->status;?></td>
									<td>&nbsp;</td>
									</tr>
									</table>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
                                
                                <form id="addForm" action="<?php echo site_url('controlpanel/registerMember/orgUserWiseMail/'.$userWiseFullInfo->id); ?>" method="post" enctype="multipart/form-data">
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">User Wise Email Send</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                                <div class="col-xs-12 col-sm-11">
                                                                    <div class="form-group">
                                                                        <label for="admin_type">Title</label>        
                                                                        <div>
                                                                           <input type="text" id="title" placeholder="Title" name="title"
                                                                            tabindex="1" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="footer_description">Message</label>
                                                                        <div>
                                                                            <textarea name="message" id="message" placeholder="Message" tabindex="2" 
                                                                            class="form-control" required></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="space-4"></div>
                                                                </div>
                                                            </div>
                                                        </div>
            
                                                        <div class="modal-footer">
                                                            <button class="btn btn-sm" data-dismiss="modal">
                                                                <i class="ace-icon fa fa-times"></i>
                                                                Cancel
                                                            </button>
            
                                                            <button class="btn btn-sm btn-primary" type="submit">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Send
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
								
								<!-- from -->
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	
	</body>
</html>
