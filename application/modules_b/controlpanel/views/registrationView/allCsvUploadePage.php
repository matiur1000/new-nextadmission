<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">All Csv file Upload</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
										   <form action="<?php echo site_url('controlpanel/registerMember/allCsvfileUploadAction'); ?>" method="post" enctype="multipart/form-data">
										       <table id="csvEditView" width="1304"  class="table table-bordered">
												  <thead>
													<tr>
													  <th width="318" align="right" valign="middle" class="text-right"  style="color:#0000FF"><?php if(!empty($suceess)){ echo $suceess; } ?></th>
													  <th width="472" align="right" class="text-right" valign="middle">Select Type : 
													  <select id="type_name" name="type_name"  tabindex="2" required >
                                                        <option value="" selected>Select Type</option>
                                                        <option value="contact">Organization Contact Csv</option>
                                                        <option value="organizeType">Organization Type Csv</option>
                                                        <option value="program">Program Manage Csv</option>
                                                       </select>
                                                      </th>
													  <th width="152" align="right" class="text-right" valign="middle">Csv file upload </th>														
													<th width="218"><input type="file" id="csvfile" name="csvfile" tabindex="2" /></th>
													<th width="120"><button class="btn btn-primary" type="submit" id="search"> Submit</button></th>
													</tr>
												  </thead>
												</table>
										    </form>
											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="20" class="center">
														   Sl No														</th>
														<th width="80">File Type </th>														
														<th width="50">File</th>
														<th width="43">Action</th>
													</tr>
												</thead>

												<tbody>
												   <?php 
												      $i    = 1;
												      foreach($allFileInfo as $v){ 
												     ?>
													<tr>
														<td class="center"><?php echo $i++; ?>
														<td>
															<a href="#"></a> <?php echo$v->type_name ?>														</td>														
														<td class="hidden-480">CSV</td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $v->id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

																<a class="red" href="#" data-id="<?php echo $v->id ?>">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>																</a>															</div>

															<div class="hidden-md hidden-lg">
																<div class="inline pos-rel">
																	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>																	</button>
																</div>
												      </div></td>
													</tr>

													<?php } ?>

												</tbody>
											</table>
</body>
											
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
    <?php $this->load->view('formJsLinkPage'); ?>
    <script type="text/javascript">
      $('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('controlpanel/registerMember/csvDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});



      $(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/registerMember/csvEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "html",
				success:function(data){
					$('#csvEditView').html(data);
				}
			});
			
			e.preventDefault();
		});

    </script>
	
	</body>
</html>
