<table id="inputTable" class="table table-striped table-bordered table-hover">
<thead>
	<tr>
		<th width="24" class="center">
		   <label class="pos-rel">
				<input type="checkbox" class="ace allCheck" />
				<span class="lbl"></span></label>
				<label class="pos-rel">
				<a class="red" href="#" data-id="<?php echo $id ?>">
					<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
				<span class="lbl"></span></label>
		</th>
		<th width="24">Sl No </th>
		<th width="86">Menu Name </th>
		<th width="128">Title</th>
		<th width="57">Position Top </th>
		<th width="57">Position Footer </th>
		<th width="50">Image</th>
		<th width="42">Status</th>														
		<th width="42">Action</th>
	</tr>
</thead>

<tbody>
<?php 
// print_r($menuInfo);
 $i = 1;
  foreach ($menuInfo as $v){
     $id  		    = $v->id;
?>
	<tr>
		<td class="center">
			<label class="pos-rel">
				<input type="checkbox" name="menu_id[]" class="ace" value="<?php echo $id ?>" />
				<span class="lbl"></span></label></td>

		<td><?php echo $i++ ?></td>
		<td><a href="#"><?php echo $v->menu_name; ?></a></td>
		<td><a href="#"><?php echo $v->menu_title; ?></a></td>
		<td><a href="#"><?php echo $v->position_top; ?></a></td>
		<td><a href="#"><?php echo $v->position_footer; ?></a></td>
		<td><img src="<?php echo base_url("Images/Menu_image/$v->image"); ?>" height="50" width="50" /></td>
		<td>
			<a href="#"><?php echo $v->status; ?></a></td>														
		<td>
			<div class="hidden-sm hidden-xs action-buttons">
				<a class="green" href="#" data-id="<?php echo $id ?>">
					<i class="ace-icon fa fa-pencil bigger-130"></i></a>

				<a class="red" href="#" data-id="<?php echo $id ?>">
					<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>

			<div class="hidden-md hidden-lg">
				<div class="inline pos-rel">
					<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
						<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>

					<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
						<li>
							<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
								<span class="green">
									<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>

						<li>
							<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
								<span class="red">
									<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
					</ul>
				</div>
			</div></td>
	</tr>
	<?php }  ?>
	<!-- <tr>
		<th height="43" class="center">
			<label class="pos-rel"><span class="lbl"></span></label></th>
		<th colspan="7"><?php echo $this->pagination->create_links(); ?> </th>
		<th></th>
	</tr> -->
</tbody>
</table>
<script>
    $(document).on("click", ".allCheck", function(e){
   	   var allChecked = this.checked; 
       var parent = $(this).parents('#inputTable');
       if(allChecked){
         parent.find('input[name="menu_id[]"]').prop('checked', true);
       } else {
         parent.find('input[name="menu_id[]"]').prop('checked', false);
       }
    });

	//callback handler for form submit
	$("#addForm").submit(function(e)
	{
		var postData = $(this).serializeArray();
		//console.log(postData);
		var formURL = $(this).attr("action");
		
		$.ajax(
		{
			url : formURL,
			type: "POST",
			async: false,
			data : postData,
			success:function(data){
				console.log(data);
				$("#listView").html(data);				
				$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea, #addForm select").val("");
				tinyMCE.get('ajaxfilemanager').setContent("");
				
				$.each($('.attachmentbody'), function(i, attachment) {
					attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
					reInitiateFileUpload(attachment);                        
				});


			}
		});
	
		e.preventDefault();
	});
	
	//callback handler for form submit
	//$(".edit").click(function(e)
	$(document).on("click", ".green", function(e)
	{
		var id 		= $(this).attr("data-id");
		var formURL = "<?php echo site_url('controlpanel/menuManage/menuEdit'); ?>";
		
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : {id: id},
			dataType: "json",
			success:function(data){
				$('#modal-form').modal('show');
				
				$('#id').val(data.id);
				$('#sl_no').val(data.sl_no);
				$('#menu_name').val(data.menu_name);
				$('#menu_title').val(data.menu_title);
				$('#sub_menu_status').val(data.sub_menu_status);
				$('#menu_image').val(data.menu_image);
				$('#ajaxfilemanager').val(data.menu_description);
				tinyMCE.get('ajaxfilemanager').setContent(data.menu_description);
				$('#position_top').val(data.position_top);
				$('#position_footer').val(data.position_footer);
				$('#status').val(data.status);
				$('.update').text("Update");
			}
		});
		
		e.preventDefault();
	});

   //form blank
	/*$(document).on("click", ".blue", function(e)
	{
		$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
		  tinyMCE.get('ajaxfilemanager').setContent("");
	});*/


   $('.red').on('click', function(e) {
       var deleteDta = $("#inputTable input").serializeArray();
       console.log(deleteDta);
		$.ajax({
		url: "<?php echo site_url('controlpanel/menuManage/menudelete') ?>",
		method: "POST",	
		data: $("#inputTable input").serializeArray(),
		dataType: "html",
		success: function(data){
			  $("#inputTable").find('input[name="menu_id[]"],.allCheck').prop('checked', false);
			  location.reload();
		   }
	    });

	   e.preventDefault();
		
	});

			
	$(document).on("click", ".blue", function(e){
       $('.update').text("Save");
	});
			
</script>