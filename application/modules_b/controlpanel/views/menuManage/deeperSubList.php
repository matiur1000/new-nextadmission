<table id="inputTable"  class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="8%" class="center">
				 <label class="pos-rel">
					<input type="checkbox" class="ace allCheck" />
					<span class="lbl"></span></label>
					<label class="pos-rel">
					<a class="red" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					<span class="lbl"></span></label>

				</th>
				<th width="10">Sn</th>
				<th width="88">Menu Name </th>
				<th width="101">Sub Menu Name </th>
				<th width="143">Deeper Sub Name </th>
				<th width="61">Title</th>
				<th width="57">Position  </th>
				<th width="50">Image</th>
				<th width="58">Status</th>														
				<th width="54">Action</th>
			</tr>
		</thead>
	
		<tbody>
		<?php 
		// print_r($deeperSubInfo);
         $i = 1;
		  foreach ($deeperSubInfo as $v){
			 $id  		    = $v->id;
		?>
			<tr>
				<td class="center">
					<label class="pos-rel">
					<input type="checkbox" name="deep_id[]" class="ace" value="<?php echo $id ?>" />
						<span class="lbl"></span></label></td>
	
				<td><a href="#"><?php echo $i++; ?></a></td>
				<td><a href="#"><?php echo $v->menu_name; ?></a></td>
				<td><a href="#"><?php echo $v->sub_menu_name; ?></a></td>
				<td><a href="#"><?php echo $v->deeper_sub_menu_name; ?></a></td>
				<td><a href="#"><?php echo $v->deeper_sub_title; ?></a></td>
				<td><a href="#"><?php echo $v->position; ?></a></td>
				<td><img src="<?php echo base_url("Images/Deeper_image/$v->image"); ?>" height="50" width="50" /></td>
				<td>
					<a href="#"><?php echo $v->status; ?></a></td>														
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="#" data-id="<?php echo $id ?>">
							<i class="ace-icon fa fa-pencil bigger-130"></i></a>
	
						<a class="red" href="#" data-id="<?php echo $id ?>">
							<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>
	
					<div class="hidden-md hidden-lg">
						<div class="inline pos-rel">
							<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
								<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>
	
							<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
								<li>
									<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
										<span class="green">
											<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>
	
								<li>
									<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
							</ul>
						</div>
					</div></td>
			</tr>
			<?php } ?>
			
		</tbody>
	</table>
<script>

	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="deep_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="deep_id[]"]').prop('checked', false);
           }
        });


		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					console.log(data);
					$("#listView").html(data);				
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					tinyMCE.get('ajaxfilemanager').setContent("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});


				}
			});
			
			e.preventDefault();

	    });
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/menuManage/deeperSubEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#sl_no').val(data.sl_no);
					$('#deeper_sub_menu_name').val(data.deeper_sub_menu_name);
					$('#deeper_sub_title').val(data.deeper_sub_title);
					$('#deeper_sub_status').val(data.deeper_sub_status);
					$('#select_position').val(data.position);
					$('#status').val(data.status);
					$('.update').text("Update");
					$('#ajaxfilemanager').val(data.deeper_sub_description);
					tinyMCE.get('ajaxfilemanager').setContent(data.deeper_sub_description);
					
					
					
					$('#menu_id').html('<option value="">Select Menu</option>');
					
					$.each(data.menuList2, function(key, value){
						var option = '<option value="'+value.id+'">'+value.menu_name+'</option>';
						$('#menu_id').append(option);						
					});
					
					$('#menu_id').val(data.menu_id);
					
					
					$('#sub_menu_id').html('<option value="">Select Sub Menu</option>');
					
					$.each(data.subList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.sub_menu_name+'</option>';
						$('#sub_menu_id').append(option);						
					});
					
					$('#sub_menu_id').val(data.sub_menu_id);
				}
			});
			
			e.preventDefault();
		});

	 //form blank
		/*$(document).on("click", ".blue", function(e)
		{
			$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
			  tinyMCE.get('ajaxfilemanager').setContent("");
		});*/
		
		//Onchang for Sub menu 
		$("#menu_id").change(function() {
		var menu_id = $("#menu_id").val();			
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/menuManage/subMenu'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : menu_id },
			dataType : "html",
			success : function(data) {			
				$("#sub_menu_id").html(data);
			}
		  });
		
	   });	


		$('.red').on('click', function(e) {
	        var deleteDta = $("#inputTable input").serializeArray();
	        console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/deeperSubdelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="deep_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});
		
				
				//POSITION WISE SERIAL 
				$("#select_position").change(function() {
				var select_position = $("#select_position").val();			
				$.ajax({
					url : SAWEB.getSiteAction('controlpanel/menuManage/positioWiseMenu'), // URL TO LOAD BEHIND THE SCREEN
					type : "POST",
					data : { id : select_position },
					dataType : "html",
					success : function(data) {			
						$("#menu_id").html(data);
					}
				});
				
			});	


			$(document).on("click", ".blue", function(e){
               $('.update').text("Save");
			});
	</script>