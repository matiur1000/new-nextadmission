<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Next Admission</title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="http://nextadmission.com/resource/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://nextadmission.com/resource/css/bootstrap-theme.min.css" rel="stylesheet">
	 <link href="http://nextadmission.com/resource/css/font-awesome.min.css" rel="stylesheet">
  
  <!--CUSTOM BODY-->
	<link href="http://nextadmission.com/resource/css/custom.css" rel="stylesheet">
	<link href="http://nextadmission.com/resource/css/menu.css" rel="stylesheet">
	<link href="http://nextadmission.com/resource/css/default.css" rel="stylesheet">
	<link href="http://nextadmission.com/resource/css/ieonly.css" rel="stylesheet">

   <script type="text/javascript" language="javascript" src="http://nextadmission.com/adapter/javascript.html"></script>
	<script src="http://nextadmission.com/resource/js/jquery.min.js"></script>
	<script src="http://nextadmission.com/resource/source/mmenu.js"></script>
	
    <!--CUSTOM BODY-->
	<script src="http://nextadmission.com/resource/js/bootstrap.min.js"></script>
	<script src="http://nextadmission.com/resource/source/text_scroll.js"></script>
	
	<style type="text/css">
	#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
	#menu1 a:active {color:black;text-decoration:none;}
	#menu1 a:hover {color:black;background-color:#FFFF99}
	#menu1 a:visited {color:black;text-decoration:none;}

	</style>
	<meta property="og:title" content="Adamas Univesity" />
	<meta property="og:site_name" content="Next Admission" />
	<meta property="og:url" content="http://nextadmission.com/adamasuniversity" />
	<meta property="og:description" content="“মানসম্মত শিক্ষা জাতির মেরুদণ্ড” যে সকল শিক্ষার্থী কম খরচে মানসম্মত শিক্ষা নিয়ে উচ্চ মানের ক্যারিয়ার গড়তে চাই,তাদের জন্য ভারতের ১৭০ টা বিশ্ববিদ্যালয়/কলেজের প্রতিনিধিত্ব করছি।
Admission Fair of Indian Universities (Spot Admission with Scholarship), 2016
বিশ্ববিদ্যালয়ের ক্যাম্পাস দেখতে লিঙ্কটি ক্লিক করুন।" />
	
	<meta property="og:image" content="http://nextadmission.com/Images/Add_image/adamas_university2.png" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="630" />

	<meta property="fb:app_id" content="104256000022505" />
	
	<style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}

				.adStyle{
					cursor: pointer;
					box-shadow: 0px 0px 5px 0px #fff;
					padding:10px !important
				}
				
				.adStyle img{
					background-color: #fff;
				    border: 1px solid #ddd;
				    border-radius: 4px;
				    display: inline-block;
				    line-height: 1.42857;
				    margin-right: 10px;
				    padding: 4px;
				    transition: all 0.2s ease-in-out 0s;
				}
				.adStyle h5{
					font-size: 16px;
					font-weight: bold;
					margin-top: 0;
					margin-bottom: 5px;
				}


				.adStyle p{
					text-align: justify; 
				}

/* below style code is for right sidebar*/
.rightsidebar{
font-size: 14px;
 background: #ad9c9c;
 margin-bottom:10px;
 width:100%;
float: right;
}	
	.rightsidebar ul{}
	.rightsidebar ul li{
	padding: 10px;
	list-style: none;
	border-bottom: 1px solid gray;
	}
	.rightsidebar ul li.passive{
	background:#32b60a;
	}															
	.rightsidebar ul li a{
	color:#fff;
	font-weight:bold;
	 font-size:17px;
	 }
	
	.rightsidebar ul li a:hover{ 
	color:red;
	text-decoration:none;
	}					
	
	.logo{ background: #669999; height: 100px; text-align: center;}
	.name{background: gray;height: 100px; text-align: center;}
	.menu{background: #ff99cc;height: 100px; text-align: center;}
	.pull-left{width: 485px;}
	/* right sidebar code  end*/	
			
	.titleorgin{
		padding-left:180px;
		font-family:Arial, Helvetica, sans-serif;
		font-size:30px;
		color:#FFFFFF;
	}
		
	</style>

	<!-- Start Video and image share -->
	<style>
	.text_exposed_root {
	    display: none !important;
	}
	</style>
	<!-- end video and image share -->



</head>
  <body>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '104256000022505',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

	<div class="container-fluid">
           <div class="row">&nbsp;</div> 
			   <style>
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}

	.popover{
    max-width: 100%; /* Max Width of the popover (depending on the container!) */
 }
    .button {
	display: inline-block;
	background: #6FB4FB;
	color: #FFFFFF;
	text-decoration: none;
	padding: 5px 10px;
}

.badge-notify{
   background:red;
   position:relative;
   top: -25px;
   left: -5px;
}

[data-notifications] {
	position: relative;
}

[data-notifications]:after {
	content: attr(data-notifications);
	position: absolute;
	background: red;
	border-radius: 50%;
	display: inline-block;
	padding: 0.3em;
	color: #f2f2f2;
	right: -15px;
	top: -15px;
}

}	

 </style>


 <link href="http://nextadmission.com/resource/source/headercss.css" rel="stylesheet">

 <script type="text/javascript">
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
	}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

 

   <div class="row">
	<div class="col-lg-3 col-xs-12 col-sm-12 text-right" style="margin-top:10px;">
		<a href="http://nextadmission.com/home.html">
		  <img src="http://nextadmission.com/Images/Basic_image/1455373282.png" width="235" height="75" align="right" />
		</a>
	</div>
	
	<div class="col-lg-5 col-xs-12 col-sm-12" align="center">
		  &nbsp; 
	</div>
	
	<div class="col-lg-4 col-xs-12 col-sm-12">
	 		  <div class="row">
		    <div class="col-md-3 col-xs-3" align="right" style="padding-right:5px">
		        <button type="button" id="customePostion" class="login_reg pop" style="height:35px;" data-container="body" data-toggle="popover"  data-placement="bottom" data-html="true" data-target-content="#myModal" data-title="Login">Login</button>
		     </div>
		    <div class="col-md-3 col-xs-3" align="right" style="padding-left:5px">
		        <button type="button" class="login_reg pop" style="height:35px;" data-container="body" data-placement="bottom" data-toggle="popover"  data-html="true" data-target-content="#regModal" data-title="Registration">Registration</button>
		    </div>
		    <div class="col-md-3 col-xs-3" style="padding-left:5px">
		       <a href="http://nextadmission.com/liveChat.html"><input type="button" name="Button22" class="login_reg" value="Live Chat" style="height:35px; width:100px"/></a>
		    </div>
		    <div class="col-md-3 col-xs-3" align="left" style="padding-left:0">
		       <input type="button" name="Button3" value="News" class="login_reg goto-news" style="height:35px;"/>
		    </div>
		 </div>

	    
		   <div class="row">
		   	  <div class="col-md-2" style="padding-top:15px"> 
		   	   		    </div>
		    <div class="col-md-4" style="padding-top:15px"><span style="color:#FF0000">Under Construction</span>  </div>
		    <div class="col-md-6" style="padding-top:15px">	                    
		        <div class="span2">
		           <div id="google_translate_element"></div> 
		        </div>
		    </div>
		   </div>

	</div>
</div>



<!-- Modal -->
 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<form id="logInForm"  action="http://nextadmission.com/loginForm/loginAuthenticate.html" method="post" class="form-horizontal">
	  <div class="col-md-12">
	  	<span class="logInFail"></span>
	  </div>
	    
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="type" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="type" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1">		
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2">
		</div>
	  </div>
	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Sign in</button> &nbsp; &nbsp; <a href="http://nextadmission.com/registration/genForgotPaword.html"> Forgot Password?</a></label>
		</div>
	  </div>
	</form>
</div>



<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="regModalLabel" >
	
	  
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="regType" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="regType" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="col-sm-12">&nbsp;</div>

  
   <div id="organize_reg">
      <form id="regForm" action="http://nextadmission.com/registration/orgRegStore.html" method="post" enctype="multipart/form-data" class="form-horizontal">
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="text" class="form-control" id="account_name" name="account_name" 
					placeholder="Account Name" tabindex="1" value="">
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
		   <input type="text" class="form-control" id="organizationname" name="organizationname" placeholder="Organization Name" tabindex="1" >
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="email" class="form-control" id="user_id2" name="user_id" placeholder="Email / Login id" tabindex="2" 
					value=""><span class="chkEmail"></span>
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<select class="form-control" id="country_id_org" name="country_id_org"  tabindex="2" >
				<option value="" selected>Sellect Country Name</option>
								<option value="336" data-country-code="011 355 " data-country-code-phon="" data-nationality="Albanian">Albania</option>
								<option value="152" data-country-code="011 213 " data-country-code-phon="" data-nationality="Algerian">Algeria</option>
								<option value="338" data-country-code="011 376 " data-country-code-phon="" data-nationality="Andorran">Andorra</option>
								<option value="262" data-country-code="011 244 " data-country-code-phon="" data-nationality="Angolan">Angola</option>
								<option value="268" data-country-code="1 " data-country-code-phon="" data-nationality="dg">Anguilla (UK)</option>
								<option value="269" data-country-code="1 " data-country-code-phon="" data-nationality="fg">Antigua and Barbuda</option>
								<option value="313" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
								<option value="335" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
								<option value="339" data-country-code="Armenia " data-country-code-phon="" data-nationality="Armenian">Armenia </option>
								<option value="270" data-country-code="1 " data-country-code-phon="" data-nationality="saf">Aruba</option>
								<option value="388" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australian">Australia</option>
								<option value="332" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australia">Australia</option>
								<option value="340" data-country-code="011 43 " data-country-code-phon="" data-nationality="Austrian">Austria</option>
								<option value="341" data-country-code="011 994 " data-country-code-phon="" data-nationality="Azerbaijani">Azerbaijan</option>
								<option value="271" data-country-code="1 " data-country-code-phon="" data-nationality="Bahamian">Bahamas</option>
								<option value="1" data-country-code="880" data-country-code-phon="222" data-nationality="Bangladeshi">Bangladesh</option>
								<option value="272" data-country-code="1 " data-country-code-phon="" data-nationality="Barbadian">Barbados</option>
								<option value="342" data-country-code="011 375 " data-country-code-phon="" data-nationality="Belorussian">Belarus</option>
								<option value="343" data-country-code="011 32 " data-country-code-phon="" data-nationality="Belgian">Belgium</option>
								<option value="273" data-country-code="011 501 " data-country-code-phon="" data-nationality="waf">Belize</option>
								<option value="274" data-country-code="1 " data-country-code-phon="" data-nationality="sdg">Bermuda (UK)</option>
								<option value="314" data-country-code="011 591 " data-country-code-phon="" data-nationality="Bolivian">Bolivia</option>
								<option value="275" data-country-code="er" data-country-code-phon="" data-nationality="sgd">Bonaire (Neth.)</option>
								<option value="344" data-country-code="011 387 " data-country-code-phon="" data-nationality="Bosnian">Bosnia and Herzegovina</option>
								<option value="315" data-country-code="011 55 " data-country-code-phon="" data-nationality="Brazilian">Brazil</option>
								<option value="276" data-country-code="1 " data-country-code-phon="" data-nationality="b">British Virgin Islands (UK)</option>
								<option value="345" data-country-code="011 359 " data-country-code-phon="" data-nationality="Bulgarian">Bulgaria</option>
								<option value="277" data-country-code="1 " data-country-code-phon="" data-nationality="Canadian">Canada</option>
								<option value="278" data-country-code="1 " data-country-code-phon="" data-nationality="az">Cayman Islands (UK)</option>
								<option value="334" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
								<option value="316" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
								<option value="16" data-country-code="011 86 " data-country-code-phon="" data-nationality="Chinese">China</option>
								<option value="279" data-country-code="g" data-country-code-phon="" data-nationality="g">Clipperton Island (Fr.)</option>
								<option value="317" data-country-code="011 57 " data-country-code-phon="" data-nationality="Colombian">Colombia</option>
								<option value="280" data-country-code="011 506 " data-country-code-phon="" data-nationality="dg">Costa Rica</option>
								<option value="346" data-country-code="011 385 " data-country-code-phon="" data-nationality="Croatian">Croatia</option>
								<option value="281" data-country-code="011 53 " data-country-code-phon="" data-nationality="Cuban">Cuba</option>
								<option value="282" data-country-code="sfg" data-country-code-phon="" data-nationality="gd">Curaçao</option>
								<option value="347" data-country-code="011 357 " data-country-code-phon="" data-nationality="Cypriot">Cyprus</option>
								<option value="348" data-country-code="011 420 " data-country-code-phon="" data-nationality="Czech">Czech Republic</option>
								<option value="349" data-country-code="011 45 " data-country-code-phon="" data-nationality="Dane">Denmark</option>
								<option value="283" data-country-code="1 " data-country-code-phon="" data-nationality="Dominican">Dominica</option>
								<option value="284" data-country-code="1 " data-country-code-phon="" data-nationality="sg">Dominican Republic</option>
								<option value="318" data-country-code="011 593 " data-country-code-phon="" data-nationality="Ecuadorean">Ecuador</option>
								<option value="77" data-country-code="011 20 " data-country-code-phon="" data-nationality="Egyptian">Egypt</option>
								<option value="285" data-country-code="011 503 " data-country-code-phon="" data-nationality="Salvadorean">El Salvador</option>
								<option value="350" data-country-code="011 372 " data-country-code-phon="" data-nationality="Estonian">Estonia</option>
								<option value="267" data-country-code="011 251 " data-country-code-phon="" data-nationality="Ethiopian">Ethiopia</option>
								<option value="319" data-country-code="011 500 " data-country-code-phon="" data-nationality="Falkland Islands (United Kingdom)">Falkland Islands (United Kingdom)</option>
								<option value="389" data-country-code="011 679 " data-country-code-phon="" data-nationality="Fijian">Fiji</option>
								<option value="351" data-country-code="011 358 " data-country-code-phon="" data-nationality="Finn">Finland</option>
								<option value="353" data-country-code="011 33 " data-country-code-phon="" data-nationality="Frenchman">France</option>
								<option value="320" data-country-code="French Guiana (France)" data-country-code-phon="" data-nationality="French Guiana (France)">French Guiana (France)</option>
								<option value="354" data-country-code="Georgia " data-country-code-phon="" data-nationality="Georgian">Georgia </option>
								<option value="355" data-country-code="011 49 " data-country-code-phon="" data-nationality="German">Germany</option>
								<option value="356" data-country-code="011 30 " data-country-code-phon="" data-nationality="Greek">Greece</option>
								<option value="286" data-country-code="011 299 " data-country-code-phon="" data-nationality="fg">Greenland (Den.)</option>
								<option value="287" data-country-code="1 " data-country-code-phon="" data-nationality="Grenadian">Grenada</option>
								<option value="288" data-country-code="Guadeloupe (Fr.)" data-country-code-phon="" data-nationality="Guadeloupe (Fr.)">Guadeloupe (Fr.)</option>
								<option value="289" data-country-code="011 502 " data-country-code-phon="" data-nationality="Guatemalan">Guatemala</option>
								<option value="321" data-country-code="011 592 " data-country-code-phon="" data-nationality="Guyanese">Guyana</option>
								<option value="290" data-country-code="011 509 " data-country-code-phon="" data-nationality="Haitian">Haiti</option>
								<option value="291" data-country-code="011 504 " data-country-code-phon="" data-nationality="Honduran">Honduras</option>
								<option value="357" data-country-code="011 36 " data-country-code-phon="" data-nationality="Hungarian">Hungary</option>
								<option value="358" data-country-code="011 354 " data-country-code-phon="" data-nationality="Icelander">Iceland</option>
								<option value="2" data-country-code="91" data-country-code-phon="333" data-nationality="Indian">India</option>
								<option value="21" data-country-code="011 62 " data-country-code-phon="" data-nationality="Indonesian">Indonesia</option>
								<option value="38" data-country-code="011 98 " data-country-code-phon="" data-nationality="Iranian">Iran</option>
								<option value="359" data-country-code="011 353 " data-country-code-phon="" data-nationality="Irishman">Ireland</option>
								<option value="360" data-country-code="011 39 " data-country-code-phon="" data-nationality="Italian">Italy</option>
								<option value="292" data-country-code="1 " data-country-code-phon="" data-nationality="Jamaican">Jamaica</option>
								<option value="17" data-country-code="011 81 " data-country-code-phon="" data-nationality="Japanese">Japan</option>
								<option value="361" data-country-code="011 7 " data-country-code-phon="" data-nationality="Kazakh">Kazakhstan</option>
								<option value="265" data-country-code="011 254 " data-country-code-phon="" data-nationality="Kenyan">Kenya</option>
								<option value="390" data-country-code="011 686 " data-country-code-phon="" data-nationality="Kiribati">Kiribati</option>
								<option value="362" data-country-code="011 371 " data-country-code-phon="" data-nationality="Latvia">Latvia</option>
								<option value="363" data-country-code="011 423 " data-country-code-phon="" data-nationality="Liechtensteiner">Liechtenstein</option>
								<option value="364" data-country-code="011 370 " data-country-code-phon="" data-nationality="Lithuanian">Lithuania</option>
								<option value="365" data-country-code="011 352 " data-country-code-phon="" data-nationality="Luxembourger">Luxembourg</option>
								<option value="366" data-country-code="011 389 " data-country-code-phon="" data-nationality="Macedonian">Macedonia</option>
								<option value="404" data-country-code="60" data-country-code-phon="+60" data-nationality="Malaysian">Malaysia</option>
								<option value="367" data-country-code="Malta" data-country-code-phon="" data-nationality="Maltese">Malta</option>
								<option value="391" data-country-code="011 692 " data-country-code-phon="" data-nationality="Marshall Islands">Marshall Islands</option>
								<option value="293" data-country-code="Martinique (Fr.)" data-country-code-phon="" data-nationality="Martinique (Fr.)">Martinique (Fr.)</option>
								<option value="294" data-country-code="011 52 " data-country-code-phon="" data-nationality="Mexican">Mexico</option>
								<option value="392" data-country-code="011 691 " data-country-code-phon="" data-nationality="Micronesia">Micronesia</option>
								<option value="368" data-country-code="Moldova " data-country-code-phon="" data-nationality="Moldovan">Moldova </option>
								<option value="369" data-country-code="011 377 " data-country-code-phon="" data-nationality="Monacan">Monaco</option>
								<option value="370" data-country-code="011 382 " data-country-code-phon="" data-nationality="Montenegrin">Montenegro</option>
								<option value="295" data-country-code="1 " data-country-code-phon="" data-nationality="Montserrat (UK)">Montserrat (UK)</option>
								<option value="263" data-country-code="011 212 " data-country-code-phon="" data-nationality="Moroccan">Morocco</option>
								<option value="403" data-country-code="95" data-country-code-phon="+95-69" data-nationality="Burmese">Myanmar</option>
								<option value="393" data-country-code="011 674 " data-country-code-phon="" data-nationality="Nauru">Nauru</option>
								<option value="296" data-country-code="Navassa Island (US)" data-country-code-phon="" data-nationality="Navassa Island (US)">Navassa Island (US)</option>
								<option value="405" data-country-code="+977" data-country-code-phon="+977" data-nationality="Nepalese">Nepal</option>
								<option value="371" data-country-code="011 31 " data-country-code-phon="" data-nationality="Netherlands">Netherlands</option>
								<option value="394" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
								<option value="329" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
								<option value="297" data-country-code="011 505 " data-country-code-phon="" data-nationality="Nicaraguan">Nicaragua</option>
								<option value="40" data-country-code="011 234 " data-country-code-phon="" data-nationality="Nigerian">Nigeria</option>
								<option value="372" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
								<option value="333" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
								<option value="331" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norway">Norway</option>
								<option value="395" data-country-code="011 680 " data-country-code-phon="" data-nationality="Palau">Palau</option>
								<option value="298" data-country-code="011 507 " data-country-code-phon="" data-nationality="Panamanian">Panama</option>
								<option value="396" data-country-code="011 675 " data-country-code-phon="" data-nationality="Papua New Guinea">Papua New Guinea</option>
								<option value="322" data-country-code="011 595 " data-country-code-phon="" data-nationality="Paraguayan">Paraguay</option>
								<option value="323" data-country-code="011 51 " data-country-code-phon="" data-nationality="Peruvian">Peru</option>
								<option value="402" data-country-code="201" data-country-code-phon="+63" data-nationality="Filipino">Philippine</option>
								<option value="373" data-country-code="011 48 " data-country-code-phon="" data-nationality="Pole">Poland</option>
								<option value="374" data-country-code="011 351 " data-country-code-phon="" data-nationality="Portuguese">Portugal</option>
								<option value="299" data-country-code="1 " data-country-code-phon="" data-nationality="Puerto Rico (US)">Puerto Rico (US)</option>
								<option value="375" data-country-code="011 40 " data-country-code-phon="" data-nationality="Romanian">Romania</option>
								<option value="376" data-country-code="Russia " data-country-code-phon="" data-nationality="Russian">Russia </option>
								<option value="19" data-country-code="011 7 " data-country-code-phon="" data-nationality="Russian">Russia</option>
								<option value="300" data-country-code="Saba (Neth.)" data-country-code-phon="" data-nationality="Saba (Neth.)">Saba (Neth.)</option>
								<option value="301" data-country-code="Saint Barthélemy (Fr.)" data-country-code-phon="" data-nationality="Saint Barthélemy (Fr.)">Saint Barthélemy (Fr.)</option>
								<option value="302" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
								<option value="303" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Lucia">Saint Lucia</option>
								<option value="304" data-country-code="Saint Martin (Fr.)" data-country-code-phon="" data-nationality="Saint Martin (Fr.)">Saint Martin (Fr.)</option>
								<option value="305" data-country-code="011 508 " data-country-code-phon="" data-nationality="Saint Pierre and Miquelon (Fr.)">Saint Pierre and Miquelon (Fr.)</option>
								<option value="306" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
								<option value="397" data-country-code="011 685 " data-country-code-phon="" data-nationality="Samoa">Samoa</option>
								<option value="377" data-country-code="011 378 " data-country-code-phon="" data-nationality="San Marino">San Marino</option>
								<option value="26" data-country-code="011 966 " data-country-code-phon="" data-nationality="Saudi">Saudi Arabia</option>
								<option value="378" data-country-code="Serbia " data-country-code-phon="" data-nationality="Serbian">Serbia </option>
								<option value="307" data-country-code="Sint Eustatius (Neth.)" data-country-code-phon="" data-nationality="Sint Eustatius (Neth.)">Sint Eustatius (Neth.)</option>
								<option value="308" data-country-code="Sint Maarten" data-country-code-phon="" data-nationality="Sint Maarten">Sint Maarten</option>
								<option value="379" data-country-code="011 421 " data-country-code-phon="" data-nationality="Slovak">Slovakia</option>
								<option value="380" data-country-code="011 386 " data-country-code-phon="" data-nationality="Slovenian">Slovenia</option>
								<option value="398" data-country-code="011 677 " data-country-code-phon="" data-nationality="Solomon Islands">Solomon Islands</option>
								<option value="88" data-country-code="011 27 " data-country-code-phon="" data-nationality="af">South Africa</option>
								<option value="18" data-country-code="011 82 " data-country-code-phon="" data-nationality="dghj">South Korea</option>
								<option value="266" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
								<option value="381" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
								<option value="264" data-country-code="011 211 " data-country-code-phon="" data-nationality="Sudanese">Sudan</option>
								<option value="325" data-country-code="011 597 " data-country-code-phon="" data-nationality="Surinamese">Suriname</option>
								<option value="382" data-country-code="011 46 " data-country-code-phon="" data-nationality="Swede">Sweden</option>
								<option value="383" data-country-code="011 41 " data-country-code-phon="" data-nationality="Swiss">Switzerland</option>
								<option value="28" data-country-code="011 886 " data-country-code-phon="" data-nationality="Taiwanese">Taiwan</option>
								<option value="399" data-country-code="011 676 " data-country-code-phon="" data-nationality="Tonga">Tongabbb</option>
								<option value="309" data-country-code="1 " data-country-code-phon="" data-nationality="Trinidadian">Trinidad and Tobago</option>
								<option value="384" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
								<option value="25" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
								<option value="310" data-country-code="1 " data-country-code-phon="" data-nationality="Turks and Caicos Islands">Turks and Caicos Islands</option>
								<option value="400" data-country-code="011 688 " data-country-code-phon="" data-nationality="Tuvalu">Tuvalu</option>
								<option value="385" data-country-code="011 380 " data-country-code-phon="" data-nationality="Ukrainian">Ukraine</option>
								<option value="328" data-country-code="011 44 " data-country-code-phon="" data-nationality="British">United Kingdom</option>
								<option value="386" data-country-code="011 44 " data-country-code-phon="" data-nationality="United Kingdom">United Kingdom</option>
								<option value="311" data-country-code="1 " data-country-code-phon="" data-nationality="American">United States</option>
								<option value="407" data-country-code="+1" data-country-code-phon="+1" data-nationality="American">United States of America</option>
								<option value="312" data-country-code="United States Virgin Islands (US)" data-country-code-phon="" data-nationality="United States Virgin Islands (US)">United States Virgin Islands (US)</option>
								<option value="326" data-country-code="011 598 " data-country-code-phon="" data-nationality="Uruguayan">Uruguay</option>
								<option value="411" data-country-code="" data-country-code-phon="" data-nationality="Uzbekistani">Uzbekistan</option>
								<option value="401" data-country-code="011 678 " data-country-code-phon="" data-nationality="Vanuatu">Vanuatu</option>
								<option value="387" data-country-code="011 39 " data-country-code-phon="" data-nationality="Vatican City">Vatican City</option>
								<option value="327" data-country-code="011 58 " data-country-code-phon="" data-nationality="Venezuelan">Venezuela</option>
								<option value="408" data-country-code="+84" data-country-code-phon="+84" data-nationality="Vietnami">Vietman</option>
								<option value="410" data-country-code="+84" data-country-code-phon="" data-nationality="Vietnamese">Vietnam</option>
				                         
			</select>
		</div>
	  </div>

	  


	   <div class="form-group">
	  	<div class="col-sm-12">
			<input type="password" class="form-control password" id="password" name="password" placeholder="Password" tabindex="2" value="">		   <span class="first"></span>
		</div>
	  </div>

	  <div class="form-group">
	    <div class="col-sm-12">
			<input type="password" class="form-control conformpassword" id="con_password" name="con_password" placeholder="Confirm Password" tabindex="2">
			<span class="second"></span>
		</div>
	  </div> 

	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Create Account</button>
		</div>
	  </div>
	</form>

 </div>


	 <div id="other_reg" style="display:none">
	     <form id="genReg" action="http://nextadmission.com/registration/genRegStore1.html" method="post" enctype="multipart/form-data" class="form-horizontal">

	       <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="email" class="control-label">UserId</label>	 -->	
				<input type="text" class="form-control" id="name" placeholder="Full Name" name="name" tabindex="5" 
					value=""> 			</div>
		  </div>

		  
		  <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="password" class="control-label">Password</label>		 -->
				<input type="text" class="form-control" id="user_id" name="user_id" placeholder="User Id/Email" tabindex="4" 
					value=""><span class="chk"></span>
			</div>
		  </div>


		  <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="password" class="control-label">Password</label>		 -->
				<select class="form-control" id="country_id" name="country_id"  tabindex="2" >
					<option value="" selected>Sellect Country Name</option>
										<option value="336" data-country-code="011 355 " data-country-code-phon="" data-nationality="Albanian">Albania</option>
										<option value="152" data-country-code="011 213 " data-country-code-phon="" data-nationality="Algerian">Algeria</option>
										<option value="338" data-country-code="011 376 " data-country-code-phon="" data-nationality="Andorran">Andorra</option>
										<option value="262" data-country-code="011 244 " data-country-code-phon="" data-nationality="Angolan">Angola</option>
										<option value="268" data-country-code="1 " data-country-code-phon="" data-nationality="dg">Anguilla (UK)</option>
										<option value="269" data-country-code="1 " data-country-code-phon="" data-nationality="fg">Antigua and Barbuda</option>
										<option value="313" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
										<option value="335" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
										<option value="339" data-country-code="Armenia " data-country-code-phon="" data-nationality="Armenian">Armenia </option>
										<option value="270" data-country-code="1 " data-country-code-phon="" data-nationality="saf">Aruba</option>
										<option value="388" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australian">Australia</option>
										<option value="332" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australia">Australia</option>
										<option value="340" data-country-code="011 43 " data-country-code-phon="" data-nationality="Austrian">Austria</option>
										<option value="341" data-country-code="011 994 " data-country-code-phon="" data-nationality="Azerbaijani">Azerbaijan</option>
										<option value="271" data-country-code="1 " data-country-code-phon="" data-nationality="Bahamian">Bahamas</option>
										<option value="1" data-country-code="880" data-country-code-phon="222" data-nationality="Bangladeshi">Bangladesh</option>
										<option value="272" data-country-code="1 " data-country-code-phon="" data-nationality="Barbadian">Barbados</option>
										<option value="342" data-country-code="011 375 " data-country-code-phon="" data-nationality="Belorussian">Belarus</option>
										<option value="343" data-country-code="011 32 " data-country-code-phon="" data-nationality="Belgian">Belgium</option>
										<option value="273" data-country-code="011 501 " data-country-code-phon="" data-nationality="waf">Belize</option>
										<option value="274" data-country-code="1 " data-country-code-phon="" data-nationality="sdg">Bermuda (UK)</option>
										<option value="314" data-country-code="011 591 " data-country-code-phon="" data-nationality="Bolivian">Bolivia</option>
										<option value="275" data-country-code="er" data-country-code-phon="" data-nationality="sgd">Bonaire (Neth.)</option>
										<option value="344" data-country-code="011 387 " data-country-code-phon="" data-nationality="Bosnian">Bosnia and Herzegovina</option>
										<option value="315" data-country-code="011 55 " data-country-code-phon="" data-nationality="Brazilian">Brazil</option>
										<option value="276" data-country-code="1 " data-country-code-phon="" data-nationality="b">British Virgin Islands (UK)</option>
										<option value="345" data-country-code="011 359 " data-country-code-phon="" data-nationality="Bulgarian">Bulgaria</option>
										<option value="277" data-country-code="1 " data-country-code-phon="" data-nationality="Canadian">Canada</option>
										<option value="278" data-country-code="1 " data-country-code-phon="" data-nationality="az">Cayman Islands (UK)</option>
										<option value="334" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
										<option value="316" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
										<option value="16" data-country-code="011 86 " data-country-code-phon="" data-nationality="Chinese">China</option>
										<option value="279" data-country-code="g" data-country-code-phon="" data-nationality="g">Clipperton Island (Fr.)</option>
										<option value="317" data-country-code="011 57 " data-country-code-phon="" data-nationality="Colombian">Colombia</option>
										<option value="280" data-country-code="011 506 " data-country-code-phon="" data-nationality="dg">Costa Rica</option>
										<option value="346" data-country-code="011 385 " data-country-code-phon="" data-nationality="Croatian">Croatia</option>
										<option value="281" data-country-code="011 53 " data-country-code-phon="" data-nationality="Cuban">Cuba</option>
										<option value="282" data-country-code="sfg" data-country-code-phon="" data-nationality="gd">Curaçao</option>
										<option value="347" data-country-code="011 357 " data-country-code-phon="" data-nationality="Cypriot">Cyprus</option>
										<option value="348" data-country-code="011 420 " data-country-code-phon="" data-nationality="Czech">Czech Republic</option>
										<option value="349" data-country-code="011 45 " data-country-code-phon="" data-nationality="Dane">Denmark</option>
										<option value="283" data-country-code="1 " data-country-code-phon="" data-nationality="Dominican">Dominica</option>
										<option value="284" data-country-code="1 " data-country-code-phon="" data-nationality="sg">Dominican Republic</option>
										<option value="318" data-country-code="011 593 " data-country-code-phon="" data-nationality="Ecuadorean">Ecuador</option>
										<option value="77" data-country-code="011 20 " data-country-code-phon="" data-nationality="Egyptian">Egypt</option>
										<option value="285" data-country-code="011 503 " data-country-code-phon="" data-nationality="Salvadorean">El Salvador</option>
										<option value="350" data-country-code="011 372 " data-country-code-phon="" data-nationality="Estonian">Estonia</option>
										<option value="267" data-country-code="011 251 " data-country-code-phon="" data-nationality="Ethiopian">Ethiopia</option>
										<option value="319" data-country-code="011 500 " data-country-code-phon="" data-nationality="Falkland Islands (United Kingdom)">Falkland Islands (United Kingdom)</option>
										<option value="389" data-country-code="011 679 " data-country-code-phon="" data-nationality="Fijian">Fiji</option>
										<option value="351" data-country-code="011 358 " data-country-code-phon="" data-nationality="Finn">Finland</option>
										<option value="353" data-country-code="011 33 " data-country-code-phon="" data-nationality="Frenchman">France</option>
										<option value="320" data-country-code="French Guiana (France)" data-country-code-phon="" data-nationality="French Guiana (France)">French Guiana (France)</option>
										<option value="354" data-country-code="Georgia " data-country-code-phon="" data-nationality="Georgian">Georgia </option>
										<option value="355" data-country-code="011 49 " data-country-code-phon="" data-nationality="German">Germany</option>
										<option value="356" data-country-code="011 30 " data-country-code-phon="" data-nationality="Greek">Greece</option>
										<option value="286" data-country-code="011 299 " data-country-code-phon="" data-nationality="fg">Greenland (Den.)</option>
										<option value="287" data-country-code="1 " data-country-code-phon="" data-nationality="Grenadian">Grenada</option>
										<option value="288" data-country-code="Guadeloupe (Fr.)" data-country-code-phon="" data-nationality="Guadeloupe (Fr.)">Guadeloupe (Fr.)</option>
										<option value="289" data-country-code="011 502 " data-country-code-phon="" data-nationality="Guatemalan">Guatemala</option>
										<option value="321" data-country-code="011 592 " data-country-code-phon="" data-nationality="Guyanese">Guyana</option>
										<option value="290" data-country-code="011 509 " data-country-code-phon="" data-nationality="Haitian">Haiti</option>
										<option value="291" data-country-code="011 504 " data-country-code-phon="" data-nationality="Honduran">Honduras</option>
										<option value="357" data-country-code="011 36 " data-country-code-phon="" data-nationality="Hungarian">Hungary</option>
										<option value="358" data-country-code="011 354 " data-country-code-phon="" data-nationality="Icelander">Iceland</option>
										<option value="2" data-country-code="91" data-country-code-phon="333" data-nationality="Indian">India</option>
										<option value="21" data-country-code="011 62 " data-country-code-phon="" data-nationality="Indonesian">Indonesia</option>
										<option value="38" data-country-code="011 98 " data-country-code-phon="" data-nationality="Iranian">Iran</option>
										<option value="359" data-country-code="011 353 " data-country-code-phon="" data-nationality="Irishman">Ireland</option>
										<option value="360" data-country-code="011 39 " data-country-code-phon="" data-nationality="Italian">Italy</option>
										<option value="292" data-country-code="1 " data-country-code-phon="" data-nationality="Jamaican">Jamaica</option>
										<option value="17" data-country-code="011 81 " data-country-code-phon="" data-nationality="Japanese">Japan</option>
										<option value="361" data-country-code="011 7 " data-country-code-phon="" data-nationality="Kazakh">Kazakhstan</option>
										<option value="265" data-country-code="011 254 " data-country-code-phon="" data-nationality="Kenyan">Kenya</option>
										<option value="390" data-country-code="011 686 " data-country-code-phon="" data-nationality="Kiribati">Kiribati</option>
										<option value="362" data-country-code="011 371 " data-country-code-phon="" data-nationality="Latvia">Latvia</option>
										<option value="363" data-country-code="011 423 " data-country-code-phon="" data-nationality="Liechtensteiner">Liechtenstein</option>
										<option value="364" data-country-code="011 370 " data-country-code-phon="" data-nationality="Lithuanian">Lithuania</option>
										<option value="365" data-country-code="011 352 " data-country-code-phon="" data-nationality="Luxembourger">Luxembourg</option>
										<option value="366" data-country-code="011 389 " data-country-code-phon="" data-nationality="Macedonian">Macedonia</option>
										<option value="404" data-country-code="60" data-country-code-phon="+60" data-nationality="Malaysian">Malaysia</option>
										<option value="367" data-country-code="Malta" data-country-code-phon="" data-nationality="Maltese">Malta</option>
										<option value="391" data-country-code="011 692 " data-country-code-phon="" data-nationality="Marshall Islands">Marshall Islands</option>
										<option value="293" data-country-code="Martinique (Fr.)" data-country-code-phon="" data-nationality="Martinique (Fr.)">Martinique (Fr.)</option>
										<option value="294" data-country-code="011 52 " data-country-code-phon="" data-nationality="Mexican">Mexico</option>
										<option value="392" data-country-code="011 691 " data-country-code-phon="" data-nationality="Micronesia">Micronesia</option>
										<option value="368" data-country-code="Moldova " data-country-code-phon="" data-nationality="Moldovan">Moldova </option>
										<option value="369" data-country-code="011 377 " data-country-code-phon="" data-nationality="Monacan">Monaco</option>
										<option value="370" data-country-code="011 382 " data-country-code-phon="" data-nationality="Montenegrin">Montenegro</option>
										<option value="295" data-country-code="1 " data-country-code-phon="" data-nationality="Montserrat (UK)">Montserrat (UK)</option>
										<option value="263" data-country-code="011 212 " data-country-code-phon="" data-nationality="Moroccan">Morocco</option>
										<option value="403" data-country-code="95" data-country-code-phon="+95-69" data-nationality="Burmese">Myanmar</option>
										<option value="393" data-country-code="011 674 " data-country-code-phon="" data-nationality="Nauru">Nauru</option>
										<option value="296" data-country-code="Navassa Island (US)" data-country-code-phon="" data-nationality="Navassa Island (US)">Navassa Island (US)</option>
										<option value="405" data-country-code="+977" data-country-code-phon="+977" data-nationality="Nepalese">Nepal</option>
										<option value="371" data-country-code="011 31 " data-country-code-phon="" data-nationality="Netherlands">Netherlands</option>
										<option value="394" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
										<option value="329" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
										<option value="297" data-country-code="011 505 " data-country-code-phon="" data-nationality="Nicaraguan">Nicaragua</option>
										<option value="40" data-country-code="011 234 " data-country-code-phon="" data-nationality="Nigerian">Nigeria</option>
										<option value="372" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
										<option value="333" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
										<option value="331" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norway">Norway</option>
										<option value="395" data-country-code="011 680 " data-country-code-phon="" data-nationality="Palau">Palau</option>
										<option value="298" data-country-code="011 507 " data-country-code-phon="" data-nationality="Panamanian">Panama</option>
										<option value="396" data-country-code="011 675 " data-country-code-phon="" data-nationality="Papua New Guinea">Papua New Guinea</option>
										<option value="322" data-country-code="011 595 " data-country-code-phon="" data-nationality="Paraguayan">Paraguay</option>
										<option value="323" data-country-code="011 51 " data-country-code-phon="" data-nationality="Peruvian">Peru</option>
										<option value="402" data-country-code="201" data-country-code-phon="+63" data-nationality="Filipino">Philippine</option>
										<option value="373" data-country-code="011 48 " data-country-code-phon="" data-nationality="Pole">Poland</option>
										<option value="374" data-country-code="011 351 " data-country-code-phon="" data-nationality="Portuguese">Portugal</option>
										<option value="299" data-country-code="1 " data-country-code-phon="" data-nationality="Puerto Rico (US)">Puerto Rico (US)</option>
										<option value="375" data-country-code="011 40 " data-country-code-phon="" data-nationality="Romanian">Romania</option>
										<option value="376" data-country-code="Russia " data-country-code-phon="" data-nationality="Russian">Russia </option>
										<option value="19" data-country-code="011 7 " data-country-code-phon="" data-nationality="Russian">Russia</option>
										<option value="300" data-country-code="Saba (Neth.)" data-country-code-phon="" data-nationality="Saba (Neth.)">Saba (Neth.)</option>
										<option value="301" data-country-code="Saint Barthélemy (Fr.)" data-country-code-phon="" data-nationality="Saint Barthélemy (Fr.)">Saint Barthélemy (Fr.)</option>
										<option value="302" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
										<option value="303" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Lucia">Saint Lucia</option>
										<option value="304" data-country-code="Saint Martin (Fr.)" data-country-code-phon="" data-nationality="Saint Martin (Fr.)">Saint Martin (Fr.)</option>
										<option value="305" data-country-code="011 508 " data-country-code-phon="" data-nationality="Saint Pierre and Miquelon (Fr.)">Saint Pierre and Miquelon (Fr.)</option>
										<option value="306" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
										<option value="397" data-country-code="011 685 " data-country-code-phon="" data-nationality="Samoa">Samoa</option>
										<option value="377" data-country-code="011 378 " data-country-code-phon="" data-nationality="San Marino">San Marino</option>
										<option value="26" data-country-code="011 966 " data-country-code-phon="" data-nationality="Saudi">Saudi Arabia</option>
										<option value="378" data-country-code="Serbia " data-country-code-phon="" data-nationality="Serbian">Serbia </option>
										<option value="307" data-country-code="Sint Eustatius (Neth.)" data-country-code-phon="" data-nationality="Sint Eustatius (Neth.)">Sint Eustatius (Neth.)</option>
										<option value="308" data-country-code="Sint Maarten" data-country-code-phon="" data-nationality="Sint Maarten">Sint Maarten</option>
										<option value="379" data-country-code="011 421 " data-country-code-phon="" data-nationality="Slovak">Slovakia</option>
										<option value="380" data-country-code="011 386 " data-country-code-phon="" data-nationality="Slovenian">Slovenia</option>
										<option value="398" data-country-code="011 677 " data-country-code-phon="" data-nationality="Solomon Islands">Solomon Islands</option>
										<option value="88" data-country-code="011 27 " data-country-code-phon="" data-nationality="af">South Africa</option>
										<option value="18" data-country-code="011 82 " data-country-code-phon="" data-nationality="dghj">South Korea</option>
										<option value="266" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
										<option value="381" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
										<option value="264" data-country-code="011 211 " data-country-code-phon="" data-nationality="Sudanese">Sudan</option>
										<option value="325" data-country-code="011 597 " data-country-code-phon="" data-nationality="Surinamese">Suriname</option>
										<option value="382" data-country-code="011 46 " data-country-code-phon="" data-nationality="Swede">Sweden</option>
										<option value="383" data-country-code="011 41 " data-country-code-phon="" data-nationality="Swiss">Switzerland</option>
										<option value="28" data-country-code="011 886 " data-country-code-phon="" data-nationality="Taiwanese">Taiwan</option>
										<option value="399" data-country-code="011 676 " data-country-code-phon="" data-nationality="Tonga">Tongabbb</option>
										<option value="309" data-country-code="1 " data-country-code-phon="" data-nationality="Trinidadian">Trinidad and Tobago</option>
										<option value="384" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
										<option value="25" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
										<option value="310" data-country-code="1 " data-country-code-phon="" data-nationality="Turks and Caicos Islands">Turks and Caicos Islands</option>
										<option value="400" data-country-code="011 688 " data-country-code-phon="" data-nationality="Tuvalu">Tuvalu</option>
										<option value="385" data-country-code="011 380 " data-country-code-phon="" data-nationality="Ukrainian">Ukraine</option>
										<option value="328" data-country-code="011 44 " data-country-code-phon="" data-nationality="British">United Kingdom</option>
										<option value="386" data-country-code="011 44 " data-country-code-phon="" data-nationality="United Kingdom">United Kingdom</option>
										<option value="311" data-country-code="1 " data-country-code-phon="" data-nationality="American">United States</option>
										<option value="407" data-country-code="+1" data-country-code-phon="+1" data-nationality="American">United States of America</option>
										<option value="312" data-country-code="United States Virgin Islands (US)" data-country-code-phon="" data-nationality="United States Virgin Islands (US)">United States Virgin Islands (US)</option>
										<option value="326" data-country-code="011 598 " data-country-code-phon="" data-nationality="Uruguayan">Uruguay</option>
										<option value="411" data-country-code="" data-country-code-phon="" data-nationality="Uzbekistani">Uzbekistan</option>
										<option value="401" data-country-code="011 678 " data-country-code-phon="" data-nationality="Vanuatu">Vanuatu</option>
										<option value="387" data-country-code="011 39 " data-country-code-phon="" data-nationality="Vatican City">Vatican City</option>
										<option value="327" data-country-code="011 58 " data-country-code-phon="" data-nationality="Venezuelan">Venezuela</option>
										<option value="408" data-country-code="+84" data-country-code-phon="+84" data-nationality="Vietnami">Vietman</option>
										<option value="410" data-country-code="+84" data-country-code-phon="" data-nationality="Vietnamese">Vietnam</option>
									</select>
			</div>
		  </div>

		   


		   <div class="form-group">
		  	<div class="col-sm-12">
				<input type="password" class="form-control password3" id="password" name="password" placeholder="Password" tabindex="23" 
				  value="" >				  <p class="third"></p>
			</div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-12">
				<input type="password" class="form-control conformpassword3" id="conform_password" name="conform_password" placeholder="Conform Password" 
					tabindex="20" value="">					<p class="fourth"></p>
			</div>
		  </div> 

		  
		  <div class="form-group">
			<div class="col-sm-12">
			  <button type="submit" class="btn btn-primary">Create Account</button>
			</div>
		  </div>

	     </form>
	 </div>

</div>

<script>

/*$(document).on("click", "image_share", function(e){

});

$(document).on("click", "video_share", function(e){

});*/

  	$(document).on("submit", "#logInForm", function(e){
  		
		var postData = $(this).serializeArray();
		var formURL  = $(this).attr("action");
		
		var type 	= $(this).find("input[name='type']:checked").val();
		
		if(type =='organization'){
		   var successUrl	= "http://nextadmission.com/organizationUserHome.html";
		}else{
		  var successUrl	= "http://nextadmission.com/generalUserHome.html";			
		}
	
		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data){
				if(data==1){
					$("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='hidden'], #logInForm input[type='password']").val("");
					$(".logInFail").text("Wrong email or password. Please try again!");
					$(".logInFail").css("color", "red");
					$("#msg").css("display", "block"); 
				} else if(data==2){
					 $(".logInFail").text("Please Select a type Organization or Other");
					 $(".logInFail").css("color", "red"); 
					 $("#msg").css("display", "block"); 					
				}else{
				 location.replace(successUrl);
				}
			}					
		});
	
	   	e.preventDefault();
	});


	function sharePost(){
		window.open('https://www.facebook.com/sharer/sharer.php?u=' + location.href, 'sharer', 'width=626,height=436');
	}


	
	$(".close").click(function(){
	   $("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='password']").val("");
	   $("#logInForm input[type='radio']").prop("checked",false);
	});

	$(window).ready(function(){
			$('[data-toggle="popover"]').popover({				
				content: function(){
					var target = $(this).attr('data-target-content');
					return $(target).html();
				}
			})
		});


	  $(document).on("change", "input[name='regType']", function(){
		   var typeValue = $(this).val();
		   var parents = $(this).parents('.popover-content');

		   if(typeValue =='organization'){
		    	parents.find("#organize_reg").css("display", "block");
		    	parents.find("#other_reg").css("display", "none");		    	
		   }else{
		     	parents.find("#organize_reg").css("display", "none");
		    	parents.find("#other_reg").css("display", "block");
		   }
		   
		  	
		});


	  $(document).on("change", "#country_id", function() {
		var country_id = $(this).val();
		parents = $(this).parents('.popover-content');	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
			   parents.find("#city_id").html(data);
			}
		});
		
	});
	
	
	$(document).on("change", "#country_id_org", function() {
		var country_id = $(this).val();
		parents = $(this).parents('.popover-content');
		console.log(country_id);	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				parents.find("#city_id_org").html(data);
			}
		});
		
	});



	// Password Count	
   
   $(document).on("keyup", ".password", function() {
	 var len = $(this).val().length;
	 parents = $(this).parents('.popover-content');
	    
		if(len<=1){
		parents.find(".first").text("");
		parents.find(".first").removeClass("red");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("green");
		
	  } else if(len<=4){
	  	parents.find(".first").text("Very Weak");
		parents.find(".first").addClass("red");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("green");
	   } else if(len<=8){

	   	parents.find(".first").text("Good");
		parents.find(".first").addClass("green");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("red");

	   } else if(len<=9){

	   	parents.find(".first").text("Good");
		parents.find(".first").addClass("green");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword", function() {
  	 parents 		= $(this).parents('.popover-content');
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		=  parents.find(".password").val();

	 if(conpass){

		  if(conpass != Pass){
		  	parents.find(".second").text("Your New Password and Confirm Password donot match!");
			parents.find(".second").addClass("red");
			parents.find(".second").removeClass("green");
		  
		  } else {
		  	parents.find(".second").text("Password Match");
			parents.find(".second").removeClass("red");
			parents.find(".second").addClass("green");
		   	
		  }

		} else {
		   parents.find(".second").text("Password Match");
		   parents.find(".second").removeClass("red");
		   parents.find(".second").removeClass("green");
		  
		}
	});
	
	
	
	// Password Count


	$(document).on("keyup", ".password3", function() {
	 var len = $(this).val().length;
	 parents = $(this).parents('.popover-content');
	    
		if(len<=1){
		parents.find(".third").text("");
		parents.find(".third").removeClass("red");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("green");
		
	  } else if(len<=4){
	  	parents.find(".third").text("Very Weak");
		parents.find(".third").addClass("red");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("green");
	   } else if(len<=8){

	   	parents.find(".third").text("Good");
		parents.find(".third").addClass("green");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("red");

	   } else if(len<=9){

	   	parents.find(".third").text("Good");
		parents.find(".third").addClass("green");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword3", function() {
  	 parents 		= $(this).parents('.popover-content');
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		=  parents.find(".password3").val();

	 if(conpass){

		  if(conpass != Pass){
		  	parents.find(".fourth").text("Your New Password and Confirm Password donot match!");
			parents.find(".fourth").addClass("red");
			parents.find(".fourth").removeClass("green");
		  
		  } else {
		  	parents.find(".fourth").text("Password Match");
			parents.find(".fourth").removeClass("red");
			parents.find(".fourth").addClass("green");
		   	
		  }

		} else {
		   parents.find(".fourth").text("Password Match");
		   parents.find(".fourth").removeClass("red");
		   parents.find(".fourth").removeClass("green");
		  
		}
	});
	


	
   

	$(document).on("blur", "#user_id", function() {
		parents 	= $(this).parents('.popover-content');
		var userId 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userId : userId },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				parents.find(".chk").text("This Email Already Exit!");
				parents.find(".chk").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				parents.find(".chk").text("");
				parents.find(".chk").removeClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});



	$(document).on("blur", "#user_id2", function() {
		parents 		= $(this).parents('.popover-content');
		var user_id 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/userEmailChk'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { user_id : user_id },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				parents.find(".chkEmail").text("This Email Already Exit!");
				parents.find(".chkEmail").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				parents.find(".chkEmail").text("");
				parents.find(".chkEmail").addClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});

	$(document).ready(function() {
	   setTimeout(getAllUnreadMsg, 2000);
	});
	

	// Get All unread msg
    
	 function getAllUnreadMsg() {
		  $.ajax({
	        url : SAWEB.getSiteAction('liveChat/getAllUnreadMsg'),
			type: "POST",
			data : {},
			dataType: "html",
	        success: function (data) { 
	        	$("#newMsgNum").html(data);

	          }
	       });
		   setTimeout(getAllUnreadMsg, 2000);
	}
	
	
	
</script>

           <div class="row">&nbsp;</div>            
       </div>      
           
          
	 
       <div class="container">
        	<div class="row">
    			<div class="col-md-12">
					<div class="row" style="padding-bottom:5px;">
						<link href="http://nextadmission.com/resource/assets/css/datepicker.min.css" rel="stylesheet">
<script src="http://nextadmission.com/resource/assets/js/bootstrap-datepicker.min.js"></script> 
 <style>
 .fomrlogo{
			float: left;
		}
		
		table td, table th{
			text-align: center;
		}
		.logo{
			float: left;
			background: #01AECC;
		}
		.title{
		    max-width: 298px;
		    margin: -22px auto;
		    text-align: center;
		}
		.title h2{
		    font-size: 40px;
			font-weight: bold;
		}
		.title p{
		    background: #222;
		    display: inline-block;
		    padding: 5px 8px;
		    color: #fff;
		    font-size: 18px;
		}

		.row input{
			margin-top:5px;
			margin-bottom:5px;
		}

		.admission_form{
			margin-top: 35px;
		}
		h4{
			margin-top: 20px;
		}
		.modal-body {
		    position: relative;
		    padding-top: 0px;
		}

		#instiute_source, #other_source{
		    margin-top: -2px;
		    margin-bottom: 0;
		    height: 34px;
    		padding: 6px 12px;
		}
		.counceling{
			margin-top: 5px;
		}
		.modal-lg {
		    width: 990px;
		}
    </style>
  
	<div class="container">
		<div class="col-md-2">
			<div class="row">
				<div class="logo">
				<!-- http://nextadmission.com/Images/Add_image/img.png -->
					<a data-toggle="modal" href="#orgLogo"><img src="#"  height="100" width="190" alt="Upload your org logo" /></a>
				</div>
			</div>
		</div>

	  <!-- Modal -->
	  <div class="modal fade" id="orgLogo" role="dialog">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Upload your Organization Logo</h4>
	        </div>
				<!--  -->
			
			<form action="#" method="post" enctype="multipart/form-data">
		        <div class="modal-body">
		        	<input type="file" title = "Upload Image" name="ogr_logo" id="org_logo"/>
		        </div>

		        <div class="modal-footer">
		          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		          <button type="submit" class="btn btn-sm btn-primary" data-dismiss="modal">Submit</button>
		        </div>
		    </form>

	      </div>
	    </div>
	  </div> <!--End Modal -->

		<div class="col-md-8">
			<div class="row" style="background:#4496EA; padding-top:31px; padding-bottom: 35px;">
				<div style="float:left;" class="titleorgin">Global Star Ltd </div>
				<div style="float:right; margin-right: 30px;">
						<button style="color:red;" type="button" class="btn btn-info" data-toggle="modal" data-target="#mymodqladmin">Apply for Admission</button>
				</div>
			</div>
		</div>
		<div class="col-md-2" style="background:#01AECC;">
		  <div class="row">
		  	<h2 style="padding-top:18px; padding-bottom:19px; padding-left:20px; color:#FFFFFF;">Latest News</h2>
		  </div>
		</div>
		<!--Start Admission Form Modal-->
		<div class="modal fade" id="mymodqladmin" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">Online Admission Form</h4>
				      </div>
				      <div class="modal-body">
				    	<header class="head_sec">
							<div class="container-fluid">
							
								<div class="fomrlogo" align="center">
									<img src="http://nextadmission.com/resource/logo/logo.jpg" alt="logo" style="height:80px;">
								</div>
								
								<div class="title">
									<h2>Global Star Ltd.</h2>
									<p>Admission Form</p>
								</div>
							</div>
						</header>
						<section class="admission_form">
							<div class="container-fluid">
								<form action="http://nextadmission.com/home/online.html" method="post">
								
									<div class="row">
									  <div class="col-md-4">
										<input type="text" name="sur_name" id="sur_name" placeholder="Sur Name" class="form-control">
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="given_name" id="given_name" placeholder="Given Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="passport_no" id="passport_no" placeholder="Passport No." class="form-control" >
									  </div>
									</div>
									
									<div class="row" style="padding-bottom:7px; padding-top:7px;">
									  <div class="col-md-4">
										<input type="text"  name="fathers_name" id="fathers_name" placeholder="Father's Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="mothers_name" id="mothers_name" placeholder="Mother's Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="std_email" id="std_email" placeholder="E-mail" class="form-control" >
									  </div>
									</div>
									
									<div class="row">
									  <div class="col-md-4">
										<input type="text"  name="std_mobile" id="std_mobile" placeholder="Mobile No." class="form-control" requied>
									  </div>
									  <div class="col-md-4" style="z-index:10000">
										<input type="text" class="form-control date-picker" name="date_of_birth" id="date_of_birth" placeholder="Date of Birth" aria-describedby="basic-addon1" tabindex="5" data-date-format="yyyy-mm-dd" required>
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="national_id" id="national_id" placeholder="National ID No. (if any)" class="form-control" >
									  </div>
									</div>
									<div class="row" style="padding-top:7px; padding-bottom:7px;">
									  <div class="col-md-3">
										<input type="text"  name="religion" id="religion" placeholder="Religion" class="form-control" >
									  </div>
									  <div class="col-md-3">
										<select id="marital_status" name="marital_status" class="form-control" required>
										  <option class="selected" value="" selected="" >Marital Status</option>
										  <option value="Single">Single</option>
										  <option value="Married">Married</option>
										  <option value="Divorced">Divorced</option>
										</select>
									  </div>
									  <div class="col-md-3">
										<input name="spouse_name" type="text" id="spouse_name" placeholder="Spouse Name" class="form-control" >
									  </div>
									  <div class="col-md-3">
										<input name="spouse_mobile" type="text" id="spouse_mobile" placeholder="Mobile No." class="form-control" >
									  </div>
									</div>
									<div class="row">
									  <div class="col-md-4">
										<input name="interested_country" type="text" id="interested_country" placeholder="Interested Country" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="interested_institute" type="text" id="interested_institute" placeholder="Interested Institute" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="interested_program_name" type="text" id="interested_program_name" placeholder="Interested Program Name" class="form-control" >
									  </div>
									</div>
									<div class="row" style="padding-top:7px; padding-bottom:7px;">
									  <div class="col-md-6">
										<textarea name="present_address" id="present_address" rows="2" cols="50" class="form-control" placeholder="Present Address..."></textarea>
									  </div>
									  <div class="col-md-6">
									<textarea name="permanent_address" rows="2" cols="50" id="permanent_address" class="form-control" placeholder="Permanent Address..."></textarea>
									  </div>
									</div>

									<h4>Father's Occupation Details:</h4>
									<div class="row">
									  <div class="col-md-4">
										<input name="guardian_name" type="text" id="guardian_name" placeholder="Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="guardian_mobile_no" type="text" id="guardian_mobile_no" placeholder="Mobile No." class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="guardian_designation" type="text" id="guardian_designation" placeholder="Designation" class="form-control" >
									  </div>
									</div>
									<div class="row" style="padding-bottom:7px; padding-top:7px;">

										<div class="col-md-6">
											<input name="guardian_ogranization_name" type="text" id="guardian_ogranization_name" placeholder="Ogranization Name/Office name (if any)" class="form-control" >
										</div>
									  <div class="col-md-6">
									  	<textarea name="guardian_address" id="guardian_address" rows="2" cols="50" placeholder="Address" class="form-control" ></textarea>
									  </div>
									</div>


									<h4>Reference in Bangladesh:</h4>
									<div class="row">
										<div class="col-md-4">
											<input name="employee_name" type="text" id="employee_name" placeholder="Name" class="form-control" >
										</div>
										<div class="col-md-4">
											<input name="employee_mobile_no" type="text" id="employee_mobile_no" placeholder="Mobile No." class="form-control" >
										</div>
										<div class="col-md-4">
										  	<textarea name="employee_address" id="employee_address" rows="2" cols="50" placeholder="Address" class="form-control" ></textarea>
										</div>
									</div>

									<div class="academic_qualification table-responsive">
										<h4>Academic Qualification:</h4>
										<table class="table table-striped table-bordered table-condensed table-hover text-center">
										    <thead>
										      <tr>
										        <th>Degree</th>
										        <th>Institute Name</th>
										        <th>Group</th>
										        <th>Result</th>
										        <th>Year</th>
										      </tr>
										    </thead>
										    <tbody>
										      <tr>
										      	<td>
													<select name="ssc_ol" id="ssc_ol" class="form-control" required>
													  <option value="" selected="" >Select one</option>
													  <option value="SSC">SSC</option>
													  <option value="OL">OL</option>
													  <option value="Diploma">Diploma</option>
													</select>
												</td>
										     <td><input name="ssc_institute" type="text" id="ssc_institute" placeholder="Write institute name..." class="form-control" ></td>
										      <td style="display:none;"><input type="text" id="ssc_group" name="ssc_group" placeholder="Write group name..." class="form-control" ></td>
										      <td>
													<select name="ssc_group" id="ssc_group" class="form-control" required>
													  <option value="" selected="" >Select Group</option>
													  <option value="Science">Science</option>
													  <option value="Humanities">Humanities</option>
													  <option value="Commerce">Commerce</option>
													</select>
												</td>
										      	<td><input type="text" id="ssc_result" name="ssc_result" placeholder="Result..." class="form-control" ></td>
										        <td style="display:none;"><input type="text" id="ssc_year" name="ssc_year" placeholder="Year..." class="form-control" ></td>
									      		<td>
													<select name="ssc_year" id="ssc_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>

										      </tr>
										      <tr>
										      	<td>
													<select name="hsc_al" name="hsc_al" class="form-control" required>
													  <option value="" selected="" >Select one</option>
													  <option value="HSC">HSC</option>
													  <option value="Diploma">Diploma</option>
													  <option value="AL">AL</option>
													</select>
												</td>
										        <td><input  name="hsc_institute" id="hsc_institute" type="text" id="" placeholder="Write institute name..." class="form-control" ></td>
										        <td style="display:none"><input  name="hsc_group" type="text" id="hsc_group" placeholder="Write group name..." class="form-control" ></td>
										        <td>
													<select name="hsc_group" id="hsc_group" class="form-control" required>
													  <option value="" selected="" >Select Group</option>
													  <option value="Science">Science</option>
													  <option value="Humanities">Humanities</option>
													  <option value="Commerce">Commerce</option>
													</select>
												</td>
										        <td><input  name="hsc_result" type="text" id="hsc_result" placeholder="Result..." class="form-control" ></td>
										        <td style="display:none;"><input  name="hsc_year" type="text" id="datetimepicker10" placeholder="Year..." class="form-control" ></td>
										        <td>
													<select name="hsc_year" id="hsc_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>
										      </tr>
										      <tr>
										        <td>Bachelor</td>
										   		<td><input  name="bachelor_institute" type="text" id="bachelor_institute" placeholder="Write Institute/University name" class="form-control" ></td>
										        <td><input  name="bachelor_group" type="text" id="bachelor_group" placeholder="Write Subject" class="form-control" ></td>
										        <td><input  name="bachelor_result" type="text" id="bachelor_result" placeholder="Result..." class="form-control" ></td>
										        <td style="display:none"><input  name="bachelor_year" type="text" id="datetimepicker10" placeholder="Year..." class="form-control" ></td>
										        <td>
													<select  name="bachelor_year" id="bachelor_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>
										    </tr>
										    <tr>
											    <td>Masters</td>
											    <td><input name="masters_institute" type="text" id="masters_institute" placeholder="Write Institute/University name" class="form-control" ></td>
											    <td><input name="masters_group" type="text" id="masters_group" placeholder="Write Subject" class="form-control" ></td>
											    <td><input name="masters_result" type="text" id="masters_result" placeholder="Result..." class="form-control" ></td>
											    <td style="display:none"><input name="masters_year" type="text" id="datetimepicker10" placeholder="Year..." class="form-control" ></td>
											    <td>
													<select name="masters_year" id="masters_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>
										      </tr>
										    </tbody>
										</table>
									</div>

									<div class="row">
									  <div class="col-md-6">
										<input style="display:none" type="text" name="other_proficiency" id="other_proficiency" placeholder="Other Proficiency." class="form-control" >
										<select name="other_proficiency" id="other_proficiency" class="form-control" required>
											<option class="selected" value="" selected="" >Select a Proficiency</option>
											<option value="IELTS">IELTS</option>
											<option value="GRE">GRE</option>
											<option value="TOEFL">TOEFL</option>
											<option value="ESL">ESL</option>
											<option value="GMAT">GMAT</option>
											<option value="LSAT">LSAT</option>
											<option value="Other">Other</option>
										</select>
									  </div>
									</div>

									<div><h4>How did you know about us?</h4></div>
									
									<div class="row">
										<div class="col-md-6">
											<select onchange="genderSelectHandler(this)" name="media_source" id="media_source" class="form-control" required>
												<option class="selected" value="" selected="" >Select a source</option>
												<option value="Social Media">Social Media</option>
												<option value="News Paper">News Paper</option>
												<option value="Institute">Institute</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="col-md-6">
											<select style="display:none" name="socialmedia_source" id="socialmedia_source" class="form-control">
												<option class="selected" value="" selected="" >Which social media?</option>
												<option value="Facebook">Facebook</option>
												<option value="Twitter">Twitter</option>
												<option value="Linkedin">Linkedin</option>
												<option value="Youtube">Youtube</option>
												<option value="Google+">Google+</option>
												<option value="Other">Other</option>
											</select>
										</div>
									  	
									  	<div class="col-md-6">
											<select style="display:none" name="newspaper_source" id="newspaper_source" class="form-control">
												<option class="selected" value="" selected="" >Which Newspaper?</option>
												<option value="Naya Diganta">Naya Diganta</option>
												<option value="Jugantor">Jugantor</option>
												<option value="Bangladesh Pratidin">Bangladesh Pratidin</option>
												<option value="Prothom Alo">Prothom Alo</option>
												<option value="Ittefaq">Ittefaq</option>
												<option value="Bhorer Kagoj">Bhorer Kagoj</option>
												<option value="Janakantha">Janakantha</option>
												<option value="Amader Shomoy">Amader Shomoy</option>
												<option value="Other">Other</option>
											</select>
										</div>

										<div class="col-md-6">
											<input style="display:none" type="text" id="instiute_source" name="instiute_source" placeholder="Write Institute/University name" class="form-control" >
										</div>

										<div class="col-md-6">
											<input style="display:none" type="text" id="other_source" name="other_source" placeholder="Write a source" class="form-control" >
										</div>
									</div>

									<div class="row counceling">
										<div class="col-md-4">
											<select name="phoneed_by" id="phoneed_by" class="form-control" required>
												<option class="selected" value="" selected="" >Phoned By:</option>
												<option value="Shamme Ferdaus">Shamme Ferdaus</option>
												<option value="Shamima Chowdhury Keya">Shamima Chowdhury Keya</option>
												<option value="Shamima Chowdhury Keya">Nusrat Majumder</option>
												<option value="Romana Remi">Romana Remi</option>
												<option value="Zehan">Zehan</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<select name="counseled_by" id="counseled_by" class="form-control" required>
												<option class="selected" value="" selected="" >Counseled By:</option>
												<option value="Shamme Ferdaus">Shamme Ferdaus</option>
												<option value="Shamima Chowdhury Keya">Shamima Chowdhury Keya</option>
												<option value="Nusrat Majumder">Nusrat Majumder</option>
												<option value="Romana Remi">Romana Remi</option>
												<option value="Zehan">Zehan</option>
												<option value="Other">Other</option>
											</select>
										</div>
									  	
									  	<div class="col-md-4">
											<select name="visa_by" id="visa_by" class="form-control" required>
												<option class="selected" value="" selected="" >Visa Processed By:</option>
												<option value="Shamme Ferdaus">Shamme Ferdaus</option>
												<option value="Shamima Chowdhury Keya">Shamima Chowdhury Keya</option>
												<option value="Nusrat Majumder">Nusrat Majumder</option>
												<option value="Romana Remi">Romana Remi</option>
												<option value="Zehan">Zehan</option>
												<option value="Other">Other</option>
											</select>
										</div>
									</div>

									<div class="row" style="margin-top:50px;">
									  <div class="col-md-12 checkbox">
									  	<label for="signchieckbox">
											<input value="ok" name="signchieckbox" type="checkbox" id="signchieckbox" required>I am the under signed, hereby declare that I will be responsible for Warning information provided here
									  	</label>
									  </div>
									</div>
									<div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								        <button type="submit" class="btn btn-success">Submit</button>
								    </div>
								</form>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
  
<!--End Admission Form Modal-->

<script>
	$('.date-picker').datepicker({
		autoclose: true	  
	}); 

	$('#datetimepicker10').datepicker({
		autoclose: true	 
	});

	$("#marital_status, #counseled_by, #phoned_by, #visa_by, #masters_year, #bachelor_year, #hsc_year, #ssc_year, #media_source, #socialmedia_source, #newspaper_source").click(function(){
    $(".selected").hide();
});

///Media Source
function genderSelectHandler(select){
	if(select.value == "Social Media"){
		$("#socialmedia_source").show(); 
	}else{
		$("#socialmedia_source").hide();
	}

	if(select.value == "News Paper"){
		$("#newspaper_source").show(); 
	}else{
		$("#newspaper_source").hide();
	}

	if(select.value == "Institute"){
		$("#instiute_source").show(); 
	}else{
		$("#instiute_source").hide();
	}

	if(select.value == "Other"){
		$("#other_source").show(); 
	}else{
		$("#other_source").hide();
	}
}
</script>					</div>
				</div>
		</div>
	<!--my top code close here-->   
	
	<!--my body code start here-->  
	    <div class="row" style="border:2px solid gray;">
    	<div class="col-md-12">
			<div class="row">
				<div class="col-md-10">
					
					<div class="row" style="padding:10px;">
						
							<div class="col-md-6">
								<div class="thumbnail">
									<img src="http://nextadmission.com/Images/Add_image/1471173883.jpg" style="height:250px; width:600px">
								</div>
							</div>

							<div class="col-md-6">
							 	<div class="thumbnail" style="height:255px;">
									<div>
										<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F1749648365247290%2Fvideos%2F1750762731802520%2F&show_text=0&width=560" width="435" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
									</div> 				
								</div>
							</div>
						<div class="" align="right" style="float:right; margin-right: 15px;">

							<div id="share-buttons">
							    <!-- Facebook -->																
							    <!-- <a href="https://www.facebook.com/sharer/sharer.php?u=http://nextadmission.com/home/orgWiseAdvritismentView/10/11.html&[images][0]=http://nextadmission.com/Images/Add_image/1471173883.jpg"
							    								   onclick="javascript:window.open(this.href, 'http://nextadmission.com/home/orgWiseAdvritismentView/10/11.html', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Share on Facbook" class="kk-share-button kk-share-via-facebook">
							    								    <img src="http://nextadmission.com/resource/images/facebook.png" alt="Facebook" style="max-height:20px;"/>
							    					    		</a> -->

					    		<a href="#"
								   onClick="sharePost()" title="Share on Facbook" class="kk-share-button kk-share-via-facebook">
								    <img src="http://nextadmission.com/resource/images/facebook.png" alt="Facebook" style="max-height:20px;"/>
					    		</a>


							   <!--  <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://nextadmission.com/home/orgWiseAdvritismentView/10/11.html&amp;src=sdkpreparse">Share</a> -->
							    
							    <!-- Google+ -->
							    <a href="https://plus.google.com/share?url={http://nextadmission.com/home/orgWiseAdvritismentView/10.html}" onClick="javascript:window.open(this.href,
				 					 '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share on Google" class="kk-share-button kk-share-via-google">
							       <img src="http://nextadmission.com/resource/images/google.png" alt="Google" style="max-height:20px;"/>	
							    </a>
							    <!-- Twitter -->
								<a href="https://twitter.com/share?url=http://nextadmission.com/home/orgWiseAdvritismentView/10.html&via=Assam Down Town University"
								   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
								   target="_blank" title="Share on Twitter" class="kk-share-button kk-share-via-twitter"><img src="http://nextadmission.com/resource/images/twitter.png" alt="Twitter" style="max-height:20px;"/>
								</a>
							</div>
						</div> 
					</div>
				
				
					<div style="padding-left:18px;">
						<div class="row" style="border:1px solid #CCCCCC; min-height:450px;">
							<div style="padding-left:10px; padding-right:10px; text-align:justify;">
							 	<p>&ldquo;মানসম্মত শিক্ষা জাতির মেরুদণ্ড&rdquo; যে সকল শিক্ষার্থী কম খরচে মানসম্মত শিক্ষা নিয়ে উচ্চ মানের ক্যারিয়ার গড়তে চাই,তাদের জন্য ভারতের ১৭০ টা বিশ্ববিদ্যালয়/কলেজের প্রতিনিধিত্ব করছি।</p>
								<p>#################################</p>
								<p>Admission Fair of Indian Universities (Spot Admission with Scholarship), 2016</p>
								<p>বিশ্ববিদ্যালয়ের ক্যাম্পাস দেখতে লিঙ্কটি ক্লিক করুন।</p>
								<p>MM University (one of the Best Deemed university in India)&nbsp;<a href="https://www.youtube.com/watch?v=17iZrH3vlqc">https://www.youtube.com/watch?v=17iZrH3vlqc</a></p>
								<p>Assam Down Town University (Best university of North East India)&nbsp;<a href="https://www.youtube.com/watch?v=iNYQRKuYbF4">https://www.youtube.com/watch?v=iNYQRKuYbF4</a></p>
								<p>############################################</p>
								<p>ভর্তি মেলা তারিখ Dhaka Office, Farmgate : June 04 to 09</p>
								<p>ভর্তি মেলা তারিখ Chittagong Office, Agrabad: June 11 to 14</p>
								<p>#############################</p>
								<p>ভর্তির জন্য আপনার আসনটি রেজিস্ট্রেশন করতে লিঙ্কটি ক্লিক করে ফর্মটি পূরণ করুন।</p>
								<div>
								<p><a href="http://nextadmission.com/form">http://nextadmission.com/form</a></p>
								</div>
								<p>MM University সুবিধা&nbsp; (scholarship offer: 10% to 40% based oন previous result)</p>
								<ul>
								<li>কোর্সঃ &nbsp;Bachelor in Engineering : Mechanical-Civil-CSE-IT-Electrical-ECE etc courses খরচঃ &nbsp;৪ বছরেঃ ( with 40% scholarship) = &nbsp;&nbsp;৪১১৬০০ টাকা</li>
								<li>, pharmacy- ৪ বছরেঃ ( with 30% scholarship) &ndash; ৩৩৫০০০ টাকা</li>
								<li>&nbsp;Hotel Management and Catering or Hospitality Management &ndash; ৪ বছরেঃ &nbsp;( with 30% scholarship)- ২৬০৮৮০ টাকা</li>
								<li>&nbsp;BBA: ৩ বছরেঃ &nbsp;&nbsp;&nbsp;( with 30% scholarship)- ১৯২৬০০ টাকা</li>
								<li>Physiotherapy : ৪ বছরেঃ &nbsp;( with 30% scholarship) : ২৬১৬০০ টাকা</li>
								</ul>
								<p>&nbsp;Medical Related so many courses (Bsc in Medical Lab Technology, Operation Technology, ৩ বছরেঃ &nbsp;&nbsp;( with 30% scholarship) : ১৯২৬০০/-</p>
								<ul>
								<li>Bachelor of Architecture -৫ বছরেঃ &nbsp;&nbsp;( with 30% scholarship) : ৩৫০০০০ টাকা</li>
								<li>LLB -৫ বছরেঃ &nbsp;&nbsp;( with 30% scholarship) : ২৮১১০০ টাকা।</li>
								</ul>
								<p>&nbsp;</p>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; থাকা+খাওয়া খরচঃ বছরে- ৬৩০০০/- টাকা ।</p>
								<p>#############################</p>
								<p>Assam Down Town University সুবিধা (Scholarship 80,000 INR for Each course during fair)</p>
								<ul>
								<li>কোর্সঃ &nbsp;Bachelor in Engineering : Mechanical-Civil-CSE-IT-Electrical-ECE etc courses খরচঃ &nbsp;৪ বছরেঃ ( after scholarship) = &nbsp;৩২৪০০০ টাকা</li>
								<li>Pharmacy- ৪ বছরেঃ (after scholarship) &ndash; ৪৮০০০০ টাকা</li>
								<li>&nbsp;Hotel Management and Catering or Hospitality Management &ndash; ৪ বছরেঃ &nbsp;( with 30% scholarship)- ২৫২০০০ টাকা</li>
								<li>&nbsp;BBA: ৩ বছরেঃ &nbsp;&nbsp;&nbsp;(after scholarship)- ২১৬০০০ টাকা</li>
								<li>Physiotherapy : ৪ বছরেঃ &nbsp;(after scholarship) : ৩২৪০০০ টাকা</li>
								<li>Medical Related so many courses (Bsc in Medical Lab Technology, Operation Technology, Biotechnology, Microbiology, Optometry : ৩ বছরেঃ &nbsp;( After scholarship) &ndash; ২৫২০০০ টাকা</li>
								</ul>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; থাকা+খাওয়া খরচঃ বছরে- ৬৯৬০০ টাকা কিন্তু মেসে থাকলে&nbsp; ৪৮০০০ টাকা লাগে।</p>
								<p>#####################################</p>
								<p>Admission process:</p>
								<ol>
								<li>All academic certificates + Transcript</li>
								<li>Passport Copy / Birth Certificate</li>
								<li>2 PP photos</li>
								<li>5000 Taka</li>
								</ol>
								<p>After submitting all required things, you will get admission letter immediately</p>
								<p>#################################</p>
								<p>&nbsp;</p>
								<p>Next Admission.com</p>
								<p>Global Star Ltd.(GSL)</p>
								<p>Dhaka office -ঢাকা অফিস</p>
								<p>31 Malek tower ( 9th floor- 2nd lift ) Farmgate, Dhaka, Bangladesh Mobile: +880 1911 342308,</p>
								<p>+88001715015972</p>
								<p>Email: smtbangladesh@yahoo.com,</p>
								<p>Chittagong office - চট্রগ্রাম অফিস</p>
								<p>Walikhan mansion, 603 sk mujib road (3rd floor), Agrabad, choumuhuni moar, chittagong</p>
								<p>Mobile: +880 1911 342308,</p>
								<p>+88001715015972 ,</p>
								<p>Email:&nbsp;<a href="mailto:info@nextadmission.com">info@nextadmission.com</a></p>
							</div>	
						</div>
					</div>
					
					<div class="row" style="padding-top:10px;">
 
	 <div class="container">
	 <nav class="navbar navbar bg-primary navbar-static-top">
		 <div class="col-md-6">
			<div class="input-group" style="padding-top:8px;">
			  <input type="text" class="form-control serchvalue" placeholder="Search for...">
			  <input type="hidden"  class="orgId" value="11">
			  <span class="input-group-btn">
				<button class="btn btn-default" type="button" id="orgserch"><i class="fa fa-search" aria-hidden="true"></i></button>
			  </span>
			</div>
		 </div>
	 
	 <div class="col-md-4" align="center">
		<div style="font-size:20px; padding-top:10px;">Previous Advertisement</div>
	 </div>
	  <style>

			table td, table th{
				text-align: center;
			}

	    </style>
	</nav>


	  <table class="table table-striped table-bordered table-hover table-condensed">
		<thead>
		  <tr>
			<th width="3%">SN</th>
			<th width="10%">Start Date</th>
			<th width="10%">End Date</th>
			<th width="30%" style="text-align:center">Title</th>
			<th width="31%" style="text-align:center">Details</th>
			<th width="8%" style="text-align:center">Picture</th>
			<th width="8%" style="text-align:center">Video</th>
		  </tr>
		</thead>
		<tbody id="serchresult">
		 
		
		  <tr>
			<td>1</td>
			<td>2016-05-26</td>
			<td>2018-02-28</td>
			<td style="text-align:center"><a href="http://nextadmission.com/home/orgWiseAdvritismentView/10/11">Assam Down Town University</a></td>
			<td style="text-align:center">
		<a href="http://nextadmission.com/home/orgWiseAdvritismentView/10/11" data-toggle="tooltip" data-placement="top" title="Advertisement details"><p>&ldquo;মানসম্মত শিক্ষা জাতির মেরুদণ্ড&rdquo; যে সকল �</a>
			</td>
			<td style="text-align:center; padding:0px;">
			 
			<img src="http://nextadmission.com/Images/Add_image/1471173883.jpg" style="max-height:35px;">
					</td>
			
			<td style="text-align:center">
		 
			<img src="http://nextadmission.com/Images/Add_image/NotAvailable.gif" style="max-height:35px;">
				</td>
			
			
			
		  </tr>
		  
		
		  <tr>
			<td>2</td>
			<td>2016-05-01</td>
			<td>2016-05-31</td>
			<td style="text-align:center"><a href="http://nextadmission.com/home/orgWiseAdvritismentView/4/11">down town Charity</a></td>
			<td style="text-align:center">
		<a href="http://nextadmission.com/home/orgWiseAdvritismentView/4/11" data-toggle="tooltip" data-placement="top" title="Advertisement details"><p style="text-align: justify;">The down town Charity Trust was formed, by the down town hospital ltd.,  under a Deed of</a>
			</td>
			<td style="text-align:center; padding:0px;">
			 
			<img src="http://nextadmission.com/Images/Add_image/1464068764.jpg" style="max-height:35px;">
					</td>
			
			<td style="text-align:center">
		 
			<img src="http://nextadmission.com/Images/Add_image/NotAvailable.gif" style="max-height:35px;">
				</td>
			
			
			
		  </tr>
		  
		
		  <tr>
			<td>3</td>
			<td>2016-05-21</td>
			<td>2017-06-30</td>
			<td style="text-align:center"><a href="http://nextadmission.com/home/orgWiseAdvritismentView/3/11">Study in India </a></td>
			<td style="text-align:center">
		<a href="http://nextadmission.com/home/orgWiseAdvritismentView/3/11" data-toggle="tooltip" data-placement="top" title="Advertisement details">ভারতে ভর্তি চলছে। ASSAM DOWN TOWN UNVIERSITY, India
	• Diploma পরীক্ষায় �</a>
			</td>
			<td style="text-align:center; padding:0px;">
			 
			<img src="http://nextadmission.com/Images/Add_image/1463827846.jpg" style="max-height:35px;">
					</td>
			
			<td style="text-align:center">
		 
			<img src="http://nextadmission.com/Images/Add_image/NotAvailable.gif" style="max-height:35px;">
				</td>
			
			
			
		  </tr>
		 	</tbody>
	  </table>
	  
	</div>
</div>


<script>
$("#orgserch").on('click', function(){
	var serchvalue = $(".serchvalue").val();
	var orgId      = $(".orgId").val();
	console.log(serchvalue);
	var serchURL = "http://nextadmission.com/home/orgIdyDataserch.html";
	
	$.ajax(
		{
			url : serchURL,
			type: "POST",
			data : {serchvalue : serchvalue, orgId : orgId},
			success:function(data){
				$("#serchresult").html(data);
			}
			
		});
	
	
});

</script>	
						
					
				</div>
				
				<div class="col-md-2">
					<div class="row" style="padding-top:10px;">
					<style type="text/css">
            .marquee,
            .marquee-with-options,
            .marquee-vert-before,
            .marquee-vert {
              overflow: hidden;
                        }
</style>

    <div class="thumbnail"> 
		 <div class='row marquee-with-options' style="height: 450px;">
			<div class="row">
</div>
		</div>
    </div>  



	<div class="modal fade" id="orgbreaking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content" id="orgbreakingNewidDeatial">
			 
			</div>
		  </div>
		</div>



<script>
$(function(){
    var $mwo = $('.marquee-with-options');
    $('.marquee').marquee();
    $('.marquee-with-options').marquee({
        //speed in milliseconds of the marquee
        speed: 12000,
        //gap in pixels between the tickers
        gap: 5,
        //gap in pixels between the tickers
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'up',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
        pauseOnHover: true
    });
    //Direction upward
    $('.marquee-vert').marquee({
        direction: 'up',
        speed: 1500
    });
    //pause and resume links
    $('.pause').click(function(e){
        e.preventDefault();
        $mwo.trigger('pause');
    });
    $('.resume').click(function(e){
        e.preventDefault();
        $mwo.trigger('resume');
    });
    //toggle
    $('.toggle').hover(function(e){
        $mwo.trigger('pause');
    },function(){
        $mwo.trigger('resume');
    })
    .click(function(e){
        e.preventDefault();
    })
});





$(".orglatenewId").on('click', function(){
	var id = $(this).attr("data-id");
	var orgurllate = "http://nextadmission.com/home/orglatenew.html";
	$.ajax(
	{
		url : orgurllate,
		type: "POST",
		data:{id:id},
		success:function(data){
			$('#orgbreaking').modal('show')
			$("#orgbreakingNewidDeatial").html(data);
		}
	});
	
});


</script>
						</div>
				</div>
			</div>
    	</div>
		
		
		
		<div class="col-md-12">
				<div class="row"><link href="http://nextadmission.com/resource/style.css" rel="stylesheet">
<div class="footer">
	  <div class="footer-grids">
		  <div class="container">
			  <div class="col-md-3 col-xs-6 footer-grid">
					
					<ul>
						 					</ul>
			  </div>
			 <div class="col-md-3 col-xs-6 footer-grid">
				
				  <ul>
										  </ul>
			 </div>
			 <div class="col-md-3 col-xs-6 footer-grid">
					
					<ul>
											</ul>
			 </div>
			 <div class="col-md-3 col-xs-6 footer-grid contact-grid">
					
					<ul>
											
				</ul>
				  <ul class="social-icons">
						<li><a href="https://www.facebook.com/nextadmissionbd/" title="Facebook" target="_blank"><span class="facebook"> </span></a></li>
						<li><a href="#" target="_blank"><span class="twitter" title="Twitter"> </span></a></li>
						<li><a href="#"><span class="thumb"> </span></a></li>
					</ul>
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<div class="copy" style="background:#2E2E2E;">
		 <p style="color:#CCCCCC;">Next Admission.com ©2015. All Rights Reserved.</p>
 </div></div>
			 </div>
    	</div>
		
 </div>
     
	 
	 
	 
	 
	 <!-- Modal -->
			<div class="modal fade"  id="myModalorg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				 
				</div>
			  </div>
			</div>
			
			
			
			<div class="modal fade"  id="myModalallDetailorg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
				 
				</div>
			  </div>
			</div>
	 
	 
	 
	 
	 
	 <script>
	 	$('.deteail').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#myModalorg .modal-content").html(data);
					$("#myModalorg").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#myModalorg").modal('show');
				}
			});
			e.preventDefault();
		});
		
		
		$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
		
		
	 </script>
	 
	 
	  <script>
	 	$('.alldetail').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#myModalallDetailorg .modal-content").html(data);
					$("#myModalallDetailorg").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#myModalallDetailorg").modal('show');
				}
			});
			e.preventDefault();
		});
	 </script>
	 	 
	 
	 
	   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src=""></script>
	  <script>
	 
	     $(document).ready(function(){
			$(".nav-tabs a").click(function(){
				$(this).tab('show');
			});
		   
		});
		
       resizereinit=true;

		menu[1] = {
		id:'menu1',
		fontsize:'100%',
		linkheight:35 , 
		hdingwidth:210 , 
		menuItems:[
		["University Home", "http://nextadmission.com/home/adWiseCompanyDetail/11/10.html", ""],
		["About Bangladesh", "http://nextadmission.com/home/orgIdbyIndiaabout/11/10.html",""],
		["About us", "http://nextadmission.com/home/orgIdbyabout/11/10.html",""],
		["Video", "http://nextadmission.com/home/orgIdbyvideo/11/10.html",""],
		["Photo Gallery", "http://nextadmission.com/home/orgIdbyphotograllay/11/10.html",""],
		["Course Fees 2016", "http://nextadmission.com/home/orgIdbycoursefees/11/10.html",""],
		["Eligiblity", "http://nextadmission.com/home/eligiblity/11/10.html",""],
		["Hostel & Other Cost", "http://nextadmission.com/home/hostelothercost/11/10.html",""],
		["Scholarship", "http://nextadmission.com/home/scholarship/11/10.html",""],
		["travel Itinerary", "http://nextadmission.com/home/travel/11/10.html",""],
		["How to Apply", "http://nextadmission.com/home/howtoapp/11/10.html",""],
		["When to Apply", "http://nextadmission.com/home/whentoapply/11/10.html",""],
		["Apply for Visa", "http://nextadmission.com/home/appforvisa/11/10.html",""],
		["Apply Online", "http://nextadmission.com/home/onlineapp/11/10.html",""],
		["Contact", "http://nextadmission.com/home/contrck/11/10.html",""],
		/*["Latest News", "http://nextadmission.com/",""],*/
		
		]}; 
		
		make_menus();
 </script>  </body>
</html>