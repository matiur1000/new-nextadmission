<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}
*my custom css*/
  .newz h3(word-wrap:break-word;)
	#box{ width:100%; height:auto;margin-left:0px !important; margin-top:-59px;}
	
	.col1{ background:#f07d02;border-bottom:solid 1px gray; width:600px; height:auto;margin: 0px;
padding: 0px;
overflow: hidden;}
		.india{width:200px; float:left;border-right: 1px solid !important;}
                .india:hover{background:#1783C1;}
                .india h2{font-weight:bold;}
		.mal{width:200px; float:left;border-right: 1px solid !important;}
                .mal:hover{background:#008080;}
                .mal h2{font-weight:bold;}
		.china{width:200px; float:left;}
                .china:hover{background:#8a2be2;}
                .china h2{font-weight:bold;}
	.col2{ background:#35b7e8;border-bottom:solid 1px gray;width:600px; height:auto; line-height:15px;margin: 0px;
padding: 0px;
overflow: hidden;}
		.guyana{width:200px; float:left;border-right: 1px solid !important;}
                .guyana:hover{background:#4169e1;}
                .guyana h2{font-weight:bold;}
 		.usa{width:200px; float:left;border-right: 1px solid !important;}
                .usa:hover{background:#2e8b57;}
                .usa h2{font-weight:bold;}
		.canada{width:200px; float:left;border-right:}
                .canada:hover{background:#ee82ee;}
                .canada h2{font-weight:bold;}

	.col3{ background:#87c156;border-bottom:solid 1px gray;width:600px; height:auto;margin: 0px;
padding: 0px;
overflow: hidden;}
	        .aus{width:200px; float:left;border-right: 1px solid !important;}
		.aus:hover{background:#483dbb;}
                .aus h2{font-weight:bold;}
		.newz{width:200px; float:left;border-right: 1px solid !important;}
                .newz:hover{background:#2f4f4f;}
                .newz h2{font-weight:bold;}
	        .poland{width:200px; float:left;}
                .poland:hover{background:#7f6cf3;}
                .poland h2{font-weight:bold;}


.post-title {
   display: none;
}		
		
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
              	<div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;"></div>
              </div>  
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12" style="margin-top:-32px; margin-bottom:50px">
<!--my tabble area-->
<div id="box" align="center">

<div class="col1">
<a href="<?php echo base_url('india');?>">
<div class="india">
<h3 style="color: #fff; height: 70px; line-height:70px;" align="center">INDIA</h3>
</div>
</a>
<a href="<?php echo base_url('malaysia');?>">
<div class="mal">
<h3 style="color: #fff;  height: 70px; line-height:70px;" align="center">MALAYSIA</h3>
</div>
</a>
<a href="<?php echo base_url('china');?>">
<div class="china">
<h3 style="color: #fff; height: 70px; line-height:70px;" align="center">CHINA</h3>
</div>
</a>

</div>
<div class="col2">
<a href="<?php echo base_url('guyana');?>">
<div class="guyana">
<h3 style="color: #fff;  height: 70px; line-height:70px;" align="center">GUYANA</h3>
</div>
</a>
<a href="<?php echo base_url('usa');?>">
<div class="usa">
<h3 style="color: #fff;  height: 70px; line-height:70px;" align="center">USA</h3>
</div>
</a>
<a href="<?php echo base_url('canada');?>">
<div class="canada">
<h3 style="color: #fff;  height: 70px; line-height:70px;" align="center">CANADA</h3>
</div>
</a>

</div>
<div class="col3">
<a href="<?php echo base_url('australia');?>">
<div class="aus">

<h3 style="color: #fff; height: 70px; line-height:70px;" align="center">AUSTRALIA</h3>
</div>
</a>

<a href="<?php echo base_url('newzealand');?>">
<div class="newz">

<h3 style="color: #fff; height: 70px; line-height:70px;" align="center">NEWZEALAND</h3>
</div>
</a>
<a href="<?php echo base_url('poland');?>">
<div class="poland">
<h3 style="color: #fff; height: 70px; line-height:70px;" align="center">POLAND</h3>
</div>
</a>

</div>
</div>
<!--my table area close-->
</div>			  

</div>			  
            </div>
		
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>