<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
        
      
        <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
		
		
        <style>
		  .area {
				background:#000000;
			}
			.area:hover{
			  background:#FF0000;
			}
		</style>
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            
             <div class="row">&nbsp;</div>     
              
			    <!--Recruters ad start-->
			   
			       <div class="row" style=" padding-left:10px; padding-right:15px;">
	 <div class="col-lg-12 wellB wellB-lg" style="border:solid 1px #e3e3e3; border-radius:3px;">
		  <div class="row"> <!--row start--> 
		     <?php 
				//print_r($middleAddInfo);
				 foreach ($middleRecruAdInfo as $v){
				  $id      		= $v->id;
				  $addViewLink 	= array('home','advritismentView', $id);
			 ?>
			  <div class="col-lg-4 col-sm-6 col-md-4 block_padd" style="padding:0px;">
               
				<div class="row" style="border:solid 1px #e3e3e3; margin:2px; border-radius:3px; background:#fff">
				   <div class="col-lg-3 col-xs-3" align="center" style="vertical-align:middle; padding:0px;">
					<div style="border: solid 1px #e3e3e3; margin:5px; float:center;">
					
							<a href="<?php echo site_url($addViewLink); ?>" style="text-decoration:none;color:#999;"><img src="<?php echo base_url("Images/Register_image/$v->image"); ?>"  alt="" width="77" height="65" /></a>
						
					</div>
				   </div>
				   <div class="col-lg-9 col-xs-9" style="font-size:12px; padding-left:3px; padding-top:3px;">
					 <div class="row">
					   <div class="col-lg-12">
						 <a href="<?php echo site_url($addViewLink); ?>" style="text-decoration:none;color:#0a81ce; font-size:14px;"><?php echo $v->name; ?></a>
					   </div>
					 </div>
					 <div class="row">
					   <div class="col-lg-12">
						 <ul style="list-style:none">
						   <li class="add_text_color"><a href="<?php echo site_url($addViewLink); ?>" style="text-decoration:none;color:#999;"> <?php echo $v->title; ?> (<?php echo $v->gender; ?>)</a></li>
						 </ul>
					   </div>
					 </div>
				   </div>
				   <br/>
				   <br/>
				  <span style="width:100%; padding-top:100px; color:#999;" class="text-left"><strong style="color:#000000;">Deadline :</strong> <?php echo $v->apllication_deadline; ?> </span>
				 </div>
                
			  </div>
			  <?php 	
					}
			  ?> 
		   </div>
	 </div>
  </div>
  
                <!--Recruters ad end-->
             <div class="row">&nbsp;</div>     
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    
  </body>
</html>