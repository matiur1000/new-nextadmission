<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	
         <?php $this->load->view('jsLinkPage'); ?>  
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
    
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">
							   <form method="post" action="#" name="formSearch" class="form-inline">
								 <div class="col-lg-3" align="right">
									<label for="search"><h5> Search All Post </h5></label>
								 </div>
								 <div class="col-lg-9" align="center">
								   <input type="search" name="search" id="search" class="form-control"  placeholder="Search by Programs" 
								   style="width:80%;"> 
										<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Search</button>  
										<div class="row">&nbsp;</div>     
								 </div>
								 </form>
								</div>
							  <div class="row">&nbsp;</div> 
							  <div class="row">
							  <?php $this->load->view('premiumUserPanel/premiumLeftMenuPage'); ?>
									
							 <?php $this->load->view('premiumUserPanel/middlePhotoPage'); ?>
                             <div class="row">
                               <div class="col-lg-12">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#F0F0F0;" class="table table-hover">
                                  <tr>
                                    <td width="14%"><strong>Sl No</strong></td>
                                    <td width="36%"><strong>Photo Title</strong></td>
                                    <td width="15%"><strong>Image</strong></td>
                                    <td width="35%"><strong>Action</strong></td>
                                  </tr>
                                  <?php 
                                   // print_r($UserWisePhotoInfo);
									$i = 1;
                                    foreach($UserWisePhotoInfo as $v){
                                        $id		= $v->id;
                                    
                                  ?>
                                  <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->title; ?></td>
                                    <td><img src="<?php echo base_url("/Images/photo_gallery/$v->image"); ?>" height="50" width="50" /></td>
                                    <td>
                                        <a href="<?php echo site_url('premiumUserHome/photoEdit/'. $id); ?>" name="Edit">
                                        <button name="edit" class="btn btn-link"> <i class="icon-edit"></i> Edit</button>
                                        </a>
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            <button  name="Delete" class="btn btn-link"> <i class="icon-remove-sign"></i> Delete</button>
                                        </a>
                                    </td>
                                  </tr> 
								 <?php } ?>
                                  <tr>
                                    <td colspan="4"></td>                       
                                  </tr>
                                 
                                </table> 
                               </div>
                             </div>
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script>
		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('premiumUserHome/photoDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});
	</script>

	
    
  </body>
</html>