<style>
	.main_menu{}
	.main_menu li{}
	.main_menu li:hover{
		background: #3B90E6 !important;
		color:#ffa15c !important;
	}
	.main_menu li a{}
	{
		display: block;
	}
</style>

<link href="<?php echo base_url('resource/source/menu/style.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('resource/source/menu/jquerymenu.js'); ?>"></script>

	<div class="container-fluid">
		<!-- Main Menu -->
		<div id="ddmenuMst">
		    <div class="menu-icon"></div>
		    <ul class="main_menu" style="background:#169FE6; color:#fff; text-align:start;">
		       <li><span class="top-heading" style="font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('home'); ?>">Home</a></span></li>
			   <li><span class="top-heading" style="font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="http://www.globalstarltdbd.com">Global Star Ltd</a></span></li>
			   <li><span class="top-heading" style="font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('currentoffer'); ?>">Current Offer</a></span></li>
				<li><span class="top-heading" style="font-size:16px; text-transform:uppercase;" id="asia">
					<a  style="text-decoration:none" href="#">Countries</a></span>
				</li>
				<li><span class="top-heading" style="font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('home/contactUS'); ?>">Contact Us</a></span></li>
				<li><span class="top-heading" style="font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="<?php echo site_url('home/blog'); ?>">Blog</a></span></li>
		    
		       <?php		   
			        foreach($menuInfo as $v){
					$menuId		= $v->id;
					$menuLink		 = array('menuDetails','index', $menuId);
		        ?>
		        <li class="full-width">
		        
		            <span class="top-heading" style="font-size:16px; text-transform:uppercase"><a style="text-decoration:none" href="<?php echo site_url($menuLink); ?>"><?php echo $v->menu_name; ?></a></span>
		             <?php if($v->sub){ ?>
		              <i class="caret"></i>
		                <div class="dropdown">
		                    <div class="dd-inner" style="background:#FFF !important; text-align:left; border:0px;">
		                     
		    					<?php 
		                        //print_r($allUserPostInfo);
		                        $a = 0;
		                        $b = 4;		
		                        foreach($v->sub as $s){
		                        $subCatId		 = $s->id;
		                        $Name	 	 	 = $s->sub_menu_name;
		                        $subLink		 = array('subMenuDetails','index',$menuId ,$subCatId);	
		                        
		                        if($a == 0) echo "<div class='row'>";
		                        if($a <= $b)  
		                        ?>
		                        <div class="column" style="font-size:13px; font-weight:bold; text-transform:uppercase; text-align:left; width:400px;">
		                           <a style="text-decoration:none;" href="<?php echo site_url($subLink);?>"><?php echo $Name; ?></a>
		                           
		    						<?php foreach($s->deeperSub as $dv){
		                               $dsubCatId		 = $dv->id;
		                               $deepSubLink		 = $subLink		 = array('deepSubMenuDetails','index',$menuId ,$subCatId,$dsubCatId);	
		                            ?>	
		                            <div style="font-size:12px; font-weight:normal; text-transform:uppercase;">
		                            <a style="text-decoration:none" href="<?php echo site_url($deepSubLink); ?>"><i style="font-size:10px;" class="glyphicon glyphicon-menu-right"></i><?php echo $dv->deeper_sub_menu_name; ?></a>
		                               
		                            </div>
		                             <?php } ?>
		                        </div>
		    					<?php 	$a++;	
		                        if($a == $b) echo "</div>";				
		                        if($a == $b) $a=0; } ?> 
		                        
		                    </div>
		                    
		                    
		                </div>

		            <?php } ?>
		        </li>

		         <?php } ?>
		    </ul>
		</div>
		<!-- End Main Menu -->
	</div>



<script type="text/javascript">
	   /*$("#asia, #south, #africa, #australia, #europ, #nort").click(function(e) {

	   		var caption = $(this).attr('data-value');
	   		var target = "#"+caption.replace(" ", "-")+"-Country";

	   		$("#globe .modal-title").html(caption);
	   		$(".country-list").hide();
	   		$(target).show();

	   		$("#globe").modal({
				keyboard: false,
				backdrop: 'static',
			});

			$("#globe").modal('show');

			e.preventDefault();
		});	*/


	   $("#asia, #south, #africa, #australia, #europ, #nort").on('click', function(e){
	   	    var globValue = $(this).attr('data-value');
			var url = "<?php echo site_url('home/globDetails'); ?>";
			
			$.ajax({
				url : url,  // URL TO LOAD BEHIND THE SCREEN
				type: "POST",
				data : {globValue: globValue},
				dataType : "html",
				success : function(data) {			
					$("#globe .modal-content").html(data);
					$("#globe").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#globe").modal('show');
				}
			});

			e.preventDefault();		
		});

	   	$("#globe .hello").on('click', function(){
	   		
	   		var caption = $(this).attr('data-value');
	   		var target = "#"+caption.replace(" ", "-")+"-Country";
	   		console.log(target);
	   		console.log($(target));
	   		$("#globe .modal-title").html(caption);
	   		$(".country-list").hide();	
	   		$(target).show();	
	   	});

	</script>