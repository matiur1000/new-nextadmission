	 <br/><div class="wellB wellB-lg">
   <div class="row">
	  <div class="col-lg-12 col-sm-6" style="height:835px;">
		 <div class="row">
			<div class="col-lg-12 col-xs-3" align="justify">
				   <div style="float:left; width:160px;">
                   <img src="<?php echo base_url("Images/Post_image/$UserPostInfo->image"); ?>" width="150" height="120" alt="" /></div>
                   <p class="tropic"><?php echo $UserPostInfo->title; ?></p>
			</div>
		 </div>
		 
		 <div class="row">&nbsp;</div>
		 
        <div class="row"> 
		  <form id="comment" action="<?php echo site_url('home/commentStore/'.$postId); ?>" method="post" enctype="multipart/form-data">
            <div class="col-lg-12">
			<?php if( isActiveUser() ) { ?>
		      <div class="row">
			      <div class="col-lg-12">
				    <div class="form-group">
						<input type="text" class="form-control" id="comment" placeholder="Comment" name="comment" tabindex="5">
				    </div>
				  </div>
			   </div>
			   
			  <div class="row">
			      <div class="col-lg-12">
				    <button type="submit" class="btn btn-info center-block pull-right" tabindex="1">Comment</button>
				  </div>
			   </div>
			   	</form>
			   <?php } else { ?>
                You must have to login first to comments here. <a href="<?php echo site_url('loginForm');?>?ref=<?php echo current_url(); ?>">Loign url.</a>
            <?php } ?>
		
			    <div class="row">&nbsp;</div>
			   <div class="row">
			   
			   <?php 
			   $i = 0;	
					 //print_r($UserCmntInfo);
					 foreach ($UserCmntInfo as $v){
					   $comment_id    = $v->id;
					   $post_id    	  = $v->post_id;
					   $name      	  = $v->name;
					   $multiId		  = array('home','replyStore', $post_id,$comment_id);
					  
					   
					  
					 
					 if ($i%2==0)
							$cls = "tr_even";
							else
							$cls = "tr_odd";
				  ?>
			      <section class="<?php echo $cls; ?>">
				  <div style="padding:5px 5px 5px 5px;">
						<div id="callback-<?php echo $comment_id?>" class="trigger" data-rel="#callback-form-<?php echo $comment_id?>">
							<div class="row">
							   <div class="col-lg-9" align="left"><span class="comnt" style="color:#0066FF; font-size:16px;"><?php echo $name; ?></span> : <span class="comnt2" style="color:#000; font-size:13px;"><?php echo $v->comment; ?></span></div>
							   <div class="col-lg-3 comnt" style="cursor:pointer;">Reply</div>
							</div>
					   </div>
					   <?php 
					     //print_r($v->com);
						  foreach ($v->reply as $reply){
						  $repName      	  = $reply->name;
						 
					?>
					   <div class="row">
					       
						   <div class="col-lg-10" align="left" style="padding-left:20px; padding-top:5px;"><span class="comnt"><?php echo $repName; ?></span> : <span class="comnt2"><?php echo $reply->reply; ?></span></div>
						</div>
						<?php } ?>
				   </div>
				   <div id="callback-form-<?php echo $comment_id?>" class="reply">
				      <form id="reply" action="<?php echo site_url($multiId); ?>" method="post" enctype="multipart/form-data">
						 <div class="form-group">
							<input type="text" class="form-control" id="reply" placeholder="Reply" name="reply" style="width:65%; height:50px;" tabindex="5">
						</div>
					  </form>
					</div>
				  </section>

				  <?php $i++; } ?>
			   </div>
			  
           </div>
		   
          </div>
	  </div>
   </div>
</div>