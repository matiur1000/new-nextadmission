<form id="update" action="<?php echo site_url('commentPopup/postUpdateAction'); ?>" method="post" enctype="multipart/form-data">
<div class="modal-header" style="border-bottom:3px solid #FF0000">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="blue bigger">Blog edit</h4>
</div>

    <div class="modal-body">
       <input type="hidden" name="blogeId" id="blogeId" value="<?php echo $postEditInfo->id ?>">
        <div class="row"> 
            <div class="col-xs-12 col-sm-12">
                <div class="form-group">
                    <label for="title">Topic</label>        
                    <div>
                       <input type="text" id="title" placeholder="Topic" name="title"
                        tabindex="1" class="form-control" value="<?php echo $postEditInfo->title ?>" required /> 
						
                    </div>
                </div>

                <div class="space-4"></div>
                <div class="row">
				  <div class="col-sm-3">
				  <div class="form-group">
					<label for="logo_image">Change Image</label>
					   <div class="controls">
							<div class="attachmentbody" data-target="#post_image" data-type="post_image">
                            <img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
                        </div> 
                        
                        <input name="post_image" id="post_image" type="hidden" value="" />
						</div>
				     </div>
				  </div>
                  
                  <div class="col-sm-3" align="left">
				  <div class="form-group">
					<label for="globe_image">Previous Image</label>
					   <div class="controls">
							<div class="attachmentbody">
                            <img class="upload" src="<?php echo base_url("Images/Post_image/$postEditInfo->image") ?>" />
                        </div> 
					    </div>
				     </div>
				  </div>
                  <div class="col-sm-6" align="left">
				  </div>
				</div>

                <div class="form-group">
                    <label for="description">Description</label>        
                    <div>
                       <textarea class="form-control" rows="40" cols="1" placeholder="*..Description" 
					   tabindex="8" name="description" id="ajaxfilemanager"  style="height:400px;"><?php echo $postEditInfo->description ?></textarea>
                    </div>
                </div>
											    
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn btn-sm" data-dismiss="modal">
            <i class="ace-icon fa fa-times"></i>
            Cancel
        </button>

        <button class="btn btn-sm btn-primary" type="submit">
            <i class="ace-icon fa fa-check"></i>
             Update
        </button>
    </div>
    </form>

    <script type="text/javascript">
      $(document).on("submit", "#update", function(e){
      	var postData  = $(this).serializeArray();
      	var formURL   = $(this).attr("action");
      	console.log(postData);

      	$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#blogDetails .modal-content").html(data);		
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
      	   e.preventDefault();
      });
     
    </script>
   
    