<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>view online form data</title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
	
	   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>resource/js/bootstrap.min.js"></script>
	
<style>
	.menu_online_admission{
		margin-bottom:35px;
		padding-top:20px;
	}
	.menu_online_admission ul{
		list-style:none;
	}
	.menu_online_admission ul li{
		float:left;
		margin-right:5px;
	}
	.menu_online_admission ul li a{
		disply:block;
		background:#5cb85c;
		color:#fff;
		padding: 10px 15px;
		text-decoration:none;
	}
	.menu_online_admission ul li a:hover{
		color:#FFFFFF;
		
	}
	
.mgs{
	color:red;
	font-weight:700;
}
</style>
</head>
<body style="background: rgb(239, 180, 164) none repeat scroll 0% 0%;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
			
				<div class="col-md-3">
				
				</div>
				
				<div class="col-md-9">
				<div class="menu_online_admission">
					<ul>
						<li><a href="<?php echo site_url('form/view');?>">Home</a></li>
						<li><a href="<?php echo site_url('form/logout');?>">Logout   <?php echo $logInUser ?></a></li>
						<li><a href="<?php echo site_url('form/changepasword');?>">Change Password</a></li>
						<li><a href="<?php echo site_url('form/formslide');?>">Ad Slide Manage</a></li>
						<li><a href="<?php echo site_url('form/notice');?>">Notice</a></li>
					</ul>
				</div>
			</div>
			
			
			
		
				<div class="col-md-3">
				
				</div>
		
				<div class="col-md-6">
				<form method="post" action="<?php echo site_url("form/passwordupdate"); ?>">
				  <div class="form-group">
					<label for="exampleInputEmail1">Old Password</label>
					<input type="password" class="form-control" id="oldpass" placeholder="Old Password"><span class="mgs"></span>
				  </div>
				  
				  <div class="form-group">
					<label for="exampleInputPassword1">New Password</label>
					<input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password">
				  </div>
				  
				  <button type="submit" class="btn btn-default submit">Update</button>
				</form>
				</div>

		</div>
		</div>
	</div>
	
	
	

	
	
	
	
</body>
</html>


<script>

	$("#oldpass").on('keyup', function(){
		var oldpass = $(this).val();
		var oldURL = "<?php echo site_url("form/chck"); ?>";
		
		$.ajax(
		{
			url: oldURL,
			type: "POST",
			data:{oldpass:oldpass},
			success:function(data){
				if(data==0){
					$(".mgs").text("Not Match!");
					$(".submit").attr("disabled", "disabled");
				} else {
					$(".mgs").text("");
					$(".submit").removeAttr("disabled", "disabled");
				}
			}
		});
		
		
	});


</script>




