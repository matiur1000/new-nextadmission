<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">  

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?php echo $page_title; ?></title>

	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />



    <!-- Bootstrap -->

    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">

    <!--CUSTOM BODY-->

	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">

	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">

		<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">

	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">

	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">

    



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <!--MENU CSS -->

        <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>

		 <script src="<?php echo base_url('resource/js/menu_script.js'); ?>"></script>

 



    

    <!--SHARE THIS

    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>

    <script type="text/javascript">var switchTo5x=true;</script>

        -->

  </head>

  <body>

        <div class="container-fluid">

           <div class="row">&nbsp;</div> 

			  <?php $this->load->view('headerPage'); ?>

           <div class="row">&nbsp;</div>            

       </div>

       <div class="container">         

            <div class="row">

            	<div class="col-lg-12">

                	<?php $this->load->view('menuPage'); ?>

                </div>

            </div> 

            <div class="row">&nbsp;</div>      

             

			<div class="col-lg-12" style="padding:30px; border:1px solid #CCCCCC;">

			 <div class="col-lg-12" style="padding:0 0 10px 0; font-size:15px">All search result data</div>

			      

               <?php 

	     if(!empty($searchUserResult)){



	     foreach($searchUserResult as $v){

	     	if($v->user_type =="General user"){

	     	  $mobile  = $v->mobile;

              $noLink = "#";

	     	}else{

              $mobile  = $v->mobile_com;

              $userProfileLink = array('home','adWiseCompanyDetail', $v->res_user_id);

	     	}

	  ?>

        <?php if ($v->user_type =="General user"){ ?>

		  <div class="col-lg-4 searchStyle" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">	

		   <?php if(!empty($v->image)){ ?>

		     <a href="<?php echo "#"; ?>">	

			   <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="77" height="77"  />	

			 </a>

		    <?php }else{ ?>

		    <a href="<?php echo "#"; ?>">	

			  <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="77" height="77" />

			</a>

		    <?php } ?>

			<p style="text-decoration:none;color:#0a81ce; font-size:16px; padding:0px;">

			 <a href="<?php echo "#"; ?>">	<?php echo $v->name; ?></a></p>

			<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->user_type; ?></p>

			<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->country_name; ?></p>

		  </div>

	    <?php }else{ ?>

	       <div class="col-lg-4 searchStyle" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">	

		   <?php if(!empty($v->image)){ ?>

		     <a href="<?php echo site_url($userProfileLink); ?>">	

			   <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="77" height="77"  />	

			 </a>

		    <?php }else{ ?>

		    <a href="<?php echo site_url($userProfileLink); ?>">	

			  <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="77" height="77" />

			</a>

		    <?php } ?>

			<p style="text-decoration:none;color:#0a81ce; font-size:16px; padding:0px;">

			 <a href="<?php echo site_url($userProfileLink); ?>">	<?php echo $v->name; ?></a></p>

			<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->user_type; ?></p>

			<p style="text-decoration:none;color:#999; padding:0px;"><?php echo $v->country_name; ?></p>

		  </div>

	    <?php } ?>



	<?php } } else if(!empty($searchTitleResult)) { ?> 





	<?php 	

		$i = 0;

		foreach ($searchTitleResult as $v){

			if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';

			



			$postId			= $v->blog_id;

			$description	= $v->description;

			$desPart	    = substr($description, 0,325);

			$postLink		= array('commentPopup','index', $postId);



			if($i % 2 == 0) {

				$class = "padding-left-0";

			} else {

				$class = "padding-right-0";

			}



			$i++;

  	?>

  	<div class="col-lg-4 post <?php echo $class ?>" data-url="<?php echo site_url($postLink); ?>">	

  	   <?php if(!empty($v->image)){ ?>		

		<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="58" height="58" />	

		<?php }else{ ?>

		   <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="58" height="58" />

		 <?php } ?>

		<h5><a><?php echo $v->blog_title; ?></a></h5>

		<p><?php echo $desPart; ?></p>	 

		

		<a class="pull-right">Read More</a>

	</div>

	<?php

		} } else if(!empty($searchProgramResult)) { 

	?>







	<?php 	

		$i = 0;

		foreach ($searchProgramResult as $v){

			if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';

			

			$programLink    = array('home','organizeCourseDetails', $v->organize_programe_id, $v->programe_id);



			if($i % 2 == 0) {

				$class = "padding-left-0";

			} else {

				$class = "padding-right-0";

			}



			$i++;

  	?>

  	<a href="<?php echo site_url($programLink);?>">

  	<div class="col-lg-4 searchStyle <?php echo $class ?>" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">

  	  <?php if(!empty($v->image)){ ?>				

		<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="77" height="77" />	

		<?php }else{ ?>

		   <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="77" height="77" />

		 <?php } ?>

		<h5><a href="<?php echo site_url($programLink);?>"><?php echo $v->regName; ?></a></h5>

		<a style="color:#000000" href="<?php echo site_url($programLink);?>"><p><?php echo $v->program_name; ?></p></a>	 

		<p><?php echo $v->country_name; ?></p>

		

		<a href="<?php echo site_url($programLink);?>" class="pull-right">Read More</a>

	</div>

	</a>

	<?php

		} } else if(!empty($searchAdResult)) { 

	?>





	<?php 	

		$i = 0;

		foreach ($searchAdResult as $v){

			if($i != 0 && $i % 2 == 0) echo '<div class="col-lg-12 padding-0"><hr ></div>';

			

			$adLink    = array('home','orgWiseAdvritismentView', $v->ad_id, $v->ad_user_id);



			if($i % 2 == 0) {

				$class = "padding-left-0";

			} else {

				$class = "padding-right-0";

			}



			$i++;

  	?>

  	<a href="<?php echo site_url($adLink);?>">

  	<div class="col-lg-4 searchStyle <?php echo $class ?>" style="border:solid 1px #e3e3e3; border-radius:3px; padding:5px;">	

  	 <?php if(!empty($v->image)){ ?>			

		<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="58" height="58" />

		<?php }else{ ?>

		  <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="77" height="77" />

		 <?php } ?>	

		<h5><a href="<?php echo site_url($adLink);?>"><?php echo $v->regName; ?></a></h5>

		<a style="color:#000000" href="<?php echo site_url($adLink);?>"><p><?php echo $v->ad_title; ?></p></a>	 

		<p><?php echo $v->country_name; ?></p>

		

		<a href="<?php echo site_url($adLink);?>" class="pull-right">Read More</a>

	</div>

	</a>



	<?php } }  ?> 

	  





</div>

           

             

       </div>

	    <div class="row">&nbsp;</div>    

       <!--footer-->

	   <?php $this->load->view('footerPage'); ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>

	

	

  </body>

</html>





