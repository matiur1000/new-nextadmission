<!DOCTYPE html>
<html lang="en" id="content">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
 	<style>
 		.wrapper{
 			background: #fff;
 		}
 		.fomrlogo{
			float: left;
		}

		.fomrlogo img{
			height: 60px !important;
		}

		.title{
		    max-width: 298px;
		    margin: -22px auto;
		    text-align: center;
		}
		.title h2{
		    font-size: 30px;
			font-weight: bold;
		}
		.title p{
            background: #222;
		    display: inline-block;
		    padding: 2px 3px;
		    color: #fff;
		    font-size: 18px;
		    margin-top: -6px;
		    line-height: 23px;
		}

		.row input{
			margin-top:5px;
			margin-bottom:5px;
		}

		.admission_form{
			margin-top: 35px;
		}
		h4{
			margin-top: 20px;
		}


		#instiute_source, #other_source{
		    margin-top: -2px;
		    margin-bottom: 0;
		    height: 34px;
    		padding: 6px 12px;
		}
		.counceling{
			margin-top: 5px;
		}
		th input, td input, .row input{
		    margin-top: 0px; 
		    margin-bottom: 0px;
		    -webkit-appearance:none; 
		    border: none;
   			border-bottom: 1px solid gray;
   			border-radius: 0;
		}
		th{
			text-align: center;
		}

		input:focus,
		select:focus,
		textarea:focus {
		    outline: none;
		    -webkit-appearance:none; 
		}

		td input, th input{
			outline: none;
		    -webkit-appearance:none; 
		    border: none;
		}

		table{
		    table-layout: fixed;
    		word-wrap: break-word;
		}
		.form-control {
		    display: block;
		    width: 100%;
		    height: 34px;
		    padding: 6px 12px;
		    font-size: 14px;
		    line-height: 1.42857143;
		    color: #222;
		    background-color: #fff;
		    background-image: none;
		    outline: none;
		    box-shadow:none;
		}
    </style>
    <link href="<?php echo base_url('resource/assets/css/print.css'); ?>" media="screen" rel="stylesheet">
</head>
<body>
	<div class="container wrapper">
		<!--Start Admission Form Modal-->
    	<header class="head_sec">
			<div class="container-fluid">
			
				<div class="fomrlogo" align="center">
					<img src="<?php echo base_url('resource/logo/logo.jpg'); ?>" alt="logo" style="height:80px;">
				</div>
				
				<div class="title">
					<h2>Global Star Ltd.</h2>
					<p>Admission Form</p>
				</div>
			</div>
		</header>
		<section class="admission_form">
			<div class="container-fluid">
				<form action="<?php echo site_url('home/onlineee///aldsfjl'); ?>" method="post">
				
					<div class="row">
					  <div class="col-md-4">
						<input type="text" value="Sur Name:  <?php if(isset($stdData->sur_name)) echo $stdData->sur_name;?>" name="sur_name" id="sur_name" placeholder="Sur Name" class="form-control">
					  </div>
					  <div class="col-md-4">
						<input type="text" value="Given Name:  <?php if(isset($stdData->given_name)) echo $stdData->given_name;?>" name="given_name" id="given_name" placeholder="Given Name" class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input type="text" value="Passport No:  <?php if(isset($stdData->passport_no)) echo $stdData->passport_no;?>" name="passport_no" id="passport_no" placeholder="Passport No." class="form-control" >
					  </div>
					</div>
					
					<div class="row" style="padding-bottom:7px; padding-top:7px;">
					  <div class="col-md-4">
						<input type="text" value="Father's Name:  <?php if(isset($stdData->fathers_name)) echo $stdData->fathers_name;?>" name="fathers_name" id="fathers_name" placeholder="Father's Name" class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input type="text" value="Mother's Name:  <?php if(isset($stdData->mothers_name)) echo $stdData->mothers_name;?>" name="mothers_name" id="mothers_name" placeholder="Mother's Name" class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input type="text" value="E-mail:  <?php if(isset($stdData->std_email)) echo $stdData->std_email;?>" name="std_email" id="std_email" placeholder="E-mail" class="form-control" >
					  </div>
					</div>
					
					<div class="row">
					  <div class="col-md-4">
						<input type="text" value="Mobile No:  <?php if(isset($stdData->std_mobile)) echo $stdData->std_mobile;?>" name="std_mobile" id="std_mobile" placeholder="Mobile No." class="form-control" requied>
					  </div>
					  <div class="col-md-4" style="z-index:10000">
						<input type="text" value="Date of Birth:  <?php if(isset($stdData->date_of_birth)) echo $stdData->date_of_birth;?>" class="form-control" name="date_of_birth" id="date_of_birth" placeholder="Date of Birth">
					  </div>
					  <div class="col-md-4">
						<input type="text" value="National ID No:  <?php if(isset($stdData->national_id)) echo $stdData->national_id;?>" name="national_id" id="national_id" placeholder="National ID No. (if any)" class="form-control" >
					  </div>
					</div>
					<div class="row" style="padding-top:7px; padding-bottom:7px;">
					  <div class="col-md-3">
						<input type="text" value="Religion:  <?php if(isset($stdData->religion)) echo $stdData->religion;?>" name="religion" id="religion" placeholder="Religion" class="form-control" >
					  </div>
					  <div class="col-md-3">
						<input type="text" value="Marital Status:  <?php if(isset($stdData->marital_status)) echo $stdData->marital_status;?>" name="marital_status" id="marital_status" placeholder="Marital Status" class="form-control" >
					  </div>
					  <div class="col-md-3">
						<input value="Spouse Name:  <?php if(isset($stdData->spouse_name)) echo $stdData->spouse_name;?>" name="spouse_name" type="text" id="spouse_name" placeholder="Spouse Name" class="form-control" >
					  </div>
					  <div class="col-md-3">
						<input value="Mobile No:  <?php if(isset($stdData->spouse_mobile)) echo $stdData->spouse_mobile;?>" name="spouse_mobile" type="text" id="spouse_mobile" placeholder="Mobile No." class="form-control" >
					  </div>
					</div>
					<div class="row">
					  <div class="col-md-4">
						<input value="Interested Country:  <?php if(isset($stdData->interested_country)) echo $stdData->interested_country;?>" name="interested_country" type="text" id="interested_country" placeholder="Interested Country" class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input value="Interested Institute:  <?php if(isset($stdData->interested_institute)) echo $stdData->interested_institute;?>" name="interested_institute" type="text" id="interested_institute" placeholder="Interested Institute" class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input value="Interested Program Name:  <?php if(isset($stdData->interested_program_name)) echo $stdData->interested_program_name;?>" name="interested_program_name" type="text" id="interested_program_name" placeholder="Interested Program Name" class="form-control" >
					  </div>
					</div>
					<div class="row" style="padding-top:7px; padding-bottom:7px;">
					  <div class="col-md-6">
						 <input value="Present Address:  <?php if(isset($stdData->present_address)) echo $stdData->present_address;?>" name="present_address" type="text" id="present_address" placeholder="Present Address..." class="form-control" >
					  </div>
					  <div class="col-md-6">
					  <input value="Permanent Address:  <?php if(isset($stdData->permanent_address)) echo $stdData->permanent_address;?>" name="permanent_address" type="text" id="permanent_address" placeholder="Permanent Address..." class="form-control" >
					  </div>
					</div>

					<h4>Father's Occupation Details:</h4>
					<div class="row">
					  <div class="col-md-4">
						<input value="Name:  <?php if(isset($stdData->guardian_name)) echo $stdData->permanent_address;?>" name="guardian_name" type="text" id="guardian_name" placeholder="Name" class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input value="Mobile No:  <?php if(isset($stdData->guardian_mobile_no)) echo $stdData->guardian_mobile_no;?>" name="guardian_mobile_no" type="text" id="guardian_mobile_no" placeholder="Mobile No." class="form-control" >
					  </div>
					  <div class="col-md-4">
						<input value="Designation:  <?php if(isset($stdData->guardian_designation)) echo $stdData->guardian_designation;?>" name="guardian_designation" type="text" id="guardian_designation" placeholder="Designation" class="form-control" >
					  </div>
					</div>
					<div class="row" style="padding-bottom:7px; padding-top:7px;">

						<div class="col-md-6">
							<input value="Ogranization Name:  <?php if(isset($stdData->guardian_ogranization_name)) echo $stdData->guardian_ogranization_name;?>" name="guardian_ogranization_name" type="text" id="guardian_ogranization_name" placeholder="Ogranization Name/Office name (if any)" class="form-control" >
						</div>
					  <div class="col-md-6">
					  	<input type="text" value="Address:  <?php if(isset($stdData->guardian_address)) echo $stdData->guardian_address;?>" name="guardian_address" id="guardian_address" placeholder="Address" class="form-control" >
					  </div>
					</div>


					<h4>Reference in Bangladesh:</h4>
					<div class="row">
						<div class="col-md-4">
							<input value="Name:  <?php if(isset($stdData->employee_name)) echo $stdData->employee_name;?>" name="employee_name" type="text" id="employee_name" placeholder="Name" class="form-control" >
						</div>
						<div class="col-md-4">
							<input value="Mobile No:  <?php if(isset($stdData->employee_mobile_no)) echo $stdData->employee_mobile_no;?>" name="employee_mobile_no" type="text" id="employee_mobile_no" placeholder="Mobile No." class="form-control" >
						</div>
						<div class="col-md-4">
							<input value="Address:  <?php if(isset($stdData->employee_address)) echo $stdData->employee_address;?>" name="employee_address" type="text" id="employee_address" placeholder="Address" class="form-control" >
						</div>
					</div>

					<div class="academic_qualification table-responsive">
						<h4>Academic Qualification:</h4>
						<table class="table table-striped table-bordered table-condensed table-hover text-center">
						    <thead>
						      <tr>
						        <th>Degree</th>
						        <th>Institute Name</th>
						        <th>Group</th>
						        <th>Result</th>
						        <th>Year</th>
						      </tr>
						    </thead>
						    <tbody>
						      <tr>
						      	<td>
									<?php if(isset($stdData->ssc_ol)) echo $stdData->ssc_ol;?>
								</td>
						     <td>
						     	<?php if(isset($stdData->ssc_institute)) echo $stdData->ssc_institute;?>
						     </td>
						      <td>
						      	<?php if(isset($stdData->ssc_group)) echo $stdData->ssc_group;?>
						      </td>
						      	<td>
						      		<?php if(isset($stdData->ssc_result)) echo $stdData->ssc_result;?>
						      	</td>
						        <td>
						       		<?php if(isset($stdData->ssc_year)) echo $stdData->ssc_year;?>
						        </td>
						      </tr>
						      <tr>
						      	<td>
						      		<?php if(isset($stdData->hsc_al)) echo $stdData->hsc_al;?>
								</td>
						        <td>
						        	<?php if(isset($stdData->hsc_institute)) echo $stdData->hsc_institute;?>
						        </td>
						        <td>
						        	<?php if(isset($stdData->hsc_group)) echo $stdData->hsc_group;?>
						        </td>
						        <td>
						        	<?php if(isset($stdData->hsc_result)) echo $stdData->hsc_result;?>
						        </td>
						        <td>
						        	<?php if(isset($stdData->hsc_year)) echo $stdData->hsc_year;?>
						        </td>
						      </tr>
						      <tr>
						        <td>Bachelor</td>
						   		<td>
						   			<?php if(isset($stdData->bachelor_institute)) echo $stdData->bachelor_institute;?>
						   		</td>
						        <td>
						        	<?php if(isset($stdData->bachelor_group)) echo $stdData->bachelor_group;?>
						        </td>
						        <td>
						        	<?php if(isset($stdData->bachelor_result)) echo $stdData->bachelor_result;?>
						        </td>
						        <td>
						        	<?php if(isset($stdData->bachelor_year)) echo $stdData->bachelor_year;?>
						        </td>
						    </tr>
						    <tr>
							    <td>Masters</td>
							    <td>
							    	<?php if(isset($stdData->masters_institute)) echo $stdData->masters_institute;?>
							    </td>
							    <td>
							    	<?php if(isset($stdData->masters_group)) echo $stdData->masters_group;?>
							    </td>
							    <td>
							    	<?php if(isset($stdData->masters_result)) echo $stdData->masters_result;?>
							    </td>
							    <td>
							    	<?php if(isset($stdData->masters_year)) echo $stdData->masters_year;?>
							    </td>
						      </tr>
						    </tbody>
						</table>
					</div>

					<div class="row">
					  <div class="col-md-3">
						<input value="Other Proficiency:  <?php if(isset($stdData->other_proficiency)) echo $stdData->other_proficiency;?>" type="text" name="other_proficiency" id="other_proficiency" placeholder="Other Proficiency." class="form-control" >
					  </div>
					</div>

					<div><h4>How did you know about us?</h4></div>
					
					<div class="row">
						<div class="col-md-6">
							<input value="Media Source:  <?php if(isset($stdData->media_source)) echo $stdData->media_source;?>" type="text" name="other_proficiency" id="other_proficiency" placeholder="Media Source" class="form-control" >
						</div>

						<div class="col-md-6">
							<?php if(!empty($stdData->socialmedia_source)){?>
							<input value="Social Media:  <?php if(isset($stdData->socialmedia_source)) echo $stdData->socialmedia_source;?>" type="text" name="socialmedia_source" id="socialmedia_source" placeholder="Social Media" class="form-control" >
							
							<?php } ?><?php if(!empty($stdData->newspaper_source)){?>
							<input value="News Paper:  <?php if(isset($stdData->newspaper_source)) echo $stdData->newspaper_source;?>" type="text" name="newspaper_source" id="newspaper_source" placeholder="News Paper" class="form-control" >
					  		
					  		<?php } ?><?php if(!empty($stdData->instiute_source)){?>
							<input value="Institute/University Name:  <?php if(isset($stdData->instiute_source)) echo $stdData->instiute_source;?>" type="text" id="instiute_source" name="instiute_source" placeholder="Write Institute/University name" class="form-control" >
							<?php } ?><?php if(!empty($stdData->other_source)){?>

							<input value="Source:  <?php if(isset($stdData->other_source)) echo $stdData->other_source;?>" type="text" id="other_source" name="other_source" placeholder="Write a source" class="form-control" >
							<?php } ?>
						</div>
					</div>

					<div class="row counceling">
						<div class="col-md-4">
							<input value="Phoned By:  <?php if(isset($stdData->phoneed_by)) echo $stdData->phoneed_by;?>" type="text" id="phoneed_by" name="phoneed_by" placeholder="Phoned By" class="form-control" >
						</div>

						<div class="col-md-4">
							<input value="Counseled By:  <?php if(isset($stdData->counseled_by)) echo $stdData->counseled_by;?>" type="text" id="counseled_by" name="counseled_by" placeholder="Counseled By" class="form-control" >

						</div>
					  	
					  	<div class="col-md-4">
					  		<input value="Processed By:  <?php if(isset($stdData->visa_by)) echo $stdData->visa_by;?>" type="text" id="visa_by" name="visa_by" placeholder="Visa Processed By" class="form-control" >
						</div>
					</div>
					<div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="submit" id="cmd" class="btn btn-success">Download</button>
				    </div>
				</form>
			</div>
		</section>
	</div>
  
	<!--End Admission Form Modal-->
	<script src="<?php echo base_url('resource/assets/js/jquery.2.1.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>

	<script src="<?php echo base_url('resource/assets/js/jspdf.debug.js'); ?>"></script>
	<script src="<?php echo base_url('resource/assets/js/jspdf.min.js'); ?>"></script>
	<script type="text/javascript">
		var doc = new jsPDF();
		var specialElementHandlers = {
			'#editor': function (element, renderer) {
				return true;
			}
		};

		$('#cmd').click(function (event) {
			event.preventDefault();
			doc.fromHTML($('#content').text(), 15, 15, {
				'width': 992,
					'elementHandlers': specialElementHandlers
			});
			doc.save('sample-file.pdf');
		});
	</script>
</body>
</html>