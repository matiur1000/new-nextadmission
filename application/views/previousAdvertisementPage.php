<div class="previous_banner">
 <div class="container">
 <nav class="navbar navbar bg-primary navbar-static-top">
 <div class="col-md-6">
	<div class="input-group" style="padding-top:8px;">
	  <input type="text" class="form-control serchvalue" placeholder="Search for...">
	  <input type="hidden"  class="orgId" value="<?php echo $organizeId; ?>">
	  <span class="input-group-btn">
		<button class="btn btn-default" type="button" id="orgserch"><i class="fa fa-search" aria-hidden="true"></i></button>
	  </span>
	</div>
 </div>
 
 <div class="col-md-4" align="center">
	<div style="font-size:20px; padding-top:10px;">Previous Advertisement</div>
 </div>
  <style>

		table td, table th{
			text-align: center;
		}

    </style>
</nav>


  <table class="table table-striped table-bordered table-hover table-condensed">
	<thead>
	  <tr>
		<th width="3%">SN</th>
		<th width="10%">Start Date</th>
		<th width="10%">End Date</th>
		<th width="30%" style="text-align:center">Title</th>
		<th width="31%" style="text-align:center">Details</th>
		<th width="8%" style="text-align:center">Picture</th>
		<th width="8%" style="text-align:center">Video</th>
	  </tr>
	</thead>
	<tbody id="serchresult">
	<?php 
	$i = 1;
	$organizeId  = $organizeId;
	foreach($addvertingorg as $v)
	{
		$detailss = htmlspecialchars_decode($v->description, ENT_QUOTES);
		$addverimage = $v->add_image;
		$video_embed = $v->video_embed;
		
		$detailssub   = substr($detailss, 0,120);
		$adId 		  = $v->id;
		
		$deletail = array('home', 'orgidBydetail', $id);
		
		$alldeletail = array('home', 'allorgidBydetail', $id);
		
		$add_details		 = array('home','orgWiseAdvritismentView', $adId, $organizeId);	
	
	?> 
	
	  <tr>
		<td><?php echo $i++; ?></td>
		<td><?php echo $v->publish_date; ?></td>
		<td><?php echo $v->expire_date; ?></td>
		<td style="text-align:center"><a href="<?php echo base_url($add_details); ?>"><?php echo $v->title; ?></a></td>
		<td style="text-align:center">
	<a href="<?php echo base_url($add_details); ?>" data-toggle="tooltip" data-placement="top" title="Advertisement details"><?php echo $detailssub; ?></a>
		</td>
		<td style="text-align:center; padding:0px;">
		<?php if(!empty($addverimage)){?> 
		<img src="<?php echo base_url("Images/Add_image/".$addverimage); ?>" style="max-height:35px;">
		<?php } else {?> 
		<img src="<?php echo base_url("Images/Add_image/purplelist-image-not-available.png"); ?>" style="max-height:30px;">
		<?php }?>
		</td>
		
		<td style="text-align:center">
	<?php if(!empty($video_embed)) {?>
	<div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $recentPhotoInfo->video_embed; ?>?rel=0&amp;showinfo=0" allowfullscreen></iframe>
</div>
 <?php } else {?> 
		<img src="<?php echo base_url("Images/Add_image/NotAvailable.gif"); ?>" style="max-height:35px;">
	<?php } ?>
		</td>
		
		
		
	  </tr>
	 <?php }?>
	</tbody>
  </table>
  
</div>
</div>


<script>
$("#orgserch").on('click', function(){
	var serchvalue = $(".serchvalue").val();
	var orgId      = $(".orgId").val();
	console.log(serchvalue);
	var serchURL = "<?php echo site_url("home/orgIdyDataserch"); ?>";
	
	$.ajax(
		{
			url : serchURL,
			type: "POST",
			data : {serchvalue : serchvalue, orgId : orgId},
			success:function(data){
				$("#serchresult").html(data);
			}
			
		});
	
	
});

</script>	
	