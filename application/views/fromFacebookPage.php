<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta property="og:url"  content="<?php echo site_url('home/fromFacebook/'.$UserPostInfo->id) ?>" />
	<meta property="og:type" content="Blog" />
    <meta property="og:title" content="<?php echo $UserPostInfo->title ?>"/>
    <meta property="og:description" content="<?php //echo $UserPostInfo->description ?>"/>
    <meta property="og:image" content="<?php echo base_url("Images/Post_image/$UserPostInfo->image"); ?>"/>
    <meta property="og:image:width" content="600 " /> 
	<meta property="og:image:height" content="315" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}

		.commentStyle {
	border-left:1px;
	border-right:1px;
	border-top:1px;
    border-style:solid;
	border-color:#CCCCCC;
	border-width:100%;
	padding:5px;
	
}

.commentStyle:last-child {
    border-bottom:1px solid #CCCCCC;	
} 

.commentStyle h5{
	font-size: 14px;
	font-weight: normal;
	margin-top: 5px;
	margin-bottom: 0px;
}
.commentStyle p{
	font-size: 14px;
	font-weight: normal;
	padding-bottom:10px;
	float:left;
}

.commentStyle img{
	background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    display: inline-block;
    line-height: 1.42857;
    margin-right: 10px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}

#share-buttons img {
width: 35px;
padding: 5px;
border: 0;
box-shadow: 0;
display: inline;
}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row" style="height:5px;"></div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				
			<div class="row">  <!--row Start-->
			   	<div class="col-lg-9">  <!--col 9 Start-->
					<div class="col-lg-12" style="padding:30px; border:1px solid #CCCCCC; border-radius: 5px;">
					  <img src="<?php echo base_url("Images/Post_image/$UserPostInfo->image"); ?>" class="pull-left" style="margin:0 15px 0 0; padding-bottom:10px" width="780" height="400" />
		              <p style="text-align:left; font-size:20px; padding:0 0 10px 0"><?php echo $UserPostInfo->title ?></p>
		              <p style="text-align:justify"><?php echo $UserPostInfo->description ?></p>	

			               <div class="row" style="padding-top:30px">
						    <div id="comntView" class="col-lg-12">
							    <?php 
								 	
							        if(!empty($UserCmntInfo)){
									 foreach ($UserCmntInfo as $v){
									   $comment_id    = $v->id;
									   $post_id    = $v->post_id;
									   $multiId		 = array('commentPopup','replyStore', $post_id,$comment_id);
									   
									  
									   
								  ?>
							     <div class="col-md-12 commentStyle">
							        <?php if($v->image){ ?>
							           <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
				                    <?php }else{ ?>
							           <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
				                    <?php } ?>
									<h5><a><?php echo $v->name; ?></a></h5>
									<h6><?php echo $v->comment_date; ?></h6>	
									<p style="float:left"><?php echo $v->comment; ?></p> 
							     </div>
							  
							  <?php } } ?>
							   
							  
				           </div>
				            <div class="col-md-12" style="margin-top:20px;">
							<?php if( isActiveUser() ) { ?>
							   <form id="comntParent" action="<?php echo site_url('commentPopup/commentStore'); ?>" method="post" enctype="multipart/form-data">
							      <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id; ?>">
						           <div class="form-group">
								    <label for="exampleInputEmail1">Write Your Comment</label>
									<textarea name="comment" id="comment" class="form-control" rows="3"></textarea>
								  </div>
							   	</form>
							   <?php } else { ?>
				                <h4 style="color:#FF0000">Please Login To Comment.</h4>
								
				            <?php } ?>
							  
				           </div>
			           </div>
					</div>

					


		        </div>  <!--col 9 end--> 
				     
				<?php $this->load->view('rightSidebarPage'); ?>                 
				<!--col 3 end--> 
		  	</div>
          </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	
	<script>

	   $(document).ready(function() {
	      $("body").on("contextmenu",function(){
	         return false;
	      }); 
	   }); 

	   $('body').bind('copy paste cut drag drop', function (e) {
   			e.preventDefault();
		});

		//North America Effict
		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(document).on("keyup", "#comment", function(e){ 
		
			var parents  = $(this).parents('#comntParent');
			var comment  = $(this).val();
			var postId   = parents.find('#post_id').val();
			var formURL  = parents.attr("action");
			console.log(postId);
			console.log(comment);
			console.log(formURL);

		    if (e.which == 13 && ! e.shiftKey) {
				$.ajax(
				{
					url : formURL,
					type: "POST",
					data : {postId : postId, comment : comment},
					success:function(data){
						$("#comntView").html(data);				
						$("#comntParent input[type='text'], textarea").val("");
					}
				});
			  }
				
				e.preventDefault();
			});

	   

		/*$("#comment").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#comntView").html(data);				
					$("#comment input[type='text'], textarea").val("");
				}
			});
			
			e.preventDefault();
		});*/

		
	</script>
	
    
  </body>
</html>