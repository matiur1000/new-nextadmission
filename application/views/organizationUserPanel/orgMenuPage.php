<script src="<?php echo base_url('resource/source/script.js'); ?>"></script>
<link href="<?php echo base_url('resource/source/styles.css'); ?>" rel="stylesheet">
<div id='cssmenu'>
	<ul>
	   <?php 
          foreach($panelMenuInfo as $v){
          	if($v->id == 8){
		       $menuLink  = array('organizationUserHome','index');
		     }else if($v->id == 9){
		       $menuLink  = array('organizationUserHome','addPost');
		     }else if($v->id == 10){
		       $menuLink  = array('organizationUserHome','programeManage');
		     }else if($v->id == 11){
		       $menuLink  = array('organizationUserHome','addManageFinal');
             }else if($v->id == 12){
		       $menuLink  = array('organizationUserHome','totalVisitor');
             }else if($v->id == 14){
		       $menuLink  = array('home','adWiseCompanyDetail', $userAutoId);
			   
		     } else {
		       $menuLink  = array('organizationUserHome','newsEventManage');
		   }
	   ?>
		
		<li><a href="<?php if($v->id == 13){ echo "#"; }else{ echo site_url($menuLink); } ?>"><?php echo $v->menu_name ?></a>
		   <ul>
		      <?php 
		          foreach($v->sub as $sv){
		           if($sv->id == 2){
			          $subMenuLink  = array('organizationUserHome','organizeProfile');
				     }else if($sv->id == 3){
				       $subMenuLink  = array('organizationUserHome','organizeAbout');
				     }else if($sv->id == 4){
				       $subMenuLink  = array('organizationUserHome','organizeContact');
				     }else if($sv->id == 5){
				       $subMenuLink  = array('organizationUserHome','organizeSlide');
				     }else if($sv->id == 6){
				       $subMenuLink  = array('organizationUserHome','organizeGallery');
				     }else if($sv->id == 7){
				       $subMenuLink  = array('organizationUserHome','organizeVideoGallery');
		             }
					 else if($sv->id == 9){
				       $subMenuLink  = array('organizationUserHome','organizeaboutindia');
		             }
					 else if($sv->id == 10){
				       $subMenuLink  = array('organizationUserHome','organizeCoursefree');
		             }
					 else if($sv->id == 11){
				       $subMenuLink  = array('organizationUserHome','organizeligiblity');
		             }
					 else if($sv->id == 12){
				       $subMenuLink  = array('organizationUserHome','organizehostelandordercost');
		             }
					 
					  else if($sv->id == 13){
				       $subMenuLink  = array('organizationUserHome','organizeScholarship');
		             }
					  else if($sv->id == 14){
				       $subMenuLink  = array('organizationUserHome','travelitinerary');
		             }
					 else if($sv->id == 15){
				       $subMenuLink  = array('organizationUserHome','howtoapply');
		             }
					 else if($sv->id == 16){
				       $subMenuLink  = array('organizationUserHome','whentoapply');
		             }
					  else if($sv->id == 17){
				       $subMenuLink  = array('organizationUserHome','applyforVisa');
		             }
					  else if($sv->id == 18){
				       $subMenuLink  = array('organizationUserHome','applyonline');
		             }
					 
					 else{
				       $subMenuLink  = array('organizationUserHome','gifImageCreate');
		             }
					 
			   ?>
					<?php if($sv->status == 'Active'){?> 
					 <li><a href="<?php echo site_url($subMenuLink);?>"><?php echo $sv->sub_menu_name ?></a></li>
					 <?php }else {?> 
					 <li><a href="#"><?php echo $sv->sub_menu_name ?></a></li>
					 <?php }?>
				 
		      <?php } ?>
			
			</ul>
		</li>

        <?php } ?>


		<!-- <li><a href="<?php echo site_url('organizationUserHome/index');?>">Organization Home</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/addPost');?>">Blog</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/programeManage');?>">Program</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/addManageFinal');?>">Promotion</a></li>
		<li><a href="<?php echo site_url('organizationUserHome/totalVisitor');?>">Total Visitor</a></li>
		<li><a href="#">Oorganization Profile</a>
		   <ul>
		      <li><a href="<?php echo site_url('organizationUserHome/organizeProfile');?>">Organize Home</a></li>
		      <li><a href="<?php echo site_url('organizationUserHome/organizeAbout');?>">About Us</a></li>
			  <li><a href="<?php echo site_url('organizationUserHome/organizeContact');?>">Contact Us</a></li>
			  <li><a href="<?php echo site_url('organizationUserHome/organizeSlide');?>">Latest News</a></li>
			  <li><a href="<?php echo site_url('organizationUserHome/organizeGallery');?>">Photo Gallery</a></li>
			 </ul>
		</li>
		<li><a target="_blank" href="<?php echo site_url('home/adWiseCompanyDetail/'.$userAutoId);?>">Organization Website</a></li> -->
	</ul>
</div>