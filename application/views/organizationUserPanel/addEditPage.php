<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
               <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				body{
				  background-color:#EEEEEE;
				
				}
				
				</style>
	
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		 <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		 <script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
		 <script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script language="javascript" type="text/javascript">
			tinyMCE.init({
				mode : "exact",
				elements : "ajaxfilemanager",
				theme : "advanced",
				// TO GET THE FULL IMAGE URL
				relative_urls: false,
	    		remove_script_host: false,
				/****************/
				/*setup : function(ed) {
				      ed.onKeyUp.add(function(ed, l) {
				         tinyMCE.triggerSave();	                    
				      });
				},*/
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
				theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",

				theme_advanced_buttons1_add_before : "newdocument,separator",
				theme_advanced_buttons1_add : "fontselect,fontsizeselect",
				theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
				theme_advanced_buttons2_add_before: "cut,copy,separator,",
				theme_advanced_buttons3_add_before : "",
				theme_advanced_buttons3_add : "media",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				extended_valid_elements : "hr[class|width|size|noshade]",
				file_browser_callback : "ajaxfilemanager",
				paste_use_dialog : false,
				theme_advanced_resizing : true,
				theme_advanced_resize_horizontal : true,
				apply_source_formatting : true,
				force_br_newlines : true,
				force_p_newlines : false,	
			});

			function ajaxfilemanager(field_name, url, type, win) {
				var ajaxfilemanagerurl = SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php");
				switch (type) {
					case "image":
						break;
					case "media":
						break;
					case "flash": 
						break;
					case "file":
						break;
					default:
						return false;
				}
	            tinyMCE.activeEditor.windowManager.open({
	                url: SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php"),
	                width: 482,
	                height: 440,
	                inline : "yes",
	                close_previous : "no"
	            },{
	                window : win,
	                input : field_name
	            });
	            
			}
		</script>
    
  </head>
  <body>
        <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					   
							 
							  <div class="row">
						
									
							 <?php $this->load->view('organizationUserPanel/middleAddEditPage'); ?>
                             
						   </div>
						 
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
   <script src="<?php echo base_url("resource/js/ajaxupload.3.5.js"); ?>"></script>
  <script>
		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('organizationUserHome/addDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});

		$('.date-picker').datepicker({
			autoclose: true	  
		});	




	resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
		
		
		// Positon Wise Serial No
		$("#select_website").change(function(){
		  var selectValue = $("#select_website").val();	
		  
		  if(selectValue == 'country'){
		    $("#selectRegion").css("display", "block"); 
		  } else {
		     $("#selectRegion").css("display", "none"); 
		  }

          $("#adSubmit").css("display", "none"); 
          $("#searchHide").css("display", "block"); 

		
	  });

		$("#publish_date, #expire_date ").change(function(){
          $("#adSubmit").css("display", "none"); 
          $("#searchHide").css("display", "block"); 

		
	  });


		// Positon Wise Serial No
		$("#positon").change(function() {
		var positon = $("#positon").val();	
		$("#adSubmit").css("display", "none"); 
        $("#searchHide").css("display", "block"); 

	
		$.ajax({
			url : SAWEB.getSiteAction('organizationUserHome/homePageSerialNo'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : positon },
			dataType : "html",
			success : function(data) {			
				$("#serial_no").html(data);
			}
		  });
		
	  });


		 $("#next").click(function(e)
	{
			
	 var postData 	= $("#inputTable input, select, textarea").serializeArray();
	 var searchuRL  = "<?php echo site_url('organizationUserHome/adBookingChk'); ?>";

		console.log(postData);
		$.ajax(
		{
			url : searchuRL,
			type: "POST",
			data : postData,
			success:function(data){
				 if(data==1){
					$(".bookMsg").text("Sorry! the ad is booked");
					$(".bookMsg").css("color", "red");
				} else {
					$("#searchHide").css("display", "none");
					$("#adSubmit").css("display", "block");
					var position = $("#positon").val();
					var serial_no = $("#serial_no").val();
					console.log(position)
					
					if(position == 'Top'){
                       $(".imageSize").text("(Size:255px X 90px)");
					} else if(position == 'Left'){
                       $(".imageSize").text("(Size:185px X 120px)");
					} else if(position == 'Right'){
						if((serial_no =='9') || (serial_no =='10') || (serial_no =='15') || (serial_no =='16')){
						  $(".imageSize").text("(Size:258px X 50px)");	
						} else{
						  $(".imageSize").text("(Size:185px X 120px)");	
						}
					}
					 
				}
					
					
					
		  	}


		});

		e.preventDefault();
		
	});	


	
		
 </script>
 
    
  </body>
</html>