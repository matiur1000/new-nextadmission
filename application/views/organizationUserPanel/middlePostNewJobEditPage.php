<!--CALENDER-->
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-1.7.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-1.8.16.custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-timepicker-addon.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('resource/js/jquery-ui-sliderAccess.js'); ?>"></script>
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('resource/css/jquery-ui-1.8.16.custom.css'); ?>" />
	<script type="text/javascript">
		
		$(function(){
			
	
			$('.example-container > pre').each(function(i){
				eval($(this).text());
			});
		});
		
	</script>
	<!--CALENDER-->
    
    
	<div class="row" style="margin-left:-5px;">
   <form id="form1" name="form1" method="post" action="<?php echo site_url('organizationUserHome/jobPostUpdateAction');?>">
   <input type="hidden" name="id" id="id" value="<?php echo $jobPostEditInfo->id; ?>" />
      <div class="col-lg-12 col-sm-12"><div style="float:left">Post New Job Ads</div><br/>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover table-bordered" style="margin-top:10px;">
  <tr>
    <td width="34%">Ad Type <span style="color:#F00; font-size:16px;">* </span></td>
    <td width="66%">
      <select name="add_type" id="add_type" style="width:200px; height:30px;">
    <option <?php if($jobPostEditInfo->add_type == 'Basic Listing'){?> selected="selected" <?php } ?> value="Basic Listing">Basic Listing</option>
    <option <?php if($jobPostEditInfo->add_type == 'Stand Listing'){?> selected="selected" <?php } ?>  value="Stand Listing">Stand Listing </option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Job Category <span style="color:#F00; font-size:16px;">* </span> </td>
    <td><select name="job_category" id="job_category" style="width:200px; height:30px;">
    <option <?php if($jobPostEditInfo->job_category == 'Education provider'){?> selected="selected" <?php } ?> value="Education provider">Education provider</option>
    <option <?php if($jobPostEditInfo->job_category == 'Agency'){?> selected="selected" <?php } ?> value="Agency">Agency </option>
    <option <?php if($jobPostEditInfo->job_category == 'Couching Centre'){?> selected="selected" <?php } ?> value="Couching Centre">Couching Centre</option>
    <option <?php if($jobPostEditInfo->job_category == 'professional Training Centre'){?> selected="selected" <?php } ?> value="professional Training Centre">professional Training Centre</option>
    <option <?php if($jobPostEditInfo->job_category == 'Other'){?> selected="selected" <?php } ?> value="Other">Other</option>
    </select></td>
  </tr>
  <tr>
    <td>Organization Type <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="organization_type" id="organization_type" style="width:200px; height:35px;" value="<?php echo $jobPostEditInfo->organization_type; ?>"/></td>
  </tr>
  <tr>
    <td>Job Title <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="title" id="title" style="width:70%; height:35px;" value="<?php echo $jobPostEditInfo->title; ?>" required /></td>
  </tr>
  <tr>
    <td>No. of Vacancies <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="vacancies" id="vacancies" style="width:200px; height:35px;" value="<?php echo $jobPostEditInfo->vacancies; ?>" /></td>
  </tr>
  <tr>
    <td>How do you want to receive CV / Resume(s) ? <span style="color:#F00; font-size:16px;">* </span></td>
    <td>(User must select at least one from the given options)</td>
  </tr>
  <tr>
    <td><input type="checkbox" name="receive_online" id="receive_online" value="online" <?php if($jobPostEditInfo->receive_online == 'online' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
      <label for="checkbox"></label>
      Online CV/Resume</td>
    <td>[Bdjobs CV format; CV will be available in your corporate inbox.] </td>
  </tr>
  <tr>
    <td><input type="checkbox" name="receive_email" id="receive_email" value="email" <?php if($jobPostEditInfo->receive_email == 'email' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
      <label for="checkbox2"></label>
      Email Attachment </td>
    <td>[Applicants can send their CV as an attachment in your given Email address.] </td>
  </tr>
  <tr>
    <td><input type="checkbox" name="receive_hardcopy" id="receive_hardcopy" value="hardcopy" <?php if($jobPostEditInfo->receive_hardcopy == 'hardcopy' ){ ?> checked="checked" checkbox="checked" <?php } ?>/>
      <label for="checkbox3"></label>
      Hard Copy CV</td>
    <td>[Applicants can send hard copy of their CV in your corporate address.] </td>
  </tr>
  <tr>
    <td>Applicant should enclose Photograph with CV ? </td>
    <td style="font-weight:lighter; font-size:11px;"><p>
      <label>
        <input type="radio" name="cv_enclose" value="yes" id="cv_enclose" <?php if($jobPostEditInfo->cv_enclose == 'yes' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Yes</label>
      
      <label>
        <input type="radio" name="cv_enclose" value="no" id="cv_enclose" <?php if($jobPostEditInfo->cv_enclose == 'no' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        No</label></p></td>
  </tr>
  <tr>
    <td>Application Deadline <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="apllication_deadline" id="apllication_deadline" style="width:70%; height:35px;" 
	onClick="('#apllication_deadline').datepicker({dateFormat: 'dd/mm/yy'});" value="<?php echo $jobPostEditInfo->apllication_deadline; ?>"  required /></td>
  </tr>
  <tr>
    <td>Billing Contact <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="contact_person" id="contact_person" style="width:200px; height:35px;" value="<?php echo $jobPostEditInfo->contact_person; ?>" /></option>
    </select></td>
  </tr>
  <tr>
    <td>Designation <span style="color:#F00; font-size:16px;">* </span></td>
    <td><input type="text" name="designation" id="designation" style="width:200px; height:35px;" value="<?php echo $jobPostEditInfo->designation; ?>" /></td>
  </tr>
  <tr>
    <td>Do you want to display Organization Name ?</td>
    <td style="font-weight:lighter; font-size:11px;"><p>
      <label>
        <input type="radio" name="desplay_orgname" value="yes" id="desplay_orgname" <?php if($jobPostEditInfo->desplay_orgname == 'yes' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Yes</label>
      
      <label>
        <input type="radio" name="desplay_orgname" value="no" id="desplay_orgname" <?php if($jobPostEditInfo->desplay_orgname == 'no' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        No</label>
    
    </p></td>
  </tr>
  <tr>
    <td>Do you want to hide Organization Address ? </td>
    <td style="font-weight:lighter; font-size:11px;"><p>
      <label>
        <input type="radio" name="desplay_address" value="yes" id="desplay_address" <?php if($jobPostEditInfo->desplay_address == 'yes' ){ ?> checked="checked" checkbox="checked" <?php } ?> /> 
        Yes</label>
      
      <label>
        <input type="radio" name="desplay_address" value="no" id="desplay_address" <?php if($jobPostEditInfo->desplay_address == 'no' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        No</label>
    
    </p></td>
  </tr>
  
  <tr>
    <td width="34%">Age Range</td>
    <td width="66%">Form
    <select name="age_range_from" id="age_range_from">
    <option <?php if($jobPostEditInfo->age_range_from == '18'){?> selected="selected" <?php } ?> value="18">18</option>
    <option <?php if($jobPostEditInfo->age_range_from == '19'){?> selected="selected" <?php } ?> value="19">19 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '20'){?> selected="selected" <?php } ?> value="20">20 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '21'){?> selected="selected" <?php } ?> value="21">21 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '22'){?> selected="selected" <?php } ?> value="22">22 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '23'){?> selected="selected" <?php } ?> value="23">23 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '24'){?> selected="selected" <?php } ?> value="24">24 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '25'){?> selected="selected" <?php } ?> value="25">25 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '26'){?> selected="selected" <?php } ?> value="26">26 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '27'){?> selected="selected" <?php } ?> value="27">27 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '28'){?> selected="selected" <?php } ?> value="28">28 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '29'){?> selected="selected" <?php } ?> value="29">29 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '30'){?> selected="selected" <?php } ?> value="30">30</option>
    <option <?php if($jobPostEditInfo->age_range_from == '31'){?> selected="selected" <?php } ?> value="31">31</option>
    <option <?php if($jobPostEditInfo->age_range_from == '32'){?> selected="selected" <?php } ?> value="32">32 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '33'){?> selected="selected" <?php } ?> value="33">33</option>
    <option <?php if($jobPostEditInfo->age_range_from == '34'){?> selected="selected" <?php } ?> value="34">34</option>
    <option <?php if($jobPostEditInfo->age_range_from == '35'){?> selected="selected" <?php } ?> value="35">35 </option>
    <option <?php if($jobPostEditInfo->age_range_from == '36'){?> selected="selected" <?php } ?> value="36">36</option>
    </select>
    
    To
    <select name="age_range_to" id="age_range_to">
    <option <?php if($jobPostEditInfo->age_range_to == '18'){?> selected="selected" <?php } ?> value="18">18</option>
    <option <?php if($jobPostEditInfo->age_range_to == '19'){?> selected="selected" <?php } ?> value="19">19 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '20'){?> selected="selected" <?php } ?> value="20">20 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '21'){?> selected="selected" <?php } ?> value="21">21 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '22'){?> selected="selected" <?php } ?> value="22">22 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '23'){?> selected="selected" <?php } ?> value="23">23 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '24'){?> selected="selected" <?php } ?> value="24">24 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '25'){?> selected="selected" <?php } ?> value="25">25 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '26'){?> selected="selected" <?php } ?> value="26">26 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '27'){?> selected="selected" <?php } ?> value="27">27 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '28'){?> selected="selected" <?php } ?> value="28">28 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '29'){?> selected="selected" <?php } ?> value="29">29 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '30'){?> selected="selected" <?php } ?> value="30">30</option>
    <option <?php if($jobPostEditInfo->age_range_to == '31'){?> selected="selected" <?php } ?> value="31">31</option>
    <option <?php if($jobPostEditInfo->age_range_to == '32'){?> selected="selected" <?php } ?> value="32">32 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '33'){?> selected="selected" <?php } ?> value="33">33</option>
    <option <?php if($jobPostEditInfo->age_range_to == '34'){?> selected="selected" <?php } ?> value="34">34</option>
    <option <?php if($jobPostEditInfo->age_range_to == '35'){?> selected="selected" <?php } ?> value="35">35 </option>
    <option <?php if($jobPostEditInfo->age_range_to == '36'){?> selected="selected" <?php } ?> value="36">36</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Gender </td>
    <td style="font-weight:lighter; font-size:11px;" >
     
        <label>
          <input type="radio" name="gender" value="male" id="gender" <?php if($jobPostEditInfo->gender == 'male' ){ ?> checked="checked" checkbox="checked" <?php } ?>/>
          Male Only</label>
       
        <label>
          <input type="radio" name="gender" value="female" id="gender" <?php if($jobPostEditInfo->gender == 'female' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
          Female Only</label>
       
        <label>
          <input type="radio" name="gender" value="any" id="gender" <?php if($jobPostEditInfo->gender == 'any' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
          Any </label></td>
  </tr>
  <tr>
    <td>Job Type </td>
    <td style="font-weight:lighter; font-size:11px;"><label>
          <input type="radio" name="job_type" value="Full Time" id="job_type" <?php if($jobPostEditInfo->job_type == 'Full Time' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
          Full Time</label>
       
        <label>
          <input type="radio" name="job_type" value="Part Time" id="job_type" <?php if($jobPostEditInfo->job_type == 'Part Time' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
          Part Time</label>
       
        <label>
          <input type="radio" name="job_type" value="Contract" id="job_type" <?php if($jobPostEditInfo->job_type == 'Contract' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
          Contract  </label></td>
  </tr>
  <tr>
    <td>Job Level <span style="color:#F00; font-size:16px;">* </span></td>
    <td style="font-weight:lighter; font-size:11px;">
    <input type="radio" name="job_level" id="job_level"  value="Entry Level Job" <?php if($jobPostEditInfo->job_level == 'Entry Level Job' ){ ?> checked="checked" checkbox="checked" <?php } ?>/>
      <label>Entry Level Job </label>
      <label>
        <input name="job_level" id="job_level" value="Mid/Managerial Level Job" type="radio" <?php if($jobPostEditInfo->job_level == 'Mid/Managerial Level Job' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
      </label>
      <label for="checkbox">Mid/Managerial Level Job 
        <input name="job_level" id="job_level" value="Top Level Job" type="radio" <?php if($jobPostEditInfo->job_level == 'Top Level Job' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Top Level Job </label></td>
  </tr>
  <tr>
    <td>Educational Qualification <span style="color:#F00; font-size:16px;">* </span></td>
    <td><textarea name="education_qualification" id="education_qualification" cols="45" rows="5" style="width:70%; height:100px;" ><?php echo $jobPostEditInfo->education_qualification; ?></textarea></td>
  </tr>
  <tr>
    <td>Job Description/Responsibility <span style="color:#F00; font-size:16px;">* </span></td>
    <td><textarea name="job_description" id="job_description" cols="45" rows="5" style="width:70%; height:100px;" ><?php echo $jobPostEditInfo->job_description; ?></textarea></td>
  </tr>
  <tr>
    <td>
      Additional Job Requirements<span style="color:#F00; font-size:16px;"> *</span></td>
    <td><textarea name="job_requirements" id="job_requirements" cols="45" rows="5" style="width:70%; height:100px;" ><?php echo $jobPostEditInfo->job_requirements; ?></textarea></td>
  </tr>
  <tr>
    <td><label for="checkbox2"></label></td>
    <td style="font-weight:lighter; font-size:11px;">  <label>
        <input type="radio" name="job_experience" value="Experience Required" id="job_experience" <?php if($jobPostEditInfo->job_experience == 'Experience Required' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Experience Required </label>
      <label>
        <input type="radio" name="job_experience" value="No Experience Required" id="job_experience" <?php if($jobPostEditInfo->job_experience == 'No Experience Required' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        No Experience Required </label></td>
  </tr>
  <tr>
    <td><label for="checkbox3"></label>
      <span id="spnExperienceYear">Total Years of Experience</span></td>
    <td>Min
    <select name="exprience_min" id="exprience_min">
    <option <?php if($jobPostEditInfo->exprience_min == '1'){?> selected="selected" <?php } ?>  value="1">1</option>
    <option <?php if($jobPostEditInfo->exprience_min == '2'){?> selected="selected" <?php } ?>  value="2">2 </option>
    <option <?php if($jobPostEditInfo->exprience_min == '3'){?> selected="selected" <?php } ?>  value="3">3 </option>
    <option <?php if($jobPostEditInfo->exprience_min == '4'){?> selected="selected" <?php } ?>  value="4">4 </option>
    <option <?php if($jobPostEditInfo->exprience_min == '5'){?> selected="selected" <?php } ?>  value="5">5 </option>
    <option <?php if($jobPostEditInfo->exprience_min == '6'){?> selected="selected" <?php } ?>  value="6">6</option>
    </select>
    
   Max
    <select name="exprience_max" id="exprience_max">
   <option <?php if($jobPostEditInfo->exprience_max == '1'){?> selected="selected" <?php } ?>  value="1">1</option>
    <option <?php if($jobPostEditInfo->exprience_max == '2'){?> selected="selected" <?php } ?>  value="2">2 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '3'){?> selected="selected" <?php } ?>  value="3">3 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '4'){?> selected="selected" <?php } ?>  value="4">4 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '5'){?> selected="selected" <?php } ?>  value="5">5 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '6'){?> selected="selected" <?php } ?>  value="6">6</option>
    <option <?php if($jobPostEditInfo->exprience_max == '7'){?> selected="selected" <?php } ?> value="7">7 </option>
	<option <?php if($jobPostEditInfo->exprience_max == '8'){?> selected="selected" <?php } ?> value="8">8 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '9'){?> selected="selected" <?php } ?> value="9">9 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '10'){?> selected="selected" <?php } ?> value="10">10 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '11'){?> selected="selected" <?php } ?> value="11">11 </option>
    <option <?php if($jobPostEditInfo->exprience_max == '12'){?> selected="selected" <?php } ?> value="12">12 </option>
    </select></td>
  </tr>
 
  
  <tr>
    <td>Compensation and Other Benefits </td>
    <td style="font-weight:lighter; font-size:11px;"><p><strong><u>Salary Range </u></strong></p>
      <p>
        <label>
          <input type="radio" name="sallary_range" value="Negotiable" id="sallary_range" <?php if($jobPostEditInfo->sallary_range == 'Negotiable' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Ready to publish</labe />
          Negotiable </label>
        <br />
        <label>
          <input type="radio" name="sallary_range" value="Not Mention" id="sallary_range" <?php if($jobPostEditInfo->sallary_range == 'Not Mention' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Ready to publish</labe />
          Don't want to mention </label>
        <br />
        <label>
          <input type="radio" name="sallary_range" value="Desplay" id="sallary_range" <?php if($jobPostEditInfo->sallary_range == 'Desplay' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Ready to publish</labe />
          Want to display the following range </label>
       
      </p>
      <div style="display:none;">
       Minimum <input type="text" name="sallary_money_min" id="sallary_money_min" style="width:20%; height:35px; margin-top:5px;" />
       Maximum <input type="text" name="sallary_money_max" id="sallary_money_max" style="width:20%; height:35px;" /> Monthly in BDT
      </div>
      </td>
  </tr>
  <tr>
    <td>Job Publish Status </td>
    <td style="font-weight:lighter; font-size:11px;"><label>
        <input type="radio" name="job_publish_status" value="Publish" id="job_publish_status" <?php if($jobPostEditInfo->job_publish_status == 'Publish' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Ready to publish</label>
      <label><input type="radio" name="job_publish_status" value="Publish later" id="job_publish_status" <?php if($jobPostEditInfo->job_publish_status == 'Publish later' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
        Publish  later (drafted jobs)</label></td>
  </tr>
  
  <tr>
    <td></td>
    <td>&nbsp;</td>
  </tr>
  </table>

<button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Update</button>

   </div>
   </form>
      
     </div>
	 <script type="text/javascript">
		$('#apllication_deadline').datepicker({dateFormat: 'dd/mm/yy'});
	</script>
   

