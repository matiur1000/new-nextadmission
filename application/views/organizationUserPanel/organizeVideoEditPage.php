<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
               <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				body{
				  background-color:#EEEEEE;
				
				}

				#myFrame { width:200px; height:150px; }
				
				</style>
	
         <?php $this->load->view('jsLinkPage'); ?>  
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
    
  </head>
  <body>
        <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
							  <div class="welll welll-lg">
   					             <div class="row">
							        <div class="col-lg-9" align="center" style="font-size:18px;"><?php echo $updateText; ?></div>
                                    <div class="col-lg-3" align="right">
		                               <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
		                                  <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a>
		                                </div>
		                            </div>
								  </div>
									<div class="row">&nbsp;</div>
									<div class="row">
									   <div class="col-lg-12" align="center">
										 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Organization Video Edit</span></div>
									</div>  
									<div class="row">&nbsp;</div>
									<div class="row">
									  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
									  </div>
									</div>
								<div class="row">&nbsp;</div>   
								<div class="row">
								  <div class="col-lg-11">
									<form action="<?php echo site_url('organizationUserHome/orgVideoStore'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
									<input type="hidden" name="edit_id" id="edit_id" value="<?php echo $orgVideoEditInfo->id ?>">
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Select video type</label>
										<div class="col-sm-7">
										   <select name="video_type" id="video_type" class="form-control">
										     <option selected="selected">Select video type</option>
										     <option <?php if($orgVideoEditInfo->video_type == 'embeded'){ ?> selected="selected" <?php } ?> value="embeded">Embeded code</option>
										     <option <?php if($orgVideoEditInfo->video_type == 'video'){ ?> selected="selected" <?php } ?> value="video">Video</option>
										   </select>
										</div>
									  </div>

									  <?php if(!empty($orgVideoEditInfo->video)){ ?>
									  <div id="upvideo" class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Upload video</label>
										<div class="col-sm-7">
										  <input type="file" id="video" name="video" tabindex="8" />
										</div>
									  </div>
                                      <?php } ?>

                                      <?php if(!empty($orgVideoEditInfo->video_embede_code)){ ?>
									  <div id="embeded" class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Embeded code</label>
										<div class="col-sm-7">
										<textarea class="form-control" name="video_embede_code" id="video_embede_code" cols="60" rows="" placeholder="Organization Address"><?php echo $orgVideoEditInfo->video_embede_code; ?></textarea>
										</div>
									  </div>

									  <?php } ?>
									  
									  <div class="form-group">
										<div class="col-sm-offset-3 col-sm-10">
										  <button type="submit" class="btn btn-primary">Update</button>
										</div>
									  </div>
									</form>
									</div>
									<div class="col-lg-1"></div>
								         
								</div>
        							<div class="row">&nbsp;</div>
									<div class="row">&nbsp;</div>
                               <div class="row">
                               <div class="col-lg-12 table-responsive" style="padding:0px;">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                  <tr class="active" style="font-weight:bold;">
                                    <td width="8%">Sl No</td>
                                    <td width="50%">Video type</td>
                                    <td width="30%">Video</td>
                                    <td width="12%" align="center" valign="middle">Action</td>
                                  </tr>
                                  <?php 
                                    //print_r($studentId);
                                    $i = 1;
                                    foreach($organizeVideoInfo as $v){
                                        $id		= $v->id;
                                        $pieces = explode(" ", $v->video_embede_code);
										$pieces[3]; 
										
                                  ?>
                                  <tr> 
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->video_type; ?></td>
                                    <td>
                                     <?php if(!empty($v->video)){ ?>
                                       <video width="200" height="150" controls>
											<source src="<?php echo base_url("/resource/video_upload/$v->video"); ?>" type="video/mp4">
										</video> 
                                        <?php }else { ?>
                                        <iframe width="200" height="150" <?php echo $pieces[3]; ?> frameborder="0" allowfullscreen></iframe>
                                        <?php } ?>
                                    </td>
                                    <td align="center">
                                        <a href="<?php echo site_url('organizationUserHome/organizeVideoEdit/'. $id); ?>" name="Edit">
                                        Edit
                                        </a>
										||
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            Delete
                                        </a>
                                    </td>
                                  </tr> <?php } ?>
                                </table> 
                               </div>
                             </div>
							</div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>  
			    
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script>
        /*$('#video_type').on('change', function() {
			var value = $(this).val();
			if(value =='embeded'){
				$('#embeded').css("display", "block");
				$('#upvideo').css("display", "none");
			} else {
			   $('#embeded').css("display", "none");
			   $('#upvideo').css("display", "block");	
			}
		});*/

		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('organizationUserHome/organizeVideoDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});
	</script>

	<script>
	resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
 </script>
    
  </body>
</html>