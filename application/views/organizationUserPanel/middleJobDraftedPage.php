
   <div class="row" style="padding-left:10px;">
  
	  <div class="col-lg-2 col-sm-1 comntorg3 active" style="height:30px; padding-top:5px;"><a href="<?php echo site_url('organizationUserHome/index'); ?>">Latest Job Ad( <?php echo $totalLatestJob; ?>)</a></div>
      <div class="col-lg-2 col-sm-1 comntorg3" style="height:30px; margin-left:3px; padding-top:5px; background:#FFF"><a href="<?php echo site_url('organizationUserHome/draftedJob'); ?>">Drafted Job ( <?php echo $totalLatestDraft; ?> )</a></div>
      <div class="col-lg-2 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px;"><a href="<?php echo site_url('organizationUserHome/ArchivedJob'); ?>">Archived Job ( <?php echo $totalLatestAchived; ?> )</a></div>
     
   </div>
  
   <div class="row comntorg4" style="margin-left:-5px;">
     
	  
      <table width="100%" cellspacing="0" cellpadding="0" class="table">
	   <tr>
			<td width="4%" height="33" style="font-size:15px">Sl</td>
			<td width="30%" style="font-size:15px">Job Title</td>
			<td width="10%" style="font-size:15px">Applications</td>
			<td width="9%" style="font-size:15px">Viewed</td>
			<td width="11%" style="font-size:15px">Not Viewed</td>
			<td width="10%" style="font-size:15px">Shortlist</td>
			<td width="12%" style="font-size:15px">Star candidates</td>
			<td width="14%" align="center" style="font-size:15px">Action</td>
	    </tr>
		  <?php 
		   $i = 1;
		  // print_r($OrganizePostInfo);
		   foreach($draftPostInfo as $v){
		   $id     = $v->id;
		   $jobLink    		= array('organizationUserHome','jobWiseDetails', $id);
		   $viewJob    		= array('organizationUserHome','viewJob', $id);
		   $cvViewLink    	= array('organizationUserHome','cvViewDetails', $id);
		   $cvNotViewLink   = array('organizationUserHome','cvNotViewDetails', $id);
		   $shortListLink   = array('organizationUserHome','shortListDetails', $id);
		   $starCandLink    = array('organizationUserHome','starCandidateDetails', $id);
		  
		  ?>
		  <tr>
			<td height="34" style="color:#0066FF;"><?php echo $i++; ?></td>
			<td><a href="<?php echo site_url($viewJob); ?>"><?php echo $v->title; ?></a></td>
			<td><a style="text-decoration:underline" href="<?php echo site_url($jobLink); ?>"><?php echo $v->totalApplicant; ?></a></td>
			<td><a style="text-decoration:underline" href="<?php echo site_url($cvViewLink); ?>"><?php echo $v->viewed; ?></a></td>
			<td><a style="text-decoration:underline" href="<?php echo site_url($cvNotViewLink); ?>"><?php echo $v->notviewed; ?></a></td>
			<td><a style="text-decoration:underline" href="<?php echo site_url($shortListLink); ?>"><?php echo $v->sortlist; ?></a></td>
			<td><a style="text-decoration:underline" href="<?php echo site_url($starCandLink); ?>"><?php echo $v->star; ?></a></td>
			<td align="center" valign="middle" style="font-weight:bold; color:#3CF">
            <a href="<?php echo site_url('organizationUserHome/jobPostEdit/'. $id); ?>"><i class="glyphicon glyphicon-pencil" title="Edit"></i></a>&nbsp;
            <a class="red" href="#" data-id="<?php echo $id ?>"><i style="color:#F00" class="glyphicon glyphicon-trash" title="Delete"></i></a>&nbsp; 
            <a class="archived" href="#" data-id="<?php echo $id ?>"><i style="color:#FC3" class="glyphicon glyphicon-floppy-disk" title="Archived"></i></a></td>
		  </tr>
		  <?php } ?>
	   </table>
	  
	  

      <div class="col-lg-12 col-sm-12" style="padding-left:15px; height:50px; padding-top:15px; text-align:end;">
      </div>
</div>
   </div>
   <script>
			
				
				$('.archived').on('click', function() {
					var x = confirm('Are you sure to Archived?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('organizationUserHome/jobPostArchived/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
				
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to Delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('organizationUserHome/jobPostDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
 </script>

