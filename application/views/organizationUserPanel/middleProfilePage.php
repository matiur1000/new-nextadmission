<?php 
   $contactDownLoadLink = array('organizationUserHome','downloadCsvFile', "contact");  
   $typeDownLoadLink 	= array('organizationUserHome', 'downloadCsvFile', "organizeType");  
?>

<div class="col-lg-12">
    <div class="row">
	  <div class="col-lg-7" align="center"></div>

      <div class="col-lg-5" align="center">
      <div class="comntorg2 link-stylejob" style="color:#FFF; padding-top:5px;">
	  <a href="<?php echo site_url('organizationUserHome/PostNewCV'); ?>">Search CV Bank</a></div>
      <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
      <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a></div></div>
		</div></div>
		</div> 
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Update Profile Information </span> 
	   </div>
	</div>  
	<div class="row text-center" style="color:green"><?php if(!empty($suceess)){ echo $suceess; } ?></div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		<div class="row">
		 <form action="<?php echo site_url('organizationUserHome/update'); ?>" method="post" enctype="multipart/form-data">
		 <input type="hidden" name="id" id="id" value="<?php echo $profileEditInfo->id; ?>" />
			<div class="col-lg-12">
			  <div class="col-lg-2">
			  	  <div class="form-group">
					<input type="text" class="form-control" id="organizationname" name="organizationname" 
					placeholder="Organization Name" tabindex="1" value="<?php echo $profileEditInfo->name; ?>">
				</div>
			  </div>
			  <div class="col-lg-4">
			     <div class="row">
                  <div class="col-lg-4" style="margin-top:5px; font-size:15px; padding:0">Company Logo</div>
                  <div class="col-lg-8" style="padding-left:0px;">
                      <div class="form-group">
                        <input type="file" id="image" name="image" tabindex="8" />
                      </div>
                	</div>
                </div>
			  	
			  </div>
			  <div class="col-lg-2">
			  	  <div class="form-group">
					<input type="text" class="form-control" id="user_id" name="user_id" placeholder="Email" tabindex="1" 
					value="<?php echo $profileEditInfo->user_id; ?>"><?php echo form_error('user_id'); ?>
				</div>
			  </div>
			  <div class="col-lg-2">
			  	  <div class="form-group">
					<input type="text" class="form-control" id="account_name" name="account_name" 
					placeholder="Account Name" tabindex="1" value="<?php echo $profileEditInfo->account_name; ?>">
				</div>
			  </div>
			  <div class="col-lg-2">
			  	  <div class="form-group">
					<input type="password" class="form-control password" id="password" name="password" placeholder="Password" tabindex="2" 
					value="<?php echo $profileEditInfo->password; ?>"><?php echo form_error('password'); ?><span class="first"></span>
				</div>
			  </div>
             <div class="row">&nbsp;</div> 
             <div class="row">&nbsp;</div> 
				
				<div class="row">
				  <div class="col-lg-2" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; height: 47px;">
					 <span style="color:#333333; font-size:20px; padding-left:12px">Contact Details</span>
				  </div>
				  <div class="col-lg-1" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0;">
					<span style="color:#333333; font-size:20px;">
					   <a href='<?php echo site_url($contactDownLoadLink);?>'><img  data-popover="true" data-html=true  data-content="Comming soon...." src="<?php echo base_url("resource/img/csv.png"); ?>" width="40" height="40" align="" /></a></span>
				  </div>
				  <div class="col-lg-6" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; height: 47px; padding-left:0">
					 <div class="row">
                  		<div class="col-lg-5 text-right" style="margin-top:5px; font-size:15px;"><strong>Upload contact csv file :</strong></div>
		                 <div class="col-lg-7" style="padding-left:0px;">
		                    <div class="form-group">
		                        <input type="file" id="contactCsv" name="contactCsv" tabindex="8" />
		                    </div>
		                </div>
		              </div>
				  </div>
				  <div class="col-lg-3" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; height: 47px; padding-left:0"></div>
				</div>
				<div class="row">&nbsp;</div>
			
			<div class="col-lg-3">
                <div class="form-group">
					<div>
						<select class="form-control" id="country_id" name="country_id"  tabindex="1" value="<?php echo set_value('country_id'); ?>">
							<option value="" selected>Select Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option <?php if($profileEditInfo->country_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
					   </select>
					 
					</div>
				</div>
                
                

				<div class="form-group">
					<div>
					    <select class="form-control" id="district_id" name="district_id"  tabindex="1" >
							<option value="" selected>Select Distict Name</option>
					    </select>
					</div>
				</div>

				<div class="form-group">
                        <input type="text" class="form-control" id="post_code" name="post_code" placeholder="Post code" tabindex="2" value="<?php echo $moreEditInfo->post_code; ?>"> 
                    </div>

				

                <div class="form-group">
					   <textarea class="form-control" name="address" id="address" cols="60" rows="" placeholder="Organization Address" style="height:85px"><?php echo $moreEditInfo->address; ?></textarea>
				</div>
                
                
			   	<div class="form-group">
					<input type="text" class="form-control" id="email_com" name="email_com" placeholder="Email" tabindex="1" value="<?php echo $moreEditInfo->email_com; ?>"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="contact_persons" name="contact_persons" placeholder="Contact Persons Name" tabindex="1" value="<?php echo $profileEditInfo->account_name; ?>"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" tabindex="2" value="<?php echo $moreEditInfo->phone; ?>"> 
				</div>

				<div class="form-group">
					<input type="text" class="form-control" id="email" name="email" placeholder="Email" tabindex="1" value="<?php echo $moreEditInfo->email; ?>"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="web_site" name="web_site" placeholder="Web Url" tabindex="2" value="<?php echo $moreEditInfo->web_site; ?>">
				</div>
				
            </div>

           <div class="col-lg-3">
                <div class="form-group">
					<div>
					    <select class="form-control" id="city_id" name="city_id"  tabindex="1" >
							<option value="" selected>Select State Name</option>
							<?php foreach ($cityInfo as $v){?>
							<option <?php if($profileEditInfo->city_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->city_name; ?></option>
							<?php } ?>
					   </select>
					</div>
				</div>
                <div class="form-group">
					<div>
					    <select class="form-control" id="police_station" name="police_station"  tabindex="1" >
							<option value="" selected>Select Police Station  </option>
					   </select>
					</div>
				</div>

				<div class="form-group">
                    <input type="text" class="form-control" id="phone_com2" name="phone_com2" placeholder="Phone" tabindex="2" value="<?php echo $moreEditInfo->phone_com; ?>">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" id="mobile_com2" name="mobile_com2" placeholder="Mobile" tabindex="2" value="<?php echo $moreEditInfo->mobile_com; ?>">
                </div>
				<div class="form-group">
					<div>
					    <select class="form-control" id="founded" name="founded"  tabindex="1" >
							<option value="" selected>Select Year</option>
							<?php foreach($allYearInfo as $v){ ?>
							<option <?php if($moreEditInfo->founded == $v->year){ ?> selected="selected" <?php } ?> value="<?php echo $v->year ?>"><?php echo $v->year ?></option>
							<?php } ?>
					    </select>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control" id="designation" name="designation" placeholder="Designation" tabindex="1" value="<?php echo $moreEditInfo->designation; ?>"> 
				</div>

				<div class="form-group">
					<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" tabindex="2" value="<?php echo $moreEditInfo->mobile; ?>">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="skype" name="skype" placeholder="Skype" tabindex="2" value="<?php echo $moreEditInfo->skype; ?>"> 
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook" tabindex="1" value="<?php echo $moreEditInfo->facebook; ?>"> 
				</div>

				<div class="form-group">
					<input type="text" class="form-control" id="tuiter" name="tuiter" placeholder="Tuiter" tabindex="1" value="<?php echo $moreEditInfo->tuiter; ?>">
				</div>
           </div> 


          <div class="col-lg-6">
          	    <div class="row">
                  <div class="col-lg-5" style="margin-top:5px; font-size:15px;">Any More Branches?</div>
                  <div class="col-lg-7" style="padding-left:0px;">
                      <div class="form-group">
                           <select class="form-control" id="any_more_campus" name="any_more_campus"  tabindex="2" onchange="nextSteap();">
                                <option value="" selected>Select Yes/No</option>
                                <option <?php if($moreEditInfo->any_more_campus == 'Yes'){?> selected="selected" <?php } ?> value="Yes">Yes</option>
                                <option <?php if($moreEditInfo->any_more_campus == 'No'){?> selected="selected" <?php } ?> value="No">No</option>
                           </select>
					      </div>
                	</div>
                </div>
          </div>

            <div id="more" style="display:none;">

		        <div class="col-lg-3">
		           <div class="form-group">
						<div>
						   <select class="form-control" id="country_id2" name="country_id2"  tabindex="1" value="<?php echo set_value('country_id'); ?>">
								<option value="" selected>Select Country Name</option>
								<?php foreach ($countryInfo as $v){?>
								<option <?php if($anyMoreEditInfo->country == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
								<?php } ?>
						   </select>
						 
						</div>
				    </div>

				    <div class="form-group">
						<div>
						    <select class="form-control" id="district_id2" name="district_id2"  tabindex="1" >
								<option value="" selected>Select Distict Name</option>
						    </select>
						</div>
					</div>
                    
                    <div class="form-group">
					   <textarea class="form-control" name="address2" id="address2" cols="60" rows="" placeholder="Organization Address" style="height:85px"><?php echo $anyMoreEditInfo->address; ?></textarea>
				    </div>
				    <div class="form-group">
					    <input type="text" class="form-control" id="email_com2" name="email_com2" placeholder="Email" tabindex="1" value="<?php echo $anyMoreEditInfo->email_com; ?>">
				    </div>
		        </div>


		        <div class="col-lg-3">
                    <div class="form-group">
						<div>
						   <select class="form-control" id="city_id2" name="city_id2"  tabindex="1" >
								<option value="" selected>Select State Name</option>
								<?php foreach ($cityInfo as $v){?>
								<option <?php if($anyMoreEditInfo->city == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->city_name; ?></option>
								<?php } ?>
						   </select>
						</div>
					</div>

					<div class="form-group">
						<div>
						    <select class="form-control" id="police_station2" name="police_station2"  tabindex="1" >
								<option value="" selected>Select Police Station  </option>
						   </select>
						</div>
					</div>

					<div class="form-group">
                        <input type="text" class="form-control" id="phone_com1" name="phone_com1" placeholder="Phone" tabindex="2" value="<?php echo $anyMoreEditInfo->phone_com; ?>"> 
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="mobile_com1" name="mobile_com1" placeholder="Mobile" tabindex="2" value="<?php echo $anyMoreEditInfo->mobile_com; ?>">
                    </div>

                    <div class="form-group">
						<div>
						    <select class="form-control" id="founded2" name="founded2"  tabindex="1" >
								<option value="" selected>Select Year</option>
								<?php foreach($allYearInfo as $v){ ?>
								<option <?php if($anyMoreEditInfo->founded == $v->year){ ?> selected="selected" <?php } ?> value="<?php echo $v->year ?>"><?php echo $v->year ?></option>
								<?php } ?>
						    </select>
						</div>
					</div>
		        </div>
	        </div>

	        <div class="col-lg-6">
	            <div class="row">
				  <div class="col-lg-5"><span style="color:#333333; font-size:20px;">Organization Type</span></div>
				   <div class="col-lg-2" style="padding-bottom:5px"><a href='<?php echo site_url($typeDownLoadLink);?>'><img  data-popover="true" data-html=true  data-content="Comming soon...." src="<?php echo base_url("resource/img/csv.png"); ?>" width="40" height="40" align="" /></a></div>
				    <div class="col-lg-5">
				     <div class="row">
	          		 <div class="col-lg-4 text-left" style="margin-top:5px; font-size:15px; padding-left:0"><strong>Upload :</strong></div>
	                   <div class="col-lg-8 text-left" style="padding-left:0px;">
	                    <div class="form-group">
	                        <input type="file" id="organize_type_csv" name="organize_type_csv" tabindex="8" />
	                    </div>
	                </div>
	               </div>
	              </div>
				</div>
			</div>

			<div class="col-lg-3">
			  
				<div class="form-group">
					<div>
					   <select class="form-control" id="type_of_organization" name="type_of_organization"  tabindex="2">
							<option value="" selected>Select Type Of Organization</option>
							<option <?php if($moreEditInfo->type_of_organization == 'EducationProvider'){?> selected="selected" <?php } ?>  value="EducationProvider">Education provider </option>
							<option <?php if($moreEditInfo->type_of_organization == 'Agency'){?> selected="selected" <?php } ?> value="Agency">Agency</option>
							<option <?php if($moreEditInfo->type_of_organization == 'CouchingCentre'){?> selected="selected" <?php } ?> value="CouchingCentre">Couching Centre</option>
							<option <?php if($moreEditInfo->type_of_organization == 'ProfessionalTrainingCentre'){?> selected="selected" <?php } ?> value="ProfessionalTrainingCentre">Professional Training Centre </option>
							<option <?php if($moreEditInfo->type_of_organization == 'Other'){?> selected="selected" <?php } ?> value="Other">Other</option>
					   </select>
					</div>
				</div>

				<div class="form-group">
					<div>
					   <input type="text" class="form-control" id="type_of_organization_other" name="type_of_organization_other" placeholder="Type Of Organition" style="display:none" value="<?php echo $moreEditInfo->type_of_organization_other; ?>">
					</div>
				</div>

				<div class="form-group">
					<div>
					   <select class="form-control" id="type_of_education_provider" name="type_of_education_provider"  tabindex="2">
							<option value="" selected>Select Type of Education provider </option>
							<option <?php if($moreEditInfo->type_of_education_provider == 'School /College upto  12'){?> selected="selected" <?php } ?>  value="School /College upto  12">School /College upto  12</option>
							<option <?php if($moreEditInfo->type_of_education_provider == 'University'){?> selected="selected" <?php } ?>  value="University">University</option>
					   </select>
					</div>
				</div>

				<div class="form-group">
					<div>
					   <select class="form-control" id="school_college" name="school_college"  tabindex="2">
							<option value="" selected>Select School /College</option>
							<option <?php if($moreEditInfo->school_college == 'KG-8 years Class'){?> selected="selected" <?php } ?>  value="KG-8 years Class">KG-8 years Class</option>
							<option <?php if($moreEditInfo->school_college == 'K-10 years Class'){?> selected="selected" <?php } ?>  value="K-10 years Class">K-10 years Class</option>
							<option <?php if($moreEditInfo->school_college == '9-10 years Class'){?> selected="selected" <?php } ?>  value="9-10 years Class">9-10 years Class</option>
							<option <?php if($moreEditInfo->school_college == '10-12 years Class'){?> selected="selected" <?php } ?>  value="10-12 years Class">10-12 years Class</option>
							<option <?php if($moreEditInfo->school_college == 'Bachelor'){?> selected="selected" <?php } ?>  value="Bachelor">Bachelor</option>
							<option <?php if($moreEditInfo->school_college == 'Masters'){?> selected="selected" <?php } ?>  value="Masters">Masters</option>
					   </select>
					</div>
				</div>


				<div class="form-group">
					<div>
					   <select class="form-control" id="agency_type" name="agency_type"  tabindex="2">
							<option value="" selected>Select Agency Type</option>
							<option <?php if($moreEditInfo->agency_type == 'Travel Agent ( Air Ticket)'){?> selected="selected" <?php } ?> value="Travel Agent ( Air Ticket)">Travel Agent ( Air Ticket)</option>
							<option <?php if($moreEditInfo->agency_type == 'Education Tour Agent'){?> selected="selected" <?php } ?> value="Education Tour Agent">Education Tour Agent</option>
							<option <?php if($moreEditInfo->agency_type == 'Package Tour Agent'){?> selected="selected" <?php } ?> value="Package Tour Agent">Package Tour Agent</option>
							<option <?php if($moreEditInfo->agency_type == 'Visa process'){?> selected="selected" <?php } ?> value="Visa process">Visa process</option>
							<option <?php if($moreEditInfo->agency_type == 'Hotel Booking'){?> selected="selected" <?php } ?> value="Hotel Booking">General user</option>
							<option <?php if($moreEditInfo->agency_type == 'Job Agent'){?> selected="selected" <?php } ?> value="Job Agent">Job Agent</option>
							<option <?php if($moreEditInfo->agency_type == 'Other'){?> selected="selected" <?php } ?> value="Other">Other</option>
					   </select>
					</div>
				</div>

				<div class="form-group">
					<div>
					   <input type="text" class="form-control" id="agency_type_other" name="agency_type_other" placeholder="Type Of Agency" style="display:none" value="<?php echo $moreEditInfo->agency_type_other; ?>">
					</div>
				</div>

				<div class="form-group">
					<div>
					   <select class="form-control" id="couching_centre" name="couching_centre"  tabindex="2">
							<option value="" selected>Select Type Of Couching Centre</option>
							<option <?php if($moreEditInfo->couching_centre == 'College Admission'){?> selected="selected" <?php } ?> value="College Admission">College Admission </option>
							<option <?php if($moreEditInfo->couching_centre == 'Medical Admission'){?> selected="selected" <?php } ?> value="Medical Admission">Medical Admission</option>
							<option <?php if($moreEditInfo->couching_centre == 'University Admission'){?> selected="selected" <?php } ?> value="University Admission">University Admission</option>
							<option <?php if($moreEditInfo->couching_centre == 'English Training : IELTS-GRE-GMET etc'){?> selected="selected" <?php } ?> value="English Training : IELTS-GRE-GMET etc">English Training : IELTS-GRE-GMET etc</option>
							<option <?php if($moreEditInfo->couching_centre == 'Foreign Language : Germany-France-Courier etc'){?> selected="selected" <?php } ?> value="Foreign Language : Germany-France-Courier etc">Foreign Language : Germany-France-Courier etc</option>
							<option <?php if($moreEditInfo->couching_centre == 'Professional'){?> selected="selected" <?php } ?> value="Professional">Professional</option>
							<option <?php if($moreEditInfo->couching_centre == 'Tutor Services'){?> selected="selected" <?php } ?> value="Tutor Services">Tutor Services</option>
					   </select>
					</div>
				</div>


				<div class="form-group">
					<div>
					   <select class="form-control" id="professional_taining" name="professional_taining"  tabindex="2">
							<option value="" selected>Select Type Of Professional Training </option>
							<option <?php if($moreEditInfo->professional_taining == 'Government Job'){?> selected="selected" <?php } ?> value="Government Job">Government Job</option>
							<option <?php if($moreEditInfo->professional_taining == 'BCS Training'){?> selected="selected" <?php } ?> value="BCS Training">BCS Training</option>
							<option <?php if($moreEditInfo->professional_taining == 'Marine Training'){?> selected="selected" <?php } ?> value="Marine Training">Marine Training</option>
							<option <?php if($moreEditInfo->professional_taining == 'Defence Training'){?> selected="selected" <?php } ?> value="Defence Training">Defence Training</option>
					   </select>
					</div>
				</div>




			   
			</div>
		    <div class="col-lg-3">

		        <div class="form-group">
					<div>
					   <select class="form-control" id="type_of_ownership" name="type_of_ownership"  tabindex="2">
							<option value="" selected>Select Type Of Ownership</option>
							<option <?php if($moreEditInfo->type_of_ownership == 'Government'){?> selected="selected" <?php } ?> value="Government">Government</option>
							<option <?php if($moreEditInfo->type_of_ownership == 'Private'){?> selected="selected" <?php } ?> value="Private">Private</option>
					   </select>
					</div>
				</div>
               <div class="form-group">
                   <select class="form-control" id="scholarship" name="scholarship"  tabindex="2">
                        <option value="" selected>Select Type Of Scholarship </option>
                        <option <?php if($moreEditInfo->scholarship == 'Full Scholarship'){?> selected="selected" <?php } ?> value="Full Scholarship">Full Scholarship</option>
                        <option <?php if($moreEditInfo->scholarship == 'Half Scholarship'){?> selected="selected" <?php } ?> value="Half Scholarship">Half Scholarship</option>
                        <option <?php if($moreEditInfo->scholarship == 'Tuition fees waver'){?> selected="selected" <?php } ?> value="Tuition fees waver">Tuition fees waver</option>
                        <option <?php if($moreEditInfo->scholarship == 'Monthly Scholarship'){?> selected="selected" <?php } ?> value="Monthly Scholarship">Monthly Scholarship</option>
                        <option <?php if($moreEditInfo->scholarship == 'Year Scholarship'){?> selected="selected" <?php } ?> value="Year Scholarship">Year Scholarship</option>
                   </select>
				</div>
                <div class="form-group">
                   <select class="form-control" id="accademic_exam_system" name="accademic_exam_system"  tabindex="2">
                        <option value="" selected>Select Type Of Academic Exam system  </option>
                        <option <?php if($moreEditInfo->accademic_exam_system == 'Quarter Exam'){?> selected="selected" <?php } ?> value="Quarter Exam">Quarter Exam</option>
                        <option <?php if($moreEditInfo->accademic_exam_system == 'Half Year semester'){?> selected="selected" <?php } ?> value="Half Year semester">Half Year semester</option>
                        <option <?php if($moreEditInfo->accademic_exam_system == 'Yearly semester'){?> selected="selected" <?php } ?> value="Yearly semester">Yearly semester</option>
                   </select>
				</div>
		    </div>

			
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			
            <div class="row">&nbsp;</div>
            <div class="col-lg-6">

            </div>
			
			
			
			
		    </div> 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	                
</div>

