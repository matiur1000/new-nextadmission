<div class="col-lg-12">
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row" style="padding-left:12px; padding-bottom:10px; font-size:20px">Account Information </div>   
		<div class="row">
		   <div class="col-lg-12" style="padding:10px;">
			 <form action="<?php echo site_url('organizationUserHome/updatePass'); ?>" method="post" enctype="multipart/form-data">
			  <input type="hidden" name="id" id="id" value="<?php echo $profileEditInfo->id; ?>" />
			     
				 <table width="100%" cellspacing="3" cellpadding="3" style="border:1px solid #CCCCCC;" class="table">
					  <tr class="active"> 
						<td width="28%" align="left" valign="middle" style="font-size:15px;">User Name </td>
						<td width="30%" style="font-size:15px;"><?php echo $profileEditInfo->user_id; ?></td>
						<td width="42%">&nbsp;</td>
					  </tr>
					  <tr>
						<td align="left" valign="middle" style="font-size:15px;">Old Password <span style="color:#F00; font-size:16px;">* </span></td>
						<td><input type="password" class="form-control" id="password" placeholder="Old Password" name="password" tabindex="5"><span class="second"></span></td>
						<td style="font-size:15px">[Maximum 8 Characters.]</td>
					  </tr>
					  <tr class="active">
						<td align="left" valign="middle" style="font-size:15px;">New  Password <span style="color:#F00; font-size:16px;">* </span></td>
						<td><input type="password" class="form-control newPassword" id="newPassword" placeholder="New Password" name="newPassword" tabindex="5"><span class="first"></span></td>
						<td style="font-size:15px">[Maximum 8 Characters.]</td>
					  </tr>
					  <tr>
						<td align="left" valign="middle" style="font-size:15px;">Confirm New Password <span style="color:#F00; font-size:16px;">* </span></td>
						<td><input type="password" class="form-control conPassword" id="conpass" placeholder="Confirm Password" name="conpass" tabindex="5"><span class="second"></span></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr class="active">
						<td>&nbsp;</td>
						<td><button type="submit" id="submit" name="submit" class="btn btn-info center-block up" tabindex="1">Update Password</button></td>
						<td>&nbsp;</td>
					  </tr>
					  
			   </table>

		     </form>  
		   </div>      
		</div>
	               
</div>

<script>

  $("#password").blur (function(){
  	 var password = $(this).val();
 	
		$.ajax(
			{
				url :"<?php echo site_url('organizationUserHome/userOldPassChk'); ?>",
				type: "POST",
				data : {password : password},
				success:function(data){
				
					if(data==1)
					{
						$(".second").text("Your Old Password Is Incorrect");
						$(".second").css("color", "red");
						//$('.up button[type="submit"]').attr("disabled", "disabled");
					}  else {
						$(".second").text("");
						$(".second").css("");
						//$('.up button[type="submit"]').removeAttr("disabled", "disabled");
					}
				  }
			
			});
		
	  
					
					
	});
					

</script>
