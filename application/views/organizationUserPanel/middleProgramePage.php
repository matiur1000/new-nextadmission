    <?php 
       $programeDownLoadLink = array('organizationUserHome','downloadCsvFile', "program");  
    ?>

    <div class="col-lg-12" align="right">
	<div class="welll welll-lg">
      <div class="row">
        <div class="col-lg-9" align="center" style="font-size:18px;"><?php echo $updateText; ?></div>
        <div class="col-lg-3" align="right">
           <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
              <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a>
            </div>
        </div>
	  </div>
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Add Program</span></div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>  
	<form action="<?php echo site_url('organizationUserHome/programCsvStore'); ?>" method="post" enctype="multipart/form-data">
	    <div class="row">
		  <div class="col-lg-3" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; height: 47px;">
			 <span class="pull-right" style="color:#333333; font-size:20px; padding-left:12px">Programe csv file </span>
		  </div>
		  <div class="col-lg-1" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0;">
			<span style="color:#333333; font-size:20px;">
			   <a href='<?php echo site_url($programeDownLoadLink);?>'><img  data-popover="true" data-html=true  data-content="Comming soon...." src="<?php echo base_url("resource/img/csv.png"); ?>" width="40" height="40" align="" /></a></span>
		  </div>
		  <div class="col-lg-5" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; height: 47px; padding-left:0">
			 <div class="row">
          		<div class="col-lg-6 text-right" style="margin-top:5px; font-size:15px;"><strong>Upload programe csv file :</strong></div>
                 <div class="col-lg-6" style="padding:0;">
                    <div class="form-group pull-left">
                        <input type="file" id="programCsv" name="programCsv" tabindex="8" />
                    </div>
                </div>
              </div>
		  </div>
		  <div class="col-lg-2 text-left" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; height: 47px; padding:0 100px 0 0"><button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Submit</button></div>
		</div> 
    </form>

		<!-- <div class="row">
		 <form action="<?php echo site_url('organizationUserHome/programStore'); ?>" method="post" enctype="multipart/form-data">
			<div class="col-lg-12">
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Program Name</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				    <div class="form-group">
						<input type="text" class="form-control" id="programe_name" name="programe_name" placeholder="Programe Name"
						 tabindex="13">
					  </div> 
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Duration</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				    <div class="form-group">
						<input type="text" class="form-control" id="course_duration" name="course_duration" placeholder="Duration"
						 tabindex="13" >
					  </div> 
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Program Details</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
						<textarea class="form-control" name="programe_details" id="ajaxfilemanager" cols="60" rows="10" placeholder="Programe Details"></textarea>
					  </div>
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   
						 
		   </div>  
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Submit</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div> -->
        <div class="row">&nbsp;</div>
        <div class="row">
       <div class="col-lg-12 table-responsive" style="padding:0px;">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
          <tr class="active" style="font-weight:bold">
            <td width="6%">Sl No</td>
            <td width="66%">Program Name </td>
            <td width="28%" align="center">Action</td>
          </tr>
          <?php 
            //print_r($studentId);
            $i = 1;
            foreach($UserWiseProgInfo as $v){
                $id		= $v->id;
          ?>
          <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $v->programe_name; ?></td>
            <td align="center">
                <a href="<?php echo site_url('organizationUserHome/programeEdit/'. $id); ?>" name="Edit">
                   Edit                                        </a>
				||
                <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                Delete                                        </a>                                    </td>
          </tr> <?php } ?>
        </table> 
       </div>
     </div>
	</div>                
</div>

