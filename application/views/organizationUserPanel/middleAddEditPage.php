    <div class="col-lg-12" align="right">
	<div class="welll welll-lg">
    <div class="row">
        <div class="col-lg-9" align="center" style="font-size:18px;"><?php echo $updateText; ?></div>
        <div class="col-lg-3" align="right">
           <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
              <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a>
            </div>
        </div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Ad 	Edit</span></div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		<div id="inputTable" class="row">
		
		   <form action="<?php echo site_url('organizationUserHome/addUpdate/'.$addEditInfo->id); ?>" method="post" enctype="multipart/form-data">
			<div class="row" style="padding-bottom:10px">
				  <div class="form-group">
					<label for="select_website" class="col-sm-3 control-label" style="margin-top:5px">Select Website</label>
					<div class="col-sm-6">
					   <select class="form-control" id="select_website" name="select_website"  tabindex="1"  required>
						<option value="" selected>Select Website</option>
						<option <?php if($addEditInfo->website == 'main'){ ?> selected ="selected" <?php } ?> value="main">Main Website</option>
						<option <?php if($addEditInfo->website == 'country'){ ?> selected ="selected" <?php } ?>  value="country">Country Website</option>
					</select>
					</div>
				  </div>
				</div>

				 
            <?php if($addEditInfo->website == 'country'){ ?>
				<div class="row" style="padding-bottom:10px"> 
				  <div id="selectRegion" class="form-group">
					<label for="region" class="col-sm-3 control-label" style="margin-top:5px">Select Country</label>
					<div class="col-sm-6">
					   <select class="form-control" id="country_name" name="country_name"  tabindex="2">
						<option value="" selected>Select Country</option>
						  <?php 
								foreach($allCountry as $v){
					  
							 ?>
							  <option <?php if($addEditInfo->country_name == $v->country_name){ ?> selected ="selected" <?php } ?> value="<?php echo $v->country_name; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
						</select>
					</div>
				  </div>
				</div>
		    <?php } ?>
				  
				<div class="row" style="padding-bottom:10px"> 
				  <div class="form-group">
					<label for="positon" class="col-sm-3 control-label" style="margin-top:5px">Select Position</label>
					<div class="col-sm-6">
					   <select class="form-control" id="positon" name="positon"  tabindex="3" required>
							<option value="" selected>Select Position</option>
							 <option <?php if($addEditInfo->positon == 'top'){ ?> selected ="selected" <?php } ?> value="top">Top</option>
							 <option <?php if($addEditInfo->positon == 'left'){ ?> selected ="selected" <?php } ?> value="left">Left</option>
							 <option <?php if($addEditInfo->positon == 'right'){ ?> selected ="selected" <?php } ?> value="right">Right</option>
					   </select>
					</div>
				  </div>
				</div>
				  
				  
				<div class="row" style="padding-bottom:10px"> 
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label" style="margin-top:5px">Select Serial</label>
					<div class="col-sm-6">
					   <select class="form-control" id="serial_no" name="serial_no"  tabindex="4" required>
							<option value="" selected>Select Serial</option>
						<?php if($addEditInfo->positon == 'Top'){ ?> 
							<option <?php if($addEditInfo->serial_no == '1'){ ?> selected ="selected" <?php } ?> value="1">1</option>
							<option <?php if($addEditInfo->serial_no == '2'){ ?> selected ="selected" <?php } ?> value="2">2</option>
                        <?php }else if($addEditInfo->positon == 'Left'){ ?> 
							<option <?php if($addEditInfo->serial_no == '1'){ ?> selected ="selected" <?php } ?> value="1">1</option>
							<option <?php if($addEditInfo->serial_no == '2'){ ?> selected ="selected" <?php } ?> value="2">2</option>
							<option <?php if($addEditInfo->serial_no == '3'){ ?> selected ="selected" <?php } ?> value="3">3</option>
							<option <?php if($addEditInfo->serial_no == '4'){ ?> selected ="selected" <?php } ?> value="4">4</option>
							<option <?php if($addEditInfo->serial_no == '5'){ ?> selected ="selected" <?php } ?> value="5">5</option>
							<option <?php if($addEditInfo->serial_no == '6'){ ?> selected ="selected" <?php } ?> value="6">6</option>
                        <?php }else{ ?> 
							<option <?php if($addEditInfo->serial_no == '1'){ ?> selected ="selected" <?php } ?> value="1">1</option>
							<option <?php if($addEditInfo->serial_no == '2'){ ?> selected ="selected" <?php } ?> value="2">2</option>
							<option <?php if($addEditInfo->serial_no == '3'){ ?> selected ="selected" <?php } ?> value="3">3</option>
							<option <?php if($addEditInfo->serial_no == '4'){ ?> selected ="selected" <?php } ?> value="4">4</option>
							<option <?php if($addEditInfo->serial_no == '5'){ ?> selected ="selected" <?php } ?> value="5">5</option>
							<option <?php if($addEditInfo->serial_no == '6'){ ?> selected ="selected" <?php } ?> value="6">6</option>
							<option <?php if($addEditInfo->serial_no == '7'){ ?> selected ="selected" <?php } ?> value="7">7</option>
							<option <?php if($addEditInfo->serial_no == '8'){ ?> selected ="selected" <?php } ?> value="8">8</option>		
							<option <?php if($addEditInfo->serial_no == '9'){ ?> selected ="selected" <?php } ?> value="9">9</option>	
							<option <?php if($addEditInfo->serial_no == '10'){ ?> selected ="selected" <?php } ?> value="10">10</option>	
							<option <?php if($addEditInfo->serial_no == '11'){ ?> selected ="selected" <?php } ?> value="11">11</option>	
							<option <?php if($addEditInfo->serial_no == '12'){ ?> selected ="selected" <?php } ?> value="12">12</option>
							<option <?php if($addEditInfo->serial_no == '13'){ ?> selected ="selected" <?php } ?> value="13">13</option>
							<option <?php if($addEditInfo->serial_no == '14'){ ?> selected ="selected" <?php } ?> value="14">14</option>	
							<option <?php if($addEditInfo->serial_no == '15'){ ?> selected ="selected" <?php } ?> value="15">15</option>
							<option <?php if($addEditInfo->serial_no == '16'){ ?> selected ="selected" <?php } ?> value="16">16</option>	
							<option <?php if($addEditInfo->serial_no == '17'){ ?> selected ="selected" <?php } ?> value="17">17</option>
							<option <?php if($addEditInfo->serial_no == '18'){ ?> selected ="selected" <?php } ?> value="18">18</option>
							<option <?php if($addEditInfo->serial_no == '19'){ ?> selected ="selected" <?php } ?> value="19">19</option>
							<option <?php if($addEditInfo->serial_no == '20'){ ?> selected ="selected" <?php } ?> value="20">20</option>		
                         <?php } ?> 
					   </select>
					</div>
				  </div>
				</div>
				  
				  
                <div class="row" style="padding-bottom:10px"> 
				 <div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label" style="margin-top:5px">Select Date</label>
					<div class="col-sm-6">
					   <div class="input-group">
				      <input type="text" class="form-control date-picker" name="publish_date" id="publish_date" value="<?php echo $addEditInfo->publish_date ?>" placeholder="Published Date" aria-describedby="basic-addon1" tabindex="5" data-date-format="yyyy-mm-dd" required>
					  <span class="input-group-addon" id="basic-addon1">To</span>
					  <input type="text" class="form-control date-picker" name="expire_date" id="expire_date" value="<?php echo $addEditInfo->expire_date ?>" placeholder="Expire Date" aria-describedby="basic-addon1" tabindex="6" data-date-format="yyyy-mm-dd" required>
				     </div>
					</div>
				  </div>
				</div>
				  

				  <div id="searchHide" class="row" style="padding-bottom:10px; display:none"> 
					  <div class="form-group" id="goSearch">
						<div class="col-sm-offset-3 col-sm-6" style="text-align:left">
						  <button  id="next" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Search</button> &nbsp; <span class="bookMsg"></span> 
						</div>
					  </div>
				   </div>



				     <div class="row" style="padding-bottom:10px"> 
						  <div class="form-group">
							<label for="add_link" class="col-sm-3 control-label">Title</label>
							<div class="col-sm-6">
							  <input type="text" class="form-control" id="title" name="title" placeholder="Title" tabindex="11" value="<?php echo $addEditInfo->title; ?>">
							</div>
						  </div>
						</div>
				  
                      
					  
					   
					    <div class="row" style="padding-bottom:10px"> 
						  <div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Ad Image</label>
							<div class="col-sm-3 text-left">
							  <div>
								 <div class="attachmentbody" data-target="#add_image" data-type="add_image">
									<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
								  </div> 
								   <input name="add_image" id="add_image" type="hidden" value="" required />  <span class="imageSize">
								     <?php if($addEditInfo->positon == 'Top'){ echo "(Size:255px X 90px)"; }else if($addEditInfo->positon == 'Left'){ echo "(Size:185px X 120px)"; }else if($addEditInfo->positon == 'Right'){ if($addEditInfo->serial_no =='9' || $addEditInfo->serial_no =='10' || $addEditInfo->serial_no =='15' || $addEditInfo->serial_no =='16'){ echo "(Size:258px X 50px)";  } else { echo "(Size:185px X 120px)"; }} ?></span>                                                                                
								</div>
							</div>
							<div class="text-left" style="padding-top:5px;">
									<img  src="<?php echo base_url("Images/Add_image/$addEditInfo->add_image") ?>" height="90" width="100" />
								</div>
							</div>
						  </div>
						</div>

						

               

                      
                         <div class="row" style="padding-bottom:10px;"> 
						  <div class="form-group">
							<label for="add_link" class="col-sm-3 control-label">Ad Deatails</label>
							<div class="col-sm-6">
							  <textarea class="form-control" name="description" id="ajaxfilemanager" cols="40" rows="20" placeholder="Add Details" tabindex="12"><?php echo $addEditInfo->description; ?></textarea>
							</div>
						  </div>
						</div>



					  
				     <div id="adSubmit">  
                         <div class="row" style="padding-bottom:10px"> 
						  <div class="form-group">
							<div class="col-sm-offset-3 col-sm-6" style="text-align:left">
							  <button type="submit" class="btn btn-primary">Update</button>
							</div>
						  </div>
						</div>
					</div>
			      </form> 
			
		    
        <div class="row">&nbsp;</div> 
		<div class="row">
		   <div class="col-lg-12 table-responsive" style="padding:0px;">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                  <tr class="active" style="font-weight:bold;">
                                    <td width="7%">Sl No</td>
                                    <td width="41%">Add Title</td>
                                    <td width="25%">Published Date</td>
                                    <td width="11%">Image</td>
                                    <td width="16%" align="center" valign="middle">Action</td>
                                  </tr>
                                  <?php 
                                    //print_r($studentId);
                                    $i = 1;
                                    foreach($UserWiseAddInfo as $v){
                                        $id		= $v->id;
                                       if($v->website =='country'){
				                          $adViewLink  = array('country', $v->country_name);
				                        } else {
				                          $adViewLink  = array('home');
				                        }
                                  ?>
                                  <tr> 
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->title; ?></td>
                                    <td><?php echo $v->publish_date; ?></td>
                                    <td><img src="<?php echo base_url("Images/Add_image/$v->add_image"); ?>" height="50" width="50" /></td>
                                    <td align="center">
                                       <a href="<?php echo site_url('organizationUserHome/addEdit/'. $id); ?>" name="Edit">Edit</a>                                                                                
										||
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De"> Delete </a> 
                                        ||
                                        <a target="_blank" href="<?php echo site_url($adViewLink); ?>" name="De">See ad</a>
                                         </td>
                                  </tr> <?php } ?>
                                </table> 
                               </div>
                               
           </div>
        <div class="row">&nbsp;</div>
        <div class="row">
                               
                             </div>
	</div>                
</div>
<script type="text/javascript">
     // ad url yes no
	$("#url").change(function() {
	var url = $("#url").val();	
	if(url =='Yes'){
	   $("#ad_url").css("display", "block");
	   $("#ad_details").css("display", "none");

	}else{
       $("#ad_url").css("display", "none");
	   $("#ad_details").css("display", "block");
	}

  });
	
		$('#deadline_date').datepicker({dateFormat: 'yy-mm-dd'});
</script>

