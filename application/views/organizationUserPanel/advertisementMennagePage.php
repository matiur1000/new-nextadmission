<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
               <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				body{
				  background-color:#EEEEEE;
				
				}
				
				</style>
	
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		 <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		 <script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
    
  </head>
  <body>
       <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
							  <div class="row">
							     <div class="col-lg-12" align="right">
	<div class="welll welll-lg">
    <div class="row">
  <div class="col-lg-7" align="center"></div>

  <div class="col-lg-5" align="center">
  <div class="comntorg2" style="color:#FFF; padding-top:5px;">Search CV Bank</div>
  <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;"><a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a></div></div>
    </div> 
	<div class="row">&nbsp;</div>
	
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Website Ad Manage</span></div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		<div class="row">
			<form action="<?php echo site_url('organizationUserHome/addStore'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
			 <input type="hidden" name="slNo" id="slNo" value="<?php echo $serial_no ?>">
			  <div class="form-group">
				<label for="select_website" class="col-sm-3 control-label">Select Website</label>
				<div class="col-sm-6">
				   <select class="form-control" id="select_website" name="select_website"  tabindex="1" readonly>
					<option value="" selected>Select Website</option>
					<option <?php if($select_website =='main'){?> selected="selected" <?php } ?> value="main">Main Website</option>
					<option  <?php if($select_website =='rigion'){?> selected="selected" <?php } ?>  value="rigion">Rigion Website</option>
				</select>
				</div>
			  </div>

			  <?php if(!empty($region)){ ?>
			  
			  <div class="form-group">
				<label for="region" class="col-sm-3 control-label">Select Region</label>
				<div class="col-sm-6">
				   <select class="form-control" id="region" name="region"  tabindex="2" readonly>
					<option value="" selected>Select Region</option>
					  <?php 
							foreach($allRegion as $v){
				  
						 ?>
						  <option <?php if($region == $v->region){?> selected="selected" <?php } ?> value="<?php echo $v->region_name; ?>"><?php echo $v->region_name; ?></option>
						<?php } ?>
					</select>
				</div>
			  </div>

			  <?php } ?>
			  
			  <div class="form-group">
				<label for="positon" class="col-sm-3 control-label">Select Position</label>
				<div class="col-sm-6">
				   <select class="form-control" id="positon" name="positon"  tabindex="3" readonly>
						<option value="" selected>Select Position</option>
						<option <?php if($positon =='Top'){?> selected="selected" <?php } ?>  value="Top">Top</option>
						<option <?php if($positon =='Left'){?> selected="selected" <?php } ?>  value="Left">Left</option>
						<option <?php if($positon =='Right'){?> selected="selected" <?php } ?>  value="Right">Right</option>
				   </select>
				</div>
			  </div>
			  
			  
			  <div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Select Serial</label>
				<div class="col-sm-6">
				   <select class="form-control" id="serial_no" name="serial_no"  tabindex="4" readonly>
					   <option value="" selected>Select Serial</option>
				   </select>
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Select Date</label>
				<div class="col-sm-6">
				   <div class="input-group">
			      <input type="text" class="form-control date-picker" name="publish_date" id="publish_date" placeholder="Published Date" aria-describedby="basic-addon1" tabindex="5" data-date-format="yyyy-mm-dd" value="<?php echo $publish_date ?>" readonly>
				  <span class="input-group-addon" id="basic-addon1">To</span>
				  <input type="text" class="form-control date-picker" name="expire_date" id="expire_date" placeholder="Expire Date" aria-describedby="basic-addon1" tabindex="6" data-date-format="yyyy-mm-dd" value="<?php echo $exp_date ?>" readonly>
			     </div>
				</div>
			  </div>
			  
		
			  
			  <div class="form-group">
				<label for="inputPassword3" class="col-sm-3 control-label">Title</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="title" name="title" placeholder="Title" tabindex="7">
				</div>
			  </div>
			  
			  
			  <div class="form-group">
				<label for="inputPassword3" class="col-sm-3 control-label">Ad Image</label>
				<div class="col-sm-6 text-left">
				  <input type="file" id="add_image" name="add_image" tabindex="8" />
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="manage_date" class="col-sm-3 control-label">Manage Date</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control date-picker" id="manage_date" name="manage_date" placeholder="Manage Date" tabindex="9" data-date-format="yyyy-mm-dd" >
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="deadline_date" class="col-sm-3 control-label">Deadline</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control date-picker" id="deadline_date" name="deadline_date" placeholder="Deadline" tabindex="10" data-date-format="yyyy-mm-dd">
				</div>
			  </div>
			  
			  
			  <div class="form-group">
				<label for="add_link" class="col-sm-3 control-label">Ad Url</label>
				<div class="col-sm-6">
				  <input type="text" class="form-control" id="add_link" name="add_link" placeholder="Ad Url" tabindex="11">
				</div>
			  </div>


			  <div class="form-group">
				<label for="add_link" class="col-sm-3 control-label">Ad Deatails</label>
				<div class="col-sm-6">
				  <textarea class="form-control" name="description" id="ajaxfilemanager" cols="60" rows="10" placeholder="Add Details" tabindex="12"></textarea>
				</div>
			  </div>
			  
			  <div class="form-group">
				<div class="col-sm-offset-3 col-sm-6" style="text-align:left">
				  <button type="submit" class="btn btn-primary">Submit</button>
				</div>
			  </div>
			  
			 
		 </form>
			    
		</div>
        <div class="row">&nbsp;</div>
		   <div class="row">
                               <div class="col-lg-12 table-responsive" style="padding:0px;">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                  <tr class="active" style="font-weight:bold;">
                                    <td width="7%">Sl No</td>
                                    <td width="41%">Add Title</td>
                                    <td width="25%">DeadLine</td>
                                    <td width="11%">Image</td>
                                    <td width="16%" align="center" valign="middle">Action</td>
                                  </tr>
                                  <?php 
                                    //print_r($studentId);
                                    $i = 1;
                                    foreach($UserWiseAddInfo as $v){
                                        $id		= $v->id;
                                  ?>
                                  <tr> 
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->title; ?></td>
                                    <td><?php echo $v->deadline_date; ?></td>
                                    <td><img src="<?php echo base_url("Images/Add_image/$v->add_image"); ?>" height="50" width="50" /></td>
                                    <td align="center">
                                       <a href="<?php echo site_url('organizationUserHome/addEdit/'. $id); ?>" name="Edit">
                                        Edit                                        </a>
										||
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            Delete                                        </a>                                    </td>
                                  </tr> <?php } ?>
                                </table> 
                               </div>
                             </div> 
        <div class="row">&nbsp;</div>
        <div class="row">
                               
                             </div>
	</div>                
</div>
						   </div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
	   <?php// $this->load->view('footerTopPage'); ?>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script>

	     $(document).ready(function(){
			$("#positon").trigger("change");
			
		});


		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('OrganizationUserHome/addDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});
		
		$('.date-picker').datepicker({
			autoclose: true	  
		});	
	</script>

	<script>
	resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
		
		
		// Positon Wise Serial No
		$("#positon").change(function() {
		var positon = $("#positon").val();	
		var slNo 	= $("#slNo").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('organizationUserHome/adManageSerialNoTrigger'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { positon : positon, slNo : slNo},
			dataType : "html",
			success : function(data) {			
				$("#serial_no").html(data);
			}
		});
		
	});
	
	// Region Wise Position
		$("#region").change(function() {
		var region = $("#region").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('organizationUserHome/regionWisePosition'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : region },
			dataType : "html",
			success : function(data) {			
				$("#regionPositon").html(data);
			}
		});
		
	});
	
	// Region Wise Sl No
		$("#regionPositon").change(function(e) {
		var region 			= $("#region").val();	
		var regionPositon 	= $("#regionPositon").val();
			
	
		$.ajax({
			url: "<?php echo site_url('organizationUserHome/regionWiseSlNo') ?>", // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { region : region, regionPositon : regionPositon },
			dataType : "html",
			success : function(data) {			
				$("#region_serial_no").html(data);
			}
		});
		
	});
 </script>
 
 <script>
	   // Region Wise Country
		$("#premium_user_type").change(function() {
			var amount = $("#premium_user_type option:selected").attr('data-amount');
		 
		    $("#amount").val(amount);
			
		});
                
	</script>
    
  </body>
</html>