<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
	
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				.menu-circular-style2 {
					height:30px;
					margin-left:5px;
					padding-top:5px;
					border-bottom:0px;
					border-radius:5px;
					border-bottom-left-radius: 0px;
					border-bottom-right-radius: 0px;
					background:#f4f4f4;
					}
					
					.menu-inactive {
					height:30px;
					margin-left:5px;
					padding-top:5px;
					border-bottom:0px;
					border-radius:5px;
					border-bottom-left-radius: 0px;
					border-bottom-right-radius: 0px;
					background:#FFFFFF;
					}
					
					body{
						background-color:#EEEEEE;
				
				    }
									
				
				</style>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
       
	    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
	    <script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
 
				  
    
    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->
  </head>
  <body>
  
      <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       
       <div class="container"> 
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">
							  <div class="col-lg-7" align="center"></div>
                        
                              <div class="col-lg-5" align="center">
                              <div class="comntorg2 link-stylejob" style="color:#FFF; padding-top:5px;">
							  <a href="<?php echo site_url('organizationUserHome/PostNewCV'); ?>">Search CV Bank</a></div>
                              <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
                              <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a></div></div>
								</div>
							  <div class="row">&nbsp;</div> 
							  <div class="row">
							  
									<div class="col-lg-12" align="center">
									<?php $this->load->view('organizationUserPanel/middlejobWiseDetailsPage'); ?>
									</div>
								
							  
							 
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
	    <?php// $this->load->view('footerTopPage'); ?>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <!--SHARE THIS-->
	 <script>
	    $(document).ready(function(){
			$(".nav-tabs a").click(function(){
				$(this).tab('show');
			});
		   
		});
		resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
		
		
		//shortlist array action
		$('#createSortListed').on('click', function(e) {
			$.ajax({
				url: "<?php echo site_url('organizationUserHome/shortListAction') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					alert('Short List');
					window.location.assign('<?php echo site_url('organizationUserHome') ?>');
				}
			});

			e.preventDefault();
			
		});
		
		//star candidate Array Action
		$('#starCandidate').on('click', function(e) {
			$.ajax({
				url: "<?php echo site_url('organizationUserHome/starCandidateAction') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					alert('Star Candidates');
					 window.location.assign('<?php echo site_url('organizationUserHome') ?>');
				}
				
			});
			
		});

        //Search all applicant

		$('a[href="#quick"]').on('click', function (){
			$('#filter_type').val(1);
		});

		$('a[href="#academic"]').on('click', function (){
			$('#filter_type').val(2);
		});
		
		//Search all applicant
		$('#search').on('click', function(e) {
			var postId			=$("#id").val(); 
			var applicantName	=$("#applicantName").val(); 
			var ageFrom			=$("#ageFrom").val(); 
			var ageTo			=$("#ageTo").val(); 
			var gender			=$("#gender").val(); 
			var starcandidates	=$('#starcandidates:checked').val();
			var degree			=$("#degree").val(); 
			var degreeTitle		=$("#degreeTitle").val(); 
			var subject			=$("#subject").val(); 
			var result			=$("#result").val(); 
			var institute		=$("#institute").val(); 

			var filter_type		= $("#filter_type").val(); 
			var has_error 		= 0;

			if(filter_type == 1) {
				if( !applicantName && !ageFrom && !ageTo && !gender && !starcandidates ){
					alert("plz select at least one field for search");
					has_error = 1;
				}
			} else {
				if( !degree && !degreeTitle && !subject && !result && !institute ){
					alert("plz select select at least one field for search");
					has_error = 1;					
				}
			}
			
			if(has_error == 0) {
				$.ajax({
					url: "<?php echo site_url('organizationUserHome/searchAllApplicant') ?>",
					type : "POST",
					data: {postId : postId, applicantName : applicantName, ageFrom : ageFrom, ageTo : ageTo
							,gender : gender, degree : degree, degreeTitle : degreeTitle,
							subject : subject, result : result, institute : institute, starcandidates : starcandidates},
					dataType : "html",
					success : function(data) {			
						$("#viewReport").html(data);
					}
				});	
			}
		
	});	


 </script>
  </body>
</html>