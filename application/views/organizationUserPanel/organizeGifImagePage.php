<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
               <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				body{
				  background-color:#EEEEEE;
				
				}
				
				</style>
	
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		 <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		 <script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
  </head>
  <body>
       <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
							  <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
							  <div class="welll welll-lg">
   					             <div class="row">
							        <div class="col-lg-9" align="center" style="font-size:18px;"><?php echo $updateText; ?></div>
                                    <div class="col-lg-3" align="right">
		                               <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
		                                  <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a>
		                                </div>
		                            </div>
								   </div>
									<div class="row">&nbsp;</div>
									<div class="row">
									   <div class="col-lg-12" align="center">
										 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Upload image for gif file </span></div>
									</div>  
									<div class="row">&nbsp;</div>
									<div class="row">
									  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
									  </div>
									</div>
								<div class="row">&nbsp;</div>   
								<div class="row">
								  <div class="col-lg-11">
									<form action="<?php echo site_url('organizationUserHome/orgGifImageStore'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
									  <div class="form-group">
										<label for="image_position" class="col-sm-3 control-label">Select promotion position</label>
										<div class="col-sm-7">
										     <select class="form-control" id="image_position" name="image_position"  tabindex="1"  required>
												<option value="" selected>Select position</option>
												<option value="Top">Top</option>
												<option value="Left">Left</option>
												<option value="Right">Right</option>
												<option value="other">Right -(9 or 10 or 15 or 16)</option>

											 </select>
										</div>
									  </div>
									  <div class="row" style="padding-bottom:10px"> 
										  <div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Upload image <span class="imagesize"></span></label>
											<div class="col-sm-2 text-left">
											   <label for="inputPassword3" class="col-sm-3 control-label">Image1</label>
											  <div>
												 <div class="attachmentbody" data-target="#image1" data-type="image1">
													<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
												  </div> 
												   <input name="image1" id="image1" type="hidden" value="" required />                  
												</div>
											</div>
											<div class="col-sm-2 text-left">
											   <label for="inputPassword3" class="col-sm-3 control-label">Image2</label>
											    <div>
												 <div class="attachmentbody" data-target="#image2" data-type="image2">
													<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
												  </div> 
												   <input name="image2" id="image2" type="hidden" value="" required />                  
												</div>
											</div>

											<div class="col-sm-2 text-left">
											   <label for="inputPassword3" class="col-sm-3 control-label">Image3</label>
											    <div>
												 <div class="attachmentbody" data-target="#image3" data-type="image3">
													<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
												  </div> 
												   <input name="image3" id="image3" type="hidden" value="" required />                  
												</div>
											</div>

											<div class="col-sm-2 text-left">
											   <label for="inputPassword3" class="col-sm-3 control-label">Image4</label>
											    <div>
												 <div class="attachmentbody" data-target="#image4" data-type="image4">
													<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
												  </div> 
												   <input name="image4" id="image4" type="hidden" value="" required />                  
												</div>
											</div>


											</div>
										  </div>
										</div>
									  
									  
									  <div class="form-group">
										<div class="col-sm-offset-3 col-sm-10">
										  <button type="submit" class="btn btn-primary">Submit</button>
										</div>
									  </div>
									</form>
									</div>
									<div class="col-lg-1"></div>
								         
								</div>
        							<div class="row">&nbsp;</div>
									<div class="row">&nbsp;</div>
                               <div class="row">
                               <div class="col-lg-12 table-responsive" style="padding:0px;">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                  <tr class="active" style="font-weight:bold;">
                                    <td width="5%">Sl No</td>
                                    <td width="17%">Image 1 </td>
                                    <td width="13%">Image 2 </td>
                                    <td width="15%">Image 3 </td>
                                    <td width="17%">Image 4 </td>
                                    <td width="33%" align="center" valign="middle">Action</td>
                                  </tr>
                                  <?php 
                                     //print_r($organizeGifInfo);
                                    $i = 1;
                                    foreach($organizeGifInfo as $v){
                                        $id		= $v->id;
                                  ?>
                                  <tr> 
                                    <td><?php echo $i++; ?></td>
                                    <td><img src="<?php echo base_url("/resource/gif_image/$v->image1"); ?>" height="50" width="100" /></td>
                                    <td><img src="<?php echo base_url("/resource/gif_image/$v->image2"); ?>" height="50" width="100" /></td>
                                    <td><img src="<?php echo base_url("/resource/gif_image/$v->image3"); ?>" height="50" width="100" /></td>
                                    <td><img src="<?php echo base_url("/resource/gif_image/$v->image4"); ?>" height="50" width="100" /></td>
                                    <td align="center">
                                        <a href="<?php echo site_url('organizationUserHome/organizeGifEdit/'. $id); ?>" name="Edit">
                                        Edit                                        </a>
										||
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            Delete </a> 
                                            ||  
                                        <a target="_blank" href="<?php echo site_url('organizationUserHome/makeGif/'. $id); ?>" name="Mke image to gif">
                                            View gif and save file</a>                                  
                                         </td>
                                  </tr> <?php } ?>
                                </table> 
                               </div>
                             </div>
							</div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			    </div> 
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url("resource/js/ajaxupload.3.5.js"); ?>"></script>
  <script>
		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('organizationUserHome/gifDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});
		
		
		
		
		resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
		
		
		// Positon Wise Serial No
		$("#image_position").change(function() {
		 var positon = $("#image_position").val();	
         if(positon == 'Top'){
            $('.imagesize').text("(255 x 90)");
         } else if(positon == 'Left'){
            $('.imagesize').text("(185 x 120)");
         } else if(positon == 'Right'){
            $('.imagesize').text("(125 x 90)");
         } else {
            $('.imagesize').text("(258 x 50)");
         }

	
		
	  });
	
	
	</script>

	
 
 
  </body>
</html>