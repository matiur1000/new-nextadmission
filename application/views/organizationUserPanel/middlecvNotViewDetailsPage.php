   <div class="row comntorg4" style="margin-left:-5px;">
     
	   <table width="100%" cellspacing="0" cellpadding="0" class="table" id="inputTable">
	   <tr>
		    <td height="34">&nbsp;Applicant Name And Contact</td>
		    <td><span id="SearchedResult">Latest Education </span></td>
		    <td>Age</td>
		    <td>Apply Date </td>
	     </tr>
		  <?php 
		   $i = 1;
		   foreach($cvNotViewApplicant as $v){
		    $applyId   	   = $v->id;
			$cvLink   	   = array('organizationUserHome','applyCvView', $v->app_user_id, $v->post_id);
			$currentDate   = date("d/m/Y");
			$birthDate     = $v->date_of_birth;
			
			$curntOrderDate = explode('/', $currentDate);
			$curday 		= $curntOrderDate[0];
			$curmonth   	= $curntOrderDate[1];
			$curyear  		= $curntOrderDate[2];
			
			$birthOrderDate = explode('/', $birthDate);
			$birthday 		= $birthOrderDate[0];
			$birthmonth   	= $birthOrderDate[1];
			$birthyear  	= $birthOrderDate[2];
			
			if($curday < $birthday){
			 $curdayValu = $curday + 30;
			}else{
			   $curdayValu = $curday;	
			  }	
			 
			 if($curday < $birthday){ 
			   $birthmonthValu  = $birthmonth + 1;  
			  }else{
			   $birthmonthValu  = $birthmonth;
			   }
			  if($birthmonthValu > $curmonth){
			     $curmonthValuMain  = $curmonth + 12;
				}else{
				  $curmonthValuMain  = $curmonth;
				  }
				  
				if($birthmonthValu > $curmonth){
				      $birthyearValu  = $birthyear + 1;
					}else{
					 $birthyearValu  = $birthyear;
					}
					
				$ageDay  = $curdayValu - $birthday;
				$ageMonth  = $curmonthValuMain - $birthmonthValu; 
				$ageYear  = $curyear - $birthyearValu; 
				
			 
		  
		  ?>
		  
		  <tr>
			<td width="48%"><table width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="2" align="center" valign="top">
				 <div class="row">
				  <div class="col-lg-2" align="right" style="padding-right:0px;"><img src="<?php echo base_url("/Images/Register_image/$v->image"); ?>" height="70" width="70" /></div>
				  <div class="col-lg-10">
				    <div class="row">
					 	<div class="col-lg-12" align="left" valign="top" style="padding-top:1px; font-size:19px; font-weight:normal;"><a target="_blank" href="<?php echo site_url($cvLink); ?>"><?php echo $v->name; ?></a></div>
						<div class="col-lg-12" align="left" valign="top" style="font-size:14px; font-weight:normal;">Contact Information</div>
						<div class="col-lg-12" align="left" valign="top" style="font-size:12px; color:#0066FF; font-weight:normal;">M :&nbsp;<?php echo $v->mobile; ?></div>
						<div class="col-lg-12" align="left" valign="top" style="font-size:12px; color:#0066FF; font-weight:normal;">E :&nbsp;<?php echo $v->email; ?></div>
						<div class="col-lg-12" align="left" valign="top" style="font-size:12px; color:#0066FF; font-weight:normal;"><?php echo $v->address; ?></div>
					</div>
				  </div>
				 </div>				</td>
              </tr>
              <tr>
                <td width="20%">&nbsp;</td>
                <td width="78%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
			<td width="18%" align="left" valign="top"><table width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="94%" align="left" valign="middle"><?php echo $v->degree_title; ?></td>
                <td width="6%">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="middle" style="padding-top:2px; padding-bottom:2px; font-size:12px; font-weight:normal; color:#0066FF;"><?php echo $v->institute_name; ?></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
			<td width="15%" align="left" valign="top"><span style="font-weight:normal">Year:</span> <?php echo $ageYear; ?> </br><span style="font-weight:normal">Month:</span> <?php echo $ageMonth; ?> <span style="font-weight:normal">Day:</span> <?php echo $ageDay; ?></td>
			<td width="17%" align="left" valign="top"><i class="glyphicon glyphicon-calendar"></i>&nbsp;<span style="font-weight:normal"><?php echo $v->job_apply_date; ?></span></td>
		  </tr>
		  <?php } ?>
	   </table>

      
</div>
   </div>

