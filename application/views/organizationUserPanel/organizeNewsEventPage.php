<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">

    
	
               <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				body{
				  background-color:#EEEEEE;
				
				}
				
				</style>
	
         <?php $this->load->view('jsLinkPage'); ?>  
	     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
	     <script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  
	     <script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
	<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "exact",
			elements : "ajaxfilemanager",
			theme : "advanced",
			// TO GET THE FULL IMAGE URL
			relative_urls: false,
    		remove_script_host: false,
			/****************/
			/*setup : function(ed) {
			      ed.onKeyUp.add(function(ed, l) {
			         tinyMCE.triggerSave();	                    
			      });
			},*/
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",

			theme_advanced_buttons1_add_before : "newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
			theme_advanced_buttons2_add_before: "cut,copy,separator,",
			theme_advanced_buttons3_add_before : "",
			theme_advanced_buttons3_add : "media",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "ajaxfilemanager",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php");
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php"),
                width: 482,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
		}
	</script>
    
  </head>
  <body>
        <div class="container-fluid" style="background-color:#FFFFFF">
           <div class="row">
		       <div class="container"> 
			      <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
			   </div>
		    </div> 
			 
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
							  <div class="welll welll-lg">
   					             <div class="row">
									  <div class="col-lg-9" align="center"></div>

									  <div class="col-lg-3" align="right">
									  <div class="comntorg link-stylejob" style="color:#FFF; padding-top:5px;">
									            <a href="<?php echo site_url('organizationUserHome/PostNewJob'); ?>">Post a New Job</a>
										    </div>
										  </div>
    									</div> 
									<div class="row">&nbsp;</div>
									<div class="row">
									   <div class="col-lg-12" align="center">
										 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">News and event manage</span></div>
									</div>  
									<div class="row">&nbsp;</div>
									<div class="row">
									  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
									  </div>
									</div>
								<div class="row">&nbsp;</div>   
								<div class="row">
								  <div class="col-lg-11">
									<form action="<?php echo site_url('organizationUserHome/orgNewEventStore'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Title</label>
										<div class="col-sm-7">
										  <input type="text" class="form-control" id="title" name="title" placeholder="Title">
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Date</label>
										<div class="col-sm-7">
										  <input type="text" class="form-control date-picker" id="date" name="date" data-date-format="yyyy-mm-dd" placeholder="Date">
										</div>
									  </div>


									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Select news type</label>
										<div class="col-sm-7">
										   <select name="news_type" id="news_type" class="form-control">
										     <option selected="selected" value="">Select news type</option>
										     <option value="image">Image</option>
										     <option value="video">Video</option>
										   </select>
										</div>
									  </div>

									  <div id="videoType" class="form-group" style="display:none">
										<label for="inputEmail3" class="col-sm-3 control-label">Select video type</label>
										<div class="col-sm-7">
										   <select name="video_type" id="video_type" class="form-control">
										     <option selected="selected" value="">Select video type</option>
										     <option value="embeded">Embeded code</option>
										     <option value="video">Self Video</option>
										   </select>
										</div>
									  </div>

									  <div id="upvideo" class="form-group" style="display:none">
										<label for="inputPassword3" class="col-sm-3 control-label">Upload video</label>
										<div class="col-sm-7">
										  <input type="file" id="video" name="video" tabindex="8" />
										</div>
									  </div>

									  <div id="embeded" class="form-group" style="display:none">
										<label for="inputPassword3" class="col-sm-3 control-label">Embeded code</label>
										<div class="col-sm-7">
										<textarea class="form-control" name="embeded_code" id="embeded_code" cols="60" rows="" placeholder="Organization Address"></textarea>
										</div>
									  </div>
									  

									  <div id="newsImage" class="form-group" style="display:none">
										<label for="inputPassword3" class="col-sm-3 control-label">News Image</label>
										<div class="col-sm-7">
										  <input type="file" id="image" name="image" tabindex="8" />
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Details</label>
										<div class="col-sm-7">
											<textarea class="form-control" name="description" id="ajaxfilemanager" cols="60" rows="20"></textarea></div>
									  </div>
									  
									  <div class="form-group">
										<div class="col-sm-offset-3 col-sm-10">
										  <button type="submit" class="btn btn-primary">Submit</button>
										</div>
									  </div>
									</form>
									</div>
									<div class="col-lg-1"></div>
								         
								</div>
        							<div class="row">&nbsp;</div>
									<div class="row">&nbsp;</div>
                               <div class="row">
                               <div class="col-lg-12 table-responsive" style="padding:0px;">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                  <tr class="active" style="font-weight:bold;">
                                    <td width="6%">Sl No</td>
                                    <td width="47%">Title</td>
                                    <td width="12%">Date</td>
                                    <td width="9%">Image</td>
                                    <td width="13%">Video</td>
                                    <td width="13%" align="center" valign="middle">Action</td>
                                  </tr>
                                  <?php 
                                    //print_r($studentId);
                                    $i = 1;
                                    foreach($newsAndEventInfo as $v){
                                        $id		= $v->id;
                                        $pieces = explode(" ", $v->embeded_code);
										$pieces[3]; 
                                  ?>
                                  <tr> 
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->title; ?></td>
                                    <td><?php echo $v->date; ?></td>
                                    <td>
                                      <?php if(!empty($v->image)){ ?>
                                        <img src="<?php echo base_url("/Images/News_image/$v->image"); ?>" height="50" width="100" />
                                       <?php }else{ ?>
                                        <img src="<?php echo base_url("/resource/img/noimage.png"); ?>" height="50" width="100" />
                                       <?php } ?>
                                     </td>
                                    <td>
                                        <?php 
                                            if(!empty($v->video) || !empty($v->embeded_code)){ 
                                              if(!empty($v->video)){
                                           ?>
	                                       <video width="150" height="100" controls>
												<source src="<?php echo base_url("/Images/News_image/$v->video"); ?>" type="video/mp4">
										   </video> 
	                                      <?php }else { ?>
	                                        <iframe width="150" height="100" <?php echo $pieces[3]; ?> frameborder="0" allowfullscreen></iframe>
	                                      <?php } } else { ?> 
                                        	<img src="<?php echo base_url("/resource/img/novideo.png"); ?>" height="50" width="100" />
	                                      <?php } ?>            
                                    </td>
                                    <td align="center">
                                        <a href="<?php echo site_url('organizationUserHome/organizeNewsEdit/'. $id); ?>" name="Edit">
                                        Edit                                        </a>
										||
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            Delete                                        </a>                                    </td>
                                  </tr> <?php } ?>
                                </table>   
                               </div>
                             </div>
							</div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>  
			    
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script>

       $('.date-picker').datepicker({
			autoclose: true	  
		});	

       $("#news_type").change(function(){
       	  var newsType  = $("#news_type").val();
       	  if(newsType == 'image'){
            $("#newsImage").css("display", "block");
            $("#videoType").css("display", "none");
       	  }else{
            $("#newsImage").css("display", "none");
            $("#videoType").css("display", "block");
       	  }

       });


       $("#video_type").change(function(){
       	  var videoType  = $("#video_type").val();
       	  if(videoType == 'embeded'){
            $("#embeded").css("display", "block");
            $("#upvideo").css("display", "none");
       	  }else{
            $("#embeded").css("display", "none");
            $("#upvideo").css("display", "block");
       	  }

       });
	 


		$('.red').on('click', function() {
			var x = confirm('Are you sure to delete?');
			
			if(x){
				var id = $(this).attr('data-id');
				console.log(id);
				var url = SAWEB.getSiteAction('organizationUserHome/organizeNewsEventDelete/'+id);
				location.replace(url);
			} else {
				return false;
			}
		});
	</script>

	<script>
	resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
 </script>
    
  </body>
</html>