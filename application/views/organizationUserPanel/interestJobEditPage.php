<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
    <style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				</style>
    
	
	 <?php $this->load->view('jsLinkPage'); ?>  
     <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
 

    
   
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('organizationUserPanel/orgHeaderPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationUserPanel/orgMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">
							   <form method="post" action="#" name="formSearch" class="form-inline">
								 <div class="col-lg-3" align="right">
									<label for="search"><h5> Search All Post </h5></label>
								 </div>
								 <div class="col-lg-9" align="center">
								   <input type="search" name="search" id="search" class="form-control"  placeholder="Search by Programs" 
								   style="width:80%;"> 
										<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Search</button>  
										<div class="row">&nbsp;</div>     
								 </div>
								 </form>
								</div>
							  <div class="row">&nbsp;</div> 
							  <div class="row">
							  <?php $this->load->view('organizationUserPanel/organizeLeftMenuPage'); ?>
									
							 <?php $this->load->view('organizationUserPanel/middleInterstPostEditPage'); ?>
                             <div class="row">
                               <div class="col-lg-12">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#F0F0F0;" class="table table-hover">
                                  <tr>
                                    <td width="3%">&nbsp;</td>
                                    <td width="4%">Sl No</td>
                                    <td width="17%">Education Provide type</td>
                                    <td width="20%">Interested Program</td>
                                    <td width="16%">Location</td>
                                    <td width="27%">Action</td>
                                  </tr>
                                  <?php 
                                    //print_r($UserWiseIntJobInfo);
                                    $i = 1;
                                    foreach($UserWiseIntJobInfo as $v){
                                        $id		= $v->id;
                                  ?>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $v->education_provider_type; ?></td>
                                    <td><?php echo $v->interested_program; ?></td>
                                    <td><?php echo $v->location; ?></td>
                                    <td>
                                        <a href="<?php echo site_url('organizationUserHome/intrestEdit/'. $id); ?>" name="Edit">
                                        <button name="edit" class="btn btn-link"> <i class="icon-edit"></i> Edit</button>
                                        </a>
                                        <a class="red" href="#" data-id="<?php echo $id ?>" name="De">
                                            <button  name="Delete" class="btn btn-link"> <i class="icon-remove-sign"></i> Delete</button>
                                        </a>
                                    </td>
                                  </tr> <?php } ?>
                                  <tr>
                                    <td colspan="6"></td>                       
                                  </tr>
                                 
                                </table>
                               </div>
                             </div> 
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <!--SHARE THIS-->
	<script type="text/javascript">stLight.options({publisher: "7521b38c-5f2b-4808-b7b5-8057deecb289", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <script>
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('organizationUserHome/intJobDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
				
				resizereinit=true;

		menu[1] = {
		id:'menu1', //use unique quoted id (quoted) REQUIRED!!
		fontsize:'100%', // express as percentage with the % sign
		linkheight:35 ,  // linked horizontal cells height
		hdingwidth:210 ,  // heading - non linked horizontal cells width
		// Finished configuration. Use default values for all other settings for this particular menu (menu[1]) ///
		
		menuItems:[ // REQUIRED!!
		//[name, link, target, colspan, endrow?] - leave 'link' and 'target' blank to make a header
		 //create header
		["Recruiter's Panel", "<?php echo site_url('organizationUserHome/index');?>", ""],
		["Job Ad Management", "#",""],
		["CV Bank", "#",""],
		["Communication", "#",""],
		["Candidate Management", "#",""],
		["Edit Account Info", "<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>",""],
		["Payment Status", "#",""],
		["Support", "#",""],
		["Change Password", "<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>",""],
		["Email Notification", "#",""],
		["Services", "#",""],
		
		]}; 
		
		make_menus();
	</script>
  </body>
</html>