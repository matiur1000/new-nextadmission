   <div class="footer_wrapper_section"><!--Footer_Wrapper_Section-->
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-3">
                          <div class="footer_left_section">
                              <h2>About Us</h2>
                              <ul>
                                  <li><a href="#">About nextaddmission.com</a></li>
                                  <li><a href="#">Terms & Condition</a></li>
                                  <li><a href="#">International Partners</a></li>
                                  <li><a href="#">Privacy Policy</a></li>
                                  <li><a href="#">Feedback</a></li>
                                  <li><a href="<?php echo site_url('home/contactUS'); ?>" >Contact Us</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="footer_left_section">
                              <h2>Job Seekers</h2>
                              <ul>
                                  <li><a href="#">Create Account</a></li>
                                  <li><a href="#">Career Counseling</a></li>
                                  <li><a href="#">FAQ</a></li>
                                  <li><a href="#">Video Guides</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="footer_left_section">
                              <h2>Organizations</h2>
                              <ul>
                                  <li><a href="#">Create Account</a></li>
                                  <li><a href="#">Service</a></li>
                                  <li><a href="#">FAQ</a></li>
                                  <li><a href="#">Post A Job</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="footer_left_section">
                              <h2>Tools & Social Media</h2>
                              <ul>
                                  <li><a href="#">Mobile App</a></li>
                                  <li><a href="#">GSL </a></li>
                                  <li><a href="#">FAQ</a></li>
                                  <li><a href="#">Post A Job</a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>

            </div>
        </div>
    </div><!--End_Footer_Wrapper_Section-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
    <script src="<?php echo base_url('resource/nextadmission/js/jquery.min.js'); ?>"></script>

   
    <script type="text/javascript">
      $('.nav button').on('click',function(){
      $('button').removeClass('active5');
      $(this).addClass('active5');
      });
    </script>

    <!-- Sign_In Script-->
    <script>
        $(document).ready(function() {
            var first = true;

            // Hide menu once we know its width
            $('#showmenu').click(function() {
                var $menu = $('.menu');
                if ($menu.is(':visible')) {
                    // Slide away
                    $menu.animate({left: -($menu.outerWidth() + 10)}, function() {
                        $menu.hide();
                    });
                }
                else {
                    // Slide in
                    $menu.show().css("left", -($menu.outerWidth() + 10)).animate({left: 0});
                }
            });
        });
    </script>






    <script>

$(function () {
   $('.tlt').textillate({
  // the default selector to use when detecting multiple texts to animate
  selector: '.texts',

  // enable looping
  loop: true,

  // sets the minimum display time for each text before it is replaced
  minDisplayTime: 6000,

  // sets the initial delay before starting the animation
  // (note that depending on the in effect you may need to manually apply
  // visibility: hidden to the element before running this plugin)
  initialDelay: 0,

  // set whether or not to automatically start animating
  autoStart: true,

  // custom set of 'in' effects. This effects whether or not the
  // character is shown/hidden before or after an animation
  inEffects: [],

  // custom set of 'out' effects
  outEffects: [ 'hinge' ],

  // in animation settings
  in: {
    // set the effect name
    effect: 'fadeInLeftBig',

    // set the delay factor applied to each consecutive character
    delayScale: .1,

    // set the delay between each character
    delay: 10,

    // set to true to animate all the characters at the same time
    sync: false,

    // randomize the character sequence
    // (note that shuffle doesn't make sense with sync = true)
    shuffle: false,

    // reverse the character sequence
    // (note that reverse doesn't make sense with sync = true)
    reverse: false,

    // callback that executes once the animation has finished
    callback: function () {}
  },

  // out animation settings.
  out: {
    effect: 'hinge',
    delayScale: .1,
    delay: 10,
    sync: false,
    shuffle: false,
    reverse: false,
    callback: function () {}
  },

  // callback that executes once textillate has finished
  callback: function () {},

  // set the type of token to animate (available types: 'char' and 'word')
  type: 'word'
});

})

$(function(){
    var $mwo = $('.LetestNews');
    //$('.marquee').marquee();
    $('.LetestNews').marquee({
        //speed in milliseconds of the marquee
        speed: 4000,
        //gap in pixels between the tickers
        gap: 1,
        //gap in pixels between the tickers
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
        pauseOnHover: true
    });
    //Direction upward
    
    //pause and resume links
    $('.pause').click(function(e){
        e.preventDefault();
        $mwo.trigger('pause');
    });
    $('.resume').click(function(e){
        e.preventDefault();
        $mwo.trigger('resume');
    });
    //toggle
    $('.toggle').hover(function(e){
        $mwo.trigger('pause');
    },function(){
        $mwo.trigger('resume');
    })
    .click(function(e){
        e.preventDefault();
    })
});


	$(document).on("click", ".updatePost", function(e)
	{
	   var id 		= $(this).attr("data-id");
	   var formURL  = "<?php echo site_url('commentPopup/postedit'); ?>";
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : {id: id},
			dataType: "html",
			success:function(data){
				$("#blogDetails .modal-content").html(data);
				$("#modal-form").modal({
					keyboard: false,
					backdrop: 'static',
				});

				 initiateFileUpload();
			}
		});
	});
	    

	   $("#searchForm").submit(function(e)
		{
		alert('matiur');
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#searchOrganization").html(data);				
				}
			});
			
			e.preventDefault();
		});
		
		//North America Effict
		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
        $(document).on("change", "#country_id_chnge", function(){
			var country_id = $(this).val();	
			

			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
        });


	   //ORGANIZATION WISE PROGRAM 

	     $("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	



        // On change search all
	     $(document).on("change", "#country_id_chnge", function(){
			var country_id = $(this).val();	
			console.log(country_id);
			alert(country_id);

			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});		
        });

	      $("#organize_id").change(function() {
			var country_id  = $("#country_id_chnge").val();	
			var organize_id = $("#organize_id").val();	
				
			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id, organize_id : organize_id},
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});			
		});	


	     $("#program_id").change(function() {
			var country_id  = $("#country_id_chnge").val();	
			var organize_id = $("#organize_id").val();	
			var program_id  = $("#program_id").val();	

			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id, organize_id : organize_id, program_id : program_id},
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});			
		});	

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.readmore').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
		
		//Breaking news
		
		$(".latesnewId").on('click', function(){
			var id = $(this).attr("data-id");
			var laturl = "<?php echo site_url("home/latesnewIdd"); ?>";
			
			$.ajax(
			{
				url : laturl,
				type: "POST",
				data:{id:id},
				success:function(data){
				$('#breaking').modal('show');
				$("#breakingNewidDeatial").html(data);
				}
			});
			
		});
	</script>
	
	
	
	
	
	
	




  </body>
</html>