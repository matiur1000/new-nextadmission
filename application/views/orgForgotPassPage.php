<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
        <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/js/menu_script.js'); ?>"></script>
 

    
    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		     <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-9">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">
							     <div class="col-lg-12">
								 </div>
							   </div>
							   <div class="row">
							     <div class="col-lg-12" style="margin-left:10px;">
								    <h1 class="forget">
										<i class="glyphicon glyphicon-question-sign" style="margin-right:10px;"></i>
										Forgot Password?
									</h1>
								 </div>
							   </div>
							   <div class="row">
							     <div class="col-lg-12" style="margin-left:10px;">
								    <p class="fortext">You can retrieve your login information (UserId / Password) to your email address.</p>
								 </div>
							   </div>
							   <div class="row">
							     <div class="col-lg-12" style="margin-left:10px;">
								    <p class="fortext">Type your Email address which you have used in your UserId: </p>
								 </div>
							   </div>
							   <div class="row">&nbsp;</div> 
							   <form method="post" action="<?php echo site_url('registration/sendMail');?>" name="sendMail" id="sendMail">
							   <div class="row">
							     <div class="col-lg-12" style="margin-left:10px;">
										<input class="form-control" type="text" placeholder="Email Address" name="user_id" id="user_id" style="min-height:50px;">
								 </div>
							   </div>
							    <div class="row">&nbsp;</div> 
							   <div class="row">
							     <div class="col-lg-12" style="margin-left:10px;">
									<button type="submit" class="btn btn-primary btn-lg" style="min-width:150px;">Send Mail</button>
								 </div>
							   </div>
							   </form>
							   <div class="row">&nbsp;</div> 
							   <div class="row">
							     <div class="col-lg-12">
								  <p class="fortext"> N.B: Do not type more than one email address.</p>
								 </div>
							   </div>
							   <div class="row">
							     <div class="col-lg-12">
								 </div>
							   </div>
							  <div class="row">&nbsp;</div> 
							  <div class="row">
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						   <?php $this->load->view('rightSidebar2Page'); ?>                 <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             <div class="row">&nbsp;</div>     
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <!--SHARE THIS-->
	<script type="text/javascript">stLight.options({publisher: "7521b38c-5f2b-4808-b7b5-8057deecb289", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <script>
    var options={ "publisher": "7521b38c-5f2b-4808-b7b5-8057deecb289", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "email", "print"]}};
    var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
    </script>
    
  </body>
</html>