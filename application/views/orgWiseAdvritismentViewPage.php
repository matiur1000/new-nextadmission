<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/font-awesome.min.css'); ?>" rel="stylesheet">
  
  <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">

	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
	
    <!--CUSTOM BODY-->
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	
	<link href="<?php echo base_url('resource/styleNew.css'); ?>" rel="stylesheet">
	
</head>
  <body>
	<div style="box-shadow: 0px 1px 5px 3px rgba(50, 50, 50, .8); box-sizing: border-box;" class="container">
		<div class="row">
			<!-- Header Area-->
			<div class="header_area">
				<div class="container-fluid">
					<?php $this->load->view('adDetailsHeader'); ?>
				</div>
			</div>
			<!-- /Header Area-->                
	          
			<section class="org_banner">
				<div class="container-fluid">
		        	<div class="row">
		    			<div class="col-md-12">
							<div class="row" style="padding-bottom:5px;">
								<?php $this->load->view("organizationwebannerPage"); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--my top code close here-->   
		
			<!--my body code start here-->  
		    <div class="row">
		    	<div class="col-md-12">
					<div class="row">
						<!--Content Area & Sidebar-->
						<div class="col-md-10">
							
							<div class="row" style="padding:10px;">
								<div class="col-md-6">
									<div class="thumbnail">
										<?php if(!empty($recentPhotoInfo->add_image)){?> 
											<img src="<?php echo base_url("Images/Add_image/" . $recentPhotoInfo->add_image); ?>" style="height:250px; width:600px">
										
										<?php } else {?> 
											
											<div class="embed-responsive embed-responsive-16by9">
								 				<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $recentPhotoInfo->video_embed; ?>?rel=0&amp;showinfo=0" allowfullscreen></iframe>
											</div>
										<?php }?>
									</div>
								</div>

								<div class="col-md-6">
								 	<div class="thumbnail">
										<?php if(!empty($videoInfo)){?> 
										
											<?php foreach($videoInfo as $v)
									{
										$orgembcode = $v->video_embede_code;
										$pieces = explode(" ", $v->video_embede_code);
										$pieces[3]; 
									?> 
										<?php if(!empty($v->video_embede_code)) {?>
											<div>
												<iframe width="450" height="250" <?php echo $pieces[3]; ?> frameborder="0" allowfullscreen></iframe>
											</div> 
										<?php } else {?>
																			
											<div class="embed-responsive embed-responsive-16by9">
							 					<iframe class="embed-responsive-item" src="<?php echo base_url("resource/video_upload/" . $v->video); ?>" allowfullscreen></iframe>
											</div>
										
										 	<?php }?>
										
										<?php }?>
									
										<?php } else {?> 
											<img src="<?php echo base_url("Images/Add_image/NotAvailable.gif"); ?>" style="max-height:250px;">
										<?php }?>
									</div>
								</div>
							</div>
							
							<!--Social Media-->
							<div class="social_media_adp">
									<div id="share-buttons">
									    <!-- Facebook -->
										
										<?php
										$adId    = $recentPhotoInfo->id;
										$organizeId = $organizeId;
										
										?>
										
									    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('home/orgWiseAdvritismentView/'.$adId.'/'.$organizeId);?>&t=<?php echo $recentPhotoInfo->title;?>); ?>"
										   onclick="javascript:window.open(this.href, '<?php echo site_url('home/orgWiseAdvritismentView/'.$adId.'/'.$organizeId);?>', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Share on Facbook" class="kk-share-button kk-share-via-facebook">
										    <img src="<?php echo base_url("resource/images/facebook.png"); ?>" alt="Facebook" style="max-height:20px;"/>
							    		</a>

									   <!--  <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://nextadmission.com/home/orgWiseAdvritismentView/10/11.html&amp;src=sdkpreparse">Share</a> -->
									    
									    <!-- Google+ -->
									    <a href="https://plus.google.com/share?url={<?php echo site_url('home/orgWiseAdvritismentView/'.$adId, $organizeId);?>}" onClick="javascript:window.open(this.href,
						 					 '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share on Google" class="kk-share-button kk-share-via-google">
									       <img src="<?php echo base_url("resource/images/google.png"); ?>" alt="Google" style="max-height:20px;"/>	
									    </a>
									    <!-- Twitter -->
										<a href="https://twitter.com/share?url=<?php echo site_url('home/orgWiseAdvritismentView/'.$adId, $organizeId);?>&via=<?php  echo $recentPhotoInfo->title; ?>"
										   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
										   target="_blank" title="Share on Twitter" class="kk-share-button kk-share-via-twitter"><img src="<?php echo base_url("resource/images/twitter.png"); ?>" alt="Twitter" style="max-height:20px;"/>
										</a>
									</div>
								</div>
							<!--/Social Media-->
							
							<div class="clearfix"></div>
							
							
							
							<!-- Content Area -->
							<div class="content_area">
								<div class="content" style="border:1px solid #CCCCCC; min-height:450px;">
									<div style="text-align:justify;">
									 	<?php echo $recentPhotoInfo->description; ?>
								    </div>	
								</div>
							</div>
							<!-- /Content Area -->
							<!-- Prevous page Area -->
							<div class="previous_area">
								<?php $this->load->view("previousAdvertisementPage"); ?>
							</div>
							<!-- /Prevous page Area -->
						</div>
						<!--/Content Area & Sidebar-->
						
						<!--Right sidebar Area-->
						<div class="col-md-2">
							<div class="right_sidebar">
								<?php $this->load->view('orgRightSidebarPage'); ?>
							</div>
						</div>
						<!--/Right sidebar Area-->
					</div>
		    	</div>

				<div class="col-md-12">
					<?php $this->load->view('footerPage'); ?>
				</div>
			</div>

		 
			<!-- Modal -->
			<div class="modal fade"  id="myModalorg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				 
				</div>
			  </div>
			</div>
						
			<div class="modal fade"  id="myModalallDetailorg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
				 
				</div>
			  </div>
			</div>
		</div>
	</div>
	<!-- End main rapper -->
	 
	 <script>
	 	$('.deteail').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#myModalorg .modal-content").html(data);
					$("#myModalorg").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#myModalorg").modal('show');
				}
			});
			e.preventDefault();
		});
		
		
		$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
		
	 </script>
	 
	 
	  <script>
	 	$('.alldetail').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#myModalallDetailorg .modal-content").html(data);
					$("#myModalallDetailorg").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#myModalallDetailorg").modal('show');
				}
			});
			e.preventDefault();
		});
	 </script>
	 	 
	<?php $this->load->view("orgjuqeymenu"); ?>
  </body>
</html>