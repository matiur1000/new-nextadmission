<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script> 
  
	<div class="container">
		<div class="col-md-2">
			<div class="row">
				<!-- <div class="logo">
				<?php echo base_url('Images/Add_image/img.png') ?>
					<a data-toggle="modal" href="#orgLogo"><img src="#"  height="100" width="190" alt="Upload your org logo" /></a>
				</div> -->
			
				<div class="logo">
					<a href="#"><img style="width:160px;" src="<?php echo base_url("Images/Register_image/" . $viewAdInfo->image); ?>"  height="100" alt="University  Logo" style="height:100px;"></a>
				</div>
			</div>
		</div>

	  <!-- Modal -->
	  <div class="modal fade" id="orgLogo" role="dialog">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Upload your Organization Logo</h4>
	        </div>
				<!-- <?php //echo base_url('home/update_logo')?> -->
			
			<form action="#" method="post" enctype="multipart/form-data">
		        <div class="modal-body">
		        	<input type="file" title = "Upload Image" name="ogr_logo" id="org_logo"/>
		        </div>

		        <div class="modal-footer">
		          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		          <button type="submit" class="btn btn-sm btn-primary" data-dismiss="modal">Submit</button>
		        </div>
		    </form>

	      </div>
	    </div>
	  </div> <!--End Modal -->

		<div class="col-md-8" style="background: #4496EA;">
			<div class="org_webbanner">
				<div class="titleorgin"><?php echo $viewAdInfo->name; ?></div>
				<div class="apply_for_admission">
					<button style="color:red;" type="button" class="btn btn-info" data-toggle="modal" data-target="#mymodqladmin">Apply for Admission</button>
				</div>
			</div>
		</div>

		<div class="banner_latestN col-md-2" style="background:#01AECC;">
		  	<h2>Latest News</h2>
		</div>
		<!--Start Admission Form Modal-->
		<div class="modal fade" id="mymodqladmin" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">Online Admission Form</h4>
				      </div>
				      <div class="modal-body">
				    	<header class="head_sec">
							<div class="container-fluid">
							
								<div class="fomrlogo" align="center">
									<img src="<?php echo base_url('resource/logo/logo.jpg'); ?>" alt="logo" style="height:80px;">
								</div>
								
								<div class="title">
									<h2>Global Star Ltd.</h2>
									<p>Admission Form</p>
								</div>
							</div>
						</header>
						<section class="admission_form">
							<div class="container-fluid">
								<form action="<?php echo site_url('home/online'); ?>" method="post">
								
									<div class="row">
									  <div class="col-md-4">
										<input type="text" name="sur_name" id="sur_name" placeholder="Sur Name" class="form-control">
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="given_name" id="given_name" placeholder="Given Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="passport_no" id="passport_no" placeholder="Passport No." class="form-control" >
									  </div>
									</div>
									
									<div class="row" style="padding-bottom:7px; padding-top:7px;">
									  <div class="col-md-4">
										<input type="text"  name="fathers_name" id="fathers_name" placeholder="Father's Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="mothers_name" id="mothers_name" placeholder="Mother's Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="std_email" id="std_email" placeholder="E-mail" class="form-control" >
									  </div>
									</div>
									
									<div class="row">
									  <div class="col-md-4">
										<input type="text"  name="std_mobile" id="std_mobile" placeholder="Mobile No." class="form-control" requied>
									  </div>
									  <div class="col-md-4" style="z-index:10000">
										<input type="text" class="form-control date-picker" name="date_of_birth" id="date_of_birth" placeholder="Date of Birth" aria-describedby="basic-addon1" tabindex="5" data-date-format="yyyy-mm-dd" required>
									  </div>
									  <div class="col-md-4">
										<input type="text"  name="national_id" id="national_id" placeholder="National ID No. (if any)" class="form-control" >
									  </div>
									</div>
									<div class="row" style="padding-top:7px; padding-bottom:7px;">
									  <div class="col-md-3">
										<input type="text"  name="religion" id="religion" placeholder="Religion" class="form-control" >
									  </div>
									  <div class="col-md-3">
										<select id="marital_status" name="marital_status" class="form-control" required>
										  <option class="selected" value="" selected="" >Marital Status</option>
										  <option value="Single">Single</option>
										  <option value="Married">Married</option>
										  <option value="Divorced">Divorced</option>
										</select>
									  </div>
									  <div class="col-md-3">
										<input name="spouse_name" type="text" id="spouse_name" placeholder="Spouse Name" class="form-control" >
									  </div>
									  <div class="col-md-3">
										<input name="spouse_mobile" type="text" id="spouse_mobile" placeholder="Mobile No." class="form-control" >
									  </div>
									</div>
									<div class="row">
									  <div class="col-md-4">
										<input name="interested_country" type="text" id="interested_country" placeholder="Interested Country" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="interested_institute" type="text" id="interested_institute" placeholder="Interested Institute" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="interested_program_name" type="text" id="interested_program_name" placeholder="Interested Program Name" class="form-control" >
									  </div>
									</div>
									<div class="row" style="padding-top:7px; padding-bottom:7px;">
									  <div class="col-md-6">
										<textarea name="present_address" id="present_address" rows="2" cols="50" class="form-control" placeholder="Present Address..."></textarea>
									  </div>
									  <div class="col-md-6">
									<textarea name="permanent_address" rows="2" cols="50" id="permanent_address" class="form-control" placeholder="Permanent Address..."></textarea>
									  </div>
									</div>

									<h4>Father's Occupation Details:</h4>
									<div class="row">
									  <div class="col-md-4">
										<input name="guardian_name" type="text" id="guardian_name" placeholder="Name" class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="guardian_mobile_no" type="text" id="guardian_mobile_no" placeholder="Mobile No." class="form-control" >
									  </div>
									  <div class="col-md-4">
										<input name="guardian_designation" type="text" id="guardian_designation" placeholder="Designation" class="form-control" >
									  </div>
									</div>
									<div class="row" style="padding-bottom:7px; padding-top:7px;">

										<div class="col-md-6">
											<input name="guardian_ogranization_name" type="text" id="guardian_ogranization_name" placeholder="Ogranization Name/Office name (if any)" class="form-control" >
										</div>
									  <div class="col-md-6">
									  	<textarea name="guardian_address" id="guardian_address" rows="2" cols="50" placeholder="Address" class="form-control" ></textarea>
									  </div>
									</div>


									<h4>Reference in Bangladesh:</h4>
									<div class="row">
										<div class="col-md-4">
											<input name="employee_name" type="text" id="employee_name" placeholder="Name" class="form-control" >
										</div>
										<div class="col-md-4">
											<input name="employee_mobile_no" type="text" id="employee_mobile_no" placeholder="Mobile No." class="form-control" >
										</div>
										<div class="col-md-4">
										  	<textarea name="employee_address" id="employee_address" rows="2" cols="50" placeholder="Address" class="form-control" ></textarea>
										</div>
									</div>

									<div class="academic_qualification table-responsive">
										<h4>Academic Qualification:</h4>
										<table class="table table-striped table-bordered table-condensed table-hover text-center">
										    <thead>
										      <tr>
										        <th>Degree</th>
										        <th>Institute Name</th>
										        <th>Group</th>
										        <th>Result</th>
										        <th>Year</th>
										      </tr>
										    </thead>
										    <tbody>
										      <tr>
										      	<td>
													<select name="ssc_ol" id="ssc_ol" class="form-control" required>
													  <option value="" selected="" >Select one</option>
													  <option value="SSC">SSC</option>
													  <option value="OL">OL</option>
													  <option value="Diploma">Diploma</option>
													</select>
												</td>
										     <td><input name="ssc_institute" type="text" id="ssc_institute" placeholder="Write institute name..." class="form-control" ></td>
										      <td style="display:none;"><input type="text" id="ssc_group" name="ssc_group" placeholder="Write group name..." class="form-control" ></td>
										      <td>
													<select name="ssc_group" id="ssc_group" class="form-control" required>
													  <option value="" selected="" >Select Group</option>
													  <option value="Science">Science</option>
													  <option value="Humanities">Humanities</option>
													  <option value="Commerce">Commerce</option>
													</select>
												</td>
										      	<td><input type="text" id="ssc_result" name="ssc_result" placeholder="Result..." class="form-control" ></td>
										        <td style="display:none;"><input type="text" id="ssc_year" name="ssc_year" placeholder="Year..." class="form-control" ></td>
									      		<td>
													<select name="ssc_year" id="ssc_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>

										      </tr>
										      <tr>
										      	<td>
													<select name="hsc_al" name="hsc_al" class="form-control" required>
													  <option value="" selected="" >Select one</option>
													  <option value="HSC">HSC</option>
													  <option value="Diploma">Diploma</option>
													  <option value="AL">AL</option>
													</select>
												</td>
										        <td><input  name="hsc_institute" id="hsc_institute" type="text" id="" placeholder="Write institute name..." class="form-control" ></td>
										        <td style="display:none"><input  name="hsc_group" type="text" id="hsc_group" placeholder="Write group name..." class="form-control" ></td>
										        <td>
													<select name="hsc_group" id="hsc_group" class="form-control" required>
													  <option value="" selected="" >Select Group</option>
													  <option value="Science">Science</option>
													  <option value="Humanities">Humanities</option>
													  <option value="Commerce">Commerce</option>
													</select>
												</td>
										        <td><input  name="hsc_result" type="text" id="hsc_result" placeholder="Result..." class="form-control" ></td>
										        <td style="display:none;"><input  name="hsc_year" type="text" id="datetimepicker10" placeholder="Year..." class="form-control" ></td>
										        <td>
													<select name="hsc_year" id="hsc_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>
										      </tr>
										      <tr>
										        <td>Bachelor</td>
										   		<td><input  name="bachelor_institute" type="text" id="bachelor_institute" placeholder="Write Institute/University name" class="form-control" ></td>
										        <td><input  name="bachelor_group" type="text" id="bachelor_group" placeholder="Write Subject" class="form-control" ></td>
										        <td><input  name="bachelor_result" type="text" id="bachelor_result" placeholder="Result..." class="form-control" ></td>
										        <td style="display:none"><input  name="bachelor_year" type="text" id="datetimepicker10" placeholder="Year..." class="form-control" ></td>
										        <td>
													<select  name="bachelor_year" id="bachelor_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>
										    </tr>
										    <tr>
											    <td>Masters</td>
											    <td><input name="masters_institute" type="text" id="masters_institute" placeholder="Write Institute/University name" class="form-control" ></td>
											    <td><input name="masters_group" type="text" id="masters_group" placeholder="Write Subject" class="form-control" ></td>
											    <td><input name="masters_result" type="text" id="masters_result" placeholder="Result..." class="form-control" ></td>
											    <td style="display:none"><input name="masters_year" type="text" id="datetimepicker10" placeholder="Year..." class="form-control" ></td>
											    <td>
													<select name="masters_year" id="masters_year" class="form-control" required>
														<option class="selected" value="" selected="" >Select a year</option>
														<option value="2000">1995</option>
														<option value="2000">1996</option>
														<option value="2000">1997</option>
														<option value="2000">1998</option>
														<option value="2000">1999</option>
														<option value="2000">2000</option>
														<option value="2001">2001</option>
														<option value="2002">2002</option>
														<option value="2003">2003</option>
														<option value="2004">2004</option>
														<option value="2005">2005</option>
														<option value="2006">2006</option>
														<option value="2007">2007</option>
														<option value="2008">2008</option>
														<option value="2009">2009</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
														<option value="2015">2015</option>
														<option value="2016">2016</option>
														<option value="2017">2017</option>
														<option value="2018">2018</option>
														<option value="2019">2019</option>
														<option value="2020">2020</option>
														<option value="2021">2021</option>
														<option value="2022">2022</option>
														<option value="2023">2023</option>
														<option value="2024">2024</option>
														<option value="2025">2025</option>
														<option value="2026">2026</option>
														<option value="2027">2027</option>
														<option value="2028">2028</option>
														<option value="2029">2029</option>
														<option value="2030">2030</option>
													</select>
												</td>
										      </tr>
										    </tbody>
										</table>
									</div>

									<div class="row">
									  <div class="col-md-6">
										<input style="display:none" type="text" name="other_proficiency" id="other_proficiency" placeholder="Other Proficiency." class="form-control" >
										<select name="other_proficiency" id="other_proficiency" class="form-control" required>
											<option class="selected" value="" selected="" >Select a Proficiency</option>
											<option value="IELTS">IELTS</option>
											<option value="GRE">GRE</option>
											<option value="TOEFL">TOEFL</option>
											<option value="ESL">ESL</option>
											<option value="GMAT">GMAT</option>
											<option value="LSAT">LSAT</option>
											<option value="Other">Other</option>
										</select>
									  </div>
									</div>

									<div><h4>How did you know about us?</h4></div>
									
									<div class="row">
										<div class="col-md-6">
											<select onchange="genderSelectHandler(this)" name="media_source" id="media_source" class="form-control" required>
												<option class="selected" value="" selected="" >Select a source</option>
												<option value="Social Media">Social Media</option>
												<option value="News Paper">News Paper</option>
												<option value="Institute">Institute</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="col-md-6">
											<select style="display:none" name="socialmedia_source" id="socialmedia_source" class="form-control">
												<option class="selected" value="" selected="" >Which social media?</option>
												<option value="Facebook">Facebook</option>
												<option value="Twitter">Twitter</option>
												<option value="Linkedin">Linkedin</option>
												<option value="Youtube">Youtube</option>
												<option value="Google+">Google+</option>
												<option value="Other">Other</option>
											</select>
										</div>
									  	
									  	<div class="col-md-6">
											<select style="display:none" name="newspaper_source" id="newspaper_source" class="form-control">
												<option class="selected" value="" selected="" >Which Newspaper?</option>
												<option value="Naya Diganta">Naya Diganta</option>
												<option value="Jugantor">Jugantor</option>
												<option value="Bangladesh Pratidin">Bangladesh Pratidin</option>
												<option value="Prothom Alo">Prothom Alo</option>
												<option value="Ittefaq">Ittefaq</option>
												<option value="Bhorer Kagoj">Bhorer Kagoj</option>
												<option value="Janakantha">Janakantha</option>
												<option value="Amader Shomoy">Amader Shomoy</option>
												<option value="Other">Other</option>
											</select>
										</div>

										<div class="col-md-6">
											<input style="display:none" type="text" id="instiute_source" name="instiute_source" placeholder="Write Institute/University name" class="form-control" >
										</div>

										<div class="col-md-6">
											<input style="display:none" type="text" id="other_source" name="other_source" placeholder="Write a source" class="form-control" >
										</div>
									</div>

									<div class="row counceling">
										<div class="col-md-4">
											<select name="phoneed_by" id="phoneed_by" class="form-control" required>
												<option class="selected" value="" selected="" >Phoned By:</option>
												<option value="Shamme Ferdaus">Shamme Ferdaus</option>
												<option value="Shamima Chowdhury Keya">Shamima Chowdhury Keya</option>
												<option value="Shamima Chowdhury Keya">Nusrat Majumder</option>
												<option value="Romana Remi">Romana Remi</option>
												<option value="Zehan">Zehan</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<select name="counseled_by" id="counseled_by" class="form-control" required>
												<option class="selected" value="" selected="" >Counseled By:</option>
												<option value="Shamme Ferdaus">Shamme Ferdaus</option>
												<option value="Shamima Chowdhury Keya">Shamima Chowdhury Keya</option>
												<option value="Nusrat Majumder">Nusrat Majumder</option>
												<option value="Romana Remi">Romana Remi</option>
												<option value="Zehan">Zehan</option>
												<option value="Other">Other</option>
											</select>
										</div>
									  	
									  	<div class="col-md-4">
											<select name="visa_by" id="visa_by" class="form-control" required>
												<option class="selected" value="" selected="" >Visa Processed By:</option>
												<option value="Shamme Ferdaus">Shamme Ferdaus</option>
												<option value="Shamima Chowdhury Keya">Shamima Chowdhury Keya</option>
												<option value="Nusrat Majumder">Nusrat Majumder</option>
												<option value="Romana Remi">Romana Remi</option>
												<option value="Zehan">Zehan</option>
												<option value="Other">Other</option>
											</select>
										</div>
									</div>

									<div class="row" style="margin-top:50px;">
									  <div class="col-md-12 checkbox">
									  	<label for="signchieckbox">
											<input value="ok" name="signchieckbox" type="checkbox" id="signchieckbox" required>I am the under signed, hereby declare that I will be responsible for Warning information provided here
									  	</label>
									  </div>
									</div>
									<div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								        <button type="submit" class="btn btn-success">Submit</button>
								    </div>
								</form>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
  
<!--End Admission Form Modal-->

<script>
	/*$('.org_logo').on('click', function(){
		var value 	= $('#org_logo').val();
		var formURL = "<?php echo site_url('organizationUserHome/fff'); ?>";
		
		$.ajax({
			url: formURL, 
			type: "POST",             
			data: {org_logo : value}, 
			contentType: false,       
			cache: false,             
			processData:false,       
			success: function(data)   
			{
				location.reload();
			}
			alert("I am not ready");
		});
		//alert("I am not ready");
	});*/

	$('.date-picker').datepicker({
		autoclose: true	  
	}); 

	$('#datetimepicker10').datepicker({
		autoclose: true	 
	});

	$("#marital_status, #counseled_by, #phoned_by, #visa_by, #masters_year, #bachelor_year, #hsc_year, #ssc_year, #media_source, #socialmedia_source, #newspaper_source").click(function(){
    $(".selected").hide();
});

///Media Source
function genderSelectHandler(select){
	if(select.value == "Social Media"){
		$("#socialmedia_source").show(); 
	}else{
		$("#socialmedia_source").hide();
	}

	if(select.value == "News Paper"){
		$("#newspaper_source").show(); 
	}else{
		$("#newspaper_source").hide();
	}

	if(select.value == "Institute"){
		$("#instiute_source").show(); 
	}else{
		$("#instiute_source").hide();
	}

	if(select.value == "Other"){
		$("#other_source").show(); 
	}else{
		$("#other_source").hide();
	}
}
</script>