<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Online Form</title>

    <!--bootstrap link -->
	<link href="http://nextadmission.com/resource/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://nextadmission.com/resource/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="<?php echo base_url("resource/slide_css/responsiveslides.css"); ?>" rel="stylesheet">
	
	
 <script>
    // You can also use "$(window).load(function() {"
    $(function () {

      // Slideshow 1
      $("#slider1").responsiveSlides({
        maxwidth: 800,
        speed: 800,
		
      });
	  
	  $("#slider2").responsiveSlides({
        maxwidth: 800,
        speed: 2500,
      });
	  
	   $("#slider3").responsiveSlides({
        maxwidth: 800,
        speed: 3500,
      });
	  
	    $("#slider4").responsiveSlides({
        maxwidth: 800,
        speed: 3500,
      });


    });
  </script>
    <style>
    	  body{ background:background-color: #2F2727; background-image: url(images/radial_bg.png); background-position: center center; background-repeat: no-repeat; /* Safari 4-5, Chrome 1-9 */ /* Can't specify a percentage size? Laaaaaame. */ background: -webkit-gradient(radial, center center, 0, center center, 460, from(#1a82f7), to(#2F2727)); /* Safari 5.1+, Chrome 10+ */ background: -webkit-radial-gradient(circle, #1a82f7, #2F2727); /* Firefox 3.6+ */ background: -moz-radial-gradient(circle, #1a82f7, #2F2727); /* IE 10 */ background: -ms-radial-gradient(circle, #1a82f7, #2F2727); /* Opera couldn't do radial gradients, then at some point they started supporting the -webkit- syntax, how it kinda does but it's kinda broken (doesn't do sizing) */}
		  
		.form-horizontal .form-group {
			margin-right: -268px;
			margin-left: -15px;
		} 
		
		@font-face {
			font-family: 'Conv_SolaimanLipi';
			src: url('<?php echo base_url("resource/fonts/SolaimanLipi_22-02-2012.eot"); ?>');
			src: local('☺'), url('<?php echo base_url("resource/fonts/SolaimanLipi_22-02-2012.woff"); ?>') format('woff'), url('<?php echo base_url("resource/fonts/SolaimanLipi_22-02-2012.ttf"); ?>') format('truetype'), url('<?php echo base_url("resource/fonts/SolaimanLipi_22-02-2012.svg"); ?>') format('svg');
			font-weight: normal;
			font-style: normal;
		}
		
	@font-face {
		font-family: 'Conv_kalpurush';
		src: url('<?php echo base_url("resource/fonts/kalpurush.eot"); ?>');
		src: local('☺'), url('<?php echo base_url("resource/fonts/kalpurush.woff"); ?>') format('woff'), url('<?php echo base_url("resource/fonts/kalpurush.ttf"); ?>') format('truetype'), url('<?php echo base_url("resource/fonts/kalpurush.svg"); ?>') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	.chk{
		color:red;
	}

	.moble_name{
		position:relative;
	}
	.moble_name input{
	
	}
	.moble_name span{
		position:absolute;
		left:-2px;
		font-size:25px;
		top:4px;
	}
	.form-control {
	    display: block;
	    width: 100%;
	    height: 34px;
	    padding: 6px 6px;
	    font-size: 14px;
	    line-height: 1.42857143;
	    color: #555;
	    background-color: #fff;
	    background-image: none;
	    border: 1px solid #ccc;
	    border-radius: 4px;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	}
		 
	</style>
	
		
  </head>
  <body width="100%" height="100%">
	<div class="container-fluid text-center" width="100%" height="100%">
		<div class="row"> <!--Start row one-->
			
			<div class="col-sm-3">
				<div style="padding: 20px; background:#fff; max-height:667px; overflow:auto; border: 5px solid rgb(5, 86, 175);
					border-radius: 10px;">

						<div class="ad_registration_notice" height="100%">
							<p style="text-align:justify;"><?php echo $notices->text_detail; ?></p>
							<p>&nbsp;</p>
				
						</div>

				</div>
			</div> <!--End Col one-->
			
			<div class="col-sm-6">
				<div style=" padding-bottom:135px; background:#fff; border: 5px solid rgb(5, 86, 175);
					border-radius: 10px;">
					<div style="">
            			<h3 style="color:#11c0f9; line-height: 50px; font-size: 40px; padding: 2px 10px; text-shadow: 2px 2px #000 !important;">NextAdmission.Com</h3>
						<h4 class="blink" style="text-shadow: 2px 1px 3px black;
   color:#FF0000;">Please Fill the Enquire Form Below</h4>
					</div>
						<div class="ad_registration_form" style="max-width:500px; margin:0 auto; font-family:Conv_kalpurush;">
							<form class="form-horizontal" action="<?php echo site_url('form/store'); ?>" method="post" enctype="multipart/form-data">
								  <div class="form-group">
									<div class="col-sm-8">
										<span style="font-size:14px; color:#0000FF"></span>
									</div>
								  </div>
								  <div class="form-group">
									
									<div class="col-sm-8 moble_name">
									  <span style="color:red;">*</span>
									  <input type="text" class="form-control" name="name" placeholder="Name/নাম?" required>
									</div>
								  </div>
								  
								  <div class="form-group">
								
									<div class="col-sm-8 moble_name">
									  <span style="color:red;">*</span>
									  <input type="text" class="form-control" id="mobile" name="mobile" style="margin-top:5px;" placeholder="Mobile/মোবাইল?" required>
									  <div style="margin-top:5px;" class="chk"></div>
									</div>
								  </div>

								  <div class="form-group">
								
									<div class="col-sm-8">
										<input type="text" class="form-control" name="country" placeholder="Interested country to go/কোন দেশে যাওয়ার ইচ্ছা?">
									</div>
								  </div>
								  <div class="form-group">
									
									<div class="col-sm-8">
									  <input type="text" class="form-control" name="purpose" placeholder="Study or other reason/পড়াশোনা বা অন্য কোন কারণ?">
									</div>
								  </div>

								 
								  <div class="form-group">
									
									<div class="col-sm-8">
									   <input type="text" class="form-control" name="course" placeholder="Interested Course/পছন্দের কোর্স?" >									</div>
								  </div>
								  

								  <div class="form-group">
									<div class="col-sm-3">
										<select id="city" name="city" onchange="citySelectHandler(this)" class="form-control" required>
											<option class="selected" value="" selected="" >What is your division?/আপনার বিভাগের নাম কি?</option>
											<option value="Dhaka">Dhaka</option>
											<option value="Chittagong">Chittagong</option>
											<option value="Sylhet">Sylhet</option>
											<option value="Rangpur">Rangpur</option>
											<option value="Rajshahi">Rajshahi</option>
											<option value="Khulna">Khulna</option>
											<option value="Barisal">Barisal</option>
											<option value="Mymensingh">Mymensingh</option>
										</select>
									</div>

									<div class="col-sm-5">
										<select id="dhaka_dist" name="dhaka_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Dhaka District">Dhaka District</option>
											<option value="Faridpur District">Faridpur District</option>
											<option value="Gazipur District">Gazipur District</option>
											<option value="Gopalganj District">Gopalganj District</option>
											<option value="Kishoreganj District">Kishoreganj District</option>
											<option value="Madaripur District">Madaripur District</option>
											<option value="Manikganj District">Manikganj District</option>
											<option value="Munshiganj District">Munshiganj District</option>
											<option value="Narayanganj District">Narayanganj District</option>
											<option value="Narsingdi District">Narsingdi District</option>
											<option value="Rajbari District">Rajbari District</option>
											<option value="Shariatpur District">Shariatpur District</option>
											<option value="Tangail District">Tangail District</option>
										</select>
									
										<select style="display:none" id="chittagong_dist" name="chittagong_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Bandarban District">Bandarban District</option>
											<option value="Brahmanbaria District">Brahmanbaria District</option>
											<option value="Chandpur District">Chandpur District</option>
											<option value="Chittagong District">Chittagong District</option>
											<option value="Comilla District">Comilla District</option>
											<option value="Cox's Bazar District">Cox's Bazar District</option>
											<option value="Feni District">Feni District</option>
											<option value="Khagrachhari District">Khagrachhari District</option>
											<option value="Lakshmipur District">Lakshmipur District</option>
											<option value="Noakhali District">Noakhali District</option>
											<option value="Rangamati District">Rangamati District</option>
										</select>
									
										<select style="display:none" id="sylhet_dist" name="sylhet_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Habiganj District">Habiganj District</option>
											<option value="Moulvibazar District">Moulvibazar District</option>
											<option value="Sunamganj District">Sunamganj District</option>
											<option value="Sylhet District">Sylhet District</option>
										</select>
									
										<select style="display:none" id="rangpur_dist" name="rangpur_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Dinajpur District">Dinajpur District</option>
											<option value="Gaibandha District">Gaibandha District</option>
											<option value="Kurigram District">Kurigram District</option>
											<option value="Lalmonirhat District">Lalmonirhat District</option>
											<option value="Nilphamari District">Nilphamari District</option>
											<option value="Panchagarh District">Panchagarh District</option>
											<option value="Rangpur District">Rangpur District</option>
											<option value="Thakurgaon District">Thakurgaon District</option>
										</select>
									
										<select style="display:none" id="rajshahi_dist" name="rajshahi_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Bogra District">Bogra District</option>
											<option value="Joypurhat District">Joypurhat District</option>
											<option value="Naogaon District">Naogaon District</option>
											<option value="Natore District">Natore District</option>
											<option value="Chapainawabganj District">Chapainawabganj District</option>
											<option value="Pabna District">Pabna District</option>
											<option value="Rajshahi District">Rajshahi District</option>
											<option value="Rajshahi District">Sirajganj District</option>
										</select>
									
										<select style="display:none" id="khulna_dist" name="khulna_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Bagerhat District">Bagerhat District</option>
											<option value="Chuadanga District">Chuadanga District</option>
											<option value="Jessore District">Jessore District</option>
											<option value="Jhenaidah District">Jhenaidah District</option>
											<option value="Khulna District">Khulna District</option>
											<option value="Kushtia District">Kushtia District</option>
											<option value="Magura District">Magura District</option>
											<option value="Meherpur District">Meherpur District</option>
											<option value="Narail District">Narail District</option>
											<option value="Satkhira District">Satkhira District</option>
										</select>
									
										<select style="display:none" id="barisal_dist" name="barisal_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Barguna District">Barguna District</option>
											<option value="Barisal District">Barisal District</option>
											<option value="Bhola District">Bhola District</option>
											<option value="Jhalokati District">Jhalokati District</option>
											<option value="Patuakhali District">Patuakhali District</option>
											<option value="Pirojpur District">Pirojpur District</option>
										</select>
									
										<select style="display:none" id="mymensingh_dist" name="mymensingh_dist" class="form-control">
											<option class="selected" value="" selected="" >What is your district?/আপনার জেলার নাম কি?</option>
											<option value="Jamalpur District">Jamalpur District</option>
											<option value="Mymensingh District">Mymensingh District</option>
											<option value="Netrakona District">Netrakona District</option>
											<option value="Sherpur District">Sherpur District</option>
										</select>
									</div>
								  </div>

								  <div class="form-group">
								
									<div class="col-sm-8">
									   <input type="text" class="form-control" id="education" name="education" placeholder="Education Qualification/শিক্ষাগত যোগ্যতা?" >
									</div>
								  </div>
								  
								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-8">
									  <button type="submit" class="btn btn-success submit" name="submit" style="margin-left: 85px; margin-top: 30px;">Submit</button>
									</div>
								  </div>
							</form>      
						</div>

				</div>
			</div>
			
			<div class="col-sm-3 col-md-3 col-lg-3">
				<div style="padding:30px 20px 30px; background:#fff;  max-height:667px; overflow:auto; border: 5px solid rgb(5, 86, 175);
					border-radius: 10px;">
						<div class="ad_registration_gallery">
							<div class="row">
								<div class="col-md-12">
									
									 
								    <ul class="rslides" id="slider1">
									 
									 <?php if(!empty($formslide1->sl_image1)){?>
									 <li><img src="<?php echo base_url("Images/admission_form_image/".$formslide1->sl_image1); ?>" alt="" style="height:120px;"></li>
									 <?php }?>
									 
									  <?php if(!empty($formslide1->sl_image2)){?> 
									   <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide1->sl_image2); ?>" alt="" style="height:120px;"></li>
									   <?php }?>
									   
									   <?php if(!empty($formslide1->sl_image3)){?> 
									    <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide1->sl_image3); ?>" style="height:120px;"></li>
										<?php }?>
									  </ul>
									  
									  <hr />
									  
									  
									<ul class="rslides" id="slider2">
									 
									 <?php if(!empty($formslide2->sl_image1)){?>
									 <li><img src="<?php echo base_url("Images/admission_form_image/".$formslide2->sl_image1); ?>" alt="" style="height:120px;"></li>
									 <?php }?>
									 
									  <?php if(!empty($formslide2->sl_image2)){?> 
									   <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide2->sl_image2); ?>" alt="" style="height:120px;"></li>
									   <?php }?>
									   
									   <?php if(!empty($formslide2->sl_image3)){?> 
									    <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide2->sl_image3); ?>" style="height:120px;"></li>
										<?php }?>
									  </ul>
								
									  <hr />
									  
									<ul class="rslides" id="slider3">
									 
									 <?php if(!empty($formslide3->sl_image1)){?>
									 <li><img src="<?php echo base_url("Images/admission_form_image/".$formslide3->sl_image1); ?>" alt="" style="height:120px;"></li>
									 <?php }?>
									 
									  <?php if(!empty($formslide3->sl_image2)){?> 
									   <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide3->sl_image2); ?>" alt="" style="height:120px;"></li>
									   <?php }?>
									   
									   <?php if(!empty($formslide3->sl_image3)){?> 
									    <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide3->sl_image3); ?>" style="height:120px;"></li>
										<?php }?>
									  </ul>
									  
									  <hr />
									 
								   <ul class="rslides" id="slider4">
									 
									 <?php if(!empty($formslide4->sl_image1)){?>
									 <li><img src="<?php echo base_url("Images/admission_form_image/".$formslide4->sl_image1); ?>" alt="" style="height:120px;"></li>
									 <?php }?>
									 
									  <?php if(!empty($formslide4->sl_image2)){?> 
									   <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide4->sl_image2); ?>" alt="" style="height:120px;"></li>
									   <?php }?>
									   
									   <?php if(!empty($formslide4->sl_image3)){?> 
									    <li><img src="<?php echo base_url("Images/admission_form_image/" .$formslide4->sl_image3); ?>" style="height:120px;"></li>
										<?php }?>
									  </ul>
									  
									  
								</div>
							</div>
						</div>
						
	

				</div>
			</div> 
		

		</div><!--End Row one-->
		
	</div>

	<script src="<?php echo base_url('resource/assets/js/jquery.2.1.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url("resource/slide_css/responsiveslides.min.js"); ?>"></script>
	<script src="<?php echo base_url("resource/js/jquery.blink.jsnnn"); ?>"></script>

	<script type="text/javascript">

		$("#dhaka_dist, #chittagong_dist, #sylhet_dist, #rangpur_dist, #rajshahi_dist, #khulna_dist, #barisal_dist, #mymensingh_dist").click(function(){
	    	$(".selected").hide();
		});

		function citySelectHandler(select){
			if(select.value == "Dhaka"){
				$("#dhaka_dist").show(); 
			}else{
				$("#dhaka_dist").css({'display':'none'});
			}

			if(select.value == "Chittagong"){
				$("#chittagong_dist").show(); 
			}else{
				$("#chittagong_dist").css({'display':'none'});
			}

			if(select.value == "Sylhet"){
				$("#sylhet_dist").show(); 
			}else{
				$("#sylhet_dist").css({'display':'none'});
			}

			if(select.value == "Rangpur"){
				$("#rangpur_dist").show(); 
			}else{
				$("#rangpur_dist").css({'display':'none'});
			}

			if(select.value == "Rajshahi"){
				$("#rajshahi_dist").show(); 
			}else{
				$("#rajshahi_dist").css({'display':'none'});
			}
			
			if(select.value == "Khulna"){
				$("#khulna_dist").show(); 
			}else{
				$("#khulna_dist").css({'display':'none'});
			}
			
			if(select.value == "Barisal"){
				$("#barisal_dist").show(); 
			}else{
				$("#barisal_dist").css({'display':'none'});
			}
			if(select.value == "Mymensingh"){
				$("#mymensingh_dist").show(); 
			}else{
				$("#mymensingh_dist").css({'display':'none'});
			}

		}

		$(document).ready(function() {
			$("#mobile").on("keyup change", function(){
				var mobile = $(this).val();
				var formURL = "<?php echo base_url('form/mobile_check'); ?>";
				  $.ajax({
					url : formURL,
					type : "POST",
					data : { mobile_no : mobile},
					success : function(data){			
						if(data == 1){
							$(".chk").text("This user already Exists!!");
							$(".submit").attr('disabled','disabled');
							$(".chk").addClass("red");
						}else {
							$(".chk").text("");
							$(".chk").removeClass("red");
							$(".submit").removeAttr('disabled','disabled');
						}
					}
				 });
			});

			//alert("Hello! welcome");
		});		
	</script>
  </body>
</html>