<link href="<?php echo base_url('resource/style.css'); ?>" rel="stylesheet">
<div class="footer">
	  <div class="footer-grids">
		  <div class="container">
		      <div class="col-md-12 col-xs-12" style="padding:0 0 10px 12px; color: white">Video gallery </div>
              <?php 
                  foreach ($videoInfo as $v){ 
                  $pieces = explode(" ", $v->video_embede_code);
				  $pieces[3]; 
              ?>
			    <div class="col-md-3 col-xs-6 footer-grid">
			       <?php if(!empty($v->video)){ ?>
                       <video width="250" height="150" controls>
							<source src="<?php echo base_url("/resource/video_upload/$v->video"); ?>" type="video/mp4">
						</video> 
                        <?php }else { ?>
                        <iframe width="250" height="150" <?php echo $pieces[3]; ?> frameborder="0" allowfullscreen></iframe>
                        <?php } ?>
			    </div>
			  <?php } ?>
		 </div>
	 </div>
</div>
<div class="copy" style="background:#000000;">
		 <p style="color:#CCCCCC;"><?php echo $headerBasicInfo->title; ?></p>
 </div>