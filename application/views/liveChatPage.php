<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/chat_window_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				
			<div class="row">  <!--row Start-->
			   	<div class="col-lg-9">  <!--col 9 Start-->
					<div class="col-lg-3 padding-0">
						<?php $this->load->view('leftSidebarPage'); ?>
					</div>
													
					<div class="col-lg-9">
					   <form id="fullSearch" method="post" action="<?php echo site_url('home/searchAll') ?>">

							<div class="form-group margin-0">				
								<div class="input-group">
									<input type="text" name="search_all" id="search_all" class="form-control"  placeholder="Search by keyword">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit" id="search"><i class="glyphicon glyphicon-search"></i> Search</button>
									</span>												
							    </div><!-- /input-group -->		
							</div>	 
						    
						    <div id="advancedSearch" style="display:none">
								<div class="col-lg-4 padding-0">
									<div class="form-group margin-0">
										<select class="form-control" id="country_id" name="country_id"  tabindex="1">
											<option value="" selected>Select Country</option>
											<?php foreach ($countryInfo as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group margin-0">
										<select class="form-control" id="organize_id" name="organize_id"  tabindex="2" >
											<option value="" selected>Select Organization</option>
											<?php foreach ($orgInfo as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-lg-4 padding-0">
									<div class="form-group margin-0">
										<select class="form-control" id="program_id" name="program_id"  tabindex="3">
											<option value="" selected>Select Program</option>
											<?php foreach ($allProgramName as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->programe_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
						
					   </form>
						
						<p><a class="advanced">Advanced Search</a></p>
										

						<div id="searchOrganization" class="blog-post">
							<?php $this->load->view('middlePage'); ?>
						</div>
					</div>						   
		        </div>  <!--col 9 end--> 
				     
				<?php $this->load->view('rightSidebarPage'); ?>                 
				<!--col 3 end--> 
				

				<!-- live chat start -->
					<div class="chat_box">
					   <input type="hidden" name="chatHiddenId" id="chatHiddenId" value="">
					   <div id="chatBarShow">
						<div class="chat_head">Chat Box <?php if(!empty($totalActiveUser)){ ?>(<?php echo $totalActiveUser ?>) <?php } ?></span><span class="noImg" style="float:right;"><i style="cursor:pointer" class="minimusChat glyphicon glyphicon-minus"></i>&nbsp;<i style="cursor:pointer" class="removeChatBox glyphicon glyphicon-remove"></i></span><span class="imgAd" style="float:right;display:none"><span><img src="<?php echo base_url("resource/img/minimize.png"); ?>" style="max-height:11px;right:50" /></span>&nbsp;<i style="cursor:pointer" class="removeChatBox glyphicon glyphicon-remove"></i></span></div>
							<div class="panel-body chat_body">
						
								<?php 
								     if(!empty($activeUser)){
								      if(!empty($allUser)){
								     foreach($allUser as $v){ 
								?>
								 <a data-id="<?php echo $v->id ?>" class="chat" href="#">
								 <div id="chatRow" class="row">
									<div class="col-md-2" style="padding-left:5px;">
									  <?php if($v->image){ ?>
									   <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>"align="right" class="img-responsive" />
									   <?php }else{ ?>
										<img src="<?php echo base_url("resource/img/profile2.png"); ?>"align="right" class="img-responsive" />
										<?php } ?>
									  </div>
									<div class="col-md-10 user" style="padding:6px 0 0 6px;"><?php echo $v->name ?></div>
								  </div>
								  <div class="row" style="padding:3px"></div>
								  </a>
								<?php } }else{ ?>
								  <span style="color:#FF0000">Chat List Empty!</span>
								<?php } }else{  ?>
								  <span style="color:#FF0000">Please login to chat</span>
								<?php } ?>

								
							</div>
					    </div>
							<div class="input-group"><input name="search_user" id="search_user" type="text" class="form-control input-sm chat_input" placeholder="search by name" />
								<span class="input-group-btn">
								  <button id="searchUser" type="submit" class="btn btn-default  btn-sm" id="btn-chat">Search</button>
							   </span>
							</div>
					 </div>
				
				<!-- live chat end -->

				<div style="position: fixed; right:265px; bottom: 0; z-index:999999999999999;" id="chatMsgView"></div>
				 
				 
		  	</div>

             <div class="row">&nbsp;</div>     
              <div class="row">
              	<div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;">Advertisement</div>
              </div>  
			  <?php $this->load->view('advertisementManagePage'); ?>
             <div class="row">&nbsp;</div>     
              <div class="row">
			    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
			     <?php $this->load->view('newsEventManagePage'); ?>
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>

		//North America Effict
		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	//News scroll	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


       
	  //post modal	
		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});

		function addZero(i) {
		    if (i < 10) {
		        i = "0" + i;
		    }
		    return i;
		}

		// chat List remove

		$(document).on("click", ".removeChatBox", function(event){
			var parents = $(this).parents('.chat_box');
			parents.css("display", "none");
			console.log(parents);

		});


		//user wise chat message window

		$(document).on("click", ".chat", function(event){
			var id = $(this).attr('data-id');
			var formURL = "<?php echo site_url('liveChat/chatWiseDetail'); ?>";

			$.ajax({
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success : function(data) {	
				 //get current date time
	                var currentdate = new Date(); 
					var new_date = + currentdate.getFullYear() + "-"
		            + addZero(currentdate.getMonth()+1)+ "-" 
		            + addZero(currentdate.getDate());
	    			//console.log(new_date);
				 //current date time end 
				if ($("#chatMsgView").find("#msgViewId[data-id="+data.id+"]").length==0){	
				   
				   var element = '<div style="padding:0" id="msgViewId" class="container" data-id="'+ data.id +'">';
	                element = element + '<div class="row chat-window col-xs-5 col-md-3" style="bottom:0 !important;">';
	                element = element + '<div class="col-xs-12 col-md-12" style="padding-right:8px; bottom:0 !important;">';
	                element = element + '<div class="">';
	                element = element + '<div class="top-bar">';
	                element = element + '<div class="col-md-8 col-xs-8">';
	                if(data.online_status =='active'){
	                 element = element + '<h3 class="panel-title">'+ data.name +'</h3>';
	                } else {
	                  element = element + '<h4 style="font-size:12px;" class="panel-title">'+ data.name +'</h4>';
	                }
	                element = element + '</div>';
                    element = element + '<div class="col-md-4 col-xs-4" style="text-align: right; padding-right:0px">';
                    element = element + '<span class="maximux" style="padding-right:25px; margin-bottom:-16px;cursor:pointer;display:none">';
                    element = element + '<img src="<?php echo base_url("resource/img/minimize.png"); ?>" style="max-height:11px;right:50" />';
                    element = element + '</span>';
                    element = element + '<a href="#"><span class="removeIcon" style="padding-right:8px; color:#FFFFFF"><i class="glyphicon glyphicon-minus minusChatBox"></i></span></a>';
                    element = element + '<a href="#"><span style="color:#FFFFFF; right:0px"><i class="glyphicon glyphicon-remove"></i></span></a>';
                    element = element + '</div></div>';
                    element = element + '<div style="height:300px" class="panel-body msg_container_base">';
                     if(data.countMsg > 20){
	                    element = element + '<div class="oldMsg" style="padding-top:5px; cursor:pointer">See old message</div>'; 
	                    element = element + '<div id="oldMsgView"></div>'; 
	                 }
                    if(data.alert != 'nomsg'){
                      element = element + '<div id="chatView">';
                       $.each(data.msg, function(key, value){
                       	    var dbDate = value.date_time; 
		                      //date time formated start

		                        var  getDateString = function(date, format) {
						        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						        getPaddedComp = function(comp) {
						            return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
						        },
						        formattedDate = format,
						        o = {
						            "y+": date.getFullYear(), // year
						            "M+": months[date.getMonth()], //month
						            "d+": getPaddedComp(date.getDate()), //day
						            "h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
						             "H+": getPaddedComp(date.getHours()), //hour
						            "m+": getPaddedComp(date.getMinutes()), //minute
						            "s+": getPaddedComp(date.getSeconds()), //second
						            "S+": getPaddedComp(date.getMilliseconds()), //millisecond,
						            "b+": (date.getHours() >= 12) ? 'PM' : 'AM'
						        };

						        for (var k in o) {
						            if (new RegExp("(" + k + ")").test(format)) {
						                formattedDate = formattedDate.replace(RegExp.$1, o[k]);
						            }
						        }
						        return formattedDate;
						    };
							
						     objDate = Date.parse(dbDate.replace(/-/g, "/"));
							 var formattedDate  = getDateString(new Date(objDate ), "d M, y at h:m b");
							 var formattedTime  = getDateString(new Date(objDate ), "h:m b");

							 //console.log(formattedmonth);

                         	//date time formated end
 
	                       //split
	                       	var dateString = value.date_time;
							var arr  = dateString.split(' ');
							var date = arr[0];
							//console.log(date);
                            if(value.send_from == data.sendUser){
                                element = element + '<div class="row msg_container base_sent">';
                                element = element + '<div class="col-md-10 col-xs-10">';
                                element = element + '<div class="messages msg_sent">';
            	                element = element + '<p>'+ value.message +'</p>';

            	                if(new_date == date){
                                   element = element + '<time style="color:#666666">'+ formattedTime +'</time>';
                                }else{
                                    element = element + '<time style="color:#666666">'+ formattedDate +'</time>';
                                }
                                element = element + '</div>';
                                element = element + '</div>';
                                element = element + '<div class="col-md-2 col-xs-2 avatar">';
                                 if(data.senderImg){
                               		 element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ data.senderImg +'"); ?>">';
                                 } else {
                                  element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';   	
                                 }
                                element = element + '</div>';
                                element = element + '</div>';

                            } else {
                                element = element + '<div class="row msg_container base_receive">';
                                element = element + '<div class="col-md-2 col-xs-2 avatar">';
                                 if(value.image){
                                   element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ value.image +'"); ?>">';
                                 } else {
                                   element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';  
                                 }

                                element = element + '</div>';
                                element = element + '<div class="col-md-10 col-xs-10">';
                                element = element + '<div class="messages msg_receive">';
                                element = element + '<p>'+ value.message +'</p>';
                                if(new_date == date){
                                   element = element + '<time style="color:#666666">'+ formattedTime +'</time>';
                                }else{
                                    element = element + '<time style="color:#666666">'+ formattedDate +'</time>';
                                }
                                element = element + '</div>';
                                element = element + '</div>';
                                element = element + '</div>';

                           }


                      });

                      element = element + '</div>';
                    } else {

                      element = element + '<div id="chatView">';
                      element = element + '<div class="row msg_container base_receive">';
                      element = element + '<div class="col-md-2 col-xs-2 avatar">';
                      if(data.image){
                      	element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ data.image +'"); ?>" class=" img-responsive ">';
                      } else {
                        element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';  
                      }
                      element = element + '</div>';
                      element = element + '<div class="col-md-10 col-xs-10">';
                      element = element + '<div class="messages msg_receive">';
                      element = element + '<p>No message</p>';
                      element = element + '</div>';
                      element = element + '</div>';
                      element = element + '</div>';
                      element = element + '</div>'; 
                    }
                    element = element + '</div>';
                    element = element + '<div class="panel-footer">';
                    element = element + '<form id="sendMsg" action="<?php echo site_url('liveChat/sendMessageAction'); ?>" method="post" enctype="multipart/form-data">';
                    element = element + '<textarea class="form-control" name="message" id="message" rows="2" placeholder="Write your message here..." ></textarea>';
                    element = element + '</form>';
                    element = element + '</div>';
                    element = element + '</div>';
                    element = element + '</div>';
                    element = element + '</div>';
                    element = element + '</div>';


                       $( "#chatMsgView" ).prepend(element);
                       $(".msg_container_base").animate({ scrollTop: $(document).height() }, "slow");
  					   return false;

                   }
	                
	              
				}


			});	

			event.preventDefault();
	});

   // open window when new message 

   function getNewMsg() {
      var activeUser = "<?php echo $userAutoId; ?>";
       $('<audio id="chatAudio"><source src="<?php echo base_url("resource/img/notify.ogg"); ?>" type="audio/ogg"><source src="<?php echo base_url("resource/img/notify.mp3"); ?>" type="audio/mpeg"><source src="<?php echo base_url("resource/img/notify.wav"); ?>" type="audio/wav"></audio>').appendTo('body');
	  $.ajax({
        url : SAWEB.getSiteAction('liveChat/getMsgFromUser'),
		type: "POST",
		data : {activeUser: activeUser},
		dataType: "json",
        success: function (data) { 
	     //get current date time
            var currentdate = new Date(); 
			var new_date = + currentdate.getFullYear() + "-"
            + addZero(currentdate.getMonth()+1)+ "-" 
            + addZero(currentdate.getDate());
		 //current date time end 
        if(data != "noNewMsg"){
		   if ($("#chatMsgView").find("#msgViewId[data-id="+data.id+"]").length==0){	

			//New Box Open
        	 var element = '<div style="padding:0" id="msgViewId" class="container" data-id="'+ data.id +'">';
                element = element + '<div class="row chat-window col-xs-5 col-md-3">';
                element = element + '<div class="col-xs-12 col-md-12" style="padding-right:10px; bottom:0;">';
                element = element + '<div class="">';
                element = element + '<div class="top-bar">';
                element = element + '<div class="col-md-8 col-xs-8">';
                if(data.online_status =='active'){
                 element = element + '<h3 class="panel-title">'+ data.name +'</h3>';
                } else {
                  element = element + '<h4 style="font-size:12px;" class="panel-title">'+ data.name +'</h4>';
                }
                element = element + '</div>';
                element = element + '<div class="col-md-4 col-xs-4" style="text-align: right; padding-right:0px">';
                element = element + '<a href="#"><span style="padding-right:8px; color:#FFFFFF"><i class="glyphicon glyphicon-minus minusChatBox"></i></span></a>';
                element = element + '<a href="#"><span style="color:#FFFFFF; right:0px"><i class="glyphicon glyphicon-remove"></i></span></a>';
                element = element + '</div></div>';
                element = element + '<div style="height:300px" class="panel-body msg_container_base">';
                if(data.countMsg > 20){
	                element = element + '<div class="oldMsg" style="padding-top:5px; cursor:pointer">See old message</div>'; 
	                element = element + '<div id="oldMsgView"></div>';
	             }
                if(data.alert != 'nomsg'){
                  element = element + '<div id="chatView">';
                   $.each(data.allmsg, function(key, value){

                   	   var dbDate = value.date_time; 

		                      //date time formated start

		                        var  getDateString = function(date, format) {
						        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						        getPaddedComp = function(comp) {
						            return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
						        },
						        formattedDate = format,
						        o = {
						            "y+": date.getFullYear(), // year
						            "M+": months[date.getMonth()], //month
						            "d+": getPaddedComp(date.getDate()), //day
						            "h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
						             "H+": getPaddedComp(date.getHours()), //hour
						            "m+": getPaddedComp(date.getMinutes()), //minute
						            "s+": getPaddedComp(date.getSeconds()), //second
						            "S+": getPaddedComp(date.getMilliseconds()), //millisecond,
						            "b+": (date.getHours() >= 12) ? 'PM' : 'AM'
						        };

						        for (var k in o) {
						            if (new RegExp("(" + k + ")").test(format)) {
						                formattedDate = formattedDate.replace(RegExp.$1, o[k]);
						            }
						        }
						        return formattedDate;
						    };
							
						     objDate = Date.parse(dbDate.replace(/-/g, "/"));
							 var formattedDate = getDateString(new Date(objDate ), "d M, y at h:m b");
							 var formattedTime  = getDateString(new Date(objDate ), "h:m b");

							 //console.log(formattedDate);

                         	//date time formated end
 
	                       //split
	                       	var dateString = value.date_time;
							var arr  = dateString.split(' ');
							var date = arr[0];

							

                        if(value.send_from == data.sendUser){
                            element = element + '<div class="row msg_container base_sent">';
                            element = element + '<div class="col-md-10 col-xs-10">';
                            element = element + '<div class="messages msg_sent">';
        	                element = element + '<p>'+ value.message +'</p>';
        	                 if(new_date == date){
                                   element = element + '<time style="color:#666666">'+ formattedTime +'</time>';
                                }else{
                                    element = element + '<time style="color:#666666">'+ formattedDate +'</time>';
                                }
                            element = element + '</div>';
                            element = element + '</div>';
                            element = element + '<div class="col-md-2 col-xs-2 avatar">';
                             if(data.senderImg){
                           		 element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ data.senderImg +'"); ?>">';
                             } else {
                              element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';   	
                             }
                            element = element + '</div>';
                            element = element + '</div>';

                        } else {
                            element = element + '<div class="row msg_container base_receive">';
                            element = element + '<div class="col-md-2 col-xs-2 avatar">';
                             if(value.image){
                               element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ value.image +'"); ?>">';
                             } else {
                               element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';  
                             }

                            element = element + '</div>';
                            element = element + '<div class="col-md-10 col-xs-10">';
                            element = element + '<div class="messages msg_receive">';
                            element = element + '<p>'+ value.message +'</p>';
                              if(new_date == date){
                                   element = element + '<time style="color:#666666">'+ formattedTime +'</time>';
                                }else{
                                    element = element + '<time style="color:#666666">'+ formattedDate +'</time>';
                                }
                            element = element + '</div>';
                            element = element + '</div>';
                            element = element + '</div>';

                       }


                  });

                  element = element + '</div>';
                 }
                
                element = element + '</div>';
                element = element + '<div class="panel-footer">';
                element = element + '<form id="sendMsg" action="<?php echo site_url('liveChat/sendMessageAction'); ?>" method="post" enctype="multipart/form-data">';
                element = element + '<textarea class="form-control" name="message" id="message" rows="2" placeholder="Write your message here..." ></textarea>';
                element = element + '</form>'; 
                element = element + '</div>';
                element = element + '</div>';
                element = element + '</div>';
                element = element + '</div>';
                element = element + '</div>';

                 $( "#chatMsgView" ).prepend(element);
                 $('#chatAudio')[0].play();
                  updateMsgStatus(data.msgId);

                 $(".msg_container_base").animate({ scrollTop: $(document).height() }, "slow");
					  return false;
					 

	            } else{
	               element = '';
                       $.each(data.allmsg, function(key, value){
                       	     var dbDate = value.date_time; 

		                      //date time formated start

		                        var  getDateString = function(date, format) {
						        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
						        getPaddedComp = function(comp) {
						            return ((parseInt(comp) < 10) ? ('0' + comp) : comp)
						        },
						        formattedDate = format,
						        o = {
						            "y+": date.getFullYear(), // year
						            "M+": months[date.getMonth()], //month
						            "d+": getPaddedComp(date.getDate()), //day
						            "h+": getPaddedComp((date.getHours() > 12) ? date.getHours() % 12 : date.getHours()), //hour
						             "H+": getPaddedComp(date.getHours()), //hour
						            "m+": getPaddedComp(date.getMinutes()), //minute
						            "s+": getPaddedComp(date.getSeconds()), //second
						            "S+": getPaddedComp(date.getMilliseconds()), //millisecond,
						            "b+": (date.getHours() >= 12) ? 'PM' : 'AM'
						        };

						        for (var k in o) {
						            if (new RegExp("(" + k + ")").test(format)) {
						                formattedDate = formattedDate.replace(RegExp.$1, o[k]);
						            }
						        }
						        return formattedDate;
						    };
							
						     objDate = Date.parse(dbDate.replace(/-/g, "/"));
							 var formattedDate = getDateString(new Date(objDate ), "d M, y at h:m b");
							 var formattedTime  = getDateString(new Date(objDate ), "h:m b");

                         	//date time formated end
 
	                       //different two datetime 
	                       	var dateString = value.date_time;
							var arr  = dateString.split(' ');
							var date = arr[0];

                            if(value.send_from == data.sendUser){
                                element = element + '<div class="row msg_container base_sent">';
                                element = element + '<div class="col-md-10 col-xs-10">';
                                element = element + '<div class="messages msg_sent">';
            	                element = element + '<p>'+ value.message +'</p>';
            	                  if(new_date == date){
                                   element = element + '<time style="color:#666666">'+ formattedTime +'</time>';
                                }else{
                                    element = element + '<time style="color:#666666">'+ formattedDate +'</time>';
                                }
                                element = element + '</div>';
                                element = element + '</div>';
                                element = element + '<div class="col-md-2 col-xs-2 avatar">';
                                 if(data.senderImg){
                               		 element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ data.senderImg +'"); ?>">';
                                 } else {
                                  element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';   	
                                 }
                                element = element + '</div>';
                                element = element + '</div>';

                            } else {
                                element = element + '<div class="row msg_container base_receive">';
                                element = element + '<div class="col-md-2 col-xs-2 avatar">';
                                 if(value.image){
                                   element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("Images/Register_image/'+ value.image +'"); ?>">';
                                 } else {
                                   element = element + '<img style="display: block; width: 100%;" src="<?php echo base_url("resource/img/profile2.png"); ?>">';  
                                 }

                                element = element + '</div>';
                                element = element + '<div class="col-md-10 col-xs-10">';
                                element = element + '<div class="messages msg_receive">';
                                element = element + '<p>'+ value.message +'</p>';
                                if(new_date == date){
                                   element = element + '<time style="color:#666666">'+ formattedTime +'</time>';
                                }else{
                                    element = element + '<time style="color:#666666">'+ formattedDate +'</time>';
                                }
                                element = element + '</div>';
                                element = element + '</div>';
                                element = element + '</div>';

                           }


                      });

                       $("#chatMsgView").find("#msgViewId[data-id="+data.id+"]").find("#chatView").html(element);
                       $('#chatAudio')[0].play();
                        updateMsgStatus(data.msgId);
                       $(".msg_container_base").animate({ scrollTop: $(document).height() }, "slow");
  					  return false;

	            }

	              


	          }
              
           }
       });
	  setTimeout(getNewMsg, 2000);

	}





	$(document).ready(function() {
	   setTimeout(getNewMsg, 2000);
	   setTimeout(getAllActiveUser, 5000); 
	});

	//status Update 
	function updateMsgStatus(msgId) {
	  $.ajax({
			url : SAWEB.getSiteAction('liveChat/updateMsgStatus'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : {msgId : msgId},
			dataType : "html",
			success : function(data) {			
			}
		});	

	}


	// chat list refresh at a time
    
	 function getAllActiveUser() {
	  var hiddenValue = $('#chatHiddenId').val();
	  console.log(hiddenValue);
      var activeUser = "<?php echo $userAutoId; ?>";
       if(hiddenValue == ''){
		  $.ajax({
	        url : SAWEB.getSiteAction('liveChat/getAllActiveUser'),
			type: "POST",
			data : {},
			dataType: "html",
	        success: function (data) { 
	        	$("#chatBarShow").html(data);

	          }
	       });
		   setTimeout(getAllActiveUser, 5000);
        }
	}

	// see old message 
     $(document).on("click", ".oldMsg", function(event){
		var parents 	 = $(this).parents('#msgViewId');
		var sendToId 	 = parents.attr('data-id');
		console.log(sendToId);	

		  $.ajax({
			url : SAWEB.getSiteAction('liveChat/oldMsgView'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { sendToId : sendToId},
			dataType : "html",
			success : function(data) {			
				parents.find("#oldMsgView").html(data);
				parents.find(".oldMsg").css("display", "none");
				
			}
		});	

        event.preventDefault();
	});


    //chat box slidetoggle
	$(document).on("click", ".glyphicon-remove", function(event){
		var parents 	 = $(this).parents('#msgViewId');
		console.log(parents);		
		parents.remove();	
        event.preventDefault();
	});

   //message box hide 
	$(document).on("click", ".minusChatBox", function(event){
		var parents 	 = $(this).parents('#msgViewId');
		if($(this).hasClass("glyphicon-minus")) {
			$(this).parents('.removeIcon').css("display","none");
			parents.find('.panel-body').hide().animate('slow');
			parents.find('.panel-footer').hide().animate('slow');
			parents.find(".chat-window").css("margin-top", "395px").animate('slow');
			parents.find('.maximux').css("display","block");
		} 
        event.preventDefault();
	});


    //message box show 
	$(document).on("click", ".maximux", function(event){
		var parents 	 = $(this).parents('#msgViewId');
			parents.find('.removeIcon').css({'display': 'block','padding-right': '25px','margin-bottom': '-16px'}); 
			parents.find('.panel-body').show().animate('slow');
			parents.find('.panel-footer').show().animate('slow');
			parents.find(".chat-window").css("margin-top", "0").animate('slow');
			parents.find('.maximux').css("display","none");
        event.preventDefault();
	});



    
    

	/*//message box fade toggle
	$(document).on("click", ".minusChatBox", function(event){
		var parents 	 = $(this).parents('#msgViewId');
		if($(this).hasClass("glyphicon-minus")) {
			$(this).removeClass("glyphicon-minus").addClass("glyphicon-plus");
			parents.find('.panel-body').hide().animate('slow');
			parents.find('.panel-footer').hide().animate('slow');
			parents.find(".chat-window").css("margin-top", "350px").animate('slow');
		} else {
			$(this).removeClass("glyphicon-plus").addClass("glyphicon-minus");
			parents.find('.panel-body').show().animate('slow');
			parents.find('.panel-footer').show().animate('slow');
			parents.find(".chat-window").css("margin-top", "0").animate('slow');
		}
        event.preventDefault();
	});*/


    // chat box hover
	$(document).on("mouseover", "#chatRow", function(){
	        $(this).css("background-color", "#E9EAED");
	});

	$(document).on("mouseleave", "#chatRow", function(){
	        $(this).css("background-color", "#FFFFFF");
	});

	// search user

	$(document).on("click", "#searchUser", function(event){
		var parents  = $(this).parents('.chat_box');
		var user 	 = parents.find("#search_user").val();
		if(user ==''){
			alert('please enter value');
			return false;
		} else {
		
			$.ajax({
					url : SAWEB.getSiteAction('liveChat/searchUser'), // URL TO LOAD BEHIND THE SCREEN
					type : "POST",
					data : { user : user},
					dataType : "html",
					success : function(data) {			
						parents.find(".chat_body").html(data);
						//parents.find("#search_user").val("");
						
					}
				});	
		}
        event.preventDefault();
	});

	// message send to user
	$(document).on("keyup", "#message", function(event){
		var message 	= $(this).val();
		var parents  	= $(this).parents('#msgViewId');	
		var viewMsg  	= parents.find("#chatView");
		var scrollMsg  	= parents.find(".msg_container_base");
		var id 			= parents.attr('data-id');
		var formURL 	= parents.find("#sendMsg").attr("action");
		console.log(formURL);

	    if (event.which == 13 && ! event.shiftKey) {
	    	$.ajax({
				url : formURL, // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : id,  message : message},
				dataType : "html",
				success : function(data) {			
					viewMsg.html(data);
					parents.find("#message").val("");
					scrollMsg.animate({ scrollTop: $(document).height() }, "slow");
  					  return false;
					
				}
			});
			
	    }

	    event.preventDefault();

	    
   });

	
	//chat box slidetoggle

	$(document).on("click", ".chat_head", function(){
		var parents = $(this).parents('.chat_box');
		if(parents.find('.minimusChat').hasClass("glyphicon-minus")){
		   $('#chatHiddenId').val('min');
		    parents.find('.minimusChat').removeClass("glyphicon-minus");
		   parents.find('.noImg').css("display", "none"); 
		   parents.find('.chat_body').hide(200); 
		   parents.find('.imgAd').css("display", "block"); 
		} else {
		   $('#chatHiddenId').val('');
		   parents.find('.minimusChat').addClass("glyphicon-minus");
		   parents.find('.chat_body').show(200);
		   parents.find('.noImg').css("display", "block"); 
		    parents.find('.imgAd').css("display", "none"); 
		   getAllActiveUser();
		}
		

	});




	// message send to user
	/*$(document).on("submit", "#sendMsg", function(event){
    	var message = $(this).parents('#msgViewId').find("#message").val();
    	console.log(message);
		var parents  = $(this).parents('#msgViewId');	
		var viewMsg  = parents.find("#chatView");
		var scrollMsg  = parents.find(".msg_container_base");
		var id = parents.attr('data-id');
		var formURL = $(this).attr("action");
		console.log(formURL);
				
			$.ajax({
				url : formURL, // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : id,  message : message},
				dataType : "html",
				success : function(data) {			
					viewMsg.html(data);
					parents.find("#message").val("");
					scrollMsg.animate({ scrollTop: $(document).height() }, "slow");
  					  return false;
					
				}
			});

			event.preventDefault();
       
	});
*/


    

	</script>
	
    
  </body>
</html>