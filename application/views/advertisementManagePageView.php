<div class="row" style=" padding-left:10px; padding-right:15px;">
	 <div class="col-lg-12 wellB wellB-lg" style="border:solid 1px #e3e3e3; border-radius:3px;">
	 <div class="row">
	   <div class="col-lg-12" style="padding-bottom:10px"><span style="color:#0a81ce; font-size:16px;"><?php echo $viewAddInfo->name; ?></span>
	   (<span style="color:#666666; font-size:12px"><?php echo $viewAddInfo->title; ?></span>)</div>
	 </div>
	 
	    <?php 
		   $jobId  = $viewAddInfo->id;
		   $applyLink 	= array('home','advritismentViewApply', $jobId);
		
		?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#FFF; font-size:12px" class="table table-bordered view-advritesmentview">
                      
                      <tr>
                        <td width="28%" style="font-weight:bold">Ad Type</td>
                        <td width="72%"><span style="font-size:12px"><?php echo $viewAddInfo->add_type; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Job Category </td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->job_category; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Organization Type</td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->organization_type; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Job Title</td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->title; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">No. of Vacancies</td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->vacancies; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Application Deadline</td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->apllication_deadline; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Billing Contact</td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->contact_person; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Age Range</td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->age_range_from; ?>-<?php echo $viewAddInfo->age_range_to; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Gender </td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->gender; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Educational Qualification </td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->education_qualification; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Compensation and Other Benefits </td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->add_type; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold"><span id="spnExperienceYear">Total Years of Experience</span></td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->exprience_min; ?>-<?php echo $viewAddInfo->exprience_max; ?></span></td>
                      </tr>
                      <tr>
                        <td style="font-weight:bold">Additional Job Requirements </td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->job_requirements; ?></span></td>
                      </tr>
					  <tr>
                        <td style="font-weight:bold">Job Description </td>
                        <td><span style="font-size:12px"><?php echo $viewAddInfo->job_description; ?></span></td>
                      </tr>
                      <tr>
                        <td colspan="2" align="center" valign="middle">
							<span class="view-advritesmentviewlink"><a href="<?php echo site_url($applyLink); ?>">Apply online</a></span></td>
                        </tr>
					  <tr>
                        <td colspan="2" align="center" valign="middle"><span style="font-size:14px; font-weight:bold">Company Information.</span><br />
						  <?php echo $viewAddInfo->name; ?>
                          <br/> 
						  <?php echo $viewAddInfo->address; ?>
						  <br/>
                         </td>
                        </tr>
                  
                    </table>

	 </div>
  </div>