<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

   <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	
	<!--PHOTO GALLERY-->
	 <link href="<?php echo base_url('resource/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7'); ?>" rel="stylesheet">
     <link href="<?php echo base_url('resource/source/helpers/jquery.fancybox-buttons.css?v=1.0.5'); ?>" rel="stylesheet">
     <link href="<?php echo base_url('resource/source/jquery.fancybox.css?v=2.1.4'); ?>" rel="stylesheet">
	 <!--PHOTO GALLERY-->

	<style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				.fancybox-custom .fancybox-skin {
				box-shadow: 0 0 50px #222;
			}
				
				
				</style>
    <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
		
    <!--PHOTO GALLERY-->
	
	<script src="<?php echo base_url('resource/source/jquery.fancybox.js?v=2.1.4'); ?>"></script>
	<script src="<?php echo base_url('resource/source/jquery.mousewheel-3.0.6.pack.js'); ?>"></script>

	<script src="<?php echo base_url('resource/source/helpers/jquery.fancybox-buttons.js?v=1.0.5'); ?>"></script>
	<script src="<?php echo base_url('resource/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7'); ?>"></script>
	<script src="<?php echo base_url('resource/source/helpers/jquery.fancybox-media.js?v=1.0.5'); ?>"></script>
	<script src="<?php echo base_url('resource/source/UeYjenowrboreb.js'); ?>"></script>

		
    <!--PHOTO GALLERY-->
		

  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('adDetailsHeader'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>     
			 <div class="row">&nbsp;</div> 
			   
              <div class="row">  <!--row Start-->           
		      <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg" style="padding-bottom:0px; min-height:500px;">
						    <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding:0px 0px 15px 12px; text-align:left"> Photo Gallery</div>
									
									  <div class="row" style="padding:0px 15px 0px 15px">
									     <?php 
											   
												$a = 0;
												$b = 4;	
												foreach ($orgPhotoInfo as $v){
												$id  		    = $v->id;
												
												if($a == 0) echo "<div class='row'>";
												if($a <= $b)  
											   ?>
									         <div class="col-lg-3" style="padding-bottom:15px">
											    <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo base_url("Images/photo_gallery/$v->image"); ?>"><img src="<?php echo base_url("Images/photo_gallery_resize/$v->image"); ?>" class="img-thumbnail" width="255" height="180"/></a>
												 
											 </div>
											 <?php 	
											        $a++;	
													if($a == $b) echo "</div>";				
													if($a == $b) $a=0; } 
												?> 
										       
										 
									   </div>  
						   		
						   </div>
				        </div>             <!--col 9 end--> 
				                  <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	   </div>                   <!--col end-->  
		     </div>
             <div class="row">&nbsp;</div>     
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    
  </body>
</html>