<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

   <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<style type="text/css">
				#menu1 a {color:black;background-color:#ededed;text-decoration:none;text-indent:1ex;}
				#menu1 a:active {color:black;text-decoration:none;}
				#menu1 a:hover {color:black;background-color:#FFFF99}
				#menu1 a:visited {color:black;text-decoration:none;}
				
				
				</style>
        <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
		<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('adDetailsHeader'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('organizationMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>     
			<div class="row">
			   <div class="col-lg-8 adcolor" style="color:#09659a; font-weight:normal; padding:10px 0px 15px 12px;">Advertisement</div>
			   <div class="col-lg-4 adcolor" style="color:#09659a; font-weight:normal; padding:10px 12px 15px 12px;">
			    <form id="adSearch" method="post" action="<?php echo site_url('home/advertiseSearch') ?>">
			       <input type="hidden" name="organize_id" id="organize_id" value="<?php echo $organizeId ?>">
	   	            <div class="form-group margin-0">				
						<div class="input-group">
							<input type="text" name="title" id="title" class="form-control"  placeholder="Search by title">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit" id="search"><i class="glyphicon glyphicon-search"></i> Search</button>
							</span>												
					    </div><!-- /input-group -->		
					</div>	
				</form> 
			   </div>
			 </div>  
              <div class="row">  <!--row Start-->           
		      <div class="col-lg-12"><!--col Start-->  
			       <div id="adSearchView" class="row" style=" padding-left:10px; padding-right:15px;">
						 <div class="col-lg-12 wellB wellB-lg" style="border:solid 1px #e3e3e3; border-radius:3px;">
							  <div class="row"> <!--row start--> 
								<?php 
									
									 foreach ($orgAdvertiseInfo as $v){
									  $id      		= $v->id;
									  $addViewLink 	= array('home','orgWiseAdvritismentView', $id, $organizeId);
								 ?>
								  <div class="col-lg-4 col-sm-6 col-md-4 block_padd" style="padding:0px;">
								   
									<div class="row" style="border:solid 1px #e3e3e3; margin:2px; border-radius:3px; background:#fff">
									   <div class="col-lg-3 col-xs-3" align="center" style="vertical-align:middle; padding:0px;">
										<div style="border: solid 1px #e3e3e3; margin:5px; float:center;">
										   <a href="<?php echo site_url($addViewLink); ?>" style="text-decoration:none;color:#999;">
                                              <?php if(!empty($v->image)){ ?>
										         <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>"  alt="" width="77" height="65" />
                                              <?php }else{ ?>
                                                  <img src="<?php echo base_url("resource/img/profile2.png"); ?>"  width="77" height="65" />
                                              <?php } ?>
										    </a>
										</div>
									   </div>
									   <div class="col-lg-9 col-xs-9" style="font-size:12px; padding-left:3px; padding-top:3px;">
										 <div class="row">
										   <div class="col-lg-12">
											 <a href="<?php echo site_url($addViewLink); ?>" style="text-decoration:none;color:#0a81ce; font-size:14px;"><?php echo $v->name; ?></a>
										   </div>
										 </div>
										 <div class="row">
										   <div class="col-lg-12">
											 <ul style="list-style:none">
											   <li class="add_text_color"><a href="<?php echo site_url($addViewLink); ?>" style="text-decoration:none;color:#999;"> <?php echo $v->title; ?></a></li>
											 </ul>
										   </div>
										 </div>
									   </div>
									 </div>
									
								  </div>
								  <?php 	
										}
								  ?> 
							   </div>
						 </div>
 				 	</div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
			 
             <div class="row">&nbsp;</div>     
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	<script>

	  //callback handler for form submit
		$("#adSearch").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#adSearchView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
	  
	//Search all keyword
	
		$('#search').on('click', function(e) {
		 var search_all = $("#search_all").val();   
		
		$.ajax({
			url: "<?php echo site_url('home/searchAll') ?>",
			type : "POST",
			data: {search_all : search_all},
			dataType : "html",
			success : function(data) {			
				$("#searchOrganization").html(data);
			}
		});
		
		
		
	});
	
		
	
	
	   
	
		
	</script>
	
    
  </body>
</html>