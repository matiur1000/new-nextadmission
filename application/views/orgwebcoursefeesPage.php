<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
  
  <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">

   <script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/mmenu.js'); ?>"></script>
	
    <!--CUSTOM BODY-->
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	
	<style type="text/css">

				#menu1{z-index: 9999999999;}
				#menu1 table{
					border: 3px solid #DC2033 !important;
				    font-family: verdana, sans-serif;
				    font-size: 100%;
				    border-collapse: collapse;
				    width: 16.75em;
				}
				#menu1 table tr{
					
				}
				#menu1 table tr td{
					border-bottom: 1px dotted #9fcfff !important;
				    border-left: 1px solid black;
				    text-align: center;
				    vertical-align: middle !important;
				    background-color: #1B9AFF !important;
				}
				#menu1 table tr td a{
					background-color: #1B9BFF !important;
					color: #F2E385 !important;
					font-size: 15px;
					font-family: arial;
					text-transform: uppercase;
					text-decoration: none;
					display: block;
					padding-top:3px;
					font-weight: 400;
					border-radius: 5px;
				}
				#menu1 table tr td a:hover{
					background-color:#ED1144 !important;
					color:#fff !important;
				}

				.adStyle{
					cursor: pointer;
					box-shadow: 0px 0px 5px 0px #fff;
					padding:10px !important
				}
				
				.adStyle img{
					background-color: #fff;
				    border: 1px solid #ddd;
				    border-radius: 4px;
				    display: inline-block;
				    line-height: 1.42857;
				    margin-right: 10px;
				    padding: 4px;
				    transition: all 0.2s ease-in-out 0s;
				}
				.adStyle h5{
					font-size: 16px;
					font-weight: bold;
					margin-top: 0;
					margin-bottom: 5px;
				}


				.adStyle p{
					text-align: justify; 
				}

/* below style code is for right sidebar*/
.rightsidebar{
font-size: 14px;
 background: #ad9c9c;
 margin-bottom:10px;
 width:100%;
float: right;
}	
	.rightsidebar ul{}
	.rightsidebar ul li{
	padding: 10px;
	list-style: none;
	border-bottom: 1px solid gray;
	}
	.rightsidebar ul li.passive{
	background:#32b60a;
	}															
	.rightsidebar ul li a{
	color:#fff;
	font-weight:bold;
	 font-size:17px;
	 }
	
	.rightsidebar ul li a:hover{ 
	color:red;
	text-decoration:none;
	}					
	
	.logo{ background: #669999; height: 100px; text-align: center;}
	.name{background: gray;height: 100px; text-align: center;}
	.menu{background: #ff99cc;height: 100px; text-align: center;}
	.pull-left{width: 485px;}
	/* right sidebar code  end*/	
			
	.titleorgin{
		padding-left:180px;
		font-family:Arial, Helvetica, sans-serif;
		font-size:30px;
		color:#FFFFFF;
	}
		
	</style>

  </head>
  <body>
	<div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('adDetailsHeader'); ?>
           <div class="row">&nbsp;</div>            
       </div>      
           
          
	 
       <div class="container">
        	<div class="row">
    			<div class="col-md-12">
					<div class="row" style="padding-bottom:5px;">
						<?php $this->load->view("organizationwebannerPage"); ?>
					</div>
				</div>
		</div>
	<!--my top code close here-->   
	
	<!--my body code start here-->  
	    <div class="row" style="border:2px solid gray;">
    	<div class="col-md-12">
			<div class="row">
				<div class="col-md-10">
					<div class="row" style="padding:10px;">
					
					
					<?php
					
					$documentfile = $orgcouefeeInfo->docs_pdf_file;
					
				
							
					?>
					<iframe src="<?php echo base_url("Images/docfileUpload/".$documentfile); ?>" style="width:950px; height:650px;" frameborder="0"></iframe>
					</div>
					
				</div>
				
				<div class="col-md-2">
					<div class="row" style="padding-top:10px;">
					<?php $this->load->view('orgRightSidebarPage'); ?>
					</div>
				</div>
			</div>
    	</div>
		
		
		
		<div class="col-md-12">
				<div class="row"><?php $this->load->view('footerPage'); ?></div>
			 </div>
    	</div>
		
 </div>
     
   
	<?php $this->load->view("orgjuqeymenu"); ?>
  </body>
</html>