<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"><?php echo $ogbreaknewIdBydetail->title; ?></h4>
</div>
<div class="modal-body">

<div class="row" style="padding:5px;">

	<div class="col-md-4">
	  	<div class="thumbnail row">
			<img src="<?php echo base_url("Images/slide_image/".$ogbreaknewIdBydetail->image); ?>" />
		</div>
	</div>
	
	<div style="padding:10px;"><?php echo $ogbreaknewIdBydetail->details; ?></div>
	
</div>


</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>