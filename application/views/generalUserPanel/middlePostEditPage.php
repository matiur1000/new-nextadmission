<div class="col-lg-9" align="right">
	<div class="welll welll-lg"> 
	<div class="row">&nbsp;</div>
	<div class="row">
	   <div class="col-lg-12" align="center">
		 <span style="color:#0e73a9; font-size:24px; font-weight:bold;">Update Post</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 16px; font-weight: normal; margin: 0; padding: 0 12px 30px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		<div class="row">
		 <form action="<?php echo site_url('generalUserHome/postUpdate'); ?>" method="post" enctype="multipart/form-data">
         <input type="hidden" name="id" id="id" value="<?php echo $postEditInfo->id; ?>" />
			<div class="col-lg-12">
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Tropic</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
					    <input type="text" class="form-control" id="title" name="title" placeholder="Tropic" tabindex="1" value="<?php echo $postEditInfo->title; ?>" >
					  </div>
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Image</span> &nbsp;: 
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group" align="left">
								<input type="file" id="image" name="image" tabindex="8" />
					  </div>
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Image Title</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				    <div class="form-group">
						<input type="text" class="form-control" id="image_title" name="image_title" placeholder="Image Title"
						 tabindex="13"  value="<?php echo $postEditInfo->image_title; ?>">
					  </div> 
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Date And Time</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				    <div class="form-group">
						<input name="date_and_time" type="text" id="date_and_time" class="form-control date-picker"
						 data-date-format="yyyy-mm-dd" value="<?php echo $postEditInfo->date_and_time; ?>"/>
					  </div> 
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
			   
			   <div class="row">
			      <div class="col-lg-4">
				     <span style="color: #333; font-size: 16px; font-weight: normal;">Details</span> &nbsp;:
				  </div>
				  <div class="col-lg-7" style="padding-left:0px;">
				     <div class="form-group">
						<textarea class="form-control" name="description" id="description" cols="60" rows="10" placeholder="Details"><?php echo $postEditInfo->description ?></textarea>
					  </div>
				  </div>
				  <div class="col-lg-1">
				  </div>
			   </div>
		   </div>  
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1" style="min-width:100px;">Update</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	</div>                
</div>
<script type="text/javascript">
		$('#date_and_time').datepicker({dateFormat: 'dd/mm/yy'});
   </script>
