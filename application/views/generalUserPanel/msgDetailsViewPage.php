<style>
  .commentStyle {
	border-left:1px;
	border-right:1px;
	border-top:1px;
    border-style:solid;
	border-color:#CCCCCC;
	border-width:100%;
	padding:5px;
	
}

.commentStyle:last-child {
    border-bottom:1px solid #CCCCCC;	
} 

.commentStyle h5{
	font-size: 14px;
	font-weight: normal;
	margin-top: 5px;
	margin-bottom: 0px;
}
.commentStyle p{
	font-size: 14px;
	font-weight: normal;
	padding-bottom:10px;
	float:left;
}

.commentStyle img{
	background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    display: inline-block;
    line-height: 1.42857;
    margin-right: 10px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}

</style>




<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title" id="myModalLabel">Message Details</h3>
</div>
  
<div class="modal-body">
	<img src="<?php echo base_url("Images/Register_image/$msgWiseDetails->image"); ?>" class="pull-left" style="margin:0 15px 0 0" width="100" height="80" />
    <p style="text-align:justify"><?php echo $msgWiseDetails->message ?></p>
	
	
	
	
     <div class="row">
	    <div id="replyView" class="col-lg-12" style="padding-top:30px;">
		    <?php 
			 	
		        if(!empty($msgWiseRep)){
				 foreach ($msgWiseRep as $v){
			  ?>
		     <div class="col-md-12 commentStyle">
		         <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
				<h5><a><?php echo $v->name; ?></a></h5>
				<h6><?php echo $v->date; ?></h6>	
				<p style="float:left"><?php echo $v->reply; ?></p> 
		     </div>
		  
		  <?php } } ?>
		   
		  
        </div>

         <div class="col-md-12" style="margin-top:20px;">
			   <form id="reply" action="<?php echo site_url('generalUserHome/messageReplyAction/'.$msgViewId); ?>" method="post" enctype="multipart/form-data">
		           <div class="form-group">
				    <label for="reply">Write Your Reply</label>
					<textarea name="reply" id="reply" class="form-control" rows="3"></textarea>
				  </div>
				  <div class="form-group" style="float:left">
					<button class="btn btn-success" type="submit">Post Reply</button>
				  </div>
			   	</form>
           </div>
	   
       
	 </div>
		 
</div>
		

<div class="modal-footer">
	<button class="btn btn-danger" data-dismiss="modal">Close</button>
</div>



<script>
  
  $("#reply").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL  = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#replyView").html(data);				
					$("#reply input[type='text'], textarea").val("");
				}
			});
			
			e.preventDefault();
		});




   
</script>