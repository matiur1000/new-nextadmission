<div class="col-lg-9">
	<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
	<div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:18px; font-weight:bold;"><i class="glyphicon glyphicon-edit icon-padding"></i> Update Resume</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
  
	  <div class="col-lg-2 col-sm-1 comntorg3" style="height:30px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/profileUpdate/'.$userDetails->id);?>">Personal Details</a></div>
      <div class="col-lg-4 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/careerInformationUpdate/'.$userDetails->id);?>">Career and Application Information</a></div>
      <div class="col-lg-2 col-sm-1 comntorg3  active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/educationUpdate/'.$userDetails->id);?>">Education</a></div>
		 <div class="col-lg-2 col-sm-1 active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/traningUpdate/'.$userDetails->id);?>">Training</a></div>
     
   </div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 18px; font-weight: bold; margin: 0; padding: 0 12px 0px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
                                <div class="row">
                                <form action="<?php echo site_url('generalUserHome/trainingAction'); ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="id" value="<?php echo $moreprofileEditInfo->user_id; ?>" />
                                <div class="col-lg-12">
                                <div class="form-group">
                                
                                <div>
                                <div class="form-group">
                                <label for="training_title"><span style="color: #333; font-size: 16px; font-weight: normal;">Training Title</span></label>
                                <input type="text" class="form-control" id="training_title" name="training_title" placeholder="Training Title" tabindex="7"
								 value="<?php echo $moreprofileEditInfo->training_title; ?>">
                             
                                </div>
                                
                                 <div class="form-group">
                                <label for="training_tropic"><span style="color: #333; font-size: 16px; font-weight: normal;">Topic Covered</span></label>
                                <textarea class="form-control"  name="training_tropic" id="training_tropic" cols="" rows=""><?php echo $moreprofileEditInfo->training_tropic; ?></textarea>
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_institute"><span style="color: #333; font-size: 16px; font-weight: normal;">Institute</span></label>
                                <input type="text" class="form-control" id="training_institute" name="training_institute" placeholder="Institute" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_institute; ?>">
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_country"><span style="color: #333; font-size: 16px; font-weight: normal;">Country</span></label>
                                <input type="text" class="form-control" id="training_country" name="training_country" placeholder="Country" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_country; ?>">
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_location"><span style="color: #333; font-size: 16px; font-weight: normal;">Location</span></label>
                                <input type="text" class="form-control" id="training_location" name="training_location" placeholder="Location" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_location; ?>">
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_year"><span style="color: #333; font-size: 16px; font-weight: normal;">Training Year</span></label>
                                <input type="text" class="form-control" id="training_year" name="training_year" placeholder="Training Year" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_year; ?>">
                             
                                </div>
                                <div class="form-group">
                                <label for="training_duration"><span style="color: #333; font-size: 16px; font-weight: normal;">Duration</span></label>
                                <input type="text" class="form-control" id="training_duration" name="training_duration" placeholder="Duration" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_duration; ?>">
                             
                                </div>
                                </div>
                                </div> 
                             
                                </div>   
                                <div class="row">&nbsp;</div>  
                                <div class="row">
                                <div class="col-lg-12" align="center">
                                <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Resume</button>
                                </div>
                                </div>
                                <div class="row">&nbsp;</div>   
                                <div class="row">&nbsp;</div>  
                                </form>        
                                </div>
                                </div>                
                                </div>
<script>
	
	$("#region_id").change(function() {
		var region_id = $("#region_id").val();			
		$.ajax({
			url : SAWEB.getSiteAction('generalUserHome/countryEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : region_id },
			dataType : "html",
			success : function(data) {			
				$("#country_id").html(data);
			}
		});
		
	});	
	$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('generalUserHome/cityEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});	
   $("#country_id").change(function() {
		var nationality = $("#country_id option:selected").attr('data-nationality');
		var countryCode = $("#country_id option:selected").attr('data-country-code');			
		
				$("#nationality").val(nationality);
				$("#mobile").val(countryCode);
			
		});
		
</script>