<div class="col-lg-9">

<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
	<div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:18px; font-weight:bold;"><i class="glyphicon glyphicon-edit icon-padding"></i> Update Resume</span>
	   </div>
	</div>
	
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
  
	  <div class="col-lg-2 col-sm-1 comntorg3 active" style="height:30px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/profileUpdate/'.$userDetails->id);?>">Personal Details</a></div>
      <div class="col-lg-4 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px; background:#FFF">
	     <a href="<?php echo site_url('generalUserHome/careerUpdate/'.$userDetails->id);?>">Career and Application Information</a></div>
      <div class="col-lg-2 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/profileUpdate/'.$userDetails->id);?>">Education</a></div>
		 <div class="col-lg-2 col-sm-1 comntorg3 active" style="height:30px; margin-left:3px; padding-top:5px;">
	     <a href="<?php echo site_url('generalUserHome/profileUpdate/'.$userDetails->id);?>">Training</a></div>
     
   </div>
	<div class="row">
	  <div class="col-lg-12" style="border-bottom: 1px dashed #bdbdbd; color: #333; font-size: 18px; font-weight: bold; margin: 0; padding: 0 12px 0px;">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		<div class="row">
		 <form action="<?php echo site_url('generalUserHome/update'); ?>" method="post" enctype="multipart/form-data">
		  <input type="hidden" name="id" id="id" value="<?php echo $profileEditInfo->id; ?>" />
			<div class="col-lg-6">
				  <div class="form-group">
					<label for="region_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Sellect Region Name</span></label>        
					<div>
					   <select class="form-control" id="region_id" name="region_id"  tabindex="1">
							<option value="" selected>Sellect Region Name</option>
							<?php foreach ($regionInfo as $v){?>
							<option <?php if($profileEditInfo->region_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->region_name; ?></option>
							<?php } ?>
					   </select>
					</div>
				</div> 
				
				<div class="space-4"></div>  
				
				<div class="form-group">
					<label for="city_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Sellect City Name</span></label>        
					<div>
					   <select class="form-control" id="city_id" name="city_id"  tabindex="1" >
							<option value="" selected>Sellect City Name</option>
							<?php foreach ($cityInfo as $v){?>
							<option <?php if($profileEditInfo->city_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->city_name; ?></option>
							<?php } ?>
					   </select>
					 
						
					</div>
				</div>
				
				
				<div class="space-4"></div>  
					  <div class="form-group">
						<label for="education_institute"><span style="color: #333; font-size: 16px; font-weight: normal;">Education Institute</span></label>
						<input type="text" class="form-control" id="education_institute" placeholder="Education Institute" name="education_institute" tabindex="5" 
						value="<?php echo $moreprofileEditInfo->education_institute; ?>">
					  </div>
					  <div class="form-group">
						<label for="date_of_birth"><span style="color: #333; font-size: 16px; font-weight: normal;">Date Of Birth</span></label>
						<input name="date_of_birth" type="text" id="date_of_birth" class="form-control"
						 onClick="('#date_of_birth').datepicker({dateFormat: 'dd/mm/yy'});" value="<?php echo $moreprofileEditInfo->date_of_birth; ?>"/>
					   
					  </div>
					  
					  <div class="form-group">
						<label for="mother_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Mother Name</span></label>
						<input type="text" class="form-control" id="mother_name" name="mother_name" placeholder="Mother Name"
						 tabindex="6" value="<?php echo $moreprofileEditInfo->mother_name; ?>">
					  </div>
					  
					 
					  <div class="form-group">
						<label for="nationality"><span style="color: #333; font-size: 16px; font-weight: normal;">Nationality</span></label>
						<input type="text" class="form-control" id="nationality" name="nationality" 
						 placeholder="Nationality" tabindex="9" value="<?php echo $moreprofileEditInfo->nationality; ?>">
					  </div>
					  <div class="form-group">
						<label for="address"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Address </span></label>
					   <textarea class="form-control" name="address" id="address" cols="60" rows="" placeholder="Permanent Address"><?php echo $moreprofileEditInfo->address; ?></textarea>
					  </div>
					  <div class="form-group">
						<label for="country_phone"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Country Phone</span></label>
						<input type="text" class="form-control" id="country_phone" name="country_phone" placeholder="Present Country Phone"
						 tabindex="13" value="<?php echo $moreprofileEditInfo->country_phone; ?>">
					  </div>
					  <div class="form-group">
						<label for="email"><span style="color: #333; font-size: 16px; font-weight: normal;">Present  Email</span></label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Present  Email" tabindex="15" 
						value="<?php echo $moreprofileEditInfo->email; ?>">
					  </div>
					  <div class="form-group">
						<label for="address_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Address</span></label>
						<textarea class="form-control" name="address_permanent" id="address_permanent" cols="60" rows="" placeholder="Permanent Address" tabindex="17"><?php echo $moreprofileEditInfo->address_permanent; ?></textarea> 
					  </div>
					  <div class="form-group">
						<label for="country_phone_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Country Phone </span></label>
						<input type="text" class="form-control" id="country_phone_permanent" name="country_phone_permanent" 
						placeholder="Permanent Country Phone" tabindex="19" value="<?php echo $moreprofileEditInfo->country_phone_permanent; ?>"> 
					  </div>
					  <div class="form-group">
						<label for="email_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Email</span></label>
						<input type="email" class="form-control" id="email_permanent" name="email_permanent" placeholder="Permanent Email" tabindex="21" 
						value="<?php echo $moreprofileEditInfo->email_permanent; ?>" >
					  </div>
					  
														   
						 
		   </div>  
			<div class="col-lg-6">
				
				  <div class="form-group">
					<label for="country_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Sellect Country Name</span></label>        
					<div>
						<select class="form-control" id="country_id" name="country_id"  tabindex="1" value="<?php echo set_value('country_id'); ?>">
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option <?php if($profileEditInfo->country_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
					   </select>
					 
					</div>
				</div> 
				
				<div class="space-4"></div>  
				
				<div class="form-group">
					<label for="user_type"><span style="color: #333; font-size: 16px; font-weight: normal;">Sellect User Type</span></label>        
					<div>
					   <select class="form-control" id="user_type" name="user_type"  tabindex="2" value="<?php echo set_value('user_type'); ?>">
							<option <?php if($profileEditInfo->user_type == 'Organization User'){?> selected="selected" <?php } ?> value="Organization User">Organization User</option>
							<option <?php if($profileEditInfo->user_type == 'General user'){?> selected="selected" <?php } ?>  value="General user">General user</option>
					   </select>
					</div>
				</div>
				
				
				<div class="space-4"></div> 
				     <div class="form-group">
						<label for="name"><span style="color: #333; font-size: 16px; font-weight: normal;">Name</span></label>
						<input type="text" class="form-control" id="name" placeholder="User Name" name="name" tabindex="5" 
						value="<?php echo $profileEditInfo->name; ?>">
					  </div> 
					  
					   <div class="form-group">
						<label for="father_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Father Name</span></label>
						<input type="text" class="form-control" id="father_name" name="father_name" placeholder="Father Name" tabindex="7" 
						value="<?php echo $moreprofileEditInfo->father_name; ?>">
					  </div>
					  
					  
					  <div class="form-group">
						<label for="image"><span style="color: #333; font-size: 16px; font-weight: normal;">Image</span></label>
						   <div class="controls">
								<input type="file" id="image" name="image" tabindex="8" />
							</div>
					  </div>
					  <div class="form-group">
						<label for="city"><span style="color: #333; font-size: 16px; font-weight: normal;">Present City</span></label>
						<input type="text" class="form-control" id="city" name="city" placeholder="Present City" tabindex="12" 
						value="<?php echo $moreprofileEditInfo->city; ?>">
					  </div>
					  <div class="form-group">
						<label for="mobile"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Mobile </span></label>
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Present Mobile" tabindex="12" 
						value="<?php echo $moreprofileEditInfo->mobile; ?>">
					  </div>
					  <div class="form-group">
						<label for="city_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent City</span></label>
						<input type="text" class="form-control" id="city_permanent" name="city_permanent" placeholder="Permanent City" tabindex="16" 
						value="<?php echo $moreprofileEditInfo->city_permanent; ?>">
					  </div>
					  <div class="form-group">
						<label for="mobile_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Mobile </span></label>
						<input type="text" class="form-control" id="mobile_permanent" name="mobile_permanent" placeholder="Permanent Mobile" tabindex="18" 
						value="<?php echo $moreprofileEditInfo->mobile_permanent; ?>">
					  </div>
					  <div class="form-group">
						<label for="user_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Gender</span></label>
						<select class="form-control" id="gender" name="gender"  tabindex="25" value="<?php echo set_value('gender'); ?>">
								<option <?php if($profileEditInfo->gender == 'Male'){?> selected="selected" <?php } ?> value="Male">Male</option>
								<option <?php if($profileEditInfo->gender == 'Female'){?> selected="selected" <?php } ?> value="Female">Female</option>
						   </select>
					  </div>
					  <div class="form-group">
						<label for="password"><span style="color: #333; font-size: 16px; font-weight: normal;">Marital Status </span></label>
						<select class="form-control" id="marital_status" name="marital_status"  tabindex="25" value="<?php echo set_value('marital_status'); ?>">
								<option <?php if($profileEditInfo->marital_status == 'Married'){?> selected="selected" <?php } ?> value="Married">Married</option>
								<option <?php if($profileEditInfo->marital_status == 'Unmarried'){?> selected="selected" <?php } ?> value="Unmarried">Unmarried</option>
						   </select>
						<p class="first"></p>
					  </div>
					  <div class="form-group">
						<label for="conform_password"><span style="color: #333; font-size: 16px; font-weight: normal;">Religion </span></label>
						<input type="text" class="form-control conformpassword" id="religion" name="religion" placeholder="Religion" 
						tabindex="20" value="<?php echo $profileEditInfo->religion; ?>">
					  </div>
					  <div class="form-group">
						<label for="status"><span style="color: #333; font-size: 16px; font-weight: normal;">Status</span></label>        
						<div>
						   <select class="form-control" id="status" name="status"  tabindex="25" value="<?php echo set_value('status'); ?>">
								<option <?php if($profileEditInfo->status == 'Active'){?> selected="selected" <?php } ?> value="Active">Active</option>
								<option <?php if($profileEditInfo->status == 'In Active'){?> selected="selected" <?php } ?> value="In Active">In Active</option>
						   </select>
						</div>
					</div>
				 </div> 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Profile</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
	</div>                
</div>
<script>
	
	$("#region_id").change(function() {
		var region_id = $("#region_id").val();			
		$.ajax({
			url : SAWEB.getSiteAction('generalUserHome/countryEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : region_id },
			dataType : "html",
			success : function(data) {			
				$("#country_id").html(data);
			}
		});
		
	});	
	$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('generalUserHome/cityEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});	
   $("#country_id").change(function() {
		var nationality = $("#country_id option:selected").attr('data-nationality');
		var countryCode = $("#country_id option:selected").attr('data-country-code');			
		
				$("#nationality").val(nationality);
				$("#mobile").val(countryCode);
			
		});
		
</script>