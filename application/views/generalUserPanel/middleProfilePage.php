<div class="col-lg-9">
	<div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
	<div class="row">
	   <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
		 <span style="font-size:18px; font-weight:bold;"><i class="glyphicon glyphicon-edit icon-padding"></i> Update Resume</span>
	   </div>
	</div>  
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	
	   <ul class="nav nav-tabs">
		<li class="active"><a href="#personal">Personal Details</a></li>
		<li><a href="#career">Career and Application Information</a></li>
		<li><a href="#education">Education</a></li>
		<li><a href="#training">Training</a></li>
	</ul>
  
	 

   <div class="tab-content" style="background:#f4f4f4; padding:0 20px; border-bottom:solid 1px #CCC; border-left:solid 1px #CCC; border-right:solid 1px #CCC;">
	  
		<div id="personal" class="tab-pane fade in active">
		 <div class="row">&nbsp;</div>
		 <form action="<?php echo site_url('generalUserHome/update'); ?>" method="post" enctype="multipart/form-data">
		  <input type="hidden" name="id" id="id" value="<?php echo $profileEditInfo->id; ?>" />
			<div class="col-lg-6">
				  <div class="form-group">
					<label for="country_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Sellect Country Name</span></label>        
					<div>
						<select class="form-control" id="country_id" name="country_id"  tabindex="1" value="<?php echo set_value('country_id'); ?>">
							<option value="" selected>Sellect Country Name</option>
							<?php foreach ($countryInfo as $v){?>
							<option <?php if($profileEditInfo->country_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
							<?php } ?>
					   </select>
					 
					</div>
				</div> 
				
				<div class="space-4"></div>  
				
				<div class="space-4"></div>  
					  <div class="form-group">
						<label for="education_institute"><span style="color: #333; font-size: 16px; font-weight: normal;">Education Institute</span></label>
						<input type="text" class="form-control" id="education_institute" placeholder="Education Institute" name="education_institute" tabindex="5" 
						value="<?php echo $moreprofileEditInfo->education_institute; ?>">
					  </div>
					  <div class="form-group">
						<label for="date_of_birth"><span style="color: #333; font-size: 16px; font-weight: normal;">Date Of Birth</span></label>
						<input name="date_of_birth" type="text" id="date_of_birth" class="form-control date-picker"
						  data-date-format="yyyy-mm-dd" value="<?php echo $moreprofileEditInfo->date_of_birth; ?>"/>
					   
					  </div>
					  
					  <div class="form-group">
						<label for="mother_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Mother Name</span></label>
						<input type="text" class="form-control" id="mother_name" name="mother_name" placeholder="Mother Name"
						 tabindex="6" value="<?php echo $moreprofileEditInfo->mother_name; ?>">
					  </div>
					  
					 
					  <div class="form-group">
						<label for="nationality"><span style="color: #333; font-size: 16px; font-weight: normal;">Nationality</span></label>
						<input type="text" class="form-control" id="nationality" name="nationality" 
						 placeholder="Nationality" tabindex="9" value="<?php echo $moreprofileEditInfo->nationality; ?>">
					  </div>
					  <div class="form-group">
						<label for="address"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Address </span></label>
					   <textarea class="form-control" name="address" id="address" cols="60" rows="" placeholder="Permanent Address"><?php echo $moreprofileEditInfo->address; ?></textarea>
					  </div>
					  <div class="form-group">
						<label for="country_phone"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Country Phone</span></label>
						<input type="text" class="form-control" id="country_phone" name="country_phone" placeholder="Present Country Phone"
						 tabindex="13" value="<?php echo $moreprofileEditInfo->country_phone; ?>">
					  </div>
					  <div class="form-group">
						<label for="email"><span style="color: #333; font-size: 16px; font-weight: normal;">Present  Email</span></label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Present  Email" tabindex="15" 
						value="<?php echo $moreprofileEditInfo->email; ?>">
					  </div>
					  <div class="form-group">
						<label for="address_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Address</span></label>
						<textarea class="form-control" name="address_permanent" id="address_permanent" cols="60" rows="" placeholder="Permanent Address" tabindex="17"><?php echo $moreprofileEditInfo->address_permanent; ?></textarea> 
					  </div>
					  <div class="form-group">
						<label for="country_phone_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Country Phone </span></label>
						<input type="text" class="form-control" id="country_phone_permanent" name="country_phone_permanent" 
						placeholder="Permanent Country Phone" tabindex="19" value="<?php echo $moreprofileEditInfo->country_phone_permanent; ?>"> 
					  </div>
					  <div class="form-group">
						<label for="email_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Email</span></label>
						<input type="email" class="form-control" id="email_permanent" name="email_permanent" placeholder="Permanent Email" tabindex="21" 
						value="<?php echo $moreprofileEditInfo->email_permanent; ?>" >
					  </div>
					  
														   
						 
		   </div>  
			<div class="col-lg-6">

			     <div class="form-group">
					<label for="city_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Sellect City Name</span></label>        
					<div>
					   <select class="form-control" id="city_id" name="city_id"  tabindex="1" >
							<option value="" selected>Sellect City Name</option>
							<?php foreach ($cityInfo as $v){?>
							<option <?php if($profileEditInfo->city_id == $v->id){ ?> selected="selected" <?php } ?> value="<?php echo $v->id; ?>"><?php echo $v->city_name; ?></option>
							<?php } ?>
					   </select>
					 
						
					</div>
				</div>
				
				
				<div class="space-4"></div> 
				     <div class="form-group">
						<label for="name"><span style="color: #333; font-size: 16px; font-weight: normal;">Name</span></label>
						<input type="text" class="form-control" id="name" placeholder="User Name" name="name" tabindex="5" 
						value="<?php echo $profileEditInfo->name; ?>">
					  </div> 
					  
					   <div class="form-group">
						<label for="father_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Father Name</span></label>
						<input type="text" class="form-control" id="father_name" name="father_name" placeholder="Father Name" tabindex="7" 
						value="<?php echo $moreprofileEditInfo->father_name; ?>">
					  </div>
					  
					  
					  <div class="form-group">
						<label for="image"><span style="color: #333; font-size: 16px; font-weight: normal;">Image</span></label>
						   <div class="controls">
								<input type="file" id="image" name="image" tabindex="8" />
							</div>
					  </div>
					  <div class="form-group">
						<label for="city"><span style="color: #333; font-size: 16px; font-weight: normal;">Present City</span></label>
						<input type="text" class="form-control" id="city" name="city" placeholder="Present City" tabindex="12" 
						value="<?php echo $moreprofileEditInfo->city; ?>">
					  </div>
					  <div class="form-group">
						<label for="mobile"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Mobile </span></label>
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Present Mobile" tabindex="12" 
						value="<?php echo $moreprofileEditInfo->mobile; ?>">
					  </div>
					  <div class="form-group">
						<label for="city_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent City</span></label>
						<input type="text" class="form-control" id="city_permanent" name="city_permanent" placeholder="Permanent City" tabindex="16" 
						value="<?php echo $moreprofileEditInfo->city_permanent; ?>">
					  </div>
					  <div class="form-group">
						<label for="mobile_permanent"><span style="color: #333; font-size: 16px; font-weight: normal;">Permanent Mobile </span></label>
						<input type="text" class="form-control" id="mobile_permanent" name="mobile_permanent" placeholder="Permanent Mobile" tabindex="18" 
						value="<?php echo $moreprofileEditInfo->mobile_permanent; ?>">
					  </div>
					  <div class="form-group">
						<label for="user_id"><span style="color: #333; font-size: 16px; font-weight: normal;">Gender</span></label>
						<select class="form-control" id="gender" name="gender"  tabindex="25" value="<?php echo set_value('gender'); ?>">
								<option <?php if($profileEditInfo->gender == 'Male'){?> selected="selected" <?php } ?> value="Male">Male</option>
								<option <?php if($profileEditInfo->gender == 'Female'){?> selected="selected" <?php } ?> value="Female">Female</option>
						   </select>
					  </div>
					  <div class="form-group">
						<label for="password"><span style="color: #333; font-size: 16px; font-weight: normal;">Marital Status </span></label>
						<select class="form-control" id="marital_status" name="marital_status"  tabindex="25" value="<?php echo set_value('marital_status'); ?>">
								<option <?php if($profileEditInfo->marital_status == 'Married'){?> selected="selected" <?php } ?> value="Married">Married</option>
								<option <?php if($profileEditInfo->marital_status == 'Unmarried'){?> selected="selected" <?php } ?> value="Unmarried">Unmarried</option>
						   </select>
						<p class="first"></p>
					  </div>
					  <div class="form-group">
						<label for="religion"><span style="color: #333; font-size: 16px; font-weight: normal;">Religion</span></label>
						<input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" tabindex="16" 
						value="<?php echo $moreprofileEditInfo->religion; ?>">
					  </div>
					  <div class="form-group">
						<label for="status"><span style="color: #333; font-size: 16px; font-weight: normal;">Status</span></label>        
						<div>
						   <select class="form-control" id="status" name="status"  tabindex="25" value="<?php echo set_value('status'); ?>">
								<option <?php if($profileEditInfo->status == 'Active'){?> selected="selected" <?php } ?> value="Active">Active</option>
								<option <?php if($profileEditInfo->status == 'In Active'){?> selected="selected" <?php } ?> value="In Active">In Active</option>
						   </select>
						</div>
					</div>
				 </div> 
		<div class="row">&nbsp;</div>   
		<div class="row">&nbsp;</div>  
			<div class="row">
			  <div class="col-lg-12" align="center">
			  <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Resume</button>
			  </div>
			</div>
			<div class="row">&nbsp;</div>   
			<div class="row">&nbsp;</div>  
			 </form>        
		</div>
		
		<div id="career" class="tab-pane fade">
		   <div class="row">&nbsp;</div>
		   <form action="<?php echo site_url('generalUserHome/carrerAction'); ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="id" value="<?php echo $moreprofileEditInfo->user_id; ?>" />
                                <div class="col-lg-12">
                                <div class="form-group">
                                
                                <div>
                                <div class="form-group">
                                <label for="country_phone"><span style="color: #333; font-size: 16px; font-weight: normal;">Objective</span></label>
                                
                                <textarea class="form-control" name="objective" id="objective" cols="" placeholder="Objective"><?php echo $moreprofileEditInfo->objective; ?></textarea>
                                </div>
                                </div>
                                </div> 
                                
                                <div class="space-4"></div>  
                                
                                <div class="form-group">
                                <label for="father_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Present Salary</span></label>
                                <input type="text" class="form-control" id="present_sallary" name="present_sallary" placeholder="Present Salary" tabindex="7" value="<?php echo $moreprofileEditInfo->present_sallary; ?>">
                                
                                
                                </div>
                                
                                
                                
                                <div class="space-4"></div>  
                                <div class="form-group">
                                <label for="father_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Expected Salary</span></label>
                                <input type="text" class="form-control" id="expected_sallary" name="expected_sallary" placeholder="Expected Salary" tabindex="7" value="<?php echo $moreprofileEditInfo->expected_sallary; ?>">
                                </div>
                                <div class="form-group">
                                <label for="date_of_birth"><span style="color: #333; font-size: 16px; font-weight: normal;">Looking for</span></label>
                                
                                <label>
                                <input type="radio" name="level" value="Entry Level" id="level" <?php if($moreprofileEditInfo->level == 'Entry Level' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
                                 Entry Level&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                
                                <label>
                                <input type="radio" name="level" value="Mid Level" id="level" <?php if($moreprofileEditInfo->level == 'Mid Level' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
                                 Mid Level&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                
                                <label>
                                <input type="radio" name="level" value="Top Level" id="level" <?php if($moreprofileEditInfo->level == 'Top Level' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
                                 Top Level</label>
								 
                                
                                
                                </div>
                                
                                <div class="form-group">
                                <label for="date_of_birth"><span style="color: #333; font-size: 16px; font-weight: normal;">Available for</span></label>
                                
                                <label>
                                <input type="radio" name="available" id="available" value="Full time" id="RadioGroup1_0" <?php if($moreprofileEditInfo->available == 'Full time' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
                                 Full time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                
                                <label>
                                <input type="radio" name="available" id="available" value="Part time" id="RadioGroup1_1" <?php if($moreprofileEditInfo->available == 'Part time' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
                                 Part time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                
                                <label>
                                <input type="radio" name="available" id="available" value="Contact" id="RadioGroup1_2" <?php if($moreprofileEditInfo->available == 'Contact' ){ ?> checked="checked" checkbox="checked" <?php } ?> />
                                 Contact</label>
                                
                                </div>
                                
                                </div>  
                                
                                <div class="row">&nbsp;</div>   
                                <div class="row">&nbsp;</div>  
                                <div class="row">
                                <div class="col-lg-12" align="center">
                                <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Resume</button>
                                </div>
                                </div>
                                <div class="row">&nbsp;</div>   
                                <div class="row">&nbsp;</div>  
                                </form>
		</div>
		
		<div id="education" class="tab-pane fade">
		  <div class="row">&nbsp;</div>
		    <form action="<?php echo site_url('generalUserHome/educationAction'); ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="id" value="<?php echo $moreprofileEditInfo->user_id; ?>" />
                                <div class="col-lg-12">
                                <div class="form-group">
                                
                                <div>
                                <div class="form-group">
									<label for="education_level"><span style="color: #333; font-size: 16px; font-weight: normal;">Level of Education</span></label>        
									<div>
									   <select class="form-control" id="education_level" name="education_level"  tabindex="2" value="<?php echo set_value('education_level'); ?>">
											<option <?php if($moreprofileEditInfo->education_level == 'Secondary'){?> selected="selected" <?php } ?> value="Secondary">Secondary</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Higher Secondary'){?> selected="selected" <?php } ?>  value="Higher Secondary">Higher Secondary</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Diploma'){?> selected="selected" <?php } ?> value="Diploma">Diploma</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Bachelor/Honors'){?> selected="selected" <?php } ?>  value="Bachelor/Honors">Bachelor/Honors</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Masters'){?> selected="selected" <?php } ?> value="Masters">Masters</option>
											<option <?php if($moreprofileEditInfo->education_level == 'Doctoral'){?> selected="selected" <?php } ?>  value="Doctoral">Doctoral</option>
									   </select>
									</div>
								</div>
								
								
                                </div>
                                </div> 
                                
                                <div class="space-4"></div>  
                                
                                <div class="form-group">
                                <label for="degree_title"><span style="color: #333; font-size: 16px; font-weight: normal;">Exam/Degree Title</span></label>
                                <input type="text" class="form-control" id="degree_title" name="degree_title" placeholder="Exam/Degree Title" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->degree_title; ?>">
                                
                                
                                </div>
                                
                                <div class="space-4"></div>  
                                <div class="form-group">
                                <label for="group"><span style="color: #333; font-size: 16px; font-weight: normal;">Concentration/ Major/Group</span></label>
                                <input type="text" class="form-control" id="group" name="group" placeholder="Concentration/ Major/Group" tabindex="7" value="<?php echo $moreprofileEditInfo->group; ?>">
                                </div>
                                <div class="form-group">
                                <label for="institute_name"><span style="color: #333; font-size: 16px; font-weight: normal;">Institute Name</span></label>
                                <input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="Institute Name" tabindex="7" value="<?php echo $moreprofileEditInfo->institute_name; ?>">
                                </div>
                                
                                <div class="form-group">
									<label for="result"><span style="color: #333; font-size: 16px; font-weight: normal;">Result</span></label>        
									<div>
									   <select class="form-control" id="result" name="result"  tabindex="2" value="<?php echo set_value('result'); ?>">
											<option <?php if($moreprofileEditInfo->result == 'First Division'){?> selected="selected" <?php } ?> value="First Division">First Division</option>
											<option <?php if($moreprofileEditInfo->result == 'Second Division'){?> selected="selected" <?php } ?>  value="Second Division">Second Division</option>
											<option <?php if($moreprofileEditInfo->result == 'Third Division'){?> selected="selected" <?php } ?> value="Third Division">First Division</option>
											<option <?php if($moreprofileEditInfo->result == 'Grade'){?> selected="selected" <?php } ?>  value="Grade">Grade</option>
											
									   </select>
									</div>
								</div>
                                
                                <div id="cgpa" class="row" style="display:none;">
								 <div class="col-lg-6">
									<div class="form-group">
									<label for="cgpa"><span style="color: #333; font-size: 16px; font-weight: normal;">CGPA</span></label>
										<input type="text" class="form-control" id="cgpa" name="cgpa" placeholder="CGPA" tabindex="7" value="<?php echo $moreprofileEditInfo->cgpa; ?>">
								  </div> 
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
									<label for="scale"><span style="color: #333; font-size: 16px; font-weight: normal;">Scale</span></label>
										<input type="text" class="form-control" id="scale" name="scale" placeholder="Scale" tabindex="7" value="<?php echo $moreprofileEditInfo->scale; ?>">
									</div> 
									</div>
                                </div>
                                
                                <div class="form-group">
                                <label for="pass_year"><span style="color: #333; font-size: 16px; font-weight: normal;">Year of Passing</span></label>
                                <input type="text" class="form-control" id="pass_year" name="pass_year" placeholder="Year of Passing " tabindex="7" value="<?php echo $moreprofileEditInfo->pass_year; ?>">
                                </div>
                                <div class="form-group">
                                <label for="duration"><span style="color: #333; font-size: 16px; font-weight: normal;">Duration</span></label>
                                <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration" tabindex="7" value="<?php echo $moreprofileEditInfo->duration; ?>">
                                </div>
                                <div class="form-group">
                                <label for="achievement"><span style="color: #333; font-size: 16px; font-weight: normal;">Achievement </span></label>
                                <input type="text" class="form-control" id="achievement" name="achievement" placeholder="Achievement" tabindex="7" value="<?php echo $moreprofileEditInfo->achievement; ?>">
                                </div>
                                </div>   
                                <div class="row">&nbsp;</div>  
                                <div class="row">
                                <div class="col-lg-12" align="center">
                                <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Resume</button>
                                </div>
                                </div>
                                <div class="row">&nbsp;</div>   
                                <div class="row">&nbsp;</div>  
                                </form>
		</div>
		<div id="training" class="tab-pane fade">
		   <div class="row">&nbsp;</div>
		    <form action="<?php echo site_url('generalUserHome/trainingAction'); ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="id" value="<?php echo $moreprofileEditInfo->user_id; ?>" />
                                <div class="col-lg-12">
                                <div class="form-group">
                                
                                <div>
                                <div class="form-group">
                                <label for="training_title"><span style="color: #333; font-size: 16px; font-weight: normal;">Training Title</span></label>
                                <input type="text" class="form-control" id="training_title" name="training_title" placeholder="Training Title" tabindex="7"
								 value="<?php echo $moreprofileEditInfo->training_title; ?>">
                             
                                </div>
                                
                                 <div class="form-group">
                                <label for="training_tropic"><span style="color: #333; font-size: 16px; font-weight: normal;">Topic Covered</span></label>
                                <textarea class="form-control"  name="training_tropic" id="training_tropic" cols="" rows=""><?php echo $moreprofileEditInfo->training_tropic; ?></textarea>
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_institute"><span style="color: #333; font-size: 16px; font-weight: normal;">Institute</span></label>
                                <input type="text" class="form-control" id="training_institute" name="training_institute" placeholder="Institute" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_institute; ?>">
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_country"><span style="color: #333; font-size: 16px; font-weight: normal;">Country</span></label>
                                <input type="text" class="form-control" id="training_country" name="training_country" placeholder="Country" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_country; ?>">
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_location"><span style="color: #333; font-size: 16px; font-weight: normal;">Location</span></label>
                                <input type="text" class="form-control" id="training_location" name="training_location" placeholder="Location" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_location; ?>">
                             
                                </div>
                                 <div class="form-group">
                                <label for="training_year"><span style="color: #333; font-size: 16px; font-weight: normal;">Training Year</span></label>
                                <input type="text" class="form-control" id="training_year" name="training_year" placeholder="Training Year" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_year; ?>">
                             
                                </div>
                                <div class="form-group">
                                <label for="training_duration"><span style="color: #333; font-size: 16px; font-weight: normal;">Duration</span></label>
                                <input type="text" class="form-control" id="training_duration" name="training_duration" placeholder="Duration" tabindex="7" 
								value="<?php echo $moreprofileEditInfo->training_duration; ?>">
                             
                                </div>
                                </div>
                                </div> 
                             
                                </div>   
                                <div class="row">&nbsp;</div>  
                                <div class="row">
                                <div class="col-lg-12" align="center">
                                <button type="submit" id="submit" name="submit" class="btn btn-info center-block" tabindex="1">Update Resume</button>
                                </div>
                                </div>
                                <div class="row">&nbsp;</div>   
                                <div class="row">&nbsp;</div>  
                                </form>
		</div>
		
	</div>
	<div class="row">
	  <div class="col-lg-12">
	  </div>
	</div>
	<div class="row">&nbsp;</div>   
		
	</div>                
</div>
<script>
	
	$("#region_id").change(function() {
		var region_id = $("#region_id").val();			
		$.ajax({
			url : SAWEB.getSiteAction('generalUserHome/countryEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : region_id },
			dataType : "html",
			success : function(data) {			
				$("#country_id").html(data);
			}
		});
		
	});	
	$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('generalUserHome/cityEdit'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});	
   $("#country_id").change(function() {
		var nationality = $("#country_id option:selected").attr('data-nationality');
		var countryCode = $("#country_id option:selected").attr('data-country-code');			
		
				$("#nationality").val(nationality);
				$("#mobile").val(countryCode);
			
		});
		
</script>