<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
  <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
  <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
 

  
  </head>
  <body>
        <div class="container">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('generalUserPanel/generalHeaderPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('generalUserPanel/generalMenuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		       <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">&nbsp;</div> 
							  <div class="row">
							  <?php $this->load->view('generalUserPanel/leftMenuPage'); ?>
									<div class="col-lg-9" align="center">

                    <div class="welll welll-lg" style="padding-top:0px; padding-right:15px; padding-left:15px;"> 
                      <div class="row">
                        <div class="col-lg-12" align="left" style="padding:8px; background-color:#CCCCCC; border-radius:3px;">
                           <span style="font-size:16px; font-weight:bold;"><i class="glyphicon glyphicon-envelope"></i> View Message</span><span style="float:right; padding-right:50px;font-size:18px;"><?php echo $updateText ?></span>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-lg-12" style="padding:0px;">
                            <table id="inputTable" width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="background-color:#FFFFFF">
                              <tr class="active" style="font-weight:bold;">
                                <td width="22%">Message From</td>
                                <td width="37%">Message </td>
                                <td width="20%">Date Time </td>
                                <td width="10%">Reply</td>
                              </tr>


                              <?php 
                                  if(isset($UserWiseMsgInfo)){
                                  foreach ($UserWiseMsgInfo as $v){ 
                                   $viewLink  = array('generalUserHome','messageDetails', $v->id);
                              ?>

                              <tr> 
                                <td><?php echo $v->name ?></td>
                                <td><?php echo $v->message ?></td>
                                <td><?php echo $v->date_time ?></td>
                                <td><a class="rep" style="color:#000000"  href="<?php echo site_url($viewLink) ?>">reply</a></td>
                              </tr> 
                              <?php } }  ?>
                              <tr class="active" style="font-weight:bold;">
                                <td width="17%"> </td>
                                <td width="47%"></td>
                                <td width="15%"></td>
                                <td width="10%"><?php echo $this->pagination->create_links(); ?> </td>
                              </tr>
                            </table>  
                          </div>
                      </div>
                      <div class="row">&nbsp;</div>   
                      
                    </div>                

									   
									</div>
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						        <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
             
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>


     <div class="modal fade" id="msgDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">       
          
        </div>
      </div>
    </div>

    

     <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
     <script type="text/javascript">


          $('.rep').on('click', function(e){
          var url = $(this).attr('href');
          
          $.ajax({
            url : url, // URL TO LOAD BEHIND THE SCREEN
            type : "GET",
            dataType : "html",
            success : function(data) {      
              $("#msgDetails .modal-content").html(data);
              $("#msgDetails").modal({
                keyboard: false,
                backdrop: 'static',
              });
              $("#msgDetails").modal('show');
            }
          });

          e.preventDefault();   
        });

        


    </script>
  </body>
</html>