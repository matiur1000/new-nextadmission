<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CommentPopup extends CI_Controller {

	static $model 	 = array('M_basic_manage','M_advertisement_manage','M_menu_manage','M_submenu_manage','M_deeper_sub','M_news_and_event_manage','M_user_post_manage','M_all_user_registration','M_post_comment',
		'M_reply_comment','M_organization_user_registration','M_general_user_registration','M_visitor_count','M_live_chat');
		static $helper   = array('url','userauthentication','generalauthentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
			$this->load->helper('cookie');
	
		}
	

		public function index($post_id)
	
		{	


			if( isActiveUser() ) {
		      $userId  					= getUserName();
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($userId);
			  $visitorId       			= $userDetails->id;
			  $data['userDetails']      = $userDetails;
		   }
			$data['page_title']  		= self::Title;	
			$data['totalNewMsg']	    = $this->M_live_chat->findAllNewMsg($userDetails->id);
			$data['UserCmntInfo']	    = $this->M_post_comment->findAllDetailes($post_id);
			$data['UserPostInfo']	    = $this->M_user_post_manage->findByPostInfo($post_id);
			$organizeId       			= $data['UserPostInfo']->user_id;

			 $linkUrl     = uri_string();
			 $current_url = current_url();


			 if ($userDetails->user_type =='General user') {
			    $datav['user_type']    = "general";
			} else {
			    $datav['user_type']    = "organize";
			}

			   $datav['organize_id']    = $organizeId;
			if (!empty($visitorId)) {
				$datav['visitor_id']    = $visitorId;
			} else {
			    $datav['visitor_id']    = "unknown";	
			}
                 $datav['blog_id']    	= $post_id;
                 $datav['visit_link']   = $linkUrl;
                  $datav['page_type']   = "Blog";
                 $datav['date_time']   	= date("Y-m-d H:i:s");
                 $datav['date']    		= date("Y-m-d");


			$urlArray = unserialize(get_cookie('visitor_tracking'));

			if(!in_array($current_url, $urlArray)) {
				$urlArray[] = $current_url;
			
			  	$cookieValue = array(
			        'name'   => 'visitor_tracking',
			        'value'  => serialize($urlArray),
			        'expire' => '86400',
			        'domain' => '.localhost',
			        'path'   => '/'
		        );
               
               if ($organizeId > 0) {
                 $this->M_visitor_count->save($datav);
               }
	          
	           set_cookie($cookieValue);
			}

			$data['post_id']			= $post_id;
			
			$this->load->view('commentPopupform', $data);
	
		}
		

		
		
		public function commentStore()
		{	
		  
		   $UserId    					= $this->session->userdata('UserId');
		   $userDetails                 = $this->M_all_user_registration->findByUserName($UserId);
		  
		  if(!empty($userDetails)){
			  $data['user_id'] 			= $userDetails->id;
		   }	  
			
		   $data['comment'] 			= $this->input->post('comment');
		   $data['post_id'] 			= $this->input->post('postId');
		   $data['comment_date'] 		= date("Y-m-d H:i:s");
            if($data['comment'] !=''){
		     $this->M_post_comment->save($data);
            }
		   redirect('commentPopup/comntListView/'.$data['post_id']);
			
		 
	  }
	  
	  
	   public function replyStore($postId,$comment_id)
		{	
		   $UserId    = $this->session->userdata('UserId');
		   
			  $userDetails   		    = $this->M_all_user_registration->findByUserName($UserId);
			  if(!empty($userDetails)){
			  $data['user_id'] 			= $userDetails->id;
			 } 
		    $data['reply'] 					= $this->input->post('reply');
		    $data['post_id'] 				= $postId;
		    $data['comment_id'] 			= $comment_id;
			$data['reply_date'] 			= date("Y-m-d H:i:s");
			$this->M_reply_comment->save($data);
			redirect('commentPopup/replyListView/'.$postId);
			
		 
	  }
	
	
	  public function comntListView($post_id)
	
		{	
			$data['page_title']  		= self::Title;	
			$data['UserCmntInfo']	    = $this->M_post_comment->findAllDetailes($post_id);
			$data['post_id']			= $post_id;
			
			$this->load->view('commentListPage', $data);
	
		}
		
		
		public function replyListView($post_id)
	
		{	
			$data['page_title']  		= self::Title;	
			$data['UserCmntInfo']	    = $this->M_post_comment->findAllDetailes($post_id);
			$data['post_id']			= $post_id;
			
			$this->load->view('replyListPage', $data);
	
		}
		
		
		
		
}