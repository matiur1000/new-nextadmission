  
 <?php $this->load->view('header'); ?>
 
 
 
<link href="<?php echo base_url('resource/nextadmission/css/contact_us.css'); ?>" rel="stylesheet">
  
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
var map;
function initialize() {
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(41.8633006, 12.547736900000018)
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

 &nbsp;
  &nbsp;
   &nbsp;
 <div class="container">
    <div class="row bottom_panel">
        <div class="col-md-8">
        <div class="col-md-12">
        <div class="leftpane">
              
              <div class="map-canvas col-md-12">
               <div class="title_text">Gmap</div>
               <style type="text/css">.google-maps {
        position: relative;
        padding-bottom: 50%; // This is the aspect ratio-you can change this 
        height: 0;
        overflow: hidden;
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
</style>

<div class="google-maps">

// Enter your embed code (iframe) bellow this line

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d767.6683979826034!2d90.38971215323997!3d23.75925558890679!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8a123db9e3b%3A0x13990d514ea340c1!2sGlobal+Star+Ltd!5e0!3m2!1sen!2sbd!4v1484820336432" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

//To get the embed code, follow this link 

</div> 
</div>
</div>
             <div class="clr"></div>
              <div class=" col-md-12 form_top">
              <div class="title_text col-md-12 dsplay">Contact via Email</div>
               <form id="contactUs">
                  <table style="width: 100%;">
                      <tbody>
                      <tr>
                          <td><input name="name" placeholder="Name" class="required"></td>
                      </tr>
                      <tr>
                          <td><input name="email" type="email" placeholder="Email" class="required email"></td>
                      </tr>
					  <tr>
                          <td><input name="phone" type="phone" placeholder="Phone" class="required phone"></td>
                      </tr>
                      <tr>
                         
                          <td><textarea name="message" placeholder="Message" class="required"></textarea></td>
                      </tr>
                      
                       </tbody>
                   </table>
                  <div class="col-md-3 btnb"><input type="submit" value="Submit" id="submit"></div>
                  <div class="col-md-3 btnb"><input type="submit" value="Cancel"></div>
                  </form>

                
              </div>
             


            
        </div>       
          
        </div>
        <div class="col-md-4">
          <div class="rightpane"> 
              <div class="address"> 
                <div class="title_text">Office Address</div>
                <div class="address_area">
                <span class="tilecontac">Dhaka Office</span><br>
						 &nbsp; &nbsp;31 Malek Tower (9th floor- 2nd lift)<br>
						 &nbsp; &nbsp;Farmgate, Dhaka, Bangladesh<br>
						 &nbsp; &nbsp;Mobile: +880 1911 342308<br>
						  &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;+880 1780 300059<br>
						 &nbsp; &nbsp;Email: smtbangladesh@yahoo.com<br>
               
                </div>
				
				<div class="address_area">
                <span class="tilecontac">Chittagong Office</span><br>
						 &nbsp; &nbsp;Walikhan Mansion<br>
						 &nbsp; &nbsp;603 SK Mujib Road (3rd floor)<br>
						 &nbsp; &nbsp;Agrabad, Choumuhuni Moar,<br>
						 &nbsp; &nbsp;Chittagong<br>
						 &nbsp; &nbsp;Mobile: +880 1911 342308<br>
						 &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;+880 01715 015972<br>
						 &nbsp; &nbsp;Email: smtbangladesh@yahoo.com<br>
               
                </div>
				
              </div>
			  
			  
            <br>
            <div class="numbers">
                <div class="title_text">Contact</div>
                <div class="numbers_area"> 
                <span class="tilecontac">Email</span><br>
                 &nbsp; &nbsp;smtbangladesh@yahoo.com<br>
				 &nbsp; &nbsp;globalstarltd@gmail.com<br>
                <br>
                <br>
                <span class="tilecontac">Phone</span><br>
                <i class="fa fa-phone" aria-hidden="true"></i>
                  &nbsp; &nbsp; +880 1911 342308<br>
				  &nbsp; &nbsp; +880 1780 300059<br>
                 <br>
                <span class="tilecontac"> Skype </span><br>
                <i class="fa fa-whatsapp" aria-hidden="true"></i>
                &nbsp; &nbsp;globalstarltd
                </div>
            </div>
            <br>
       </div>
        </div> 
    </div>
  </div>

 
 
 <?php $this->load->view('footer'); ?>


