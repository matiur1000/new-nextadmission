<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
        <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
  <script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
 
    
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		     <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-12">  <!--col 9 Start-->   
					   		
					       <div class="welll welll-lg">
						    
                            <div class="row">
								<div class="col-lg-12 col-xs-3" align="justify">
						   			<div style="float:left; padding-right:20px;">
<img class="img-rounded thumbnail" style="border-radius:4px;" src="<?php echo base_url("Images/News_image/$UserNewsInfo->image"); ?>"
 width="300" height="220"  alt=""/></div>
<p class="text_app"><span style="font-size:24px; color:#000"> <?php echo $UserNewsInfo->title; ?> <span style="padding-left:280px;">
<?php if(!empty($orgranId)){ ?> 
	<a href="#" class="grean" data-id="<?php echo $UserNewsInfo->id; ?>"><button type="button"  class="btn btn-info">Post Edit</button></a></span></span><br/>
<?php } ?>
									   <span style="color:#0e73a9"> <?php echo $UserNewsInfo->date; ?></span><br/>
									<?php echo $UserNewsInfo->description; ?></p>
								</div>
							 </div>	
							 			  
						   </div>
				        </div>             <!--col 9 end-->
			     </div> 
				 
				 
				 <div class="row viewup" style="display:none;">
								  <div class="col-lg-11">
									<form action="<?php echo site_url('home/newseventUpdate'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
									 <input type="hidden" id="id" name="id">
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Title</label>
										<div class="col-sm-7">
										  <input type="text" class="form-control" id="title" name="title" placeholder="Title">
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Date</label>
										<div class="col-sm-7">
										  <input type="text" class="form-control date-picker" id="date" name="date" data-date-format="yyyy-mm-dd" placeholder="Date">
										</div>
									  </div>


									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Select news type</label>
										<div class="col-sm-7">
										   <select name="news_type" id="news_type" class="form-control">
										     <option selected="selected" value="">Select news type</option>
										     <option value="image">Image</option>
										     <option value="video">Video</option>
										   </select>
										</div>
									  </div>

									  <div id="videoType" class="form-group" style="display:none">
										<label for="inputEmail3" class="col-sm-3 control-label">Select video type</label>
										<div class="col-sm-7">
										   <select name="video_type" id="video_type" class="form-control">
										     <option selected="selected" value="">Select video type</option>
										     <option value="embeded">Embeded code</option>
										     <option value="video">Self Video</option>
										   </select>
										</div>
									  </div>

									  <div id="upvideo" class="form-group" style="display:none">
										<label for="inputPassword3" class="col-sm-3 control-label">Upload video</label>
										<div class="col-sm-7">
										  <input type="file" id="video" name="video" tabindex="8" />
										</div>
									  </div>

									  <div id="embeded" class="form-group" style="display:none">
										<label for="inputPassword3" class="col-sm-3 control-label">Embeded code</label>
										<div class="col-sm-7">
										<textarea class="form-control" name="embeded_code" id="embeded_code" cols="60" rows="" placeholder="Organization Address"></textarea>
										</div>
									  </div>
									  

									  <div id="newsImage" class="form-group" style="display:none">
										<label for="inputPassword3" class="col-sm-3 control-label">News Image</label>
										<div class="col-sm-7">
										  <input type="file" id="image" name="image" tabindex="8" />
										</div>
									  </div>

									  <div class="form-group">
										<label for="description" class="col-sm-3 control-label">Details</label>
										<div class="col-sm-7">
											<textarea class="form-control" name="description" id="ajaxfilemanager" cols="60" rows="20"></textarea></div>
									  </div>
									  
									  <div class="form-group">
										<div class="col-sm-offset-3 col-sm-10">
										  <button type="submit" class="btn btn-primary">Update</button>
										</div>
									  </div>
									</form>
									</div>
									<div class="col-lg-1"></div>
								         
								</div>
				  
           <div class="row">
          <div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
           <?php $this->load->view('newsEventManagePage'); ?>
            </div>                <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
       </div>

						
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
    <!--SHARE THIS-->
	<script type="text/javascript">stLight.options({publisher: "7521b38c-5f2b-4808-b7b5-8057deecb289", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <script>
       $('.goto-news').on('click', function(){
      console.log($('#news'));
      console.log($('#news').offset().top);
      $('body,html').animate({
        scrollTop: ($('#news').offset().top - 50),
        }, 1200
      );
    }); 


       $(function(){
          var $mwo = $('.marquee-with-options');
          $('.marquee').marquee();
          $('.marquee-with-options').marquee({
              //speed in milliseconds of the marquee
              speed: 9000,
              //gap in pixels between the tickers
              gap: 5,
              //gap in pixels between the tickers
              delayBeforeStart: 0,
              //'left' or 'right'
              direction: 'up',
              //true or false - should the marquee be duplicated to show an effect of continues flow
              duplicated: true,
              //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
              pauseOnHover: true
          });
          //Direction upward
          $('.marquee-vert').marquee({
              direction: 'up',
              speed: 1500
          });
          //pause and resume links
          $('.pause').click(function(e){
              e.preventDefault();
              $mwo.trigger('pause');
          });
          $('.resume').click(function(e){
              e.preventDefault();
              $mwo.trigger('resume');
          });
          //toggle
          $('.toggle').hover(function(e){
              $mwo.trigger('pause');
          },function(){
              $mwo.trigger('resume');
          })
          .click(function(e){
              e.preventDefault();
          })
      });

		
		$(document).on("click", ".grean", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('home/newsuserIdEdit'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('.viewup').css('display','block');
					$('#id').val(data.id);
					$('#title').val(data.title);
					$('#date').val(data.date);
					$('#embeded_code').val(data.embeded_code);
					$('#ajaxfilemanager').val(data.description);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
	

    </script>
    
  </body>
</html>