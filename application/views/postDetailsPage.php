<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

   <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
    
   
	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--MENU CSS -->
        
      
         <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('resource/js/menu_script.js'); ?>"></script>
		<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
        <script src="<?php echo base_url('resource/source/marquee.js'); ?>"></script>
		<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>  
		<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/typeahead.bundle.min.js'); ?>"></script>
	  <script type="text/javascript" language="javascript">
		$('document').ready(function () {
			 $('.trigger').click(function () {
				var currentId = '#' + $('.reply:visible').prop('id');
				var newId = $(this).data('rel');
				$('.reply').fadeOut();
				if (currentId != newId) {
					$(newId).fadeIn();
				}
			});
		});
	 </script>
    
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>           
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>      
              <div class="row">  <!--row Start-->           
		     <div class="col-lg-12"><!--col Start-->  
			    <div class="row"><!--row Start-->  
					   <div class="col-lg-9">  <!--col 9 Start--> 
					       <div class="welll welll-lg">
							  <div class="row">
							   <form method="post" action="#" name="formSearch" class="form-inline">
								 <div class="col-lg-3">
									<label for="search"><h2 style="font-size:15px;">Search Education Provider </h2></label>
								 </div>
								 <div class="col-lg-9" align="center" style="padding-left:2px;">
								   <input type="search" name="search" id="search" class="form-control"  placeholder="Search by Programs" 
								   style="width:80%;"> 
										<button type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Search</button>  
										     
								 </div>
								 </form>
								</div>
							 
							  <div class="row">
							  <?php $this->load->view('leftSidebarPage'); ?>
									<div class="col-lg-9" align="center">
									<?php $this->load->view('middlePostDetailsPage'); ?>
									</div>
								
							  
							 
						   </div>
						   </div>
				        </div>             <!--col 9 end--> 
				
						   <?php $this->load->view('rightSidebarPage'); ?>                 <!--col 3 end--> 
			     </div>                  <!--row end--> 
		  	 </div>                   <!--col end-->  
		  </div>
             <div class="row">&nbsp;</div>     
              <div class="row"><div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;">Advertisement</div></div>  
			   <?php $this->load->view('advertisementManagePage'); ?>
             <div class="row">&nbsp;</div>     
              <div class="row"><div class="col-lg-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:11px;">News and Event</div></div>  
			  <?php $this->load->view('newsEventManagePage'); ?>
       </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
   <script>
	
	
	   /*$('body').on('mouseover', '.preview', function() 
			{
				var e = $(this);
				if (e.data('title') == undefined)
				{
					// set the title, so we don't get here again.
					e.data('title', e.find('.popover-title').text());
			
					// set a loader image, so the user knows we're doing something
					e.data('content', e.find('.popover-content').html());
					e.popover({ html : true, trigger : 'hover'}).popover('show');
			
					// retrieve the real content for this popover, from location set in data-href
					$.get(e.data('href'), function(response)
					{
						// set the ajax-content as content for the popover
						e.data('content', response.html);
			
						// replace the popover
						e.popover('destroy').popover({ html : true, trigger : 'hover'});
			
						// check that we're still hovering over the preview, and if so show the popover
						if (e.is(':hover'))
						{
							e.popover('show');
						}
					});
				}
			});*/
	</script>
  </body>
</html>