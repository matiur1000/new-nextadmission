<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		.visa:hover{ border-radius:10px; background: #e3e3e3;}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
<div class="col-lg-8 col-md-8 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
<h2><strong><span style="color: #3366ff;">অস্ট্রেলিয়া সম্পর্কে</span></strong></h2>
<span style="color: #000000;">অস্ট্রেলিয়া (ইংরেজি: Australia) হল একটি মহাদেশ। অস্ট্রেলিয়ার মূল ভূখণ্ড থেকে মহাদেশীয় অংশটিকে আলাদা করে বোঝাতে এই মহাদেশকে সাহুল (ইংরেজি: Sahul), অস্ট্রালিনিয়া (ইংরেজি: Australinea) বা মেগানেশিয়া (ইংরেজি: Meganesia) নামেও উল্লেখ করা হয়ে থাকে। অস্ট্রেলিয়ার মূল ভূখণ্ড, তাসমানিয়া, নিউ গিনি, সারেম, ও সম্ভবত টিমোর ও আশেপাশের দ্বীপগুলিকে নিয়ে এই মহাদেশ গঠিত।

অস্ট্রেলিয়া প্রথাগত ইংরেজি ধারণার সাতটি মহাদেশের মধ্যে ক্ষুদ্রতম। অস্ট্রেলিয়ার মূল ভূখণ্ড ও নিউগিনির মধ্যস্থ আরাফুরা সাগর ও টরেস প্রণালী এবং অস্ট্রেলিয়ার মূল ভূখণ্ড ও তাসমানিয়ার মধ্যস্থ বাস প্রণালী—এই সমুদ্রগুলি দ্বারা বিভাজিত একটি মহাদেশীয় সোপানে এই মহাদেশ অবস্থিত। প্লেইস্টোসিন হিমযুগ ও লাস্ট গ্লেসিয়াল ম্যাক্সিমামের সময় (খ্রিস্টপূর্ব ১৮,০০০ অব্দ) যখন সমুদ্রতলের উচ্চতা কম ছিল, তখন এই বিচ্ছিন্ন স্থলভাগগুলি পরস্পর শুষ্ক স্থলভাগ দ্বারা সংযুক্ত ছিল। শেষ দশ হাজার বছরে সমুদ্রতলের উচ্চতা বৃদ্ধি পেয়ে নিম্নভূমিগুলিকে জলমগ্ন করেছে। এর ফলে মহাদেশটিও আজকের নিচু শুষ্ক ও অর্ধ-শুষ্ক মূল ভূখণ্ডটিকে পর্বতময় নিউ গিনি ও তাসমানিয়া দ্বীপপুঞ্জ থেকে বিচ্ছিন্ন করেছে। ভৌগোলিকভাবে মহাদেশীয় সোপানের শেষভাগ থেকে একটি মহাদেশ প্রসারিত রয়েছে যাতে, অধুনা-বিচ্ছিন্ন দ্বীপগুলি মহাদেশের অংশ হতে পারে।[১]</span>
<h2><strong><span style="color: #3366ff;">অস্ট্রেলিয়ায় উচ্চ শিক্ষা</span></strong></h2>
<span style="color: #000000;">চ্চশিক্ষার জন্য কম বেশি সকলের পছন্দের তালিকায় অস্ট্রেলিয়া এক বিরাট অংশ দখল করে আছে।  প্রতি বছর ই কয়েক লাখ শিক্ষার্থী অস্ট্রেলিয়ায় যায় উচ্চশিক্ষার জন্য । বাংলাদেশ থেকেও এর সংখ্যা নেহায়েত কম নয়। কিন্তু যোগ্যতা  থাকা সত্ত্বেও অনেকেই এই সুযোগ থেকে পিছিয়ে পরে কেবল মাত্র সঠিক তথ্যের অভাবে। এ ক্ষেত্রে কিছু দেশি বিদেশি আন্তর্জাতিক ব্যবসায়িক  সংস্থার কথা উল্লেখ করা যেতে পারে, যাদের দ্বারা শিক্ষার্থীরা শুধু আর্থিকভাবে প্রতারিতই হন না,বরং জীবন থেকে হারিয়ে ফেলেন মুল্যবান কিছু সময় ও । এ জন্য দরকার উপযুক্ত তথ্য।</span>
</div>

			<a class="js-open-modal " href="#" data-modal-id="popup1" style="text-decoration:none;">	
		<div class="col-lg-4 col-md-4 col-xs-12" style="text-align: justify;
                text-justify: inter-word; font-size:20px;border: 2px solid rgb(60, 225, 89);
			border-radius: 10px;" id="visa" >	
			<div class="visa">
			
<h2 align="center"><strong><span style="color: #3366ff;">অস্ট্রেলিয়ায় ভর্তি ও ভিসা</span></strong></h2>
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>IELTS 5.5 or English Medium Study</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ 8000 $ to 10000$</br>
<span style="color:#3366ff;">&#10140;</span>স্পন্সারঃ 20 Lac to 40 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>টোটাল কস্টঃ 8 Lac to 10 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 8 week</br>
<span style="color:#3366ff;">&#10140;</span><font style="color:red;">Note:</font> cost is different on according to course & institutions.</br>
</p>
</div>
	</div>
	</a>
		<!--pop up code start-->	
<div id="popup1" class="modal-box">
  <header> <a href="#" class="js-modal-close close">×</a>
<h2 align="center"><strong><span style="color: #3366ff;">অস্ট্রেলিয়ায় ভর্তি ও ভিসা</span></strong></h2>
  </header>
  <div class="modal-body">
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>IELTS 5.5 or English Medium Study</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ 8000 $ to 10000$</br>
<span style="color:#3366ff;">&#10140;</span>স্পন্সারঃ 20 Lac to 40 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>টোটাল কস্টঃ 8 Lac to 10 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 8 week</br>
<span style="color:#3366ff;">&#10140;</span><font style="color:red;">Note:</font> cost is different on according to course & institutions.</br>
</p>
  </div>
  <footer> <a href="#" class=" btn-danger btn-small js-modal-close">Close</a> </footer>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		

              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>