<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		.visa:hover{ border-radius:10px; background: #e3e3e3;}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
<div class="col-lg-8 col-md-8 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
<h2><strong><span style="color: #3366ff;">পোল্যান্ড সম্পর্কে</span></strong></h2>
<span style="color: #000000;">পোল্যান্ড (পোলীয়: Polska পল্‌স্কা), সরকারিভাবে পোল্যান্ড প্রজান্ত্র (Rzeczpospolita Polska রেচ্‌পস্‌পলিতা পল্‌স্কা) ইউরোপ মহাদেশের মধ্যস্থলের একটি রাষ্ট্র ও ঐতিহাসিক অঞ্চল। এর রাজধানীর নাম ওয়ার্‌শ। এর পশ্চিমে জার্মানি, দক্ষিণে চেক প্রজাতন্ত্র ও স্লোভাকিয়া, পূর্বে ইউক্রেন ও বেলারুস, এবং উত্তরে বাল্টিক সাগর, লিথুয়ানিয়া, ও রাশিয়া অবস্থিত। বাল্টিক সাগরে পোল্যান্ডের সাথে ডেনমার্কের জলসীমান্ত রয়েছে। পোল্যান্ড ২০০৪ সালের ১লা মে তারিখ থেকে ইউরোপীয় ইউনিয়নের সদস্য।

প্রায় ১,০০০ বছর আগে পিয়াস্ট রাজবংশের অধীনস্থ রাজ্য হিসাবে পোল্যান্ড সর্বপ্রথম সংগঠিত হয়। ষোড়শ শতকের শেষভাগকে পোল্যান্ডের স্বর্ণযুগ বলা হয়, যখন জাগিয়েলনীয় রাজবংশের অধীনে পোল্যান্ড ইউরোপের সবচেয়ে বৃহৎ, সমৃদ্ধ ও প্রভাবশালী রাষ্ট্রে পরিণত হয়।</span>
<h2><strong><span style="color: #3366ff;">পোল্যান্ডে উচ্চ শিক্ষা</span></strong></h2>
<span style="color: #000000;">উচ্চশিক্ষার জন্য যারা বিদেশ যেতে চান তারা ইউরোপের কেন্দ্রে অবস্থিত পোল্যান্ডকেও বেছে নিতে পারেন। দেশটির পড়াশোনার খরচ তুলনামূলক কম। পোলিশ ভাষার পাশাপাশি ইংরেজি ভাষায়ও পড়াশোনার সুযোগ আছে। তবে পোলিশ ভাষা জানা থাকলে অনেক সুবিধা হয়। দেশটির প্রায় সব বিশ্ববিদ্যালয়েই 'স্কুল অব পোলিশ ল্যাঙ্গুয়েজ অ্যান্ড কালচার' নামের বিভাগ আছে, যেখানে বিদেশি শিক্ষার্থীরা পোলিশ ভাষা শেখার সুযোগ পায়।কম্পিউটার, সিভিল, কেমিক্যাল, আর্কিটেকচার, ইলেকট্রিক্যাল, মেকানিক্যাল ইঞ্জিনিয়ারিং ছাড়াও বিবিএ, মার্কেটিং, ফিন্যান্স, ট্যুরিজম অ্যান্ড হোটেল ম্যানেজমেন্ট, অ্যাগ্রিকালচার, মেডিসিন, ফার্মেসি, ফরেস্ট্রি, ল, জার্নালিজম, মিউজিক অ্যান্ড মিউজিকোলজিসহ চাহিদা আছে, এমন সব বিষয়েই পড়াশোনা করার সুযোগ আছে দেশটিতে।</span>
</div>

			<a class="js-open-modal " href="#" data-modal-id="popup1" style="text-decoration:none;">	
		<div class="col-lg-4 col-md-4 col-xs-12" style="text-align: justify;
                text-justify: inter-word; font-size:20px;border: 2px solid rgb(60, 225, 89);
			border-radius: 10px;" id="visa" >	
			<div class="visa">
			
<h2 align="center"><strong><span style="color: #3366ff;">পোল্যান্ডে ভর্তি ও ভিসা</span></strong></h2>
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>এডমিশন  ফিঃ 200 Euros</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ 2500 to 4000 Euros</br>
<span style="color:#3366ff;">&#10140;</span>ইনস্যুরেন্সঃ 2500 taka</br>
<span style="color:#3366ff;">&#10140;</span>এম্বাসিঃ 7500 taka</br>
<span style="color:#3366ff;">&#10140;</span>Indian Cost: 30,000 taka</br>
<span style="color:#3366ff;">&#10140;</span>থাকার খরচঃ 600 Euros</br>
<span style="color:#3366ff;">&#10140;</span>স্পন্সারঃ 15 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>এয়ার টিকেটঃ70,000 taka</br>
<span style="color:#3366ff;">&#10140;</span>টোটাল কস্টঃ 4 Lac to 5.5 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 6 Month</br>
</p>
</div>
	</div>
	</a>
	
		<!--pop up code start-->	
<div id="popup1" class="modal-box">
  <header> <a href="#" class="js-modal-close close">×</a>
<h2 align="center"><strong><span style="color: #3366ff;">পোল্যান্ডে ভর্তি ও ভিসা</span></strong></h2>
  </header>
  <div class="modal-body">
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>এডমিশন  ফিঃ 200 Euros</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ 2500 to 4000 Euros</br>
<span style="color:#3366ff;">&#10140;</span>ইনস্যুরেন্সঃ 2500 taka</br>
<span style="color:#3366ff;">&#10140;</span>এম্বাসিঃ 7500 taka</br>
<span style="color:#3366ff;">&#10140;</span>Indian Cost: 30,000 taka</br>
<span style="color:#3366ff;">&#10140;</span>থাকার খরচঃ 600 Euros</br>
<span style="color:#3366ff;">&#10140;</span>স্পন্সারঃ 15 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>এয়ার টিকেটঃ70,000 taka</br>
<span style="color:#3366ff;">&#10140;</span>টোটাল কস্টঃ 4 Lac to 5.5 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 6 Month</br>
</p>
  </div>
  <footer> <a href="#" class=" btn-danger btn-small js-modal-close">Close</a> </footer>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		

              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>