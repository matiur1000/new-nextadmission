<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Next Admission</title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="http://localhost/application/resource/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://localhost/application/resource/css/bootstrap-theme.min.css" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="http://localhost/application/resource/css/custom.css" rel="stylesheet">
	<link href="http://localhost/application/resource/css/menu.css" rel="stylesheet">
	<link href="http://localhost/application/resource/css/menu_styles.css" rel="stylesheet">
	<link href="http://localhost/application/resource/css/default.css" rel="stylesheet">
	<link href="http://localhost/application/resource/css/ieonly.css" rel="stylesheet">
	<link href="http://localhost/application/resource/css/login_style.css" rel="stylesheet">
	<link href="http://localhost/application/resource/css/popup.css" rel="stylesheet">
    <script src="http://localhost/application/resource/js/jquery.min.js"></script>
	<script src="http://localhost/application/resource/js/bootstrap.min.js"></script>
	<script src="http://localhost/application/resource/ajax_function.js"></script>
	<script src="http://localhost/application/resource/source/text_scroll.js"></script>
	<script type="text/javascript" language="javascript" src="http://localhost/application/adapter/javascript.html"></script>
	
    <style>
		span{
			font-size: 16px;
		}
	</style>
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			   <style>
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}

	.popover{
    max-width: 100%; /* Max Width of the popover (depending on the container!) */
 }
    .button {
	display: inline-block;
	background: #6FB4FB;
	color: #FFFFFF;
	text-decoration: none;
	padding: 5px 10px;
}

.badge-notify{
   background:red;
   position:relative;
   top: -25px;
   left: -5px;
}

[data-notifications] {
	position: relative;
}

[data-notifications]:after {
	content: attr(data-notifications);
	position: absolute;
	background: red;
	border-radius: 50%;
	display: inline-block;
	padding: 0.3em;
	color: #f2f2f2;
	right: -15px;
	top: -15px;
}

}	

 </style>
 <link href="http://localhost/application/resource/source/headercss.css" rel="stylesheet">

 <script type="text/javascript">
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
	}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

 

   <div class="row">
	<div class="col-lg-3 col-xs-12 col-sm-12 text-right" style="margin-top:10px;">
		<a href="http://localhost/application/home.html">
		  <img src="http://localhost/application/Images/Basic_image/1455373282.png" width="235" height="75" align="right" />
		</a>
	</div>
		<div class="col-lg-5 col-xs-12 col-sm-12" align="center">
		<a target="_blank" href="http://localhost/application/home/orgWiseAdvritismentView/3/11.html">
		    <img src="http://localhost/application/Images/Add_image/1463827846.jpg" width="255" height="90" style="border:1px solid;"  />
		</a>   
		  &nbsp; 
		<a target="_blank" href="http://localhost/application/home/orgWiseAdvritismentView/2/15.html">
		   <img src="http://localhost/application/Images/Add_image/1463816230.gif" width="255" height="90" style="border:1px solid;"  />
		 </a>
	</div>
	
	<div class="col-lg-4 col-xs-12 col-sm-12">
	   		  <div class="row">
		    <div class="col-md-3 col-xs-3" align="right" style="padding-right:5px">
		        <button type="button" id="customePostion" class="login_reg pop" style="height:35px;" data-container="body" data-toggle="popover"  data-placement="bottom" data-html="true" data-target-content="#myModal" data-title="Login">Login</button>
		     </div>
		    <div class="col-md-3 col-xs-3" align="right" style="padding-left:5px">
		        <button type="button" class="login_reg pop" style="height:35px;" data-container="body" data-placement="bottom" data-toggle="popover"  data-html="true" data-target-content="#regModal" data-title="Registration">Registration</button>
		    </div>
		    <div class="col-md-3 col-xs-3" style="padding-left:5px">
		       <a href="http://localhost/application/liveChat.html"><input type="button" name="Button22" class="login_reg" value="Live Chat" style="height:35px; width:100px"/></a>
		    </div>
		    <div class="col-md-3 col-xs-3" align="left" style="padding-left:0">
		       <input type="button" name="Button3" value="News" class="login_reg goto-news" style="height:35px;"/>
		    </div>
		 </div>

	    
		   <div class="row">
		   	  <!-- <div class="col-md-3" style="padding-top:25px"> 
		   	   		    </div> -->
		    <div class="col-md-6 text-right" style="padding-top:15px"><span style="color:#FF0000; font-size:16px">Under Construction</span>  </div>
		    <div class="col-md-6" style="padding-top:15px">	                    
		        <div class="span2">
		           <div id="google_translate_element"></div> 
		        </div>
		    </div>
		   </div>


	</div>
</div>



<!-- Modal -->
 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<form id="logInForm"  action="http://localhost/application/loginForm/loginAuthenticate.html" method="post" class="form-horizontal">
	  <div class="col-md-12">
	  	<span class="logInFail"></span>
	  </div>
	    
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="type" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="type" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1">		
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2">
		</div>
	  </div>
	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Sign in</button> &nbsp; &nbsp; <a href="http://localhost/application/registration/genForgotPaword.html"> Forgot Password?</a></label>
		</div>
	  </div>
	</form>
</div>



<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="regModalLabel" >
	
	  
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="regType" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="regType" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="col-sm-12">&nbsp;</div>

  
   <div id="organize_reg">
      <form id="regForm" action="http://localhost/application/registration/orgRegStore.html" method="post" enctype="multipart/form-data" class="form-horizontal">
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="text" class="form-control" id="account_name" name="account_name" 
					placeholder="Account Name" tabindex="1" value="">
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
		   <input type="text" class="form-control" id="organizationname" name="organizationname" placeholder="Organization Name" tabindex="1" >
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="email" class="form-control" id="user_id2" name="user_id" placeholder="Email / Login id" tabindex="2" 
					value=""><span class="chkEmail"></span>
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<select class="form-control" id="country_id_org" name="country_id_org"  tabindex="2" >
				<option value="" selected>Sellect Country Name</option>
								<option value="336" data-country-code="011 355 " data-country-code-phon="" data-nationality="Albanian">Albania</option>
								<option value="152" data-country-code="011 213 " data-country-code-phon="" data-nationality="Algerian">Algeria</option>
								<option value="338" data-country-code="011 376 " data-country-code-phon="" data-nationality="Andorran">Andorra</option>
								<option value="262" data-country-code="011 244 " data-country-code-phon="" data-nationality="Angolan">Angola</option>
								<option value="268" data-country-code="1 " data-country-code-phon="" data-nationality="dg">Anguilla (UK)</option>
								<option value="269" data-country-code="1 " data-country-code-phon="" data-nationality="fg">Antigua and Barbuda</option>
								<option value="313" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
								<option value="335" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
								<option value="339" data-country-code="Armenia " data-country-code-phon="" data-nationality="Armenian">Armenia </option>
								<option value="270" data-country-code="1 " data-country-code-phon="" data-nationality="saf">Aruba</option>
								<option value="332" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australia">Australia</option>
								<option value="388" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australian">Australia</option>
								<option value="340" data-country-code="011 43 " data-country-code-phon="" data-nationality="Austrian">Austria</option>
								<option value="341" data-country-code="011 994 " data-country-code-phon="" data-nationality="Azerbaijani">Azerbaijan</option>
								<option value="271" data-country-code="1 " data-country-code-phon="" data-nationality="Bahamian">Bahamas</option>
								<option value="1" data-country-code="880" data-country-code-phon="222" data-nationality="Bangladeshi">Bangladesh</option>
								<option value="272" data-country-code="1 " data-country-code-phon="" data-nationality="Barbadian">Barbados</option>
								<option value="342" data-country-code="011 375 " data-country-code-phon="" data-nationality="Belorussian">Belarus</option>
								<option value="343" data-country-code="011 32 " data-country-code-phon="" data-nationality="Belgian">Belgium</option>
								<option value="273" data-country-code="011 501 " data-country-code-phon="" data-nationality="waf">Belize</option>
								<option value="274" data-country-code="1 " data-country-code-phon="" data-nationality="sdg">Bermuda (UK)</option>
								<option value="314" data-country-code="011 591 " data-country-code-phon="" data-nationality="Bolivian">Bolivia</option>
								<option value="275" data-country-code="er" data-country-code-phon="" data-nationality="sgd">Bonaire (Neth.)</option>
								<option value="344" data-country-code="011 387 " data-country-code-phon="" data-nationality="Bosnian">Bosnia and Herzegovina</option>
								<option value="315" data-country-code="011 55 " data-country-code-phon="" data-nationality="Brazilian">Brazil</option>
								<option value="276" data-country-code="1 " data-country-code-phon="" data-nationality="b">British Virgin Islands (UK)</option>
								<option value="345" data-country-code="011 359 " data-country-code-phon="" data-nationality="Bulgarian">Bulgaria</option>
								<option value="277" data-country-code="1 " data-country-code-phon="" data-nationality="Canadian">Canada</option>
								<option value="278" data-country-code="1 " data-country-code-phon="" data-nationality="az">Cayman Islands (UK)</option>
								<option value="316" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
								<option value="334" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
								<option value="16" data-country-code="011 86 " data-country-code-phon="" data-nationality="Chinese">China</option>
								<option value="279" data-country-code="g" data-country-code-phon="" data-nationality="g">Clipperton Island (Fr.)</option>
								<option value="317" data-country-code="011 57 " data-country-code-phon="" data-nationality="Colombian">Colombia</option>
								<option value="280" data-country-code="011 506 " data-country-code-phon="" data-nationality="dg">Costa Rica</option>
								<option value="346" data-country-code="011 385 " data-country-code-phon="" data-nationality="Croatian">Croatia</option>
								<option value="281" data-country-code="011 53 " data-country-code-phon="" data-nationality="Cuban">Cuba</option>
								<option value="282" data-country-code="sfg" data-country-code-phon="" data-nationality="gd">Curaçao</option>
								<option value="347" data-country-code="011 357 " data-country-code-phon="" data-nationality="Cypriot">Cyprus</option>
								<option value="348" data-country-code="011 420 " data-country-code-phon="" data-nationality="Czech">Czech Republic</option>
								<option value="349" data-country-code="011 45 " data-country-code-phon="" data-nationality="Dane">Denmark</option>
								<option value="283" data-country-code="1 " data-country-code-phon="" data-nationality="Dominican">Dominica</option>
								<option value="284" data-country-code="1 " data-country-code-phon="" data-nationality="sg">Dominican Republic</option>
								<option value="318" data-country-code="011 593 " data-country-code-phon="" data-nationality="Ecuadorean">Ecuador</option>
								<option value="77" data-country-code="011 20 " data-country-code-phon="" data-nationality="Egyptian">Egypt</option>
								<option value="285" data-country-code="011 503 " data-country-code-phon="" data-nationality="Salvadorean">El Salvador</option>
								<option value="350" data-country-code="011 372 " data-country-code-phon="" data-nationality="Estonian">Estonia</option>
								<option value="267" data-country-code="011 251 " data-country-code-phon="" data-nationality="Ethiopian">Ethiopia</option>
								<option value="319" data-country-code="011 500 " data-country-code-phon="" data-nationality="Falkland Islands (United Kingdom)">Falkland Islands (United Kingdom)</option>
								<option value="389" data-country-code="011 679 " data-country-code-phon="" data-nationality="Fijian">Fiji</option>
								<option value="351" data-country-code="011 358 " data-country-code-phon="" data-nationality="Finn">Finland</option>
								<option value="353" data-country-code="011 33 " data-country-code-phon="" data-nationality="Frenchman">France</option>
								<option value="320" data-country-code="French Guiana (France)" data-country-code-phon="" data-nationality="French Guiana (France)">French Guiana (France)</option>
								<option value="354" data-country-code="Georgia " data-country-code-phon="" data-nationality="Georgian">Georgia </option>
								<option value="355" data-country-code="011 49 " data-country-code-phon="" data-nationality="German">Germany</option>
								<option value="356" data-country-code="011 30 " data-country-code-phon="" data-nationality="Greek">Greece</option>
								<option value="286" data-country-code="011 299 " data-country-code-phon="" data-nationality="fg">Greenland (Den.)</option>
								<option value="287" data-country-code="1 " data-country-code-phon="" data-nationality="Grenadian">Grenada</option>
								<option value="288" data-country-code="Guadeloupe (Fr.)" data-country-code-phon="" data-nationality="Guadeloupe (Fr.)">Guadeloupe (Fr.)</option>
								<option value="289" data-country-code="011 502 " data-country-code-phon="" data-nationality="Guatemalan">Guatemala</option>
								<option value="321" data-country-code="011 592 " data-country-code-phon="" data-nationality="Guyanese">Guyana</option>
								<option value="290" data-country-code="011 509 " data-country-code-phon="" data-nationality="Haitian">Haiti</option>
								<option value="291" data-country-code="011 504 " data-country-code-phon="" data-nationality="Honduran">Honduras</option>
								<option value="357" data-country-code="011 36 " data-country-code-phon="" data-nationality="Hungarian">Hungary</option>
								<option value="358" data-country-code="011 354 " data-country-code-phon="" data-nationality="Icelander">Iceland</option>
								<option value="2" data-country-code="91" data-country-code-phon="333" data-nationality="Indian">India</option>
								<option value="21" data-country-code="011 62 " data-country-code-phon="" data-nationality="Indonesian">Indonesia</option>
								<option value="38" data-country-code="011 98 " data-country-code-phon="" data-nationality="Iranian">Iran</option>
								<option value="359" data-country-code="011 353 " data-country-code-phon="" data-nationality="Irishman">Ireland</option>
								<option value="360" data-country-code="011 39 " data-country-code-phon="" data-nationality="Italian">Italy</option>
								<option value="292" data-country-code="1 " data-country-code-phon="" data-nationality="Jamaican">Jamaica</option>
								<option value="17" data-country-code="011 81 " data-country-code-phon="" data-nationality="Japanese">Japan</option>
								<option value="361" data-country-code="011 7 " data-country-code-phon="" data-nationality="Kazakh">Kazakhstan</option>
								<option value="265" data-country-code="011 254 " data-country-code-phon="" data-nationality="Kenyan">Kenya</option>
								<option value="390" data-country-code="011 686 " data-country-code-phon="" data-nationality="Kiribati">Kiribati</option>
								<option value="362" data-country-code="011 371 " data-country-code-phon="" data-nationality="Latvia">Latvia</option>
								<option value="363" data-country-code="011 423 " data-country-code-phon="" data-nationality="Liechtensteiner">Liechtenstein</option>
								<option value="364" data-country-code="011 370 " data-country-code-phon="" data-nationality="Lithuanian">Lithuania</option>
								<option value="365" data-country-code="011 352 " data-country-code-phon="" data-nationality="Luxembourger">Luxembourg</option>
								<option value="366" data-country-code="011 389 " data-country-code-phon="" data-nationality="Macedonian">Macedonia</option>
								<option value="404" data-country-code="60" data-country-code-phon="+60" data-nationality="Malaysian">Malaysia</option>
								<option value="367" data-country-code="Malta" data-country-code-phon="" data-nationality="Maltese">Malta</option>
								<option value="391" data-country-code="011 692 " data-country-code-phon="" data-nationality="Marshall Islands">Marshall Islands</option>
								<option value="293" data-country-code="Martinique (Fr.)" data-country-code-phon="" data-nationality="Martinique (Fr.)">Martinique (Fr.)</option>
								<option value="294" data-country-code="011 52 " data-country-code-phon="" data-nationality="Mexican">Mexico</option>
								<option value="392" data-country-code="011 691 " data-country-code-phon="" data-nationality="Micronesia">Micronesia</option>
								<option value="368" data-country-code="Moldova " data-country-code-phon="" data-nationality="Moldovan">Moldova </option>
								<option value="369" data-country-code="011 377 " data-country-code-phon="" data-nationality="Monacan">Monaco</option>
								<option value="370" data-country-code="011 382 " data-country-code-phon="" data-nationality="Montenegrin">Montenegro</option>
								<option value="295" data-country-code="1 " data-country-code-phon="" data-nationality="Montserrat (UK)">Montserrat (UK)</option>
								<option value="263" data-country-code="011 212 " data-country-code-phon="" data-nationality="Moroccan">Morocco</option>
								<option value="403" data-country-code="95" data-country-code-phon="+95-69" data-nationality="Burmese">Myanmar</option>
								<option value="393" data-country-code="011 674 " data-country-code-phon="" data-nationality="Nauru">Nauru</option>
								<option value="296" data-country-code="Navassa Island (US)" data-country-code-phon="" data-nationality="Navassa Island (US)">Navassa Island (US)</option>
								<option value="405" data-country-code="+977" data-country-code-phon="+977" data-nationality="Nepalese">Nepal</option>
								<option value="371" data-country-code="011 31 " data-country-code-phon="" data-nationality="Netherlands">Netherlands</option>
								<option value="329" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
								<option value="394" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
								<option value="297" data-country-code="011 505 " data-country-code-phon="" data-nationality="Nicaraguan">Nicaragua</option>
								<option value="40" data-country-code="011 234 " data-country-code-phon="" data-nationality="Nigerian">Nigeria</option>
								<option value="331" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norway">Norway</option>
								<option value="333" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
								<option value="372" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
								<option value="395" data-country-code="011 680 " data-country-code-phon="" data-nationality="Palau">Palau</option>
								<option value="298" data-country-code="011 507 " data-country-code-phon="" data-nationality="Panamanian">Panama</option>
								<option value="396" data-country-code="011 675 " data-country-code-phon="" data-nationality="Papua New Guinea">Papua New Guinea</option>
								<option value="322" data-country-code="011 595 " data-country-code-phon="" data-nationality="Paraguayan">Paraguay</option>
								<option value="323" data-country-code="011 51 " data-country-code-phon="" data-nationality="Peruvian">Peru</option>
								<option value="402" data-country-code="201" data-country-code-phon="+63" data-nationality="Filipino">Philippine</option>
								<option value="373" data-country-code="011 48 " data-country-code-phon="" data-nationality="Pole">Poland</option>
								<option value="374" data-country-code="011 351 " data-country-code-phon="" data-nationality="Portuguese">Portugal</option>
								<option value="299" data-country-code="1 " data-country-code-phon="" data-nationality="Puerto Rico (US)">Puerto Rico (US)</option>
								<option value="375" data-country-code="011 40 " data-country-code-phon="" data-nationality="Romanian">Romania</option>
								<option value="19" data-country-code="011 7 " data-country-code-phon="" data-nationality="Russian">Russia</option>
								<option value="376" data-country-code="Russia " data-country-code-phon="" data-nationality="Russian">Russia </option>
								<option value="300" data-country-code="Saba (Neth.)" data-country-code-phon="" data-nationality="Saba (Neth.)">Saba (Neth.)</option>
								<option value="301" data-country-code="Saint Barthélemy (Fr.)" data-country-code-phon="" data-nationality="Saint Barthélemy (Fr.)">Saint Barthélemy (Fr.)</option>
								<option value="302" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
								<option value="303" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Lucia">Saint Lucia</option>
								<option value="304" data-country-code="Saint Martin (Fr.)" data-country-code-phon="" data-nationality="Saint Martin (Fr.)">Saint Martin (Fr.)</option>
								<option value="305" data-country-code="011 508 " data-country-code-phon="" data-nationality="Saint Pierre and Miquelon (Fr.)">Saint Pierre and Miquelon (Fr.)</option>
								<option value="306" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
								<option value="397" data-country-code="011 685 " data-country-code-phon="" data-nationality="Samoa">Samoa</option>
								<option value="377" data-country-code="011 378 " data-country-code-phon="" data-nationality="San Marino">San Marino</option>
								<option value="26" data-country-code="011 966 " data-country-code-phon="" data-nationality="Saudi">Saudi Arabia</option>
								<option value="378" data-country-code="Serbia " data-country-code-phon="" data-nationality="Serbian">Serbia </option>
								<option value="307" data-country-code="Sint Eustatius (Neth.)" data-country-code-phon="" data-nationality="Sint Eustatius (Neth.)">Sint Eustatius (Neth.)</option>
								<option value="308" data-country-code="Sint Maarten" data-country-code-phon="" data-nationality="Sint Maarten">Sint Maarten</option>
								<option value="379" data-country-code="011 421 " data-country-code-phon="" data-nationality="Slovak">Slovakia</option>
								<option value="380" data-country-code="011 386 " data-country-code-phon="" data-nationality="Slovenian">Slovenia</option>
								<option value="398" data-country-code="011 677 " data-country-code-phon="" data-nationality="Solomon Islands">Solomon Islands</option>
								<option value="88" data-country-code="011 27 " data-country-code-phon="" data-nationality="af">South Africa</option>
								<option value="18" data-country-code="011 82 " data-country-code-phon="" data-nationality="dghj">South Korea</option>
								<option value="266" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
								<option value="381" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
								<option value="264" data-country-code="011 211 " data-country-code-phon="" data-nationality="Sudanese">Sudan</option>
								<option value="325" data-country-code="011 597 " data-country-code-phon="" data-nationality="Surinamese">Suriname</option>
								<option value="382" data-country-code="011 46 " data-country-code-phon="" data-nationality="Swede">Sweden</option>
								<option value="383" data-country-code="011 41 " data-country-code-phon="" data-nationality="Swiss">Switzerland</option>
								<option value="28" data-country-code="011 886 " data-country-code-phon="" data-nationality="Taiwanese">Taiwan</option>
								<option value="399" data-country-code="011 676 " data-country-code-phon="" data-nationality="Tonga">Tongabbb</option>
								<option value="309" data-country-code="1 " data-country-code-phon="" data-nationality="Trinidadian">Trinidad and Tobago</option>
								<option value="25" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
								<option value="384" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
								<option value="310" data-country-code="1 " data-country-code-phon="" data-nationality="Turks and Caicos Islands">Turks and Caicos Islands</option>
								<option value="400" data-country-code="011 688 " data-country-code-phon="" data-nationality="Tuvalu">Tuvalu</option>
								<option value="385" data-country-code="011 380 " data-country-code-phon="" data-nationality="Ukrainian">Ukraine</option>
								<option value="328" data-country-code="011 44 " data-country-code-phon="" data-nationality="British">United Kingdom</option>
								<option value="386" data-country-code="011 44 " data-country-code-phon="" data-nationality="United Kingdom">United Kingdom</option>
								<option value="311" data-country-code="1 " data-country-code-phon="" data-nationality="American">United States</option>
								<option value="407" data-country-code="+1" data-country-code-phon="+1" data-nationality="American">United States of America</option>
								<option value="312" data-country-code="United States Virgin Islands (US)" data-country-code-phon="" data-nationality="United States Virgin Islands (US)">United States Virgin Islands (US)</option>
								<option value="326" data-country-code="011 598 " data-country-code-phon="" data-nationality="Uruguayan">Uruguay</option>
								<option value="411" data-country-code="" data-country-code-phon="" data-nationality="Uzbekistani">Uzbekistan</option>
								<option value="401" data-country-code="011 678 " data-country-code-phon="" data-nationality="Vanuatu">Vanuatu</option>
								<option value="387" data-country-code="011 39 " data-country-code-phon="" data-nationality="Vatican City">Vatican City</option>
								<option value="327" data-country-code="011 58 " data-country-code-phon="" data-nationality="Venezuelan">Venezuela</option>
								<option value="408" data-country-code="+84" data-country-code-phon="+84" data-nationality="Vietnami">Vietman</option>
								<option value="410" data-country-code="+84" data-country-code-phon="" data-nationality="Vietnamese">Vietnam</option>
				                         
			</select>
		</div>
	  </div>

	  


	   <div class="form-group">
	  	<div class="col-sm-12">
			<input type="password" class="form-control password" id="password" name="password" placeholder="Password" tabindex="2" value="">		   <span class="first"></span>
		</div>
	  </div>

	  <div class="form-group">
	    <div class="col-sm-12">
			<input type="password" class="form-control conformpassword" id="con_password" name="con_password" placeholder="Confirm Password" tabindex="2">
			<span class="second"></span>
		</div>
	  </div> 

	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Create Account</button>
		</div>
	  </div>
	</form>

 </div>


	 <div id="other_reg" style="display:none">
	     <form id="genReg" action="http://localhost/application/registration/genRegStore1.html" method="post" enctype="multipart/form-data" class="form-horizontal">

	       <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="email" class="control-label">UserId</label>	 -->	
				<input type="text" class="form-control" id="name" placeholder="Full Name" name="name" tabindex="5" 
					value=""> 			</div>
		  </div>

		  
		  <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="password" class="control-label">Password</label>		 -->
				<input type="text" class="form-control" id="user_id" name="user_id" placeholder="User Id/Email" tabindex="4" 
					value=""><span class="chk"></span>
			</div>
		  </div>


		  <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="password" class="control-label">Password</label>		 -->
				<select class="form-control" id="country_id" name="country_id"  tabindex="2" >
					<option value="" selected>Sellect Country Name</option>
										<option value="336" data-country-code="011 355 " data-country-code-phon="" data-nationality="Albanian">Albania</option>
										<option value="152" data-country-code="011 213 " data-country-code-phon="" data-nationality="Algerian">Algeria</option>
										<option value="338" data-country-code="011 376 " data-country-code-phon="" data-nationality="Andorran">Andorra</option>
										<option value="262" data-country-code="011 244 " data-country-code-phon="" data-nationality="Angolan">Angola</option>
										<option value="268" data-country-code="1 " data-country-code-phon="" data-nationality="dg">Anguilla (UK)</option>
										<option value="269" data-country-code="1 " data-country-code-phon="" data-nationality="fg">Antigua and Barbuda</option>
										<option value="313" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
										<option value="335" data-country-code="011 54 " data-country-code-phon="" data-nationality="Argentinian">Argentina</option>
										<option value="339" data-country-code="Armenia " data-country-code-phon="" data-nationality="Armenian">Armenia </option>
										<option value="270" data-country-code="1 " data-country-code-phon="" data-nationality="saf">Aruba</option>
										<option value="332" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australia">Australia</option>
										<option value="388" data-country-code="011 61 " data-country-code-phon="" data-nationality="Australian">Australia</option>
										<option value="340" data-country-code="011 43 " data-country-code-phon="" data-nationality="Austrian">Austria</option>
										<option value="341" data-country-code="011 994 " data-country-code-phon="" data-nationality="Azerbaijani">Azerbaijan</option>
										<option value="271" data-country-code="1 " data-country-code-phon="" data-nationality="Bahamian">Bahamas</option>
										<option value="1" data-country-code="880" data-country-code-phon="222" data-nationality="Bangladeshi">Bangladesh</option>
										<option value="272" data-country-code="1 " data-country-code-phon="" data-nationality="Barbadian">Barbados</option>
										<option value="342" data-country-code="011 375 " data-country-code-phon="" data-nationality="Belorussian">Belarus</option>
										<option value="343" data-country-code="011 32 " data-country-code-phon="" data-nationality="Belgian">Belgium</option>
										<option value="273" data-country-code="011 501 " data-country-code-phon="" data-nationality="waf">Belize</option>
										<option value="274" data-country-code="1 " data-country-code-phon="" data-nationality="sdg">Bermuda (UK)</option>
										<option value="314" data-country-code="011 591 " data-country-code-phon="" data-nationality="Bolivian">Bolivia</option>
										<option value="275" data-country-code="er" data-country-code-phon="" data-nationality="sgd">Bonaire (Neth.)</option>
										<option value="344" data-country-code="011 387 " data-country-code-phon="" data-nationality="Bosnian">Bosnia and Herzegovina</option>
										<option value="315" data-country-code="011 55 " data-country-code-phon="" data-nationality="Brazilian">Brazil</option>
										<option value="276" data-country-code="1 " data-country-code-phon="" data-nationality="b">British Virgin Islands (UK)</option>
										<option value="345" data-country-code="011 359 " data-country-code-phon="" data-nationality="Bulgarian">Bulgaria</option>
										<option value="277" data-country-code="1 " data-country-code-phon="" data-nationality="Canadian">Canada</option>
										<option value="278" data-country-code="1 " data-country-code-phon="" data-nationality="az">Cayman Islands (UK)</option>
										<option value="316" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
										<option value="334" data-country-code="011 56 " data-country-code-phon="" data-nationality="Chilean">Chile</option>
										<option value="16" data-country-code="011 86 " data-country-code-phon="" data-nationality="Chinese">China</option>
										<option value="279" data-country-code="g" data-country-code-phon="" data-nationality="g">Clipperton Island (Fr.)</option>
										<option value="317" data-country-code="011 57 " data-country-code-phon="" data-nationality="Colombian">Colombia</option>
										<option value="280" data-country-code="011 506 " data-country-code-phon="" data-nationality="dg">Costa Rica</option>
										<option value="346" data-country-code="011 385 " data-country-code-phon="" data-nationality="Croatian">Croatia</option>
										<option value="281" data-country-code="011 53 " data-country-code-phon="" data-nationality="Cuban">Cuba</option>
										<option value="282" data-country-code="sfg" data-country-code-phon="" data-nationality="gd">Curaçao</option>
										<option value="347" data-country-code="011 357 " data-country-code-phon="" data-nationality="Cypriot">Cyprus</option>
										<option value="348" data-country-code="011 420 " data-country-code-phon="" data-nationality="Czech">Czech Republic</option>
										<option value="349" data-country-code="011 45 " data-country-code-phon="" data-nationality="Dane">Denmark</option>
										<option value="283" data-country-code="1 " data-country-code-phon="" data-nationality="Dominican">Dominica</option>
										<option value="284" data-country-code="1 " data-country-code-phon="" data-nationality="sg">Dominican Republic</option>
										<option value="318" data-country-code="011 593 " data-country-code-phon="" data-nationality="Ecuadorean">Ecuador</option>
										<option value="77" data-country-code="011 20 " data-country-code-phon="" data-nationality="Egyptian">Egypt</option>
										<option value="285" data-country-code="011 503 " data-country-code-phon="" data-nationality="Salvadorean">El Salvador</option>
										<option value="350" data-country-code="011 372 " data-country-code-phon="" data-nationality="Estonian">Estonia</option>
										<option value="267" data-country-code="011 251 " data-country-code-phon="" data-nationality="Ethiopian">Ethiopia</option>
										<option value="319" data-country-code="011 500 " data-country-code-phon="" data-nationality="Falkland Islands (United Kingdom)">Falkland Islands (United Kingdom)</option>
										<option value="389" data-country-code="011 679 " data-country-code-phon="" data-nationality="Fijian">Fiji</option>
										<option value="351" data-country-code="011 358 " data-country-code-phon="" data-nationality="Finn">Finland</option>
										<option value="353" data-country-code="011 33 " data-country-code-phon="" data-nationality="Frenchman">France</option>
										<option value="320" data-country-code="French Guiana (France)" data-country-code-phon="" data-nationality="French Guiana (France)">French Guiana (France)</option>
										<option value="354" data-country-code="Georgia " data-country-code-phon="" data-nationality="Georgian">Georgia </option>
										<option value="355" data-country-code="011 49 " data-country-code-phon="" data-nationality="German">Germany</option>
										<option value="356" data-country-code="011 30 " data-country-code-phon="" data-nationality="Greek">Greece</option>
										<option value="286" data-country-code="011 299 " data-country-code-phon="" data-nationality="fg">Greenland (Den.)</option>
										<option value="287" data-country-code="1 " data-country-code-phon="" data-nationality="Grenadian">Grenada</option>
										<option value="288" data-country-code="Guadeloupe (Fr.)" data-country-code-phon="" data-nationality="Guadeloupe (Fr.)">Guadeloupe (Fr.)</option>
										<option value="289" data-country-code="011 502 " data-country-code-phon="" data-nationality="Guatemalan">Guatemala</option>
										<option value="321" data-country-code="011 592 " data-country-code-phon="" data-nationality="Guyanese">Guyana</option>
										<option value="290" data-country-code="011 509 " data-country-code-phon="" data-nationality="Haitian">Haiti</option>
										<option value="291" data-country-code="011 504 " data-country-code-phon="" data-nationality="Honduran">Honduras</option>
										<option value="357" data-country-code="011 36 " data-country-code-phon="" data-nationality="Hungarian">Hungary</option>
										<option value="358" data-country-code="011 354 " data-country-code-phon="" data-nationality="Icelander">Iceland</option>
										<option value="2" data-country-code="91" data-country-code-phon="333" data-nationality="Indian">India</option>
										<option value="21" data-country-code="011 62 " data-country-code-phon="" data-nationality="Indonesian">Indonesia</option>
										<option value="38" data-country-code="011 98 " data-country-code-phon="" data-nationality="Iranian">Iran</option>
										<option value="359" data-country-code="011 353 " data-country-code-phon="" data-nationality="Irishman">Ireland</option>
										<option value="360" data-country-code="011 39 " data-country-code-phon="" data-nationality="Italian">Italy</option>
										<option value="292" data-country-code="1 " data-country-code-phon="" data-nationality="Jamaican">Jamaica</option>
										<option value="17" data-country-code="011 81 " data-country-code-phon="" data-nationality="Japanese">Japan</option>
										<option value="361" data-country-code="011 7 " data-country-code-phon="" data-nationality="Kazakh">Kazakhstan</option>
										<option value="265" data-country-code="011 254 " data-country-code-phon="" data-nationality="Kenyan">Kenya</option>
										<option value="390" data-country-code="011 686 " data-country-code-phon="" data-nationality="Kiribati">Kiribati</option>
										<option value="362" data-country-code="011 371 " data-country-code-phon="" data-nationality="Latvia">Latvia</option>
										<option value="363" data-country-code="011 423 " data-country-code-phon="" data-nationality="Liechtensteiner">Liechtenstein</option>
										<option value="364" data-country-code="011 370 " data-country-code-phon="" data-nationality="Lithuanian">Lithuania</option>
										<option value="365" data-country-code="011 352 " data-country-code-phon="" data-nationality="Luxembourger">Luxembourg</option>
										<option value="366" data-country-code="011 389 " data-country-code-phon="" data-nationality="Macedonian">Macedonia</option>
										<option value="404" data-country-code="60" data-country-code-phon="+60" data-nationality="Malaysian">Malaysia</option>
										<option value="367" data-country-code="Malta" data-country-code-phon="" data-nationality="Maltese">Malta</option>
										<option value="391" data-country-code="011 692 " data-country-code-phon="" data-nationality="Marshall Islands">Marshall Islands</option>
										<option value="293" data-country-code="Martinique (Fr.)" data-country-code-phon="" data-nationality="Martinique (Fr.)">Martinique (Fr.)</option>
										<option value="294" data-country-code="011 52 " data-country-code-phon="" data-nationality="Mexican">Mexico</option>
										<option value="392" data-country-code="011 691 " data-country-code-phon="" data-nationality="Micronesia">Micronesia</option>
										<option value="368" data-country-code="Moldova " data-country-code-phon="" data-nationality="Moldovan">Moldova </option>
										<option value="369" data-country-code="011 377 " data-country-code-phon="" data-nationality="Monacan">Monaco</option>
										<option value="370" data-country-code="011 382 " data-country-code-phon="" data-nationality="Montenegrin">Montenegro</option>
										<option value="295" data-country-code="1 " data-country-code-phon="" data-nationality="Montserrat (UK)">Montserrat (UK)</option>
										<option value="263" data-country-code="011 212 " data-country-code-phon="" data-nationality="Moroccan">Morocco</option>
										<option value="403" data-country-code="95" data-country-code-phon="+95-69" data-nationality="Burmese">Myanmar</option>
										<option value="393" data-country-code="011 674 " data-country-code-phon="" data-nationality="Nauru">Nauru</option>
										<option value="296" data-country-code="Navassa Island (US)" data-country-code-phon="" data-nationality="Navassa Island (US)">Navassa Island (US)</option>
										<option value="405" data-country-code="+977" data-country-code-phon="+977" data-nationality="Nepalese">Nepal</option>
										<option value="371" data-country-code="011 31 " data-country-code-phon="" data-nationality="Netherlands">Netherlands</option>
										<option value="329" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
										<option value="394" data-country-code="011 64 " data-country-code-phon="" data-nationality="New Zealand">New Zealand</option>
										<option value="297" data-country-code="011 505 " data-country-code-phon="" data-nationality="Nicaraguan">Nicaragua</option>
										<option value="40" data-country-code="011 234 " data-country-code-phon="" data-nationality="Nigerian">Nigeria</option>
										<option value="331" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norway">Norway</option>
										<option value="333" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
										<option value="372" data-country-code="011 47 " data-country-code-phon="" data-nationality="Norwegian">Norway</option>
										<option value="395" data-country-code="011 680 " data-country-code-phon="" data-nationality="Palau">Palau</option>
										<option value="298" data-country-code="011 507 " data-country-code-phon="" data-nationality="Panamanian">Panama</option>
										<option value="396" data-country-code="011 675 " data-country-code-phon="" data-nationality="Papua New Guinea">Papua New Guinea</option>
										<option value="322" data-country-code="011 595 " data-country-code-phon="" data-nationality="Paraguayan">Paraguay</option>
										<option value="323" data-country-code="011 51 " data-country-code-phon="" data-nationality="Peruvian">Peru</option>
										<option value="402" data-country-code="201" data-country-code-phon="+63" data-nationality="Filipino">Philippine</option>
										<option value="373" data-country-code="011 48 " data-country-code-phon="" data-nationality="Pole">Poland</option>
										<option value="374" data-country-code="011 351 " data-country-code-phon="" data-nationality="Portuguese">Portugal</option>
										<option value="299" data-country-code="1 " data-country-code-phon="" data-nationality="Puerto Rico (US)">Puerto Rico (US)</option>
										<option value="375" data-country-code="011 40 " data-country-code-phon="" data-nationality="Romanian">Romania</option>
										<option value="19" data-country-code="011 7 " data-country-code-phon="" data-nationality="Russian">Russia</option>
										<option value="376" data-country-code="Russia " data-country-code-phon="" data-nationality="Russian">Russia </option>
										<option value="300" data-country-code="Saba (Neth.)" data-country-code-phon="" data-nationality="Saba (Neth.)">Saba (Neth.)</option>
										<option value="301" data-country-code="Saint Barthélemy (Fr.)" data-country-code-phon="" data-nationality="Saint Barthélemy (Fr.)">Saint Barthélemy (Fr.)</option>
										<option value="302" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
										<option value="303" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Lucia">Saint Lucia</option>
										<option value="304" data-country-code="Saint Martin (Fr.)" data-country-code-phon="" data-nationality="Saint Martin (Fr.)">Saint Martin (Fr.)</option>
										<option value="305" data-country-code="011 508 " data-country-code-phon="" data-nationality="Saint Pierre and Miquelon (Fr.)">Saint Pierre and Miquelon (Fr.)</option>
										<option value="306" data-country-code="1 " data-country-code-phon="" data-nationality="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
										<option value="397" data-country-code="011 685 " data-country-code-phon="" data-nationality="Samoa">Samoa</option>
										<option value="377" data-country-code="011 378 " data-country-code-phon="" data-nationality="San Marino">San Marino</option>
										<option value="26" data-country-code="011 966 " data-country-code-phon="" data-nationality="Saudi">Saudi Arabia</option>
										<option value="378" data-country-code="Serbia " data-country-code-phon="" data-nationality="Serbian">Serbia </option>
										<option value="307" data-country-code="Sint Eustatius (Neth.)" data-country-code-phon="" data-nationality="Sint Eustatius (Neth.)">Sint Eustatius (Neth.)</option>
										<option value="308" data-country-code="Sint Maarten" data-country-code-phon="" data-nationality="Sint Maarten">Sint Maarten</option>
										<option value="379" data-country-code="011 421 " data-country-code-phon="" data-nationality="Slovak">Slovakia</option>
										<option value="380" data-country-code="011 386 " data-country-code-phon="" data-nationality="Slovenian">Slovenia</option>
										<option value="398" data-country-code="011 677 " data-country-code-phon="" data-nationality="Solomon Islands">Solomon Islands</option>
										<option value="88" data-country-code="011 27 " data-country-code-phon="" data-nationality="af">South Africa</option>
										<option value="18" data-country-code="011 82 " data-country-code-phon="" data-nationality="dghj">South Korea</option>
										<option value="266" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
										<option value="381" data-country-code="011 34 " data-country-code-phon="" data-nationality="Spaniard">Spain</option>
										<option value="264" data-country-code="011 211 " data-country-code-phon="" data-nationality="Sudanese">Sudan</option>
										<option value="325" data-country-code="011 597 " data-country-code-phon="" data-nationality="Surinamese">Suriname</option>
										<option value="382" data-country-code="011 46 " data-country-code-phon="" data-nationality="Swede">Sweden</option>
										<option value="383" data-country-code="011 41 " data-country-code-phon="" data-nationality="Swiss">Switzerland</option>
										<option value="28" data-country-code="011 886 " data-country-code-phon="" data-nationality="Taiwanese">Taiwan</option>
										<option value="399" data-country-code="011 676 " data-country-code-phon="" data-nationality="Tonga">Tongabbb</option>
										<option value="309" data-country-code="1 " data-country-code-phon="" data-nationality="Trinidadian">Trinidad and Tobago</option>
										<option value="25" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
										<option value="384" data-country-code="011 90 " data-country-code-phon="" data-nationality="Turk">Turkey</option>
										<option value="310" data-country-code="1 " data-country-code-phon="" data-nationality="Turks and Caicos Islands">Turks and Caicos Islands</option>
										<option value="400" data-country-code="011 688 " data-country-code-phon="" data-nationality="Tuvalu">Tuvalu</option>
										<option value="385" data-country-code="011 380 " data-country-code-phon="" data-nationality="Ukrainian">Ukraine</option>
										<option value="328" data-country-code="011 44 " data-country-code-phon="" data-nationality="British">United Kingdom</option>
										<option value="386" data-country-code="011 44 " data-country-code-phon="" data-nationality="United Kingdom">United Kingdom</option>
										<option value="311" data-country-code="1 " data-country-code-phon="" data-nationality="American">United States</option>
										<option value="407" data-country-code="+1" data-country-code-phon="+1" data-nationality="American">United States of America</option>
										<option value="312" data-country-code="United States Virgin Islands (US)" data-country-code-phon="" data-nationality="United States Virgin Islands (US)">United States Virgin Islands (US)</option>
										<option value="326" data-country-code="011 598 " data-country-code-phon="" data-nationality="Uruguayan">Uruguay</option>
										<option value="411" data-country-code="" data-country-code-phon="" data-nationality="Uzbekistani">Uzbekistan</option>
										<option value="401" data-country-code="011 678 " data-country-code-phon="" data-nationality="Vanuatu">Vanuatu</option>
										<option value="387" data-country-code="011 39 " data-country-code-phon="" data-nationality="Vatican City">Vatican City</option>
										<option value="327" data-country-code="011 58 " data-country-code-phon="" data-nationality="Venezuelan">Venezuela</option>
										<option value="408" data-country-code="+84" data-country-code-phon="+84" data-nationality="Vietnami">Vietman</option>
										<option value="410" data-country-code="+84" data-country-code-phon="" data-nationality="Vietnamese">Vietnam</option>
									</select>
			</div>
		  </div>

		   


		   <div class="form-group">
		  	<div class="col-sm-12">
				<input type="password" class="form-control password3" id="password" name="password" placeholder="Password" tabindex="23" 
				  value="" >				  <p class="third"></p>
			</div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-12">
				<input type="password" class="form-control conformpassword3" id="conform_password" name="conform_password" placeholder="Conform Password" 
					tabindex="20" value="">					<p class="fourth"></p>
			</div>
		  </div> 

		  
		  <div class="form-group">
			<div class="col-sm-12">
			  <button type="submit" class="btn btn-primary">Create Account</button>
			</div>
		  </div>

	     </form>
	 </div>

</div>


<script>
  	$(document).on("submit", "#logInForm", function(e){
  		
		var postData = $(this).serializeArray();
		var formURL  = $(this).attr("action");
		
		var type 	= $(this).find("input[name='type']:checked").val();
		
		if(type =='organization'){
		   var successUrl	= "http://localhost/application/organizationUserHome.html";
		}else{
		  var successUrl	= "http://localhost/application/generalUserHome.html";			
		}
	
		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data){
				if(data==1){
					$("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='hidden'], #logInForm input[type='password']").val("");
					$(".logInFail").text("Wrong email or password. Please try again!");
					$(".logInFail").css("color", "red");
					$("#msg").css("display", "block"); 
				} else if(data==2){
					 $(".logInFail").text("Please Select a type Organization or Other");
					 $(".logInFail").css("color", "red"); 
					 $("#msg").css("display", "block"); 					
				}else{
				 location.replace(successUrl);
				}
			}					
		});
	
	   	e.preventDefault();
	});
	
	$(".close").click(function(){
	   $("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='password']").val("");
	   $("#logInForm input[type='radio']").prop("checked",false);
	});

	
  
  $(window).ready(function(){
	  $('[data-toggle="popover"]').popover({
		    content: function(){
			  var target = $(this).attr('data-target-content');
			   return $(target).html();
			  },
		    trigger: 'manual'
		}).click(function (e) {
			$( this ).popover( "toggle" ); 
			e.stopPropagation();
			  	
		});

		$('[data-toggle="popover"]').on('click', function (e) {
		    $('[data-toggle="popover"]').not(this).popover('hide');
		});

	  $('body').on('click', function (e) {
	    $('[data-toggle="popover"]').each(function () {
	        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
	            $(this).popover('hide');
	        }
	    });
	  });

   });


	  $(document).on("change", "input[name='regType']", function(){
		   var typeValue = $(this).val();
		   var parents = $(this).parents('.popover-content');

		   if(typeValue =='organization'){
		    	parents.find("#organize_reg").css("display", "block");
		    	parents.find("#other_reg").css("display", "none");		    	
		   }else{
		     	parents.find("#organize_reg").css("display", "none");
		    	parents.find("#other_reg").css("display", "block");
		   }
		   
		  	
		});


	  $(document).on("change", "#country_id", function() {
		var country_id = $(this).val();
		parents = $(this).parents('.popover-content');	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
			   parents.find("#city_id").html(data);
			}
		});
		
	});
	
	
	$(document).on("change", "#country_id_org", function() {
		var country_id = $(this).val();
		parents = $(this).parents('.popover-content');
		console.log(country_id);	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				parents.find("#city_id_org").html(data);
			}
		});
		
	});



	// Password Count	
   
   $(document).on("keyup", ".password", function() {
	 var len = $(this).val().length;
	 parents = $(this).parents('.popover-content');
	    
		if(len<=1){
		parents.find(".first").text("");
		parents.find(".first").removeClass("red");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("green");
		
	  } else if(len<=4){
	  	parents.find(".first").text("Very Weak");
		parents.find(".first").addClass("red");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("green");
	   } else if(len<=8){

	   	parents.find(".first").text("Good");
		parents.find(".first").addClass("green");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("red");

	   } else if(len<=9){

	   	parents.find(".first").text("Good");
		parents.find(".first").addClass("green");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword", function() {
  	 parents 		= $(this).parents('.popover-content');
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		=  parents.find(".password").val();

	 if(conpass){

		  if(conpass != Pass){
		  	parents.find(".second").text("Your New Password and Confirm Password donot match!");
			parents.find(".second").addClass("red");
			parents.find(".second").removeClass("green");
		  
		  } else {
		  	parents.find(".second").text("Password Match");
			parents.find(".second").removeClass("red");
			parents.find(".second").addClass("green");
		   	
		  }

		} else {
		   parents.find(".second").text("Password Match");
		   parents.find(".second").removeClass("red");
		   parents.find(".second").removeClass("green");
		  
		}
	});
	
	
	
	// Password Count


	$(document).on("keyup", ".password3", function() {
	 var len = $(this).val().length;
	 parents = $(this).parents('.popover-content');
	    
		if(len<=1){
		parents.find(".third").text("");
		parents.find(".third").removeClass("red");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("green");
		
	  } else if(len<=4){
	  	parents.find(".third").text("Very Weak");
		parents.find(".third").addClass("red");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("green");
	   } else if(len<=8){

	   	parents.find(".third").text("Good");
		parents.find(".third").addClass("green");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("red");

	   } else if(len<=9){

	   	parents.find(".third").text("Good");
		parents.find(".third").addClass("green");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword3", function() {
  	 parents 		= $(this).parents('.popover-content');
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		=  parents.find(".password3").val();

	 if(conpass){

		  if(conpass != Pass){
		  	parents.find(".fourth").text("Your New Password and Confirm Password donot match!");
			parents.find(".fourth").addClass("red");
			parents.find(".fourth").removeClass("green");
		  
		  } else {
		  	parents.find(".fourth").text("Password Match");
			parents.find(".fourth").removeClass("red");
			parents.find(".fourth").addClass("green");
		   	
		  }

		} else {
		   parents.find(".fourth").text("Password Match");
		   parents.find(".fourth").removeClass("red");
		   parents.find(".fourth").removeClass("green");
		  
		}
	});
	


	
   

	$(document).on("blur", "#user_id", function() {
		parents 	= $(this).parents('.popover-content');
		var userId 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userId : userId },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				parents.find(".chk").text("This Email Already Exit!");
				parents.find(".chk").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				parents.find(".chk").text("");
				parents.find(".chk").removeClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});



	$(document).on("blur", "#user_id2", function() {
		parents 		= $(this).parents('.popover-content');
		var user_id 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/userEmailChk'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { user_id : user_id },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				parents.find(".chkEmail").text("This Email Already Exit!");
				parents.find(".chkEmail").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				parents.find(".chkEmail").text("");
				parents.find(".chkEmail").addClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});

	$(document).ready(function() {
	   setTimeout(getAllUnreadMsg, 2000);
	});
	

	// Get All unread msg
    
	 function getAllUnreadMsg() {
		  $.ajax({
	        url : SAWEB.getSiteAction('liveChat/getAllUnreadMsg'),
			type: "POST",
			data : {},
			dataType: "html",
	        success: function (data) { 
	        	$("#newMsgNum").html(data);

	          }
	       });
		   setTimeout(getAllUnreadMsg, 2000);
	}
	
	
	
</script>

           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<link href="http://localhost/application/resource/source/menu/style.css" rel="stylesheet">
<script src="http://localhost/application/resource/source/menu/jquerymenu.js"></script>


    
<div id="ddmenuMst">
    <div class="menu-icon"></div>
    <ul style="background:#d9e3ef; text-align:start;">
       <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="http://localhost/application/home.html">Home</a></span></li>
	   <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="http://www.globalstarltdbd.com">Global Star Ltd</a></span></li>
	   <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="http://localhost/application/currentoffer.html">Current Offer</a></span></li>
	   
	   
	   
	   
<li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;" id="asia">
<a  style="text-decoration:none" href="#">Countries</a></span>
</li>
	   
	   
	   


   
    
              <li><span class="top-heading" style="color:#000; font-size:16px; text-transform:uppercase;"><a style="text-decoration:none" href="http://localhost/application/home/contactUS.html">Contact Us</a></span></li>

    </ul>
</div>


<script type="text/javascript">
	   /*$("#asia, #south, #africa, #australia, #europ, #nort").click(function(e) {

	   		var caption = $(this).attr('data-value');
	   		var target = "#"+caption.replace(" ", "-")+"-Country";

	   		$("#globe .modal-title").html(caption);
	   		$(".country-list").hide();
	   		$(target).show();

	   		$("#globe").modal({
				keyboard: false,
				backdrop: 'static',
			});

			$("#globe").modal('show');

			e.preventDefault();
		});	*/


	   $("#asia, #south, #africa, #australia, #europ, #nort").on('click', function(e){
	   	    var globValue = $(this).attr('data-value');
			var url = "http://localhost/application/home/globDetails.html";
			
			$.ajax({
				url : url,  // URL TO LOAD BEHIND THE SCREEN
				type: "POST",
				data : {globValue: globValue},
				dataType : "html",
				success : function(data) {			
					$("#globe .modal-content").html(data);
					$("#globe").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#globe").modal('show');
				}
			});

			e.preventDefault();		
		});

	   	$("#globe .hello").on('click', function(){
	   		
	   		var caption = $(this).attr('data-value');
	   		var target = "#"+caption.replace(" ", "-")+"-Country";
	   		console.log(target);
	   		console.log($(target));
	   		$("#globe .modal-title").html(caption);
	   		$(".country-list").hide();	
	   		$(target).show();	
	   	});

	</script>
</div>
</div> 
<div class="row">&nbsp;</div>  
	
 <div class="row">&nbsp;</div>     
  <div class="row">
  	<div class="col-lg-12 col-md-12 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
<h2 style="color: #3366ff;">চায়না (China) সম্পর্কে</h2>
<span style="color: #000000;">গণপ্রজাতন্ত্রী চায়না (চায়না ভাষায়: 中国 অর্থাৎ "মধ্যদেশ", ম্যান্ডারিন উচ্চারণে: চুংকুও; ইংরেজি উচ্চারণ: শুনুনi/ˈtʃaɪnə/) পূর্ব এশিয়ার একটি রাষ্ট্র। ১৩০ কোটি জনসংখ্যার অধিকারী চায়না পৃথিবীর সবচেয়ে জনবহুল এবং আয়তনের দিক থেকে এশিয়ার সর্ববৃহৎ রাষ্ট্র। এর আয়তন প্রায় মার্কিন যুক্তরাষ্ট্রের সমান। পৃথিবীর ৩য় বৃহত্তম রাষ্ট্র চায়নার উত্তরে রয়েছে মঙ্গোলিয়া; উত্তর পূর্বে রাশিয়া ও উত্তর কোরিয়া; পূর্বে চায়না সাগর; দক্ষিণে ভিয়েতনাম, লাওস, মায়ানমার, ভারত, ভূটান, নেপাল; দক্ষিণ পশ্চিমে পাকিস্তান; পশ্চিমে আফগানিস্তান, তাজিকিস্তান, কির্গিজিস্তান ও কাজাকিস্তান। এই ১৪টি দেশ বাদে চায়নার পূর্বে পীত সাগরের পাশে রয়েছে দক্ষিণ কোরিয়া ও জাপান; দক্ষিণ চীন সাগরের উল্টো দিকে আছে ফিলিপাইন। চায়নারা তাদের দেশকে চুংকুও নামে ডাকে, যার অর্থ "মধ্যদেশ" বা "মধ্যবর্তী রাজ্য"। "চীন" নামটি বিদেশীদের দেওয়া; এটি সম্ভবত খ্রিস্টপূর্ব ৩য় শতকের ছিন রাজবংশের নামের বিকৃত রূপ। চায়নায় বিশ্বের জনসংখ্যার এক-পঞ্চমাংশের বাস। এদের ৯০%-এরও বেশি হল চৈনিক হান জাতির লোক। হান জাতি বাদে চীনে আরও ৫৫টি সংখ্যালঘু জাতির বাস। এদের মধ্যে আছে তিব্বতি, মঙ্গোল, উইঘুর, ছুয়াং, মিয়াও, য়ি এবং আরও অনেক ছোট ছোট জাতি। হান জাতির লোকদের মধ্যেও অঞ্চলভেদে ভাষাগত পার্থক্য দেখা যায়। যদিও শিক্ষাব্যবস্থায় ও গণমাধ্যমে পুতোংহুয়া নামের একটি সাধারণ ভাষা ব্যবহার করা হয়, আঞ্চলিক কথ্য ভাষাগুলি প্রায়শই পরস্পর বোধগম্য নয়। তবে চিত্রলিপিভিত্তিক লিখন পদ্ধতি ব্যবহার করে বলে সব চীনা উপভাষাই একই ভাবে লেখা যায়; এর ফলে গোটা চীন জুড়ে যোগাযোগ সহজ ।
</span>

<h2 style="color: #3366ff;">চায়নায় উচ্চ শিক্ষা</h2>
<span style="color: #000000;">আমাদের দেশে থেকে প্রতি বছর প্রচুর শিক্ষার্থী উচ্চ শিক্ষার জন্য ইউরোপ, আমেরিকা,অস্ট্রেলিয়া সহ বিভিন্ন দেশে চলে যাচ্ছে। যেসব শিক্ষার্থী চায়নায় পড়তে চান, কিংবা যেসব অবিভাবক সন্তানদের উচ্চ শিক্ষার জন্য বিদেশে পড়াতে আগ্রহী তারা চায়নায় পড়ানোর সুযোগ নিতে পারেন অনেক সহজেই। চায়নার উচ্চশিক্ষা অনার্স ডিগ্রি, ব্যাচেলর ডিগ্রি, স্নাতকোত্তর ডিগ্রি, ডক্টরেট ডিগ্রি কয়েকটি পর্যায়ে বিভক্ত। উচ্চশিক্ষা প্রতিষ্ঠানের মধ্যে অন্তর্ভুক্ত আছে সাধারণ উচ্চশিক্ষা প্রতিষ্ঠান, পেশাগত উচ্চশিক্ষা প্রতিষ্ঠান, বেতার ও টেলিভিশন বিশ্ববিদ্যালয়, প্রাপ্তবয়স্কদের(!) উচ্চশিক্ষা প্রতিষ্ঠান। চীনে তিন হাজার উচ্চশিক্ষা প্রতিষ্ঠানের মধ্যে দুই-তৃতীয়াংশই সরকারি। ছাত্র-ছাত্রীর সংখ্যা দুই কোটি। বিখ্যাত সব বিশ্ববিদ্যালই সরকারি। ভর্তির সময়/সেমিস্টারঃ চায়নায় স্প্রিং সেমিস্টারে সেপ্টেম্বর থেকে অক্টোবর পর্যন্ত এবং সামার সেমিস্টারে ভর্তি কার্যক্রম চলে মার্চ থেকে এপ্রিল পর্যন্ত। ডিগ্রিঃ সাধারণ অনার্স ডিগ্রি তিন বছর মেয়াদী। ব্যাচেলর ডিগ্রি চার বছর মেয়াদী। মাস্টার্স ও ডক্টরেট ডিগ্রি দুই থেকে তিন বছর মেয়াদী। ভর্তির যোগ্যতাঃ এইচএসসি অথবা সমমানের পরীক্ষায় উত্তীর্ণরা ব্যাচেলর ও অনার্সে ভর্তি হতে পারেন এবং এই দুই পরীক্ষায় উত্তীর্ণরা মাস্টার্সে ভর্তি হতে পারেন। চায়নায় পড়াশুনার জন্য হান ভাষা (চায়না ভাষা) জানা বাধ্যতামূলক। ইংরেজি ও হান ভাষা এই দুই ভাষায় অথবা শুধু হান ভাষায় পড়ানো হয়। এর বাইরেও কোন বিশ্ববিদ্যালয় টোয়েফল, জিআরই, জিএমএট এসএটি স্কোর চায়। কিছু বিশ্ববিদ্যালয় ভাষার ওপর বাংলাদেশের সার্টিফিকেট কোর্সের সনদ গ্রহণ করে।
চায়নায় ভর্তি ও ভিসা ।
</span>
	</div>
				
  
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		
		

		</div>
              </div>  
            </div>
       <!--footer-->
	   <link href="http://localhost/application/resource/style.css" rel="stylesheet">
<div class="footer">
	  <div class="footer-grids">
		  <div class="container">
			  <div class="col-md-3 col-xs-6 footer-grid">
					
					<ul>
						 					</ul>
			  </div>
			 <div class="col-md-3 col-xs-6 footer-grid">
				
				  <ul>
										  </ul>
			 </div>
			 <div class="col-md-3 col-xs-6 footer-grid">
					
					<ul>
											</ul>
			 </div>
			 <div class="col-md-3 col-xs-6 footer-grid contact-grid">
					
					<ul>
											
				</ul>
				  <ul class="social-icons">
						<li><a href="https://www.facebook.com/nextadmissionbd/" title="Facebook" target="_blank"><span class="facebook"> </span></a></li>
						<li><a href="#" target="_blank"><span class="twitter" title="Twitter"> </span></a></li>
						<li><a href="#"><span class="thumb"> </span></a></li>
					</ul>
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<div class="copy" style="background:#2E2E2E;">
		 <p style="color:#CCCCCC;">Next Admission.com ©2015. All Rights Reserved.</p>
 </div>	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>