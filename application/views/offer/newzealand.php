<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		.visa:hover{ border-radius:10px; background: #e3e3e3;}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
<div class="col-lg-8 col-md-8 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
<h2><strong><span style="color: #3366ff;">নিউজিল্যান্ড সম্পর্কে</span></strong></h2>
<span style="color: #000000;">নিউজিল্যান্ড (ইংরেজি New Zealand নিউ যীল্যান্ড, মাওরি Aotearoa আওটেয়ারোয়া) ওশেনিয়ার একটি দ্বীপ রাষ্ট্র। এটি অস্ট্রেলিয়ার দক্ষিণ-পূর্ব দিকে অবস্থিত। এর রাজধানীর নাম ওয়েলিংটন। নিউজিল্যান্ড অসংখ্য ক্ষুদ্র দ্বীপের সমন্বয়ে গঠিত। তবে এদের মধ্যে সবচেয়ে উল্লেখযোগ্য হল স্টুয়ার্ট দ্বীপ এবং চাথাম দ্বীপ। নিউজিল্যান্ডের আদিম অধিবাসীদের ভাষা হল মাওরি।রাজধানী	ওয়েলিংটন
৪১°১৭′ দক্ষিণ ১৭৪°২৭′ পূর্ব বৃহত্তম শহর	অকল্যান্ড।নিউজিল্যান্ডের রাষ্ট্রপ্রধান হল ইংল্যান্ডের রাণী দ্বিতীয় এলিজাবেথ। তাঁর প্রতিনিধি নিউজিল্যান্ডের সরকার রাষ্ট্রীয় ক্ষমতার অধিকারী। প্রকৃতপক্ষে রাষ্ট্রের রাজনৈতিক ব্যাপের রাণীর কোন প্রভাব নেই, রাণী কেবল আনুষ্ঠানিকভাবে রাষ্ট্রপ্রধান। গণতান্ত্রিকভাবে নির্বাচিত প্রধানমন্ত্রীর অধীন সংসদই হল রাষ্ট্র ক্ষমতার অধিকারী। প্রধানমন্ত্রীই নিউজিল্যান্ডের সরকার প্রধান।</span>
<h2><strong><span style="color: #3366ff;">নিউজিল্যান্ডে উচ্চ শিক্ষা</span></strong></h2>
<span style="color: #000000;">
 নিউজিল্যান্ডে উচ্চশিক্ষার জন্য ডিপ্লোমা ডিগ্রি, ব্যাচেলর্স ডিগ্রি, অনার্স ডিগ্রি, মাস্টার্স ডিগ্রি ও ডক্টরেট করার করার সুযোগ রয়েছে। তিনটি সেমিস্টারে শিক্ষা কার্যক্রম চলে। প্রথম সেমিস্টার ফেব্রুয়ারিতে দ্বিতীয় সেমিস্টার জুলাইয়ে তৃতীয় সেমিস্টার নভেম্বরে শুরু হয়। তবে বেশির ভাগ বিশ্ববিদ্যালয়ে দু্থটি সেমিস্টারে ভর্তি করা হয়।
ভর্তির যোগ্যতা : ব্যাচেলর্স ডিগ্রিতে ভর্তির জন্য টোফেল সিবিটি স্কোর ২১৩ অথবা টোফেল আইবিটি স্কোর ৭৯-৮০ অথবা আইইএলটিএস স্কোর ৬ থাকতে হয়। মাস্টার্সে ভর্তির জন্য টোফেল সিবিটি স্কোর ২৪০ অথবা টোফেল আইবিটি স্কোর ৯৪-৯৫ অথবা আইইএলটিএস স্কোর ৬.৫ থাকতে হয়।
কোর্সের মেয়াদ : ডিপ্লোমা ডিগ্রি দু্থবছরমেয়াদি, ব্যাচেলর্স ডিগ্রি তিন বছর মেয়াদি, অনার্স ডিগ্রি চার বছরমেয়াদি, মাস্টার্স ডিগ্রি দু্থবছরমেয়াদি এবং ডক্টরেট ডিগ্রি সাধারণ বিষয়ে ৩ বছরমেয়াদি এবং মেডিকেল সায়েন্সে ৪-৫ বছরমেয়াদি।
বিষয় : এগ্রিকালচার, হর্টিকালচার, কোয়ান্টিটি সার্ভেইং, এডভান্সড স্টাডিস ফর টিচার, ল্যাঙ্গুয়েজ, সোস্যাল সায়েন্স, ব্যবসায় প্রশাসন, একাউন্ট্যান্সি, অফিস সিস্টেম, ম্যানেজমেন্ট, কমিউনিটি এডুকেশন, নন-ফরমাল এডুকেশন, জেনারেল এডুকেশন, কম্পিউটার সায়েন্স, ভেনস্টিট্রি, ইঞ্জিনিয়ারিং, টেকনোলজি, ফাইন আর্টস, ডিজাইন, হেলথ সায়েন্স, আইন, মেডিসিন, মিউজিক, ট্রেড, ভেটেরিনারি, নার্সিং, মেডিকেল ইমেজিং, মিডউইফারি, অকুপেশনাল থেরাপি, ফিজিওথেরাপি, মেডিকেল রেডিয়েশন থেরাপি, ফার্মেসি, ক্লিনিক্যাল সাইকোলজিসহ বিভিন্ন বিষয়ে উচ্চশিক্ষার জন্য নিউজিল্যান্ড যেতে পারেন।</span>
	</div>
	
			<a class="js-open-modal " href="#" data-modal-id="popup1" style="text-decoration:none;">	
		<div class="col-lg-4 col-md-4 col-xs-12" style="text-align: justify;
                text-justify: inter-word; font-size:20px;border: 2px solid rgb(60, 225, 89);
			border-radius: 10px;" id="visa" >	
			<div class="visa">
			
<h2 align="center"><strong><span style="color: #3366ff;">নিউজিল্যান্ডে ভর্তি ও ভিসা</span></strong></h2>
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>IELTS 5.5 or English Medium Study</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ 8000 $ to 10000$</br>
<span style="color:#3366ff;">&#10140;</span>স্পন্সারঃ 20 Lac to 40 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>টোটাল কস্টঃ 8 Lac to 10 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 8 week</br>
<span style="color:#3366ff;">&#10140;</span><font style="color:red;">Note:</font> cost is different on according to course & institutions.</br>
</p>
</div>
	</div>
	</a>
		<!--pop up code start-->	
<div id="popup1" class="modal-box">
  <header> <a href="#" class="js-modal-close close">×</a>
<h2 align="center"><strong><span style="color: #3366ff;">নিউজিল্যান্ডে ভর্তি ও ভিসা</span></strong></h2>
  </header>
  <div class="modal-body">
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>IELTS 5.5 or English Medium Study</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ 8000 $ to 10000$</br>
<span style="color:#3366ff;">&#10140;</span>স্পন্সারঃ 20 Lac to 40 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>টোটাল কস্টঃ 8 Lac to 10 Lac taka<br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 8 week</br>
<span style="color:#3366ff;">&#10140;</span><font style="color:red;">Note:</font> cost is different on according to course & institutions.</br>
</p>
  </div>
  <footer> <a href="#" class=" btn-danger btn-small js-modal-close">Close</a> </footer>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		
              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>