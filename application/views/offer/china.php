<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
     
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		.visa:hover{ border-radius:10px; background: #e3e3e3;}
		td > a:link {
			color: green !important;
		}
		
		td > a:visited {
			color: #000080 !important;
		}
		
		td > a:hover {
			color: #398EE6 !important;
		}
		
		td > a:active {
			color: #398EE6 !important;
		}
		tr > th{
			text-align: center;
		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
		    padding: 8px;
		    line-height: 1.42857143;
		    vertical-align: top;
		    border-top: 1px solid #F012BE !important;
		}
		.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
		    border: 1px solid #F012BE;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     


<h2 style="max-width:445px; margin:0 auto;"><span style="color:#FF0000; font-size:30px; text-shadow:2px 2px #FF3333;">চায়নায় ২০১৬ সালে সর্বশেষ ভর্তির সুযোগ</span></h2> 
<!--Row Start Here-->
<div class="row">
<!--Column 4 Start Here-->

	<div class="col-lg-12 col-md-12 col-xs-12" style="text-align: justify; text-justify: inter-word; font-size:20px; border: 2px solid #0074D9; border-radius: 10px;" id="visa" >

	<!--University List Start Here-->
	<h2 style="color: #fff; text-align: center; background: #000080">বিস্তারিত জানতে University নাম এর উপর Click করুন</h2>
	<a target="_blank" style="float: right; margin-top: -43px; padding: 5px 10px; background: #F012BE; border-radius: 5px; color: #fff !important;" href="<?php echo base_url('china/aboutChina');?>">About China</a>
	<table class="table table-bordered">
		<thead>
			<th>SL.</th>
			<th>University/Institute Name</th>
			<th>State/City Name</th>
			<th>Tutinon Fee Range</th>
		</thead>
		<tbody>
			<tr>
				<td>1</td> 
			 	<td><a target="_blank" href="http://www.cmu.edu.cn">China Medical University</a></td> 
			 	<td>China, Liaoning, Shenyang, Heping</td>
			 	<td></td> 
			</tr>
			<tr>
				<td>2</td>
			  	<td><a target="_blank" href="http://en.jlu.edu.cn/University/">Jilin University</a></td>
			 	<td>Nanguan, Changchun, Jilin, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>3</td>
			  	<td><a target="_blank" href="http://www.nankai.edu.cn">Nankai University</a></td>
			 	<td>Nankai, Tianjin, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>4</td>
			 	<td><a target="_blank" href="http://www.ccmu.edu.cn/‎">Capital Medical University</a></td>
			 	<td>China, Beijing, Fengtai</td>
			 	<td></td>
			</tr>
			<tr>
				<td>5</td>
			  	<td><a target="_blank" href="http://english.hust.edu.cn/">Huazhong University of Science and Technology</a></td>
			 	<td>Hongshan, Wuhan, Hubei, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>6</td>
			  	<td><a target="_blank" href="http://eng.ujs.edu.cn/">Jiangsu University</a></td>
			 	<td>Jingkou, Zhenjiang, Jiangsu, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>7</td>
			  	<td><a target="_blank" href="http://www.xzmc.edu.cn/">Xuzhou Medical College</a></td>
			 	<td>China, Jiangsu, Xuzhou, Yunlong</td>
			 	<td></td>
			</tr>
			<tr>
				<td>8</td>
			  	<td><a target="_blank" href="http://www.nbu.edu.cn/">Ningbo University</a></td>
			 	<td>Jiangbei, Ningbo, Zhejiang, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>9</td>
			  <td><a target="_blank" href="http://www.dlmedu.edu.cn/english/">Dalian Medical University</a></td>
			 	<td>Lvshunkou, Dalian, Liaoning, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>10</td>
			  	<td><a target="_blank" href="http://www.sdu.edu.cn/english/‎">Shandong University</a></td>
			 	<td>China, Shandong, Jinan, Lixia</td>
			 	<td></td>
			</tr>
			<tr>
				<td>11</td>
			  	<td><a target="_blank" href="http://www.zju.edu.cn/english/">Zhejiang University</a>‎</td>
			 	<td>Xihu, Hangzhou, Zhejiang, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>12</td>
			  	<td><a target="_blank" href="http://www.fudan.edu.cn/englishnew/">Fudan University</a></td>
			 	<td>Yangpu, Shanghai, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>13</td>
			  	<td><a target="_blank" href="http://en.whu.edu.cn/">Wuhan University</a></td>
			 	<td>Wuchang, Wuhan, Hubei, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>14</td>
			  	<td><a target="_blank" href="http://www.scu.edu.cn/en/">Sichuan University</a></td>
			 	<td>Wuhou, Chengdu, Sichuan, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>15</td>
			  	<td><a target="_blank" href="http://www.xjtu.edu.cn/en/">Xi'an Jiaotong University</a></td>
			 	<td>Beilin, Xi'an, Shaanxi, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>16</td>
			  	<td><a target="_blank" href="http://www.tongji.edu.cn/english/‎">Tongji University</a></td>
			 	<td>Yangpu, Shanghai, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>17</td>
			  	<td><a target="_blank" href="http://www.seu.edu.cn/‎">Southeast University</a></td>
			 	<td>Xuanwu, Nanjing, Jiangsu, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>18</td>
			  	<td><a target="_blank" href="http://www.kmmc.cn/‎">Kunming Medical University</a></td>
			 	<td>Xishan, Kunming, Yunnan, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>19</td>
			  	<td><a target="_blank" href="http://www.bcu.edu.cn/">Beijing City University</a></td>
			 	<td>Beijing, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>20</td>
			  	<td><a target="_blank" href="http://www.hunaneu.com/">Hunan International Economic University</a></td>
			 	<td>Hunan, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>21</td>
			  	<td><a target="_blank" href="http://www.nclg.com.cn/">Nanchang Institute of Technology</a></td>
			 	<td>Jiangxi, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>22</td>
			  	<td><a target="_blank" href="http://www.nclg.com.cn/">Nanchang Institute of Technology</a></td>
			 	<td>Jiangxi, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>23</td>
			  	<td><a target="_blank" href="http://www.jxut.edu.cn/">NanJiangxi BlueSky University</a></td>
			 	<td>Jiangxi, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>24</td>
			  	<td><a target="_blank" href="http://www.axhu.cn/en/">Anhui Xinhua University</a></td>
			 	<td>Anhui, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>25</td>
			  	<td><a target="_blank" href="http://202.101.109.130/english/english.htm">Yang-En University</a></td>
			 	<td>Fujian, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>26</td>
			  	<td><a target="_blank" href="http://en.eurasia.edu/">Xi'an Eurasia University</a></td>
			 	<td>Shaanxi, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>27</td>
			  	<td><a target="_blank" href="http://www.gench.com.cn/">Shanghai Jianqiao College</a></td>
			 	<td>Shanghai, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>28</td>
			  	<td><a target="_blank" href="http://www.huabridge.com/">Jilin Huaqiao Foreign Languages Institute</a></td>
			 	<td>Jilin, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>29</td>
			  	<td><a target="_blank" href="http://xafy.edu.cn/">Xi’an Fanyi University</a></td>
			 	<td>Shaanxi, China</td>
			 	<td></td>
			</tr>
			<tr>
				<td>30</td>
			  	<td><a target="_blank" href="http://www.sandau.edu.cn/">Shanghai Sanda Institute</a></td>
			 	<td>Shanghai, China</td>
			 	<td></td>
			</tr>
		<tbody>
	</table>
	<!--Start Modal-->
			<div class="visa">
				<h2 align="center"><strong><span style="color: #3366ff;">Admission Requirements</span></strong></h2>
				<span style="color:#3366ff;">&#10140;</span>Academic Transcript</br>
				<span style="color:#3366ff;">&#10140;</span>Photo 1 Copy (Passport Size)</br>
				<span style="color:#3366ff;">&#10140;</span>Passport Copy/Birth Certificate/National ID</br>
				<span style="color:#3366ff;">&#10140;</span>Registration Fees</br>
				</p>
			</div>
	</div>
	<!--Column 4 End Here-->
</div>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		
		
              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>