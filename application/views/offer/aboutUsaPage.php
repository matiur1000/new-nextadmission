<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
      
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		.visa:hover{ border-radius:10px; background: #e3e3e3;}
    span{
      font-size: 18px;
    }
  </style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
<div class="col-lg-12 col-md-12 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
<h2 style="color: #3366ff;">মার্কিন যুক্তরাষ্ট্র সম্পর্কে</h2>
<span style="color: #000000;">মার্কিন যুক্তরাষ্ট্র (ইংরেজি: United States of America ইউনাইটেড্ স্টেইট্‌স্ অফ্ আমেরিকা) উত্তর আমেরিকায় অবস্থিত পঞ্চাশটি রাজ্য ও একটি যুক্তরাষ্ট্রীয় জেলা নিয়ে গঠিত এক যুক্তরাষ্ট্রীয় সাংবিধানিক প্রজাতন্ত্র। এই দেশটি ইউনাইটেড স্টেটস, ইউ. এস. , যুক্তরাষ্ট্র ও আমেরিকা যুক্তরাষ্ট্র নামেও পরিচিত। মার্কিন যুক্তরাষ্ট্রের আয়তন ৩.৭৯ মিলিয়ন বর্গমাইল (৯.৮৩ মিলিয়ন বর্গকিলোমিটার)। দেশের জনসংখ্যা প্রায় ৩০৯ মিলিয়ন। সামগ্রিক আয়তনের হিসেবে মার্কিন যুক্তরাষ্ট্র বিশ্বের তৃতীয় অথবা চতুর্থ বৃহত্তম রাষ্ট্র। আবার স্থলভূমির আয়তন ও জনসংখ্যার হিসেবে যুক্তরাষ্ট্র বিশ্বের তৃতীয় বৃহত্তম দেশ।</span>
<h2 style="color: #3366ff;">মার্কিন যুক্তরাষ্ট্রে উচ্চ শিক্ষা</h2>
<span style="color: #000000;">উচ্চ শিক্ষার ক্ষেত্রে বিশ্বের অন্যতম শীর্ষস্থানীয় দেশ হচ্ছে আমেরিকা বা মার্কিন যুক্তরাষ্ট্র। পৃথিবীর অন্যান্য দেশের মতো বাংলাদেশ থেকেও প্রতিবছর বেশ কিছু সংখ্যক শিক্ষার্থী উচ্চ শিক্ষার উদ্দেশ্যে আমেরিকায় পাড়ি জমান।
ডিগ্রী সমূহ:
আমেরিকার উচ্চ শিক্ষা প্রতিষ্ঠানসমূহে নিম্নলিখিত ডিগ্রীগুলো প্রদান করা হয়:
এসোসিয়েট ডিগ্রী
ব্যাচেলর ডিগ্রী
মাষ্টার্স ডিগ্রী
পি,এইচ,ডি বা ডক্টরেট ডিগ্রী। স্প্রিং সেমিষ্টার: জানুয়ারী থেকে মে পর্যন্ত
সামার সেমিষ্টার: মে থেকে জুলাই পর্যন্ত
ফল সেমিষ্টার: আগষ্ট থেকে ডিসেম্বর পর্যন্ত ।</span>
</div>
	
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		

              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
			
	
    
  </body>
</html>