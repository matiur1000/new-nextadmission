<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		span{
			font-size: 16px;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
					<h2 style="color: #3366ff;">মালয়েশিয়া সম্পর্কে </h2>
					<span style="color: #000000;">মালয়েশিয়া (মালয়: Malaysia) তেরটি রাষ্ট্র এবং তিনটি ঐক্যবদ্ধ প্রদেশ নিয়ে গঠিত দক্ষিনপূর্ব এশিয়ার একটি দেশ। [১] যার মোট আয়তন ৩,২৯,৮৪৫ বর্গকিমি।[২] দেশটির রাজধানী শহর কুয়ালালামপুর এবং পুত্রজায়া হল ফেডারেল সরকারের রাজধানী। দক্ষিণ চীন সাগর দ্বারা দেশটি দুই ভাগে বিভক্ত, পেনিনসুলার মালয়েশিয়া এবং পূর্ব মালয়েশিয়া। মালয়েশিয়ার স্থল সীমান্তে রয়েছে থাইল্যান্ড, ইন্দোনেশিয়া, এবং ব্রুনাই; এর সমুদ্র সীমান্ত রয়েছে সিঙ্গাপুর, ভিয়েতনাম ও ফিলিপাইন এর সাথে। [২] মালয়েশিয়ার মোট জনসংখ্যা ২৮ মিলিয়নের অধিক।</span>
					<h2 style="color: #3366ff;">মালয়েশিয়ায় উচ্চ শিক্ষা</h2>
					<span style="color: #000000;">বর্তমানে মালয়েশিয়া একটি উন্নত দেশ । আপনি যদি Software Engineering, Electrical, Mechanical, Automobile, Computer, Aerospace, Engineering পড়তে চান, তবে Malaysia কে বেছে নিতে পারেন। পড়াশুনার পাশাপাশি কাজের সুযোগ, ভিসার নিশ্চয়তা, Credit Transfer করে UK, Australia, USA তে যাবার সুযোগ থাকাতে মালয়েশিয়া এখন সবচেয়ে আকর্ষণীয় দেশ। </span>
				</div>
		</div> 
</a>        
	<!--pop up code start-->	
<div id="popup1" class="modal-box">
  <header> <a href="#" class="js-modal-close close">×</a>
	<h2 align="center" style="color: #3366ff;">মালয়েশিয়ায় ভর্তি ও ভিসা</h2>
  </header>
  <div class="modal-body">
<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>EMGS Fee:1850 RM</br>
<span style="color:#3366ff;">&#10140;</span>টিউশন ফিঃ350 to 14000 RM</br>
<span style="color:#3366ff;">&#10140;</span>এয়ার টিকেট ফিঃ 22000 taka</br>
<span style="color:#3366ff;">&#10140;</span>থাকার খরচঃ 2400 RM</br>
<span style="color:#3366ff;">&#10140;</span>মোট খরচঃ 170,000 taka to 43
0,000 taka</br>
<span style="color:#3366ff;">&#10140;</span>Processing Time: 6 week</br>
</p>
  </div>
  <footer> <a href="#" class=" btn-danger btn-small js-modal-close">Close</a> </footer>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		




		</div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>