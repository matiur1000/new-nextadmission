<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
     
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		.visa:hover{ border-radius:10px; background: #e3e3e3;}
		td > a:link {
			color: green !important;
		}
		
		td > a:visited {
			color: #000080 !important;
		}
		
		td > a:hover {
			color: #398EE6 !important;
		}
		
		td > a:active {
			color: #398EE6 !important;
		}
		tr > th{
			text-align: center;
		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
		    padding: 8px;
		    line-height: 1.42857143;
		    vertical-align: top;
		    border-top: 1px solid #F012BE !important;
		}
		.table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
		    border: 1px solid #F012BE;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     


<h2 style="max-width:530px; margin:0 auto; text-align:center;"><span style="color:#FF0000; font-size:30px; text-shadow:2px 2px #FF3333;">মালয়েশিয়া ২০১৬ সালে সর্বশেষ ভর্তির সুযোগ</span></h2> 
<!--Row Start Here-->
<div class="row">
<!--Column 4 Start Here-->

	<div class="col-lg-12 col-md-12 col-xs-12" style="text-align: justify; text-justify: inter-word; font-size:20px; border: 2px solid #0074D9; border-radius: 10px;" id="visa" >
	<a target="_blank" style="float: right; margin-top: -43px; padding: 5px 10px; background: #F012BE; border-radius: 5px; color: #fff !important;" href="<?php echo base_url('malaysia/Medical_list_Malaysia');?>">Medical list for Malaysia</a>
	<!--University List Start Here-->
	<h2 style="color: #fff; text-align: center; background: #000080">বিস্তারিত জানতে University নাম এর উপর Click করুন</h2>
	<a target="_blank" style="float: right; margin-top: -43px; padding: 5px 10px; background: #F012BE; border-radius: 5px; color: #fff !important;" href="<?php echo base_url('malaysia/aboutMalaysia');?>">About Malaysia</a>
	<table class="table table-bordered">
		<thead>
			<th>SL.</th>
			<th>University/Institute Name</th>
			<th>State/City Name</th>
			<th>Tutinon Fee Range</th>
		</thead>
		<tbody>
			<tr>
				<td>1</td> 
			 	<td><a target="_blank" href="http://www.rima.edu.my/">Rima College</a></td>
			 	<td> Johor Bahru Johor, Malaysia</td> 
			 	<td></td> 
			</tr>
			<tr>
				<td>2</td>
			  	<td><a target="_blank" href="http://www.lincoln.edu.my/">Lincoln University college</a></td>
			  	<td>Petaling Jaya, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>3</td>
			  	<td><a target="_blank" href="https://www.limkokwing.net">Limkok wing Univesity</a></td>
			  	<td>Cyberjaya, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>4</td>
			 	<td><a target="_blank" href="http://www.segi.edu.my/">Segi University</a></td>
			 	<td>Petaling Jaya, Selangor, Malaysia</td> 
			 	<td></td> 
			</tr>
			<tr>
				<td>5</td>
			  	<td><a target="_blank" href="http://university.taylors.edu.my/">Taylors University</a></td>
			  	<td>Subang Jaya, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>6</td>
			  	<td><a target="_blank" href="http://www.um.edu.my/">University of Malaya</a></td>
			  	<td>Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>7</td>
			  	<td><a target="_blank" href="http://www.usm.my/">Universiti Sains Malaysia</a></td>
			  	<td>Gelugor, Penang, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>8</td>
			  	<td><a target="_blank" href="http://www.utm.my/">Universiti Teknologi Malaysia</a></td>
			  	<td>Skudai, Johor, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>9</td>
			  	<td><a target="_blank" href="http://www.upm.edu.my/">Universiti Putra Malaysia</a></td>
			  	<td>Serdang, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>10</td>
			  	<td><a target="_blank" href="http://www.ukm.my/">Universiti Kebangsaan Malaysia / National University of Malaysia</a></td>
			  	<td>Bangi, Selangor, Malaysia</td>
			  	<td></td>
			</tr>

			<tr>
				<td>11</td>
			  	<td><a target="_blank" href="http://www.uitm.edu.my/index.php/en/">Universiti Teknologi MARA / MARA University of Technology</a></td>
			  	<td>Shah Alam, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>12</td>
			  	<td><a target="_blank" href="http://www.uniten.edu.my/Pages/default.aspx">Universiti Tenaga Nasional</a></td>
			  	<td>Bandar Baru Bangi, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>13</td>
			  	<td><a target="_blank" href="http://www.iium.edu.my/">International Islamic University of Malaysia</a></td>
			  	<td>Kuala Lumpur, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>14</td>
			  	<td><a target="_blank" href="http://www.uthm.edu.my/v2/">Universiti Tun Hussein Onn Malaysia</a></td>
			  	<td>Parit Raja, Johor, Malaysia</td> 
			  	<td></td> 
			</tr>

			<tr>
				<td>15</td>
			  	<td><a target="_blank" href="http://www.ump.edu.my/">Universiti Malaysia Pahang</a></td>
			  	<td>Pekan, Pahang, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>16</td>
			  	<td><a target="_blank" href="https://www.mmu.edu.my/">Multimedia University</a></td>
			  	<td>Cyberjaya, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>17</td>
			  	<td><a target="_blank" href="http://www.unimap.edu.my/index.php/my/">Universiti Malaysia Perlis</a></td>
			  	<td>Arau, Perlis, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>18</td>
			  	<td><a target="_blank" href="http://www.nottingham.edu.my/index.aspx">University of Nottingham Malaysia</a></td>
			  	<td>Semenyih, Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>19</td>
			  	<td><a target="_blank" href="http://www.ums.edu.my/v5/index.php/ms">University Malaysia Sabah</a></td>
			  	<td> Kota Kinabalu, Sabah, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>20</td>
			  	<td><a target="_blank" href="http://www.uum.edu.my/index.php/en/">Universiti Utara Malaysia</a></td>
			  	<td>Kedah, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>21</td>
			  	<td><a target="_blank" href="http://www.aimst.edu.my/">AIMST University</a></td>
			  	<td>Jalan Bedong-Semeling, Kedah, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>22</td>
			  	<td><a target="_blank" href="http://aeu.edu.my">Asia e University</a></td>
			  	<td>Federal Territory of Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>23</td>
			  	<td><a target="_blank" href="http://apu.edu.my">Asia Pacific University of Technology and Innovation</a></td>
			  	<td>Wilayah Persekutuan Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>24</td>
			  	<td><a target="_blank" href="http://bac.edu.my">Bricksfield Asia College</a></td>
			  	<td>Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>25</td>
			  	<td><a target="_blank" href="http://www.berjaya.edu.my/">Berjaya University College of Hospitality</a></td>
			  	<td>Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>26</td>
			  	<td><a target="_blank" href="http://binary.edu.my">Binary University College of Management and Entrepreneurship</a></td>
			  	<td>Selangor, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>27</td>
			  	<td><a target="_blank" href="http://www.curtin.edu.my/">Curtin University of Technology</a></td>
			  	<td>Sarawak, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>28</td>
			  	<td><a target="_blank" href="http://cybermed.edu.my/">Cyberjaya University College of Medical Sciences</a></td>
			  	<td>Cyberjaya, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>29</td>
			  	<td><a target="_blank" href="http://help.edu.my/">HELP University</a></td>
			  	<td>Wilayah Persekutuan Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>
			<tr>
				<td>30</td>
			  	<td><a target="_blank" href="http://www.inceif.org/">International Centre for Education in Islamic Finance</a></td>
			  	<td>Wilayah Persekutuan Kuala Lumpur, Malaysia</td> 
			  	<td></td> 
			</tr>
		<tbody>
	</table>

<!--Start Modal-->
			<div class="visa">
				<h2 align="center"><strong><span style="color: #3366ff;">Admission Requirements</span></strong></h2>
				<span style="color:#3366ff;">&#10140;</span>Academic Transcript</br>
				<span style="color:#3366ff;">&#10140;</span>Photo 1 Copy (Passport Size)</br>
				<span style="color:#3366ff;">&#10140;</span>Passport Copy/Birth Certificate/National ID</br>
				<span style="color:#3366ff;">&#10140;</span>Registration Fees</br>
				</p>
			</div>
	</div>
	<!--Column 4 End Here-->
</div>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		
		
              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>