<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
     
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		span{
			font-size: 16px;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     

<!--Row Start Here-->
<div class="row">
<!--Column 8 Start Here-->
	<div class="col-lg-12 col-md-12 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">

		
		<h3 style="color: #3366ff;">ভারত সম্পর্কে</h3> 
		<span style="color: #000000;">ভারত দক্ষিণ এশিয়ার একটি রাষ্ট্র। দেশটির সরকারি নাম ভারতীয় প্রজাতন্ত্র। ভৌগোলিক আয়তনের বিচারে এটি দক্ষিণ এশিয়ার বৃহত্তম এবং বিশ্বের সপ্তম বৃহত্তম রাষ্ট্র। অন্যদিকে জনসংখ্যার বিচারে এই দেশ বিশ্বের দ্বিতীয় সর্বাধিক জনবহুল তথা বৃহত্তম গণতান্ত্রিক রাষ্ট্র। ভারতের পশ্চিম সীমান্তে পাকিস্তান[১৫] উত্তর-পূর্বে চীন, নেপাল, ও ভূটান এবং পূর্বে বাংলাদেশ, মায়ানমার ও মালয়েশিয়া অবস্থিত। এছাড়া ভারত মহাসাগরে অবস্থিত শ্রীলঙ্কা, মালদ্বীপ ও ইন্দোনেশিয়া ভারতের নিকটবর্তী কয়েকটি দ্বীপরাষ্ট্র। দক্ষিণে ভারত মহাসাগর, পশ্চিমে আরব সাগর ও পূর্বে বঙ্গোপসাগর দ্বারা বেষ্টিত ভারতের উপকূলরেখার সম্মিলিত দৈর্ঘ্য ৭,৫১৭ কিলোমিটার (৪,৬৭১ মাইল)।[১৬]</span>
		<table class="infobox geography vcard">
		<tbody>
		<tr class="mergedtoprow">
		<td style="text-align: center;" colspan="2"><span style="color: #000000;"><b>রাজধানী</b></span></td>
		<td style="text-align: center;"><span style="color: #000000;"><a class="mw-redirect" style="color: #000000;" title="নয়া দিল্লি" href="https://bn.wikipedia.org/wiki/%E0%A6%A8%E0%A6%AF%E0%A6%BC%E0%A6%BE_%E0%A6%A6%E0%A6%BF%E0%A6%B2%E0%A7%8D%E0%A6%B2%E0%A6%BF">নয়া দিল্লি</a></span></td>
		</tr>
		<tr class="mergedbottomrow">
		<th colspan="2"><strong><span style="color: #000000;">বৃহত্তম শহর</span></strong></th>
		<td style="text-align: center;"><span style="color: #000000;"><a style="color: #000000;" title="মুম্বই" href="https://bn.wikipedia.org/wiki/%E0%A6%AE%E0%A7%81%E0%A6%AE%E0%A7%8D%E0%A6%AC%E0%A6%87">মুম্বই</a></span></td>
		</tr>
		<tr>
		<th colspan="2"><span style="color: #000000;">রাষ্ট্রীয় ভাষাসমূহ</span></th>
		<td>
		<div id="NavFrame1" class="NavFrame collapsed">
		<div class="NavHead"><span style="color: #000000;"><a class="mw-redirect" style="color: #000000;" title="ভারতের রাষ্ট্রভাষা" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%B0%E0%A6%BE%E0%A6%B7%E0%A7%8D%E0%A6%9F%E0%A7%8D%E0%A6%B0%E0%A6%AD%E0%A6%BE%E0%A6%B7%E0%A6%BE#.E0.A6.B9.E0.A6.BF.E0.A6.A8.E0.A7.8D.E0.A6.A6.E0.A6.BF_.E0.A6.93_.E0.A6.87.E0.A6.82.E0.A6.B0.E0.A7.87.E0.A6.9C.E0.A6.BF">হিন্দি, ইংরেজি</a><a id="NavToggle1" class="NavToggle" style="color: #000000;" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4#">[দেখাও]</a></span></div>
		</div></td>
		</tr>
		<tr>
		<th colspan="2"><span style="color: #000000;"><a class="mw-redirect" style="color: #000000;" title="ভারতের রাষ্ট্রভাষা" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%B0%E0%A6%BE%E0%A6%B7%E0%A7%8D%E0%A6%9F%E0%A7%8D%E0%A6%B0%E0%A6%AD%E0%A6%BE%E0%A6%B7%E0%A6%BE">সাংবিধানিক ভাষা</a></span></th>
		<td>
		<div id="NavFrame2" class="NavFrame collapsed">
		<div class="NavHead"><span style="color: #000000;"><a class="mw-redirect" style="color: #000000;" title="ভারতের রাষ্ট্রভাষা" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%B0%E0%A6%BE%E0%A6%B7%E0%A7%8D%E0%A6%9F%E0%A7%8D%E0%A6%B0%E0%A6%AD%E0%A6%BE%E0%A6%B7%E0%A6%BE#.E0.A6.B8.E0.A6.82.E0.A6.AC.E0.A6.BF.E0.A6.A7.E0.A6.BE.E0.A6.A8.E0.A7.87.E0.A6.B0_.E0.A6.85.E0.A6.B7.E0.A7.8D.E0.A6.9F.E0.A6.AE_.E0.A6.A4.E0.A6.AB.E0.A6.B8.E0.A6.BF.E0.A6.B2.E0.A6.AD.E0.A7.81.E0.A6.95.E0.A7.8D.E0.A6.A4_.E0.A6.B8.E0.A6.B0.E0.A6.95.E0.A6.BE.E0.A6.B0.E0.A6.BF_.E0.A6.AD.E0.A6.BE.E0.A6.B7.E0.A6.BE">অষ্টম তফসিল:</a><a id="NavToggle2" class="NavToggle" style="color: #000000;" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4#">[দেখাও]</a></span></div>
		</div></td>
		</tr>
		<tr>
		<th colspan="2"><span style="color: #000000;">জাতীয়তাসূচক বিশেষণ</span></th>
		<td style="text-align: center;"><span style="color: #000000;"><a style="color: #000000;" title="ভারতের জনপরিসংখ্যান" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%9C%E0%A6%A8%E0%A6%AA%E0%A6%B0%E0%A6%BF%E0%A6%B8%E0%A6%82%E0%A6%96%E0%A7%8D%E0%A6%AF%E0%A6%BE%E0%A6%A8">ভারতীয়</a></span></td>
		</tr>
		<tr>
		<th colspan="2"><span style="color: #000000;"><a style="color: #000000;" title="ভারতের রাজনীতি" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%B0%E0%A6%BE%E0%A6%9C%E0%A6%A8%E0%A7%80%E0%A6%A4%E0%A6%BF">সরকার</a></span></th>
		<td style="text-align: center;"><span style="color: #000000;"><a style="color: #000000;" title="যুক্তরাষ্ট্রীয় প্রজাতন্ত্র" href="https://bn.wikipedia.org/wiki/%E0%A6%AF%E0%A7%81%E0%A6%95%E0%A7%8D%E0%A6%A4%E0%A6%B0%E0%A6%BE%E0%A6%B7%E0%A7%8D%E0%A6%9F%E0%A7%8D%E0%A6%B0%E0%A7%80%E0%A6%AF%E0%A6%BC_%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%9C%E0%A6%BE%E0%A6%A4%E0%A6%A8%E0%A7%8D%E0%A6%A4%E0%A7%8D%E0%A6%B0">যুক্তরাষ্ট্রীয় প্রজাতন্ত্র</a></span></td>
		</tr>
		<tr class="mergedrow">
		<td><span style="color: #000000;"> </span></td>
		<td><strong><span style="color: #000000;"><a style="color: #000000;" title="ভারতের রাষ্ট্রপতি" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%B0%E0%A6%BE%E0%A6%B7%E0%A7%8D%E0%A6%9F%E0%A7%8D%E0%A6%B0%E0%A6%AA%E0%A6%A4%E0%A6%BF">রাষ্ট্রপতি</a></span></strong></td>
		<td style="text-align: center;"><span style="color: #000000;"><a class="mw-redirect" style="color: #000000;" title="প্রণব মুখার্জী" href="https://bn.wikipedia.org/wiki/%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%A3%E0%A6%AC_%E0%A6%AE%E0%A7%81%E0%A6%96%E0%A6%BE%E0%A6%B0%E0%A7%8D%E0%A6%9C%E0%A7%80">প্রণব মুখার্জী</a></span></td>
		</tr>
		<tr class="mergedbottomrow">
		<td><span style="color: #000000;"> </span></td>
		<td><strong><span style="color: #000000;"><a style="color: #000000;" title="ভারতের প্রধানমন্ত্রী" href="https://bn.wikipedia.org/wiki/%E0%A6%AD%E0%A6%BE%E0%A6%B0%E0%A6%A4%E0%A7%87%E0%A6%B0_%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%A7%E0%A6%BE%E0%A6%A8%E0%A6%AE%E0%A6%A8%E0%A7%8D%E0%A6%A4%E0%A7%8D%E0%A6%B0%E0%A7%80">প্রধানমন্ত্রী</a></span></strong></td>
		<td style="text-align: center;"><span style="color: #000000;"><a style="color: #000000;" title="নরেন্দ্র মোদী" href="https://bn.wikipedia.org/wiki/%E0%A6%A8%E0%A6%B0%E0%A7%87%E0%A6%A8%E0%A7%8D%E0%A6%A6%E0%A7%8D%E0%A6%B0_%E0%A6%AE%E0%A7%8B%E0%A6%A6%E0%A7%80">নরেন্দ্র মোদী</a></span></td>
		</tr>
		</tbody>
		</table>
		<h3 style="color: #3366ff;">ভারতে উচ্চ শিক্ষা</h2>
		<span style="color: #000000;">বাংলাদেশের নিকটতম প্রতিবেশী ভারতে উচ্চ শিক্ষা গ্রহনের ক্ষেত্রে বর্তমানে ব্যাপক সুযোগ রয়েছে। এ কারনে প্রতিবছর প্রচুর বাংলাদেশী শিক্ষার্থী উচ্চ শিক্ষার উদ্দেশ্যে ভারতে পাড়ি জমাচ্ছে।</span>
		
		<span style="color: #000000;"><strong>ডিগ্রী সমূহ:</strong></span>
		
		<span style="color: #000000;">ভারতে উচ্চ শিক্ষা প্রতিষ্ঠানগুলোতে নিম্নলিখিত ডিগ্রীগুলো প্রদান করা হয়:</span>
		<ul>
			<li><span style="color: #000000;">ডিপ্লোমা</span></li>
			<li><span style="color: #000000;">ব্যাচেলর</span></li>
			<li><span style="color: #000000;">মাষ্টার্স</span></li>
			<li><span style="color: #000000;">ডক্টরেট</span></li>
		</ul>
		<span style="color: #000000;"><strong>বিষয়সমূহ:</strong></span>
		
		<span style="color: #000000;">ভারতে একজন শিক্ষার্থী নিম্নলিখিত বিষয়সমূহে অধ্যয়ন করতে পারেন।</span>
		<ul>
			<li><span style="color: #000000;">কম্পিউটার বিজ্ঞান </span></li>
			<li><span style="color: #000000;">স্থাপত্য</span></li>
			<li><span style="color: #000000;">মেডিসিন এন্ড সার্জারী</span></li>
			<li><span style="color: #000000;">তড়িৎ প্রকৌশল</span></li>
			<li><span style="color: #000000;">রাসায়নিক প্রযুক্তি</span></li>
			<li><span style="color: #000000;">আইন</span></li>
			<li><span style="color: #000000;">যন্ত্র প্রকৌশল</span></li>
			<li><span style="color: #000000;">সঙ্গীত</span></li>
			<li><span style="color: #000000;">নার্সিং</span></li>
			<li><span style="color: #000000;">প্রাচ্যদেশীয় শিক্ষা</span></li>
			<li><span style="color: #000000;">শারিরীক শিক্ষা</span></li>
			<li><span style="color: #000000;">আয়ুর্বেদ চর্চা</span></li>
			<li><span style="color: #000000;">টেলিযোগাযোগ প্রকৌশল</span></li>
			<li><span style="color: #000000;">ভেটেরনারী সায়েন্স এন্ড অ্যানিম্যাল হাজব্যান্ড্রি</span></li>
			<li><span style="color: #000000;">গ্রন্থাগার বিজ্ঞান</span></li>
			<li><span style="color: #000000;">সমাজ কর্ম</span></li>
			<li><span style="color: #000000;">ব্যবসা ব্যবস্থাপনা</span></li>
			<li><span style="color: #000000;">হোমিওপ্যাথি</span></li>
			<li><span style="color: #000000;">কৃষি</span><span style="color: #000000;"> ইত্যাদিসহ আরো অনেক বিষয়।</span></li>
		</ul>
	</div>
<!--Column 8 End Here-->


<!--Row End Here-->

<!--pop up code start-->	
	<div id="popup1" class="modal-box">
	  <header> <a href="#" class="js-modal-close close">×</a>
	<h3 style="color: #3366ff;">ভারতে ভর্তি ও ভিসা</h3>
	  </header>
	  <div class="modal-body">
	  <p style="color: #000000; font-size:18px">খরচ কম কিন্তু পড়াশুনার মান অনেকেগুন বেশি। এক একটা বিশ্ববিদ্যালয় এর আয়তন ৫০ থেকে ৩০০ একর জমির উপর স্থাপিত।</p></br>

	<p style="color: #000000; font-size:20px; margin-bottom:10px;"><span style="color:#3366ff;">&#10140;</span>Tution Fees: INR 20,000 to 500,000/-</br>
	<span style="color:#3366ff;">&#10140;</span>Living & Food: INR 60,000 to 1,20,000/-</br>
	<span style="color:#3366ff;">&#10140;</span>থাকা খাওয়া খরচ মাসিক ৭ হাজার টাকা  যাহা ঢাকার চেয়েও কম।</br>
	<span style="color:#3366ff;">&#10140;</span>বাংলাদেশের যেকোনো বিশ্ববিদ্যালয় থেকে cradit transfer সম্ভব ।</br>
	<span style="color:#3366ff;">&#10140;</span>নিজস্ব হাসপাতালে চিকিৎসা ফ্রি !</br>
	<span style="color:#3366ff;">&#10140;</span>স্বাস্থ্য সম্মত খাবার সরবরাহ ।</br>
	</p>
  </div>
  <footer> <a href="#" class=" btn-danger btn-small js-modal-close">Close</a> </footer>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		
		
              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>