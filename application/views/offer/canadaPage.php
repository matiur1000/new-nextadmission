<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $page_title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
	
    <style>
		span{
			font-size: 16px;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container-fluid">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row">&nbsp;</div>            
       </div>
       <div class="container">         
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				


             <div class="row">&nbsp;</div>     
              <div class="row">
              	<div class="col-lg-12 col-md-12 col-xs-12 adcolor" style="color:#09659a; font-weight:normal; padding-left:12px;	text-align: justify; text-justify: inter-word;">
<h2 style="color: #3366ff;">কানাডা সম্পর্কে</h2>
<span style="color: #000000;">কানাডা (ইংরেজি: Canada; উচ্চরণ: /'kænədə/ ক্যানাডা, ফরাসিতে /kanada/ কানাদা) বিশ্বের দ্বিতীয় বৃহত্তম রাষ্ট্র যা উত্তর আমেরিকার উত্তরাঞ্চলের বেশির ভাগ এলাকা জুড়ে আছে।রাজধানী	অটোয়া
৪৫°২৪′ উত্তর ৭৫°৪০′ পশ্চিম
বৃহত্তম শহর	টরন্টো
রাষ্ট্রীয় ভাষাসমূহ	ইংরেজি, ফরাসি
কানাডার অধিকৃত ভূমি প্রথম বসবাসের জন্য চেষ্টা চালায় আদিবাসী জনগোষ্টিসমূহ। ১৫তম শতকের শুরুতে ইংরেজ এবং ফরাসি অভিযাত্রীরা আটলান্টিক উপকূল আবিষ্কার করে এবং পরে বসতি স্থাপনের উদ্যোগ নেয়। ফ্রান্স দীর্ঘ সাত বছরের যুদ্ধে পরাজয়ের ফলস্বরূপ ১৭৬৩ সালে উত্তর আমেরিকায় তাদের সব উপনিবাস ইংরেজদের কাছে ছেড়ে দেয়। ১৮৬৭ সালে, মৈত্রিতার মধ্য দিয়ে চারটি স্বায়ত্তশাসিত প্রদেশ নিয়ে দেশ হিসেবে কানাডা গঠন করা হয়। এর ফলে আরো প্রদেশ এবং অঞ্চল সংযোজনের পথ সুগম, এবং ইংল্যান্ড থেকে স্বায়ত্তশাসন পাওয়ার প্রক্রিয়া ত্বরান্বিত হয়। ১৯৮২ সালে জারীকৃত কানাডা অ্যাক্ট অনুসারে, দশটি প্রদেশ এবং তিনটি অঞ্চল নিয়ে গঠিত কানাডা সংসদীয় গণতন্ত্র এবং আইনগত রাজ্যতন্ত্র উভয়ই মেনে চলে। রাষ্ট্রের প্রধান রাণী দ্বিতীয় এলিজাবেথ। কানাডা দ্বিভাষিক (ইংরেজি ও ফরাসি ভাষা দুটোই সরকারী ভাষা) এবং বহুকৃষ্টির দেশ।।</span>
<h2 style="color: #3366ff;">কানাডায় উচ্চ শিক্ষা</h2>
<span style="color: #000000;">উচ্চশিক্ষার জন্য তরুণদের অন্যতম পছন্দের দেশ কানাডা। কানাডার শিক্ষা প্রতিষ্ঠানগুলোর ডিগ্রি যুক্তরাষ্ট্র এবং কমনওয়েলথভুক্ত দেশগুলোর সমতুল্য এবং সারা বিশ্বে কানাডার শিক্ষা প্রতিষ্ঠান থেকে নেয়া ডিগ্রিকে স্বীকৃতি দেয়া হয়। কানাডার শিক্ষা প্রতিষ্ঠানগুলোর ডিগ্রি বিশ্বের প্রথম সারির দেশগুলোর সাথে তুলনীয় হলেও অধিকাংশ ক্ষেত্রে আন্তর্জাতিক শিক্ষার্থীদের টিউশন ফি এবং থাকার খরচ যুক্তরাষ্ট্র ও ব্রিটেনের তুলনায় কম।কানাডার বিশ্ববিদ্যালয়গুলোর কোর্সগুলোকে দু'টো লেভেলে ভাগ করা হয়। একটি আন্ডারগ্রাজুয়েট বা ব্যাচেলর ডিগ্রি আর অন্যটি পোস্টগ্রাজুয়েট। মাস্টার্স এবং পিএইচডি'কে পোস্টগ্রাজুয়েট লেভেলের অংশ হিসেবে দেখা হয়।
কানাডার বিশ্ববিদ্যালয়গুলোতে শিক্ষাবর্ষকে সাধারণত তিনটি সেমিস্টারে ভাগ করা হয়:
১. ফল সেমিস্টার, সেপ্টেম্বর- ডিসেম্বর
২. উইন্টার, জানুয়ারি-এপ্রিল
৩. সামার, মে-আগস্ট</span>
</div>

</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> 
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });
 
});
 
$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});
 
$(window).resize();
 
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--popup code close-->		
	

              </div>  
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>
	   
       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="width: 800px;">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
	
	<script>
		//North America Effict

		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
		$("#country_id").change(function() {
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	
			
	   //ORGANIZATION WISE PROGRAM 
		$("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	
	  
		//Search all keyword	
		$('#fullSearch').submit(function(e) {
			var search_all 	= $("#search_all").val();   
			if(search_all == '') {
				alert("Please Enter Search Keyword");
				return false;
			} else {
			  return true;	
			}			
		});

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.post').on('click', function(e){
			var url = $(this).attr('data-url');
			
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
	</script>
	
    
  </body>
</html>