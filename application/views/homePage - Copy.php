<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $headerBasicInfo->title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/breakingNews.css'); ?>" rel="stylesheet">
	
	<link href="<?php echo base_url('resource/css/animate.css'); ?>" rel="stylesheet">
  	<script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/jquery.fittext.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/jquery.lettering.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/jquery.textillate.js'); ?>"></script>
	<script src="<?php echo base_url("resource/js/ajaxupload.3.5.js"); ?>"></script>
	
	

    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}

		body.modal
		{
		   overflow:hidden;
		   position:relative;
		}
		.breakingNews>.bn-title {
		    background: #00c853;
		}
		.breakingNews>.bn-title h2{
			color: #e51e54 !important;
		}

		.goog-te-menu-value {
		    text-decoration: none;
		    color: #0000cc;
		    white-space: nowrap;
		    margin-left: -4px !important;
		    margin-right: 4px;
		}
	</style>

	<link href="<?php echo base_url('resource/styleNew.css'); ?>" rel="stylesheet">
		
  </head>
  <body>
  <!-- Start Main wrapper -->
    <div style="box-shadow: 0px 1px 5px 1px rgba(50, 50, 50, .8); box-sizing: border-box; padding-right: 0px; padding-left: 0px;" class="container main_wrapper">  
		<!-- Header Section -->
		<header class="header_area">
			<?php $this->load->view('headerPageNew'); ?>
		</header>
		<!-- /Header Section -->
		<div class="clearfix"></div>
		<!-- Breaking News -->
		<section class="breakingN_area">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12" style="padding-bottom:5px;">
						<div class="breakingNews bn-small">
							<div class="bn-title"><h2>Breaking News</h2><span></span></div>
							<div class="marque_area">
								<ul>
									<marquee id='scroll_news'>
										<div onMouseOver="document.getElementById('scroll_news').stop();" onMouseOut="document.getElementById('scroll_news').start();">
										<?php foreach($breakInfo as $v){?>
											&nbsp; <i class="fa fa-hand-o-right" aria-hidden="true"></i> <a href="#" data-toggle="modal" data-target="#breaking_news_d" class="latesnewId" data-id="<?php echo $v->id; ?>"><?php echo $v->title; ?></a> 
										<?php } ?>
										</div>

									</marquee>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		<!-- /Breking news -->
		<!-- Start Main menu -->
		<section class="main_manu_area">
			<?php $this->load->view('menuPage'); ?>
		</section>
		<!-- /Main Menu -->
		<!-- Start Search -->
		<section class="search_area">
			<div class="container-fluid">
				<div class="search_box">
					<form id="searchForm" action="<?php echo site_url('home/searchAll'); ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">				
							<div class="input-group">
								<input type="text" name="search_all" id="search_all" class="form-control" placeholder="Search by keyword">
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i> Search</button>
								</span>												
							</div><!-- /input-group -->		
						</div>	
					</form> 

				    <div id="advancedSearch">
						<div class="col-lg-4 col-md-4">
							<div class="form-group">
								<select class="form-control" id="country_id_chnge" name="country_id"  tabindex="1">
									<option value="" selected>Select Country</option>
									<?php foreach ($countryInfo as $v){ ?>
									<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="form-group">
								<select class="form-control" id="organize_id" name="organize_id"  tabindex="2" >
									<option value="" selected>Select Organization</option>
									<?php foreach ($orgInfo as $v){ ?>
									<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="col-lg-4 col-md-4">
							<div class="form-group">
								<select class="form-control" id="program_id" name="program_id"  tabindex="3">
									<option value="" selected>Select Program</option>
									<?php foreach ($allProgramName as $v){ ?>
										<option value="<?php echo $v->id; ?>"><?php echo $v->programe_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

					</div>
						
					<p><a class="advanced">Advanced Search</a></p>
				</div>
			</div>
		</section>
		<!-- /Search -->
		<!-- Sidebar section -->
		<section class="ads_area">
			<div class="container-fluid">
				<!--Start main row-->
				<div class="row">
					<!--col 9 Start or left_sidebar and Bloa_area-->
					<div class="col-lg-9">

						<!--Start sub col 9 or blog_area-->
						<div class="col-lg-9 blog_post_area">
							<div id="searchOrganization" class="blog-post">
								<p style="text-align: justify;">জানুয়ারি-২০১৭ সেশনে ভারতে ভর্তি চলছে। রয়েছে ৫০% স্কলারশিপ পাওয়ার সুযোগ। সবাইকে জানাতে দয়াকরে LIKE ও SHARE করুণ। Assam Down Town University. বাংলাদেশের উত্তর-পূর্বে অবস্থিত ভারতের আসাম রাজ্যের রাজধানী Guwahati তে অবস্থিত। ইন্ডিয়া সরকারের ঘোষণা অনুযায়ী ভারতের ২০ টি Smart সিটির মধ্যে আসামের Guwahati অন্যতম। মেঘালয়ের শিলং , নেপাল, ভুটান, চায়নার প্রাকৃতিক সুন্দরজমন্ডিত পাহাড়ী অঞ্চল নিকটে থাকাই বিশুদ্ধ ও সহনশীল আবহাওয়া সহজ লভ্য, যাহা বাংলাদেশীদের জন্য অত্যন্ত মানানসই ও উপযুক্ত। যেখানে সকল আধুনিক সুবিধা সহ রয়েছে অল্প খরচে পড়াশোনার অফুরন্ত সুযোগ। Admission with Scholarship Deadline: 25 October,2016 বিস্তারিত জানতে রেজিস্ট্রেশন করুনঃ<a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fnextadmission.com%2Fform&amp;h=gAQFts3H5AQHH-nRt4207TyY6xhfwQ6m4kxlT7fEIQSOpQQ&amp;enc=AZMDzqU6zqGiY9n7tabVzWJO7XqrAs5JoEha9UiOjWYOM8dkdVv8MCGcrB3ZTsxmVeiyudMIFRMINzu0Aie7CZCtICii8GvCvtby5gfHy5QFZhpQEkaUTlSh_TKdC_wElsAfRjm-8O3G2Q9NppTcJFUpy_8FyzOfkS_mqUvhLxRdWosv3V-Wz_82WP9PKC5t12RkenMZdBtmsseJGlDWIEnG&amp;s=1">http://nextadmission.com/form</a> , ক্যাম্পাসটি দেখতে ক্লিকঃ<a href="https://www.facebook.com/l.php?u=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DFZNtpHiGFcY&amp;h=VAQHGLrdHAQFLXYltSdbjnx9Uu8wGLwwgreEhCSk_5v_EgA&amp;enc=AZP1Yn-1I0ml4YzE4ZQGLSJs6SwG4CG-NcisiLlC7M8rG3051EhynbXk9Pd_pGXYSzabfPZCqZsiycTfobY3X5jRTajnZeT482LA36iYUr_hjZohN2zolw1Hnggi3H3cK8BHe_wHK4XrmhsMN-_R7BPqUEhzRGYCmEnbrvYesxaPXwLJSVxPXgmU5efBsxDqqZDgpzBe_BCsiLSW4hN8PO3l&amp;s=1">https://www.youtube.com/watch?v=FZNtpHiGFcY</a> মোবাইলঃ 01911342308, 01715015972</p>

								<h4>#কোর্সগুলো নিম্নরুপঃ</h4>
								বাংলাদেশি শিক্ষার্থীরা মেধাবী কিন্তু ইংরেজি ও কিছু Basic ধারণা কম থাকায় বিশ্ববিদ্যালয় কর্তৃপক্ষ বিনা মূল্যে 4 month special কোর্সের ব্যবস্থা করেছে। যাতে থাকবেঃ 1. International English Communication 2. Foundation of the main program 3 Local Language and Culture. 4. Computer
								**
								• Diploma পরীক্ষায় উত্তীর্ণ শিক্ষার্থীরা one year lateral Entry তে (BSC/B-tech) ৩ বছরে ব্যাচেলর ডিগ্রী করতে পারবে।
								• Bachelor Technology (B-Tech Engineering) total credit : 197
								<h4>#Special Scholarship package:</h4>
								<p style="text-align: justify;">স্কলারশিপ সহ বছরে Tuition Fees : ( Admission fees : 40,000 to 50000 INR ( one time), NO exam fees
								a) B-tech/BSc Engineering (Civil, Mechanical, EC, CSE,IT, Electronics and Communication : ----
								-Per semester: 30,000 INR, ( per year 2 semester)
								# BSc in Physiotherapy ( 4 years) ---per semester tuition fees : 30,000 INR
								# BSc in PARAMEDICAL: (, Medical Laboratory, OT Technology, Optometry , Radiograph, Dialysis ,(3 years) ,
								Per semester: 30,000 INR, ( per year 2 semester)
								# BSc in ALLIED HEALTH (Food, Nutrition and Dietetics, Biotechnology, Microbiology,</p>
								<p style="text-align: justify;"># Bsc in Pharmacy (4 years)- Per semester fees : 50,000 INR</p>
								<p style="text-align: justify;">6) Hotel Management and Catering – 3 years ----------per semester : 30,000 INR.
								7) BBA : 3 years :---25,000 INR</p>
								<p style="text-align: justify;">To convert Bangladesh Taka, please count the rate : 1 INR = 1.20 taka</p>
								<p style="text-align: justify;">থাকা-খাওয়াঃ মাসে ৪০০০ – ৫০০০ রুপি পর্যন্ত</p>

								<h4 style="text-align: justify;">#<a href="https://www.facebook.com/hashtag/%E0%A6%A6%E0%A6%BE%E0%A6%A4%E0%A6%AC%E0%A7%8D%E0%A6%AF?source=feed_text&amp;story_id=1330543253643185">দাতব্য</a> তহবিলে চলে বিধায় এর খরচ কম।</h4>
								<p style="text-align: justify;">১। Dispur, Guwahati is the capital of Assam. রাজধানীতে আধুনিক সুযোগ-সুবিধায় বসবাস করে শ্রেষ্ঠ বিশ্ববিদ্যালয়ে অধ্যয়নের সুযোগ। আসামের রাজধানী এলাকায় বসবাস করে পাহাড়ী প্রাকৃতিক স্বাদ পাওয়া।
								৩। প্রাপ্তিসাধ্য বিশুদ্ধ বা ফরমালিন মুক্ত খাবার ও ভারতের শ্রেষ্ঠ আবহাওয়ার পরিবেশ।
								৪। গুয়াহাটিতে খাদ্য ও বাসস্থান বাংলাদেশ ও ভারতের সকল শহরের তুলনায় সস্তা ও স্বাস্থ্যকর।
								৫। বাংলাদেশের সাথে স্থল সীমান্ত থাকায় যাতায়াত অনেক সহজ। Brahmanbaria, Sylhet,Jamalpur, Rangpur, Feni, Jessore, Mymensingh রয়েছে স্থল সীমান্ত।
								৬। ADTU এর সার্টিফিকেট বাংলাদেশ সহ সারা পৃথিবীতে গ্রহণযোগ্য।
								৭। অবসরে বিনোদনের জন্য নিকটবর্তী শ্রেষ্ঠ পর্যটন স্পট- Shilong, Meghalay, Darjeeling, Nepal, Bhutan বেড়ানোর সুযোগ।
								৮। বিশ্ববিদ্যালয়ের মাধ্যমে শিক্ষা সফরের ব্যবস্থা- China, Singapore, Thailand, Malaysia, Indonesia, Nepal, Bhutan ইত্যাদি দেশে।<a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fnextadmission.com%2Fform&amp;h=lAQHv3R3CAQHkQKhADT9Ffmz9mABvXam9yNTAPzgx_XlPDg&amp;enc=AZNBAcl2lPyrxQPZ9KGXVq_7r8LYA6elQxG93vp6BV84NI7FOKPsv5kjypI8y92LIVyoA6o3Caczsgax9Vy-yVByd3Eyp1fBG0WMthsOvLV4BUQq3X4ECC6HglEMe-olhQ2Q6VKsS-iDl8k-f0xL5qfGbS8pJml3S6iepCyeNwR2WU2P_G8QEhzYDVAPpIUmDJu7SdyxaR_F-vWaAsCBsqFf&amp;s=1">http://nextadmission.com/form</a> ফর্মটি পূরণ না করে তাকলে এটি পূরণ করুন। আমরা আপনাকে ফোন করে বিস্তারিত জানাব:</p>

								<h4>#ভর্তির আবেদন করার জন্য সংগে আনতে হবে বা please send below documents to info@nextadmission.com</h4>
								1. All previous Certificates and Transcripts
								2. Passport copy or birth certificate copy
								3. photo ) Size-2/2 and white background
								4. ৫000 taka for registration. Deposit to : Global Star Ltd, City Bank, Account Number: 1421779619001 or Bikash: 01911311415
								5. Fill the Admission form : <a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fwww.nextadmission.com%2Fadmission&amp;h=HAQGW6dffAQET95AXJbXliqdyKbVy6ZlsAeaY96S0vnA7Yw&amp;enc=AZOxDMdDep95IM2lLim7z0qQkx0eqGR3DO6rFiTpVZVeGbQU_JMBrAUL904Kc-iwkRp1Uzq9vobEW1_PUS2IGQma5TxieuLOx-ZDYNb8UKGIfW33zaOgFSBKdHHT9u77-qDTrm_MvsF62D17fOevuxiW8ZtI3dBLI6rdTtcRJ354CWy8aXs83_qDmte6fsgseY3542qr2KwLKAutf7Dl06iz&amp;s=1">www.nextadmission.com/admission</a>
								উপরে উল্লেখিত পেপারস এবং টাকা দিয়ে ভর্তির আবেদন করলে এক দিনের মধ্যে Pre Admission Letter আসবে।
								<h4>#সরাসরি ঢাকা অফিস বা চট্রগ্রাম অফিসে নিম্ন ঠিকানায় যোগাযোগ করুন।</h4>
								<a href="http://l.facebook.com/l.php?u=http%3A%2F%2FNEXTADMISSION.COM%2F&amp;h=lAQHv3R3CAQFZKFR8gGkG4b1CQYs7DiY6j9XY6S41auUUSA&amp;enc=AZOlRfSjerj_aAs4_Qy0j60WD0LvWi1frQQNkGIZj5R1W1_l3LyyoAfpAg1QDrC-q7Jfy292Ed1xpYLAW7b2Fbd7jAajMImZhaLBeVl_-RJovPLgijjs0NwJv59JcDDrD663GfbiEnAZTlRDrH0IZ_kk0rgUpPIIDwKLjXs45Lb64KenniN4gSqANIVSD5I9d4Hl9jORmeLfegFQ0s_efR28&amp;s=1">NEXTADMISSION.COM</a>
								Global Star Ltd.(GSL
								<strong>ঢাকা অফিস ( Dhaka office)</strong>
								31 Malek tower ( 9th floor- 2nd lift ) Farmgate, Dhaka, Bangladesh Mobile: +880 1911 342308,
								+88001715015972 ,
								Email: info@nextadmission.com
								<strong>চট্রগ্রাম অফিস (Chittagong Office)</strong>
								Walikhan mansion, 603 sk mujib road (3r d floor), Agrabad, choumuhuni moar, chittagong
								Mobile: +880 1911 342308, +88001715015972 , Email: smtbangladesh@yahoo.com
							</div>
						</div>
						<!--End sub col 9 or bloa_area-->

						<!--Start sub col 3 or left_sidebar-->
						<div class="col-lg-3 left_sidebar_ads">
							<?php $this->load->view('leftSidebarPageNew'); ?>
						</div>
						<!--End sub col 3 or left_sidebar-->
					</div> 
					<!--End main col 9 --> 
					
					<!--Start main col 3 or Start Right Sidebar -->
					<div class="col-lg-3 right_sidebar_ads">     
				   		<?php $this->load->view('rightSidebarPageNew'); ?>
					</div>
					<!--End main col 3 or right sidebar -->
				</div>
				<!--End Main row second-->
			</div>
		</section>
		<!-- /Left Sidebar -->
		<div class="clearfix"></div>
	</div>
	<!-- End Main wrapper -->
	<footer class="footer_area">
		<?php $this->load->view('footerPage'); ?>
	</footer>



		<div class="modal fade" id="breaking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content" id="breakingNewidDeatial">
			 
			</div>
		  </div>
		</div>		




<script>

$(function () {
   $('.tlt').textillate({
  // the default selector to use when detecting multiple texts to animate
  selector: '.texts',

  // enable looping
  loop: true,

  // sets the minimum display time for each text before it is replaced
  minDisplayTime: 6000,

  // sets the initial delay before starting the animation
  // (note that depending on the in effect you may need to manually apply
  // visibility: hidden to the element before running this plugin)
  initialDelay: 0,

  // set whether or not to automatically start animating
  autoStart: true,

  // custom set of 'in' effects. This effects whether or not the
  // character is shown/hidden before or after an animation
  inEffects: [],

  // custom set of 'out' effects
  outEffects: [ 'hinge' ],

  // in animation settings
  in: {
    // set the effect name
    effect: 'fadeInLeftBig',

    // set the delay factor applied to each consecutive character
    delayScale: .1,

    // set the delay between each character
    delay: 10,

    // set to true to animate all the characters at the same time
    sync: false,

    // randomize the character sequence
    // (note that shuffle doesn't make sense with sync = true)
    shuffle: false,

    // reverse the character sequence
    // (note that reverse doesn't make sense with sync = true)
    reverse: false,

    // callback that executes once the animation has finished
    callback: function () {}
  },

  // out animation settings.
  out: {
    effect: 'hinge',
    delayScale: .1,
    delay: 10,
    sync: false,
    shuffle: false,
    reverse: false,
    callback: function () {}
  },

  // callback that executes once textillate has finished
  callback: function () {},

  // set the type of token to animate (available types: 'char' and 'word')
  type: 'word'
});

})

$(function(){
    var $mwo = $('.LetestNews');
    //$('.marquee').marquee();
    $('.LetestNews').marquee({
        //speed in milliseconds of the marquee
        speed: 4000,
        //gap in pixels between the tickers
        gap: 1,
        //gap in pixels between the tickers
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
        pauseOnHover: true
    });
    //Direction upward
    
    //pause and resume links
    $('.pause').click(function(e){
        e.preventDefault();
        $mwo.trigger('pause');
    });
    $('.resume').click(function(e){
        e.preventDefault();
        $mwo.trigger('resume');
    });
    //toggle
    $('.toggle').hover(function(e){
        $mwo.trigger('pause');
    },function(){
        $mwo.trigger('resume');
    })
    .click(function(e){
        e.preventDefault();
    })
});


	$(document).on("click", ".updatePost", function(e)
	{
	   var id 		= $(this).attr("data-id");
	   var formURL  = "<?php echo site_url('commentPopup/postedit'); ?>";
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : {id: id},
			dataType: "html",
			success:function(data){
				$("#blogDetails .modal-content").html(data);
				$("#modal-form").modal({
					keyboard: false,
					backdrop: 'static',
				});

				 initiateFileUpload();
			}
		});
	});
	    

	   $("#searchForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#searchOrganization").html(data);				
				}
			});
			
			e.preventDefault();
		});
		
		//North America Effict
		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
        $(document).on("change", "#country_id_chnge", function(){
			var country_id = $(this).val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
        });


	   //ORGANIZATION WISE PROGRAM 

	     $("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	



        // On change search all
	     $(document).on("change", "#country_id_chnge", function(){
			var country_id = $(this).val();	
			console.log(country_id);

			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});		
        });

	      $("#organize_id").change(function() {
			var country_id  = $("#country_id_chnge").val();	
			var organize_id = $("#organize_id").val();	
				
			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id, organize_id : organize_id},
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});			
		});	


	     $("#program_id").change(function() {
			var country_id  = $("#country_id_chnge").val();	
			var organize_id = $("#organize_id").val();	
			var program_id  = $("#program_id").val();	

			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id, organize_id : organize_id, program_id : program_id},
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});			
		});	

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.readmore').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
		
		//Breaking news
		
		$(".latesnewId").on('click', function(){
			var id = $(this).attr("data-id");
			var laturl = "<?php echo site_url("home/latesnewIdd"); ?>";
			
			$.ajax(
			{
				url : laturl,
				type: "POST",
				data:{id:id},
				success:function(data){
				$('#breaking').modal('show');
				$("#breakingNewidDeatial").html(data);
				}
			});
			
		});
	</script>
  </body>
</html>