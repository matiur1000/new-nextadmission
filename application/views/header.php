<DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $headerBasicInfo->title; ?></title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
<!-- Bootstrap -->
<link href="<?php echo base_url('resource/nextadmission/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('resource/nextadmission/css/font-awesome.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('resource/nextadmission/css/custom.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('resource/nextadmission/css/responsive.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/jquery.fittext.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/jquery.lettering.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url('resource/js/jquery.textillate.js'); ?>"></script>
<script src="<?php echo base_url("resource/js/ajaxupload.3.5.js"); ?>"></script>
<style> 

</style>
</head>
<body>
<div class="top_header_wrapper_section">
  <!--Top_Header_Section-->
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="top_header_inner_section">
            <div class="col-md-8 col-sm-7 col-xs-12 col-lg-8">
              <div class="top_header_section">
                <ul>
                  <li class="myactive"><a href="#">University</a></li>
                  <li><a class="mybdjobs" href="#">Forum</a></li>
                  <li><a class="mytraining" href="#">Photo Gallery</a></li>
                  <li><a class="myemployee" href="#">Organizations</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4 col-sm-5 hidden-xs col-lg-4">
              <div class="top_header_right_section">
                <div class="top_header_right_left_widget">
                  <ul>
                    <li id="phone"><a href="<?php echo site_url('home/contactUS'); ?>"  target="_blank"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                    <li id="youtube"><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <li id="google_plus"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li id="facebook_icon"><a href="https://www.facebook.com/nextadmissionbd/" target="_blank"><i class="fa fa-facebook"  aria-hidden="true"></i></a></li>
                  </ul>
                </div>
                <div class="top_header_right_right_widget">
                  <div class="top_header_right_right_inner_widget">
                    <div class="top_header_right_right_inner_left_widget">
                      <p><a href="#">Eng</a></p>
                    </div>
                    <div class="top_header_right_right_inner_right_widget">
                      <p><a href="#">?????</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--End_Top_Header_Section-->
<div class="menu_wrapper_section">
  <!--Menu_Wrapper_Section-->
  <div class="container">
    <div class="row">
      <div class="menu_inner_section">
        <div class="col-md-4 col-sm-3 hidden-xs col-lg-4">
          <div class="menu_image_section"> <a href="<?php echo site_url("home"); ?>"> <img src="<?php echo base_url('resource/nextadmission/images/logo.png'); ?>" width="176" height="70"/> </a> </div>
        </div>
        <div class="col-md-8 col-sm-9  col-xs-12 col-lg-8">
          <div class="menu_right_section">
            <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
				  <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Study <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Study in India</a></li>
                      <li><a href="#">Study in Malaysia</a></li>
                      <li><a href="#">Study in China</a></li>
                      <li><a href="#">Study in Poland </a></li>
                      <li><a href="#">Study in Canada</a></li>
                      <li><a href="#">Study in USA</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Career Resources <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Career Guide</a></li>
                      <li><a href="#">Interview Tips</a></li>
                      <li><a href="#">Resume Writing Tips</a></li>
                      <li><a href="#">Cover Letter </a></li>
                      <li><a href="#">Articles</a></li>
                      <li><a href="#">Career Counseling</a></li>
                    </ul>
                  </li>
                  <li class="li_design"> <a href="#" id="showmenu">Sign In </a>
                    <div class="menu" style="display: none; position: relative;top:5px;">
                      <div class="tab_wrapper">
                        <div class="tab_inner_wrapper">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#account" aria-controls="home" role="tab" data-toggle="tab">Organization</a></li>
                            <li role="presentation"><a href="#employee" aria-controls="profile" role="tab" data-toggle="tab">Others</a></li>
                          </ul>
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="account">
                              <div class="tab_content_inner_section">
							  
							

	<?php /*?><form id="logInForm"  action="<?php echo site_url('loginForm/loginAuthenticate'); ?>" method="post" class="form-horizontal">

	  <div class="col-md-12">

	  	<span class="logInFail"></span>

	  </div>

	    

	  <div class="form-group">

		<div class="col-sm-12">

		

				<input type="radio" name="type" value="organization" style="margin-top:2px" /> Organization

			

		

				<input type="radio"  name="type" value="other" style="margin-top:2px" />  Others
						

		</div>

	  </div>

	  <div class="form-group">

	  	<div class="col-sm-12">

			<!-- <label for="email" class="control-label">UserId</label>	 -->	

			<input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1">		

		</div>

	  </div>

	  <div class="form-group">

	  	<div class="col-sm-12">

			<!-- <label for="password" class="control-label">Password</label>		 -->

			<input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2">

		</div>

	  </div>

	  

	  <div class="form-group">

		<div class="col-sm-12">

		  <button type="submit" class="btn btn-primary">Sign in</button> &nbsp; &nbsp; <a href="<?php echo site_url('registration/genForgotPaword'); ?>"> Forgot Password?</a></label>

		</div>

	  </div>

	</form><?php */?>


							  
                              <form id="logInForm"  action="<?php echo site_url('loginForm/loginAuthenticate'); ?>" method="post" class="form-horizontal">
							  
							  <div class="col-md-12">

	  	<span class="logInFail"></span>

	  </div>
							  
							   <div class="form-group">

		<div class="col-sm-12">

		

				<input type="radio" name="type" value="organization" style="margin-top:2px"  id="organization"  checked="checked" /> 
				
				

			

		

			
						

		</div>

	  </div>
                                  
								  
								  
								  
                                  <div class="sign_in_user_name"> <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1"/>
                                  </div>
                                  <div class="sign_in_user_name"> <i class="fa fa-lock" aria-hidden="true"></i>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2"/>
                                  </div>
                                  <div class="sign_in_user_name">
                                    <button type="submit" class="btn btn-success" name="submit" > Sign In</button>
                                  </div>
                                  <div class="sign_in_user_name">
                                    <div class="sign_in_account_left">
                                      <p>Don't have an account?</p>
                                    </div>
                                    <div class="create_button_right">
                                      <p><a href="#">Create An Account</a></p>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="employee">
                              <div class="tab_content_inner_section">
                                <form id="logInForm"  action="<?php echo site_url('loginForm/loginAuthenticate'); ?>" method="post" class="form-horizontal">
							  
							  <div class="col-md-12">

	  	<span class="logInFail"></span>

	  </div>
							  
							   <div class="form-group">

		<div class="col-sm-12">

		

				<input checked="checked" type="radio" id="others" name="type" value="other" style="margin-top:2px" />
				
				

			

		

			
						

		</div>

	  </div>
                                  
								  
								  
								  
                                  <div class="sign_in_user_name"> <i class="fa fa-user" aria-hidden="true"></i>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1"/>
                                  </div>
                                  <div class="sign_in_user_name"> <i class="fa fa-lock" aria-hidden="true"></i>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2"/>
                                  </div>
                                  <div class="sign_in_user_name">
                                    <button type="submit" class="btn btn-success" name="submit" > Sign In</button>
                                  </div>
                                  <div class="sign_in_user_name">
                                    <div class="sign_in_account_left">
                                      <p>Don't have an account?</p>
                                    </div>
                                    <div class="create_button_right">
                                      <p><a href="#">Create An Account</a></p>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li><a href="#">Create Account</a></li>
                  <li><a href="<?php echo site_url('home/contactUS'); ?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i> &nbsp;Contact Us</a></li>
                </ul>
              </div>
              <!-- /.navbar-collapse -->
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--End_Menu_Wrapper_Section-->




<script>

  	$(document).on("submit", "#logInForm", function(e){
	
			
			alert('manik');

  		

		var postData = $(this).serializeArray();

		var formURL  = $(this).attr("action");

		

		var type 	= $(this).find("input[name='type']:checked").val();

		

		if(type =='organization'){

		   var successUrl	= "<?php echo site_url('organizationUserHome'); ?>";

		}else{

		  var successUrl	= "<?php echo site_url('generalUserHome'); ?>";			

		}

	

		$.ajax({

			url : formURL,

			type: "POST",

			data : postData,

			success:function(data){

				if(data==1){

					$("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='hidden'], #logInForm input[type='password']").val("");

					$(".logInFail").text("Wrong email or password. Please try again!");

					$(".logInFail").css("color", "red");

					$("#msg").css("display", "block"); 

				} else if(data==2){

					 $(".logInFail").text("Please Select a type Organization or Other");

					 $(".logInFail").css("color", "red"); 

					 $("#msg").css("display", "block"); 					

				}else{

				 location.replace(successUrl);

				}

			}					

		});

	

	   	e.preventDefault();

	});

	

	$(".close").click(function(){

	   $("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='password']").val("");

	   $("#logInForm input[type='radio']").prop("checked",false);

	});



	

  

  $(window).ready(function(){

	  $('[data-toggle="popover"]').popover({

		    content: function(){

			  var target = $(this).attr('data-target-content');

			   return $(target).html();

			  },

		    trigger: 'manual'

		}).click(function (e) {

			$( this ).popover( "toggle" ); 

			e.stopPropagation();

			  	

		});



		$('[data-toggle="popover"]').on('click', function (e) {

		    $('[data-toggle="popover"]').not(this).popover('hide');

		});



	  $('body').on('click', function (e) {

	    $('[data-toggle="popover"]').each(function () {

	        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {

	            $(this).popover('hide');

	        }

	    });

	  });



   });





	  $(document).on("change", "input[name='regType']", function(){

		   var typeValue = $(this).val();

		   var parents = $(this).parents('.popover-content');



		   if(typeValue =='organization'){

		    	parents.find("#organize_reg").css("display", "block");

		    	parents.find("#other_reg").css("display", "none");		    	

		   }else{

		     	parents.find("#organize_reg").css("display", "none");

		    	parents.find("#other_reg").css("display", "block");

		   }

		   

		  	

		});





	  $(document).on("change", "#country_id", function() {

		var country_id = $(this).val();

		parents = $(this).parents('.popover-content');	

	

		$.ajax({

			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN

			type : "POST",

			data : { id : country_id },

			dataType : "html",

			success : function(data) {			

			   parents.find("#city_id").html(data);

			}

		});

		

	});

	

	

	$(document).on("change", "#country_id_org", function() {

		var country_id = $(this).val();

		parents = $(this).parents('.popover-content');

		console.log(country_id);	

	

		$.ajax({

			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN

			type : "POST",

			data : { id : country_id },

			dataType : "html",

			success : function(data) {			

				parents.find("#city_id_org").html(data);

			}

		});

		

	});







	// Password Count	

   

   $(document).on("keyup", ".password", function() {

	 var len = $(this).val().length;

	 parents = $(this).parents('.popover-content');

	    

		if(len<=1){

		parents.find(".first").text("");

		parents.find(".first").removeClass("red");

		parents.find(".first").removeClass("yellow");

		parents.find(".first").removeClass("green");

		

	  } else if(len<=4){

	  	parents.find(".first").text("Very Weak");

		parents.find(".first").addClass("red");

		parents.find(".first").removeClass("yellow");

		parents.find(".first").removeClass("green");

	   } else if(len<=8){



	   	parents.find(".first").text("Good");

		parents.find(".first").addClass("green");

		parents.find(".first").removeClass("yellow");

		parents.find(".first").removeClass("red");



	   } else if(len<=9){



	   	parents.find(".first").text("Good");

		parents.find(".first").addClass("green");

		parents.find(".first").removeClass("yellow");

		parents.find(".first").removeClass("red");

	   }

	});

	

	

  $(document).on("keyup", ".conformpassword", function() {

  	 parents 		= $(this).parents('.popover-content');

	 var conpass 	= $(this).val();

	 //var Pass = $(".password").val();

	 var Pass 		=  parents.find(".password").val();



	 if(conpass){



		  if(conpass != Pass){

		  	parents.find(".second").text("Your New Password and Confirm Password donot match!");

			parents.find(".second").addClass("red");

			parents.find(".second").removeClass("green");

		  

		  } else {

		  	parents.find(".second").text("Password Match");

			parents.find(".second").removeClass("red");

			parents.find(".second").addClass("green");

		   	

		  }



		} else {

		   parents.find(".second").text("Password Match");

		   parents.find(".second").removeClass("red");

		   parents.find(".second").removeClass("green");

		  

		}

	});

	

	

	

	// Password Count





	$(document).on("keyup", ".password3", function() {

	 var len = $(this).val().length;

	 parents = $(this).parents('.popover-content');

	    

		if(len<=1){

		parents.find(".third").text("");

		parents.find(".third").removeClass("red");

		parents.find(".third").removeClass("yellow");

		parents.find(".third").removeClass("green");

		

	  } else if(len<=4){

	  	parents.find(".third").text("Very Weak");

		parents.find(".third").addClass("red");

		parents.find(".third").removeClass("yellow");

		parents.find(".third").removeClass("green");

	   } else if(len<=8){



	   	parents.find(".third").text("Good");

		parents.find(".third").addClass("green");

		parents.find(".third").removeClass("yellow");

		parents.find(".third").removeClass("red");



	   } else if(len<=9){



	   	parents.find(".third").text("Good");

		parents.find(".third").addClass("green");

		parents.find(".third").removeClass("yellow");

		parents.find(".third").removeClass("red");

	   }

	});

	

	

  $(document).on("keyup", ".conformpassword3", function() {

  	 parents 		= $(this).parents('.popover-content');

	 var conpass 	= $(this).val();

	 //var Pass = $(".password").val();

	 var Pass 		=  parents.find(".password3").val();



	 if(conpass){



		  if(conpass != Pass){

		  	parents.find(".fourth").text("Your New Password and Confirm Password donot match!");

			parents.find(".fourth").addClass("red");

			parents.find(".fourth").removeClass("green");

		  

		  } else {

		  	parents.find(".fourth").text("Password Match");

			parents.find(".fourth").removeClass("red");

			parents.find(".fourth").addClass("green");

		   	

		  }



		} else {

		   parents.find(".fourth").text("Password Match");

		   parents.find(".fourth").removeClass("red");

		   parents.find(".fourth").removeClass("green");

		  

		}

	});

	





	

   



	$(document).on("blur", "#user_id", function() {

		parents 	= $(this).parents('.popover-content');

		var userId 	= $(this).val();

		  $.ajax({

			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN

			type : "POST",

			data : { userId : userId },

			dataType : "html",

			success : function(data) {			

				if(data == 1){

				parents.find(".chk").text("This Email Already Exit!");

				parents.find(".chk").addClass("red");

				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");

				} else {

				parents.find(".chk").text("");

				parents.find(".chk").removeClass("red");

				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");

	   			 

			  }

		    }

		 });

			

	});







	$(document).on("blur", "#user_id2", function() {

		parents 		= $(this).parents('.popover-content');

		var user_id 	= $(this).val();

		  $.ajax({

			url : SAWEB.getSiteAction('registration/userEmailChk'), // URL TO LOAD BEHIND THE SCREEN

			type : "POST",

			data : { user_id : user_id },

			dataType : "html",

			success : function(data) {			

				if(data == 1){

				parents.find(".chkEmail").text("This Email Already Exit!");

				parents.find(".chkEmail").addClass("red");

				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");

				} else {

				parents.find(".chkEmail").text("");

				parents.find(".chkEmail").addClass("red");

				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");

	   			 

			  }

		    }

		 });

			

	});



	$(document).ready(function() {

	   setTimeout(getAllUnreadMsg, 2000);

	});

	



	// Get All unread msg

    

	 function getAllUnreadMsg() {

		  $.ajax({

	        url : SAWEB.getSiteAction('liveChat/getAllUnreadMsg'),

			type: "POST",

			data : {},

			dataType: "html",

	        success: function (data) { 

	        	$("#newMsgNum").html(data);



	          }

	       });

		   setTimeout(getAllUnreadMsg, 2000);

	}

	

</script>
<style>
#organization{
	display:none;
	}
#others{
	display:none;
	}
</style>
