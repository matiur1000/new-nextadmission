<?php
/**
 * Slideshow Demo
 *
 * Creates a slideshow of 5 images which are resampled from 1200x800 to 600x400
 * with an overlay added to the final frame.
 */

// Include the class
require_once('GifCreator.php');

// Instanciate the class (uses default options with the addition of width/height specified)
if($gifData->image_position =='Top'){
  $gif = new GifCreator(0, 2, array(-1, -1, -1), 255, 90);
} else if($gifData->image_position =='Left'){
  $gif = new GifCreator(0, 2, array(-1, -1, -1), 185, 120);
} else if($gifData->image_position =='Right'){
  $gif = new GifCreator(0, 2, array(-1, -1, -1), 125, 90);
} else {
  $gif = new GifCreator(0, 2, array(-1, -1, -1), 258, 50);
}

// Add each frame to the animation
if(!empty($gifData->image1)) $gif->addFrame(file_get_contents("resource/gif_image/$gifData->image1"), 200, true);
if(!empty($gifData->image2)) $gif->addFrame(file_get_contents("resource/gif_image/$gifData->image2"), 200, true);
if(!empty($gifData->image3)) $gif->addFrame(file_get_contents("resource/gif_image/$gifData->image3"), 200, true);
if(!empty($gifData->image4)) $gif->addFrame(file_get_contents("resource/gif_image/$gifData->image4"), 200, true);

header('Content-type: image/gif');
echo $gif->getAnimation();