  
 <?php $this->load->view('header'); ?>
  
    <div class="slider_wrapper_section" style="background-image:url(resource/nextadmission/images/aerztin.jpg)"><!--slider_Wrapper_Section-->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-9 ">
                            <div class="slider_header_section">
                                <div class="find_job_title">
                                    <h2>Find the right Subject</h2>
                                </div>
                                <div class="searching_section">
                                    <div class="searching_inner_section">
									
									    <div class="searching_search_middle_section">
                                                <div class="inner-addon2 keyword-search">
                                                      <i class="fa fa-briefcase" aria-hidden="true"></i>
                                                      <select class="form-control" id="country_id_chnge" name="country_id"  tabindex="1">
                                                            <div class="form-control" placeholder="Organized Type" aria-describedby="basic-addon1"></div>
                                    <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Country</option>

                                    <?php foreach ($countryInfo as $v){ ?>
									<option value="<?php echo $v->id; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $v->country_name; ?></option>
									<?php } ?>
                                                           
                                                      </select>
                                                </div>
                                            </div>
                                        <form id="searchForm" action="<?php echo site_url('home/searchAll'); ?>" method="post" enctype="multipart/form-data">
                                            <div class="searching_search_left_section">
                                                <div class="inner-addon keyword-search">
                                                    <i class="fa fa-tag" aria-hidden="true"></i>
                <input type="text" name="search_all" id="search_all" class="form-control" placeholder="Search by keyword" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
											
											 <div class="submit_section">
                                                <input type="submit" class="btn btn-success" value="Search" >
                                            </div>
                                        </form>

                                        
                                           
                                    </div>
                                </div>

                                <div class="live_job_section">

                                     <div class="live_job_left_section">
                                        <div class="live_job_wrapper_section">
                                            <div class="live_job_icon_wrapper">
                                                <div class="live_job_icon_section">
                                                    <div class="live_job_icon_inner_section">
                                                      <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="live_job_text_wrapper_section">
                                                <div class="live_job_top_wrapper">
                                                    <h6>Success Visa</h6>
                                                </div>
                                                <div class="live_job_bottom_text_section">
                                                    <h2>500</h2>
                                                </div>
                                            </div>
                                        </div>
                                      </div>

                                      <div class="live_job_left_section">
                                          <div class="live_job_wrapper_section">
                                              <div class="live_job_icon_wrapper">
                                                  <div class="live_job_icon_section">
                                                      <div class="live_job_icon_inner_section">
                                                          <i class="fa fa-building-o" aria-hidden="true"></i>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="live_job_text_wrapper_section">
                                                  <div class="live_job_top_wrapper">
                                                      <h6>Countries</h6>
                                                  </div>
                                                  <div class="live_job_bottom_text_section">
                                                      <h2>15</h2>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="live_job_left_section">
                                          <div class="live_job_wrapper_section">
                                              <div class="live_job_icon_wrapper">
                                                  <div class="live_job_icon_section">
                                                      <div class="live_job_icon_inner_section">
                                                        <i class="fa fa-mouse-pointer" aria-hidden="true"></i>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="live_job_text_wrapper_section">
                                                  <div class="live_job_top_wrapper">
                                                      <h6>Subjects</h6>
                                                  </div>
                                                  <div class="live_job_bottom_text_section">
                                                      <h2>50</h2>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="divisional_job_section">
                                <div class="slider_sidebar_widget">
                                   <h4>countrywise universites</h4>
                                  <div class="all_division_widget">
                                      <a href="#" class="link_design">India (400)</a>
                                      <a href="#" class="link_design">China (100)</a>
                                      <a href="#" class="link_design">Malaysia (107)</a>
                                      <a href="#" class="link_design">UK (58)</a>
                                      <a href="#" class="link_design">USA (30)</a>
                                      <a href="#" class="link_design">Poland (142)</a>
                                      <a href="#" class="link_design">Germany (52)</a>
                                      <a href="#" class="link_design">Canada (20)</a>
                                  </div>
                                  <h4>Quick Links</h4>
                                  <div class="quick_link_widget">
                                      <ul>
                                          <li><a href="http://www.dip.gov.bd/site/page/f2d015a9-1132-4426-8eef-147f1c4bac8a" class="quick_link" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Apply online for MRP Passport</a></li>
										   <li><a href="http://www.ivacbd.com/"  target="_blank" class="quick_link"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Indian Visa Application Centre (IVAC)</a></li>
                                          <li><a href="http://bris.lgd.gov.bd/pub/?pg=application_form"  target="_blank" class="quick_link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> 
Online Birth Registration</a></li>
                                          <li><a href="http://www.ugc.ac.in/" class="quick_link" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  UGC, New Delhi, India</a></li>
										    <li><a href="http://www.bmet.gov.bd/BMET/onlinaVisaCheckAction" target="_blank"  class="quick_link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Verify Visa</a></li>
                                          
                                         

                                      </ul>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--End_Slider_Wrapper_Section-->
     
    <div class="browse_category_section"><!--Browse_Category_Section-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="browse_category_wrapper_left_section">
                                <div class="browse_category_top_header_widget">
                                    <div class="browse_category_top_left_section">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                        <span>&nbsp;Browse Subject</span>
                                    </div>
									
                                    <div class="browse_category_main_bottom_section">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home">
                                               <div class="row">
                                                  <div class="col-md-4 padding_style">
                                                      <div class="tab_pane_left_section">
                                                          <ul>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Engineering and Technology<span> (40)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>  Management <span>(10)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>  Pharmacy  <span>(10)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> 	Nursing Sciences and Research  <span>(10)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> 	 Allied Health <span>(10)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Paramedical Courses  <span>(08)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Hospitality Courses   <span>(05)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>		Ph.D Courses   <span>(30)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>  All Distance Education Courses  <span>(06)</span></a></li>
                                                          </ul>
                                                      </div>
                                                  </div>

                                                  <div class="col-md-4 padding_style">
                                                      <div class="tab_pane_left_section">
                                                          <ul>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>  All Skill Development Courses <span>(15)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Research Programme <span>(50)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> 		 Short Term Courses <span>(5)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> 		 Basic Sciences and Research <span>(15)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Architecture & Planning <span>(05)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Art, Design and Media Studies  <span>(20)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>  Law <span>(05)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Languages & Culture  <span>(50)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>  Dental Sciences  <span>(73)</span></a></li>
                                                          </ul>
                                                      </div>
                                                  </div>

                                                  <div class="col-md-4 padding_style">
                                                      <div class="tab_pane_left_section">
                                                          <ul>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Medical Sciences & Research <span>(30)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Folklore and Ethnomusicology <span>(170)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> European Studies <span>(25)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Applied Sport Science <span>(76)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Athletic Training  <span>(122)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Atmospheric Science  <span>(39)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Audiology  <span>(125)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Biblical and Literary Criticism <span>(22)</span></a></li>
                                                              <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Others <span>(14)</span></a></li>
                                                          </ul>
                                                      </div>
                                                  </div>

                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="govt_job_wrapper_section">
                              <h4><i class="fa fa-globe" aria-hidden="true"></i> Scholarship</h4>
                              <div class="govt_job_caurousal">
                                  <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                          <div class="item active" id="govt_job_list">
                                              <h2 class="carousal_header">Sharda University</h2>
                                              <span class="carousal_header">Uttar Pradesh, India</span>
                                              <h2 class="carousal_header">Vit University</h2>
                                              <span class="carousal_header">Chennai, India</span>
                                         
                                          </div>
                                          <div class="item" id="govt_job_list">
                                              <h2 class="carousal_header">Adamas University</h2>
                                              <span class="carousal_header">India</span>
                                              <h2 class="carousal_header">MMU University</h2>
                                              <span class="carousal_header">Haryana, India</span>
                                           
                                          </div>
                                         
                                        </div>
                                        <a href="#">View All <span>200</span></a>

                                        <!-- Controls -->
                                        <div class="caurosal_arrow_right">
                                            <a href="#carousel-example-generic1" role="button" data-slide="prev">
                                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                              <span class="sr-only">Previous</span>
                                            </a>
                                            <a href="#carousel-example-generic1" role="button" data-slide="next">
                                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                              <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                      </div>
                              </div>
                            </div>
                            <div class="special_job_wrapper_section">
                              <h4><i class="fa fa-briefcase" aria-hidden="true"></i> Tuition Waiver</h4>
                              <div class="govt_job_caurousal">
                                  <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                          <div class="item active" id="govt_job_list">
                                              <h2 class="carousal_header">Jain University</h2>
                                              <span class="carousal_header">Bengaluru, India</span>
                                              <h2 class="carousal_header">Christ Universitys</h2>
                                              <span class="carousal_header">Bengaluru, India</span>
                                         
                                          </div>
                                          <div class="item" id="govt_job_list">
                                              <h2 class="carousal_header">Presidency University</h2>
                                              <span class="carousal_header">Bengaluru, India</span>
                                              <h2 class="carousal_header">Adamas University</h2>
                                              <span class="carousal_header">India</span>
                                           
                                          </div>
                                         
                                        </div>
                                        <a href="#">View All <span>200</span></a>

                                        <!-- Controls -->
                                        <div class="caurosal_arrow_right">
                                            <a href="#carousel-example-generic2" role="button" data-slide="prev">
                                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                              <span class="sr-only">Previous</span>
                                            </a>
                                            <a href="#carousel-example-generic2" role="button" data-slide="next">
                                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                              <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--End_Browse_Category_Section-->

    <div class="hot_job_section" ><!--Hot_Job_Section-->
	
	 <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="hot_job_wrapper_section">
                                <h2><i class="glyphicon glyphicon-fire" aria-hidden="true"></i> Hot <span>Universities</span></h2>
                                <div class="all_jobs_section" id="searchOrganization">
								
								  
   
               <?php 

			     if(!empty($searchAdResult)){
				 
                  $a = 0;

				  $b = 3;	

			      foreach($searchAdResult as $v){

			     	if($v->user_type =="General user"){

			     	  $mobile  = $v->mobile;

		              $noLink = "#";

			     	}else{

		              $mobile  = $v->mobile_com;

		              $userProfileLink = array('home','adWiseCompanyDetail', $v->res_user_id);

			     	}



			     	if($a == 0) echo "<div class='row hj-row'>";

					if($a <= $b)  

			  ?>

		        <?php if ($v->user_type =="General user"){ ?>

					<div class="col-md-4 col-sm-4 col-xs-12 border_right pr">
					
  					  <div class="hot_job_company">
      					<div class="company_logo"> 
						<?php if(!empty($v->image)){ ?>
						<img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="" width="48" height="48"  />
						 <?php }else{ ?>
						 <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="" width="48" height="48" />
						   <?php } ?>
						</div>
   					    <div class="company_details">
						<h5><?php echo $v->name; ?></h5>
						
						<ul>
						  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $v->user_type; ?></a></li>
						  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $v->country_name; ?></a></li>
						</ul>
     				 </div>
   					 </div>
	
  					</div>

			    <?php }else{ ?>

            		 <div class="col-md-4 col-sm-4 col-xs-12 border_right pr">
                                            <div class="hot_job_company">
                                                <div class="company_logo">
												
												 <?php if(!empty($v->image)){ ?>
												    <a href="<?php echo site_url($userProfileLink); ?>">	
                                                   
												  <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="" width="48" height="48"  />
												   </a>
												  
											    <?php }else{ ?>
												<a href="<?php echo site_url($userProfileLink); ?>">
												
												 <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="" width="48" height="48" />
												  </a>
												 <?php } ?>
												 


                                                </div>
                                                <div class="company_details">
                                                    <h5><a href="<?php echo site_url($userProfileLink); ?>"><?php echo $v->name; ?></a></h5>
                                                    <ul>
												 <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> 
												   <?php echo $v->user_type; ?></a>
												 </li>
												  <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> 
												  <?php echo $v->country_name; ?></a>
												  
												 </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
			    <?php } ?>

			

			<?php

				 $a++;	

				 if($a == $b) echo "</div>";				

				 if($a == $b) $a=0; } 
				 
				 
				 }	
				 
				 else { ?> 

				   <span style="color:#FF0000">Sorry result not fund!</span>
			
			
			
				  <?php } ?>


                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="hot_job_add_wrapper_section">
                                <div class="single_promo_section">
                                
                                <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/BAT.png'); ?>"  alt="#" /></a>
                                </div>

                                <div class="single_promo_section">
                             
                                <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/260x80_GP-Logo.jpg'); ?>"  alt="#" /></a>

                                </div>

                                <div class="single_promo_section">

                                 <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/Coats.gif'); ?>"  alt="#" /></a>

                                    
                                </div>

                                <div class="single_promo_section">

                                <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/Bdjobs-Citygroup_02.png'); ?>"  alt="#" /></a>
                               
                                </div>

                                <div class="single_promo_section">
                                   

                                    <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/nestle.png'); ?>"  alt="#" /></a>

                                </div>

                                <div class="single_promo_section">

                                <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/bkash.gif'); ?>"  alt="#" /></a>
                                   
                                </div>

                                <div class="single_promo_section">

                                 <a href="#"><img  src="<?php echo base_url('resource/nextadmission/images/company_promo/armyhome.gif'); ?>"  alt="#" /></a>

                                   
                                </div>

                            </div>

                           

                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div><!--End_Hot_Job_Section-->


    <div class="bottom_slider_section"><!--Bottom_Slider_Section-->
        <div class="fluid-container">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active">

                    <img  src="<?php echo base_url('resource/nextadmission/images/slider/international_banner.jpg'); ?>"  alt="#" />

                    <div class="carousel-caption">
                      <h2>Develop Yourself for better career</h2>
                    </div>
                  </div>
                  <div class="item">
                    

                    <img  src="<?php echo base_url('resource/nextadmission/images/slider/1.jpg'); ?>"  alt="#" />

                    <div class="carousel-caption">
                      <h2>Develop Yourself for better career</h2>
                    </div>
                  </div>
                   <div class="item">
                    
                   <img  src="<?php echo base_url('resource/nextadmission/images/slider/2.jpg'); ?>"  alt="#" />

                    <div class="carousel-caption">
                     <h2>Develop Yourself for better career</h2>
                    </div>
                  </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

    </div><!--End_Bottom_Slider_Section-->

    <div class="workshop_training_section"><!--Workshop_Trainer_Section-->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="workshop_training_left_section">
                                <div class="workshop_training_header">
                                    <i class="glyphicon glyphicon-th" aria-hidden="true"></i> Workshop Training
                                </div>
                                <div class="row all_workshop_training">
                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="marketing_bg signal_icon">
                                               <i class="fa fa-signal" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>SPIN Selling Skills</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="commercial_bg signal_icon">
                                               <i class="fa fa-building-o" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>Fundamentals of Supply Chain Management</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="it_bg signal_icon">
                                               <i class="fa fa-laptop" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>MS Excel 2010: From Novice to Professional</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="hr_bg signal_icon">
                                               <i class="fa fa-male" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>HR Planning</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="banking_bg signal_icon">
                                               <i class="fa fa-university" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>Appraisal, Financing & Management of Project</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="banking_bg signal_icon">
                                               <i class="fa fa-upload" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>Export Import, Indenting, C&F Agent, e-Tendering & Buying House Business Functional Activities</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-md-12">
                                        <a href="#">
                                            <div class="banking_bg signal_icon">
                                               <i class="fa fa-users" aria-hidden="true"></i>
                                            </div>
                                            <div class="marketing_bg_icon">
                                               <h5>Management Skills for Administrative & HR Professionals</h5>
                                               <p>Friday, January 13, 2017</p>
                                            </div>
                                        </a>
                                    </div>
                                    <a href="#" class="btn btn-view-more no-border" target="_blank">View all workshop training</a>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-md-6">

                            <div class="workshop_training_right_section">
                                  <div class="workshop_training_header">
                                      <i class="fa fa-book" aria-hidden="true"></i> Certificate Courses
                                  </div>
                                  <div class="row all_workshop_training">
                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="marketing_bg signal_icon">
                                                 <i class="fa fa-university" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Banking, L/C, Customs & Shipping Procedure for Export Import Business</h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>

                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="commercial_bg signal_icon">
                                                 <i class="fa fa-building-o" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Human Resource Management</h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>

                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="it_bg signal_icon">
                                                 <i class="fa fa-male" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Winning A UN/International Job</h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>

                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="hr_bg signal_icon">
                                                 <i class="fa fa-male" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Professional Social Media Marketing </h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>

                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="banking_bg signal_icon">
                                                 <i class="fa fa-university" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Appraisal, Financing & Management of Project</h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>

                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="banking_bg signal_icon">
                                                 <i class="fa fa-upload" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Legal Issues for Administrative Professionals</h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>

                                      <div class="col-md-12">
                                          <a href="#">
                                              <div class="banking_bg signal_icon">
                                                 <i class="fa fa-users" aria-hidden="true"></i>
                                              </div>
                                              <div class="marketing_bg_icon">
                                                 <h5>Management Skills for Administrative & HR Professionals</h5>
                                                 <p>Friday, January 13, 2017</p>
                                              </div>
                                          </a>
                                      </div>
                                      <a href="#" class="btn btn-view-more no-border" target="_blank">View all workshop training</a>
                                  </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
          </div>
    </div><!--End_Workshop_Trainer_Section-->

 
 
 <?php $this->load->view('footer'); ?>


