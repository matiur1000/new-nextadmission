<script src="<?php echo base_url('resource/source/script.js'); ?>"></script>
<link href="<?php echo base_url('resource/source/styles.css'); ?>" rel="stylesheet">
<div id='cssmenu'>
	<ul>
	    <?php
			foreach($profileMenuInfo as $v){
			 if($v->id == 3){
		       $menuLink  = array('home','adWiseCompanyDetail',$organizeId);
		     }else if($v->id == 4){
		       $menuLink  = array('home','organizeAbout',$organizeId);
		     }else if($v->id == 6){
		       $menuLink  = array('home','organizeAdvertise',$organizeId);
		     }else if($v->id == 7){
		       $menuLink  = array('home','organizeContact',$organizeId);
             }else if($v->id == 16){
		       $menuLink  = array('home','organizePhotogallery',$organizeId);
             }
		?> 
	    <li><a href="<?php if($v->id == 5){ echo "#"; }else{ echo site_url($menuLink); } ?>"><?php echo $v->menu_name ?></a>
	        <?php if($v->id == 5){ ?>
			 <ul>
			      <?php
				       foreach($orgProgramInfo as $v){
					    $courseLink  = array('home','organizeCourseDetails',$organizeId, $v->id);
				 ?>
			      <li><a href="<?php echo site_url($courseLink);?>"><?php echo $v->programe_name; ?></a></li>
				  <?php } ?>
			 </ul>
			<?php } ?>
		</li>
		<?php } ?>
	</ul>
</div>