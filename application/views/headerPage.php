 <style>
	.red{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#FF0000;
	}
	.green{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#3300FF;
	}
	.yellow{
	font:Arial, Helvetica, sans-serif;
	font-size:13px;
	font-weight:normal;
	color:#003300;
	}

	.popover{
    max-width: 100%; /* Max Width of the popover (depending on the container!) */
 }
    .button {
	display: inline-block;
	background: #6FB4FB;
	color: #FFFFFF;
	text-decoration: none;
	padding: 5px 10px;
}

.badge-notify{
   background:red;
   position:relative;
   top: -25px;
   left: -5px;
}

[data-notifications] {
	position: relative;
}

[data-notifications]:after {
	content: attr(data-notifications);
	position: absolute;
	background: red;
	border-radius: 50%;
	display: inline-block;
	padding: 0.3em;
	color: #f2f2f2;
	right: -15px;
	top: -15px;
}

}	

 </style>
 
 <link href="<?php echo base_url('resource/source/headercss.css'); ?>" rel="stylesheet">

 <script type="text/javascript">
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
	}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

 

   <div class="row">
   		<!-- Logo -->
	<div class="col-lg-3 col-xs-12 col-sm-3 text-right" style="margin-top:10px; float:left;">
		<a href="<?php echo site_url("home"); ?>">
		  <img src="<?php echo base_url("Images/Basic_image/$headerBasicInfo->logo_image"); ?>" width="235" height="75" align="right" />
		</a>
	</div>
	<?php 
	     $lik_valu1  = $topAddInfo1->add_link;     
		  if(!empty($lik_valu1)){
		   $topLinkd  = $lik_valu1;
		 }
		 
		 $lik_valu2  = $topAddInfo2->add_link;
		  if(!empty($lik_valu2)){
		   $topLink2d = $lik_valu2;
		 }
	  
		 $topLink		 = array('home','orgWiseAdvritismentView', $topAddInfo1->id, $topAddInfo1->user_id);
		 
		 $topLink2		 = array('home','orgWiseAdvritismentView', $topAddInfo2->id, $topAddInfo2->user_id);
	
	
	?>
	<div class="col-lg-5 col-xs-12 col-sm-5" align="center">
		<a target="_blank" href="<?php if(!empty($topLinkd)){ echo $topLinkd; } else { echo site_url($topLink); } ?>">
		    <img style="display:inline;" src="<?php echo base_url("Images/Add_image/$topAddInfo1->add_image"); ?>" width="220" height="90" style="border:1px solid;"  />
		</a>   
		  &nbsp; 
		<a target="_blank" href="<?php if(!empty($topLink2d)){ echo $topLink2d; } else { echo site_url($topLink2); } ?>">
		   <img style="display:inline;" src="<?php echo base_url("Images/Add_image/$topAddInfo2->add_image"); ?>" width="220" height="90" style="border:1px solid;"  />
		 </a>
	</div>
	
	<div class="col-lg-4 col-xs-12 col-sm-4 pull-right">
	   <?php 
			$regLink  	 	= array('registration','index');
			$orgRegLink  	= array('registration','organizeRegistration');
			$liveChatLink  	= array('liveChat');
	         if(! isActiveUser()) { 
	      ?>
		  <div class="row" style="float:right;">
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="float:right; margin-right:0; padding-right:0;">
		        <button type="button" id="customePostion" class="login_reg pop" style="height:35px; float:left; margin-right:2px;" data-container="body" data-toggle="popover"  data-placement="bottom" data-html="true" data-target-content="#myModal" data-title="Login">Login</button>
		        <button type="button" class="login_reg pop" style="height:35px; float:left; margin-right:2px;" data-container="body" data-placement="bottom" data-toggle="popover"  data-html="true" data-target-content="#regModal" data-title="Registration">Registration</button>
		       <a style="float:left; margin-right:2px;" href="<?php echo site_url($liveChatLink) ?>"><input type="button" name="Button22" class="login_reg" value="Live Chat" style="height:35px; width:100px"/></a>
		       <input type="button" name="Button3" value="News" class="login_reg goto-news" style="height:35px; float:left; margin-right:2px;"/>
		    </div>
		 </div>

	    <?php }else{ ?>
	       <div class="row">
		     <div class="col-md-1">
		     </div>
		      <div class="col-md-3" style="padding-left:5px">
		       <a href="<?php echo site_url($liveChatLink) ?>"><input type="button" name="Button22" class="login_reg" value="Live Chat" style="height:35px; width:100px"/></a>
		    </div>
		    <div class="col-md-2" align="left" style="padding-left:0">
		       <input type="button" name="Button3" value="News" class="login_reg goto-news" style="height:35px;"/>
		    </div> 
		    <div class="col-md-6" style="padding-right:30px;">
		         <?php if( isActiveUser() ) {
		      if($userType == 'Organization User'){
			    $userPanelLink = array('organizationUserHome');  
			  }else{
			    $userPanelLink = array('generalUserHome');
			  }
		    ?>
		      <div class='cssmenu' style="background:none; height:30px; top:-8px; float:right">
				<ul style="padding:0">
				   <li class='has-sub' style="padding-bottom:5px"><a style="font-size:12px; text-align:top;" href='#'><span style="color:#0000FF"><?php echo $activeUser; ?></span></a>
				      <ul>
                        <li><a href='<?php echo site_url($userPanelLink);?>'><span>My Account</span></a> </li>
				         <li><a href="<?php echo site_url('organizationUserHome/profileUpdate/'.$userDetails->id);?>"><span>Edit Accounts</span></a> </li>
						   <li><a href="<?php echo site_url('organizationUserHome/changPassword/'.$userDetails->id);?>"><span>Change password</span></a> </li>
				          <!-- <li><a href='#'><span>Feedback</span></a> </li>
						  <li><a href='#'><span>Help / Support</span></a> </li>
						   <li><a href='#'><span>Service & support</span></a> </li>-->
						   <li><a href='<?php echo site_url('loginForm/organizeLogout'); ?>'><span>Logout</span></a> </li>
				      </ul>
				   </li>
				</ul>
			</div>   
			<?php } ?>
		    </div>
		 </div>

	    <?php } ?>

	   <div class="row" style="padding-right:0; margin-right:0; float:right;">
		    <div class="col-md-6" style="padding-top:15px; padding-right:0; float:right; margin-right:45px;">	                    
		        <div id="google_translate_element"></div> 
		    </div>
	   </div>
	</div>
</div> <!-- End Row -->



<!-- Modal -->
 <!-- Start Modal one -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<form id="logInForm"  action="<?php echo site_url('loginForm/loginAuthenticate'); ?>" method="post" class="form-horizontal">
	  <div class="col-md-12">
	  	<span class="logInFail"></span>
	  </div>
	    
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="type" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="type" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="email" class="form-control" id="email" name="email" placeholder="Email / User Name" tabindex="1">		
		</div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2">
		</div>
	  </div>
	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Sign in</button> &nbsp; &nbsp; <a href="<?php echo site_url('registration/genForgotPaword'); ?>"> Forgot Password?</a></label>
		</div>
	  </div>
	</form>
</div>
<!-- End Modal one -->

<!-- Start Modal two -->
<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="regModalLabel" >
	
	  
	  <div class="form-group">
		<div class="col-sm-12">
			<label class="radio-inline">
				<input type="radio" name="regType" value="organization" style="margin-top:2px" /> Organization
			</label>
			<label class="radio-inline">
				<input type="radio"  name="regType" value="other" style="margin-top:2px" />  Others
			</label>							
		</div>
	  </div>
	  <div class="col-sm-12">&nbsp;</div>

  
<div id="organize_reg">
      <form id="regForm" action="<?php echo site_url('registration/orgRegStore'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="email" class="control-label">UserId</label>	 -->	
			<input type="text" class="form-control" id="account_name" name="account_name" 
					placeholder="Account Name" tabindex="1" value="<?php echo set_value('account_name'); ?>">
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
		   <input type="text" class="form-control" id="organizationname" name="organizationname" placeholder="Organization Name" tabindex="1" >
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<input type="email" class="form-control" id="user_id2" name="user_id" placeholder="Email / Login id" tabindex="2" 
					value="<?php echo set_value('user_id'); ?>"><span class="chkEmail"></span>
		</div>
	  </div>


	  <div class="form-group">
	  	<div class="col-sm-12">
			<!-- <label for="password" class="control-label">Password</label>		 -->
			<select class="form-control" id="country_id_org" name="country_id_org"  tabindex="2" >
				<option value="" selected>Sellect Country Name</option>
				<?php foreach ($countryInfo as $v){?>
				<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
				<?php } ?>
                         
			</select>
		</div>
	  </div>

	  


	   <div class="form-group">
	  	<div class="col-sm-12">
			<input type="password" class="form-control password" id="password" name="password" placeholder="Password" tabindex="2" value="<?php echo set_value('password'); ?>"><?php echo form_error('password'); ?>
		   <span class="first"></span>
		</div>
	  </div>

	  <div class="form-group">
	    <div class="col-sm-12">
			<input type="password" class="form-control conformpassword" id="con_password" name="con_password" placeholder="Confirm Password" tabindex="2">
			<span class="second"></span>
		</div>
	  </div> 

	  
	  <div class="form-group">
		<div class="col-sm-12">
		  <button type="submit" class="btn btn-primary">Create Account</button>
		</div>
	  </div>
	</form>

</div>


	 <div id="other_reg" style="display:none">
	     <form id="genReg" action="<?php echo site_url('registration/genRegStore1'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

	       <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="email" class="control-label">UserId</label>	 -->	
				<input type="text" class="form-control" id="name" placeholder="Full Name" name="name" tabindex="5" 
					value="<?php echo set_value('name'); ?>"> <?php echo form_error('name'); ?>
			</div>
		  </div>

		  <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="password" class="control-label">Password</label>		 -->
				<input type="text" class="form-control" id="user_id" name="user_id" placeholder="User Id/Email" tabindex="4" 
					value="<?php echo set_value('user_id'); ?>"><?php echo form_error('user_id'); ?><span class="chk"></span>
			</div>
		  </div>

		  <div class="form-group">
		  	<div class="col-sm-12">
				<!-- <label for="password" class="control-label">Password</label>		 -->
				<select class="form-control" id="country_id" name="country_id"  tabindex="2" >
					<option value="" selected>Sellect Country Name</option>
					<?php foreach ($countryInfo as $v){?>
					<option value="<?php echo $v->id; ?>" data-country-code="<?php echo $v->country_code; ?>" data-country-code-phon="<?php echo $v->country_code_phon; ?>" data-nationality="<?php echo $v->nationality; ?>"><?php echo $v->country_name; ?></option>
					<?php } ?>
				</select>
			</div>
		  </div>

		   <div class="form-group">
		  	<div class="col-sm-12">
				<input type="password" class="form-control password3" id="password" name="password" placeholder="Password" tabindex="23" 
				  value="<?php echo set_value('password'); ?>" ><?php echo form_error('password'); ?>
				  <p class="third"></p>
			</div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-12">
				<input type="password" class="form-control conformpassword3" id="conform_password" name="conform_password" placeholder="Conform Password" 
					tabindex="20" value="<?php echo set_value('conform_password'); ?>"><?php echo form_error('conform_password'); ?>
					<p class="fourth"></p>
			</div>
		  </div> 

		  <div class="form-group">
			<div class="col-sm-12">
			  <button type="submit" class="btn btn-primary">Create Account</button>
			</div>
		  </div>

	     </form>
	 </div>

</div>
<!-- End Modal tow -->

<script>
  	$(document).on("submit", "#logInForm", function(e){
  		
		var postData = $(this).serializeArray();
		var formURL  = $(this).attr("action");
		
		var type 	= $(this).find("input[name='type']:checked").val();
		
		if(type =='organization'){
		   var successUrl	= "<?php echo site_url('organizationUserHome'); ?>";
		}else{
		  var successUrl	= "<?php echo site_url('generalUserHome'); ?>";			
		}
	
		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data){
				if(data==1){
					$("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='hidden'], #logInForm input[type='password']").val("");
					$(".logInFail").text("Wrong email or password. Please try again!");
					$(".logInFail").css("color", "red");
					$("#msg").css("display", "block"); 
				} else if(data==2){
					 $(".logInFail").text("Please Select a type Organization or Other");
					 $(".logInFail").css("color", "red"); 
					 $("#msg").css("display", "block"); 					
				}else{
				 location.replace(successUrl);
				}
			}					
		});
	
	   	e.preventDefault();
	});
	
	$(".close").click(function(){
	   $("#logInForm input[type='text'], #logInForm input[type='email'], #logInForm input[type='password']").val("");
	   $("#logInForm input[type='radio']").prop("checked",false);
	});

	
  
  $(window).ready(function(){
	  $('[data-toggle="popover"]').popover({
		    content: function(){
			  var target = $(this).attr('data-target-content');
			   return $(target).html();
			  },
		    trigger: 'manual'
		}).click(function (e) {
			$( this ).popover( "toggle" ); 
			e.stopPropagation();
			  	
		});

		$('[data-toggle="popover"]').on('click', function (e) {
		    $('[data-toggle="popover"]').not(this).popover('hide');
		});

	  $('body').on('click', function (e) {
	    $('[data-toggle="popover"]').each(function () {
	        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
	            $(this).popover('hide');
	        }
	    });
	  });

   });


	  $(document).on("change", "input[name='regType']", function(){
		   var typeValue = $(this).val();
		   var parents = $(this).parents('.popover-content');

		   if(typeValue =='organization'){
		    	parents.find("#organize_reg").css("display", "block");
		    	parents.find("#other_reg").css("display", "none");		    	
		   }else{
		     	parents.find("#organize_reg").css("display", "none");
		    	parents.find("#other_reg").css("display", "block");
		   }
		   
		  	
		});


	  $(document).on("change", "#country_id", function() {
		var country_id = $(this).val();
		parents = $(this).parents('.popover-content');	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
			   parents.find("#city_id").html(data);
			}
		});
		
	});
	
	
	$(document).on("change", "#country_id_org", function() {
		var country_id = $(this).val();
		parents = $(this).parents('.popover-content');
		console.log(country_id);	
	
		$.ajax({
			url : SAWEB.getSiteAction('registration/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				parents.find("#city_id_org").html(data);
			}
		});
		
	});



	// Password Count	
   
   $(document).on("keyup", ".password", function() {
	 var len = $(this).val().length;
	 parents = $(this).parents('.popover-content');
	    
		if(len<=1){
		parents.find(".first").text("");
		parents.find(".first").removeClass("red");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("green");
		
	  } else if(len<=4){
	  	parents.find(".first").text("Very Weak");
		parents.find(".first").addClass("red");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("green");
	   } else if(len<=8){

	   	parents.find(".first").text("Good");
		parents.find(".first").addClass("green");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("red");

	   } else if(len<=9){

	   	parents.find(".first").text("Good");
		parents.find(".first").addClass("green");
		parents.find(".first").removeClass("yellow");
		parents.find(".first").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword", function() {
  	 parents 		= $(this).parents('.popover-content');
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		=  parents.find(".password").val();

	 if(conpass){

		  if(conpass != Pass){
		  	parents.find(".second").text("Your New Password and Confirm Password donot match!");
			parents.find(".second").addClass("red");
			parents.find(".second").removeClass("green");
		  
		  } else {
		  	parents.find(".second").text("Password Match");
			parents.find(".second").removeClass("red");
			parents.find(".second").addClass("green");
		   	
		  }

		} else {
		   parents.find(".second").text("Password Match");
		   parents.find(".second").removeClass("red");
		   parents.find(".second").removeClass("green");
		  
		}
	});
	
	
	
	// Password Count


	$(document).on("keyup", ".password3", function() {
	 var len = $(this).val().length;
	 parents = $(this).parents('.popover-content');
	    
		if(len<=1){
		parents.find(".third").text("");
		parents.find(".third").removeClass("red");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("green");
		
	  } else if(len<=4){
	  	parents.find(".third").text("Very Weak");
		parents.find(".third").addClass("red");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("green");
	   } else if(len<=8){

	   	parents.find(".third").text("Good");
		parents.find(".third").addClass("green");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("red");

	   } else if(len<=9){

	   	parents.find(".third").text("Good");
		parents.find(".third").addClass("green");
		parents.find(".third").removeClass("yellow");
		parents.find(".third").removeClass("red");
	   }
	});
	
	
  $(document).on("keyup", ".conformpassword3", function() {
  	 parents 		= $(this).parents('.popover-content');
	 var conpass 	= $(this).val();
	 //var Pass = $(".password").val();
	 var Pass 		=  parents.find(".password3").val();

	 if(conpass){

		  if(conpass != Pass){
		  	parents.find(".fourth").text("Your New Password and Confirm Password donot match!");
			parents.find(".fourth").addClass("red");
			parents.find(".fourth").removeClass("green");
		  
		  } else {
		  	parents.find(".fourth").text("Password Match");
			parents.find(".fourth").removeClass("red");
			parents.find(".fourth").addClass("green");
		   	
		  }

		} else {
		   parents.find(".fourth").text("Password Match");
		   parents.find(".fourth").removeClass("red");
		   parents.find(".fourth").removeClass("green");
		  
		}
	});
	


	
   

	$(document).on("blur", "#user_id", function() {
		parents 	= $(this).parents('.popover-content');
		var userId 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/chkUserId'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userId : userId },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				parents.find(".chk").text("This Email Already Exit!");
				parents.find(".chk").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				parents.find(".chk").text("");
				parents.find(".chk").removeClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});



	$(document).on("blur", "#user_id2", function() {
		parents 		= $(this).parents('.popover-content');
		var user_id 	= $(this).val();
		  $.ajax({
			url : SAWEB.getSiteAction('registration/userEmailChk'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { user_id : user_id },
			dataType : "html",
			success : function(data) {			
				if(data == 1){
				parents.find(".chkEmail").text("This Email Already Exit!");
				parents.find(".chkEmail").addClass("red");
				//parents.find(".regsub button[type="submit"]").attr("disabled", "disabled");
				} else {
				parents.find(".chkEmail").text("");
				parents.find(".chkEmail").addClass("red");
				//parents.find(".regsub button[type="submit"]").removeAttr("disabled", "disabled");
	   			 
			  }
		    }
		 });
			
	});

	$(document).ready(function() {
	   setTimeout(getAllUnreadMsg, 2000);
	});
	

	// Get All unread msg
    
	 function getAllUnreadMsg() {
		  $.ajax({
	        url : SAWEB.getSiteAction('liveChat/getAllUnreadMsg'),
			type: "POST",
			data : {},
			dataType: "html",
	        success: function (data) { 
	        	$("#newMsgNum").html(data);

	          }
	       });
		   setTimeout(getAllUnreadMsg, 2000);
	}
	
</script>