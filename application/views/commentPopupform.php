<style>
  .commentStyle {
	border-left:1px;
	border-right:1px;
	border-top:1px;
    border-style:solid;
	border-color:#CCCCCC;
	border-width:100%;
	padding:5px;
	
}

.commentStyle:last-child {
    border-bottom:1px solid #CCCCCC;	
} 

.commentStyle h5{
	font-size: 14px;
	font-weight: normal;
	margin-top: 5px;
	margin-bottom: 0px;
}
.commentStyle p{
	font-size: 14px;
	font-weight: normal;
	padding-bottom:10px;
	float:left;
}

.commentStyle img{
	background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    display: inline-block;
    line-height: 1.42857;
    margin-right: 10px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}

#share-buttons img {
width: 35px;
padding: 5px;
border: 0;
box-shadow: 0;
display: inline;
}

</style>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title" id="myModalLabel"><?php echo $UserPostInfo->title; ?> <?php if($visitorId == $UserPostInfo->user_id ){ ?><spsn style="float:right; font-size:12px; padding-right:10px;"><a class="updatePost" data-id="<?php echo $UserPostInfo->id ?>" style="cursor:pointer">Edit post</a></spsn><?php } ?></h3>
</div>
  
		<div class="modal-body">
		   <div style="min-height:220px">
			<img src="<?php echo base_url("Images/Post_image/$UserPostInfo->image"); ?>" class="pull-left" style="margin:0 15px 0 0" width="180" height="180" />
		    <p style="text-align:justify"><?php echo $UserPostInfo->description ?></p>
			
			<p style="text-align:right; padding-bottom:10px;">Posted By : <span style="font-size:14px; font-weight:bold"><?php echo $UserPostInfo->name; ?></span></p>
	        </div>

			<div class="row"  style="padding-bottom:20px">
			    <div class="col-lg-8">
		  
                </div>
                 <div class="col-lg-1 text-right">  </div>
		        <div class="col-lg-3 text-right">
		           <span style="padding:0 23px 25px 0; font-size:20px; color:#2DAAE1">Share on</span>
		           <div id="share-buttons">
					    <!-- Facebook -->
					    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('home/fromFacebook/'.$post_id);?>&t=<?php echo $UserPostInfo->title;?>&[images][0]=<?php echo base_url("Images/Post_image500_347/$UserPostInfo->image"); ?>"
					   onclick="javascript:window.open(this.href, '<?php echo site_url('home/fromFacebook/'.$post_id);?>', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Share on Facbook" class="kk-share-button kk-share-via-facebook">
					       <img src="<?php echo base_url("resource/images/facebook.png"); ?>" alt="Facebook" />	
					    </a>
					    
					    <!-- Google+ -->
					    <a href="https://plus.google.com/share?url={<?php echo site_url('home/fromFacebook/'.$post_id);?>}" onclick="javascript:window.open(this.href,
		  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share on Google" class="kk-share-button kk-share-via-google">
					       <img src="<?php echo base_url("resource/images/google.png"); ?>" alt="Google" />	
					    </a>
					    <!-- Twitter -->
					   <a href="https://twitter.com/share?url=<?php echo site_url('home/fromFacebook/'.$post_id);?>&via=<?php  echo $UserPostInfo->title; ?>"
		   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
		   target="_blank" title="Share on Twitter" class="kk-share-button kk-share-via-twitter"><img src="<?php echo base_url("resource/images/twitter.png"); ?>" alt="Twitter" /></a>

					</div>
		           <!-- <div class="fb-share-button" data-href="<?php echo site_url('commentPopup/index/'.$post_id); ?>" data-layout="button"></div> -->
		        </div>
	</div>
	
	
	     <div class="row">
		    <div id="comntView" class="col-lg-12">
			    <?php 
				 	
			        if(!empty($UserCmntInfo)){
					 //print_r($UserCmntInfo);
					 foreach ($UserCmntInfo as $v){
					   $comment_id    = $v->id;
					   $post_id    = $v->post_id;
					   $multiId		 = array('commentPopup','replyStore', $post_id,$comment_id);
					   
					  
					   
				  ?>
			     <div class="col-md-12 commentStyle">
			        <?php if($v->image){ ?>
			           <img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
                    <?php }else{ ?>
			           <img src="<?php echo base_url("resource/img/profile2.png"); ?>" class="pull-left" width="60" height="60" style="padding-right:5px;" />	
                    <?php } ?>
					<h5><a><?php echo $v->name; ?></a></h5>
					<h6><?php echo $v->comment_date; ?></h6>	
					<p style="float:left"><?php echo $v->comment; ?></p> 
			     </div>
			  
			  <?php } } ?>
			   
			  
           </div>
		   
		    
            <div class="col-md-12" style="margin-top:20px;">
			<?php if( isActiveUser() ) { ?>
			   <form id="comntParent" action="<?php echo site_url('commentPopup/commentStore'); ?>" method="post" enctype="multipart/form-data">
			      <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id; ?>">
		           <div class="form-group">
				    <label for="exampleInputEmail1">Write Your Comment</label>
					<textarea name="comment" id="comment" class="form-control" rows="3"></textarea>
				  </div>
			   	</form>
			   <?php } else { ?>
                <h4 style="color:#FF0000">Please Login To Comment.</h4>
				
            <?php } ?>
			  
           </div>
		 </div>
		
</div>
		

<div class="modal-footer">
	<button class="btn btn-danger" data-dismiss="modal">Close</button>
</div>



<script>
  $(document).ready(function(){

  });
  $(".comnt").click(function(){
   $(this).parents('#blockCmnt').find('#reply').css("display", "block");
   
  });
  
  
  $(document).on("keyup", "#comment", function(e){ 

	var parents  = $(this).parents('#comntParent');
	var comment  = $(this).val();
	var postId   = parents.find('#post_id').val();
	var formURL  = parents.attr("action");
	console.log(postId);
	console.log(comment);
	console.log(formURL);

    if (e.which == 13 && ! e.shiftKey) {
		$.ajax(
		{
			url : formURL,
			type: "POST",
			data : {postId : postId, comment : comment},
			success:function(data){
				$("#comntView").html(data);				
				$("#comntParent input[type='text'], textarea").val("");
			}
		});
	  }
		
		e.preventDefault();
	});





   $("#replyForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#replayView").html(data);				
					$("#replyForm input[type='text']").val("");
				}
			});
			
			e.preventDefault();
		});
</script>