<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>online form</title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/popup.css'); ?>" rel="stylesheet">
    <style>
		.click{
		  	background:#a9d23b;
			padding: 5px;
			border-radius: 5px;
			color: black;
		}
		.success{

		}
		.documents{margin-bottom: 50px;}
		.documents p{
		    line-height: 35px;
		    font-size: 31px;
		    margin-left: 100px;
		    margin-bottom: 10px;
		    color: Gold;
		    line-height: 30px;
		}
		address p{
		    font-size: 25px;
		    width: 800px;
		    color: beige;
		    line-height: 30px;
		}
		table{
			width: 750px !important;
		    font-size: 25px;
		    margin: 0 auto;
		    color: #f0ad4e;
		}
		th{
			text-align:center;
		}
	  	body{ background:background-color: #2F2727; background-image: url(images/radial_bg.png); background-position: center center; background-repeat: no-repeat; /* Safari 4-5, Chrome 1-9 */ /* Can't specify a percentage size? Laaaaaame. */ background: -webkit-gradient(radial, center center, 0, center center, 460, from(#1a82f7), to(#2F2727)); /* Safari 5.1+, Chrome 10+ */ background: -webkit-radial-gradient(circle, #1a82f7, #2F2727); /* Firefox 3.6+ */ background: -moz-radial-gradient(circle, #1a82f7, #2F2727); /* IE 10 */ background: -ms-radial-gradient(circle, #1a82f7, #2F2727); /* Opera couldn't do radial gradients, then at some point they started supporting the -webkit- syntax, how it kinda does but it's kinda broken (doesn't do sizing) */}
	</style>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="success" align="center">
						<h1 align="center" style="margin-bottom: 25px;color: rgb(255, 255, 255); font-size: 38px; text-shadow: 0px 0px 5px red, 0px 0px 10px rgb(0, 0, 0), 0px 0px 10px rgb(255, 0, 222);font-weight: unset;">
							Thank you very much for submitting your Admission form. <br/>Please send your admission related documents as below required:
						</h1>
						<div class="documents" style="text-align:left; ">
							<p>1. Previous Academic documents' scan copies (Transcript / Mark sheets)</p>
							<p>2. Photo</p>
							<p>3. Passport scan copy/Birth Certificate scan copy</p>
							<p>4. 5000 Taka for file opening charge to <span style="background-color:#f0ad4e; color:Crimson;" >Bkash No.:</span> 01911342308 or</p>
							<p style="text-indent:30px;">Following Bank Accounts:</p>
							<table class="table table-bordered" cellpadding="1" cellspacing="1" border="1" >
								<thead>
									<tr>
										<th>Bank Name</th>
										<th>A/C No.</th>
										<th>A/C Name</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Standerd Charterd Bank</td>
										<td>18977583801</td>
										<td>Obaidur Rahman</td>
									</tr>
									<tr>
										<td>Dutch Bangla Bank Ltd</td>
										<td>10510188924</td>
										<td>Obaidur Rahman</td>
									</tr>
									<tr>
										<td>City Bank</td>
										<td>1421779619001</td>
										<td>Global Star Ltd.</td>
									</tr>
								</tbody>
							</table>
						</div>
						<address>
							<p>Please use e-mail: info@nextadmission.com or globalstarltdbd@gmail.com for sending your above documents.</p>
							<p>Wish your all the best</p><br>

							<p><img style="width: 70px; vertical-align: -webkit-baseline-middle; margin-right: 10px;" src="<?php echo base_url('resource/images/logo_gsl.png'); ?>" alt="logo">Global Star Ltd</p>  
							<p>(Education Consultant)</p>  
							<p>31 Malek tower ( 9th floor- 2nd lift ) Farmgate, Dhaka, Bangladesh Mobile: +880 1911 342308,
							04478335521-5,7,8 01950222777, 01780300059, 01715015972,01626523929.</p>  
						</address>



						<!-- <h1>
							<span class="click"><a href="http://www.nextadmission.com" style="color: rgb(141, 0, 255);">click here</a></span>
						</h1> -->
					</div>
				</div>
			</div>
		</div>
	</body>
</html>