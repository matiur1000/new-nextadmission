<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paymentcollection extends MX_Controller {

		static $model 	 = array('M_admin','M_menu_manage', 'M_submenu_manage', 'M_deeper_sub','M_organize_profile_manage');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('session');
			$this->load->library('pagination');
			isAuthenticate();
	
		}
	

		public function index()
	
		{		
			
		 if(isAuthenticate()){
			$data['userName']    = getUserName();
			$data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;		
			$data['maxSlNo']	= $this->M_menu_manage->findMaxSl();
			
			$data['paymentdetails']	= $this->M_organize_profile_manage->findAllMenu('payment_manage');
			
			
			$this->load->view('controlpanel/paymentDetails/paymentcollectionPage', $data);
	      }
		}
	
		public function approve()
		{
			$id = $this->input->post("id");
			$data['payment_status'] = "paid";
			$this->db->update('payment_manage', $data, array('id' => $id));    
		}
			
			
		

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

