<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NewsAndAdvertiseManage extends MX_Controller {

		static $model 	 = array('M_admin','M_news_and_event_manage','M_advertisement_manage','M_carrer_consultant_manage','M_add_manage','M_programe_manage',
			'M_add_booking','M_region_manage','M_country_manage','M_search_table','M_organize_profile_manage');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('image_lib');
			$this->load->library('session');
			$this->load->library('pagination');
			isAuthenticate();
	
		}
		
		public function index()
	
		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;			

			$data['newsInfo']	 = $this->M_news_and_event_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/newsManagePage', $data);
	       }
		}


		
		public function newsList_()
		{
			$data['newsInfo']	= $this->M_news_and_event_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/newsListPage', $data);
		}
		
		
		
		public function store()
		{	
			$id		    		    	= $this->input->post('id');
			$data['company_name'] 		= $this->input->post('company_name');
			$data['title']				= $this->input->post('title');
			$data['description'] 		= $this->input->post('description');
			$data['date']				= $this->input->post('date');
			$data['status'] 			= $this->input->post('status');
			$image		 				= $this->input->post('news_image');
				
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
				$news_info = $this->M_news_and_event_manage->findById($id);
				
				if( !empty($news_info->image) && file_exists('./Images/News_image/'.$news_info->image) && !empty($image) ) {					
					unlink('./Images/News_image/'.$news_info->image);	
				}
				$this->M_news_and_event_manage->update($data, $id);
				} else {	
				  if(isAuthenticate()){
				   $data['user_id']         = getUserName();
				  }	
				    if($data['title'] !=''){			
					  $this->M_news_and_event_manage->save($data);
					}
				}
				
				$this->newsList_();
			 
	     }
			
		
		public function newsEdit()
		{
			$id 				= $this->input->post('id');		
			$newsEditInfo 		= $this->M_news_and_event_manage->findById($id);
					
			echo json_encode($newsEditInfo);
		}

		public function latestNewsManage()
	
		{		
		  if(isAuthenticate()){
			$data['userName']    		= getUserName();
			 $data['adminType']         = $this->session->userdata('adminType');
			$data['page_title']  		= self::Title;			

			$data['latestNewsInfo']	 = $this->M_organize_profile_manage->findAllData('breaking_news_manage');
			$this->load->view('controlpanel/newsAndAdvertise/latestNewsManagePage', $data);
	       }
		}



		public function latestNewsStore()
		{	
			$id		    		    	= $this->input->post('id');
			$data['title']				= $this->input->post('title');
			$data['details'] 			= $this->input->post('details');
				
			    if(!empty($id)) {
				    $this->M_organize_profile_manage->update2('breaking_news_manage',$id, $data);
				} else {
				  if($data['title'] !='' || $data['details'] !=''){	
					 $this->M_organize_profile_manage->save('breaking_news_manage', $data);
				   }
				}
				
				$this->breakingNewsList_();
			 
			}


	    public function breakingNewsList_()
		{
			$data['latestNewsInfo']	= $this->M_organize_profile_manage->findAllData('breaking_news_manage');
			$this->load->view('controlpanel/newsAndAdvertise/latestNewsListPage', $data);
		}

		public function latestNewsEdit()
		{
			$id 				= $this->input->post('id');		
			$latestNewsEditInfo = $this->M_organize_profile_manage->findById('breaking_news_manage',$id);
					
			echo json_encode($latestNewsEditInfo);
		}

		public function latestNewsDelete()
		{ 		
			$deleteDataList = $this->input->post('news_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				  $this->M_organize_profile_manage->destroy('breaking_news_manage', $deleteId);
				}
			}
		}

		public function newsDelete()
		{ 		
			$deleteDataList = $this->input->post('news_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){	
				  $model	= $this->M_news_and_event_manage->findById($deleteId);
				  if(!empty($model->image)){
					unlink('./Images/News_image/'.$model->image);
				  }					
				  $this->M_news_and_event_manage->destroy($deleteId);
				}
			}
		}
		
		public function addManage($onset = 0)
	
		{		
		 if(isAuthenticate()){
			$data['userName']    	= getUserName();
			 $data['adminType']     = $this->session->userdata('adminType');
			$data['page_title']  	= self::Title;
			
			$perPageLimit        	= $this->input->post('perPageLimit');


            if($perPageLimit !='all'){
			   if(!empty($perPageLimit)){
					$setLimit = $perPageLimit; 
				} else {
					$setLimit = "30"; 
				}
					$data['perPageLimit']   = $perPageLimit; 	
					$data['addInfo']	 	= $this->M_add_manage->findAll($setLimit, $onset);
					$data['allRegion']	 	= $this->M_region_manage->findAll();
					$data['allCountry']	 	= $this->M_country_manage->findAllCountry();

					$data['onset'] 			= $onset;
					$config['base_url'] 	= base_url('controlpanel/newsAndAdvertiseManage/addManage');
					$config['total_rows'] 	= $this->M_add_manage->countAll();
					$config['uri_segment'] 	= 4;
					$config['per_page'] 	= $setLimit;
					$config['num_links'] 	= 7;
					$config['first_link']	= FALSE;
					$config['last_link'] 	= FALSE;
					$config['prev_link']	= 'Prev';
					$config['next_link'] 	= 'Next';
			
					$this->pagination->initialize($config); 
			    } else {
			    	$data['perPageLimit']   = $perPageLimit; 	
					$data['addInfo']	 	= $this->M_add_manage->findAllData();
					$data['allRegion']	 	= $this->M_region_manage->findAll();
					$data['allCountry']	 	= $this->M_country_manage->findAllCountry();
			    }

			$this->load->view('controlpanel/newsAndAdvertise/promotionPage', $data);
	      }
		}
		
		
		public function approvedAd()
	
		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;		
			$data['addInfo']	 = $this->M_add_manage->findAllUpapprovedAd();
			$this->load->view('controlpanel/newsAndAdvertise/approveadManagePage', $data);
	      }
		}

		public function approvedNews()
		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;		
			$data['newsInfo']	 = $this->M_news_and_event_manage->findAllUpapprovedNews();
			$this->load->view('controlpanel/newsAndAdvertise/approveadNewsPage', $data);
	      }
		}
		
		
		
		public function addStore()
		{	
			
			$country_name 		    	= $this->input->post('country_name');
			$regionDetails   		    = $this->M_country_manage->findCountryId($country_name);
			$regionName   		        = $this->M_region_manage->findById($regionDetails->region_id);
			$select_website 		    = $this->input->post('select_website');
			if ($select_website == 'country') {
			   $data['region'] 		    = $regionName->region_name;
			   $data['country_name']    = $country_name;
			}
			//$data['company_name'] 		= $this->input->post('company_name');
			$data['positon'] 			= $this->input->post('positon');
			$data['serial_no'] 			= $this->input->post('serial_no');
			$data['publish_date'] 		= $this->input->post('publish_date');
			$data['expire_date'] 		= $this->input->post('expire_date');
			$data['manage_date'] 		= $this->input->post('manage_date');
			$data['deadline_date'] 		= $this->input->post('deadline_date');
			$data['title']				= $this->input->post('title');
			$data['description'] 		= $this->input->post('description');
			$data['add_link'] 			= $this->input->post('add_link');
			$data['status'] 			= $this->input->post('status');
			$image		 				= $this->input->post('add_image');

				if(!empty($image)){
					$data['add_image'] = $image;	
				}

				if(isAuthenticate()){
				   $data['user_id']    = getUserName();
				  }				
				  $this->M_add_manage->save($data);
			
				
				$this->addList_();
			 
			}
			
			
			public function addList_()
			{
				$perPageLimit        	= $this->input->post('perPageLimit');


	            if($perPageLimit !='all'){
				   if(!empty($perPageLimit)){
						$setLimit = $perPageLimit; 
					} else {
						$setLimit = "30"; 
					}
						$data['perPageLimit']   = $perPageLimit; 	
						$data['addInfo']	 	= $this->M_add_manage->findAll($setLimit, $onset);
						$data['allRegion']	 	= $this->M_region_manage->findAll();
						$data['allCountry']	 	= $this->M_country_manage->findAllCountry();

						$data['onset'] 			= $onset;
						$config['base_url'] 	= base_url('controlpanel/newsAndAdvertiseManage/addManage');
						$config['total_rows'] 	= $this->M_add_manage->countAll();
						$config['uri_segment'] 	= 4;
						$config['per_page'] 	= $setLimit;
						$config['num_links'] 	= 7;
						$config['first_link']	= FALSE;
						$config['last_link'] 	= FALSE;
						$config['prev_link']	= 'Prev';
						$config['next_link'] 	= 'Next';
				
						$this->pagination->initialize($config); 
				    } else {
				    	$data['perPageLimit']   = $perPageLimit; 	
						$data['addInfo']	 	= $this->M_add_manage->findAllData();
						$data['allRegion']	 	= $this->M_region_manage->findAll();
						$data['allCountry']	 	= $this->M_country_manage->findAllCountry();
				    }

				$this->load->view('controlpanel/newsAndAdvertise/addListPage', $data);
			}
				
			
			
		public function advertisement()

		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;			
			$data['addInfo']	 = $this->M_advertisement_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/advertisementPage', $data);
	      }
		}

		public function addUpdate()
		{

		 $id				    	= $this->input->post('id');
		 $data['title']				= $this->input->post('title');
		 $data['description'] 		= $this->input->post('description');
		 $data['add_link'] 			= $this->input->post('add_link');
		 $data['deadline_date']     = $this->input->post('deadline_date');
		 $image		 				= $this->input->post('add_image');

			if(!empty($image)){
				$data['add_image'] = $image;	
			}

			$add_info = $this->M_add_manage->findById($id);
				
			if( !empty($add_info->add_image) && file_exists('./Images/Add_manage/'.$add_info->add_image) && !empty($image) ) {					
				unlink('./Images/Add_manage/'.$add_info->add_image);	
			}	
		   $this->M_add_manage->update2($data, $id);


		   $adDetails 			    	= $this->M_add_manage->findById($id);
		   $datas['ad_title'] 	    	= $adDetails->title;
		   $adDetailsChk 				= $this->M_search_table->findByAdId($id);

		    if(!empty($adDetailsChk)){
		       $this->M_search_table->updateAd($datas, $id);
		    } else{
			  $datas['ad_id'] 	    	= $id;
			  $datas['ad_user_id']    	= $adDetails->user_id;

		      $this->M_search_table->save($datas);	
		    }

		   redirect ('controlpanel/newsAndAdvertiseManage/addManage');  

		}
		
		public function advertiseStore()
		{	
			$id		    		    	= $this->input->post('id');
			$data['company_name'] 		= $this->input->post('company_name');
			$data['title']				= $this->input->post('title');
			$data['description'] 		= $this->input->post('description');
			$data['status'] 			= $this->input->post('status');
			$image		 				= $this->input->post('com_logo');
				
				if(!empty($image)){
					$data['company_logo'] = $image;	
				}
				
				if(!empty($id)) {
				$add_info = $this->M_advertisement_manage->findById($id);
				
				if( !empty($add_info->company_logo) && file_exists('./Images/Add_manage/'.$add_info->company_logo) && !empty($image) ) {					
					unlink('./Images/Add_manage/'.$add_info->company_logo);	
				}
				$this->M_advertisement_manage->update($data, $id);
				} else {
				if(isAuthenticate()){
				   $data['user_id']         = getUserName();
				  }				
					$this->M_advertisement_manage->save($data);
				}
				
				$this->advertiseList_();
			 
			}
		
		public function advertiseList_()
		{
			$data['addInfo']	= $this->M_advertisement_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/advertiseListPage', $data);
		}
		
		
		
		
		public function addEdit()
		{
			$id 					= $this->input->post('id');		
			$addEditInfo 			= $this->M_add_manage->findById($id);
			//$addEditInfo->subList 	=  $this->M_add_manage->findAllByPosition($addEditInfo->positon);
			echo json_encode($addEditInfo);
		}

		public function approvedNewsEdit()
		{
			$id 					= $this->input->post('id');	
			$data['status']         = "aprove"; 
			$this->M_news_and_event_manage->update($data, $id);
		}

		
		
		public function approvedAdEdit()
		{
			$id 						= $this->input->post('id');	
			$adDateRange                = $this->M_add_manage->findById($id);

			$publishedDate              = $adDateRange->publish_date;
			$expireDate              	= $adDateRange->expire_date;
			$region              		= $adDateRange->region;
			$country_name              	= $adDateRange->country_name;
			$positon              		= $adDateRange->positon;
			$serial_no              	= $adDateRange->serial_no;
			$ad_id              		= $adDateRange->id;

			
		
			   function getDatesFromRange($start, $end) {
					$interval = new DateInterval('P1D');
				
					$realEnd = new DateTime($end);
					$realEnd->add($interval);
				
					$period = new DatePeriod(
						 new DateTime($start),
						 $interval,
						 $realEnd
					);
				
					foreach($period as $date) { 
						$array[] = $date->format('Y-m-d'); 
					}
				
					return $array;
			    }
		
		
			 $dates = getDatesFromRange($publishedDate, $expireDate);
		
		     if(!empty($dates)){
		     	$data['region']            = $region;
		     	$data['country_name']      = $country_name;
		     	$data['position']          = $positon;
		     	$data['serial_no']         = $serial_no;
				$data['ad_mId']            = $ad_id;

		     	foreach ($dates as $k => $v){
		     	   $data['booking_date']   = $dates[$k];	
		     	   $this->M_add_booking->save($data);			 
				}
		     }
		
				

			$data2['published_status']  	= "approve";
			$this->M_add_manage->update2($data2, $id);	
		}
		
		
		
		
		
		
		
		
		public function addvertiseDelete($id)
		{	
		    $model	= $this->M_advertisement_manage->findById($id);
			$only_image  = $model->company_logo;
						
			if(!empty($only_image)){
			unlink('./Images/Add_manage/'.$only_image);
			}		
			$this->M_advertisement_manage->destroy($id);
			redirect ('controlpanel/newsAndAdvertiseManage/advertisement');
		}


		public function addDelete()
		{ 		
			$deleteDataList = $this->input->post('add_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){
				  $model					= $this->M_add_manage->findById($deleteId);	
				   if(!empty($model->add_image)){
					 unlink('./Images/Add_image/'.$model->add_image);
				   }

				    $where   				= array('booking_date >=' =>$model->publish_date, 'booking_date <=' =>$model->expire_date);
					$bookingValue			= $this->M_add_booking->findAllDeleteData($where);

					foreach ($bookingValue as $v){
				       $this->M_add_booking->destroy($v->add_id);
					}
				  	
					$this->M_add_manage->destroy($deleteId);
					$this->M_search_table->destroyOPromotion($deleteId);				
				  
				}
			}
		}

		public function careerConsultant()
	
		{		
		   if(isAuthenticate()){
			$data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;			
			$data['careerInfo']	 = $this->M_carrer_consultant_manage->findAll();
			$this->load->view('controlpanel/CarrerConsultant/careerConsultantPage', $data);
	      }
		}
		
		public function careerStore()
		{
		    $id		    		    	= $this->input->post('id');
			$data['consultancy_title'] 	= $this->input->post('consultancy_title');
			$data['description']		= $this->input->post('description');
			
			if(!empty($id)){
			$this->M_carrer_consultant_manage->update($data, $id);
			} else {
			$this->M_carrer_consultant_manage->save($data);
			}
			$this->careerList_();
		}
		
		public function careerList_()
		{
			$data['careerInfo']	= $this->M_carrer_consultant_manage->findAll();
			$this->load->view('controlpanel/CarrerConsultant/careerListPage', $data);
		}
		
		
		public function careerEdit()
		{
			$id 				= $this->input->post('id');		
			$careditInfo 		= $this->M_carrer_consultant_manage->findById($id);
					
			echo json_encode($careditInfo);
		}
		
		public function careerDelete($id)
		{	
			$this->M_carrer_consultant_manage->destroy($id);
			redirect ('controlpanel/newsAndAdvertiseManage/careerConsultant');
		}
		
		public function programeManage()

		{		
		  if(isAuthenticate()){
			$data['userName']    = getUserName();
			 $data['adminType']    = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;		
			$data['programeInfo']	 = $this->M_programe_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/programeManagePage', $data);
	      }
		}
		
		public function proStore()
		{
		    $id		    		    	= $this->input->post('id');
			$data['programe_name'] 		= $this->input->post('programe_name');
			$data['programe_details']	= $this->input->post('programe_details');
			
			if(!empty($id)){
			  $this->M_programe_manage->update($data, $id);
			} else {
              if($data['programe_name'] !=''){
			    $this->M_programe_manage->save($data);
			  }
			}
			$this->proList_();
		}
		
		public function proList_()
		{
			$data['progInfo']	= $this->M_programe_manage->findAll();
			$this->load->view('controlpanel/newsAndAdvertise/progListPage', $data);
		}
		
		public function progEdit()
		{
			$id 				= $this->input->post('id');		
			$progInfo 			= $this->M_programe_manage->findById($id);
					
			echo json_encode($progInfo);
		}

		public function progDelete()
		{ 		
			$deleteDataList = $this->input->post('programe_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){					
				  $this->M_programe_manage->destroy($deleteId);
				}
			}
		}

		
		public function positonWiseSerial()
			{
				$positon  = $this->input->post('id');
				
				if($positon == 'top'){
				
				echo '<option value="">Select Serial No</option>';
				echo '<option value="'.one.'">'.one.'</option>';
				echo '<option value="'.two.'">'.Two.'</option>';
				echo '<option value="'.three.'">'.Three.'</option>';
				echo '<option value="'.four.'">'.Four.'</option>';
				
				}else if($positon == 'left'){
				
				echo '<option value="">Select Serial No</option>';
				echo '<option value="'.one.'">'.one.'</option>';
				echo '<option value="'.two.'">'.Two.'</option>';
				echo '<option value="'.three.'">'.Three.'</option>';
				echo '<option value="'.four.'">'.Four.'</option>';
				
				}else{
				
				echo '<option value="">Select Serial No</option>';
				echo '<option value="'.one.'">'.one.'</option>';
				echo '<option value="'.two.'">'.Two.'</option>';
				echo '<option value="'.three.'">'.Three.'</option>';
				echo '<option value="'.four.'">'.Four.'</option>';
				echo '<option value="'.five.'">'.Five.'</option>';
				echo '<option value="'.six.'">'.Six.'</option>';
				echo '<option value="'.seven.'">'.Seven.'</option>';
				echo '<option value="'.eight.'">'.Eight.'</option>';
				echo '<option value="'.nine.'">'.Nine.'</option>';
				echo '<option value="'.ten.'">'.Ten.'</option>';
				echo '<option value="'.eleven.'">'.Eleven.'</option>';
				echo '<option value="'.twelve.'">'.Twelve.'</option>';
				echo '<option value="'.thirteen.'">'.Thirteen.'</option>';
				echo '<option value="'.fourteen.'">'.Fourteen.'</option>';
				echo '<option value="'.fifteen.'">'.Fifteen.'</option>';
				echo '<option value="'.sixteen.'">'.Sixteen.'</option>';
				
				
				}
				
								
			}
		

		public function logout()
		{	
			logoutUser();
		}

	

	

}

