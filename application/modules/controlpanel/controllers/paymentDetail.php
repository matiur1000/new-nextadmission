<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PaymentDetail extends MX_Controller {

		static $model 	 = array('M_admin','M_menu_manage', 'M_submenu_manage', 'M_deeper_sub','M_organize_profile_manage');
		static $helper	= array('url', 'authentication');
	
		const  Title	 = 'Next Admission';
		
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('session');
			$this->load->library('pagination');
			isAuthenticate();
	
		}
	

		public function index()
	
		{		
			
		 if(isAuthenticate()){
			$data['userName']    = getUserName();
			$data['adminType']  = $this->session->userdata('adminType');
			$data['page_title']  = self::Title;		
			$data['maxSlNo']	= $this->M_menu_manage->findMaxSl();
			$data['paymentdetails']	= $this->M_organize_profile_manage->findAllMenu('payment_details');
			
			
			$this->load->view('controlpanel/paymentDetails/paymentdetailsPage', $data);
	      }
		}
	
			
			
			
		public function paymentEdit()
		{
			$id = $this->input->post("id");
			$where = array('id' =>$id);
			$result = $this->M_organize_profile_manage->findfindbyorgByUser('payment_details', $where);
			echo json_encode($result);
			
		}
		
		
		public function update()
		{
			$id = $this->input->post("id");
			$data['pmtitle']	= $this->input->post('pmtitle');
			$data['paymentdetails']	= $this->input->post('paymentdetails');
			$this->db->update('payment_details', $data, array('id' => $id));
			 redirect("controlpanel/paymentDetail");
			
		}
		
	
		

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

