<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RegisterMember extends MX_Controller {

		static $model 	 = array('M_admin','M_region_manage','M_country_manage','M_city_manage','M_all_user_registration',
		'M_general_user_registration','M_organization_user_registration','M_search_table','M_organize_profile_manage', 'M_region_manage', 'M_country_manage');
		static $helper	= array('url', 'authentication','allusersms','generalusersms','organizationusersms');
	
		const  Title	 = 'Next Admission';
	
		public function __construct(){
	
			parent::__construct();
	
			$this->load->database();
			$this->load->model(self::$model);
			$this->load->helper(self::$helper);
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->load->library('session');
			$this->load->library('email');
			$this->load->library('pagination');
			isAuthenticate();
	
		}
	

		public function index($onset = 0)
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;

		    $perPageLimit        = $this->input->post('perPageLimit');

			if($perPageLimit !='all'){
				if(!empty($perPageLimit)){
					$setLimit = $perPageLimit; 
				} else {
					$setLimit = "30"; 
				}
				$data['perPageLimit']   = $perPageLimit; 
				$data['regionInfo']		= $this->M_region_manage->findAll();
				$data['allCountryInfo']	= $this->M_country_manage->findAllCountry();
				$data['allRegterInfo']	= $this->M_all_user_registration->findAll(array(), $setLimit, $onset);
				
				$data['onset'] 			= $onset;
				$config['base_url'] 	= base_url('controlpanel/registerMember/index');
				$config['total_rows'] 	= $this->M_all_user_registration->countAll();
				$config['uri_segment'] 	= 4;
				$config['per_page'] 	= $setLimit;
				$config['num_links'] 	= 7;
				$config['first_link']	= FALSE;
				$config['last_link'] 	= FALSE;
				$config['prev_link']	= 'Prev';
				$config['next_link'] 	= 'Next';
		
				$this->pagination->initialize($config); 
			} else {
			  $data['perPageLimit']   	= $perPageLimit; 
			  $data['regionInfo']		= $this->M_region_manage->findAll();
			  $data['allCountryInfo']	= $this->M_country_manage->findAllCountry();
			  $data['allRegterInfo']	= $this->M_all_user_registration->findAllOrganizeData();
			}
			
			$this->load->view('controlpanel/registrationView/organizationRegisterPage', $data);
	      }
		}


		public function countryWiseUser()
		{
			$country_id 		    = $this->input->post('country_id');		
			$data['allRegterInfo'] 	= $this->M_all_user_registration->countryWiseData($country_id);
			$this->load->view('controlpanel/registrationView/countryWiseOrganizeDataPage', $data);						
			
		}

		public function findOrganizeUser()
		{
			$userName 		    	= $this->input->post('userName');		
			$data['allRegterInfo'] 	= $this->M_all_user_registration->findByOrgUserName($userName);
			$this->load->view('controlpanel/registrationView/findOrganizeUserPage', $data);						
			
		}



		public function genRegStore1()
		{	
			
			$data['country_id'] 			= $this->input->post('country_id');
			$data['user_type'] 				= "General user";
			$data['name'] 					= $this->input->post('name');
			$data['user_id'] 				= $this->input->post('user_id');
			$data['password'] 				= $this->input->post('password');
			$user_id 						= $this->input->post('user_id');
			$data['status'] 				= "In Active";
			$regionDetail 					= $this->M_country_manage->findById($data['country_id']);
			$data['region_id'] 				= $regionDetail->region_id;
			
			if($data['user_id']) {
		           $emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
					if(!empty($emil_chk)){
					$this->load->view('email_alert');
				 }else{
				 
					$userData	= array('lastId' => $user_id);
					$this->session->set_userdata($userData);
				
					$gen_id = $this->M_all_user_registration->save($data);

					$orgazeDetails 						= $this->M_all_user_registration->findById($gen_id);
					$datas['res_user_id'] 				= $gen_id;
					$datas['user_type'] 				= $orgazeDetails->user_type;
					$datas['name'] 						= $orgazeDetails->name;
					$datas['region_id'] 				= $regionDetail->region_id;
					$datas['country_id'] 				= $data['country_id'];
					

					$this->M_search_table->save($datas);
						
			
					
				}
				
				  	$title 							= "Verify Account";
					$message						= '<a href="'.site_url('registration/emailVerification/'.$gen_id).'">.Your Account need to verify go to the link for verify.</a>';
					$name					   	 	= "Next Admission";
					$senderEmail	            	= "info@nextadmission.com";
					$userEmail	                	= $user_id;
					
					$config['protocol'] 			= 'sendmail';
					$config['mailpath'] 			= '/usr/sbin/sendmail';
					$config['charset'] 				= 'iso-8859-1';
					$config['wordwrap'] 			= TRUE;
					$config['mailtype'] 			='html';
					
					$this->email->initialize($config);
					
					
					$this->email->from($senderEmail, $name);
					$this->email->to($userEmail);			
					$this->email->subject($title);
					$this->email->message($message);
					$this->email->reply_to($senderEmail);
					$this->email->send();
					
				    $this->registration_list();	
				
			  }
		   }



		   public function orgRegStore()
		   {	
				
			$data['account_name'] 	    	= $this->input->post('account_name');
			$data['name'] 	    			= $this->input->post('organizationname');
			$data['user_id'] 				= $this->input->post('user_id');
			$user_id 						= $this->input->post('user_id');
			$data['password'] 				= $this->input->post('password');
			$data['user_type'] 				= "Organization User";
			$data['status'] 				= "In Active";
			$data['country_id'] 			= $this->input->post('country_id_org');
			$regionDetail 					= $this->M_country_manage->findById($data['country_id']);
			$data['region_id'] 				= $regionDetail->region_id;
			
			
			
			
			 
			$userData	= array('orgLastId' => $user_id);
			$this->session->set_userdata($userData);
			 if($data['account_name'] !=''){
		
				$orgnize_id = $this->M_all_user_registration->save($data);

				$orgazeDetails 					= $this->M_all_user_registration->findById($orgnize_id);
				$datas['res_user_id'] 			= $orgnize_id;
				$datas['user_type'] 			= $orgazeDetails->user_type;
				$datas['name'] 					= $orgazeDetails->name;
				$datas['region_id'] 			= $regionDetail->region_id;
				$datas['country_id'] 			= $data['country_id'];

				$this->M_search_table->save($datas);
				
				
				  
				  	$title 							= "Verify Account";
					$message						= '<a href="'.site_url('registration/emailVerification/'.$orgnize_id).'">.Your Account need to verify go to the link for verify.</a>';
					$name					   	 	= "Next Admission";
					$senderEmail	            	= "info@nextadmission";
					$userEmail	                	= $user_id;
					
					$config['protocol'] 			= 'sendmail';
					$config['mailpath'] 			= '/usr/sbin/sendmail';
					$config['charset'] 				= 'iso-8859-1';
					$config['wordwrap'] 			= TRUE;
					$config['mailtype'] 			='html';
					
					$this->email->initialize($config);
					
					
					$this->email->from($senderEmail, $name);
					$this->email->to($userEmail);			
					$this->email->subject($title);
					$this->email->message($message);
					$this->email->reply_to($senderEmail);
					$this->email->send();
				
				}		
				$this->registration_list();	
		  
	   }

	   public function registration_list()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;		
			$data['countryInfo']	= $this->M_country_manage->findAllCountry();
			$data['allRegterInfo']	= $this->M_all_user_registration->findAllRegData();	
			$this->load->view('controlpanel/registrationView/memberRegistrationListPage', $data);
	      }
	
		}

		public function csvUploade($onset = 0)
	
		{
			$data['page_title']  	= self::Title;	
			 $data['adminType']     = $this->session->userdata('adminType');
			$data['suceess']        = $this->session->userdata('suceess');
		    $this->session->set_userdata(array('suceess' => ""));
			$data['allCsvRegInfo']   = $this->M_all_user_registration->findAllCsvData($onset);


			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/registerMember/csvUploade');
			$config['total_rows'] 	= $this->M_all_user_registration->countAllCsv();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 10;
			$config['num_links'] 	= 10;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
			$this->pagination->initialize($config); 	
			$this->load->view('controlpanel/registrationView/csvUploadePage', $data);
		}

		public function csvfileUploadAction()
		{

		  	$config['upload_path'] 		= './resource/csvUpload/';
			$config['allowed_types'] 	= '*';
			$config['max_size']			= '0';
			$config['file_name']		= time();
			$file_field					= 'csvfile';
			
			$this->upload->initialize($config);
			if($this->upload->do_upload('csvfile')){
			   $uploadData = $this->upload->data();
			   $data['csv_file'] = $uploadData['file_name'];

			}

			$lastId 		 = $this->M_organize_profile_manage->save('resgistration_csv_upload', $data);
	        $lastFileDetail  = $this->M_organize_profile_manage->findById('resgistration_csv_upload', $lastId); 
			// open the csv file
			$fp = fopen(base_url("resource/csvUpload/$lastFileDetail->csv_file"),"r");

			//parse the csv file row by row
			$i = 0;
			while(($row = fgetcsv($fp,"500",",")) != FALSE)
			{
				
			      $data2['name']       = $row[0];
			      //echo"<br>";
				   $data2['region_id']  = $row[1];
				  //echo"<br>";
				  $data2['country_id'] = $row[2];
				 //echo"<br>";
				 $data2['user_type']  = "Organization User";
				 $data2['status']     = "Active";
				 $data2['dataCsv']    = "yes";
                    if($i>0){
					  if($data2['name'] != ''){
					  	$gen_id = $this->M_all_user_registration->save($data2);

					  	$orgazeDetails 						= $this->M_all_user_registration->findById($gen_id);
						$countryDetail 						= $this->M_country_manage->findById($orgazeDetails->country_id);

						$datas['res_user_id'] 				= $gen_id;
						$datas['user_type'] 				= $orgazeDetails->user_type;
						$datas['name'] 						= $orgazeDetails->name;	
						if(!empty($orgazeDetails->region_id)){				
						  $datas['region_id'] 			    = $orgazeDetails->region_id;					
						}
						if(!empty($orgazeDetails->country_id)){
						  $datas['country_id'] 				= $orgazeDetails->country_id;
					    }
					     $datas['country_university'] 	= $countryDetail->country_name ." university";
					     $this->M_search_table->save($datas);
					  }
					}
					
				      $this->session->set_userdata(array('suceess' => "Csv file uploade successfully"));
					$i++;

			}
	          fclose($fp);
			  
			  redirect('controlpanel/registerMember/csvUploade');
		}


		public function allCsvUploade()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;	
			$data['suceess']        = $this->session->userdata('suceess');
		    $this->session->set_userdata(array('suceess' => ""));
			$data['allFileInfo']    = $this->M_organize_profile_manage->findAllData('csv_file_upload'); 	
			$this->load->view('controlpanel/registrationView/allCsvUploadePage', $data);
		  }
		} 



		public function allCsvfileUploadAction()
		{
			$id 		    			= $this->input->post('id');
			$data['type_name'] 		    = $this->input->post('type_name');
		  	$config['upload_path'] 		= './resource/csvUpload/';
			$config['allowed_types'] 	= '*';
			$config['max_size']			= '0';
			$config['file_name']		= time();
			
			$this->upload->initialize($config);
			if($this->upload->do_upload('csvfile')){
				$uploadData = $this->upload->data();
				$data['csv_file'] = $uploadData['file_name'];
			}
            if(!empty($id)){
			  $csvEditInfo 	   		= $this->M_organize_profile_manage->findById('csv_file_upload', $id);
			  if(!empty($csvEditInfo->csv_file)){
				 unlink('./resource/csvUpload/'.$csvEditInfo->csv_file);
			   }
			   $this->M_organize_profile_manage->update2('csv_file_upload', $id, $data);
			   $this->session->set_userdata(array('suceess' => "Update successfully"));

            } else{
			  $this->M_organize_profile_manage->save('csv_file_upload', $data);
			  $this->session->set_userdata(array('suceess' => "Csv file upload successfully"));

            }
		   redirect('controlpanel/registerMember/allCsvUploade');
		}



		public function csvEdit()
		{
			$id 			   		   = $this->input->post('id');		
			$data['csvEditInfo'] 	   = $this->M_organize_profile_manage->findById('csv_file_upload', $id);

			$this->load->view('controlpanel/registrationView/allCsvEditPage', $data);
					
			
		}


		
		
		
		   
		
		public function generalRegister($onset = 0)
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;		
			$data['regionInfo']		= $this->M_region_manage->findAll();
			$data['allRegterInfo']	= $this->M_all_user_registration->findAllGeneral(array(), $onset);
			
			$data['onset'] 			= $onset;
			$config['base_url'] 	= base_url('controlpanel/registerMember/generalRegister');
			$config['total_rows'] 	= $this->M_all_user_registration->countAll2();
			$config['uri_segment'] 	= 4;
			$config['per_page'] 	= 5;
			$config['num_links'] 	= 7;
			$config['first_link']	= FALSE;
			$config['last_link'] 	= FALSE;
			$config['prev_link']	= 'Prev';
			$config['next_link'] 	= 'Next';
	
	
			$this->pagination->initialize($config); 
			$this->load->view('controlpanel/registrationView/generalRegisterPage', $data);
		  }
		}
		
		
		public function userWiseDetails($id)
	
		{		
		 if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;		
			$data['userWiseFullInfo']	= $this->M_all_user_registration->findAllValu($id);	
			$this->load->view('controlpanel/registrationView/userWiseDetailsPage', $data);
	      }
		}
		
		public function orgUserWiseDetails($id)
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;		
			$data['userWiseFullInfo']	= $this->M_all_user_registration->findAllorgValu($id);	
			$this->load->view('controlpanel/registrationView/orgUserWiseDetailsPage', $data);
	      }
		}
		
		public function orgUserWiseMail($id)
	
		{		
			$userWiseFullInfo	        = $this->M_all_user_registration->findAllValu($id);	
			$userEmail	                = $userWiseFullInfo->user_id;	
			$title 						= $this->input->post('title');
			$message					= $this->input->post('message');
			$name					    = "Next Admission";
			$senderEmail	            = "info@nextadmission.com";
			
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			
			$this->email->from($senderEmail, $name);
			$this->email->to($userEmail);			
			$this->email->subject($title);
			$this->email->message($message);
			$this->email->reply_to($senderEmail);
			
				if($this->email->send())
				{
				  $this->load->view('controlpanel/registrationView/email_send_alert');
				}
				 else
				{
				  show_error($this->email->print_debugger());
				} 
	        
		}


		public function genUserWiseMail($id)
	
		{		
			$userWiseFullInfo	        = $this->M_all_user_registration->findAllValu($id);	
			$userEmail	                = $userWiseFullInfo->user_id;	
			$title 						= $this->input->post('title');
			$message					= $this->input->post('message');
			$name					    = "Find Global Study";
			$senderEmail	            = "ayenal754@gmail.com";
			
			$name					    = "Find Global Study";
			$senderEmail	            = "ayenal754@gmail.com";
			
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			
			$this->email->from($senderEmail, $name);
			$this->email->to($userEmail);			
			$this->email->subject($title);
			$this->email->message($message);
			$this->email->reply_to($senderEmail);
	      
		 if($this->email->send())
			 {
			  $this->load->view('controlpanel/registrationView/general_email_send_alert');
			 }
			 else
			{
			  show_error($this->email->print_debugger());
			} 
	        
		}
		
		public function organizationUserMailAction()
	
		{		
			$title						= $this->input->post('title2');
			$message					= $this->input->post('msg');
			$userId                     = $this->input->post('user_id');

			foreach($userId as $userAutoId) {
			  if($userAutoId){
				$mailList			= $this->M_all_user_registration->findById($userAutoId);	
				$name			    = "Next Admission";
				$senderEmail	    = "info@nextadmission.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] 	= 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($mailList->user_id);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				$this->email->send();
				  
				}
			} 

		}



		public function generalUserMailAction()
	
		{		
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');
			$mailList					= $this->M_all_user_registration->findAllGeneralUserEmail();	
			
			foreach($mailList as $v){
				$userEmail  = $v->user_id;

				$name					    = "Find Global Study";
				$senderEmail	            = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				}
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/registrationView/general_email_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}

		public function allUserMailAction()
	
		{		
			$userType					= $this->input->post('user_type');
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');

			if($userType == 'general'){
			 $mailList					= $this->M_all_user_registration->findAllGeneralUserEmail();					
			}else{
			 $mailList					= $this->M_all_user_registration->findAllOrganizeUser();					
			}
			
			foreach($mailList as $v){
				$userEmail  = $v->user_id;

				$name					    = "Find Global Study";
				$senderEmail	            = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				}
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/mail/all_mail_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}

		public function generalUserMailAction2()
	
		{		
			$userEmail					= $this->input->post('user_type');
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');


				$name					= "Find Global Study";
				$senderEmail	        = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] 	= 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/mail/general_mail_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}


		public function organizationUserMailAction2()
	
		{		
			$userEmail					= $this->input->post('user_type');
			$title						= $this->input->post('title');
			$message					= $this->input->post('message');

				$name					    = "Find Global Study";
				$senderEmail	            = "ayenal754@gmail.com";
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				
				
				$this->email->from($senderEmail, $name);
				$this->email->to($userEmail);			
				$this->email->subject($title);
				$this->email->message($message);
				$this->email->reply_to($senderEmail);
				
			 if($this->email->send())
				 {
				  $this->load->view('controlpanel/mail/organization_mail_send_alert');
				 }
				 else
				{
				  show_error($this->email->print_debugger());
				} 
			 
	        
		}

		public function allSmsSend()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;	
			$this->load->view('controlpanel/sms/allUsertSmsPage', $data);
	      }
		}
		
		public function smsSendGeneralUserWise()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;		
			$data['smsinfo']   	 = $this->M_all_user_registration->findByGeneralUser($user_id);
			
			$this->load->view('controlpanel/sms/smsSendGeneralUserWisePage', $data);
	      }
		}
		
		public function smsSendOrganizationUserWise()
	
		{		
		 if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;
			$data['smsinfo']   		= $this->M_all_user_registration->findByOrganizatinUser($user_id);	
		 	$this->load->view('controlpanel/sms/smsSendOrganizationUserWisePage', $data);
	      }
		}
		public function allUserMailSend()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;
			$this->load->view('controlpanel/mail/allUsertMailPage', $data);
	      }
		}
		public function generalUserMailSend()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;
			$data['mailList']	    = $this->M_all_user_registration->findAllGeneralUserEmail();					
			$this->load->view('controlpanel/mail/generalUserMailPage', $data);
	      }
		}
		public function organizationUserMailSend()
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;
			$data['mailList']		= $this->M_all_user_registration->findAllOrganizeUser();					
			$this->load->view('controlpanel/mail/organizationUserMailPage', $data);
	      } 
		}
		
		public function allUserSmsAction()
			{
				sendSms();
			}
			
		public function smsSendGeneralWiseAction()
			{
				generalSendSms();
			}
			
		public function organizationUserSmsAction()
			{
				organizationSendSms();
			}
		
		public function memberRegistration($onset = 0)
	
		{		
		  if(isAuthenticate()){
		    $data['userName']    = getUserName();
			 $data['adminType']  = $this->session->userdata('adminType');
		    $data['page_title']  = self::Title;

		    $perPageLimit           = $this->input->post('perPageLimit');
			if($perPageLimit !='all'){
				if(!empty($perPageLimit)){
					$setLimit = $perPageLimit; 
				} else {
					$setLimit = "30"; 
				}
				$data['perPageLimit']   = $perPageLimit; 
				$data['regionInfo']		= $this->M_region_manage->findAll();
			  	$data['countryInfo']	= $this->M_country_manage->findAllCountry();
				$data['allRegterInfo']	= $this->M_all_user_registration->findAllUserRegData(array(), $setLimit, $onset);
				
				$data['onset'] 			= $onset;
				$config['base_url'] 	= base_url('controlpanel/registerMember/memberRegistration');
				$config['total_rows'] 	= $this->M_all_user_registration->countAll();
				$config['uri_segment'] 	= 4;
				$config['per_page'] 	= $setLimit;
				$config['num_links'] 	= 7;
				$config['first_link']	= FALSE;
				$config['last_link'] 	= FALSE;
				$config['prev_link']	= 'Prev';
				$config['next_link'] 	= 'Next';
		
				$this->pagination->initialize($config); 
			} else {
			  $data['perPageLimit']   	= $perPageLimit; 
			  $data['regionInfo']		= $this->M_region_manage->findAll();
			  $data['countryInfo']		= $this->M_country_manage->findAllCountry();
			  $data['allRegterInfo']	= $this->M_all_user_registration->findAllRegData();	
			}
			 $this->load->view('controlpanel/registrationView/memberRegistrationPage', $data);
	      } 
		}
		
		public function store()
			{	
				$id		    		    		= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				$image		 				= $this->input->post('register_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
					$register_info = $this->M_all_user_registration->findById($id);
					
					if( !empty($register_info->image) && file_exists('./Images/Register_image/'.$register_info->image) && !empty($image) ) {					
						unlink('./Images/Register_image/'.$register_info->image);	
					}
					$this->M_all_user_registration->update($data, $id);
					$this->registerList_();
					} else {	
					if($data['user_id']) {
							$emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
							if(!empty($emil_chk)){
							$this->load->view('controlpanel/registrationView/email_alert', $data);
							}else{			
						$this->M_all_user_registration->save($data);
						$this->registerList_();
				  }
				
				}
			   }
				 
			}
			
			
			
			public function orgUpdate()
			{	
				$id		    		    		= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				$image		 				= $this->input->post('register_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
					$register_info = $this->M_all_user_registration->findById($id);
					
					if( !empty($register_info->image) && file_exists('./Images/Register_image/'.$register_info->image) && !empty($image) ) {					
						unlink('./Images/Register_image/'.$register_info->image);	
					}
					$this->M_all_user_registration->update($data, $id);
					$this->orgList_();
					} else {	
					if($data['user_id']) {
							$emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
							if(!empty($emil_chk)){
							$this->load->view('controlpanel/registrationView/email_alert', $data);
							}else{			
						$this->M_all_user_registration->save($data);
						$this->orgList_();
				  }
				
				}
			   }
				 
			}
			
			
			public function genUpdate()
			{	
				$id		    		    		= $this->input->post('id');
				$data['region_id'] 				= $this->input->post('region_id');
				$data['country_id'] 			= $this->input->post('country_id');
				$data['city_id'] 				= $this->input->post('city_id');
				$data['user_type'] 				= $this->input->post('user_type');
				$data['name'] 					= $this->input->post('name');
				$data['date_of_birth'] 			= $this->input->post('date_of_birth');
				$data['father_name'] 			= $this->input->post('father_name');
				$data['mother_name'] 			= $this->input->post('mother_name');
				$data['nationality'] 			= $this->input->post('nationality');
				$data['address'] 				= $this->input->post('address');
				$data['city'] 					= $this->input->post('city');
				$data['country_phone'] 			= $this->input->post('country_phone');
				$data['mobile'] 				= $this->input->post('mobile');
				$data['email'] 					= $this->input->post('email');
				$data['address_permanent'] 		= $this->input->post('address_permanent');
				$data['city_permanent'] 		= $this->input->post('city_permanent');
				$data['country_phone_permanent'] = $this->input->post('country_phone_permanent');
				$data['mobile_permanent'] 		= $this->input->post('mobile_permanent');
				$data['email_permanent'] 		= $this->input->post('email_permanent');
				$data['user_id'] 				= $this->input->post('user_id');
				$data['password'] 				= $this->input->post('password');
				$data['status'] 				= $this->input->post('status');
				
				$image		 				= $this->input->post('register_image');
			
				if(!empty($image)){
					$data['image'] = $image;	
				}
				
				if(!empty($id)) {
					$register_info = $this->M_all_user_registration->findById($id);
					
					if( !empty($register_info->image) && file_exists('./Images/Register_image/'.$register_info->image) && !empty($image) ) {					
						unlink('./Images/Register_image/'.$register_info->image);	
					}
					$this->M_all_user_registration->update($data, $id);
					$this->genList_();
					} else {	
					if($data['user_id']) {
							$emil_chk = $this->M_all_user_registration->findByEmail($data['user_id']);
							if(!empty($emil_chk)){
							$this->load->view('controlpanel/registrationView/email_alert', $data);
							}else{			
						$this->M_all_user_registration->save($data);
						$this->genList_();
				  }
				
				}
			   }
				 
			}
			
			public function genList_()
			{
				$data['allRegterInfo']	= $this->M_all_user_registration->findAllGeneral($where);
				$this->load->view('controlpanel/registrationView/genList', $data);
			}
			
			public function registerList_()
			{
				$data['allRegterInfo']	= $this->M_all_user_registration->findAll();	
				$this->load->view('controlpanel/registrationView/registerList', $data);
			}
			
			public function orgList_()
			{
				$data['allRegterInfo']	= $this->M_all_user_registration->findAll($where);
				$this->load->view('controlpanel/registrationView/orgList', $data);
			}
			
			public function edit()
			{
				$id 						= $this->input->post('id');		
				$regEditInfo 				= $this->M_all_user_registration->findById($id);
				$regEditInfo->countryList 	= $this->M_country_manage->findAllInfo($regEditInfo->region_id);
				$regEditInfo->cityList 		= $this->M_city_manage->findAllCity($regEditInfo->country_id);
						
				echo json_encode($regEditInfo);
			}
			
			
			public function organizEdit()
			{
				$id 						= $this->input->post('id');		
				$regEditInfo 				= $this->M_all_user_registration->findById($id);
				$regEditInfo->countryList 	= $this->M_country_manage->findAllInfo($regEditInfo->region_id);
				$regEditInfo->cityList 		= $this->M_city_manage->findAllCity($regEditInfo->country_id);
						
				echo json_encode($regEditInfo);
			}
			
			public function genEdit()
			{
				$id 						= $this->input->post('id');		
				$regEditInfo 				= $this->M_all_user_registration->findById($id);
				$regEditInfo->countryList 	= $this->M_country_manage->findAllInfo($regEditInfo->region_id);
				$regEditInfo->cityList 		= $this->M_city_manage->findAllCity($regEditInfo->country_id);
						
				echo json_encode($regEditInfo);
			}
			
		

		    
		  public function countryName()
			{
				$region_id 	   = $this->input->post('id');
				
				//$where 			   = array('region_id' => $region_id);		
				$countryList   	   = $this->M_country_manage->findAllInfo($region_id);				
			
				echo '<option value="">Sellect Country Name</option>';
				foreach($countryList as $v) {
					echo '<option value="'.$v->id.'" data-country-code="'.$v->country_code.'" data-nationality="'.$v->nationality.'">'.$v->country_name.'</option>';
				}
				
			}
			
			public function cityName()
			{
				$country_id 	      = $this->input->post('id');
				
				//$where 			   	  = array('country_id' => $country_id);		
			 	$countryWiseCityList   = $this->M_city_manage->findAllCity($country_id);				
			
				echo '<option value="">Sellect City Name</option>';
				foreach($countryWiseCityList as $v) {
					echo '<option value="'.$v->id.'">'.$v->city_name.'</option>';
				}
								
			}

			public function regDelete()
			{ 		
				$deleteDataList = $this->input->post('user_id');
				
				foreach($deleteDataList as $deleteId) {
					if($deleteId){	
				      $model	= $this->M_all_user_registration->findById($deleteId);
				      if(!empty($model->image)){
						unlink('./Images/Register_image/'.$model->image);
					  }	
                       $this->M_all_user_registration->destroy($deleteId);
					   $this->M_search_table->destroyUser($deleteId);
					 
					}
				}
			}
			
	      
		 
		
		  public function csvDelete($id)
			{
				$model	  = $this->M_organize_profile_manage->findById('csv_file_upload', $id);
				$csvFile  = $model->csv_file;
							
				if(!empty($csvFile)){
				unlink('./resource/csvUpload/'.$csvFile);
				}	
				$this->M_organize_profile_manage->destroy('csv_file_upload', $id);
				redirect ('controlpanel/registerMember/allCsvUploade');
			}

		public function orgDelete()
		{ 		
			$deleteDataList = $this->input->post('user_id');
			
			foreach($deleteDataList as $deleteId) {
				if($deleteId){	
				  $model	= $this->M_all_user_registration->findById($deleteId);
				  if(!empty($model->image)){
				     unlink('./Images/Register_image/'.$model->image);
				  }	
				   $this->M_all_user_registration->destroy($deleteId);
				}
			}
		}
	
		public function genDelete($id)
		{
			$model	= $this->M_all_user_registration->findById($id);
			$only_image  = $model->image;
						
			if(!empty($only_image)){
			unlink('./Images/Register_image/'.$only_image);
			}	
			$this->M_all_user_registration->destroy($id);
			 redirect ('controlpanel/registerMember/generalRegister');
		}
	

		public function logout()
	
		{	
	
			logoutUser();
	
		}

	

	

}

