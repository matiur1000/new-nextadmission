<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">

		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">News And Event Manage</li>
						</ul><!-- /.breadcrumb -->
						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i> 	 </a>
										</div>
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>	</th>
														<th>Company Name </th>														
														<th class="hidden-480">News Title </th>
														<th class="hidden-480">Image</th>
														<th class="hidden-480">User Id </th>
														<th class="hidden-480">Status</th>
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($newsInfo);
												 $i = 0;
												  foreach ($newsInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="news_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span></label></td>

														<td>
															<a href="#"><?php echo $v->company_name; ?></a></td>														
														<td class="hidden-480"><?php echo $v->title; ?></td>
														<td class="hidden-480"><img src="<?php echo base_url("Images/News_image/$v->image"); ?>" height="50" width="50" /></td>
														<td class="hidden-480"><?php echo $v->user_id; ?></td>
														<td class="hidden-480"><?php echo $v->status; ?></td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i></a>
																</div>

															</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<form id="addForm" action="<?php echo site_url('controlpanel/newsAndAdvertiseManage/store'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
								<div id="modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog" style="width:700px;">
										<div class="modal-content" c>
											<div class="modal-header" style="border-bottom:3px solid #FF0000">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">News And Event Manage</h4>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12">
														

														<div class="form-group">
															<label for="company_name">Company Name</label>

															<div>
																<input type="text" name="company_name" id="company_name" placeholder="Company Name" tabindex="1" 
																class="form-control" required  />
															</div>
														</div>
														
														<div class="form-group">
															<label for="title">Title</label>

															<div>
																<input type="text" name="title" id="title" placeholder="Title" tabindex="1" 
																class="form-control" required  />
															</div>
														</div>
														<div class="form-group">
															<label for="date">Date</label>
															<input name="date" type="text" id="date" placeholder="Date" class="form-control date-picker"
															 data-date-format="yyyy-mm-dd" />
														  </div>
														<div class="row">
															<div class="col-sm-12">
															  <div class="form-group">
																<label for="news_image">Image Upload</label>
																   <div>
																	 <div class="attachmentbody" data-target="#news_image" data-type="news_image">
																		<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
																	  </div> 
																	   <input name="news_image" id="news_image" type="hidden" value="" required />                                                                                
																	</div>
																 </div>
															  </div>
														 </div>

														<div class="space-4"></div>
														<div class="form-group">
														  <label for="status">Status</label>        
															<div>
															   <select class="form-control" id="status" name="status"  tabindex="7" required >
																	<option value="" selected>Select Status</option>
																	<option value="aprove">Aprove</option>
																	<option value="un aprove">Un Aprove</option>
															   </select>
															</div>
														</div>
														<div class="form-group">
														  <label for="description">Description</label>        
															<div>
															   <textarea class="form-control" rows="20" placeholder="*..Description" 
															   tabindex="8" name="description" id="description"></textarea>
															</div>
														  </div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Cancel
												</button>

												<button class="btn btn-sm btn-primary" type="submit">
													<i class="ace-icon fa fa-check"></i>
													<span class="update">Save</span>
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
								</form>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
<script>

        $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="news_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="news_id[]"]').prop('checked', false);
           }
        });
        
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});


		$('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#company_name').val(data.company_name);
					$('#title').val(data.title);
					$('#description').val(data.description);
					$('#date').val(data.date);
					$('#status').val(data.status);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});



		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="news_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

		$(document).on("click", ".blue", function(e){
           $("#addForm").find("input[type=text], textarea").val("");
           $('.update').text("Save");
		});
	</script>
	</body>
</html>
