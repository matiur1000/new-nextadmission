<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">

		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  

		<style>
			.bookMsg{
				font-family:Arial, Helvetica, sans-serif;
				font-weight:800;
				color:red;
			}
		</style>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Promotion Manage </a>
							</li>
							
						</ul><!-- /.breadcrumb -->
						
					</div>
<style>
.pagi a {
	color: black;
 	border: 1px solid #ddd;
	padding: 8px 16px 8px !important;
	border-radius : 0px !important;
}

.pagi strong {
	background-color: #4CAF50;
    color: white;
	padding: 8px 16px;
    border: 1px solid #4CAF50;
}
</style>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i> 	 </a>
										</div>
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center" width="2">SN</th>
														<th width="10">Organization ID</th>														
														<th class="hidden-480">Org name</th>
														<th class="hidden-480">Org Country</th>
														<th class="hidden-480">Title</th>
														<th class="hidden-480" width="5">position</th>
														<th class="hidden-480" width="5">Serial</th>
														<th class="hidden-480">start date</th>
														<th class="hidden-480">end date</th>
														<th class="hidden-480" width="8">Images</th>
														<th>Status</th>
													</tr>
												</thead>

												<tbody>
												
													<?php 
														$i=1;
													
													if(isset($addinfo)) {
													 $sn = $onset + 1;
													 foreach($addinfo as $v)
													 {
													 	$addimage = $v->add_image; 
													 ?>
													<tr>
														<td class="center"><?php echo $i++;?></td>
														<td><a href="#" class="green" data-id="<?php echo $v->id; ?>"><?php echo $v->user_id; ?></a></td>														
														<td class="hidden-480"><?php echo $v->name; ?></td>
														<td class="hidden-480"><?php echo $v->country_name; ?></td>
														<td class="hidden-480"><?php echo $v->title; ?></td>
														<td class="hidden-480"><?php echo $v->positon; ?></td>
														<td class="hidden-480"><?php echo $v->serial_no; ?></td>
														<td class="hidden-480"><?php echo $v->publish_date; ?></td>
														<td class="hidden-480"><?php echo $v->expire_date; ?></td>
								<td class="hidden-480">
									<?php if(!empty($addimage)){?> 
									<img src="<?php echo base_url("Images/Add_image/" . $addimage); ?>" style="max-height:50px; max-width:50px;">
									<?php } else {?> 
									<div class="embed-responsive embed-responsive-16by9">
				  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $v->video_embed; ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
									</div>
									<?php }?>
								</td>
														
														
														<td>
															<div>
																<a  href="#">Approve</a>
																</div>

															</td>
													</tr>
													<?php } } ?>
												</tbody>
											</table>
											
											<div class="row" align="right" style="padding-right:20px;">
												 <div id="page">
														<label class="pos-rel"><span class="lbl"></span></label>
														<ul class="pagination-sm list-inline text-center">
															  <li class="pagi"><?php echo $this->pagination->create_links(); ?></li>
														</ul>
												 </div>
											</div>
											
										</div>
									</div>
								</div>
								
								
								
								
								<form id="addForm" action="<?php echo site_url('controlpanel/promotionManage/store'); ?>" method="post" enctype="multipart/form-data">
								<div id="modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog" style="width:700px;">
										<div class="modal-content">
											<div class="modal-header" style="border-bottom:3px solid #FF0000">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Promotion Ad</h4>
											</div>

											<div class="modal-body" style="height:490px; overflow:scroll;">
												<div class="row">
													<div class="col-xs-12 col-sm-12">
														

														
														<div class="form-group">
														  <label for="status">Organization Id</label>        
															<div>
															   <select class="form-control" id="orgId" class="orgId" name="orgId"  tabindex="7" required>
																<option value="" selected>Select Organization Id</option>
																<?php foreach($allorguser as $v){?> 
																<option value="<?php echo $v->id; ?>"><?php echo $v->user_id; ?></option>
																<?php }?>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group">
															<label for="title">Organization Name</label>
															<div>
																<input type="text" name="orgname" id="orgname" placeholder="" tabindex="1" 
																class="form-control" required disabled="disabled" />
															</div>
														</div>
														
														
														<div class="form-group">
															<label for="title">Organization Country</label>
															<div>
															<input type="text" name="orgcounty" id="orgcounty" placeholder="Organization Country" tabindex="1" 
																class="form-control" required  disabled="disabled"/>
															</div>
														</div>
														
														<div class="form-group">
														  <label for="status">Select Website</label>        
															<div>
															   <select class="form-control" id="website" name="website"  tabindex="7" required>
																<option value="" selected>Select Website</option>
																 <option value="main">Main Website</option>
																 <option value="country">Country Website</option>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group countryweb" style="display:none;">
														  <label for="status">Publish Country</label>        
															<div>
															   <select class="form-control" id="publishcounty" name="publishcounty"  tabindex="7">
																<option value="" selected>Select Publish Country</option>
																	<?php foreach($contryname as $v){?> 
																	  <option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
																	<?php } ?>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group">
															<label for="title">Ad Title</label>
															<div>
															<input type="text" name="addtitle" id="addtitle" placeholder="Add Title" tabindex="1" 
																class="form-control" required/>
															</div>
														</div>
														
														
														<div class="form-group">
														  <label for="status">Select position</label>        
															<div>
															   <select class="form-control posishon" id="position" name="position"  tabindex="7" required>
																	<option value="" selected>Select Position</option>
																	<option value="top">Top</option>
																	<option value="left">Left</option>
																	<option value="right">Right</option>
															   </select>
															</div>
														</div>
														
														
														
														<div class="form-group top" style="display:none;">
														  <label for="status">Position Serial</label>        
															<div>
															   <select class="form-control" id="positionSerial" name="positionSerial"  tabindex="7">
																	<option value="" selected>Position Serial</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group left" style="display:none;">
														  <label for="status">Position Serial</label>        
															<div>
															   <select class="form-control" id="positionSerialL" name="positionSerialL"  tabindex="7">
																	<option value="" selected>Position Serial</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
															   </select>
															</div>
														</div>
														
														
														<div class="form-group right" style="display:none;">
														  <label for="status">Position Serial</label>        
															<div>
															   <select class="form-control" id="positionSerialR" name="positionSerialR"  tabindex="7">
																	<option value="" selected>Position Serial</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																	<option value="13">13</option>
																	<option value="14">14</option>
																	<option value="15">15</option>
																	<option value="16">16</option>
																	<option value="17">17</option>
																	<option value="18">18</option>
																	<option value="19">19</option>
																	<option value="20">20</option>
															   </select>
															</div>
														</div>
														
														
														
														
														<div class="form-group">
															<label for="date">Ad Start Date</label>
															<input name="startDeta" type="text" id="startDeta" placeholder="Date" class="form-control date-picker"
															 data-date-format="yyyy-mm-dd" />
														  </div>
														  
														  <div class="form-group">
															<label for="date">Ad End Date</label>
															<input name="endDeta" type="text" id="endDeta" placeholder="Date" class="form-control date-picker endDeta"
															 data-date-format="yyyy-mm-dd" /><span class="bookMsg"></span>
														  </div>
														  
														  
														  
														 <div class="form-group">
														  <label for="status">Ad Video?</label>        
															<div>
															   <select class="form-control" id="embednadimage" name="embednadimage"  tabindex="7">
																	<option value="" selected>Select Yes or No</option>
																	<option value="yes">Yes</option>
																	<option value="no">No</option>
															   </select>
															</div>
														</div>
														  
														  
														<div class="row">
															<div class="col-sm-12">
															  <div class="form-group images" style="display:none;">
																<label for="news_image">Image Upload</label>
																   <div>
																	 <div class="attachmentbody" data-target="#add_image" data-type="add_image">
																		<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
																	  </div> 
																	   <input name="add_image" id="add_image" type="hidden" value="" required />                                                                                
																	</div>
																 </div>
															  </div>
														 </div>


														<div class="form-group embedcode" style="display:none;">
														  <label for="description">Embed Code</label>        
															<div>
															   <textarea class="form-control" rows="5" placeholder="Embed Code" 
															   tabindex="3" name="embed_link" id="embed_link"></textarea>
															</div>
														  </div>
														

													
														<div class="form-group">
														  <label for="status">Ad url?</label>        
															<div>
															   <select class="form-control" id="addurl" name="addurl"  tabindex="7">
																	<option value="" selected>Select Yes or No</option>
																	<option value="yes">Yes</option>
																	<option value="no">No</option>
															   </select>
															</div>
														</div>




														<div class="form-group weblink" style="display:none;">
															<label for="title">Web link</label>
															<div>
															<input type="text" name="addlink" id="addlink" placeholder="Web link" tabindex="1" 
																class="form-control"/>
															</div>
														</div>


														<div class="space-4"></div>
														
														
														<div class="form-group description" style="display:none;">
														  <label for="description">Description</label>        
															<div>
															   <textarea class="form-control" rows="20" placeholder="*..Description" 
															   tabindex="8" name="description" id="description"></textarea>
															</div>
														  </div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Cancel
												</button>

												<button class="btn btn-sm btn-primary" type="submit">
													<i class="ace-icon fa fa-check"></i>
													<span class="update">Submit</span>
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
								</form>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
<script>

        $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="news_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="news_id[]"]').prop('checked', false);
           }
        });
   
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});


		$('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/promotionManage/proEdit'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					$('#id').val(data.id);
					$('#orgId').val(data.user_id);
					
					
					$('#title').val(data.title);
					$('#description').val(data.description);
					$('#date').val(data.date);
					
					$('#description').val(data.description);
					
					if(description == data.description){
						$(".description").css('display', 'block');
					}
					
					
				}
			});
			
			e.preventDefault();
		});


		
		
		
		
		$("#orgId").on('change', function(){
			var orgId = $(this).val();
			console.log(orgId);
			var formURL = "<?php echo site_url('controlpanel/promotionManage/orgidbyname'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {orgId: orgId},
				success:function(data){
					$('#orgname').val(data);
						
				}
			});
			
		});
		
		
		$("#orgId").on('change', function(){
			var orgId = $(this).val();
			console.log(orgId);
			var formURL = "<?php echo site_url('controlpanel/promotionManage/orgidbycontryname'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {orgId: orgId},
				success:function(data){
					$('#orgcounty').val(data);
						
				}
			});
			
		});
		

	//start position

		$(".posishon").on('change', function(){
			var posishon = $(this).val();
			if(posishon == 'top'){
				$('.top').css("display","block");
				$('.left').css("display","none");
	
				$('.right').css("display","none");
			} else if(posishon == 'left'){
				$('.left').css("display","block");
				$('.top').css("display","none");
				$('.right').css("display","none");
			}else{
			$('.right').css("display","block");
			$('.top').css("display","none");
			$('.left').css("display","none");
			}
	});
	
	
	$("#addurl").on('change', function(){
		var addurl = $(this).val();
		if(addurl == 'yes'){
			$('.weblink').css('display', 'block');
			$('.description').css('display', 'none');
		}else {
			$('.description').css('display', 'block');
			$('.weblink').css('display', 'none');
		}
		
	});
	
	
	$("#embednadimage").on('change', function(){
		var embednadimage = $(this).val();
		
		if(embednadimage == 'yes'){
			$(".embedcode").css('display', 'block');
			$(".images").css('display', 'none');
		}
		else {
			$(".images").css('display', 'block');
			$(".embedcode").css('display', 'none');
		}
		
	});
	
	
	
	
	$("#website").on('change', function(){
		var website = $(this).val();
		
		if(website == 'country'){
			$('.countryweb').css("display","block");
		}else if(website == 'main'){
			$('.countryweb').css("display","none");
		}
	
		
	})
	
	
//end position



	$("#endDeta").on('change', function(){
		var endDeta = $("#endDeta").val();
		var startDeta = $("#startDeta").val();
		var website = $("#website").val();
		var publishcounty = $("#publishcounty").val();
		var position = $("#position").val();
		var positionSerial = $("#positionSerial").val();
		var positionSerialL = $("#positionSerialL").val();
		var positionSerialR = $("#positionSerialR").val();
		var formURL = "<?php echo site_url('controlpanel/promotionManage/chckadddate'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {endDeta : endDeta, startDeta : startDeta, website : website, publishcounty : publishcounty, position : position, positionSerial : positionSerial, positionSerialL : positionSerialL, positionSerialR : positionSerialR},
				success:function(data){
					if(data == 1){
						$(".bookMsg").text("Sorry! the ad is booked");
						$('.submit').attr('disabled', 'disabled');
					}else {
					$(".bookMsg").text("");
					$('.submit').removeAttr('disabled');
					}
				}
			});

	});





		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/newsDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="news_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

		$(document).on("click", ".blue", function(e){
           $("#addForm").find("input[type=text], textarea").val("");
           
		});
	</script>
	</body>
</html>
