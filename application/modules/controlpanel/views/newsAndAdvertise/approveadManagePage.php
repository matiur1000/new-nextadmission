<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->
        
	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							
								<li class="active">Approved Ad</li>
						
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            
										</div>
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="24" class="center">Sl No</th>
														
														<th width="100">Company Name </th>														
														<th width="249" class="hidden-480">Title</th>
														<th width="50" class="hidden-480">Ad Image  </th>
														<th width="53" class="hidden-480">Position</th>
														<th width="33" class="hidden-480">Sl No </th>
														<th width="48" class="hidden-480">Status</th>
														<th width="46">Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($addInfo);
												 $i = 1;
												  foreach ($addInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center"><?php echo $i++ ?></td>
														<td><?php echo $v->name; ?></td>														
														<td class="hidden-480"><?php echo $v->	title; ?></td>
														<td class="hidden-480"><img src="<?php echo base_url("Images/Add_image/$v->add_image"); ?>" height="50" width="60" /></td>
														<td class="hidden-480"><?php echo $v->positon; ?></td>
														<td class="hidden-480"><?php echo $v->serial_no; ?></td>
														<td class="hidden-480"><?php echo $v->status; ?></td>
														<td><a class="green" href="#" data-id="<?php echo $id ?>">Approved</a></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<form id="addForm" action="<?php echo site_url('controlpanel/newsAndAdvertiseManage/addStore'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
								<div id="modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Add Manage</h4>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12 col-sm-11">
														

														<div class="form-group">
															<label for="company_name">Company Name</label>

															<div>
																<input type="text" name="company_name" id="company_name" placeholder="Company Name" tabindex="1" 
																class="form-control" required  />
															</div>
														</div>
														
														<div class="form-group">
															<label for="title">Title</label>

															<div>
																<input type="text" name="title" id="title" placeholder="Title" tabindex="1" 
																class="form-control" required  />
															</div>
														</div>
														<div class="form-group">
														  <label for="positon">Add Positon</label>        
															<div>
															   <select class="form-control" id="positon" name="positon"  tabindex="7" required >
																	<option value="" selected>Select Add Positon</option>
																	<option value="top">Top</option>
																	<option value="left">Left</option>
																	<option value="right">Right</option>
															   </select>
															</div>
														</div>
														
														<div class="form-group">
														  <label for="serial_no">Serial No</label>        
															<div>
															   <select class="form-control" id="serial_no" name="serial_no"  tabindex="7" required >
																	<option value="" selected>Select Serial No</option>
                                                                  	<option value="1">one</option>
                                                                    <option value="2">Two</option>
                                                                    <option value="3">Three</option>
                                                                    <option value="4">Four</option>
                                                                    <option value="5">Five</option>
                                                                    <option value="6">Six</option>
                                                                    <option value="7">Seven</option>
                                                                    <option value="8">Eight</option>
                                                                    <option value="9">Nine</option>
                                                                    <option value="10">Ten</option>
                                                                    <option value="11">Eleven</option>
                                                                    <option value="12">Twelve</option>
                                                                    <option value="13">Thirteen</option>
                                                                    <option value="14">Fourteen</option>
                                                                    <option value="15">Fifteen</option>
                                                                    <option value="16">Sixteen</option>
																	<option value="17">seventeen</option>
                                                                    <option value="18">Eighteen</option>
                                                                    <option value="19">Nineteen</option>
                                                                    <option value="20">Twenty</option>
															   </select>
															</div>
														</div>
														
														<div class="row">
															<div class="col-sm-12">
															  <div class="form-group">
																<label for="com_logo">Add Image</label>
																   <div>
																	 <div class="attachmentbody" data-target="#add_img" data-type="add_img">
																		<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
																	  </div> 
																	   <input name="add_img" id="add_img" type="hidden" value="" required />                                                                                
																	</div>
																 </div>
															  </div>
														 </div>

														<div class="space-4"></div>
														<div class="form-group">
														  <label for="status">Status</label>        
															<div>
															   <select class="form-control" id="status" name="status"  tabindex="7" required >
																	<option value="" selected>Select Status</option>
																	<option value="aprove">Aprove</option>
																	<option value="un aprove">Un Aprove</option>
															   </select>
															</div>
														</div>
														<div class="form-group">
															<label for="title">Add Url</label>

															<div>
																<input type="text" name="add_link" id="add_link" placeholder="Add Url" tabindex="1" 
																class="form-control" />
															</div>
														</div>
														<div class="form-group">
														  <label for="description">Description</label>        
															<div>
															   <textarea class="form-control" rows="20" placeholder="*..Description" 
															   tabindex="8" name="description" id="description"></textarea>
															</div>
														  </div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Cancel
												</button>

												<button class="btn btn-sm btn-primary" type="submit">
													<i class="ace-icon fa fa-check"></i>
													Save
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
								</form>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>

<script>
		$(document).on("click", ".green", function(e)
		{
			var id 			= $(this).attr("data-id");
			var formURL 	= "<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedAdEdit'); ?>";
			var successUrl  = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedAd'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				success:function(data){
				  location.reload(); 
				  location.replace(successUrl);
					
				}
			});
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/newsAndAdvertiseManage/addDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>
	</body>
</html>
