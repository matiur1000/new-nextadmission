<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

    <!--SHARE THIS
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
        -->
        
	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							
								<li class="active">Approved Ad</li>
						
							
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            
										</div>
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="24" class="center">Sl No</th>
														<th width="100">Company Name </th>														
														<th width="249" class="hidden-480">Title</th>
														<th width="50" class="hidden-480">News Image</th>
														<th width="50" class="hidden-480">News video</th>
														<th width="48" class="hidden-480">Status</th>
														<th width="46">Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($addInfo);
												 $i = 1;
												  foreach ($newsInfo as $v){
												     $id     = $v->id;
												     $pieces = explode(" ", $v->embeded_code);
													 $pieces[3]; 
												?>
													<tr>
														<td class="center"><?php echo $i++ ?></td>
														<td><?php echo $v->name; ?></td>														
														<td class="hidden-480"><?php echo $v->title; ?></td>
														<td class="hidden-480">
														   <?php if(!empty($v->image)){ ?>
					                                        <img src="<?php echo base_url("/Images/News_image/$v->image"); ?>" height="50" width="100" />
					                                       <?php }else{ ?>
					                                        <img src="<?php echo base_url("/resource/img/noimage.png"); ?>" height="50" width="100" />
					                                       <?php } ?>
														 </td>
														<td class="hidden-480">
														      <?php 
					                                           if(!empty($v->video) || !empty($v->embeded_code)){ 
					                                              if(!empty($v->video)){
					                                          ?>
						                                       <video width="150" height="100" controls>
																	<source src="<?php echo base_url("/Images/News_image/$v->video"); ?>" type="video/mp4">
															   </video> 
						                                      <?php }else { ?>
						                                        <iframe width="150" height="100" <?php echo $pieces[3]; ?> frameborder="0" allowfullscreen></iframe>
						                                      <?php } } else { ?> 
					                                        	<img src="<?php echo base_url("/resource/img/novideo.png"); ?>" height="50" width="100" />
						                                      <?php } ?>   
														</td>
														<td class="hidden-480"><?php echo $v->status; ?></td>
														<td><a class="green" href="#" data-id="<?php echo $id ?>">Approved</a></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>

<script>
		$(document).on("click", ".green", function(e)
		{
			var id 			= $(this).attr("data-id");
			var formURL 	= "<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedNewsEdit'); ?>";
			var successUrl  = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/approvedNews'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				success:function(data){
				  location.reload(); 
				  location.replace(successUrl);
					
				}
			});
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/newsAndAdvertiseManage/addDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>
	</body>
</html>
