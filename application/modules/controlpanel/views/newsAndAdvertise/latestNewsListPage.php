								<table id="inPut" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<td width="10%" class="center">
												<label class="pos-rel">
													<input type="checkbox" class="ace allCheck" />
													<span class="lbl"></span></label>
													<label class="pos-rel">
													<a class="red" href="#" data-id="<?php echo $id ?>">
														<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
													<span class="lbl"></span></label>

													  

											</td>
											<td width="80%">News Title  </td>														
											<td width="10%">Action</td>
										</tr>
									</thead>

									<tbody>
									<?php 
									// print_r($breakingNewsInfo);
									 $i = 0;
									  foreach ($latestNewsInfo as $v){
									     $id  		    = $v->id;
									?>
										<tr>
											<td class="center" width="10%">
												<label class="pos-rel">
													<input type="checkbox" name="news_id[]" class="ace" value="<?php echo $id ?>" />
													<span class="lbl"></span></label></td>

											<td>
												<a href="#"><?php echo $v->title; ?></a></td>														
											<td>
												<div class="hidden-sm hidden-xs action-buttons">
													<a class="green" href="#" data-id="<?php echo $id ?>">
														<i class="ace-icon fa fa-pencil bigger-130"></i></a>
													</div>

												</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
<script>

       $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="news_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="news_id[]"]').prop('checked', false);
           }
       });
        
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL  = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);	
					//$(this).find("input[type=text], textarea").val("");			
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
				}
			});
		  }
			
			e.preventDefault();
		});


		$('.date-picker').datepicker({
			autoclose: true	  
		});	
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/latestNewsEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#title').val(data.title);
					$('#details').val(data.details);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
		
			$('.red').on('click', function(e) {
		       var deleteDta = $("#inputTable input").serializeArray();
		       console.log(deleteDta);
				$.ajax({
				url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/latestNewsDelete') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					  location.reload();
					  $("#inputTable").find('input[name="news_id[]"],.allCheck').prop('checked', false);
				   }
			    });

			   e.preventDefault();
				
			});

				$(document).on("click", ".blue", function(e){
                   $("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>