<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('resource/assets/css/datepicker.min.css'); ?>" rel="stylesheet">

		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/assets/js/bootstrap-datepicker.min.js'); ?>"></script>  

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">Latest News Manage</li>
						</ul><!-- /.breadcrumb -->
						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i> 	 </a>
										</div>
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											 <table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<td width="10%" class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>

																  

														</td>
														<td width="80%">News Title  </td>														
														<td width="10%">Action</td>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($breakingNewsInfo);
												 $i = 0;
												  foreach ($latestNewsInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center" width="10%">
															<label class="pos-rel">
																<input type="checkbox" name="news_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span></label></td>

														<td>
															<a href="#"><?php echo $v->title; ?></a></td>														
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i></a>
																</div>

															</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<form id="addForm" action="<?php echo site_url('controlpanel/newsAndAdvertiseManage/latestNewsStore'); ?>" method="post">
								<input type="hidden" name="id" id="id" value="" />
								<div id="modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog" style="width:700px;">
										<div class="modal-content" c>
											<div class="modal-header" style="border-bottom:3px solid #FF0000">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Latest News Manage</h4>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12">
														<div class="form-group">
															<label for="title">Title</label>

															<div>
																<input type="text" name="title" id="title" placeholder="Title" tabindex="1" 
																class="form-control" required  />
															</div>
														</div>
														
														<div class="form-group">
														  <label for="description">Description</label>        
															<div>
															   <textarea class="form-control" rows="15" placeholder="*..Description" 
															   tabindex="8" name="details" id="details"></textarea>
															</div>
														  </div>
														</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Cancel
												</button>

												<button id="save" class="btn btn-sm btn-primary" type="submit">
													<i class="ace-icon fa fa-check"></i>
													<span class="update">Save</span>
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
								</form>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
    <?php $this->load->view('formJsLinkPage'); ?>
    <script>

       $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="news_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="news_id[]"]').prop('checked', false);
           }
       });
        
		//callback handler for form submit
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			e.preventDefault();
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm input[type='text'], input[type='hidden'], #addForm textarea").val("");
					
				}
			});
			
			
		});

	


		$('.date-picker').datepicker({
			autoclose: true	  
		});	

		$('#title').keyup(function(){
			var button = $('#save');
			button.prop('disabled', false);
		});
		
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/newsAndAdvertiseManage/latestNewsEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#title').val(data.title);
					$('#details').val(data.details);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
		
			$('.red').on('click', function(e) {
		       var deleteDta = $("#inputTable input").serializeArray();
		       console.log(deleteDta);
				$.ajax({
				url: "<?php echo site_url('controlpanel/newsAndAdvertiseManage/latestNewsDelete') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					  $("#inputTable").find('input[name="news_id[]"],.allCheck').prop('checked', false);
					  location.reload();
				   }
			    });

			   e.preventDefault();
				
			});

				$(document).on("click", ".blue", function(e){
                   $("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>
	</body>
</html>
