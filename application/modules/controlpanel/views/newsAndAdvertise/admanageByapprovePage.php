<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center" width="2">SN</th>
														<th width="10">Organization ID</th>														
														<th class="hidden-480">Org name</th>
														<th class="hidden-480">Org Country</th>
														<th class="hidden-480">Publish Country </th>
														<th class="hidden-480" width="5">position</th>
														<th class="hidden-480" width="5">Serial</th>
														<th class="hidden-480">Title</th>
														<th class="hidden-480">start date</th>
														<th class="hidden-480">end date</th>
														<th class="hidden-480" width="8">Images</th>
														<th>Status</th>
													</tr>
												</thead>

												<tbody>
												
													<?php 
														$i=1;
												
													 foreach($addvierind as $v)
													 {
													 	$addimage = $v->add_image;
														$published_status = $v->published_status;
														$id  = $v-id;
													 ?>
													<tr>
														<td class="center"><?php echo $i++;?></td>
														<td><a href="#" class="green" data-id="<?php echo $v->id; ?>"><?php echo $v->user_id; ?></a></td>														
														<td class="hidden-480"><?php echo $v->name; ?></td>
														<td class="hidden-480"><?php echo $v->d1name; ?></td>
														<td class="hidden-480"><?php echo $v->d2name; ?></td>
														<td class="hidden-480"><?php echo $v->positon; ?></td>
														<td class="hidden-480"><?php echo $v->serial_no; ?></td>
														<td class="hidden-480"><?php echo $v->title; ?></td>
														<td class="hidden-480"><?php echo $v->publish_date; ?></td>
														<td class="hidden-480"><?php echo $v->expire_date; ?></td>
								<td class="hidden-480">
									<?php if(!empty($addimage)){?> 
									<img src="<?php echo base_url("Images/Add_image/" . $addimage); ?>" style="max-height:50px; max-width:50px;">
									<?php } else {?> 
									<div class="embed-responsive embed-responsive-16by9">
				  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $v->video_embed; ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
									</div>
									<?php }?>
								</td>
														
														
														<td>
															<?php if($published_status == 'unaproave') {?> 
															 <div>
																<a  class="approves" href="#" data-id="<?php echo $id ?>">Approve</a>
														    </div>
															<?php } else {?> <div>Approved</div><?php }?>
														</td>
													</tr>
													<?php  } ?>
												</tbody>
											</table>