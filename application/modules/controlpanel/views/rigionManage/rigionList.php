<table id="inputTable"  class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th class="center" width="10%">
					<label class="pos-rel">
						<input type="checkbox" class="ace allCheck" />
						<span class="lbl"></span></label>
						<label class="pos-rel">
						<a class="red" href="#">
							<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
						<span class="lbl"></span></label>														</th>
				<th>Region  Name</th>														
				<th width="10%">Action</th>
			</tr>
		</thead>

		<tbody>
		<?php 
		// print_r($regionInfo);
		 $i = 0;
		  foreach ($regionInfo as $v){
		     $id  		    = $v->id;
		?>
			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" name="region_id[]" class="ace" value="<?php echo $id ?>" />
						<span class="lbl"></span>															</label>														</td>

				<td>
					<a href="#"><?php echo $v->region_name; ?></a>														</td>														
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="#" data-id="<?php echo $id ?>">
							<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

			</tr>
			<?php } ?>
		</tbody>
	</table>
	<script>
	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="region_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="region_id[]"]').prop('checked', false);
           }
       });
        
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/regionManage/regionEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_name').val(data.region_name);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
		
	    $('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/regionManage/regiondelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="region_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});
				
		$(document).on("click", ".blue", function(e){
           $("#addForm").find("input[type=text], textarea").val("");
           $('.update').text("Save");
		});
		
	</script>