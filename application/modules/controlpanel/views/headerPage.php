<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="<?php echo site_url("controlpanel/home"); ?>" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							<?php echo $adminType; ?>
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo base_url("resource/logo/logo.jpg"); ?>" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo $adminType; ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#" data-toggle="modal" data-target="#myModal">
										<i class="ace-icon fa fa-key"></i>
										Change password
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<?php echo site_url('controlpanel/login/logout'); ?>">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
		
		
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
      </div>
      <div class="modal-body">
       <form action="<?php echo base_url("controlpanel/home/passUpdate"); ?>" method="post">
			<div class="form-group">
				<label for="exampleInputPassword1">Old Password</label>
				<input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Password" required><span class="oldpassmgs" style="font-weight:800; color:red;"></span>
			  </div>
			  
			<div class="form-group">
				<label for="exampleInputPassword1">New Password</label>
				<input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="New Password" required>
			  </div>
	  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary submit">Submit</button>
      </div>
	  </form>
    </div>
  </div>
</div>

