<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
			


	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">Payment Collection </li>
						</ul><!-- /.breadcrumb -->
						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														
														<th width="5">SN</th>
														<th width="86">User Type</th>
														<th width="86">Payment Type</th>
														<th width="86">Bank Account Number</th>
														<th width="86">Account Name</th>
														<th width="86">Bkash Number</th>
														<th width="86">Amount</th>
														<th width="86">Payment Date</th>
														<th width="86">Status</th>											
														<th width="42">Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($menuInfo);
                                                 $i = 1;
												  foreach ($paymentdetails as $v)
												  
												  {
												     $id  		    = $v->id;
												?>
													<tr>
														
														<td><?php echo $i++ ?></td>
														<td><?php echo $v->user_type; ?></td>
														<td><?php echo $v->payment_by; ?></td>
														<td><?php echo $v->bankaccountnumber; ?></td>
														<td><?php echo $v->bankaccountname; ?></td>
														<td><?php echo $v->bashphoneNumber; ?></td>
														<td><?php echo $v->amount; ?></td>
														<td><?php echo $v->paymentdate; ?></td>
														<td><?php echo $v->payment_status; ?></td>
														<td>
															<div class="hidden-sm">
															<?php if($v->payment_status == 'unpaid') {?> 
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	Approve
																</a>
															<?php } else {?> 
															approved
															<?php }?>
																</div>
																
															</td>
													</tr>
													<?php }  ?>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
														
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	<script>
	 
	 
	 $(".green").on('click', function(){
	 	var id = $(this).attr("data-id");
		var paymenturl = "<?php echo site_url("controlpanel/paymentcollection"); ?>";
		var aprroveurl = "<?php echo site_url("controlpanel/paymentcollection/approve"); ?>";
		$.ajax(
		{
			url: aprroveurl,
			type:"POST",
			data:{id:id},
			success:function(data){
			 location.reload(paymenturl);
			}
		});
		
	 });




       $('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/menuManage/menudelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="menu_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

				
		$(document).on("click", ".blue", function(e){
           $('.update').text("Save");
		});
				
	</script>
	</body>
</html>
