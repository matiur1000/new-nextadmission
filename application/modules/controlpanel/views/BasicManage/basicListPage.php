<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span></label></th>
			<th>Title</th>														
			<th class="hidden-480">Logo  Image</th>
			<th class="hidden-480">Glob Image</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
	<?php 
	// print_r($basicInfo);
	 $i = 0;
	  foreach ($basicInfo as $v){
		 $id  		    = $v->id;
	?>
		<tr>
			<td class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span></label></td>

			<td>
				<a href="#"><?php echo $v->title; ?></a></td>														
			<td class="hidden-480"><img src="<?php echo base_url("Images/Basic_image/$v->logo_image"); ?>" alt="" width="50" height="50" /></td>
			<td class="hidden-480"><img src="<?php echo base_url("Images/Basic_image/$v->globe_image"); ?>" height="50" width="50" /></td>
			<td>
				<div class="hidden-sm hidden-xs action-buttons">
					<a class="green" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-pencil bigger-130"></i></a>

					<a class="red" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>

				<div class="hidden-md hidden-lg">
					<div class="inline pos-rel">
						<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
							<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>

						<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
							<li>
								<a class="green" href="#" data-id="<?php echo $id ?>">
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>

							<li>
								<a class="red" href="#" data-id="<?php echo $id ?>">
									<span class="red">
										<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
						</ul>
					</div>
				</div></td>
		</tr>
		<?php } ?>
	</tbody>
</table>