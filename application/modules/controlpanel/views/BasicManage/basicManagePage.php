<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
        
	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">Basic manage </li>
						</ul><!-- /.breadcrumb -->

					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										
										
										<!-- div.dataTables_borderWrap -->
										<div id="listView">
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<span class="lbl"></span></label></th>
														<th>Title</th>														
														<th class="hidden-480"> LogoImage</th>
														<th class="hidden-480"> Globe Image</th>
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($basicInfo);
												 $i = 0;
												  foreach ($basicInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span></label></td>

														<td>
															<a href="#"><?php echo $v->title; ?></a></td>														
														<td class="hidden-480"><img src="<?php echo base_url("Images/Basic_image/$v->logo_image"); ?>" alt="" width="50" height="50" /></td>
														<td class="hidden-480"><img src="<?php echo base_url("Images/Basic_image/$v->globe_image"); ?>" height="50" width="50" /></td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i></a>

																</div>

															<div class="hidden-md hidden-lg">
																<div class="inline pos-rel">
																	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>

																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																		<li>
																			<a class="green" href="#" data-id="<?php echo $id ?>">
																				<span class="green">
																					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>

																		<li>
																			<a class="red" href="#" data-id="<?php echo $id ?>">
																				<span class="red">
																					<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
																	</ul>
																</div>
															</div></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<form id="addForm" action="<?php echo site_url('controlpanel/basicManage/store'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
								<div id="modal-form" class="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header" style="border-bottom:3px solid #FF0000">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="blue bigger">Basic Information Manage</h4>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12">
														

														<div class="form-group">
															<label for="title">Title</label>

															<div>
																<input type="text" name="title" id="title" placeholder="Title" tabindex="1" 
																class="form-control" required  />
															</div>
														</div>

														<div class="space-4"></div>

														<div class="form-group">
															<label for="footer_description">Footer Description</label>

															<div>
																<textarea name="footer_description" id="footer_description" placeholder="Footer Description" tabindex="2" 
																class="form-control" required></textarea>
															</div>
														</div>

														<div class="space-4"></div>
														<div class="row">
														  <div class="col-sm-3">
														  <div class="form-group">
															<label for="logo_image">Logo Image</label>
															   <div class="controls">
																	<div class="attachmentbody" data-target="#logo_image" data-type="logo_image">
                                                                    <img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
                                                                </div> 
                                                                
                                                                <input name="logo_image" id="logo_image" type="hidden" value="" />
																</div>
														     </div>
														  </div>
                                                          
                                                          <div class="col-sm-3" align="left">
														  <div class="form-group">
															<label for="globe_image">Globe Image</label>
															   <div class="controls">
																	<div class="attachmentbody" data-target="#globe_image" data-type="globe_image">
                                                                    <img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
                                                                </div> 
                                                                
                                                                <input name="globe_image" id="globe_image" type="hidden" value="" />
																</div>
														     </div>
														  </div>
                                                          <div class="col-sm-6" align="left">
														  </div>
														</div>
                                                        
													  <div class="form-group">
															<label for="image_title">Image Title</label>

															<div>
																<input type="text" name="image_title" id="image_title" placeholder="Image Title" tabindex="5" 
																class="form-control" required  />
															</div>
														</div>
													
													   <div class="form-group">
															<label for="image_alt">Image Alt</label>

															<div>
																<input type="text" name="image_alt" id="image_alt" placeholder="Image Alt" tabindex="6" 
																class="form-control" required  />
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="modal-footer">
												<button class="btn btn-sm" data-dismiss="modal">
													<i class="ace-icon fa fa-times"></i>
													Cancel
												</button>

												<button class="btn btn-sm btn-primary" type="submit">
													<i class="ace-icon fa fa-check"></i>
													<span class="update">Save</span>
												</button>
											</div>
										</div>
									</div>
								</div><!-- PAGE CONTENT ENDS -->
								</form>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>

<script>
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/basicManage/edit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#title').val(data.title);
					$('#footer_description').val(data.footer_description);
					$('#image_type').val(data.image_type);
					$('#basic_image').val(data.basic_image);
					$('#image_title').val(data.image_title);
					$('#image_alt').val(data.image_alt);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/basicManage/delete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});

				$(document).on("click", ".blue", function(e){
                   $("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>
	</body>
</html>
