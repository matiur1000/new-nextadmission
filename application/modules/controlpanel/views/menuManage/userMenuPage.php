<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">User Menu Manage </li>
						</ul><!-- /.breadcrumb -->

					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable"  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>														</th>
														<th>Menu Position </th>
														<th>Menu Name</th>														
														<th class="hidden-480">Sub menu Status</th>
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($adminTypeInfo);
												 $i = 0;
												  foreach ($menuInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="user_menu_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span>															</label>														</td>

														<td><?php echo $v->menu_position; ?></td>
														<td>
															<?php echo $v->menu_name; ?>														</td>														
														<td class="hidden-480"><?php echo $v->sub_menu_status; ?></td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

															</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
																					</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
								<form id="addForm" action="<?php echo site_url('controlpanel/menuManage/userMenustore'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="border-bottom:3px solid #FF0000">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">User Menu Manage</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                                <div class="col-xs-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label for="sl_no">Sl No</label>        
                                                                        <div>
                                                                           <input type="text" id="sl_no" placeholder="Sl No" name="sl_no"
                                                                            tabindex="1" value="<?php echo $maxSlNo; ?>" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
                                                                    <div class="space-4"></div>
                                                                    <div class="form-group">
                                                                        <label for="menu_position">Menu position</label>
            
                                                                        <div>
                                                                           <select class="form-control" id="menu_position" name="menu_position"  tabindex="2" required >
                                                                                <option value="" selected>Select position</option>
                                                                                <option value="Profile">Profile menu</option>
                                                                                <option value="Panel">Panel menu</option>
                                                                           </select>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="space-4"></div>     

                                                                    <div class="form-group">
                                                                        <label for="menu_name">Menu Name</label>        
                                                                        <div>
                                                                           <input type="text" id="menu_name" placeholder="Menu Name" name="menu_name"
                                                                            tabindex="2" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>

                                                                     <div class="space-4"></div>
            
                                                                    <div class="form-group">
                                                                        <label for="status">Sub Menu Status</label>
            
                                                                        <div>
                                                                           <select class="form-control" id="sub_menu_status" name="sub_menu_status"  tabindex="2" required >
                                                                                <option value="" selected>Select Status</option>
                                                                                <option value="Yes">Yes</option>
                                                                                <option value="No">No</option>
                                                                           </select>
                                                                        </div>
                                                                    </div>        
                                                                </div>
                                                            </div>
                                                        </div>
            
                                                        <div class="modal-footer">
                                                            <button class="btn btn-sm" data-dismiss="modal">
                                                                <i class="ace-icon fa fa-times"></i>
                                                                Cancel
                                                            </button>
            
                                                            <button class="btn btn-sm btn-primary" type="submit">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                <span class="update">Save</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
   <?php $this->load->view('formJsLinkPage'); ?>
	<script>

	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="user_menu_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="user_menu_id[]"]').prop('checked', false);
           }
       });
        
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], select, textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/menuManage/userMenuedit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#sl_no').val(data.sl_no);
					$('#menu_position').val(data.menu_position);
					$('#menu_name').val(data.menu_name);
					$('#sub_menu_status').val(data.sub_menu_status);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
		

		$('.red').on('click', function(e) {
		       var deleteDta = $("#inputTable input").serializeArray();
		       console.log(deleteDta);
				$.ajax({
				url: "<?php echo site_url('controlpanel/menuManage/userMenudelete') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					  $("#inputTable").find('input[name="user_menu_id[]"],.allCheck').prop('checked', false);
					  location.reload();
				   }
			    });

			   e.preventDefault();
				
			});

				$(document).on("click", ".blue", function(e){
                   //$("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>
	</body>
</html>
