<table id="inputTable"  class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace allCheck" />
					<span class="lbl"></span></label>
					<label class="pos-rel">
					<a class="red" href="#">
						<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					<span class="lbl"></span></label>														</th>
			<th>Menu Name</th>														
			<th class="hidden-480">Sub Menu Name </th>
			<th class="hidden-480">Status </th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>
	<?php 
	// print_r($adminTypeInfo);
	 $i = 0;
	  foreach ($menuInfo as $v){
	     $id  		    = $v->id;
	?>
		<tr>
			<td class="center">
				<label class="pos-rel">
					<input type="checkbox" name="manager_id[]" class="ace" value="<?php echo $id ?>" />
					<span class="lbl"></span>															</label>															</td>

			<td>
				<?php echo $v->menu_name; ?>														</td>														
			<td class="hidden-480"><?php echo $v->sub_menu_name; ?></td>
			<td class="hidden-480"><?php echo $v->status; ?></td>
			<td>
				<div class="hidden-sm hidden-xs action-buttons">
					<a class="green" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-pencil bigger-130"></i>																</a>

					<a class="red" href="#" data-id="<?php echo $id ?>">
						<i class="ace-icon fa fa-trash-o bigger-130"></i>																</a>															</div>

				<div class="hidden-md hidden-lg">
					<div class="inline pos-rel">
						<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
							<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>																	</button>

						<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
							<li>
								<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>																				</span>																			</a>																		</li>

							<li>
								<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
									<span class="red">
										<i class="ace-icon fa fa-trash-o bigger-120"></i>																				</span>																			</a>																		</li>
						</ul>
					</div>
				</div></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<script>

	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="manager_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="manager_id[]"]').prop('checked', false);
           }
       });


		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], select, textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/menuManage/managerSubMenuedit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#sl_no').val(data.sl_no);
					$('#menu_id').val(data.menu_id);
					$('#sub_menu_name').val(data.sub_menu_name);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});


		$('.red').on('click', function(e) {
		       var deleteDta = $("#inputTable input").serializeArray();
		       console.log(deleteDta);
				$.ajax({
				url: "<?php echo site_url('controlpanel/menuManage/managerSubMenudelete') ?>",
				method: "POST",	
				data: $("#inputTable input").serializeArray(),
				dataType: "html",
				success: function(data){
					  $("#inputTable").find('input[name="manager_id[]"],.allCheck').prop('checked', false);
					  location.reload();
				   }
			    });

			   e.preventDefault();
				
			});
		
		
				

				$(document).on("click", ".blue", function(e){
                   //$("#addForm").find("input[type=text], textarea").val("");
                   $('.update').text("Save");
				});
	</script>