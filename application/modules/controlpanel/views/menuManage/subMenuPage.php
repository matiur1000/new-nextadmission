<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script src="<?php echo base_url('resource/jscripts/tiny_mce/tiny_mce.js'); ?>"></script>
		<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "exact",
			elements : "ajaxfilemanager",
			//full url
			relative_urls : "false",
		    remove_script_host : false,
            convert_urls : false,
			//end full url,
			theme : "advanced",
			setup : function(ed) {
			      ed.onKeyUp.add(function(ed, l) {
			         tinyMCE.triggerSave();	                    
			      });
			},
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",

			theme_advanced_buttons1_add_before : "newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
			theme_advanced_buttons2_add_before: "cut,copy,separator,",
			theme_advanced_buttons3_add_before : "",
			theme_advanced_buttons3_add : "media",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "ajaxfilemanager",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true
		});

		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php");
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: SAWEB.getBaseAction("resource/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php"),
                width: 482,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
		}
	</script>


	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Web Site Manage </a>
							</li>
							<li class="active">Sub Menu Manage </li>
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">											
                                            <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">  <i class="ace-icon fa fa-plus"></i>  </a>
                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable"  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
														 <label class="pos-rel">
																<input type="checkbox" class="ace allCheck" />
																<span class="lbl"></span></label>
																<label class="pos-rel">
																<a class="red" href="#">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
																<span class="lbl"></span></label>
														</th>
														<th>Sn</th>
														<th>Menu Name </th>
														<th>Sub Menu Name </th>
														<th>Title</th>
														<th>Position </th>
														<th>Image</th>
														<th>Status</th>														
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
												<?php 
												// print_r($menuInfo);
                                                 $i = 1;
												  foreach ($subMenuInfo as $v){
												     $id  		    = $v->id;
												?>
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" name="sub_id[]" class="ace" value="<?php echo $id ?>" />
																<span class="lbl"></span></label></td>

														<td><a href="#"><?php echo $i++; ?></a></td>
														<td><a href="#"><?php echo $v->menu_name; ?></a></td>
														<td><a href="#"><?php echo $v->sub_menu_name; ?></a></td>
														<td><a href="#"><?php echo $v->sub_menu_title; ?></a></td>
														<td><a href="#"><?php echo $v->position; ?></a></td>
														<td><img src="<?php echo base_url("Images/Sub_menu_image/$v->image"); ?>" height="50" width="50" /></td>
														<td>
															<a href="#"><?php echo $v->status; ?></a></td>														
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="green" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-pencil bigger-130"></i></a>

																<a class="red" href="#" data-id="<?php echo $id ?>">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>

															<div class="hidden-md hidden-lg">
																<div class="inline pos-rel">
																	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>

																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																			<span class="green">
																				<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>																	</span>																			</a></li>
																		<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="ace-icon fa fa-trash-o bigger-120"></i>																	</span>																			</a></li>
																	</ul>
																</div>
															</div></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
								<form id="addForm" action="<?php echo site_url('controlpanel/menuManage/subMenuStore');?>" method="post">
								<input type="hidden" name="id" id="id" value="" />
                                            <div id="modal-form" class="modal" tabindex="-1">
                                                <div class="modal-dialog" style="width:730px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="border-bottom:3px solid #FF0000">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="blue bigger">Sub Menu Manage</h4>
                                                        </div>
            
                                                        <div class="modal-body">
                                                            <div class="row"> 
                                                                <div class="col-xs-12 col-lg-12">
                                                                    <div class="form-group">
                                                                        <label for="sl_no">Sl No</label>        
                                                                        <div>
                                                                           <input type="text" id="sl_no" placeholder="Sl No" name="sl_no"
                                                                            tabindex="1" value="<?php echo $maxSlNo; ?>" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
                                                                        <label for="position_footer">Select Position</label>        
                                                                        <div>
                                                                           <select class="form-control" id="select_position" name="select_position"  tabindex="5" required >
                                                                                <option value="" selected>Select Position</option>
																				<option value="top">Top</option>
                                                                                <option value="footer">Footer</option>
                                                                           </select>
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
                                                                        <label for="sub_menu_status">Select Menu</label>        
                                                                        <div>
                                                                           <select class="form-control" id="menu_id" name="menu_id"  tabindex="6" required >
                                                                                <option value="" selected>Select Menu</option>
                                                                           </select>
                                                                        </div>
                                                                    </div>
																	
																	<div class="form-group">
                                                                        <label for="menu_name">Sub Menu Name</label>        
                                                                        <div>
                                                                           <input type="text" id="sub_menu_name" placeholder="Sub Menu Name" name="sub_menu_name"
                                                                            tabindex="2" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	<div class="row">
																		<div class="col-sm-12">
																		  <div class="form-group">
																			<label for="sub_image">Image Upload</label>
																			   <div>
																				 <div class="attachmentbody" data-target="#sub_image" data-type="sub_image">
																					<img class="upload" src="<?php echo base_url('resource/img/no_image.png') ?>" />
																				  </div> 
																				<input name="sub_image" id="sub_image" type="hidden" value="" required />                                                                                
																				</div>
																		     </div>
																		  </div>
																	 </div>
																	<div class="form-group">
                                                                        <label for="menu_title">Sub Menu Title</label>        
                                                                        <div>
                                                                           <input type="text" id="sub_menu_title" placeholder="Sub Menu Title" name="sub_menu_title"
                                                                            tabindex="3" class="form-control" required /> 
																			
                                                                        </div>
                                                                    </div>
																	
																	
																	<div class="form-group">
                                                                        <label for="sub_menu_status">Deeper Sub Status</label>        
                                                                        <div>
                                                                           <select class="form-control" id="deeper_sub_status" name="deeper_sub_status"  tabindex="6" required >
                                                                                <option value="" selected>Select Sub Menu Status</option>
                                                                                <option value="Available">Available</option>
                                                                                <option value="Not Available">Not Available</option>
                                                                           </select>
                                                                        </div>
                                                                    </div>
																	<div class="form-group">
                                                                        <label for="status">Status</label>        
                                                                        <div>
                                                                           <select class="form-control" id="status" name="status"  tabindex="7" required >
                                                                                <option value="" selected>Select Status</option>
                                                                                <option value="Available">Available</option>
                                                                                <option value="Not Available">Not Available</option>
                                                                           </select>
                                                                        </div>
                                                                    </div>
																	
																	<div class="form-group">
                                                                        <label for="menu_description">Description</label>        
                                                                        <div>
                                                                           <textarea class="form-control" rows="20" placeholder="*..Description" 
																		   tabindex="8" name="sub_menu_description" id="ajaxfilemanager" style="width:680px; height:350px;"></textarea>
																			
                                                                        </div>
                                                                    </div>
																	
                                                                </div>
                                                            </div>
                                                        </div>
            
                                                        <div class="modal-footer">
                                                            <button class="btn btn-sm" data-dismiss="modal">
                                                                <i class="ace-icon fa fa-times"></i>
                                                                Cancel
                                                            </button>
            
                                                            <button class="btn btn-sm btn-primary" type="submit">
                                                                <i class="ace-icon fa fa-check"></i>
                                                               <span class="update"> Save </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
	<script>

	    $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="sub_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="sub_id[]"]').prop('checked', false);
           }
       });


		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					tinyMCE.get('ajaxfilemanager').setContent("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/menuManage/subMenuEdit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#sl_no').val(data.sl_no);
					$('#sub_menu_name').val(data.sub_menu_name);
					$('#sub_menu_title').val(data.sub_menu_title);
					$('#deeper_sub_status').val(data.deeper_sub_status);
					$('#ajaxfilemanager').val(data.sub_menu_description);
					tinyMCE.get('ajaxfilemanager').setContent(data.sub_menu_description);
					$('#select_position').val(data.position);
					$('#status').val(data.status);
					
					$('#menu_id').html('<option value="">Select Menu</option>');
					
					$.each(data.menuList, function(key, value){
						var option = '<option value="'+value.id+'">'+value.menu_name+'</option>';
						$('#menu_id').append(option);						
					});
					
					$('#menu_id').val(data.menu_id);
					$('.update').text("Update");
				}
			});
			
			e.preventDefault();
		});
		

		/*//form blank
		$(document).on("click", ".blue", function(e)
		{
			$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm input[type='select'], #addForm textarea").val("");
			  tinyMCE.get('ajaxfilemanager').setContent("");
		});
*/

          $('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/menuManage/subMenudelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="sub_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});
								
				
				//POSITION WISE SERIAL 
				$("#select_position").change(function() {
				var select_position = $("#select_position").val();			
				$.ajax({
					url : SAWEB.getSiteAction('controlpanel/menuManage/positioWiseMenu'), // URL TO LOAD BEHIND THE SCREEN
					type : "POST",
					data : { id : select_position },
					dataType : "html",
					success : function(data) {			
						$("#menu_id").html(data);
					}
				});
				
			});	

		   $(document).on("click", ".blue", function(e){
               $('.update').text("Save");
		   });
		   
	</script>
	</body>
</html>
