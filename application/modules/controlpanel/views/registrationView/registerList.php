<table  class="table table-striped table-bordered table-hover">
	<thead>
	<tr>
	<th class="center">
	<label class="pos-rel">
	<input type="checkbox" class="ace" />
	<span class="lbl"></span></label></th>
	<th>User Type </th>														
	<th class="hidden-480">Name </th>
	<th class="hidden-480">Country</th>
	<th class="hidden-480">City</th>
	<th class="hidden-480">User Id</th>
	<th class="hidden-480">Image</th>
	<th class="hidden-480">Status</th>
	<th></th>
	</tr>
	</thead>
	
	<tbody>
	<?php 
	// print_r($allRegterInfo);
	$i = 0;
	foreach ($allRegterInfo as $v){
	$id  		    = $v->id;
	?>
	<tr>
	<td class="center">
	<label class="pos-rel">
	<input type="checkbox" class="ace" />
	<span class="lbl"></span></label></td>
	
	<td>
	<a href="#"><?php echo $v->user_type; ?></a></td>														
	<td class="hidden-480"><?php echo $v->name; ?></td>
	<td class="hidden-480"><?php echo $v->country_name; ?></td>
	<td class="hidden-480"><?php echo $v->city_name; ?></td>
	<td class="hidden-480"><?php echo $v->user_id; ?></td>
	<td class="hidden-480"><img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" height="50" width="50" /></td>
	<td class="hidden-480"><?php echo $v->status; ?></td>	
	<td>
	<div class="hidden-sm hidden-xs action-buttons">
	<a class="green" href="#" data-id="<?php echo $id ?>">
	<i class="ace-icon fa fa-pencil bigger-130"></i></a>
	
	<a class="red" href="<?php echo site_url('controlpanel/adminManage/delete/'.$id); ?>">
	<i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>
	
	<div class="hidden-md hidden-lg">
	<div class="inline pos-rel">
	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
	<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button>
	
	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
	<li>
	<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
	<span class="blue">
	<i class="ace-icon fa fa-search-plus bigger-120"></i></span></a></li>
	
	<li>
	<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
	<span class="green">
	<i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span></a></li>
	
	<li>
	<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
	<span class="red">
	<i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li>
	</ul>
	</div>
	</div></td>
	</tr>
	<?php } ?>
	</tbody>
	</table>
	<script type="text/javascript">
		$('#date_of_birth').datepicker({dateFormat: 'dd/mm/yy'});
   </script>
	<script>
	// Region Wise Country
		$("#region_id").change(function() {
			var region_id = $("#region_id").val();			
			$.ajax({
				url : SAWEB.getSiteAction('controlpanel/registerMember/countryName'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : region_id },
				dataType : "html",
				success : function(data) {			
					$("#country_id").html(data);
				}
			});
			
		});
		
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});
	
	
	// Country Wise Nationality
	
	$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/nationality'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#nationality").html(data);
			}
		});
		
	});
	
	// Country Wise Code
	
	$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/countryCode'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#mobile").html(data);
			}
		});
		
	});
	
	// same data pass
	
	$('#same').on('change', function(){
		var inputFields = ['#address', '#city', '#country_phone','#mobile', '#email'];
		$.each(inputFields, function(index, value){
		
			var inputValue = $(value).val();
			
			console.log(inputValue)
			
			if( inputValue != '' && $('#same:checked').val()) {
				$(value+'_permanent').val(inputValue).attr('readonly', 'readonly');
			} else {
				$(value+'_permanent').val('').removeAttr('readonly');
			}
		});
	});	
	
	// Password Count
	
    $("#password").keyup (function(){
	 var len = $(this).val().length;
	    
		if(len<=1){
		$(".first").text("");
		$(".first").removeClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	  } else if(len<=4){
	    $(".first").text("Very Weak");
		$(".first").addClass("red");
		$(".first").removeClass("yellow");
		$(".first").removeClass("green");
		
	   } else if(len<=8){
	    $(".first").text("Good");
		$(".first").addClass("green");
		$(".first").removeClass("yellow");
		$(".first").removeClass("red");
		
	   } else if(len<=9){
	   $(".first").text("Strong");
	   $(".first").addClass("yellow");
	   $(".first").removeClass("green");
	   $(".first").removeClass("red");
	   }
	});
	
	// Password and confirm password match
	
    $('#submit').click(function(event){
    
        data = $('.password').val();
        var pass = data.length;
        
        if(pass < 1) {
            alert("Password cannot be blank");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('.password').val() != $('.conformpassword').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         
    });
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					$("#addForm").find("input[type=text], textarea").val("");
				}
			});
			
			e.preventDefault();
		});
		
		//callback handler for form submit
		//$(".edit").click(function(e)
		$(document).on("click", ".green", function(e)
		{
			var id 		= $(this).attr("data-id");
			var formURL = "<?php echo site_url('controlpanel/registerMember/edit'); ?>";
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "json",
				success:function(data){
					$('#modal-form').modal('show');
					
					$('#id').val(data.id);
					$('#region_id').val(data.region_id);
					$('#country_id').val(data.country_id);
					$('#city_id').val(data.city_id);
					$('#status').val(data.status);
					$('#user_type').val(data.user_type);
					$('#name').val(data.name);
					$('#date_of_birth').val(data.date_of_birth);
					$('#father_name').val(data.father_name);
					$('#mother_name').val(data.mother_name);
					$('#nationality').val(data.nationality);
					$('#address').val(data.address);
					$('#city').val(data.city);
					$('#country_phone').val(data.country_phone);
					$('#mobile').val(data.mobile);
					$('#email').val(data.email);
					$('#address_permanent').val(data.address_permanent);
					$('#city_permanent').val(data.city_permanent);
					$('#country_phone_permanent').val(data.country_phone_permanent);
					$('#mobile_permanent').val(data.mobile_permanent);
					$('#user_id').val(data.user_id);
					$('#password').val(data.password);
					$('#status').val(data.status);
				}
			});
			
			e.preventDefault();
		});
		
		//select/deselect all rows according to table header checkbox
                var active_class = 'active';
                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
                    var th_checked = this.checked;//checkbox inside "TH" table header
                    
                    $(this).closest('table').find('tbody > tr').each(function(){
                        var row = this;
                        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                    });
                });
                
                //select/deselect a row when the checkbox is checked/unchecked
                $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
                    var $row = $(this).closest('tr');
                    if(this.checked) $row.addClass(active_class);
                    else $row.removeClass(active_class);
                });
				
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/registerMember/delete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>