<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Registration Csv file</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
										   <form action="<?php echo site_url('controlpanel/registerMember/csvfileUploadAction'); ?>" method="post" enctype="multipart/form-data">
											<table width="1304"  class="table table-striped table-bordered table-hover">
											<thead>
											<tr>
											  <th align="right" valign="middle" class="text-right"  style="color:#0000FF"><?php if(!empty($suceess)){ echo $suceess; } ?></th>
											  <th width="358" align="right" class="text-right" valign="middle">Csv file upload </th>														
											<th width="100"><input type="file" id="csvfile" name="csvfile" tabindex="2" /></th>
											<th width="120"><button class="btn btn-primary" type="submit" id="search"> Submit</button></th>
											</tr>
											</thead>
											</table>
											</form>

											<table  class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="20" class="center">
														   Sl No														</th>
														<th width="80">Region</th>
														<th width="80">Country</th>
														<th width="80">User Name</th>														
														<th width="50">User Type</th>
													</tr>
												</thead>

												<tbody>
												   <?php 
												      
												      if(isset($allCsvRegInfo)) {
                                                       $i = $onset + 1;
												       foreach($allCsvRegInfo as $v){ 
												     ?>
													
													<tr>
														<td class="center"><?php echo $i++; ?>
														<td><?php echo $v->region_name ?></td>
														<td><?php echo $v->country_name ?></td>
														<td>
															<a href="#"></a> <?php echo $v->name ?>														</td>														
														<td class="hidden-480"><?php echo $v->user_type ?></td>
													</tr>

													<?php } }  ?>
													
													<tr>
												  <td colspan="5" align="right" valign="middle" class="center"><?php echo $this->pagination->create_links(); ?>													  </tr>
												</tbody>
											</table>

</body>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
    <?php $this->load->view('formJsLinkPage'); ?>
	
	</body>
</html>
