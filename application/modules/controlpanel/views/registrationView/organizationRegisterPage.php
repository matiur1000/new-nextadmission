<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Registration View </a>
							</li>
							<li class="active">All Organization User Registration Details</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">
										   <form action="<?php echo site_url('controlpanel/registerMember'); ?>" method="POST">
										    <div style="float:left; display:inline">Display &nbsp;
											    <select name="perPageLimit" id="perPageLimit" onchange="this.form.submit()">
											        <option <?php if($perPageLimit =='30'){ ?> selected ="selected" <?php } ?> value="30">30</option>
											        <option <?php if($perPageLimit =='50'){ ?> selected ="selected" <?php } ?> value="50">50</option>
											        <option <?php if($perPageLimit =='100'){ ?> selected ="selected" <?php } ?> value="100">100</option>
											        <option <?php if($perPageLimit =='all'){ ?> selected ="selected" <?php } ?> value="all">all</option>
											    </select> &nbsp; records
											</div>	
											</form>
											<div style="float:left; display:inline; padding-left:40px">Select Country &nbsp;
											    <select name="country_id" id="country_id">
											       <?php foreach ($allCountryInfo as $v){ ?>
											          <option value="">Select country</option>
											          <option value="<?php echo $v->id ?>"><?php echo $v->country_name ?></option>
											        <?php } ?>
											    </select> 
											</div>

											<div style="float:left; display:inline; padding-left:40px">Search By Name &nbsp;
											    <input type="text" id="searchUser" placeholder="Search" name="searchUser"
                                                                            tabindex="2" style="height:30px"  /> 
											</div>		
                                        	<a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">Send Mail</a>											
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable"   class="table table-striped table-bordered table-hover">
											<input type="hidden" name="title2" id="title2">
											<input type="hidden" name="msg" id="msg">
											<thead>
											<tr>
											<th class="center">
											<label class="pos-rel">
											<input type="checkbox" class="ace allCheck" />
											<span class="lbl"></span></label>
											<label class="pos-rel">
											<a class="red" href="#">
												<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
											<span class="lbl"></span></label></th>
											<th>Sl</th>
											<th class="hidden-480">Name </th>
											<th class="hidden-480">Country</th>
											<th class="hidden-480">City</th>
											<th class="hidden-480">User Id</th>
											<th class="hidden-480">Image</th>
											<th class="hidden-480">Status</th>
											<th align="center" valign="middle">All Details</th>
											</tr>
											</thead>
											
											<tbody>
											<?php 
											// print_r($allRegterInfo);
											if(isset($allRegterInfo)) {
                                              $i = $onset + 1;
											   foreach ($allRegterInfo as $v){
											   $id  		    = $v->id;
											?>
											<tr>
											<td class="center">
											<label class="pos-rel">
											  <input type="checkbox" name="user_id[]" class="ace" value="<?php echo $id ?>" />
											<span class="lbl"></span></label></td>
											
											
											<td><?php echo $i++; ?></td>
											<td class="hidden-480"><?php echo $v->name; ?></td>
											<td class="hidden-480"><?php echo $v->country_name; ?></td>
											<td class="hidden-480"><?php echo $v->city_name; ?></td>
											<td class="hidden-480"><?php echo $v->user_id; ?></td>
											<td class="hidden-480"><img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" height="50" width="50" /></td>
											<td class="hidden-480"><?php echo $v->status; ?></td>	
											<td align="center" valign="middle"><a href="<?php echo site_url('controlpanel/registerMember/orgUserWiseDetails/'.$id);?>">View</a></td>
											</tr>
											<?php } } ?>
											<tr>
												<th height="43" colspan="9" class="center">
													<label class="pos-rel"><span class="lbl"></span></label>
													  <ul class="pager">
														  <li><?php echo $this->pagination->create_links(); ?></li>
													  </ul>													</th>
												</tr>
											</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
					         <form id="addForm" action="<?php echo site_url('controlpanel/registerMember/organizationUserMailAction'); ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" id="id" value="" />
	                                <div id="modal-form" class="modal" tabindex="-1">
	                                    <div class="modal-dialog">
	                                        <div class="modal-content">
	                                            <div class="modal-header">
	                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                                <h4 class="blue bigger">All Organization User Email Send</h4>
	                                                <span class="alert"></span>
	                                            </div>

	                                            <div class="modal-body">
	                                                <div class="row"> 
	                                                    <div class="col-xs-12 col-sm-12">
	                                                        <div class="space-4"></div>

	                                                        <div class="form-group">
	                                                            <label for="amount">Title</label>

	                                                           <div>
	                                                               <input type="text" id="title" placeholder="Title" name="title"
                                                                            tabindex="2" class="form-control" required /> 
																	
	                                                            </div>
	                                                        </div>
	                                                        <div class="form-group">
	                                                            <label for="amount">Message</label>

	                                                           <div>
                                                                        <textarea class="form-control" rows="15" cols="1" placeholder="*..Message" 
																	   tabindex="8" name="message" id="message" ></textarea>
																	
	                                                            </div>
	                                                        </div>              
	                                                    </div>
	                                                   
	                                                </div>
	                                            </div>

	                                            <div class="modal-footer">
	                                                <button class="btn btn-sm" data-dismiss="modal">
	                                                    <i class="ace-icon fa fa-times"></i>
	                                                    Cancel
	                                                </button>

	                                                <button class="btn btn-sm btn-primary" type="submit">
	                                                    <i class="ace-icon fa fa-check"></i>
	                                                    Save
	                                                </button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </form>
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
   <script>

        $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="user_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="user_id[]"]').prop('checked', false);
           }
       });



    //callback handler for form submit
		$("#addForm").submit(function(e)
		{

			var message 		= $("#message").val();
			var title 			= $("#title").val();
			$("#inputTable #title2").val(title);	
			$("#inputTable #msg").val(message);	
			
			var postData = $("#inputTable input, select").serializeArray();
			var formURL = $(this).attr("action");
			console.log(formURL);

		
			$.ajax(
			{
				url : formURL,
				type: "POST",
				async: false,
				data : postData,
				success:function(data){
					$("#addForm").find("input[type=text], textarea").val("");
					alert('Email send success');
					$("#inputTable").find('input[name="user_id[]"],.allCheck').prop('checked', false);
					location.reload();
				}
			});
			
			e.preventDefault();
		});
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
		console.log(country_id);
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/countryWiseUser'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { country_id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#listView").html(data);
			}
		});
		
	});

		$("#searchUser").keyup(function() {
		var userName = $("#searchUser").val();	
		console.log(userName);
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/findOrganizeUser'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { userName : userName },
			dataType : "html",
			success : function(data) {			
				$("#listView").html(data);
			}
		});
		
	});


		$('.red').on('click', function(e) {
	       var deleteDta = $("#inputTable input").serializeArray();
	       console.log(deleteDta);
			$.ajax({
			url: "<?php echo site_url('controlpanel/registerMember/orgDelete') ?>",
			method: "POST",	
			data: $("#inputTable input").serializeArray(),
			dataType: "html",
			success: function(data){
				  $("#inputTable").find('input[name="user_id[]"],.allCheck').prop('checked', false);
				  location.reload();
			   }
		    });

		   e.preventDefault();
			
		});

	</script>
	
	</body>
</html>
