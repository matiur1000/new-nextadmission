<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_title; ?></title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<?php  $this->load->view('cssLinkPage'); ?>
		<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>

	</head>

	<body class="no-skin">
		<?php  $this->load->view('headerPage'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<?php  $this->load->view('leftSidebar'); ?><!-- /.sidebar-shortcuts -->				
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>

							<li>
								<a href="#">Registration View </a>
							</li>
							<li class="active">All General User Registration Details</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->															
								<div class="row">
									<div class="col-xs-12">
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header" align="right">
                                        <a href="#modal-form" role="button" class="label label-xlg label-light arrowed-in-right blue"
                                             data-toggle="modal" style="text-decoration:none;">Send Mail</a>											
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
                                        
                                        <!--MODAL FORM-->
                                        
										<!-- div.dataTables_borderWrap -->
										 <div class="span12" id="listView">
											<table id="inputTable"  class="table table-striped table-bordered table-hover">
											<thead>
											<tr>
											<th class="center">
											<label class="pos-rel">
											<input type="checkbox" class="ace allCheck" />
											<span class="lbl"></span></label>
											<label class="pos-rel">
											<a class="red" href="#">
												<i class="ace-icon fa fa-trash-o bigger-130"></i></a>
											<span class="lbl"></span></label></th>
											<th>Sl</th>
											<th>User Type </th>														
											<th class="hidden-480">Name </th>
											<th class="hidden-480">Country</th>
											<th class="hidden-480">City</th>
											<th class="hidden-480">User Id</th>
											<th class="hidden-480">Image</th>
											<th class="hidden-480">Status</th>
											<th align="center" valign="middle">All Details</th>
											</tr>
											</thead>
											
											<tbody>
											<?php 
											// print_r($allRegterInfo);
											if(isset($allRegterInfo)) {
                                              	$i = $onset + 1;
												foreach ($allRegterInfo as $v){
												$id  		    = $v->id;
											?>
											<tr>
											<td class="center">
											<label class="pos-rel">
											<input type="checkbox" name="user_id[]" class="ace" value="<?php echo $id ?>" />
											<span class="lbl"></span></label></td>
											
											
											<td><?php echo $i++; ?></td>
											<td> <?php echo $v->user_type; ?></td>														
											<td class="hidden-480"><?php echo $v->name; ?></td>
											<td class="hidden-480"><?php echo $v->country_name; ?></td>
											<td class="hidden-480"><?php echo $v->city_name; ?></td>
											<td class="hidden-480"><?php echo $v->user_id; ?></td>
											<td class="hidden-480"><img src="<?php echo base_url("Images/Register_image/$v->image"); ?>" height="50" width="50" /></td>
											<td class="hidden-480"><?php echo $v->status; ?></td>	
											<td align="center" valign="middle"><a href="<?php echo site_url('controlpanel/registerMember/userWiseDetails/'.$id);?>">View</a></td>
											</tr>
											<?php } } ?>
											<tr>
												<th height="43" colspan="10" class="center">
													<label class="pos-rel"><span class="lbl"></span></label>
													    <ul class="pager">
														  <li><?php echo $this->pagination->create_links(); ?></li>
													    </ul>														  
													</th>
											  </tr>
											</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
								
							<form id="addForm" action="<?php echo site_url('controlpanel/registerMember/generalUserMailAction'); ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="" />
	                                <div id="modal-form" class="modal" tabindex="-1">
	                                    <div class="modal-dialog">
	                                        <div class="modal-content">
	                                            <div class="modal-header">
	                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                                <h4 class="blue bigger">All General User Email Send</h4>
	                                            </div>

	                                            <div class="modal-body">
	                                                <div class="row"> 
	                                                    <div class="col-xs-12 col-sm-12">
	                                                        <div class="space-4"></div>

	                                                        <div class="form-group">
	                                                            <label for="amount">Tittle</label>

	                                                           <div>
	                                                               <input type="text" id="tittle" placeholder="Tittle" name="tittle"
                                                                            tabindex="2" class="form-control" required /> 
																	
	                                                            </div>
	                                                        </div>
	                                                        <div class="form-group">
	                                                            <label for="amount">Message</label>

	                                                           <div>
                                                                        <textarea class="form-control" rows="15" cols="1" placeholder="*..Message" 
																	   tabindex="8" name="message" id="message" ></textarea>
																	
	                                                            </div>
	                                                        </div>              
	                                                    </div>
	                                                   
	                                                </div>
	                                            </div>

	                                            <div class="modal-footer">
	                                                <button class="btn btn-sm" data-dismiss="modal">
	                                                    <i class="ace-icon fa fa-times"></i>
	                                                    Cancel
	                                                </button>

	                                                <button class="btn btn-sm btn-primary" type="submit">
	                                                    <i class="ace-icon fa fa-check"></i>
	                                                    Save
	                                                </button>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </form>
								
								<!-- from -->
						
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				<?php  $this->load->view('footerPage'); ?>

		
		</div><!-- /.main-container -->

		<!-- basic scripts -->
<?php $this->load->view('formJsLinkPage'); ?>
     <script>

     $(document).on("click", ".allCheck", function(e){
       	   var allChecked = this.checked; 
           var parent = $(this).parents('#inputTable');
           if(allChecked){
             parent.find('input[name="user_id[]"]').prop('checked', true);
           } else {
             parent.find('input[name="user_id[]"]').prop('checked', false);
           }
       });
	
		
		// Country Wise City
		
		$("#country_id").change(function() {
		var country_id = $("#country_id").val();	
	
		$.ajax({
			url : SAWEB.getSiteAction('controlpanel/registerMember/cityName'), // URL TO LOAD BEHIND THE SCREEN
			type : "POST",
			data : { id : country_id },
			dataType : "html",
			success : function(data) {			
				$("#city_id").html(data);
			}
		});
		
	});
	
	
	
		//callback handler for form submit
		$("#addForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#listView").html(data);				
					//$("#addForm").find("input[type=text], textarea").val("");
					$("#addForm input[type='text'], #addForm input[type='hidden'], #addForm textarea").val("");
					
					$.each($('.attachmentbody'), function(i, attachment) {
						attachment = $(attachment).html('<img class="upload" src="'+SAWEB.getBaseAction('resource/img/no_image.png')+'" />');
						reInitiateFileUpload(attachment);                        
					});
				}
			});
			
			e.preventDefault();
		});
		
		
		
				$('.red').on('click', function() {
					var x = confirm('Are you sure to delete?');
					
					if(x){
						var id = $(this).attr('data-id');
						console.log(id);
						var url = SAWEB.getSiteAction('controlpanel/registerMember/genDelete/'+id);
						location.replace(url);
					} else {
						return false;
					}
				});
	</script>
	
	</body>
</html>
