<?php

class M_seo extends CI_Model {

	const TABLE	= 'seo';

    public function __construct()
    {
        parent::__construct();
    }
		
	public function save($data)
    {
        $this->db->insert(self::TABLE, $data);        
    }
	
	
	public function findAll(Array $where = array())
    {
        $this->db->select('*');
        $this->db->from(self::TABLE);
	    $this->db->where($where);
		$this->db->order_by("id", "asc"); 
		$query = $this->db->get();
        return $query->result();
    }
	
			
}