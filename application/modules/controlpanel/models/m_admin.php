<?php

	class M_admin extends CI_Model {
	
		const TABLE	= 'admin';
	
		public function __construct()
		{
			parent::__construct();
		}
			
		public function save($data)
		{
			$this->db->insert(self::TABLE, $data);        
		}
		
		public function findByUserName($userName)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('user_name', $userName);
			$this->db->where('satus', 'Active');
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function findAll(Array $where = array())
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->order_by("id", "desc"); 
			$query = $this->db->get();
			return $query->result();
		}
		
		
		public function findById($id)
		{
			$this->db->select('*');
			$this->db->from(self::TABLE);
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}
		
		
		public function update($data, $id)
		{
			$this->db->update(self::TABLE, $data, array('id' => $id));        
		}
		
		public function destroy($id)
		{
			$this->db->delete(self::TABLE, array('id' => $id));        
		}
		
		
			
	}
?>