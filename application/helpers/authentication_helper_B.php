<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('loginUser')) 
{
	function loginUser()
	{
		$_CI = &get_instance();
		
		$UserName		 = $_CI->input->post('adminname');
		$UserPass		 = $_CI->input->post('password');
		$pass_md5		 = md5($UserPass);
		
		$UserDetails	 = $_CI->M_admin->findByUserName($UserName);
		
		if( !empty($UserDetails) && $UserDetails->password == $pass_md5){
			setUserData($UserName);	 // TO SET DATA IN SESSION FIELD	
			redirect('controlpanel/home'); 
		} else {
			$_CI->session->set_userdata(array('msg' => "You are not a valid user"));
			redirect('controlpanel/login');
		}
	}
}

// TO SET DATA IN UserName SESSION FIELD
if ( ! function_exists('setUserData'))
{
	function setUserData($UserName)
	{
		$_CI = &get_instance();
		$userData	= array(
			'UserName'    	=> $UserName
		);
		$_CI->session->set_userdata($userData);
	}
}


if ( ! function_exists('getUserName'))
{
	function getUserName()
	{
		$_CI = &get_instance();
		return $_CI->session->userdata('UserName');
	}
}


if ( ! function_exists('invalidUserData'))
{
	function invalidUserData()
	{
		setUserData(NULL);
	}
}


if ( ! function_exists('isActiveUser'))
{
	function isActiveUser()
	{
		return getUserName() != NULL;
	}
}

if ( ! function_exists('isAuthenticate'))
{
	function isAuthenticate()
	{
		if(!isActiveUser()) {
			redirect('controlpanel/login');	
		} else {
			return true;	
		}
	}
}


if ( ! function_exists('logoutUser'))
{
	function logoutUser()
	{
		invalidUserData();
		if(!isActiveUser()){
			redirect('controlpanel/login');
		}
	}
}


// ------------------------------------------------------------------------
/* End of file authentication_helper.php */
/* Location: ./application/helpers/authentication_helper.php */