<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('loginUser')) 
{
	function loginUser()
	{
		$_CI = &get_instance();
		
		$UserName		 = $_CI->input->post('email');
		$UserPass		 = $_CI->input->post('password');
		
		$UserDetails	 = $_CI->M_all_user_registration->findByUserName($UserName);
		
		$ref = $_CI->session->userdata('ref');
		$_CI->session->set_userdata(array('ref' => ''));		
		
		if( !empty($UserDetails) && $UserDetails->password == $UserPass && $UserDetails->user_type == 'General user'){
			setUserData($UserName);	 // TO SET DATA IN SESSION FIELD	
			if(!empty($ref)) {
				redirect($ref); 
			} else {
				redirect('generalUserHome'); 
			}
		 }else if( !empty($UserDetails) && $UserDetails->password == $UserPass && $UserDetails->user_type == 'Organization User' && $UserDetails->premium_status == '0'){
		    setUserData($UserName);	 // TO SET DATA IN SESSION FIELD	
			if(!empty($ref)) {
				redirect($ref); 
			} else {
				redirect('organizationUserHome'); 
			}
		}else if( !empty($UserDetails) && $UserDetails->password == $UserPass && $UserDetails->user_type == 'Organization User' && $UserDetails->premium_status == '1'){
			setUserData($UserName, 1);	 // TO SET DATA IN SESSION FIELD	
			if(!empty($ref)) {
				redirect($ref); 
			} else {
				redirect('premiumUserHome'); 
			}
		} else {
			$data['msg']   = 'you r not a valid user';
			redirect('loginForm', $data);
			$_CI->load->view('loginForm', $data);
		}
	}
}

// TO SET DATA IN UserName SESSION FIELD
if ( ! function_exists('setUserData'))
{
	function setUserData($UserName, $PremiumStatus = 0)
	{
		$_CI = &get_instance();
		$userData	= array(
			'UserId'    	=> $UserName,
			'PremiumStatus'	=> $PremiumStatus
		);
		$_CI->session->set_userdata($userData);
	}
}


if ( ! function_exists('getUserName'))
{
	function getUserName()
	{
		$_CI = &get_instance();
		return $_CI->session->userdata('UserId');
	}
}


if ( ! function_exists('invalidUserData'))
{
	function invalidUserData()
	{
		setUserData(NULL);
	}
}


if ( ! function_exists('isActiveUser'))
{
	function isActiveUser()
	{
		return getUserName() != NULL;
	}
}

if ( ! function_exists('isAuthenticate'))
{
	function isAuthenticate()
	{
		if(!isActiveUser()) {
			redirect('loginForm');	
		} else {
			return true;	
		}
	}
}


if ( ! function_exists('logoutUser'))
{
	function logoutUser()
	{
		invalidUserData();
		if(!isActiveUser()){
			redirect('loginForm');
		}
	}
}


