<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $headerBasicInfo->title; ?></title>
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico" />

    <!-- Bootstrap -->
    <link href="<?php echo base_url('resource/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('resource/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!--CUSTOM BODY-->
	<link href="<?php echo base_url('resource/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/menu_styles.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/default.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/ieonly.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/login_style.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('resource/css/breakingNews.css'); ?>" rel="stylesheet">
	
      
    <script src="<?php echo base_url('resource/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('resource/ajax_function.js'); ?>"></script>
	<script src="<?php echo base_url('resource/source/text_scroll.js'); ?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('adapter/javascript'); ?>"></script>
    <style>
	  	.area {
			background:#000000;
		}
		.area:hover{
		  background:#FF0000;
		}
		.advanced, .advanced:hover {
			text-decoration: none;
			cursor: pointer;
		}
		#advancedSearch{
			margin-top: 7px;
		}

		body.modal
		{
		   overflow:hidden;
		   position:relative;
		}
		.breakingNews>.bn-title {
		    background: #e77522;
		}
		.breakingNews>.bn-title h2{
			color: #fff !important;
		}
	</style>
	
		
  </head>
  <body>
        <div class="container">
           <div class="row">&nbsp;</div> 
			  <?php $this->load->view('headerPage'); ?>
           <div class="row" style="height:15px;"></div>            
	    
            <div class="row">
               <div class="col-lg-12" style="padding:10px 0 0 15px; padding-bottom:5px;">
                   <div class="breakingNews bn-small">
				    <div class="bn-title"><h2>Breaking News</h2><span></span></div>
						
						<ul>
							<div class="row" style="padding-top:5px;">
							<marquee id='scroll_news'>
								<div onMouseOver="document.getElementById('scroll_news').stop();" onMouseOut="document.getElementById('scroll_news').start();">
								<?php foreach($breakInfo as $v){?>
				&nbsp; <i class="fa fa-hand-o-right" aria-hidden="true"></i> <a href="#" class="latesnewId" data-id="<?php echo $v->id; ?>"><?php echo $v->title; ?></a> 
								<?php } ?>
								</div>
							</marquee>
							</div>
						</ul>
					
				    </div>
				</div>
            </div>  
			       
            <div class="row">
            	<div class="col-lg-12" style="padding-right:0px;">
                	<?php $this->load->view('menuPage'); ?>
                </div>
            </div> 
            <div class="row">&nbsp;</div>  
				
			<div class="row">  <!--row Start-->
			   	<div class="col-lg-9">  <!--col 9 Start-->
					<div class="col-lg-3" style="float:left;">
						<?php $this->load->view('leftSidebarPage'); ?>
					</div>
					
													
					<div class="col-lg-9">
					    <form id="searchForm" action="<?php echo site_url('home/searchAll'); ?>" method="post" enctype="multipart/form-data">
							<div class="form-group margin-0">				
								<div class="input-group">
									<input type="text" name="search_all" id="search_all" class="form-control"  placeholder="Search by keyword">
									<span class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i> Search</button>
									</span>												
							    </div><!-- /input-group -->		
							</div>	
						</form> 

						    <div id="advancedSearch" style="display:none">
								<div class="col-lg-4 padding-0">
									<div class="form-group margin-0">
										<select class="form-control" id="country_id_chnge" name="country_id"  tabindex="1">
											<option value="" selected>Select Country</option>
											<?php foreach ($countryInfo as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->country_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group margin-0">
										<select class="form-control" id="organize_id" name="organize_id"  tabindex="2" >
											<option value="" selected>Select Organization</option>
											<?php foreach ($orgInfo as $v){ ?>
											<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-lg-4 padding-0">
									<div class="form-group margin-0">
										<select class="form-control" id="program_id" name="program_id"  tabindex="3">
											<option value="" selected>Select Program</option>
											<?php foreach ($allProgramName as $v){ ?>
												<option value="<?php echo $v->id; ?>"><?php echo $v->programe_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
	
							</div>
						
						<p><a class="advanced">Advanced Search</a></p>
										

						<div id="searchOrganization" class="blog-post">
							<?php $this->load->view('middlePage'); ?>
						</div>
					</div>						   
		        </div>  <!--col 9 end--> 
					<?php $this->load->view('rightSidebarPage'); ?>
				<!--col 3 end--> 
		  	</div>

			 	 <!-- <?php $this->load->view('advertisementManagePage'); ?>
			           <div class="row">&nbsp;</div>      -->
 
			     <?php $this->load->view('newsEventManagePage'); ?>
            </div>
       <!--footer-->
	   <?php $this->load->view('footerPage'); ?>

       <div class="modal fade" id="blogDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">				
					
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="breaking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content" id="breakingNewidDeatial">
			 
			</div>
		  </div>
		</div>		

	   <script src="<?php echo base_url("resource/js/ajaxupload.3.5.js"); ?>"></script>
	<script>

	     $(function(){
    var $mwo = $('.LetestNews');
    //$('.marquee').marquee();
    $('.LetestNews').marquee({
        //speed in milliseconds of the marquee
        speed: 9000,
        //gap in pixels between the tickers
        gap: 1,
        //gap in pixels between the tickers
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
        pauseOnHover: true
    });
    //Direction upward
    
    //pause and resume links
    $('.pause').click(function(e){
        e.preventDefault();
        $mwo.trigger('pause');
    });
    $('.resume').click(function(e){
        e.preventDefault();
        $mwo.trigger('resume');
    });
    //toggle
    $('.toggle').hover(function(e){
        $mwo.trigger('pause');
    },function(){
        $mwo.trigger('resume');
    })
    .click(function(e){
        e.preventDefault();
    })
});


	    $(document).on("click", ".updatePost", function(e)
		{
		   var id 		= $(this).attr("data-id");
		   var formURL  = "<?php echo site_url('commentPopup/postedit'); ?>";
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : {id: id},
				dataType: "html",
				success:function(data){
					$("#blogDetails .modal-content").html(data);
					$("#modal-form").modal({
						keyboard: false,
						backdrop: 'static',
					});

					 initiateFileUpload();
				}
			});
		});
	    
	   /*$(document).ready(function() {
	      $("body").on("contextmenu",function(){
	         return false;
	      }); 
	   }); */

	   /*$('body').bind('copy paste cut drag drop', function (e) {
   			e.preventDefault();
		});
*/

	   $("#searchForm").submit(function(e)
		{
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			console.log(postData);
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data){
					$("#searchOrganization").html(data);				
				}
			});
			
			e.preventDefault();
		});
		
		//North America Effict
		$(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		$(".advanced").on("click", function(){
		 	$("#advancedSearch").toggle(300);
		});
		 
		//COUNTRY WISE SEARCH 
        $(document).on("change", "#country_id_chnge", function(){
			var country_id = $(this).val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
        });


        


		/*$("#country_id_chnge").change(function() {
			alert('okk');
			var country_id = $("#country_id").val();	
			console.log(country_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/countryWiseOrganize'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#organize_id").html(data);
				}
			});			
		});	*/
			
	   //ORGANIZATION WISE PROGRAM 

	     $("#organize_id").change(function() {
			var organize_id = $("#organize_id").val();	
			console.log(organize_id);
				
			$.ajax({
				url : SAWEB.getSiteAction('home/OrganizeWiseProgram'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { id : organize_id },
				dataType : "html",
				success : function(data) {			
					$("#program_id").html(data);
				}
			});			
		});	



        // On change search all
	     $(document).on("change", "#country_id_chnge", function(){
			var country_id = $(this).val();	
			console.log(country_id);

			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id },
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});		
        });

	      $("#organize_id").change(function() {
			var country_id  = $("#country_id_chnge").val();	
			var organize_id = $("#organize_id").val();	
				
			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id, organize_id : organize_id},
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});			
		});	


	     $("#program_id").change(function() {
			var country_id  = $("#country_id_chnge").val();	
			var organize_id = $("#organize_id").val();	
			var program_id  = $("#program_id").val();	

			$.ajax({
				url : SAWEB.getSiteAction('home/onChangeSearchAll'), // URL TO LOAD BEHIND THE SCREEN
				type : "POST",
				data : { country_id : country_id, organize_id : organize_id, program_id : program_id},
				dataType : "html",
				success : function(data) {			
					$("#searchOrganization").html(data);
				}
			});			
		});	

	
		$('.goto-news').on('click', function(){
			console.log($('#news'));
			console.log($('#news').offset().top);
			$('body,html').animate({
				scrollTop: ($('#news').offset().top - 50),
			 	}, 1200
			);
		});	


		$('.readmore').on('click', function(e){
			var url = $(this).attr('data-url');
			$.ajax({
				url : url, // URL TO LOAD BEHIND THE SCREEN
				type : "GET",
				dataType : "html",
				success : function(data) {			
					$("#blogDetails .modal-content").html(data);
					$("#blogDetails").modal({
						keyboard: false,
						backdrop: 'static',
					});
					$("#blogDetails").modal('show');
				}
			});

			e.preventDefault();		
		});
		
		
		
		
		
		$(".latesnewId").on('click', function(){
			var id = $(this).attr("data-id");
			var laturl = "<?php echo site_url("home/latesnewIdd"); ?>";
			
			$.ajax(
			{
				url : laturl,
				type: "POST",
				data:{id:id},
				success:function(data){
				$('#breaking').modal('show');
				$("#breakingNewidDeatial").html(data);
				}
			});
			
		});
		
		
		
		
		
		
		
	</script>
	
    
  </body>
</html>